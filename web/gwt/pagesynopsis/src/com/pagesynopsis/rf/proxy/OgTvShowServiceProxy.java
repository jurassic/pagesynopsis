package com.pagesynopsis.rf.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;

import com.pagesynopsis.rf.config.Config;


public class OgTvShowServiceProxy extends AbstractBaseServiceProxy implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_OGTVSHOW = "ogTvShows";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public OgTvShowServiceProxy()
    {
        initCache();
    }


    protected WebResource getOgTvShowWebResource()
    {
        return getWebResource(RESOURCE_OGTVSHOW);
    }
    protected WebResource getOgTvShowWebResource(String path)
    {
        return getWebResource(RESOURCE_OGTVSHOW, path);
    }
    protected WebResource getOgTvShowWebResourceByGuid(String guid)
    {
        return getOgTvShowWebResource(guid);
    }


    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        OgTvShow bean = null;

        String key = getResourcePath(RESOURCE_OGTVSHOW, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getOgTvShowWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgTvShowStub stub = clientResponse.getEntity(OgTvShowStub.class);
                bean = MarshalHelper.convertOgTvShowToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgTvShow bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        OgTvShow bean = getOgTvShow(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("director")) {
            return bean.getDirector();
        } else if(field.equals("writer")) {
            return bean.getWriter();
        } else if(field.equals("actor")) {
            return bean.getActor();
        } else if(field.equals("duration")) {
            return bean.getDuration();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("releaseDate")) {
            return bean.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        throw new NotImplementedException("To be implemented.");
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<OgTvShow> list = null;
    	
        WebResource webResource = getOgTvShowWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgTvShowListStub stub = clientResponse.getEntity(OgTvShowListStub.class);
                list = MarshalHelper.convertOgTvShowListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgTvShow list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;
    	
        WebResource webResource = getOgTvShowWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgTvShow key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<OgTvShow> list = null;
    	
//        ClientResponse clientResponse = getOgTvShowWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getOgTvShowWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgTvShowListStub stub = clientResponse.getEntity(OgTvShowListStub.class);
                list = MarshalHelper.convertOgTvShowListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgTvShow list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
    	List<String> list = null;

    	WebResource webResource = getOgTvShowWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgTvShow key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getOgTvShowWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "OgTvShow count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        OgTvShowBean bean = new OgTvShowBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgTvShow(bean);        
    }

    @Override
    public String createOgTvShow(OgTvShow bean) throws BaseException
    {
        String guid = null;
        OgTvShowStub stub = MarshalHelper.convertOgTvShowToStub(bean);
        WebResource webResource = getOgTvShowWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New OgTvShow guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New OgTvShow resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public OgTvShow constructOgTvShow(OgTvShow bean) throws BaseException
    {
        OgTvShowStub stub = MarshalHelper.convertOgTvShowToStub(bean);
        WebResource webResource = getOgTvShowWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(OgTvShowStub.class);
                bean = MarshalHelper.convertOgTvShowToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New OgTvShow bean = " + bean);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New OgTvShow resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgTvShow guid is invalid.");
        	throw new BaseException("OgTvShow guid is invalid.");
        }
        OgTvShowBean bean = new OgTvShowBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgTvShow(bean);        
    }

    @Override
    public Boolean updateOgTvShow(OgTvShow bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgTvShow object is invalid.");
        	throw new BaseException("OgTvShow object is invalid.");
        }
        OgTvShowStub stub = MarshalHelper.convertOgTvShowToStub(bean);

        WebResource webResource = getOgTvShowWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        //ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).put(ClientResponse.class, stub);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public OgTvShow refreshOgTvShow(OgTvShow bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgTvShow object is invalid.");
        	throw new BaseException("OgTvShow object is invalid.");
        }
        OgTvShowStub stub = MarshalHelper.convertOgTvShowToStub(bean);

        WebResource webResource = getOgTvShowWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                stub = clientResponse.getEntity(OgTvShowStub.class);
                bean = MarshalHelper.convertOgTvShowToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully refreshed the OgTvShow bean = " + bean);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgTvShow guid is invalid.");
        	throw new BaseException("OgTvShow guid is invalid.");
        }

        WebResource webResource = getOgTvShowWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deleteOgTvShow(OgTvShow bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteOgTvShow(guid);
    }

    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getOgTvShowWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exception if the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        // Order of the mime types ???
        ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted OgTvShows: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGTVSHOW);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    {
        log.finer("BEGIN");

        if(ogTvShows == null) {
            log.log(Level.WARNING, "createOgTvShows() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = ogTvShows.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOgTvShows() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OgTvShow ogTvShow : ogTvShows) {
            String guid = createOgTvShow(ogTvShow);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOgTvShows() failed for at least one ogTvShow. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgTvShows(List<OgTvShow> ogTvShows) throws BaseException
    //{
    //}

}
