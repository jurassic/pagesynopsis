package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OpenGraphMetaJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.OpenGraphMetaWebService;
import com.pagesynopsis.web.proxy.OpenGraphMetaWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OpenGraphMeta;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OpenGraphMetaProxyWebService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaProxyWebService.class.getName());
     
    // Af service interface.
    private OpenGraphMetaWebServiceProxy mServiceProxy = null;

    public OpenGraphMetaProxyWebService()
    {
        //this(new OpenGraphMetaWebServiceProxy());
        this(null);
    }
    public OpenGraphMetaProxyWebService(OpenGraphMetaWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private OpenGraphMetaWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new OpenGraphMetaWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public OpenGraphMetaJsBean findOpenGraphMetaByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findOpenGraphMetaByTargetUrl(targetUrl, fetch, null);
    }
    public OpenGraphMetaJsBean findOpenGraphMetaByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            OpenGraphMetaJsBean jsBean = null;
            OpenGraphMeta openGraphMeta = getServiceProxy().findOpenGraphMetaByTargetUrl(targetUrl, fetch, refresh);
            if(openGraphMeta != null) {
                jsBean = OpenGraphMetaWebService.convertOpenGraphMetaToJsBean(openGraphMeta);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
