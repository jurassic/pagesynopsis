package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.RobotsTextFileWebService;
import com.pagesynopsis.web.proxy.RobotsTextFileWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextFile;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextFileProxyWebService
{
    private static final Logger log = Logger.getLogger(RobotsTextFileProxyWebService.class.getName());
     
    // Af service interface.
    private RobotsTextFileWebServiceProxy mServiceProxy = null;

    public RobotsTextFileProxyWebService()
    {
        //this(new RobotsTextFileWebServiceProxy());
        this(null);
    }
    public RobotsTextFileProxyWebService(RobotsTextFileWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private RobotsTextFileWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new RobotsTextFileWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public RobotsTextFileJsBean findRobotsTextFileBySiteUrl(String siteUrl) throws WebException
    {
        try {
            RobotsTextFileJsBean jsBean = null;
            RobotsTextFile robotsTextFile = getServiceProxy().findRobotsTextFileBySiteUrl(siteUrl);
            if(robotsTextFile != null) {
                jsBean = RobotsTextFileWebService.convertRobotsTextFileToJsBean(robotsTextFile);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
