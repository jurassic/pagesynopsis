package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.LinkListJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.LinkListWebService;
import com.pagesynopsis.web.proxy.LinkListWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.LinkList;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkListProxyWebService
{
    private static final Logger log = Logger.getLogger(LinkListProxyWebService.class.getName());
     
    // Af service interface.
    private LinkListWebServiceProxy mServiceProxy = null;

    public LinkListProxyWebService()
    {
        //this(new LinkListWebServiceProxy());
        this(null);
    }
    public LinkListProxyWebService(LinkListWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private LinkListWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new LinkListWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public LinkListJsBean findLinkListByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findLinkListByTargetUrl(targetUrl, fetch, null);
    }
    public LinkListJsBean findLinkListByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            LinkListJsBean jsBean = null;
            LinkList linkList = getServiceProxy().findLinkListByTargetUrl(targetUrl, fetch, refresh);
            if(linkList != null) {
                jsBean = LinkListWebService.convertLinkListToJsBean(linkList);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
