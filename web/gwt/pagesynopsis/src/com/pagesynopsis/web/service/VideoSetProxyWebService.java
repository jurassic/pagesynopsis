package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.VideoSetJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.VideoSetWebService;
import com.pagesynopsis.web.proxy.VideoSetWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.VideoSet;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class VideoSetProxyWebService
{
    private static final Logger log = Logger.getLogger(VideoSetProxyWebService.class.getName());
     
    // Af service interface.
    private VideoSetWebServiceProxy mServiceProxy = null;

    public VideoSetProxyWebService()
    {
        //this(new VideoSetWebServiceProxy());
        this(null);
    }
    public VideoSetProxyWebService(VideoSetWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private VideoSetWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new VideoSetWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public VideoSetJsBean findVideoSetByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findVideoSetByTargetUrl(targetUrl, fetch, null);
    }
    public VideoSetJsBean findVideoSetByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            VideoSetJsBean jsBean = null;
            VideoSet videoSet = getServiceProxy().findVideoSetByTargetUrl(targetUrl, fetch, refresh);
            if(videoSet != null) {
                jsBean = VideoSetWebService.convertVideoSetToJsBean(videoSet);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
