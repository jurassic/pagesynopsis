package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.PageFetchJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.PageFetchWebService;
import com.pagesynopsis.web.proxy.PageFetchWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.PageFetch;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PageFetchProxyWebService
{
    private static final Logger log = Logger.getLogger(PageFetchProxyWebService.class.getName());
     
    // Af service interface.
    private PageFetchWebServiceProxy mServiceProxy = null;

    public PageFetchProxyWebService()
    {
        //this(new PageFetchWebServiceProxy());
        this(null);
    }
    public PageFetchProxyWebService(PageFetchWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private PageFetchWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new PageFetchWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public PageFetchJsBean findPageFetchByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findPageFetchByTargetUrl(targetUrl, fetch, null);
    }
    public PageFetchJsBean findPageFetchByTargetUrl(String targetUrl, Boolean fetch, Integer redirects) throws WebException
    {
        return findPageFetchByTargetUrl(targetUrl, fetch, redirects, null);
    }
    public PageFetchJsBean findPageFetchByTargetUrl(String targetUrl, Boolean fetch, Integer redirects, Integer refresh) throws WebException
    {
        try {
            PageFetchJsBean jsBean = null;
            PageFetch pageFetch = getServiceProxy().findPageFetchByTargetUrl(targetUrl, fetch, redirects, refresh);
            if(pageFetch != null) {
                jsBean = PageFetchWebService.convertPageFetchToJsBean(pageFetch);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
