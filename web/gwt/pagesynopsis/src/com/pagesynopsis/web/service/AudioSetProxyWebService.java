package com.pagesynopsis.web.service;

import java.util.logging.Logger;

import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.AudioSetJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;
import com.pagesynopsis.rw.service.AudioSetWebService;
import com.pagesynopsis.web.proxy.AudioSetWebServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.AudioSet;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AudioSetProxyWebService
{
    private static final Logger log = Logger.getLogger(AudioSetProxyWebService.class.getName());
     
    // Af service interface.
    private AudioSetWebServiceProxy mServiceProxy = null;

    public AudioSetProxyWebService()
    {
        //this(new AudioSetWebServiceProxy());
        this(null);
    }
    public AudioSetProxyWebService(AudioSetWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private AudioSetWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new AudioSetWebServiceProxy();
        }
        return mServiceProxy;
    }
    

//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
    public AudioSetJsBean findAudioSetByTargetUrl(String targetUrl, Boolean fetch) throws WebException
    {
        return findAudioSetByTargetUrl(targetUrl, fetch, null);
    }
    public AudioSetJsBean findAudioSetByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws WebException
    {
        try {
            AudioSetJsBean jsBean = null;
            AudioSet audioSet = getServiceProxy().findAudioSetByTargetUrl(targetUrl, fetch, refresh);
            if(audioSet != null) {
                jsBean = AudioSetWebService.convertAudioSetToJsBean(audioSet);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
