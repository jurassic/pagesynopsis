package com.pagesynopsis.web.proxy;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.proxy.AbstractBaseServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class RobotsTextFileWebServiceProxy extends AbstractBaseServiceProxy
{
    private static final Logger log = Logger.getLogger(RobotsTextFileWebServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding app.resource class.
    private final static String RESOURCE_PAGEINFO = "robotstextfile";


    // Cache service
    private Cache mCache = null;

    public RobotsTextFileWebServiceProxy()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            props.put(GCacheFactory.EXPIRATION_DELTA, 1800);     // TBD: Get this from Config...
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    protected WebResource getRobotsTextFileWebResource()
    {
        return getWebResource(RESOURCE_PAGEINFO);
    }
    protected WebResource getRobotsTextFileWebResource(String path)
    {
        return getWebResource(RESOURCE_PAGEINFO, path);
    }
    protected WebResource getRobotsTextFileWebResourceByGuid(String guid)
    {
        return getRobotsTextFileWebResource(guid);
    }


//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
//  public AudioSet getRobotsTextFileBySiteUrl(String siteUrl, Boolean fetch) throws BaseException
//  {
//      return findRobotsTextFileBySiteUrl(siteUrl, fetch);
//  }    
//  @Deprecated
    public RobotsTextFile findRobotsTextFileBySiteUrl(String siteUrl) throws BaseException
    {
    	RobotsTextFile robotsTextFile = null;

    	// ???
        String key = getResourcePath(RESOURCE_PAGEINFO, siteUrl);
        if(mCache != null) {
//            CacheEntry entry = mCache.getCacheEntry(key);
//            if(entry != null) {
//                // TBD: eTag, lastModified, expires, etc....
//                // ???
//                Object obj = entry.getValue();
//                if(obj instanceof RobotsTextFileBean) {
//                    log.info("robotsTextFile returned from cache! key = " + key);
//                    return (RobotsTextFileBean) obj;
//                }
//            }
        }

    	WebResource webResource = getRobotsTextFileWebResource();
    	if(siteUrl == null || siteUrl.isEmpty()) {
    	    // Should we allow empty siteUrl????
    	} else {
    	    webResource = webResource.queryParam("siteUrl", siteUrl);
    	}

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                RobotsTextFileStub stub = clientResponse.getEntity(RobotsTextFileStub.class);
                robotsTextFile = MarshalHelper.convertRobotsTextFileToBean(stub);
                log.log(Level.FINE, "RobotsTextFile = " + robotsTextFile);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        // ???
        if(mCache != null) {
            if(robotsTextFile != null) {
                log.info("robotsTextFile saved to cache! key = " + key);
                mCache.put(key, robotsTextFile);
            }
        }

        return robotsTextFile;
    }


}
