package com.pagesynopsis.web.proxy;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.rf.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.rf.proxy.AbstractBaseServiceProxy;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.AudioSetStub;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class AudioSetWebServiceProxy extends AbstractBaseServiceProxy
{
    private static final Logger log = Logger.getLogger(AudioSetWebServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding app.resource class.
    private final static String RESOURCE_AUDIOSET = "audioset";


    // Cache service
    private Cache mCache = null;

    public AudioSetWebServiceProxy()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            props.put(GCacheFactory.EXPIRATION_DELTA, 1800);     // TBD: Get this from Config...
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    protected WebResource getAudioSetWebResource()
    {
        return getWebResource(RESOURCE_AUDIOSET);
    }
    protected WebResource getAudioSetWebResource(String path)
    {
        return getWebResource(RESOURCE_AUDIOSET, path);
    }
    protected WebResource getAudioSetWebResourceByGuid(String guid)
    {
        return getAudioSetWebResource(guid);
    }


//  "Get" is better than "Find" for this: Get returns a full object, whereas Find returns only an object with "default fetch". 
//    public AudioSet getAudioSetByTargetUrl(String targetUrl, Boolean fetch) throws BaseException
//    {
//        return findAudioSetByTargetUrl(targetUrl, fetch);
//    }    
//    @Deprecated
    public AudioSet findAudioSetByTargetUrl(String targetUrl, Boolean fetch) throws BaseException
    {
        return findAudioSetByTargetUrl(targetUrl, fetch, null);
    }
    public AudioSet findAudioSetByTargetUrl(String targetUrl, Boolean fetch, Integer refresh) throws BaseException
    {
    	AudioSet audioSet = null;

    	// ???
        String key = getResourcePath(RESOURCE_AUDIOSET, targetUrl);
        if(mCache != null) {
//            CacheEntry entry = mCache.getCacheEntry(key);
//            if(entry != null) {
//                // TBD: eTag, lastModified, expires, etc....
//                // ???
//                Object obj = entry.getValue();
//                if(obj instanceof AudioSetBean) {
//                    log.info("audioSet returned from cache! key = " + key);
//                    return (AudioSetBean) obj;
//                }
//            }
        }

    	WebResource webResource = getAudioSetWebResource();
    	if(targetUrl == null || targetUrl.isEmpty()) {
    	    // Should we allow empty targetUrl????
    	} else {
    	    webResource = webResource.queryParam("targetUrl", targetUrl);
    	}
        if(fetch != null) {
            webResource = webResource.queryParam("fetch", fetch.toString());
        }
        if(refresh != null) {
            webResource = webResource.queryParam("refresh", refresh.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                AudioSetStub stub = clientResponse.getEntity(AudioSetStub.class);
                audioSet = MarshalHelper.convertAudioSetToBean(stub);
                log.log(Level.FINE, "AudioSet = " + audioSet);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = clientResponse.getEntity(ErrorStub.class);
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        // ???
        if(mCache != null) {
            if(audioSet != null) {
                log.info("audioSet saved to cache! key = " + key);
                mCache.put(key, audioSet);
            }
        }

        return audioSet;
    }


}
