package com.pagesynopsis.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ImageSetJsBean;
import com.pagesynopsis.rw.service.ImageSetWebService;
import com.pagesynopsis.rw.service.UserWebService;
import com.pagesynopsis.web.service.ImageSetProxyWebService;
import com.pagesynopsis.ws.core.StatusCode;


public class ImageSetServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageSetServlet.class.getName());
    
    // temporary
    private static final String QUERY_PARAM_TARGET_URL = "targetUrl";
    private static final String QUERY_PARAM_FETCH = "fetch";
    
    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private ImageSetWebService imageSetWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ImageSetWebService getImageSetService()
    {
        if(imageSetWebService == null) {
            imageSetWebService = new ImageSetWebService();
        }
        return imageSetWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();        
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
   
        // TBD:
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String targetUrl = null;
        String[] targetUrls = req.getParameterValues(QUERY_PARAM_TARGET_URL);
        if(targetUrls != null && targetUrls.length > 0) {
            // TBD: Support multiple target urls???
            targetUrl = URLDecoder.decode(targetUrls[0], "UTF-8");  // ?????
        } else {
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        log.fine("targetUrl = " + targetUrl);
        
        boolean fetch = true;   // default value.
        String[] fetchStrs = req.getParameterValues(QUERY_PARAM_FETCH);
        if(fetchStrs != null && fetchStrs.length > 0) {
            if(fetchStrs[0].equals("0") || fetchStrs[0].equalsIgnoreCase("false")) {  // temporary
                fetch = false;
            }
            // else ignore.
        }
        log.fine("fetch = " + fetch);
        
        ImageSetProxyWebService proxyWebService = new ImageSetProxyWebService();
        ImageSetJsBean imageSet = null;
        try {
            imageSet = proxyWebService.findImageSetByTargetUrl(targetUrl, fetch);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve imageSet", e);
        }
       
        if(imageSet != null) {
            String jsonStr = imageSet.toJsonString();
            resp.setContentType("application/json");  // ????
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
}
