package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgImageStructWebService // implements OgImageStructService
{
    private static final Logger log = Logger.getLogger(OgImageStructWebService.class.getName());
     
    public static OgImageStructJsBean convertOgImageStructToJsBean(OgImageStruct ogImageStruct)
    {
        OgImageStructJsBean jsBean = null;
        if(ogImageStruct != null) {
            jsBean = new OgImageStructJsBean();
            jsBean.setUuid(ogImageStruct.getUuid());
            jsBean.setUrl(ogImageStruct.getUrl());
            jsBean.setSecureUrl(ogImageStruct.getSecureUrl());
            jsBean.setType(ogImageStruct.getType());
            jsBean.setWidth(ogImageStruct.getWidth());
            jsBean.setHeight(ogImageStruct.getHeight());
        }
        return jsBean;
    }

    public static OgImageStruct convertOgImageStructJsBeanToBean(OgImageStructJsBean jsBean)
    {
        OgImageStructBean ogImageStruct = null;
        if(jsBean != null) {
            ogImageStruct = new OgImageStructBean();
            ogImageStruct.setUuid(jsBean.getUuid());
            ogImageStruct.setUrl(jsBean.getUrl());
            ogImageStruct.setSecureUrl(jsBean.getSecureUrl());
            ogImageStruct.setType(jsBean.getType());
            ogImageStruct.setWidth(jsBean.getWidth());
            ogImageStruct.setHeight(jsBean.getHeight());
        }
        return ogImageStruct;
    }

}
