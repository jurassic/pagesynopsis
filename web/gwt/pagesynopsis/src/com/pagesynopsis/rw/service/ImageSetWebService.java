package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.ImageSetJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ImageSetWebService // implements ImageSetService
{
    private static final Logger log = Logger.getLogger(ImageSetWebService.class.getName());
     
    // Af service interface.
    private ImageSetService mService = null;

    public ImageSetWebService()
    {
        this(ServiceProxyFactory.getInstance().getImageSetServiceProxy());
    }
    public ImageSetWebService(ImageSetService service)
    {
        mService = service;
    }
    
    private ImageSetService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getImageSetServiceProxy();
        }
        return mService;
    }
    
    
    public ImageSetJsBean getImageSet(String guid) throws WebException
    {
        try {
            ImageSet imageSet = getService().getImageSet(guid);
            ImageSetJsBean bean = convertImageSetToJsBean(imageSet);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getImageSet(String guid, String field) throws WebException
    {
        try {
            return getService().getImageSet(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ImageSetJsBean> getImageSets(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ImageSetJsBean> jsBeans = new ArrayList<ImageSetJsBean>();
            List<ImageSet> imageSets = getService().getImageSets(guids);
            if(imageSets != null) {
                for(ImageSet imageSet : imageSets) {
                    jsBeans.add(convertImageSetToJsBean(imageSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ImageSetJsBean> getAllImageSets() throws WebException
    {
        return getAllImageSets(null, null, null);
    }

    // @Deprecated
    public List<ImageSetJsBean> getAllImageSets(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllImageSets(ordering, offset, count, null);
    }

    public List<ImageSetJsBean> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ImageSetJsBean> jsBeans = new ArrayList<ImageSetJsBean>();
            List<ImageSet> imageSets = getService().getAllImageSets(ordering, offset, count, forwardCursor);
            if(imageSets != null) {
                for(ImageSet imageSet : imageSets) {
                    jsBeans.add(convertImageSetToJsBean(imageSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllImageSetKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<ImageSetJsBean> findImageSets(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findImageSets(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<ImageSetJsBean> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<ImageSetJsBean> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<ImageSetJsBean> jsBeans = new ArrayList<ImageSetJsBean>();
            List<ImageSet> imageSets = getService().findImageSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(imageSets != null) {
                for(ImageSet imageSet : imageSets) {
                    jsBeans.add(convertImageSetToJsBean(imageSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws WebException
    {
        try {
            return getService().createImageSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createImageSet(ImageSetJsBean jsBean) throws WebException
    {
        try {
            ImageSet imageSet = convertImageSetJsBeanToBean(jsBean);
            return getService().createImageSet(imageSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ImageSetJsBean constructImageSet(ImageSetJsBean jsBean) throws WebException
    {
        try {
            ImageSet imageSet = convertImageSetJsBeanToBean(jsBean);
            imageSet = getService().constructImageSet(imageSet);
            jsBean = convertImageSetToJsBean(imageSet);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws WebException
    {
        try {
            return getService().updateImageSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateImageSet(ImageSetJsBean jsBean) throws WebException
    {
        try {
            ImageSet imageSet = convertImageSetJsBeanToBean(jsBean);
            return getService().updateImageSet(imageSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ImageSetJsBean refreshImageSet(ImageSetJsBean jsBean) throws WebException
    {
        try {
            ImageSet imageSet = convertImageSetJsBeanToBean(jsBean);
            imageSet = getService().refreshImageSet(imageSet);
            jsBean = convertImageSetToJsBean(imageSet);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteImageSet(String guid) throws WebException
    {
        try {
            return getService().deleteImageSet(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteImageSet(ImageSetJsBean jsBean) throws WebException
    {
        try {
            ImageSet imageSet = convertImageSetJsBeanToBean(jsBean);
            return getService().deleteImageSet(imageSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteImageSets(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteImageSets(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ImageSetJsBean convertImageSetToJsBean(ImageSet imageSet)
    {
        ImageSetJsBean jsBean = null;
        if(imageSet != null) {
            jsBean = new ImageSetJsBean();
            jsBean.setGuid(imageSet.getGuid());
            jsBean.setUser(imageSet.getUser());
            jsBean.setFetchRequest(imageSet.getFetchRequest());
            jsBean.setTargetUrl(imageSet.getTargetUrl());
            jsBean.setPageUrl(imageSet.getPageUrl());
            jsBean.setQueryString(imageSet.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = imageSet.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(imageSet.getLastFetchResult());
            jsBean.setResponseCode(imageSet.getResponseCode());
            jsBean.setContentType(imageSet.getContentType());
            jsBean.setContentLength(imageSet.getContentLength());
            jsBean.setLanguage(imageSet.getLanguage());
            jsBean.setRedirect(imageSet.getRedirect());
            jsBean.setLocation(imageSet.getLocation());
            jsBean.setPageTitle(imageSet.getPageTitle());
            jsBean.setNote(imageSet.getNote());
            jsBean.setDeferred(imageSet.isDeferred());
            jsBean.setStatus(imageSet.getStatus());
            jsBean.setRefreshStatus(imageSet.getRefreshStatus());
            jsBean.setRefreshInterval(imageSet.getRefreshInterval());
            jsBean.setNextRefreshTime(imageSet.getNextRefreshTime());
            jsBean.setLastCheckedTime(imageSet.getLastCheckedTime());
            jsBean.setLastUpdatedTime(imageSet.getLastUpdatedTime());
            jsBean.setMediaTypeFilter(imageSet.getMediaTypeFilter());
            Set<ImageStructJsBean> pageImagesJsBeans = new HashSet<ImageStructJsBean>();
            Set<ImageStruct> pageImagesBeans = imageSet.getPageImages();
            if(pageImagesBeans != null) {
                for(ImageStruct imageStruct : pageImagesBeans) {
                    ImageStructJsBean jB = ImageStructWebService.convertImageStructToJsBean(imageStruct);
                    pageImagesJsBeans.add(jB); 
                }
            }
            jsBean.setPageImages(pageImagesJsBeans);
            jsBean.setCreatedTime(imageSet.getCreatedTime());
            jsBean.setModifiedTime(imageSet.getModifiedTime());
        }
        return jsBean;
    }

    public static ImageSet convertImageSetJsBeanToBean(ImageSetJsBean jsBean)
    {
        ImageSetBean imageSet = null;
        if(jsBean != null) {
            imageSet = new ImageSetBean();
            imageSet.setGuid(jsBean.getGuid());
            imageSet.setUser(jsBean.getUser());
            imageSet.setFetchRequest(jsBean.getFetchRequest());
            imageSet.setTargetUrl(jsBean.getTargetUrl());
            imageSet.setPageUrl(jsBean.getPageUrl());
            imageSet.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            imageSet.setQueryParams(queryParamsBeans);
            imageSet.setLastFetchResult(jsBean.getLastFetchResult());
            imageSet.setResponseCode(jsBean.getResponseCode());
            imageSet.setContentType(jsBean.getContentType());
            imageSet.setContentLength(jsBean.getContentLength());
            imageSet.setLanguage(jsBean.getLanguage());
            imageSet.setRedirect(jsBean.getRedirect());
            imageSet.setLocation(jsBean.getLocation());
            imageSet.setPageTitle(jsBean.getPageTitle());
            imageSet.setNote(jsBean.getNote());
            imageSet.setDeferred(jsBean.isDeferred());
            imageSet.setStatus(jsBean.getStatus());
            imageSet.setRefreshStatus(jsBean.getRefreshStatus());
            imageSet.setRefreshInterval(jsBean.getRefreshInterval());
            imageSet.setNextRefreshTime(jsBean.getNextRefreshTime());
            imageSet.setLastCheckedTime(jsBean.getLastCheckedTime());
            imageSet.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            imageSet.setMediaTypeFilter(jsBean.getMediaTypeFilter());
            Set<ImageStruct> pageImagesBeans = new HashSet<ImageStruct>();
            Set<ImageStructJsBean> pageImagesJsBeans = jsBean.getPageImages();
            if(pageImagesJsBeans != null) {
                for(ImageStructJsBean imageStruct : pageImagesJsBeans) {
                    ImageStruct b = ImageStructWebService.convertImageStructJsBeanToBean(imageStruct);
                    pageImagesBeans.add(b); 
                }
            }
            imageSet.setPageImages(pageImagesBeans);
            imageSet.setCreatedTime(jsBean.getCreatedTime());
            imageSet.setModifiedTime(jsBean.getModifiedTime());
        }
        return imageSet;
    }

}
