package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextGroupWebService // implements RobotsTextGroupService
{
    private static final Logger log = Logger.getLogger(RobotsTextGroupWebService.class.getName());
     
    public static RobotsTextGroupJsBean convertRobotsTextGroupToJsBean(RobotsTextGroup robotsTextGroup)
    {
        RobotsTextGroupJsBean jsBean = null;
        if(robotsTextGroup != null) {
            jsBean = new RobotsTextGroupJsBean();
            jsBean.setUuid(robotsTextGroup.getUuid());
            jsBean.setUserAgent(robotsTextGroup.getUserAgent());
            jsBean.setAllows(robotsTextGroup.getAllows());
            jsBean.setDisallows(robotsTextGroup.getDisallows());
        }
        return jsBean;
    }

    public static RobotsTextGroup convertRobotsTextGroupJsBeanToBean(RobotsTextGroupJsBean jsBean)
    {
        RobotsTextGroupBean robotsTextGroup = null;
        if(jsBean != null) {
            robotsTextGroup = new RobotsTextGroupBean();
            robotsTextGroup.setUuid(jsBean.getUuid());
            robotsTextGroup.setUserAgent(jsBean.getUserAgent());
            robotsTextGroup.setAllows(jsBean.getAllows());
            robotsTextGroup.setDisallows(jsBean.getDisallows());
        }
        return robotsTextGroup;
    }

}
