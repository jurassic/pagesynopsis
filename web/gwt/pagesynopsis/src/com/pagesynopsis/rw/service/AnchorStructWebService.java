package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AnchorStructWebService // implements AnchorStructService
{
    private static final Logger log = Logger.getLogger(AnchorStructWebService.class.getName());
     
    public static AnchorStructJsBean convertAnchorStructToJsBean(AnchorStruct anchorStruct)
    {
        AnchorStructJsBean jsBean = null;
        if(anchorStruct != null) {
            jsBean = new AnchorStructJsBean();
            jsBean.setUuid(anchorStruct.getUuid());
            jsBean.setId(anchorStruct.getId());
            jsBean.setName(anchorStruct.getName());
            jsBean.setHref(anchorStruct.getHref());
            jsBean.setHrefUrl(anchorStruct.getHrefUrl());
            jsBean.setUrlScheme(anchorStruct.getUrlScheme());
            jsBean.setRel(anchorStruct.getRel());
            jsBean.setTarget(anchorStruct.getTarget());
            jsBean.setInnerHtml(anchorStruct.getInnerHtml());
            jsBean.setAnchorText(anchorStruct.getAnchorText());
            jsBean.setAnchorTitle(anchorStruct.getAnchorTitle());
            jsBean.setAnchorImage(anchorStruct.getAnchorImage());
            jsBean.setAnchorLegend(anchorStruct.getAnchorLegend());
            jsBean.setNote(anchorStruct.getNote());
        }
        return jsBean;
    }

    public static AnchorStruct convertAnchorStructJsBeanToBean(AnchorStructJsBean jsBean)
    {
        AnchorStructBean anchorStruct = null;
        if(jsBean != null) {
            anchorStruct = new AnchorStructBean();
            anchorStruct.setUuid(jsBean.getUuid());
            anchorStruct.setId(jsBean.getId());
            anchorStruct.setName(jsBean.getName());
            anchorStruct.setHref(jsBean.getHref());
            anchorStruct.setHrefUrl(jsBean.getHrefUrl());
            anchorStruct.setUrlScheme(jsBean.getUrlScheme());
            anchorStruct.setRel(jsBean.getRel());
            anchorStruct.setTarget(jsBean.getTarget());
            anchorStruct.setInnerHtml(jsBean.getInnerHtml());
            anchorStruct.setAnchorText(jsBean.getAnchorText());
            anchorStruct.setAnchorTitle(jsBean.getAnchorTitle());
            anchorStruct.setAnchorImage(jsBean.getAnchorImage());
            anchorStruct.setAnchorLegend(jsBean.getAnchorLegend());
            anchorStruct.setNote(jsBean.getNote());
        }
        return anchorStruct;
    }

}
