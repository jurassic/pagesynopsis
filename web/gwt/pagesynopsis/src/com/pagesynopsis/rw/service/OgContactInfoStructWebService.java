package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgContactInfoStruct;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgContactInfoStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgContactInfoStructWebService // implements OgContactInfoStructService
{
    private static final Logger log = Logger.getLogger(OgContactInfoStructWebService.class.getName());
     
    public static OgContactInfoStructJsBean convertOgContactInfoStructToJsBean(OgContactInfoStruct ogContactInfoStruct)
    {
        OgContactInfoStructJsBean jsBean = null;
        if(ogContactInfoStruct != null) {
            jsBean = new OgContactInfoStructJsBean();
            jsBean.setUuid(ogContactInfoStruct.getUuid());
            jsBean.setStreetAddress(ogContactInfoStruct.getStreetAddress());
            jsBean.setLocality(ogContactInfoStruct.getLocality());
            jsBean.setRegion(ogContactInfoStruct.getRegion());
            jsBean.setPostalCode(ogContactInfoStruct.getPostalCode());
            jsBean.setCountryName(ogContactInfoStruct.getCountryName());
            jsBean.setEmailAddress(ogContactInfoStruct.getEmailAddress());
            jsBean.setPhoneNumber(ogContactInfoStruct.getPhoneNumber());
            jsBean.setFaxNumber(ogContactInfoStruct.getFaxNumber());
            jsBean.setWebsite(ogContactInfoStruct.getWebsite());
        }
        return jsBean;
    }

    public static OgContactInfoStruct convertOgContactInfoStructJsBeanToBean(OgContactInfoStructJsBean jsBean)
    {
        OgContactInfoStructBean ogContactInfoStruct = null;
        if(jsBean != null) {
            ogContactInfoStruct = new OgContactInfoStructBean();
            ogContactInfoStruct.setUuid(jsBean.getUuid());
            ogContactInfoStruct.setStreetAddress(jsBean.getStreetAddress());
            ogContactInfoStruct.setLocality(jsBean.getLocality());
            ogContactInfoStruct.setRegion(jsBean.getRegion());
            ogContactInfoStruct.setPostalCode(jsBean.getPostalCode());
            ogContactInfoStruct.setCountryName(jsBean.getCountryName());
            ogContactInfoStruct.setEmailAddress(jsBean.getEmailAddress());
            ogContactInfoStruct.setPhoneNumber(jsBean.getPhoneNumber());
            ogContactInfoStruct.setFaxNumber(jsBean.getFaxNumber());
            ogContactInfoStruct.setWebsite(jsBean.getWebsite());
        }
        return ogContactInfoStruct;
    }

}
