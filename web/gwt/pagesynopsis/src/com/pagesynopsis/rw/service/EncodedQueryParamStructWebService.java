package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.EncodedQueryParamStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class EncodedQueryParamStructWebService // implements EncodedQueryParamStructService
{
    private static final Logger log = Logger.getLogger(EncodedQueryParamStructWebService.class.getName());
     
    public static EncodedQueryParamStructJsBean convertEncodedQueryParamStructToJsBean(EncodedQueryParamStruct encodedQueryParamStruct)
    {
        EncodedQueryParamStructJsBean jsBean = null;
        if(encodedQueryParamStruct != null) {
            jsBean = new EncodedQueryParamStructJsBean();
            jsBean.setParamType(encodedQueryParamStruct.getParamType());
            jsBean.setOriginalString(encodedQueryParamStruct.getOriginalString());
            jsBean.setEncodedString(encodedQueryParamStruct.getEncodedString());
            jsBean.setNote(encodedQueryParamStruct.getNote());
        }
        return jsBean;
    }

    public static EncodedQueryParamStruct convertEncodedQueryParamStructJsBeanToBean(EncodedQueryParamStructJsBean jsBean)
    {
        EncodedQueryParamStructBean encodedQueryParamStruct = null;
        if(jsBean != null) {
            encodedQueryParamStruct = new EncodedQueryParamStructBean();
            encodedQueryParamStruct.setParamType(jsBean.getParamType());
            encodedQueryParamStruct.setOriginalString(jsBean.getOriginalString());
            encodedQueryParamStruct.setEncodedString(jsBean.getEncodedString());
            encodedQueryParamStruct.setNote(jsBean.getNote());
        }
        return encodedQueryParamStruct;
    }

}
