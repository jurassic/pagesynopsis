package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.RobotsTextGroupJsBean;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextFileWebService // implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(RobotsTextFileWebService.class.getName());
     
    // Af service interface.
    private RobotsTextFileService mService = null;

    public RobotsTextFileWebService()
    {
        this(ServiceProxyFactory.getInstance().getRobotsTextFileServiceProxy());
    }
    public RobotsTextFileWebService(RobotsTextFileService service)
    {
        mService = service;
    }
    
    private RobotsTextFileService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getRobotsTextFileServiceProxy();
        }
        return mService;
    }
    
    
    public RobotsTextFileJsBean getRobotsTextFile(String guid) throws WebException
    {
        try {
            RobotsTextFile robotsTextFile = getService().getRobotsTextFile(guid);
            RobotsTextFileJsBean bean = convertRobotsTextFileToJsBean(robotsTextFile);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getRobotsTextFile(String guid, String field) throws WebException
    {
        try {
            return getService().getRobotsTextFile(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RobotsTextFileJsBean> getRobotsTextFiles(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<RobotsTextFileJsBean> jsBeans = new ArrayList<RobotsTextFileJsBean>();
            List<RobotsTextFile> robotsTextFiles = getService().getRobotsTextFiles(guids);
            if(robotsTextFiles != null) {
                for(RobotsTextFile robotsTextFile : robotsTextFiles) {
                    jsBeans.add(convertRobotsTextFileToJsBean(robotsTextFile));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RobotsTextFileJsBean> getAllRobotsTextFiles() throws WebException
    {
        return getAllRobotsTextFiles(null, null, null);
    }

    // @Deprecated
    public List<RobotsTextFileJsBean> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    public List<RobotsTextFileJsBean> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<RobotsTextFileJsBean> jsBeans = new ArrayList<RobotsTextFileJsBean>();
            List<RobotsTextFile> robotsTextFiles = getService().getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
            if(robotsTextFiles != null) {
                for(RobotsTextFile robotsTextFile : robotsTextFiles) {
                    jsBeans.add(convertRobotsTextFileToJsBean(robotsTextFile));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<RobotsTextFileJsBean> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<RobotsTextFileJsBean> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<RobotsTextFileJsBean> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<RobotsTextFileJsBean> jsBeans = new ArrayList<RobotsTextFileJsBean>();
            List<RobotsTextFile> robotsTextFiles = getService().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(robotsTextFiles != null) {
                for(RobotsTextFile robotsTextFile : robotsTextFiles) {
                    jsBeans.add(convertRobotsTextFileToJsBean(robotsTextFile));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().createRobotsTextFile(siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createRobotsTextFile(RobotsTextFileJsBean jsBean) throws WebException
    {
        try {
            RobotsTextFile robotsTextFile = convertRobotsTextFileJsBeanToBean(jsBean);
            return getService().createRobotsTextFile(robotsTextFile);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public RobotsTextFileJsBean constructRobotsTextFile(RobotsTextFileJsBean jsBean) throws WebException
    {
        try {
            RobotsTextFile robotsTextFile = convertRobotsTextFileJsBeanToBean(jsBean);
            robotsTextFile = getService().constructRobotsTextFile(robotsTextFile);
            jsBean = convertRobotsTextFileToJsBean(robotsTextFile);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws WebException
    {
        try {
            return getService().updateRobotsTextFile(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateRobotsTextFile(RobotsTextFileJsBean jsBean) throws WebException
    {
        try {
            RobotsTextFile robotsTextFile = convertRobotsTextFileJsBeanToBean(jsBean);
            return getService().updateRobotsTextFile(robotsTextFile);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public RobotsTextFileJsBean refreshRobotsTextFile(RobotsTextFileJsBean jsBean) throws WebException
    {
        try {
            RobotsTextFile robotsTextFile = convertRobotsTextFileJsBeanToBean(jsBean);
            robotsTextFile = getService().refreshRobotsTextFile(robotsTextFile);
            jsBean = convertRobotsTextFileToJsBean(robotsTextFile);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteRobotsTextFile(String guid) throws WebException
    {
        try {
            return getService().deleteRobotsTextFile(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteRobotsTextFile(RobotsTextFileJsBean jsBean) throws WebException
    {
        try {
            RobotsTextFile robotsTextFile = convertRobotsTextFileJsBeanToBean(jsBean);
            return getService().deleteRobotsTextFile(robotsTextFile);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteRobotsTextFiles(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static RobotsTextFileJsBean convertRobotsTextFileToJsBean(RobotsTextFile robotsTextFile)
    {
        RobotsTextFileJsBean jsBean = null;
        if(robotsTextFile != null) {
            jsBean = new RobotsTextFileJsBean();
            jsBean.setGuid(robotsTextFile.getGuid());
            jsBean.setSiteUrl(robotsTextFile.getSiteUrl());
            List<RobotsTextGroupJsBean> groupsJsBeans = new ArrayList<RobotsTextGroupJsBean>();
            List<RobotsTextGroup> groupsBeans = robotsTextFile.getGroups();
            if(groupsBeans != null) {
                for(RobotsTextGroup robotsTextGroup : groupsBeans) {
                    RobotsTextGroupJsBean jB = RobotsTextGroupWebService.convertRobotsTextGroupToJsBean(robotsTextGroup);
                    groupsJsBeans.add(jB); 
                }
            }
            jsBean.setGroups(groupsJsBeans);
            jsBean.setSitemaps(robotsTextFile.getSitemaps());
            jsBean.setContent(robotsTextFile.getContent());
            jsBean.setContentHash(robotsTextFile.getContentHash());
            jsBean.setLastCheckedTime(robotsTextFile.getLastCheckedTime());
            jsBean.setLastUpdatedTime(robotsTextFile.getLastUpdatedTime());
            jsBean.setCreatedTime(robotsTextFile.getCreatedTime());
            jsBean.setModifiedTime(robotsTextFile.getModifiedTime());
        }
        return jsBean;
    }

    public static RobotsTextFile convertRobotsTextFileJsBeanToBean(RobotsTextFileJsBean jsBean)
    {
        RobotsTextFileBean robotsTextFile = null;
        if(jsBean != null) {
            robotsTextFile = new RobotsTextFileBean();
            robotsTextFile.setGuid(jsBean.getGuid());
            robotsTextFile.setSiteUrl(jsBean.getSiteUrl());
            List<RobotsTextGroup> groupsBeans = new ArrayList<RobotsTextGroup>();
            List<RobotsTextGroupJsBean> groupsJsBeans = jsBean.getGroups();
            if(groupsJsBeans != null) {
                for(RobotsTextGroupJsBean robotsTextGroup : groupsJsBeans) {
                    RobotsTextGroup b = RobotsTextGroupWebService.convertRobotsTextGroupJsBeanToBean(robotsTextGroup);
                    groupsBeans.add(b); 
                }
            }
            robotsTextFile.setGroups(groupsBeans);
            robotsTextFile.setSitemaps(jsBean.getSitemaps());
            robotsTextFile.setContent(jsBean.getContent());
            robotsTextFile.setContentHash(jsBean.getContentHash());
            robotsTextFile.setLastCheckedTime(jsBean.getLastCheckedTime());
            robotsTextFile.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            robotsTextFile.setCreatedTime(jsBean.getCreatedTime());
            robotsTextFile.setModifiedTime(jsBean.getModifiedTime());
        }
        return robotsTextFile;
    }

}
