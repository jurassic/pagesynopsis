package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UserWebsiteStruct;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.UserWebsiteStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserWebsiteStructWebService // implements UserWebsiteStructService
{
    private static final Logger log = Logger.getLogger(UserWebsiteStructWebService.class.getName());
     
    public static UserWebsiteStructJsBean convertUserWebsiteStructToJsBean(UserWebsiteStruct userWebsiteStruct)
    {
        UserWebsiteStructJsBean jsBean = null;
        if(userWebsiteStruct != null) {
            jsBean = new UserWebsiteStructJsBean();
            jsBean.setUuid(userWebsiteStruct.getUuid());
            jsBean.setPrimarySite(userWebsiteStruct.getPrimarySite());
            jsBean.setHomePage(userWebsiteStruct.getHomePage());
            jsBean.setBlogSite(userWebsiteStruct.getBlogSite());
            jsBean.setPortfolioSite(userWebsiteStruct.getPortfolioSite());
            jsBean.setTwitterProfile(userWebsiteStruct.getTwitterProfile());
            jsBean.setFacebookProfile(userWebsiteStruct.getFacebookProfile());
            jsBean.setGooglePlusProfile(userWebsiteStruct.getGooglePlusProfile());
            jsBean.setNote(userWebsiteStruct.getNote());
        }
        return jsBean;
    }

    public static UserWebsiteStruct convertUserWebsiteStructJsBeanToBean(UserWebsiteStructJsBean jsBean)
    {
        UserWebsiteStructBean userWebsiteStruct = null;
        if(jsBean != null) {
            userWebsiteStruct = new UserWebsiteStructBean();
            userWebsiteStruct.setUuid(jsBean.getUuid());
            userWebsiteStruct.setPrimarySite(jsBean.getPrimarySite());
            userWebsiteStruct.setHomePage(jsBean.getHomePage());
            userWebsiteStruct.setBlogSite(jsBean.getBlogSite());
            userWebsiteStruct.setPortfolioSite(jsBean.getPortfolioSite());
            userWebsiteStruct.setTwitterProfile(jsBean.getTwitterProfile());
            userWebsiteStruct.setFacebookProfile(jsBean.getFacebookProfile());
            userWebsiteStruct.setGooglePlusProfile(jsBean.getGooglePlusProfile());
            userWebsiteStruct.setNote(jsBean.getNote());
        }
        return userWebsiteStruct;
    }

}
