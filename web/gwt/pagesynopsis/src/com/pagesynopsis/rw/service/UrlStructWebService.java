package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UrlStructWebService // implements UrlStructService
{
    private static final Logger log = Logger.getLogger(UrlStructWebService.class.getName());
     
    public static UrlStructJsBean convertUrlStructToJsBean(UrlStruct urlStruct)
    {
        UrlStructJsBean jsBean = null;
        if(urlStruct != null) {
            jsBean = new UrlStructJsBean();
            jsBean.setUuid(urlStruct.getUuid());
            jsBean.setStatusCode(urlStruct.getStatusCode());
            jsBean.setRedirectUrl(urlStruct.getRedirectUrl());
            jsBean.setAbsoluteUrl(urlStruct.getAbsoluteUrl());
            jsBean.setHashFragment(urlStruct.getHashFragment());
            jsBean.setNote(urlStruct.getNote());
        }
        return jsBean;
    }

    public static UrlStruct convertUrlStructJsBeanToBean(UrlStructJsBean jsBean)
    {
        UrlStructBean urlStruct = null;
        if(jsBean != null) {
            urlStruct = new UrlStructBean();
            urlStruct.setUuid(jsBean.getUuid());
            urlStruct.setStatusCode(jsBean.getStatusCode());
            urlStruct.setRedirectUrl(jsBean.getRedirectUrl());
            urlStruct.setAbsoluteUrl(jsBean.getAbsoluteUrl());
            urlStruct.setHashFragment(jsBean.getHashFragment());
            urlStruct.setNote(jsBean.getNote());
        }
        return urlStruct;
    }

}
