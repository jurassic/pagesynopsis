package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgAudioStructWebService // implements OgAudioStructService
{
    private static final Logger log = Logger.getLogger(OgAudioStructWebService.class.getName());
     
    public static OgAudioStructJsBean convertOgAudioStructToJsBean(OgAudioStruct ogAudioStruct)
    {
        OgAudioStructJsBean jsBean = null;
        if(ogAudioStruct != null) {
            jsBean = new OgAudioStructJsBean();
            jsBean.setUuid(ogAudioStruct.getUuid());
            jsBean.setUrl(ogAudioStruct.getUrl());
            jsBean.setSecureUrl(ogAudioStruct.getSecureUrl());
            jsBean.setType(ogAudioStruct.getType());
        }
        return jsBean;
    }

    public static OgAudioStruct convertOgAudioStructJsBeanToBean(OgAudioStructJsBean jsBean)
    {
        OgAudioStructBean ogAudioStruct = null;
        if(jsBean != null) {
            ogAudioStruct = new OgAudioStructBean();
            ogAudioStruct.setUuid(jsBean.getUuid());
            ogAudioStruct.setUrl(jsBean.getUrl());
            ogAudioStruct.setSecureUrl(jsBean.getSecureUrl());
            ogAudioStruct.setType(jsBean.getType());
        }
        return ogAudioStruct;
    }

}
