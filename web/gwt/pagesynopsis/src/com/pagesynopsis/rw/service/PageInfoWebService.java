package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.PageInfoJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PageInfoWebService // implements PageInfoService
{
    private static final Logger log = Logger.getLogger(PageInfoWebService.class.getName());
     
    // Af service interface.
    private PageInfoService mService = null;

    public PageInfoWebService()
    {
        this(ServiceProxyFactory.getInstance().getPageInfoServiceProxy());
    }
    public PageInfoWebService(PageInfoService service)
    {
        mService = service;
    }
    
    private PageInfoService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getPageInfoServiceProxy();
        }
        return mService;
    }
    
    
    public PageInfoJsBean getPageInfo(String guid) throws WebException
    {
        try {
            PageInfo pageInfo = getService().getPageInfo(guid);
            PageInfoJsBean bean = convertPageInfoToJsBean(pageInfo);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getPageInfo(String guid, String field) throws WebException
    {
        try {
            return getService().getPageInfo(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<PageInfoJsBean> getPageInfos(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<PageInfoJsBean> jsBeans = new ArrayList<PageInfoJsBean>();
            List<PageInfo> pageInfos = getService().getPageInfos(guids);
            if(pageInfos != null) {
                for(PageInfo pageInfo : pageInfos) {
                    jsBeans.add(convertPageInfoToJsBean(pageInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<PageInfoJsBean> getAllPageInfos() throws WebException
    {
        return getAllPageInfos(null, null, null);
    }

    // @Deprecated
    public List<PageInfoJsBean> getAllPageInfos(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllPageInfos(ordering, offset, count, null);
    }

    public List<PageInfoJsBean> getAllPageInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<PageInfoJsBean> jsBeans = new ArrayList<PageInfoJsBean>();
            List<PageInfo> pageInfos = getService().getAllPageInfos(ordering, offset, count, forwardCursor);
            if(pageInfos != null) {
                for(PageInfo pageInfo : pageInfos) {
                    jsBeans.add(convertPageInfoToJsBean(pageInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllPageInfoKeys(ordering, offset, count, null);
    }

    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllPageInfoKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<PageInfoJsBean> findPageInfos(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findPageInfos(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<PageInfoJsBean> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<PageInfoJsBean> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<PageInfoJsBean> jsBeans = new ArrayList<PageInfoJsBean>();
            List<PageInfo> pageInfos = getService().findPageInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(pageInfos != null) {
                for(PageInfo pageInfo : pageInfos) {
                    jsBeans.add(convertPageInfoToJsBean(pageInfo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createPageInfo(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws WebException
    {
        try {
            return getService().createPageInfo(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createPageInfo(PageInfoJsBean jsBean) throws WebException
    {
        try {
            PageInfo pageInfo = convertPageInfoJsBeanToBean(jsBean);
            return getService().createPageInfo(pageInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public PageInfoJsBean constructPageInfo(PageInfoJsBean jsBean) throws WebException
    {
        try {
            PageInfo pageInfo = convertPageInfoJsBeanToBean(jsBean);
            pageInfo = getService().constructPageInfo(pageInfo);
            jsBean = convertPageInfoToJsBean(pageInfo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updatePageInfo(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String pageAuthor, String pageDescription, String favicon, String faviconUrl) throws WebException
    {
        try {
            return getService().updatePageInfo(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, pageAuthor, pageDescription, favicon, faviconUrl);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updatePageInfo(PageInfoJsBean jsBean) throws WebException
    {
        try {
            PageInfo pageInfo = convertPageInfoJsBeanToBean(jsBean);
            return getService().updatePageInfo(pageInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public PageInfoJsBean refreshPageInfo(PageInfoJsBean jsBean) throws WebException
    {
        try {
            PageInfo pageInfo = convertPageInfoJsBeanToBean(jsBean);
            pageInfo = getService().refreshPageInfo(pageInfo);
            jsBean = convertPageInfoToJsBean(pageInfo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deletePageInfo(String guid) throws WebException
    {
        try {
            return getService().deletePageInfo(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deletePageInfo(PageInfoJsBean jsBean) throws WebException
    {
        try {
            PageInfo pageInfo = convertPageInfoJsBeanToBean(jsBean);
            return getService().deletePageInfo(pageInfo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deletePageInfos(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deletePageInfos(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static PageInfoJsBean convertPageInfoToJsBean(PageInfo pageInfo)
    {
        PageInfoJsBean jsBean = null;
        if(pageInfo != null) {
            jsBean = new PageInfoJsBean();
            jsBean.setGuid(pageInfo.getGuid());
            jsBean.setUser(pageInfo.getUser());
            jsBean.setFetchRequest(pageInfo.getFetchRequest());
            jsBean.setTargetUrl(pageInfo.getTargetUrl());
            jsBean.setPageUrl(pageInfo.getPageUrl());
            jsBean.setQueryString(pageInfo.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = pageInfo.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(pageInfo.getLastFetchResult());
            jsBean.setResponseCode(pageInfo.getResponseCode());
            jsBean.setContentType(pageInfo.getContentType());
            jsBean.setContentLength(pageInfo.getContentLength());
            jsBean.setLanguage(pageInfo.getLanguage());
            jsBean.setRedirect(pageInfo.getRedirect());
            jsBean.setLocation(pageInfo.getLocation());
            jsBean.setPageTitle(pageInfo.getPageTitle());
            jsBean.setNote(pageInfo.getNote());
            jsBean.setDeferred(pageInfo.isDeferred());
            jsBean.setStatus(pageInfo.getStatus());
            jsBean.setRefreshStatus(pageInfo.getRefreshStatus());
            jsBean.setRefreshInterval(pageInfo.getRefreshInterval());
            jsBean.setNextRefreshTime(pageInfo.getNextRefreshTime());
            jsBean.setLastCheckedTime(pageInfo.getLastCheckedTime());
            jsBean.setLastUpdatedTime(pageInfo.getLastUpdatedTime());
            jsBean.setPageAuthor(pageInfo.getPageAuthor());
            jsBean.setPageDescription(pageInfo.getPageDescription());
            jsBean.setFavicon(pageInfo.getFavicon());
            jsBean.setFaviconUrl(pageInfo.getFaviconUrl());
            jsBean.setCreatedTime(pageInfo.getCreatedTime());
            jsBean.setModifiedTime(pageInfo.getModifiedTime());
        }
        return jsBean;
    }

    public static PageInfo convertPageInfoJsBeanToBean(PageInfoJsBean jsBean)
    {
        PageInfoBean pageInfo = null;
        if(jsBean != null) {
            pageInfo = new PageInfoBean();
            pageInfo.setGuid(jsBean.getGuid());
            pageInfo.setUser(jsBean.getUser());
            pageInfo.setFetchRequest(jsBean.getFetchRequest());
            pageInfo.setTargetUrl(jsBean.getTargetUrl());
            pageInfo.setPageUrl(jsBean.getPageUrl());
            pageInfo.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            pageInfo.setQueryParams(queryParamsBeans);
            pageInfo.setLastFetchResult(jsBean.getLastFetchResult());
            pageInfo.setResponseCode(jsBean.getResponseCode());
            pageInfo.setContentType(jsBean.getContentType());
            pageInfo.setContentLength(jsBean.getContentLength());
            pageInfo.setLanguage(jsBean.getLanguage());
            pageInfo.setRedirect(jsBean.getRedirect());
            pageInfo.setLocation(jsBean.getLocation());
            pageInfo.setPageTitle(jsBean.getPageTitle());
            pageInfo.setNote(jsBean.getNote());
            pageInfo.setDeferred(jsBean.isDeferred());
            pageInfo.setStatus(jsBean.getStatus());
            pageInfo.setRefreshStatus(jsBean.getRefreshStatus());
            pageInfo.setRefreshInterval(jsBean.getRefreshInterval());
            pageInfo.setNextRefreshTime(jsBean.getNextRefreshTime());
            pageInfo.setLastCheckedTime(jsBean.getLastCheckedTime());
            pageInfo.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            pageInfo.setPageAuthor(jsBean.getPageAuthor());
            pageInfo.setPageDescription(jsBean.getPageDescription());
            pageInfo.setFavicon(jsBean.getFavicon());
            pageInfo.setFaviconUrl(jsBean.getFaviconUrl());
            pageInfo.setCreatedTime(jsBean.getCreatedTime());
            pageInfo.setModifiedTime(jsBean.getModifiedTime());
        }
        return pageInfo;
    }

}
