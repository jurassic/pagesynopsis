package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgBookWebService // implements OgBookService
{
    private static final Logger log = Logger.getLogger(OgBookWebService.class.getName());
     
    // Af service interface.
    private OgBookService mService = null;

    public OgBookWebService()
    {
        this(ServiceProxyFactory.getInstance().getOgBookServiceProxy());
    }
    public OgBookWebService(OgBookService service)
    {
        mService = service;
    }
    
    private OgBookService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOgBookServiceProxy();
        }
        return mService;
    }
    
    
    public OgBookJsBean getOgBook(String guid) throws WebException
    {
        try {
            OgBook ogBook = getService().getOgBook(guid);
            OgBookJsBean bean = convertOgBookToJsBean(ogBook);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgBook(String guid, String field) throws WebException
    {
        try {
            return getService().getOgBook(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgBookJsBean> getOgBooks(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgBookJsBean> jsBeans = new ArrayList<OgBookJsBean>();
            List<OgBook> ogBooks = getService().getOgBooks(guids);
            if(ogBooks != null) {
                for(OgBook ogBook : ogBooks) {
                    jsBeans.add(convertOgBookToJsBean(ogBook));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgBookJsBean> getAllOgBooks() throws WebException
    {
        return getAllOgBooks(null, null, null);
    }

    // @Deprecated
    public List<OgBookJsBean> getAllOgBooks(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgBooks(ordering, offset, count, null);
    }

    public List<OgBookJsBean> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgBookJsBean> jsBeans = new ArrayList<OgBookJsBean>();
            List<OgBook> ogBooks = getService().getAllOgBooks(ordering, offset, count, forwardCursor);
            if(ogBooks != null) {
                for(OgBook ogBook : ogBooks) {
                    jsBeans.add(convertOgBookToJsBean(ogBook));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgBookKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgBookJsBean> findOgBooks(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgBooks(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgBookJsBean> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgBookJsBean> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgBookJsBean> jsBeans = new ArrayList<OgBookJsBean>();
            List<OgBook> ogBooks = getService().findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogBooks != null) {
                for(OgBook ogBook : ogBooks) {
                    jsBeans.add(convertOgBookToJsBean(ogBook));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().createOgBook(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgBook(OgBookJsBean jsBean) throws WebException
    {
        try {
            OgBook ogBook = convertOgBookJsBeanToBean(jsBean);
            return getService().createOgBook(ogBook);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgBookJsBean constructOgBook(OgBookJsBean jsBean) throws WebException
    {
        try {
            OgBook ogBook = convertOgBookJsBeanToBean(jsBean);
            ogBook = getService().constructOgBook(ogBook);
            jsBean = convertOgBookToJsBean(ogBook);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().updateOgBook(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgBook(OgBookJsBean jsBean) throws WebException
    {
        try {
            OgBook ogBook = convertOgBookJsBeanToBean(jsBean);
            return getService().updateOgBook(ogBook);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgBookJsBean refreshOgBook(OgBookJsBean jsBean) throws WebException
    {
        try {
            OgBook ogBook = convertOgBookJsBeanToBean(jsBean);
            ogBook = getService().refreshOgBook(ogBook);
            jsBean = convertOgBookToJsBean(ogBook);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgBook(String guid) throws WebException
    {
        try {
            return getService().deleteOgBook(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgBook(OgBookJsBean jsBean) throws WebException
    {
        try {
            OgBook ogBook = convertOgBookJsBeanToBean(jsBean);
            return getService().deleteOgBook(ogBook);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgBooks(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteOgBooks(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OgBookJsBean convertOgBookToJsBean(OgBook ogBook)
    {
        OgBookJsBean jsBean = null;
        if(ogBook != null) {
            jsBean = new OgBookJsBean();
            jsBean.setGuid(ogBook.getGuid());
            jsBean.setUrl(ogBook.getUrl());
            jsBean.setType(ogBook.getType());
            jsBean.setSiteName(ogBook.getSiteName());
            jsBean.setTitle(ogBook.getTitle());
            jsBean.setDescription(ogBook.getDescription());
            jsBean.setFbAdmins(ogBook.getFbAdmins());
            jsBean.setFbAppId(ogBook.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogBook.getImage();
            if(imageBeans != null) {
                for(OgImageStruct ogImageStruct : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebService.convertOgImageStructToJsBean(ogImageStruct);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogBook.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct ogAudioStruct : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebService.convertOgAudioStructToJsBean(ogAudioStruct);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogBook.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct ogVideoStruct : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebService.convertOgVideoStructToJsBean(ogVideoStruct);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogBook.getLocale());
            jsBean.setLocaleAlternate(ogBook.getLocaleAlternate());
            jsBean.setAuthor(ogBook.getAuthor());
            jsBean.setIsbn(ogBook.getIsbn());
            jsBean.setTag(ogBook.getTag());
            jsBean.setReleaseDate(ogBook.getReleaseDate());
            jsBean.setCreatedTime(ogBook.getCreatedTime());
            jsBean.setModifiedTime(ogBook.getModifiedTime());
        }
        return jsBean;
    }

    public static OgBook convertOgBookJsBeanToBean(OgBookJsBean jsBean)
    {
        OgBookBean ogBook = null;
        if(jsBean != null) {
            ogBook = new OgBookBean();
            ogBook.setGuid(jsBean.getGuid());
            ogBook.setUrl(jsBean.getUrl());
            ogBook.setType(jsBean.getType());
            ogBook.setSiteName(jsBean.getSiteName());
            ogBook.setTitle(jsBean.getTitle());
            ogBook.setDescription(jsBean.getDescription());
            ogBook.setFbAdmins(jsBean.getFbAdmins());
            ogBook.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean ogImageStruct : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebService.convertOgImageStructJsBeanToBean(ogImageStruct);
                    imageBeans.add(b); 
                }
            }
            ogBook.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean ogAudioStruct : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebService.convertOgAudioStructJsBeanToBean(ogAudioStruct);
                    audioBeans.add(b); 
                }
            }
            ogBook.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean ogVideoStruct : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebService.convertOgVideoStructJsBeanToBean(ogVideoStruct);
                    videoBeans.add(b); 
                }
            }
            ogBook.setVideo(videoBeans);
            ogBook.setLocale(jsBean.getLocale());
            ogBook.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogBook.setAuthor(jsBean.getAuthor());
            ogBook.setIsbn(jsBean.getIsbn());
            ogBook.setTag(jsBean.getTag());
            ogBook.setReleaseDate(jsBean.getReleaseDate());
            ogBook.setCreatedTime(jsBean.getCreatedTime());
            ogBook.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogBook;
    }

}
