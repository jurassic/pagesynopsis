package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AudioStructWebService // implements AudioStructService
{
    private static final Logger log = Logger.getLogger(AudioStructWebService.class.getName());
     
    public static AudioStructJsBean convertAudioStructToJsBean(AudioStruct audioStruct)
    {
        AudioStructJsBean jsBean = null;
        if(audioStruct != null) {
            jsBean = new AudioStructJsBean();
            jsBean.setUuid(audioStruct.getUuid());
            jsBean.setId(audioStruct.getId());
            jsBean.setControls(audioStruct.getControls());
            jsBean.setAutoplayEnabled(audioStruct.isAutoplayEnabled());
            jsBean.setLoopEnabled(audioStruct.isLoopEnabled());
            jsBean.setPreloadEnabled(audioStruct.isPreloadEnabled());
            jsBean.setRemark(audioStruct.getRemark());
            jsBean.setSource(MediaSourceStructWebService.convertMediaSourceStructToJsBean(audioStruct.getSource()));
            jsBean.setSource1(MediaSourceStructWebService.convertMediaSourceStructToJsBean(audioStruct.getSource1()));
            jsBean.setSource2(MediaSourceStructWebService.convertMediaSourceStructToJsBean(audioStruct.getSource2()));
            jsBean.setSource3(MediaSourceStructWebService.convertMediaSourceStructToJsBean(audioStruct.getSource3()));
            jsBean.setSource4(MediaSourceStructWebService.convertMediaSourceStructToJsBean(audioStruct.getSource4()));
            jsBean.setSource5(MediaSourceStructWebService.convertMediaSourceStructToJsBean(audioStruct.getSource5()));
            jsBean.setNote(audioStruct.getNote());
        }
        return jsBean;
    }

    public static AudioStruct convertAudioStructJsBeanToBean(AudioStructJsBean jsBean)
    {
        AudioStructBean audioStruct = null;
        if(jsBean != null) {
            audioStruct = new AudioStructBean();
            audioStruct.setUuid(jsBean.getUuid());
            audioStruct.setId(jsBean.getId());
            audioStruct.setControls(jsBean.getControls());
            audioStruct.setAutoplayEnabled(jsBean.isAutoplayEnabled());
            audioStruct.setLoopEnabled(jsBean.isLoopEnabled());
            audioStruct.setPreloadEnabled(jsBean.isPreloadEnabled());
            audioStruct.setRemark(jsBean.getRemark());
            audioStruct.setSource(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource()));
            audioStruct.setSource1(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource1()));
            audioStruct.setSource2(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource2()));
            audioStruct.setSource3(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource3()));
            audioStruct.setSource4(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource4()));
            audioStruct.setSource5(MediaSourceStructWebService.convertMediaSourceStructJsBeanToBean(jsBean.getSource5()));
            audioStruct.setNote(jsBean.getNote());
        }
        return audioStruct;
    }

}
