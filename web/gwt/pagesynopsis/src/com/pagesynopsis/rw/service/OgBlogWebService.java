package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgBlogWebService // implements OgBlogService
{
    private static final Logger log = Logger.getLogger(OgBlogWebService.class.getName());
     
    // Af service interface.
    private OgBlogService mService = null;

    public OgBlogWebService()
    {
        this(ServiceProxyFactory.getInstance().getOgBlogServiceProxy());
    }
    public OgBlogWebService(OgBlogService service)
    {
        mService = service;
    }
    
    private OgBlogService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getOgBlogServiceProxy();
        }
        return mService;
    }
    
    
    public OgBlogJsBean getOgBlog(String guid) throws WebException
    {
        try {
            OgBlog ogBlog = getService().getOgBlog(guid);
            OgBlogJsBean bean = convertOgBlogToJsBean(ogBlog);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgBlog(String guid, String field) throws WebException
    {
        try {
            return getService().getOgBlog(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgBlogJsBean> getOgBlogs(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgBlogJsBean> jsBeans = new ArrayList<OgBlogJsBean>();
            List<OgBlog> ogBlogs = getService().getOgBlogs(guids);
            if(ogBlogs != null) {
                for(OgBlog ogBlog : ogBlogs) {
                    jsBeans.add(convertOgBlogToJsBean(ogBlog));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgBlogJsBean> getAllOgBlogs() throws WebException
    {
        return getAllOgBlogs(null, null, null);
    }

    // @Deprecated
    public List<OgBlogJsBean> getAllOgBlogs(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgBlogs(ordering, offset, count, null);
    }

    public List<OgBlogJsBean> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgBlogJsBean> jsBeans = new ArrayList<OgBlogJsBean>();
            List<OgBlog> ogBlogs = getService().getAllOgBlogs(ordering, offset, count, forwardCursor);
            if(ogBlogs != null) {
                for(OgBlog ogBlog : ogBlogs) {
                    jsBeans.add(convertOgBlogToJsBean(ogBlog));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgBlogKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgBlogJsBean> findOgBlogs(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgBlogs(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgBlogJsBean> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgBlogJsBean> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<OgBlogJsBean> jsBeans = new ArrayList<OgBlogJsBean>();
            List<OgBlog> ogBlogs = getService().findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogBlogs != null) {
                for(OgBlog ogBlog : ogBlogs) {
                    jsBeans.add(convertOgBlogToJsBean(ogBlog));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws WebException
    {
        try {
            return getService().createOgBlog(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgBlog(OgBlogJsBean jsBean) throws WebException
    {
        try {
            OgBlog ogBlog = convertOgBlogJsBeanToBean(jsBean);
            return getService().createOgBlog(ogBlog);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgBlogJsBean constructOgBlog(OgBlogJsBean jsBean) throws WebException
    {
        try {
            OgBlog ogBlog = convertOgBlogJsBeanToBean(jsBean);
            ogBlog = getService().constructOgBlog(ogBlog);
            jsBean = convertOgBlogToJsBean(ogBlog);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws WebException
    {
        try {
            return getService().updateOgBlog(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgBlog(OgBlogJsBean jsBean) throws WebException
    {
        try {
            OgBlog ogBlog = convertOgBlogJsBeanToBean(jsBean);
            return getService().updateOgBlog(ogBlog);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgBlogJsBean refreshOgBlog(OgBlogJsBean jsBean) throws WebException
    {
        try {
            OgBlog ogBlog = convertOgBlogJsBeanToBean(jsBean);
            ogBlog = getService().refreshOgBlog(ogBlog);
            jsBean = convertOgBlogToJsBean(ogBlog);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgBlog(String guid) throws WebException
    {
        try {
            return getService().deleteOgBlog(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgBlog(OgBlogJsBean jsBean) throws WebException
    {
        try {
            OgBlog ogBlog = convertOgBlogJsBeanToBean(jsBean);
            return getService().deleteOgBlog(ogBlog);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgBlogs(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteOgBlogs(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static OgBlogJsBean convertOgBlogToJsBean(OgBlog ogBlog)
    {
        OgBlogJsBean jsBean = null;
        if(ogBlog != null) {
            jsBean = new OgBlogJsBean();
            jsBean.setGuid(ogBlog.getGuid());
            jsBean.setUrl(ogBlog.getUrl());
            jsBean.setType(ogBlog.getType());
            jsBean.setSiteName(ogBlog.getSiteName());
            jsBean.setTitle(ogBlog.getTitle());
            jsBean.setDescription(ogBlog.getDescription());
            jsBean.setFbAdmins(ogBlog.getFbAdmins());
            jsBean.setFbAppId(ogBlog.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogBlog.getImage();
            if(imageBeans != null) {
                for(OgImageStruct ogImageStruct : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebService.convertOgImageStructToJsBean(ogImageStruct);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogBlog.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct ogAudioStruct : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebService.convertOgAudioStructToJsBean(ogAudioStruct);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogBlog.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct ogVideoStruct : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebService.convertOgVideoStructToJsBean(ogVideoStruct);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogBlog.getLocale());
            jsBean.setLocaleAlternate(ogBlog.getLocaleAlternate());
            jsBean.setNote(ogBlog.getNote());
            jsBean.setCreatedTime(ogBlog.getCreatedTime());
            jsBean.setModifiedTime(ogBlog.getModifiedTime());
        }
        return jsBean;
    }

    public static OgBlog convertOgBlogJsBeanToBean(OgBlogJsBean jsBean)
    {
        OgBlogBean ogBlog = null;
        if(jsBean != null) {
            ogBlog = new OgBlogBean();
            ogBlog.setGuid(jsBean.getGuid());
            ogBlog.setUrl(jsBean.getUrl());
            ogBlog.setType(jsBean.getType());
            ogBlog.setSiteName(jsBean.getSiteName());
            ogBlog.setTitle(jsBean.getTitle());
            ogBlog.setDescription(jsBean.getDescription());
            ogBlog.setFbAdmins(jsBean.getFbAdmins());
            ogBlog.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean ogImageStruct : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebService.convertOgImageStructJsBeanToBean(ogImageStruct);
                    imageBeans.add(b); 
                }
            }
            ogBlog.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean ogAudioStruct : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebService.convertOgAudioStructJsBeanToBean(ogAudioStruct);
                    audioBeans.add(b); 
                }
            }
            ogBlog.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean ogVideoStruct : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebService.convertOgVideoStructJsBeanToBean(ogVideoStruct);
                    videoBeans.add(b); 
                }
            }
            ogBlog.setVideo(videoBeans);
            ogBlog.setLocale(jsBean.getLocale());
            ogBlog.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogBlog.setNote(jsBean.getNote());
            ogBlog.setCreatedTime(jsBean.getCreatedTime());
            ogBlog.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogBlog;
    }

}
