package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgQuantityStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgQuantityStructWebService // implements OgQuantityStructService
{
    private static final Logger log = Logger.getLogger(OgQuantityStructWebService.class.getName());
     
    public static OgQuantityStructJsBean convertOgQuantityStructToJsBean(OgQuantityStruct ogQuantityStruct)
    {
        OgQuantityStructJsBean jsBean = null;
        if(ogQuantityStruct != null) {
            jsBean = new OgQuantityStructJsBean();
            jsBean.setUuid(ogQuantityStruct.getUuid());
            jsBean.setValue(ogQuantityStruct.getValue());
            jsBean.setUnits(ogQuantityStruct.getUnits());
        }
        return jsBean;
    }

    public static OgQuantityStruct convertOgQuantityStructJsBeanToBean(OgQuantityStructJsBean jsBean)
    {
        OgQuantityStructBean ogQuantityStruct = null;
        if(jsBean != null) {
            ogQuantityStruct = new OgQuantityStructBean();
            ogQuantityStruct.setUuid(jsBean.getUuid());
            ogQuantityStruct.setValue(jsBean.getValue());
            ogQuantityStruct.setUnits(jsBean.getUnits());
        }
        return ogQuantityStruct;
    }

}
