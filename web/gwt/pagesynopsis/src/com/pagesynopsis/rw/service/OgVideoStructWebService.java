package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgVideoStructWebService // implements OgVideoStructService
{
    private static final Logger log = Logger.getLogger(OgVideoStructWebService.class.getName());
     
    public static OgVideoStructJsBean convertOgVideoStructToJsBean(OgVideoStruct ogVideoStruct)
    {
        OgVideoStructJsBean jsBean = null;
        if(ogVideoStruct != null) {
            jsBean = new OgVideoStructJsBean();
            jsBean.setUuid(ogVideoStruct.getUuid());
            jsBean.setUrl(ogVideoStruct.getUrl());
            jsBean.setSecureUrl(ogVideoStruct.getSecureUrl());
            jsBean.setType(ogVideoStruct.getType());
            jsBean.setWidth(ogVideoStruct.getWidth());
            jsBean.setHeight(ogVideoStruct.getHeight());
        }
        return jsBean;
    }

    public static OgVideoStruct convertOgVideoStructJsBeanToBean(OgVideoStructJsBean jsBean)
    {
        OgVideoStructBean ogVideoStruct = null;
        if(jsBean != null) {
            ogVideoStruct = new OgVideoStructBean();
            ogVideoStruct.setUuid(jsBean.getUuid());
            ogVideoStruct.setUrl(jsBean.getUrl());
            ogVideoStruct.setSecureUrl(jsBean.getSecureUrl());
            ogVideoStruct.setType(jsBean.getType());
            ogVideoStruct.setWidth(jsBean.getWidth());
            ogVideoStruct.setHeight(jsBean.getHeight());
        }
        return ogVideoStruct;
    }

}
