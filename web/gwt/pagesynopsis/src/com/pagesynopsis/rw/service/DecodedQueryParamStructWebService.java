package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.DecodedQueryParamStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DecodedQueryParamStructWebService // implements DecodedQueryParamStructService
{
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructWebService.class.getName());
     
    public static DecodedQueryParamStructJsBean convertDecodedQueryParamStructToJsBean(DecodedQueryParamStruct decodedQueryParamStruct)
    {
        DecodedQueryParamStructJsBean jsBean = null;
        if(decodedQueryParamStruct != null) {
            jsBean = new DecodedQueryParamStructJsBean();
            jsBean.setParamType(decodedQueryParamStruct.getParamType());
            jsBean.setOriginalString(decodedQueryParamStruct.getOriginalString());
            jsBean.setDecodedString(decodedQueryParamStruct.getDecodedString());
            jsBean.setNote(decodedQueryParamStruct.getNote());
        }
        return jsBean;
    }

    public static DecodedQueryParamStruct convertDecodedQueryParamStructJsBeanToBean(DecodedQueryParamStructJsBean jsBean)
    {
        DecodedQueryParamStructBean decodedQueryParamStruct = null;
        if(jsBean != null) {
            decodedQueryParamStruct = new DecodedQueryParamStructBean();
            decodedQueryParamStruct.setParamType(jsBean.getParamType());
            decodedQueryParamStruct.setOriginalString(jsBean.getOriginalString());
            decodedQueryParamStruct.setDecodedString(jsBean.getDecodedString());
            decodedQueryParamStruct.setNote(jsBean.getNote());
        }
        return decodedQueryParamStruct;
    }

}
