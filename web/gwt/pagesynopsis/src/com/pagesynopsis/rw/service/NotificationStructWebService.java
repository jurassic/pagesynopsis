package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.NotificationStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class NotificationStructWebService // implements NotificationStructService
{
    private static final Logger log = Logger.getLogger(NotificationStructWebService.class.getName());
     
    public static NotificationStructJsBean convertNotificationStructToJsBean(NotificationStruct notificationStruct)
    {
        NotificationStructJsBean jsBean = null;
        if(notificationStruct != null) {
            jsBean = new NotificationStructJsBean();
            jsBean.setPreferredMode(notificationStruct.getPreferredMode());
            jsBean.setMobileNumber(notificationStruct.getMobileNumber());
            jsBean.setEmailAddress(notificationStruct.getEmailAddress());
            jsBean.setTwitterUsername(notificationStruct.getTwitterUsername());
            jsBean.setFacebookId(notificationStruct.getFacebookId());
            jsBean.setLinkedinId(notificationStruct.getLinkedinId());
            jsBean.setNote(notificationStruct.getNote());
        }
        return jsBean;
    }

    public static NotificationStruct convertNotificationStructJsBeanToBean(NotificationStructJsBean jsBean)
    {
        NotificationStructBean notificationStruct = null;
        if(jsBean != null) {
            notificationStruct = new NotificationStructBean();
            notificationStruct.setPreferredMode(jsBean.getPreferredMode());
            notificationStruct.setMobileNumber(jsBean.getMobileNumber());
            notificationStruct.setEmailAddress(jsBean.getEmailAddress());
            notificationStruct.setTwitterUsername(jsBean.getTwitterUsername());
            notificationStruct.setFacebookId(jsBean.getFacebookId());
            notificationStruct.setLinkedinId(jsBean.getLinkedinId());
            notificationStruct.setNote(jsBean.getNote());
        }
        return notificationStruct;
    }

}
