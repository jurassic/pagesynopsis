package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ImageStructWebService // implements ImageStructService
{
    private static final Logger log = Logger.getLogger(ImageStructWebService.class.getName());
     
    public static ImageStructJsBean convertImageStructToJsBean(ImageStruct imageStruct)
    {
        ImageStructJsBean jsBean = null;
        if(imageStruct != null) {
            jsBean = new ImageStructJsBean();
            jsBean.setUuid(imageStruct.getUuid());
            jsBean.setId(imageStruct.getId());
            jsBean.setAlt(imageStruct.getAlt());
            jsBean.setSrc(imageStruct.getSrc());
            jsBean.setSrcUrl(imageStruct.getSrcUrl());
            jsBean.setMediaType(imageStruct.getMediaType());
            jsBean.setWidthAttr(imageStruct.getWidthAttr());
            jsBean.setWidth(imageStruct.getWidth());
            jsBean.setHeightAttr(imageStruct.getHeightAttr());
            jsBean.setHeight(imageStruct.getHeight());
            jsBean.setNote(imageStruct.getNote());
        }
        return jsBean;
    }

    public static ImageStruct convertImageStructJsBeanToBean(ImageStructJsBean jsBean)
    {
        ImageStructBean imageStruct = null;
        if(jsBean != null) {
            imageStruct = new ImageStructBean();
            imageStruct.setUuid(jsBean.getUuid());
            imageStruct.setId(jsBean.getId());
            imageStruct.setAlt(jsBean.getAlt());
            imageStruct.setSrc(jsBean.getSrc());
            imageStruct.setSrcUrl(jsBean.getSrcUrl());
            imageStruct.setMediaType(jsBean.getMediaType());
            imageStruct.setWidthAttr(jsBean.getWidthAttr());
            imageStruct.setWidth(jsBean.getWidth());
            imageStruct.setHeightAttr(jsBean.getHeightAttr());
            imageStruct.setHeight(jsBean.getHeight());
            imageStruct.setNote(jsBean.getNote());
        }
        return imageStruct;
    }

}
