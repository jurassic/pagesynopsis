package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.AppBrandStruct;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.AppBrandStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppBrandStructWebService // implements AppBrandStructService
{
    private static final Logger log = Logger.getLogger(AppBrandStructWebService.class.getName());
     
    public static AppBrandStructJsBean convertAppBrandStructToJsBean(AppBrandStruct appBrandStruct)
    {
        AppBrandStructJsBean jsBean = null;
        if(appBrandStruct != null) {
            jsBean = new AppBrandStructJsBean();
            jsBean.setBrand(appBrandStruct.getBrand());
            jsBean.setName(appBrandStruct.getName());
            jsBean.setDescription(appBrandStruct.getDescription());
        }
        return jsBean;
    }

    public static AppBrandStruct convertAppBrandStructJsBeanToBean(AppBrandStructJsBean jsBean)
    {
        AppBrandStructBean appBrandStruct = null;
        if(jsBean != null) {
            appBrandStruct = new AppBrandStructBean();
            appBrandStruct.setBrand(jsBean.getBrand());
            appBrandStruct.setName(jsBean.getName());
            appBrandStruct.setDescription(jsBean.getDescription());
        }
        return appBrandStruct;
    }

}
