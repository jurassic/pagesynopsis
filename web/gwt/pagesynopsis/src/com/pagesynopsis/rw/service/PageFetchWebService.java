package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.PageFetchJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PageFetchWebService // implements PageFetchService
{
    private static final Logger log = Logger.getLogger(PageFetchWebService.class.getName());
     
    // Af service interface.
    private PageFetchService mService = null;

    public PageFetchWebService()
    {
        this(ServiceProxyFactory.getInstance().getPageFetchServiceProxy());
    }
    public PageFetchWebService(PageFetchService service)
    {
        mService = service;
    }
    
    private PageFetchService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getPageFetchServiceProxy();
        }
        return mService;
    }
    
    
    public PageFetchJsBean getPageFetch(String guid) throws WebException
    {
        try {
            PageFetch pageFetch = getService().getPageFetch(guid);
            PageFetchJsBean bean = convertPageFetchToJsBean(pageFetch);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getPageFetch(String guid, String field) throws WebException
    {
        try {
            return getService().getPageFetch(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<PageFetchJsBean> getPageFetches(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<PageFetchJsBean> jsBeans = new ArrayList<PageFetchJsBean>();
            List<PageFetch> pageFetches = getService().getPageFetches(guids);
            if(pageFetches != null) {
                for(PageFetch pageFetch : pageFetches) {
                    jsBeans.add(convertPageFetchToJsBean(pageFetch));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<PageFetchJsBean> getAllPageFetches() throws WebException
    {
        return getAllPageFetches(null, null, null);
    }

    // @Deprecated
    public List<PageFetchJsBean> getAllPageFetches(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllPageFetches(ordering, offset, count, null);
    }

    public List<PageFetchJsBean> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<PageFetchJsBean> jsBeans = new ArrayList<PageFetchJsBean>();
            List<PageFetch> pageFetches = getService().getAllPageFetches(ordering, offset, count, forwardCursor);
            if(pageFetches != null) {
                for(PageFetch pageFetch : pageFetches) {
                    jsBeans.add(convertPageFetchToJsBean(pageFetch));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllPageFetchKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<PageFetchJsBean> findPageFetches(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<PageFetchJsBean> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<PageFetchJsBean> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<PageFetchJsBean> jsBeans = new ArrayList<PageFetchJsBean>();
            List<PageFetch> pageFetches = getService().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(pageFetches != null) {
                for(PageFetch pageFetch : pageFetches) {
                    jsBeans.add(convertPageFetchToJsBean(pageFetch));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws WebException
    {
        try {
            return getService().createPageFetch(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createPageFetch(PageFetchJsBean jsBean) throws WebException
    {
        try {
            PageFetch pageFetch = convertPageFetchJsBeanToBean(jsBean);
            return getService().createPageFetch(pageFetch);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public PageFetchJsBean constructPageFetch(PageFetchJsBean jsBean) throws WebException
    {
        try {
            PageFetch pageFetch = convertPageFetchJsBeanToBean(jsBean);
            pageFetch = getService().constructPageFetch(pageFetch);
            jsBean = convertPageFetchToJsBean(pageFetch);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws WebException
    {
        try {
            return getService().updatePageFetch(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updatePageFetch(PageFetchJsBean jsBean) throws WebException
    {
        try {
            PageFetch pageFetch = convertPageFetchJsBeanToBean(jsBean);
            return getService().updatePageFetch(pageFetch);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public PageFetchJsBean refreshPageFetch(PageFetchJsBean jsBean) throws WebException
    {
        try {
            PageFetch pageFetch = convertPageFetchJsBeanToBean(jsBean);
            pageFetch = getService().refreshPageFetch(pageFetch);
            jsBean = convertPageFetchToJsBean(pageFetch);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deletePageFetch(String guid) throws WebException
    {
        try {
            return getService().deletePageFetch(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deletePageFetch(PageFetchJsBean jsBean) throws WebException
    {
        try {
            PageFetch pageFetch = convertPageFetchJsBeanToBean(jsBean);
            return getService().deletePageFetch(pageFetch);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deletePageFetches(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deletePageFetches(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static PageFetchJsBean convertPageFetchToJsBean(PageFetch pageFetch)
    {
        PageFetchJsBean jsBean = null;
        if(pageFetch != null) {
            jsBean = new PageFetchJsBean();
            jsBean.setGuid(pageFetch.getGuid());
            jsBean.setUser(pageFetch.getUser());
            jsBean.setFetchRequest(pageFetch.getFetchRequest());
            jsBean.setTargetUrl(pageFetch.getTargetUrl());
            jsBean.setPageUrl(pageFetch.getPageUrl());
            jsBean.setQueryString(pageFetch.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = pageFetch.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(pageFetch.getLastFetchResult());
            jsBean.setResponseCode(pageFetch.getResponseCode());
            jsBean.setContentType(pageFetch.getContentType());
            jsBean.setContentLength(pageFetch.getContentLength());
            jsBean.setLanguage(pageFetch.getLanguage());
            jsBean.setRedirect(pageFetch.getRedirect());
            jsBean.setLocation(pageFetch.getLocation());
            jsBean.setPageTitle(pageFetch.getPageTitle());
            jsBean.setNote(pageFetch.getNote());
            jsBean.setDeferred(pageFetch.isDeferred());
            jsBean.setStatus(pageFetch.getStatus());
            jsBean.setRefreshStatus(pageFetch.getRefreshStatus());
            jsBean.setRefreshInterval(pageFetch.getRefreshInterval());
            jsBean.setNextRefreshTime(pageFetch.getNextRefreshTime());
            jsBean.setLastCheckedTime(pageFetch.getLastCheckedTime());
            jsBean.setLastUpdatedTime(pageFetch.getLastUpdatedTime());
            jsBean.setInputMaxRedirects(pageFetch.getInputMaxRedirects());
            jsBean.setResultRedirectCount(pageFetch.getResultRedirectCount());
            List<UrlStructJsBean> redirectPagesJsBeans = new ArrayList<UrlStructJsBean>();
            List<UrlStruct> redirectPagesBeans = pageFetch.getRedirectPages();
            if(redirectPagesBeans != null) {
                for(UrlStruct urlStruct : redirectPagesBeans) {
                    UrlStructJsBean jB = UrlStructWebService.convertUrlStructToJsBean(urlStruct);
                    redirectPagesJsBeans.add(jB); 
                }
            }
            jsBean.setRedirectPages(redirectPagesJsBeans);
            jsBean.setDestinationUrl(pageFetch.getDestinationUrl());
            jsBean.setPageAuthor(pageFetch.getPageAuthor());
            jsBean.setPageSummary(pageFetch.getPageSummary());
            jsBean.setFavicon(pageFetch.getFavicon());
            jsBean.setFaviconUrl(pageFetch.getFaviconUrl());
            jsBean.setCreatedTime(pageFetch.getCreatedTime());
            jsBean.setModifiedTime(pageFetch.getModifiedTime());
        }
        return jsBean;
    }

    public static PageFetch convertPageFetchJsBeanToBean(PageFetchJsBean jsBean)
    {
        PageFetchBean pageFetch = null;
        if(jsBean != null) {
            pageFetch = new PageFetchBean();
            pageFetch.setGuid(jsBean.getGuid());
            pageFetch.setUser(jsBean.getUser());
            pageFetch.setFetchRequest(jsBean.getFetchRequest());
            pageFetch.setTargetUrl(jsBean.getTargetUrl());
            pageFetch.setPageUrl(jsBean.getPageUrl());
            pageFetch.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            pageFetch.setQueryParams(queryParamsBeans);
            pageFetch.setLastFetchResult(jsBean.getLastFetchResult());
            pageFetch.setResponseCode(jsBean.getResponseCode());
            pageFetch.setContentType(jsBean.getContentType());
            pageFetch.setContentLength(jsBean.getContentLength());
            pageFetch.setLanguage(jsBean.getLanguage());
            pageFetch.setRedirect(jsBean.getRedirect());
            pageFetch.setLocation(jsBean.getLocation());
            pageFetch.setPageTitle(jsBean.getPageTitle());
            pageFetch.setNote(jsBean.getNote());
            pageFetch.setDeferred(jsBean.isDeferred());
            pageFetch.setStatus(jsBean.getStatus());
            pageFetch.setRefreshStatus(jsBean.getRefreshStatus());
            pageFetch.setRefreshInterval(jsBean.getRefreshInterval());
            pageFetch.setNextRefreshTime(jsBean.getNextRefreshTime());
            pageFetch.setLastCheckedTime(jsBean.getLastCheckedTime());
            pageFetch.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            pageFetch.setInputMaxRedirects(jsBean.getInputMaxRedirects());
            pageFetch.setResultRedirectCount(jsBean.getResultRedirectCount());
            List<UrlStruct> redirectPagesBeans = new ArrayList<UrlStruct>();
            List<UrlStructJsBean> redirectPagesJsBeans = jsBean.getRedirectPages();
            if(redirectPagesJsBeans != null) {
                for(UrlStructJsBean urlStruct : redirectPagesJsBeans) {
                    UrlStruct b = UrlStructWebService.convertUrlStructJsBeanToBean(urlStruct);
                    redirectPagesBeans.add(b); 
                }
            }
            pageFetch.setRedirectPages(redirectPagesBeans);
            pageFetch.setDestinationUrl(jsBean.getDestinationUrl());
            pageFetch.setPageAuthor(jsBean.getPageAuthor());
            pageFetch.setPageSummary(jsBean.getPageSummary());
            pageFetch.setFavicon(jsBean.getFavicon());
            pageFetch.setFaviconUrl(jsBean.getFaviconUrl());
            pageFetch.setCreatedTime(jsBean.getCreatedTime());
            pageFetch.setModifiedTime(jsBean.getModifiedTime());
        }
        return pageFetch;
    }

}
