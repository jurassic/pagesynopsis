package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.LinkListJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkListWebService // implements LinkListService
{
    private static final Logger log = Logger.getLogger(LinkListWebService.class.getName());
     
    // Af service interface.
    private LinkListService mService = null;

    public LinkListWebService()
    {
        this(ServiceProxyFactory.getInstance().getLinkListServiceProxy());
    }
    public LinkListWebService(LinkListService service)
    {
        mService = service;
    }
    
    private LinkListService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getLinkListServiceProxy();
        }
        return mService;
    }
    
    
    public LinkListJsBean getLinkList(String guid) throws WebException
    {
        try {
            LinkList linkList = getService().getLinkList(guid);
            LinkListJsBean bean = convertLinkListToJsBean(linkList);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getLinkList(String guid, String field) throws WebException
    {
        try {
            return getService().getLinkList(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkListJsBean> getLinkLists(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkListJsBean> jsBeans = new ArrayList<LinkListJsBean>();
            List<LinkList> linkLists = getService().getLinkLists(guids);
            if(linkLists != null) {
                for(LinkList linkList : linkLists) {
                    jsBeans.add(convertLinkListToJsBean(linkList));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkListJsBean> getAllLinkLists() throws WebException
    {
        return getAllLinkLists(null, null, null);
    }

    // @Deprecated
    public List<LinkListJsBean> getAllLinkLists(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllLinkLists(ordering, offset, count, null);
    }

    public List<LinkListJsBean> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<LinkListJsBean> jsBeans = new ArrayList<LinkListJsBean>();
            List<LinkList> linkLists = getService().getAllLinkLists(ordering, offset, count, forwardCursor);
            if(linkLists != null) {
                for(LinkList linkList : linkLists) {
                    jsBeans.add(convertLinkListToJsBean(linkList));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllLinkListKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<LinkListJsBean> findLinkLists(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<LinkListJsBean> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<LinkListJsBean> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<LinkListJsBean> jsBeans = new ArrayList<LinkListJsBean>();
            List<LinkList> linkLists = getService().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(linkLists != null) {
                for(LinkList linkList : linkLists) {
                    jsBeans.add(convertLinkListToJsBean(linkList));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws WebException
    {
        try {
            return getService().createLinkList(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkList(LinkListJsBean jsBean) throws WebException
    {
        try {
            LinkList linkList = convertLinkListJsBeanToBean(jsBean);
            return getService().createLinkList(linkList);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkListJsBean constructLinkList(LinkListJsBean jsBean) throws WebException
    {
        try {
            LinkList linkList = convertLinkListJsBeanToBean(jsBean);
            linkList = getService().constructLinkList(linkList);
            jsBean = convertLinkListToJsBean(linkList);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws WebException
    {
        try {
            return getService().updateLinkList(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateLinkList(LinkListJsBean jsBean) throws WebException
    {
        try {
            LinkList linkList = convertLinkListJsBeanToBean(jsBean);
            return getService().updateLinkList(linkList);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkListJsBean refreshLinkList(LinkListJsBean jsBean) throws WebException
    {
        try {
            LinkList linkList = convertLinkListJsBeanToBean(jsBean);
            linkList = getService().refreshLinkList(linkList);
            jsBean = convertLinkListToJsBean(linkList);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkList(String guid) throws WebException
    {
        try {
            return getService().deleteLinkList(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkList(LinkListJsBean jsBean) throws WebException
    {
        try {
            LinkList linkList = convertLinkListJsBeanToBean(jsBean);
            return getService().deleteLinkList(linkList);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteLinkLists(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteLinkLists(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static LinkListJsBean convertLinkListToJsBean(LinkList linkList)
    {
        LinkListJsBean jsBean = null;
        if(linkList != null) {
            jsBean = new LinkListJsBean();
            jsBean.setGuid(linkList.getGuid());
            jsBean.setUser(linkList.getUser());
            jsBean.setFetchRequest(linkList.getFetchRequest());
            jsBean.setTargetUrl(linkList.getTargetUrl());
            jsBean.setPageUrl(linkList.getPageUrl());
            jsBean.setQueryString(linkList.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = linkList.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(linkList.getLastFetchResult());
            jsBean.setResponseCode(linkList.getResponseCode());
            jsBean.setContentType(linkList.getContentType());
            jsBean.setContentLength(linkList.getContentLength());
            jsBean.setLanguage(linkList.getLanguage());
            jsBean.setRedirect(linkList.getRedirect());
            jsBean.setLocation(linkList.getLocation());
            jsBean.setPageTitle(linkList.getPageTitle());
            jsBean.setNote(linkList.getNote());
            jsBean.setDeferred(linkList.isDeferred());
            jsBean.setStatus(linkList.getStatus());
            jsBean.setRefreshStatus(linkList.getRefreshStatus());
            jsBean.setRefreshInterval(linkList.getRefreshInterval());
            jsBean.setNextRefreshTime(linkList.getNextRefreshTime());
            jsBean.setLastCheckedTime(linkList.getLastCheckedTime());
            jsBean.setLastUpdatedTime(linkList.getLastUpdatedTime());
            jsBean.setUrlSchemeFilter(linkList.getUrlSchemeFilter());
            List<AnchorStructJsBean> pageAnchorsJsBeans = new ArrayList<AnchorStructJsBean>();
            List<AnchorStruct> pageAnchorsBeans = linkList.getPageAnchors();
            if(pageAnchorsBeans != null) {
                for(AnchorStruct anchorStruct : pageAnchorsBeans) {
                    AnchorStructJsBean jB = AnchorStructWebService.convertAnchorStructToJsBean(anchorStruct);
                    pageAnchorsJsBeans.add(jB); 
                }
            }
            jsBean.setPageAnchors(pageAnchorsJsBeans);
            jsBean.setExcludeRelativeUrls(linkList.isExcludeRelativeUrls());
            jsBean.setExcludedBaseUrls(linkList.getExcludedBaseUrls());
            jsBean.setCreatedTime(linkList.getCreatedTime());
            jsBean.setModifiedTime(linkList.getModifiedTime());
        }
        return jsBean;
    }

    public static LinkList convertLinkListJsBeanToBean(LinkListJsBean jsBean)
    {
        LinkListBean linkList = null;
        if(jsBean != null) {
            linkList = new LinkListBean();
            linkList.setGuid(jsBean.getGuid());
            linkList.setUser(jsBean.getUser());
            linkList.setFetchRequest(jsBean.getFetchRequest());
            linkList.setTargetUrl(jsBean.getTargetUrl());
            linkList.setPageUrl(jsBean.getPageUrl());
            linkList.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            linkList.setQueryParams(queryParamsBeans);
            linkList.setLastFetchResult(jsBean.getLastFetchResult());
            linkList.setResponseCode(jsBean.getResponseCode());
            linkList.setContentType(jsBean.getContentType());
            linkList.setContentLength(jsBean.getContentLength());
            linkList.setLanguage(jsBean.getLanguage());
            linkList.setRedirect(jsBean.getRedirect());
            linkList.setLocation(jsBean.getLocation());
            linkList.setPageTitle(jsBean.getPageTitle());
            linkList.setNote(jsBean.getNote());
            linkList.setDeferred(jsBean.isDeferred());
            linkList.setStatus(jsBean.getStatus());
            linkList.setRefreshStatus(jsBean.getRefreshStatus());
            linkList.setRefreshInterval(jsBean.getRefreshInterval());
            linkList.setNextRefreshTime(jsBean.getNextRefreshTime());
            linkList.setLastCheckedTime(jsBean.getLastCheckedTime());
            linkList.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            linkList.setUrlSchemeFilter(jsBean.getUrlSchemeFilter());
            List<AnchorStruct> pageAnchorsBeans = new ArrayList<AnchorStruct>();
            List<AnchorStructJsBean> pageAnchorsJsBeans = jsBean.getPageAnchors();
            if(pageAnchorsJsBeans != null) {
                for(AnchorStructJsBean anchorStruct : pageAnchorsJsBeans) {
                    AnchorStruct b = AnchorStructWebService.convertAnchorStructJsBeanToBean(anchorStruct);
                    pageAnchorsBeans.add(b); 
                }
            }
            linkList.setPageAnchors(pageAnchorsBeans);
            linkList.setExcludeRelativeUrls(jsBean.isExcludeRelativeUrls());
            linkList.setExcludedBaseUrls(jsBean.getExcludedBaseUrls());
            linkList.setCreatedTime(jsBean.getCreatedTime());
            linkList.setModifiedTime(jsBean.getModifiedTime());
        }
        return linkList;
    }

}
