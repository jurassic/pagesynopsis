package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.TwitterCardProductDataJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterCardProductDataWebService // implements TwitterCardProductDataService
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataWebService.class.getName());
     
    public static TwitterCardProductDataJsBean convertTwitterCardProductDataToJsBean(TwitterCardProductData twitterCardProductData)
    {
        TwitterCardProductDataJsBean jsBean = null;
        if(twitterCardProductData != null) {
            jsBean = new TwitterCardProductDataJsBean();
            jsBean.setData(twitterCardProductData.getData());
            jsBean.setLabel(twitterCardProductData.getLabel());
        }
        return jsBean;
    }

    public static TwitterCardProductData convertTwitterCardProductDataJsBeanToBean(TwitterCardProductDataJsBean jsBean)
    {
        TwitterCardProductDataBean twitterCardProductData = null;
        if(jsBean != null) {
            twitterCardProductData = new TwitterCardProductDataBean();
            twitterCardProductData.setData(jsBean.getData());
            twitterCardProductData.setLabel(jsBean.getLabel());
        }
        return twitterCardProductData;
    }

}
