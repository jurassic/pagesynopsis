package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgGeoPointStructJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgGeoPointStructWebService // implements OgGeoPointStructService
{
    private static final Logger log = Logger.getLogger(OgGeoPointStructWebService.class.getName());
     
    public static OgGeoPointStructJsBean convertOgGeoPointStructToJsBean(OgGeoPointStruct ogGeoPointStruct)
    {
        OgGeoPointStructJsBean jsBean = null;
        if(ogGeoPointStruct != null) {
            jsBean = new OgGeoPointStructJsBean();
            jsBean.setUuid(ogGeoPointStruct.getUuid());
            jsBean.setLatitude(ogGeoPointStruct.getLatitude());
            jsBean.setLongitude(ogGeoPointStruct.getLongitude());
            jsBean.setAltitude(ogGeoPointStruct.getAltitude());
        }
        return jsBean;
    }

    public static OgGeoPointStruct convertOgGeoPointStructJsBeanToBean(OgGeoPointStructJsBean jsBean)
    {
        OgGeoPointStructBean ogGeoPointStruct = null;
        if(jsBean != null) {
            ogGeoPointStruct = new OgGeoPointStructBean();
            ogGeoPointStruct.setUuid(jsBean.getUuid());
            ogGeoPointStruct.setLatitude(jsBean.getLatitude());
            ogGeoPointStruct.setLongitude(jsBean.getLongitude());
            ogGeoPointStruct.setAltitude(jsBean.getAltitude());
        }
        return ogGeoPointStruct;
    }

}
