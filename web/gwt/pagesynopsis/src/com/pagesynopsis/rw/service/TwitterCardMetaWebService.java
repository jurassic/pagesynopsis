package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.TwitterCardMetaJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterCardMetaWebService // implements TwitterCardMetaService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaWebService.class.getName());
     
    // Af service interface.
    private TwitterCardMetaService mService = null;

    public TwitterCardMetaWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterCardMetaServiceProxy());
    }
    public TwitterCardMetaWebService(TwitterCardMetaService service)
    {
        mService = service;
    }
    
    private TwitterCardMetaService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterCardMetaServiceProxy();
        }
        return mService;
    }
    
    
    public TwitterCardMetaJsBean getTwitterCardMeta(String guid) throws WebException
    {
        try {
            TwitterCardMeta twitterCardMeta = getService().getTwitterCardMeta(guid);
            TwitterCardMetaJsBean bean = convertTwitterCardMetaToJsBean(twitterCardMeta);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterCardMeta(String guid, String field) throws WebException
    {
        try {
            return getService().getTwitterCardMeta(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterCardMetaJsBean> getTwitterCardMetas(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterCardMetaJsBean> jsBeans = new ArrayList<TwitterCardMetaJsBean>();
            List<TwitterCardMeta> twitterCardMetas = getService().getTwitterCardMetas(guids);
            if(twitterCardMetas != null) {
                for(TwitterCardMeta twitterCardMeta : twitterCardMetas) {
                    jsBeans.add(convertTwitterCardMetaToJsBean(twitterCardMeta));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterCardMetaJsBean> getAllTwitterCardMetas() throws WebException
    {
        return getAllTwitterCardMetas(null, null, null);
    }

    // @Deprecated
    public List<TwitterCardMetaJsBean> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    public List<TwitterCardMetaJsBean> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterCardMetaJsBean> jsBeans = new ArrayList<TwitterCardMetaJsBean>();
            List<TwitterCardMeta> twitterCardMetas = getService().getAllTwitterCardMetas(ordering, offset, count, forwardCursor);
            if(twitterCardMetas != null) {
                for(TwitterCardMeta twitterCardMeta : twitterCardMetas) {
                    jsBeans.add(convertTwitterCardMetaToJsBean(twitterCardMeta));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterCardMetaKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<TwitterCardMetaJsBean> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterCardMetas(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<TwitterCardMetaJsBean> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<TwitterCardMetaJsBean> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<TwitterCardMetaJsBean> jsBeans = new ArrayList<TwitterCardMetaJsBean>();
            List<TwitterCardMeta> twitterCardMetas = getService().findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(twitterCardMetas != null) {
                for(TwitterCardMeta twitterCardMeta : twitterCardMetas) {
                    jsBeans.add(convertTwitterCardMetaToJsBean(twitterCardMeta));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCardJsBean summaryCard, TwitterPhotoCardJsBean photoCard, TwitterGalleryCardJsBean galleryCard, TwitterAppCardJsBean appCard, TwitterPlayerCardJsBean playerCard, TwitterProductCardJsBean productCard) throws WebException
    {
        try {
            return getService().createTwitterCardMeta(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, TwitterSummaryCardWebService.convertTwitterSummaryCardJsBeanToBean(summaryCard), TwitterPhotoCardWebService.convertTwitterPhotoCardJsBeanToBean(photoCard), TwitterGalleryCardWebService.convertTwitterGalleryCardJsBeanToBean(galleryCard), TwitterAppCardWebService.convertTwitterAppCardJsBeanToBean(appCard), TwitterPlayerCardWebService.convertTwitterPlayerCardJsBeanToBean(playerCard), TwitterProductCardWebService.convertTwitterProductCardJsBeanToBean(productCard));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterCardMeta(TwitterCardMetaJsBean jsBean) throws WebException
    {
        try {
            TwitterCardMeta twitterCardMeta = convertTwitterCardMetaJsBeanToBean(jsBean);
            return getService().createTwitterCardMeta(twitterCardMeta);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterCardMetaJsBean constructTwitterCardMeta(TwitterCardMetaJsBean jsBean) throws WebException
    {
        try {
            TwitterCardMeta twitterCardMeta = convertTwitterCardMetaJsBeanToBean(jsBean);
            twitterCardMeta = getService().constructTwitterCardMeta(twitterCardMeta);
            jsBean = convertTwitterCardMetaToJsBean(twitterCardMeta);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCardJsBean summaryCard, TwitterPhotoCardJsBean photoCard, TwitterGalleryCardJsBean galleryCard, TwitterAppCardJsBean appCard, TwitterPlayerCardJsBean playerCard, TwitterProductCardJsBean productCard) throws WebException
    {
        try {
            return getService().updateTwitterCardMeta(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, TwitterSummaryCardWebService.convertTwitterSummaryCardJsBeanToBean(summaryCard), TwitterPhotoCardWebService.convertTwitterPhotoCardJsBeanToBean(photoCard), TwitterGalleryCardWebService.convertTwitterGalleryCardJsBeanToBean(galleryCard), TwitterAppCardWebService.convertTwitterAppCardJsBeanToBean(appCard), TwitterPlayerCardWebService.convertTwitterPlayerCardJsBeanToBean(playerCard), TwitterProductCardWebService.convertTwitterProductCardJsBeanToBean(productCard));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterCardMeta(TwitterCardMetaJsBean jsBean) throws WebException
    {
        try {
            TwitterCardMeta twitterCardMeta = convertTwitterCardMetaJsBeanToBean(jsBean);
            return getService().updateTwitterCardMeta(twitterCardMeta);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterCardMetaJsBean refreshTwitterCardMeta(TwitterCardMetaJsBean jsBean) throws WebException
    {
        try {
            TwitterCardMeta twitterCardMeta = convertTwitterCardMetaJsBeanToBean(jsBean);
            twitterCardMeta = getService().refreshTwitterCardMeta(twitterCardMeta);
            jsBean = convertTwitterCardMetaToJsBean(twitterCardMeta);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterCardMeta(String guid) throws WebException
    {
        try {
            return getService().deleteTwitterCardMeta(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterCardMeta(TwitterCardMetaJsBean jsBean) throws WebException
    {
        try {
            TwitterCardMeta twitterCardMeta = convertTwitterCardMetaJsBeanToBean(jsBean);
            return getService().deleteTwitterCardMeta(twitterCardMeta);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteTwitterCardMetas(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterCardMetaJsBean convertTwitterCardMetaToJsBean(TwitterCardMeta twitterCardMeta)
    {
        TwitterCardMetaJsBean jsBean = null;
        if(twitterCardMeta != null) {
            jsBean = new TwitterCardMetaJsBean();
            jsBean.setGuid(twitterCardMeta.getGuid());
            jsBean.setUser(twitterCardMeta.getUser());
            jsBean.setFetchRequest(twitterCardMeta.getFetchRequest());
            jsBean.setTargetUrl(twitterCardMeta.getTargetUrl());
            jsBean.setPageUrl(twitterCardMeta.getPageUrl());
            jsBean.setQueryString(twitterCardMeta.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = twitterCardMeta.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(twitterCardMeta.getLastFetchResult());
            jsBean.setResponseCode(twitterCardMeta.getResponseCode());
            jsBean.setContentType(twitterCardMeta.getContentType());
            jsBean.setContentLength(twitterCardMeta.getContentLength());
            jsBean.setLanguage(twitterCardMeta.getLanguage());
            jsBean.setRedirect(twitterCardMeta.getRedirect());
            jsBean.setLocation(twitterCardMeta.getLocation());
            jsBean.setPageTitle(twitterCardMeta.getPageTitle());
            jsBean.setNote(twitterCardMeta.getNote());
            jsBean.setDeferred(twitterCardMeta.isDeferred());
            jsBean.setStatus(twitterCardMeta.getStatus());
            jsBean.setRefreshStatus(twitterCardMeta.getRefreshStatus());
            jsBean.setRefreshInterval(twitterCardMeta.getRefreshInterval());
            jsBean.setNextRefreshTime(twitterCardMeta.getNextRefreshTime());
            jsBean.setLastCheckedTime(twitterCardMeta.getLastCheckedTime());
            jsBean.setLastUpdatedTime(twitterCardMeta.getLastUpdatedTime());
            jsBean.setCardType(twitterCardMeta.getCardType());
            jsBean.setSummaryCard(TwitterSummaryCardWebService.convertTwitterSummaryCardToJsBean(twitterCardMeta.getSummaryCard()));
            jsBean.setPhotoCard(TwitterPhotoCardWebService.convertTwitterPhotoCardToJsBean(twitterCardMeta.getPhotoCard()));
            jsBean.setGalleryCard(TwitterGalleryCardWebService.convertTwitterGalleryCardToJsBean(twitterCardMeta.getGalleryCard()));
            jsBean.setAppCard(TwitterAppCardWebService.convertTwitterAppCardToJsBean(twitterCardMeta.getAppCard()));
            jsBean.setPlayerCard(TwitterPlayerCardWebService.convertTwitterPlayerCardToJsBean(twitterCardMeta.getPlayerCard()));
            jsBean.setProductCard(TwitterProductCardWebService.convertTwitterProductCardToJsBean(twitterCardMeta.getProductCard()));
            jsBean.setCreatedTime(twitterCardMeta.getCreatedTime());
            jsBean.setModifiedTime(twitterCardMeta.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterCardMeta convertTwitterCardMetaJsBeanToBean(TwitterCardMetaJsBean jsBean)
    {
        TwitterCardMetaBean twitterCardMeta = null;
        if(jsBean != null) {
            twitterCardMeta = new TwitterCardMetaBean();
            twitterCardMeta.setGuid(jsBean.getGuid());
            twitterCardMeta.setUser(jsBean.getUser());
            twitterCardMeta.setFetchRequest(jsBean.getFetchRequest());
            twitterCardMeta.setTargetUrl(jsBean.getTargetUrl());
            twitterCardMeta.setPageUrl(jsBean.getPageUrl());
            twitterCardMeta.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            twitterCardMeta.setQueryParams(queryParamsBeans);
            twitterCardMeta.setLastFetchResult(jsBean.getLastFetchResult());
            twitterCardMeta.setResponseCode(jsBean.getResponseCode());
            twitterCardMeta.setContentType(jsBean.getContentType());
            twitterCardMeta.setContentLength(jsBean.getContentLength());
            twitterCardMeta.setLanguage(jsBean.getLanguage());
            twitterCardMeta.setRedirect(jsBean.getRedirect());
            twitterCardMeta.setLocation(jsBean.getLocation());
            twitterCardMeta.setPageTitle(jsBean.getPageTitle());
            twitterCardMeta.setNote(jsBean.getNote());
            twitterCardMeta.setDeferred(jsBean.isDeferred());
            twitterCardMeta.setStatus(jsBean.getStatus());
            twitterCardMeta.setRefreshStatus(jsBean.getRefreshStatus());
            twitterCardMeta.setRefreshInterval(jsBean.getRefreshInterval());
            twitterCardMeta.setNextRefreshTime(jsBean.getNextRefreshTime());
            twitterCardMeta.setLastCheckedTime(jsBean.getLastCheckedTime());
            twitterCardMeta.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            twitterCardMeta.setCardType(jsBean.getCardType());
            twitterCardMeta.setSummaryCard(TwitterSummaryCardWebService.convertTwitterSummaryCardJsBeanToBean(jsBean.getSummaryCard()));
            twitterCardMeta.setPhotoCard(TwitterPhotoCardWebService.convertTwitterPhotoCardJsBeanToBean(jsBean.getPhotoCard()));
            twitterCardMeta.setGalleryCard(TwitterGalleryCardWebService.convertTwitterGalleryCardJsBeanToBean(jsBean.getGalleryCard()));
            twitterCardMeta.setAppCard(TwitterAppCardWebService.convertTwitterAppCardJsBeanToBean(jsBean.getAppCard()));
            twitterCardMeta.setPlayerCard(TwitterPlayerCardWebService.convertTwitterPlayerCardJsBeanToBean(jsBean.getPlayerCard()));
            twitterCardMeta.setProductCard(TwitterProductCardWebService.convertTwitterProductCardJsBeanToBean(jsBean.getProductCard()));
            twitterCardMeta.setCreatedTime(jsBean.getCreatedTime());
            twitterCardMeta.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterCardMeta;
    }

}
