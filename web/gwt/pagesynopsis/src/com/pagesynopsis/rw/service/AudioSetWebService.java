package com.pagesynopsis.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.fe.bean.TwitterProductCardJsBean;
import com.pagesynopsis.fe.bean.TwitterSummaryCardJsBean;
import com.pagesynopsis.fe.bean.OgBlogJsBean;
import com.pagesynopsis.fe.bean.TwitterPlayerCardJsBean;
import com.pagesynopsis.fe.bean.UrlStructJsBean;
import com.pagesynopsis.fe.bean.ImageStructJsBean;
import com.pagesynopsis.fe.bean.TwitterGalleryCardJsBean;
import com.pagesynopsis.fe.bean.TwitterPhotoCardJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.fe.bean.OgBookJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.fe.bean.TwitterAppCardJsBean;
import com.pagesynopsis.fe.bean.AnchorStructJsBean;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.OgArticleJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.fe.bean.AudioStructJsBean;
import com.pagesynopsis.fe.bean.VideoStructJsBean;
import com.pagesynopsis.fe.bean.OgProfileJsBean;
import com.pagesynopsis.fe.bean.AudioSetJsBean;
import com.pagesynopsis.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AudioSetWebService // implements AudioSetService
{
    private static final Logger log = Logger.getLogger(AudioSetWebService.class.getName());
     
    // Af service interface.
    private AudioSetService mService = null;

    public AudioSetWebService()
    {
        this(ServiceProxyFactory.getInstance().getAudioSetServiceProxy());
    }
    public AudioSetWebService(AudioSetService service)
    {
        mService = service;
    }
    
    private AudioSetService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getAudioSetServiceProxy();
        }
        return mService;
    }
    
    
    public AudioSetJsBean getAudioSet(String guid) throws WebException
    {
        try {
            AudioSet audioSet = getService().getAudioSet(guid);
            AudioSetJsBean bean = convertAudioSetToJsBean(audioSet);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAudioSet(String guid, String field) throws WebException
    {
        try {
            return getService().getAudioSet(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AudioSetJsBean> getAudioSets(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AudioSetJsBean> jsBeans = new ArrayList<AudioSetJsBean>();
            List<AudioSet> audioSets = getService().getAudioSets(guids);
            if(audioSets != null) {
                for(AudioSet audioSet : audioSets) {
                    jsBeans.add(convertAudioSetToJsBean(audioSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AudioSetJsBean> getAllAudioSets() throws WebException
    {
        return getAllAudioSets(null, null, null);
    }

    // @Deprecated
    public List<AudioSetJsBean> getAllAudioSets(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAudioSets(ordering, offset, count, null);
    }

    public List<AudioSetJsBean> getAllAudioSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<AudioSetJsBean> jsBeans = new ArrayList<AudioSetJsBean>();
            List<AudioSet> audioSets = getService().getAllAudioSets(ordering, offset, count, forwardCursor);
            if(audioSets != null) {
                for(AudioSet audioSet : audioSets) {
                    jsBeans.add(convertAudioSetToJsBean(audioSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllAudioSetKeys(ordering, offset, count, null);
    }

    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAudioSetKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<AudioSetJsBean> findAudioSets(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAudioSets(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<AudioSetJsBean> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<AudioSetJsBean> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        try {
            List<AudioSetJsBean> jsBeans = new ArrayList<AudioSetJsBean>();
            List<AudioSet> audioSets = getService().findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(audioSets != null) {
                for(AudioSet audioSet : audioSets) {
                    jsBeans.add(convertAudioSetToJsBean(audioSet));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAudioSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios) throws WebException
    {
        try {
            return getService().createAudioSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAudioSet(AudioSetJsBean jsBean) throws WebException
    {
        try {
            AudioSet audioSet = convertAudioSetJsBeanToBean(jsBean);
            return getService().createAudioSet(audioSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AudioSetJsBean constructAudioSet(AudioSetJsBean jsBean) throws WebException
    {
        try {
            AudioSet audioSet = convertAudioSetJsBeanToBean(jsBean);
            audioSet = getService().constructAudioSet(audioSet);
            jsBean = convertAudioSetToJsBean(audioSet);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAudioSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios) throws WebException
    {
        try {
            return getService().updateAudioSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAudioSet(AudioSetJsBean jsBean) throws WebException
    {
        try {
            AudioSet audioSet = convertAudioSetJsBeanToBean(jsBean);
            return getService().updateAudioSet(audioSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AudioSetJsBean refreshAudioSet(AudioSetJsBean jsBean) throws WebException
    {
        try {
            AudioSet audioSet = convertAudioSetJsBeanToBean(jsBean);
            audioSet = getService().refreshAudioSet(audioSet);
            jsBean = convertAudioSetToJsBean(audioSet);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAudioSet(String guid) throws WebException
    {
        try {
            return getService().deleteAudioSet(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAudioSet(AudioSetJsBean jsBean) throws WebException
    {
        try {
            AudioSet audioSet = convertAudioSetJsBeanToBean(jsBean);
            return getService().deleteAudioSet(audioSet);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAudioSets(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteAudioSets(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static AudioSetJsBean convertAudioSetToJsBean(AudioSet audioSet)
    {
        AudioSetJsBean jsBean = null;
        if(audioSet != null) {
            jsBean = new AudioSetJsBean();
            jsBean.setGuid(audioSet.getGuid());
            jsBean.setUser(audioSet.getUser());
            jsBean.setFetchRequest(audioSet.getFetchRequest());
            jsBean.setTargetUrl(audioSet.getTargetUrl());
            jsBean.setPageUrl(audioSet.getPageUrl());
            jsBean.setQueryString(audioSet.getQueryString());
            List<KeyValuePairStructJsBean> queryParamsJsBeans = new ArrayList<KeyValuePairStructJsBean>();
            List<KeyValuePairStruct> queryParamsBeans = audioSet.getQueryParams();
            if(queryParamsBeans != null) {
                for(KeyValuePairStruct keyValuePairStruct : queryParamsBeans) {
                    KeyValuePairStructJsBean jB = KeyValuePairStructWebService.convertKeyValuePairStructToJsBean(keyValuePairStruct);
                    queryParamsJsBeans.add(jB); 
                }
            }
            jsBean.setQueryParams(queryParamsJsBeans);
            jsBean.setLastFetchResult(audioSet.getLastFetchResult());
            jsBean.setResponseCode(audioSet.getResponseCode());
            jsBean.setContentType(audioSet.getContentType());
            jsBean.setContentLength(audioSet.getContentLength());
            jsBean.setLanguage(audioSet.getLanguage());
            jsBean.setRedirect(audioSet.getRedirect());
            jsBean.setLocation(audioSet.getLocation());
            jsBean.setPageTitle(audioSet.getPageTitle());
            jsBean.setNote(audioSet.getNote());
            jsBean.setDeferred(audioSet.isDeferred());
            jsBean.setStatus(audioSet.getStatus());
            jsBean.setRefreshStatus(audioSet.getRefreshStatus());
            jsBean.setRefreshInterval(audioSet.getRefreshInterval());
            jsBean.setNextRefreshTime(audioSet.getNextRefreshTime());
            jsBean.setLastCheckedTime(audioSet.getLastCheckedTime());
            jsBean.setLastUpdatedTime(audioSet.getLastUpdatedTime());
            jsBean.setMediaTypeFilter(audioSet.getMediaTypeFilter());
            Set<AudioStructJsBean> pageAudiosJsBeans = new HashSet<AudioStructJsBean>();
            Set<AudioStruct> pageAudiosBeans = audioSet.getPageAudios();
            if(pageAudiosBeans != null) {
                for(AudioStruct audioStruct : pageAudiosBeans) {
                    AudioStructJsBean jB = AudioStructWebService.convertAudioStructToJsBean(audioStruct);
                    pageAudiosJsBeans.add(jB); 
                }
            }
            jsBean.setPageAudios(pageAudiosJsBeans);
            jsBean.setCreatedTime(audioSet.getCreatedTime());
            jsBean.setModifiedTime(audioSet.getModifiedTime());
        }
        return jsBean;
    }

    public static AudioSet convertAudioSetJsBeanToBean(AudioSetJsBean jsBean)
    {
        AudioSetBean audioSet = null;
        if(jsBean != null) {
            audioSet = new AudioSetBean();
            audioSet.setGuid(jsBean.getGuid());
            audioSet.setUser(jsBean.getUser());
            audioSet.setFetchRequest(jsBean.getFetchRequest());
            audioSet.setTargetUrl(jsBean.getTargetUrl());
            audioSet.setPageUrl(jsBean.getPageUrl());
            audioSet.setQueryString(jsBean.getQueryString());
            List<KeyValuePairStruct> queryParamsBeans = new ArrayList<KeyValuePairStruct>();
            List<KeyValuePairStructJsBean> queryParamsJsBeans = jsBean.getQueryParams();
            if(queryParamsJsBeans != null) {
                for(KeyValuePairStructJsBean keyValuePairStruct : queryParamsJsBeans) {
                    KeyValuePairStruct b = KeyValuePairStructWebService.convertKeyValuePairStructJsBeanToBean(keyValuePairStruct);
                    queryParamsBeans.add(b); 
                }
            }
            audioSet.setQueryParams(queryParamsBeans);
            audioSet.setLastFetchResult(jsBean.getLastFetchResult());
            audioSet.setResponseCode(jsBean.getResponseCode());
            audioSet.setContentType(jsBean.getContentType());
            audioSet.setContentLength(jsBean.getContentLength());
            audioSet.setLanguage(jsBean.getLanguage());
            audioSet.setRedirect(jsBean.getRedirect());
            audioSet.setLocation(jsBean.getLocation());
            audioSet.setPageTitle(jsBean.getPageTitle());
            audioSet.setNote(jsBean.getNote());
            audioSet.setDeferred(jsBean.isDeferred());
            audioSet.setStatus(jsBean.getStatus());
            audioSet.setRefreshStatus(jsBean.getRefreshStatus());
            audioSet.setRefreshInterval(jsBean.getRefreshInterval());
            audioSet.setNextRefreshTime(jsBean.getNextRefreshTime());
            audioSet.setLastCheckedTime(jsBean.getLastCheckedTime());
            audioSet.setLastUpdatedTime(jsBean.getLastUpdatedTime());
            audioSet.setMediaTypeFilter(jsBean.getMediaTypeFilter());
            Set<AudioStruct> pageAudiosBeans = new HashSet<AudioStruct>();
            Set<AudioStructJsBean> pageAudiosJsBeans = jsBean.getPageAudios();
            if(pageAudiosJsBeans != null) {
                for(AudioStructJsBean audioStruct : pageAudiosJsBeans) {
                    AudioStruct b = AudioStructWebService.convertAudioStructJsBeanToBean(audioStruct);
                    pageAudiosBeans.add(b); 
                }
            }
            audioSet.setPageAudios(pageAudiosBeans);
            audioSet.setCreatedTime(jsBean.getCreatedTime());
            audioSet.setModifiedTime(jsBean.getModifiedTime());
        }
        return audioSet;
    }

}
