//////////////////////////////////////////
// Note: Need to authenticate through --oauth2 first
//       Otherwise, it'll prompt for password, and we have no way to input it from the script...
//////////////////////////////////////////

var fs = require("fs");
var path = require("path");
// var child_process = require("child_process");
var exec = require("child_process").exec;

// temporary
var adminEmail = "yinventor@gmail.com";

var gaeSdkBinDir = "C:\\Apps\\appengine-java-sdk-1.8.2\\bin";
// var gaeSdkBinDir = "C:\\Apps\\appengine-java-sdk-1.7.7.1\\bin";
// var gaeSdkBinDir = "C:\\Apps\\appengine-java-sdk-1.7.6\\bin";
// var gaeSdkBinDir = "C:\\Apps\\appengine-java-sdk-1.7.5\\bin";
var warDir = "..\\war";
var webInfDir = warDir + "\\WEB-INF";
// var webInfDir = "..\\tmp";
fs.exists(webInfDir, function(exists) {
    if(exists) {
        /*
        fs.readdir(webInfDir, function(err, files) {
            var fileList = [];

            // [1] Use all appengine files in the dir
            if(files) {
                files.forEach(function(f) {
                    if(path.extname(f) == ".xml" 
                        && f.indexOf("appengine-web") == 0 
                        && ( f.indexOf("-alpha.xml") == -1 && f.indexOf("-saved.xml") == -1 && f.indexOf("-tmp.xml") == -1 && f.indexOf("-tbd.xml") == -1 )
                        && f != "appengine-web.xml") {
                        console.log("appengine-web xml file: f = " + f);
                        fileList.push(f);
                    }
                });
            }
            // [2] Or, hardcode the list
            // fileList = [...];
            // ....

            // Deploy the projects one at a time...
            deployProject(fileList, 0); 
        });
        */
        
        var fileList = [];
        
// fileList.push("appengine-web-glassappfinder-beta.xml");
// fileList.push("appengine-web-glassappfinder.xml");
// fileList.push("appengine-web-glassappstudio-beta.xml");
// fileList.push("appengine-web-glassappstudio.xml");
// fileList.push("appengine-web-glassbeamer-beta.xml");
// fileList.push("appengine-web-glassbeamer.xml");
// fileList.push("appengine-web-glassbookmark-beta.xml");
// fileList.push("appengine-web-glassbookmark.xml");
// fileList.push("appengine-web-glasschatter-beta.xml");
// fileList.push("appengine-web-glasschatter.xml");
// fileList.push("appengine-web-glasschest-beta.xml");
// fileList.push("appengine-web-glasschest.xml");
// fileList.push("appengine-web-glasscollage-beta.xml");
// fileList.push("appengine-web-glasscollage.xml");
// fileList.push("appengine-web-glasscue-beta.xml");
// fileList.push("appengine-web-glasscue.xml");
// fileList.push("appengine-web-glasscurrent-beta.xml");
// fileList.push("appengine-web-glasscurrent.xml");
// fileList.push("appengine-web-glassdiary-beta.xml");
// fileList.push("appengine-web-glassdiary.xml");
// fileList.push("appengine-web-glassflashcard-beta.xml");
// fileList.push("appengine-web-glassflashcard.xml");
// fileList.push("appengine-web-glassflasher-beta.xml");
// fileList.push("appengine-web-glassflasher.xml");
// fileList.push("appengine-web-glassfolder-beta.xml");
// fileList.push("appengine-web-glassfolder.xml");
// fileList.push("appengine-web-glassfortune-beta.xml");
// fileList.push("appengine-web-glassfortune.xml");
// fileList.push("appengine-web-glassfriday-beta.xml");
// fileList.push("appengine-web-glassfriday.xml");
// fileList.push("appengine-web-glasslane-beta.xml");
// fileList.push("appengine-web-glasslane.xml");
// fileList.push("appengine-web-glasslenz-beta.xml");
// fileList.push("appengine-web-glasslenz.xml");
// fileList.push("appengine-web-glassmemo-beta.xml");
// fileList.push("appengine-web-glassmemo.xml");
// fileList.push("appengine-web-glasspager-beta.xml");
// fileList.push("appengine-web-glasspager.xml");
// fileList.push("appengine-web-glasspictorial-beta.xml");
// fileList.push("appengine-web-glasspictorial.xml");
// fileList.push("appengine-web-glassplugin-beta.xml");
// fileList.push("appengine-web-glassplugin.xml");
// fileList.push("appengine-web-glasspoll-beta.xml");
// fileList.push("appengine-web-glasspoll.xml");
// fileList.push("appengine-web-glasspuzzle-beta.xml");
fileList.push("appengine-web-glasspuzzle.xml");
// fileList.push("appengine-web-glassqueue-beta.xml");
// fileList.push("appengine-web-glassqueue.xml");
// fileList.push("appengine-web-glasstalkie-beta.xml");
// fileList.push("appengine-web-glasstalkie.xml");
// fileList.push("appengine-web-glasstally-beta.xml");
// fileList.push("appengine-web-glasstally.xml");
// fileList.push("appengine-web-glassterm-beta.xml");
// fileList.push("appengine-web-glassterm.xml");
// fileList.push("appengine-web-glasstexter-beta.xml");
// fileList.push("appengine-web-glasstexter.xml");
// fileList.push("appengine-web-glasstimeline-beta.xml");
// fileList.push("appengine-web-glasstimeline.xml");
// fileList.push("appengine-web-glasstodo-beta.xml");
// fileList.push("appengine-web-glasstodo.xml");
// fileList.push("appengine-web-glassvitals-beta.xml");
// fileList.push("appengine-web-glassvitals.xml");
// fileList.push("appengine-web-glasswebservice-beta.xml");
// fileList.push("appengine-web-glasswebservice.xml");
// fileList.push("appengine-web-magglass-beta.xml");
// fileList.push("appengine-web-magglass.xml");
// fileList.push("appengine-web-navglass-beta.xml");
// fileList.push("appengine-web-navglass.xml");
// fileList.push("appengine-web-tellyglass-beta.xml");
// fileList.push("appengine-web-tellyglass.xml");

        deployProject(fileList, 0); 

    } else {
        console.log("The directory, " + webInfDir + ", does not exist!");
    }
});


// Recursive - async calls...
var deployProject = function(fileList, idx) 
{
    if(fileList) {
        var len = fileList.length;
        if(idx < 0 || idx >= len) {
            return;
        }

        var f = fileList[idx];
        var srcFile = webInfDir + '\\' + f;
        var destFile = webInfDir + '\\appengine-web.xml';
        console.log("Start processing: " + idx + ": " + f);

        // var that = this;
        var copyCmd = 'copy ' + srcFile + ' ' + destFile;
        var copyProc = exec(copyCmd, function(copyError, copyStdout, copyStderr) {
            console.log('copyStdout: ' + copyStdout);
            console.log('copyStderr: ' + copyStderr);
            if (copyError !== null) {
                console.log('Copy exec error: ' + copyError);
                return;
            } else {
                // var deployCmd = 'dir ' + webInfDir;    // for testing...
                // var deployCmd = gaeSdkBinDir + '\\appcfg --email=" + adminEmail + " update ' + warDir;
                var deployCmd = gaeSdkBinDir + '\\appcfg --oauth2 --use_java7 --enable_jar_splitting update ' + warDir;
                var deployProc = exec(deployCmd, function(deployError, deployStdout, deployStderr) {
                    console.log('deployStdout: ' + deployStdout);
                    console.log('deployStderr: ' + deployStderr);
                    if (deployError !== null) {
                        console.log('Deploy exec error: ' + deployError);
                        return;
                    } else {
                        console.log('Sucessfully deployed: f = ' + f);
                        deployProject(fileList, idx + 1);
                        return;
                    }
                });
            }
        });
    } else {
        console.log('fileList is empty/null.');
        return;
    }
};

