package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "twitterCardMetas")
@XmlType(propOrder = {"twitterCardMeta", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterCardMetaListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardMetaListStub.class.getName());

    private List<TwitterCardMetaStub> twitterCardMetas = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TwitterCardMetaListStub()
    {
        this(new ArrayList<TwitterCardMetaStub>());
    }
    public TwitterCardMetaListStub(List<TwitterCardMetaStub> twitterCardMetas)
    {
        this(twitterCardMetas, null);
    }
    public TwitterCardMetaListStub(List<TwitterCardMetaStub> twitterCardMetas, String forwardCursor)
    {
        this.twitterCardMetas = twitterCardMetas;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(twitterCardMetas == null) {
            return true;
        } else {
            return twitterCardMetas.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterCardMetas == null) {
            return 0;
        } else {
            return twitterCardMetas.size();
        }
    }


    @XmlElement(name = "twitterCardMeta")
    public List<TwitterCardMetaStub> getTwitterCardMeta()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterCardMetaStub> getList()
    {
        return twitterCardMetas;
    }
    public void setList(List<TwitterCardMetaStub> twitterCardMetas)
    {
        this.twitterCardMetas = twitterCardMetas;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TwitterCardMetaStub> it = this.twitterCardMetas.iterator();
        while(it.hasNext()) {
            TwitterCardMetaStub twitterCardMeta = it.next();
            sb.append(twitterCardMeta.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterCardMetaListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterCardMetaListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardMetaListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardMetaListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardMetaListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterCardMetaListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterCardMetaListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterCardMetaListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardMetaListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardMetaListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardMetaListStub object.", e);
        }
        
        return null;
    }

}
