package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogTvEpisode")
@XmlType(propOrder = {"guid", "url", "type", "siteName", "title", "description", "fbAdmins", "fbAppId", "imageStub", "audioStub", "videoStub", "locale", "localeAlternate", "director", "writer", "actorStub", "series", "duration", "tag", "releaseDate", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgTvEpisodeStub extends OgObjectBaseStub implements OgTvEpisode, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgTvEpisodeStub.class.getName());

    private List<String> director;
    private List<String> writer;
    private List<OgActorStructStub> actor;
    private String series;
    private Integer duration;
    private List<String> tag;
    private String releaseDate;

    public OgTvEpisodeStub()
    {
        this(null);
    }
    public OgTvEpisodeStub(OgTvEpisode bean)
    {
        super(bean);
        if(bean != null) {
            this.director = bean.getDirector();
            this.writer = bean.getWriter();
            List<OgActorStructStub> _actorStubs = null;
            if(bean.getActor() != null) {
                _actorStubs = new ArrayList<OgActorStructStub>();
                for(OgActorStruct b : bean.getActor()) {
                    _actorStubs.add(OgActorStructStub.convertBeanToStub(b));
                }
            }
            this.actor = _actorStubs;
            this.series = bean.getSeries();
            this.duration = bean.getDuration();
            this.tag = bean.getTag();
            this.releaseDate = bean.getReleaseDate();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    @XmlElement
    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    @XmlElement
    public String getSiteName()
    {
        return super.getSiteName();
    }
    public void setSiteName(String siteName)
    {
        super.setSiteName(siteName);
    }

    @XmlElement
    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    @XmlElement
    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    @XmlElement
    public List<String> getFbAdmins()
    {
        return super.getFbAdmins();
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        super.setFbAdmins(fbAdmins);
    }

    @XmlElement
    public List<String> getFbAppId()
    {
        return super.getFbAppId();
    }
    public void setFbAppId(List<String> fbAppId)
    {
        super.setFbAppId(fbAppId);
    }

    @XmlElement(name = "image")
    @JsonIgnore
    public List<OgImageStructStub> getImageStub()
    {
        return super.getImageStub();
    }
    public void setImageStub(List<OgImageStructStub> image)
    {
        super.setImageStub(image);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgImageStructStub.class)
    public List<OgImageStruct> getImage()
    {  
        return super.getImage();
    }
    public void setImage(List<OgImageStruct> image)
    {
        super.setImage(image);
    }

    @XmlElement(name = "audio")
    @JsonIgnore
    public List<OgAudioStructStub> getAudioStub()
    {
        return super.getAudioStub();
    }
    public void setAudioStub(List<OgAudioStructStub> audio)
    {
        super.setAudioStub(audio);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgAudioStructStub.class)
    public List<OgAudioStruct> getAudio()
    {  
        return super.getAudio();
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        super.setAudio(audio);
    }

    @XmlElement(name = "video")
    @JsonIgnore
    public List<OgVideoStructStub> getVideoStub()
    {
        return super.getVideoStub();
    }
    public void setVideoStub(List<OgVideoStructStub> video)
    {
        super.setVideoStub(video);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgVideoStructStub.class)
    public List<OgVideoStruct> getVideo()
    {  
        return super.getVideo();
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        super.setVideo(video);
    }

    @XmlElement
    public String getLocale()
    {
        return super.getLocale();
    }
    public void setLocale(String locale)
    {
        super.setLocale(locale);
    }

    @XmlElement
    public List<String> getLocaleAlternate()
    {
        return super.getLocaleAlternate();
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        super.setLocaleAlternate(localeAlternate);
    }

    @XmlElement
    public List<String> getDirector()
    {
        return this.director;
    }
    public void setDirector(List<String> director)
    {
        this.director = director;
    }

    @XmlElement
    public List<String> getWriter()
    {
        return this.writer;
    }
    public void setWriter(List<String> writer)
    {
        this.writer = writer;
    }

    @XmlElement(name = "actor")
    @JsonIgnore
    public List<OgActorStructStub> getActorStub()
    {
        return this.actor;
    }
    public void setActorStub(List<OgActorStructStub> actor)
    {
        this.actor = actor;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgActorStructStub.class)
    public List<OgActorStruct> getActor()
    {  
        return (List<OgActorStruct>) ((List<?>) getActorStub());
    }
    public void setActor(List<OgActorStruct> actor)
    {
        // TBD
        //if(actor instanceof List<OgActorStructStub>) {
            setActorStub((List<OgActorStructStub>) ((List<?>) actor));
        //} else {
        //    // TBD
        //    List<OgActorStructStub> _OgActorStructList = new ArrayList<OgActorStructStub>(); // ???
        //    for(OgActorStruct b : actor) {
        //        _OgActorStructList.add(OgActorStructStub.convertBeanToStub(actor));
        //    }
        //    setActorStub(_OgActorStructList);
        //}
    }

    @XmlElement
    public String getSeries()
    {
        return this.series;
    }
    public void setSeries(String series)
    {
        this.series = series;
    }

    @XmlElement
    public Integer getDuration()
    {
        return this.duration;
    }
    public void setDuration(Integer duration)
    {
        this.duration = duration;
    }

    @XmlElement
    public List<String> getTag()
    {
        return this.tag;
    }
    public void setTag(List<String> tag)
    {
        this.tag = tag;
    }

    @XmlElement
    public String getReleaseDate()
    {
        return this.releaseDate;
    }
    public void setReleaseDate(String releaseDate)
    {
        this.releaseDate = releaseDate;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("director", this.director);
        dataMap.put("writer", this.writer);
        dataMap.put("actor", this.actor);
        dataMap.put("series", this.series);
        dataMap.put("duration", this.duration);
        dataMap.put("tag", this.tag);
        dataMap.put("releaseDate", this.releaseDate);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = director == null ? 0 : director.hashCode();
        _hash = 31 * _hash + delta;
        delta = writer == null ? 0 : writer.hashCode();
        _hash = 31 * _hash + delta;
        delta = actor == null ? 0 : actor.hashCode();
        _hash = 31 * _hash + delta;
        delta = series == null ? 0 : series.hashCode();
        _hash = 31 * _hash + delta;
        delta = duration == null ? 0 : duration.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = releaseDate == null ? 0 : releaseDate.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static OgTvEpisodeStub convertBeanToStub(OgTvEpisode bean)
    {
        OgTvEpisodeStub stub = null;
        if(bean instanceof OgTvEpisodeStub) {
            stub = (OgTvEpisodeStub) bean;
        } else {
            if(bean != null) {
                stub = new OgTvEpisodeStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgTvEpisodeStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgTvEpisodeStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgTvEpisodeStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgTvEpisodeStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgTvEpisodeStub object as a string.", e);
        }
        
        return null;
    }
    public static OgTvEpisodeStub fromJsonString(String jsonStr)
    {
        try {
            OgTvEpisodeStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgTvEpisodeStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvEpisodeStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvEpisodeStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvEpisodeStub object.", e);
        }
        
        return null;
    }

}
