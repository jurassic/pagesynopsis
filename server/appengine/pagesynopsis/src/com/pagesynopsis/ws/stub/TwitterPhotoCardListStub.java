package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "twitterPhotoCards")
@XmlType(propOrder = {"twitterPhotoCard", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterPhotoCardListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterPhotoCardListStub.class.getName());

    private List<TwitterPhotoCardStub> twitterPhotoCards = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TwitterPhotoCardListStub()
    {
        this(new ArrayList<TwitterPhotoCardStub>());
    }
    public TwitterPhotoCardListStub(List<TwitterPhotoCardStub> twitterPhotoCards)
    {
        this(twitterPhotoCards, null);
    }
    public TwitterPhotoCardListStub(List<TwitterPhotoCardStub> twitterPhotoCards, String forwardCursor)
    {
        this.twitterPhotoCards = twitterPhotoCards;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(twitterPhotoCards == null) {
            return true;
        } else {
            return twitterPhotoCards.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterPhotoCards == null) {
            return 0;
        } else {
            return twitterPhotoCards.size();
        }
    }


    @XmlElement(name = "twitterPhotoCard")
    public List<TwitterPhotoCardStub> getTwitterPhotoCard()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterPhotoCardStub> getList()
    {
        return twitterPhotoCards;
    }
    public void setList(List<TwitterPhotoCardStub> twitterPhotoCards)
    {
        this.twitterPhotoCards = twitterPhotoCards;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TwitterPhotoCardStub> it = this.twitterPhotoCards.iterator();
        while(it.hasNext()) {
            TwitterPhotoCardStub twitterPhotoCard = it.next();
            sb.append(twitterPhotoCard.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterPhotoCardListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterPhotoCardListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterPhotoCardListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterPhotoCardListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterPhotoCardListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterPhotoCardListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterPhotoCardListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterPhotoCardListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPhotoCardListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPhotoCardListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPhotoCardListStub object.", e);
        }
        
        return null;
    }

}
