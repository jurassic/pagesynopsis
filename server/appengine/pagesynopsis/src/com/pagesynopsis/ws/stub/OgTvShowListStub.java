package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogTvShows")
@XmlType(propOrder = {"ogTvShow", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgTvShowListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgTvShowListStub.class.getName());

    private List<OgTvShowStub> ogTvShows = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgTvShowListStub()
    {
        this(new ArrayList<OgTvShowStub>());
    }
    public OgTvShowListStub(List<OgTvShowStub> ogTvShows)
    {
        this(ogTvShows, null);
    }
    public OgTvShowListStub(List<OgTvShowStub> ogTvShows, String forwardCursor)
    {
        this.ogTvShows = ogTvShows;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogTvShows == null) {
            return true;
        } else {
            return ogTvShows.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogTvShows == null) {
            return 0;
        } else {
            return ogTvShows.size();
        }
    }


    @XmlElement(name = "ogTvShow")
    public List<OgTvShowStub> getOgTvShow()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgTvShowStub> getList()
    {
        return ogTvShows;
    }
    public void setList(List<OgTvShowStub> ogTvShows)
    {
        this.ogTvShows = ogTvShows;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgTvShowStub> it = this.ogTvShows.iterator();
        while(it.hasNext()) {
            OgTvShowStub ogTvShow = it.next();
            sb.append(ogTvShow.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgTvShowListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgTvShowListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgTvShowListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgTvShowListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgTvShowListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgTvShowListStub fromJsonString(String jsonStr)
    {
        try {
            OgTvShowListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgTvShowListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvShowListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvShowListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgTvShowListStub object.", e);
        }
        
        return null;
    }

}
