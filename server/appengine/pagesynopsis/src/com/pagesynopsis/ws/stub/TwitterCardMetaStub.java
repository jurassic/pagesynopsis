package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "twitterCardMeta")
@XmlType(propOrder = {"guid", "user", "fetchRequest", "targetUrl", "pageUrl", "queryString", "queryParamsStub", "lastFetchResult", "responseCode", "contentType", "contentLength", "language", "redirect", "location", "pageTitle", "note", "deferred", "status", "refreshStatus", "refreshInterval", "nextRefreshTime", "lastCheckedTime", "lastUpdatedTime", "cardType", "summaryCardStub", "photoCardStub", "galleryCardStub", "appCardStub", "playerCardStub", "productCardStub", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterCardMetaStub extends PageBaseStub implements TwitterCardMeta, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardMetaStub.class.getName());

    private String cardType;
    private TwitterSummaryCardStub summaryCard;
    private TwitterPhotoCardStub photoCard;
    private TwitterGalleryCardStub galleryCard;
    private TwitterAppCardStub appCard;
    private TwitterPlayerCardStub playerCard;
    private TwitterProductCardStub productCard;

    public TwitterCardMetaStub()
    {
        this(null);
    }
    public TwitterCardMetaStub(TwitterCardMeta bean)
    {
        super(bean);
        if(bean != null) {
            this.cardType = bean.getCardType();
            this.summaryCard = TwitterSummaryCardStub.convertBeanToStub(bean.getSummaryCard());
            this.photoCard = TwitterPhotoCardStub.convertBeanToStub(bean.getPhotoCard());
            this.galleryCard = TwitterGalleryCardStub.convertBeanToStub(bean.getGalleryCard());
            this.appCard = TwitterAppCardStub.convertBeanToStub(bean.getAppCard());
            this.playerCard = TwitterPlayerCardStub.convertBeanToStub(bean.getPlayerCard());
            this.productCard = TwitterProductCardStub.convertBeanToStub(bean.getProductCard());
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    @XmlElement
    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    @XmlElement
    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    @XmlElement
    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    @XmlElement(name = "queryParams")
    @JsonIgnore
    public List<KeyValuePairStructStub> getQueryParamsStub()
    {
        return super.getQueryParamsStub();
    }
    public void setQueryParamsStub(List<KeyValuePairStructStub> queryParams)
    {
        super.setQueryParamsStub(queryParams);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=KeyValuePairStructStub.class)
    public List<KeyValuePairStruct> getQueryParams()
    {  
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    @XmlElement
    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    @XmlElement
    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    @XmlElement
    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    @XmlElement
    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    @XmlElement
    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    @XmlElement
    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    @XmlElement
    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    @XmlElement
    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    @XmlElement
    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    @XmlElement
    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    @XmlElement
    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    @XmlElement
    public String getCardType()
    {
        return this.cardType;
    }
    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    @XmlElement(name = "summaryCard")
    @JsonIgnore
    public TwitterSummaryCardStub getSummaryCardStub()
    {
        return this.summaryCard;
    }
    public void setSummaryCardStub(TwitterSummaryCardStub summaryCard)
    {
        this.summaryCard = summaryCard;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterSummaryCardStub.class)
    public TwitterSummaryCard getSummaryCard()
    {  
        return getSummaryCardStub();
    }
    public void setSummaryCard(TwitterSummaryCard summaryCard)
    {
        if((summaryCard == null) || (summaryCard instanceof TwitterSummaryCardStub)) {
            setSummaryCardStub((TwitterSummaryCardStub) summaryCard);
        } else {
            // TBD
            setSummaryCardStub(TwitterSummaryCardStub.convertBeanToStub(summaryCard));
        }
    }

    @XmlElement(name = "photoCard")
    @JsonIgnore
    public TwitterPhotoCardStub getPhotoCardStub()
    {
        return this.photoCard;
    }
    public void setPhotoCardStub(TwitterPhotoCardStub photoCard)
    {
        this.photoCard = photoCard;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterPhotoCardStub.class)
    public TwitterPhotoCard getPhotoCard()
    {  
        return getPhotoCardStub();
    }
    public void setPhotoCard(TwitterPhotoCard photoCard)
    {
        if((photoCard == null) || (photoCard instanceof TwitterPhotoCardStub)) {
            setPhotoCardStub((TwitterPhotoCardStub) photoCard);
        } else {
            // TBD
            setPhotoCardStub(TwitterPhotoCardStub.convertBeanToStub(photoCard));
        }
    }

    @XmlElement(name = "galleryCard")
    @JsonIgnore
    public TwitterGalleryCardStub getGalleryCardStub()
    {
        return this.galleryCard;
    }
    public void setGalleryCardStub(TwitterGalleryCardStub galleryCard)
    {
        this.galleryCard = galleryCard;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterGalleryCardStub.class)
    public TwitterGalleryCard getGalleryCard()
    {  
        return getGalleryCardStub();
    }
    public void setGalleryCard(TwitterGalleryCard galleryCard)
    {
        if((galleryCard == null) || (galleryCard instanceof TwitterGalleryCardStub)) {
            setGalleryCardStub((TwitterGalleryCardStub) galleryCard);
        } else {
            // TBD
            setGalleryCardStub(TwitterGalleryCardStub.convertBeanToStub(galleryCard));
        }
    }

    @XmlElement(name = "appCard")
    @JsonIgnore
    public TwitterAppCardStub getAppCardStub()
    {
        return this.appCard;
    }
    public void setAppCardStub(TwitterAppCardStub appCard)
    {
        this.appCard = appCard;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterAppCardStub.class)
    public TwitterAppCard getAppCard()
    {  
        return getAppCardStub();
    }
    public void setAppCard(TwitterAppCard appCard)
    {
        if((appCard == null) || (appCard instanceof TwitterAppCardStub)) {
            setAppCardStub((TwitterAppCardStub) appCard);
        } else {
            // TBD
            setAppCardStub(TwitterAppCardStub.convertBeanToStub(appCard));
        }
    }

    @XmlElement(name = "playerCard")
    @JsonIgnore
    public TwitterPlayerCardStub getPlayerCardStub()
    {
        return this.playerCard;
    }
    public void setPlayerCardStub(TwitterPlayerCardStub playerCard)
    {
        this.playerCard = playerCard;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterPlayerCardStub.class)
    public TwitterPlayerCard getPlayerCard()
    {  
        return getPlayerCardStub();
    }
    public void setPlayerCard(TwitterPlayerCard playerCard)
    {
        if((playerCard == null) || (playerCard instanceof TwitterPlayerCardStub)) {
            setPlayerCardStub((TwitterPlayerCardStub) playerCard);
        } else {
            // TBD
            setPlayerCardStub(TwitterPlayerCardStub.convertBeanToStub(playerCard));
        }
    }

    @XmlElement(name = "productCard")
    @JsonIgnore
    public TwitterProductCardStub getProductCardStub()
    {
        return this.productCard;
    }
    public void setProductCardStub(TwitterProductCardStub productCard)
    {
        this.productCard = productCard;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=TwitterProductCardStub.class)
    public TwitterProductCard getProductCard()
    {  
        return getProductCardStub();
    }
    public void setProductCard(TwitterProductCard productCard)
    {
        if((productCard == null) || (productCard instanceof TwitterProductCardStub)) {
            setProductCardStub((TwitterProductCardStub) productCard);
        } else {
            // TBD
            setProductCardStub(TwitterProductCardStub.convertBeanToStub(productCard));
        }
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("cardType", this.cardType);
        dataMap.put("summaryCard", this.summaryCard);
        dataMap.put("photoCard", this.photoCard);
        dataMap.put("galleryCard", this.galleryCard);
        dataMap.put("appCard", this.appCard);
        dataMap.put("playerCard", this.playerCard);
        dataMap.put("productCard", this.productCard);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = cardType == null ? 0 : cardType.hashCode();
        _hash = 31 * _hash + delta;
        delta = summaryCard == null ? 0 : summaryCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = photoCard == null ? 0 : photoCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = galleryCard == null ? 0 : galleryCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = appCard == null ? 0 : appCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = playerCard == null ? 0 : playerCard.hashCode();
        _hash = 31 * _hash + delta;
        delta = productCard == null ? 0 : productCard.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static TwitterCardMetaStub convertBeanToStub(TwitterCardMeta bean)
    {
        TwitterCardMetaStub stub = null;
        if(bean instanceof TwitterCardMetaStub) {
            stub = (TwitterCardMetaStub) bean;
        } else {
            if(bean != null) {
                stub = new TwitterCardMetaStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterCardMetaStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterCardMetaStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardMetaStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardMetaStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardMetaStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterCardMetaStub fromJsonString(String jsonStr)
    {
        try {
            TwitterCardMetaStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterCardMetaStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardMetaStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardMetaStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardMetaStub object.", e);
        }
        
        return null;
    }

}
