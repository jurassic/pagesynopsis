package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogQuantityStructs")
@XmlType(propOrder = {"ogQuantityStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgQuantityStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgQuantityStructListStub.class.getName());

    private List<OgQuantityStructStub> ogQuantityStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgQuantityStructListStub()
    {
        this(new ArrayList<OgQuantityStructStub>());
    }
    public OgQuantityStructListStub(List<OgQuantityStructStub> ogQuantityStructs)
    {
        this(ogQuantityStructs, null);
    }
    public OgQuantityStructListStub(List<OgQuantityStructStub> ogQuantityStructs, String forwardCursor)
    {
        this.ogQuantityStructs = ogQuantityStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogQuantityStructs == null) {
            return true;
        } else {
            return ogQuantityStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogQuantityStructs == null) {
            return 0;
        } else {
            return ogQuantityStructs.size();
        }
    }


    @XmlElement(name = "ogQuantityStruct")
    public List<OgQuantityStructStub> getOgQuantityStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgQuantityStructStub> getList()
    {
        return ogQuantityStructs;
    }
    public void setList(List<OgQuantityStructStub> ogQuantityStructs)
    {
        this.ogQuantityStructs = ogQuantityStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgQuantityStructStub> it = this.ogQuantityStructs.iterator();
        while(it.hasNext()) {
            OgQuantityStructStub ogQuantityStruct = it.next();
            sb.append(ogQuantityStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgQuantityStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgQuantityStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgQuantityStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgQuantityStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgQuantityStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgQuantityStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgQuantityStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgQuantityStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgQuantityStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgQuantityStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgQuantityStructListStub object.", e);
        }
        
        return null;
    }

}
