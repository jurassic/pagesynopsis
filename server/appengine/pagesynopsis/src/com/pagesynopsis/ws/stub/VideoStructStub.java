package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "videoStruct")
@XmlType(propOrder = {"uuid", "id", "width", "height", "controls", "autoplayEnabled", "loopEnabled", "preloadEnabled", "muted", "remark", "sourceStub", "source1Stub", "source2Stub", "source3Stub", "source4Stub", "source5Stub", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoStructStub implements VideoStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VideoStructStub.class.getName());

    private String uuid;
    private String id;
    private Integer width;
    private Integer height;
    private String controls;
    private Boolean autoplayEnabled;
    private Boolean loopEnabled;
    private Boolean preloadEnabled;
    private Boolean muted;
    private String remark;
    private MediaSourceStructStub source;
    private MediaSourceStructStub source1;
    private MediaSourceStructStub source2;
    private MediaSourceStructStub source3;
    private MediaSourceStructStub source4;
    private MediaSourceStructStub source5;
    private String note;

    public VideoStructStub()
    {
        this(null);
    }
    public VideoStructStub(VideoStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.id = bean.getId();
            this.width = bean.getWidth();
            this.height = bean.getHeight();
            this.controls = bean.getControls();
            this.autoplayEnabled = bean.isAutoplayEnabled();
            this.loopEnabled = bean.isLoopEnabled();
            this.preloadEnabled = bean.isPreloadEnabled();
            this.muted = bean.isMuted();
            this.remark = bean.getRemark();
            this.source = MediaSourceStructStub.convertBeanToStub(bean.getSource());
            this.source1 = MediaSourceStructStub.convertBeanToStub(bean.getSource1());
            this.source2 = MediaSourceStructStub.convertBeanToStub(bean.getSource2());
            this.source3 = MediaSourceStructStub.convertBeanToStub(bean.getSource3());
            this.source4 = MediaSourceStructStub.convertBeanToStub(bean.getSource4());
            this.source5 = MediaSourceStructStub.convertBeanToStub(bean.getSource5());
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    @XmlElement
    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
    }

    @XmlElement
    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
    }

    @XmlElement
    public String getControls()
    {
        return this.controls;
    }
    public void setControls(String controls)
    {
        this.controls = controls;
    }

    @XmlElement
    public Boolean isAutoplayEnabled()
    {
        return this.autoplayEnabled;
    }
    public void setAutoplayEnabled(Boolean autoplayEnabled)
    {
        this.autoplayEnabled = autoplayEnabled;
    }

    @XmlElement
    public Boolean isLoopEnabled()
    {
        return this.loopEnabled;
    }
    public void setLoopEnabled(Boolean loopEnabled)
    {
        this.loopEnabled = loopEnabled;
    }

    @XmlElement
    public Boolean isPreloadEnabled()
    {
        return this.preloadEnabled;
    }
    public void setPreloadEnabled(Boolean preloadEnabled)
    {
        this.preloadEnabled = preloadEnabled;
    }

    @XmlElement
    public Boolean isMuted()
    {
        return this.muted;
    }
    public void setMuted(Boolean muted)
    {
        this.muted = muted;
    }

    @XmlElement
    public String getRemark()
    {
        return this.remark;
    }
    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    @XmlElement(name = "source")
    @JsonIgnore
    public MediaSourceStructStub getSourceStub()
    {
        return this.source;
    }
    public void setSourceStub(MediaSourceStructStub source)
    {
        this.source = source;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=MediaSourceStructStub.class)
    public MediaSourceStruct getSource()
    {  
        return getSourceStub();
    }
    public void setSource(MediaSourceStruct source)
    {
        if((source == null) || (source instanceof MediaSourceStructStub)) {
            setSourceStub((MediaSourceStructStub) source);
        } else {
            // TBD
            setSourceStub(MediaSourceStructStub.convertBeanToStub(source));
        }
    }

    @XmlElement(name = "source1")
    @JsonIgnore
    public MediaSourceStructStub getSource1Stub()
    {
        return this.source1;
    }
    public void setSource1Stub(MediaSourceStructStub source1)
    {
        this.source1 = source1;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=MediaSourceStructStub.class)
    public MediaSourceStruct getSource1()
    {  
        return getSource1Stub();
    }
    public void setSource1(MediaSourceStruct source1)
    {
        if((source1 == null) || (source1 instanceof MediaSourceStructStub)) {
            setSource1Stub((MediaSourceStructStub) source1);
        } else {
            // TBD
            setSource1Stub(MediaSourceStructStub.convertBeanToStub(source1));
        }
    }

    @XmlElement(name = "source2")
    @JsonIgnore
    public MediaSourceStructStub getSource2Stub()
    {
        return this.source2;
    }
    public void setSource2Stub(MediaSourceStructStub source2)
    {
        this.source2 = source2;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=MediaSourceStructStub.class)
    public MediaSourceStruct getSource2()
    {  
        return getSource2Stub();
    }
    public void setSource2(MediaSourceStruct source2)
    {
        if((source2 == null) || (source2 instanceof MediaSourceStructStub)) {
            setSource2Stub((MediaSourceStructStub) source2);
        } else {
            // TBD
            setSource2Stub(MediaSourceStructStub.convertBeanToStub(source2));
        }
    }

    @XmlElement(name = "source3")
    @JsonIgnore
    public MediaSourceStructStub getSource3Stub()
    {
        return this.source3;
    }
    public void setSource3Stub(MediaSourceStructStub source3)
    {
        this.source3 = source3;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=MediaSourceStructStub.class)
    public MediaSourceStruct getSource3()
    {  
        return getSource3Stub();
    }
    public void setSource3(MediaSourceStruct source3)
    {
        if((source3 == null) || (source3 instanceof MediaSourceStructStub)) {
            setSource3Stub((MediaSourceStructStub) source3);
        } else {
            // TBD
            setSource3Stub(MediaSourceStructStub.convertBeanToStub(source3));
        }
    }

    @XmlElement(name = "source4")
    @JsonIgnore
    public MediaSourceStructStub getSource4Stub()
    {
        return this.source4;
    }
    public void setSource4Stub(MediaSourceStructStub source4)
    {
        this.source4 = source4;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=MediaSourceStructStub.class)
    public MediaSourceStruct getSource4()
    {  
        return getSource4Stub();
    }
    public void setSource4(MediaSourceStruct source4)
    {
        if((source4 == null) || (source4 instanceof MediaSourceStructStub)) {
            setSource4Stub((MediaSourceStructStub) source4);
        } else {
            // TBD
            setSource4Stub(MediaSourceStructStub.convertBeanToStub(source4));
        }
    }

    @XmlElement(name = "source5")
    @JsonIgnore
    public MediaSourceStructStub getSource5Stub()
    {
        return this.source5;
    }
    public void setSource5Stub(MediaSourceStructStub source5)
    {
        this.source5 = source5;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(as=MediaSourceStructStub.class)
    public MediaSourceStruct getSource5()
    {  
        return getSource5Stub();
    }
    public void setSource5(MediaSourceStruct source5)
    {
        if((source5 == null) || (source5 instanceof MediaSourceStructStub)) {
            setSource5Stub((MediaSourceStructStub) source5);
        } else {
            // TBD
            setSource5Stub(MediaSourceStructStub.convertBeanToStub(source5));
        }
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getControls() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isAutoplayEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLoopEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreloadEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isMuted() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource3() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource4() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource5() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("id", this.id);
        dataMap.put("width", this.width);
        dataMap.put("height", this.height);
        dataMap.put("controls", this.controls);
        dataMap.put("autoplayEnabled", this.autoplayEnabled);
        dataMap.put("loopEnabled", this.loopEnabled);
        dataMap.put("preloadEnabled", this.preloadEnabled);
        dataMap.put("muted", this.muted);
        dataMap.put("remark", this.remark);
        dataMap.put("source", this.source);
        dataMap.put("source1", this.source1);
        dataMap.put("source2", this.source2);
        dataMap.put("source3", this.source3);
        dataMap.put("source4", this.source4);
        dataMap.put("source5", this.source5);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = width == null ? 0 : width.hashCode();
        _hash = 31 * _hash + delta;
        delta = height == null ? 0 : height.hashCode();
        _hash = 31 * _hash + delta;
        delta = controls == null ? 0 : controls.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoplayEnabled == null ? 0 : autoplayEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = loopEnabled == null ? 0 : loopEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = preloadEnabled == null ? 0 : preloadEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = muted == null ? 0 : muted.hashCode();
        _hash = 31 * _hash + delta;
        delta = remark == null ? 0 : remark.hashCode();
        _hash = 31 * _hash + delta;
        delta = source == null ? 0 : source.hashCode();
        _hash = 31 * _hash + delta;
        delta = source1 == null ? 0 : source1.hashCode();
        _hash = 31 * _hash + delta;
        delta = source2 == null ? 0 : source2.hashCode();
        _hash = 31 * _hash + delta;
        delta = source3 == null ? 0 : source3.hashCode();
        _hash = 31 * _hash + delta;
        delta = source4 == null ? 0 : source4.hashCode();
        _hash = 31 * _hash + delta;
        delta = source5 == null ? 0 : source5.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static VideoStructStub convertBeanToStub(VideoStruct bean)
    {
        VideoStructStub stub = null;
        if(bean instanceof VideoStructStub) {
            stub = (VideoStructStub) bean;
        } else {
            if(bean != null) {
                stub = new VideoStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static VideoStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of VideoStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write VideoStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write VideoStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write VideoStructStub object as a string.", e);
        }
        
        return null;
    }
    public static VideoStructStub fromJsonString(String jsonStr)
    {
        try {
            VideoStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, VideoStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoStructStub object.", e);
        }
        
        return null;
    }

}
