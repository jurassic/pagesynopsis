package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "encodedQueryParamStructs")
@XmlType(propOrder = {"encodedQueryParamStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class EncodedQueryParamStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(EncodedQueryParamStructListStub.class.getName());

    private List<EncodedQueryParamStructStub> encodedQueryParamStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public EncodedQueryParamStructListStub()
    {
        this(new ArrayList<EncodedQueryParamStructStub>());
    }
    public EncodedQueryParamStructListStub(List<EncodedQueryParamStructStub> encodedQueryParamStructs)
    {
        this(encodedQueryParamStructs, null);
    }
    public EncodedQueryParamStructListStub(List<EncodedQueryParamStructStub> encodedQueryParamStructs, String forwardCursor)
    {
        this.encodedQueryParamStructs = encodedQueryParamStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(encodedQueryParamStructs == null) {
            return true;
        } else {
            return encodedQueryParamStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(encodedQueryParamStructs == null) {
            return 0;
        } else {
            return encodedQueryParamStructs.size();
        }
    }


    @XmlElement(name = "encodedQueryParamStruct")
    public List<EncodedQueryParamStructStub> getEncodedQueryParamStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<EncodedQueryParamStructStub> getList()
    {
        return encodedQueryParamStructs;
    }
    public void setList(List<EncodedQueryParamStructStub> encodedQueryParamStructs)
    {
        this.encodedQueryParamStructs = encodedQueryParamStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<EncodedQueryParamStructStub> it = this.encodedQueryParamStructs.iterator();
        while(it.hasNext()) {
            EncodedQueryParamStructStub encodedQueryParamStruct = it.next();
            sb.append(encodedQueryParamStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static EncodedQueryParamStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of EncodedQueryParamStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write EncodedQueryParamStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write EncodedQueryParamStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write EncodedQueryParamStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static EncodedQueryParamStructListStub fromJsonString(String jsonStr)
    {
        try {
            EncodedQueryParamStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, EncodedQueryParamStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into EncodedQueryParamStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into EncodedQueryParamStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into EncodedQueryParamStructListStub object.", e);
        }
        
        return null;
    }

}
