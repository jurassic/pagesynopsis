package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "notificationStructs")
@XmlType(propOrder = {"notificationStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(NotificationStructListStub.class.getName());

    private List<NotificationStructStub> notificationStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public NotificationStructListStub()
    {
        this(new ArrayList<NotificationStructStub>());
    }
    public NotificationStructListStub(List<NotificationStructStub> notificationStructs)
    {
        this(notificationStructs, null);
    }
    public NotificationStructListStub(List<NotificationStructStub> notificationStructs, String forwardCursor)
    {
        this.notificationStructs = notificationStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(notificationStructs == null) {
            return true;
        } else {
            return notificationStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(notificationStructs == null) {
            return 0;
        } else {
            return notificationStructs.size();
        }
    }


    @XmlElement(name = "notificationStruct")
    public List<NotificationStructStub> getNotificationStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<NotificationStructStub> getList()
    {
        return notificationStructs;
    }
    public void setList(List<NotificationStructStub> notificationStructs)
    {
        this.notificationStructs = notificationStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<NotificationStructStub> it = this.notificationStructs.iterator();
        while(it.hasNext()) {
            NotificationStructStub notificationStruct = it.next();
            sb.append(notificationStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static NotificationStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of NotificationStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write NotificationStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write NotificationStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write NotificationStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static NotificationStructListStub fromJsonString(String jsonStr)
    {
        try {
            NotificationStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, NotificationStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into NotificationStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into NotificationStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into NotificationStructListStub object.", e);
        }
        
        return null;
    }

}
