package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "pageFetch")
@XmlType(propOrder = {"guid", "user", "fetchRequest", "targetUrl", "pageUrl", "queryString", "queryParamsStub", "lastFetchResult", "responseCode", "contentType", "contentLength", "language", "redirect", "location", "pageTitle", "note", "deferred", "status", "refreshStatus", "refreshInterval", "nextRefreshTime", "lastCheckedTime", "lastUpdatedTime", "inputMaxRedirects", "resultRedirectCount", "redirectPagesStub", "destinationUrl", "pageAuthor", "pageSummary", "favicon", "faviconUrl", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageFetchStub extends PageBaseStub implements PageFetch, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageFetchStub.class.getName());

    private Integer inputMaxRedirects;
    private Integer resultRedirectCount;
    private List<UrlStructStub> redirectPages;
    private String destinationUrl;
    private String pageAuthor;
    private String pageSummary;
    private String favicon;
    private String faviconUrl;

    public PageFetchStub()
    {
        this(null);
    }
    public PageFetchStub(PageFetch bean)
    {
        super(bean);
        if(bean != null) {
            this.inputMaxRedirects = bean.getInputMaxRedirects();
            this.resultRedirectCount = bean.getResultRedirectCount();
            List<UrlStructStub> _redirectPagesStubs = null;
            if(bean.getRedirectPages() != null) {
                _redirectPagesStubs = new ArrayList<UrlStructStub>();
                for(UrlStruct b : bean.getRedirectPages()) {
                    _redirectPagesStubs.add(UrlStructStub.convertBeanToStub(b));
                }
            }
            this.redirectPages = _redirectPagesStubs;
            this.destinationUrl = bean.getDestinationUrl();
            this.pageAuthor = bean.getPageAuthor();
            this.pageSummary = bean.getPageSummary();
            this.favicon = bean.getFavicon();
            this.faviconUrl = bean.getFaviconUrl();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    @XmlElement
    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    @XmlElement
    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    @XmlElement
    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    @XmlElement(name = "queryParams")
    @JsonIgnore
    public List<KeyValuePairStructStub> getQueryParamsStub()
    {
        return super.getQueryParamsStub();
    }
    public void setQueryParamsStub(List<KeyValuePairStructStub> queryParams)
    {
        super.setQueryParamsStub(queryParams);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=KeyValuePairStructStub.class)
    public List<KeyValuePairStruct> getQueryParams()
    {  
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    @XmlElement
    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    @XmlElement
    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    @XmlElement
    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    @XmlElement
    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    @XmlElement
    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    @XmlElement
    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    @XmlElement
    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    @XmlElement
    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    @XmlElement
    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    @XmlElement
    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    @XmlElement
    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    @XmlElement
    public Integer getInputMaxRedirects()
    {
        return this.inputMaxRedirects;
    }
    public void setInputMaxRedirects(Integer inputMaxRedirects)
    {
        this.inputMaxRedirects = inputMaxRedirects;
    }

    @XmlElement
    public Integer getResultRedirectCount()
    {
        return this.resultRedirectCount;
    }
    public void setResultRedirectCount(Integer resultRedirectCount)
    {
        this.resultRedirectCount = resultRedirectCount;
    }

    @XmlElement(name = "redirectPages")
    @JsonIgnore
    public List<UrlStructStub> getRedirectPagesStub()
    {
        return this.redirectPages;
    }
    public void setRedirectPagesStub(List<UrlStructStub> redirectPages)
    {
        this.redirectPages = redirectPages;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=UrlStructStub.class)
    public List<UrlStruct> getRedirectPages()
    {  
        return (List<UrlStruct>) ((List<?>) getRedirectPagesStub());
    }
    public void setRedirectPages(List<UrlStruct> redirectPages)
    {
        // TBD
        //if(redirectPages instanceof List<UrlStructStub>) {
            setRedirectPagesStub((List<UrlStructStub>) ((List<?>) redirectPages));
        //} else {
        //    // TBD
        //    List<UrlStructStub> _UrlStructList = new ArrayList<UrlStructStub>(); // ???
        //    for(UrlStruct b : redirectPages) {
        //        _UrlStructList.add(UrlStructStub.convertBeanToStub(redirectPages));
        //    }
        //    setRedirectPagesStub(_UrlStructList);
        //}
    }

    @XmlElement
    public String getDestinationUrl()
    {
        return this.destinationUrl;
    }
    public void setDestinationUrl(String destinationUrl)
    {
        this.destinationUrl = destinationUrl;
    }

    @XmlElement
    public String getPageAuthor()
    {
        return this.pageAuthor;
    }
    public void setPageAuthor(String pageAuthor)
    {
        this.pageAuthor = pageAuthor;
    }

    @XmlElement
    public String getPageSummary()
    {
        return this.pageSummary;
    }
    public void setPageSummary(String pageSummary)
    {
        this.pageSummary = pageSummary;
    }

    @XmlElement
    public String getFavicon()
    {
        return this.favicon;
    }
    public void setFavicon(String favicon)
    {
        this.favicon = favicon;
    }

    @XmlElement
    public String getFaviconUrl()
    {
        return this.faviconUrl;
    }
    public void setFaviconUrl(String faviconUrl)
    {
        this.faviconUrl = faviconUrl;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("inputMaxRedirects", this.inputMaxRedirects);
        dataMap.put("resultRedirectCount", this.resultRedirectCount);
        dataMap.put("redirectPages", this.redirectPages);
        dataMap.put("destinationUrl", this.destinationUrl);
        dataMap.put("pageAuthor", this.pageAuthor);
        dataMap.put("pageSummary", this.pageSummary);
        dataMap.put("favicon", this.favicon);
        dataMap.put("faviconUrl", this.faviconUrl);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = inputMaxRedirects == null ? 0 : inputMaxRedirects.hashCode();
        _hash = 31 * _hash + delta;
        delta = resultRedirectCount == null ? 0 : resultRedirectCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectPages == null ? 0 : redirectPages.hashCode();
        _hash = 31 * _hash + delta;
        delta = destinationUrl == null ? 0 : destinationUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageAuthor == null ? 0 : pageAuthor.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageSummary == null ? 0 : pageSummary.hashCode();
        _hash = 31 * _hash + delta;
        delta = favicon == null ? 0 : favicon.hashCode();
        _hash = 31 * _hash + delta;
        delta = faviconUrl == null ? 0 : faviconUrl.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static PageFetchStub convertBeanToStub(PageFetch bean)
    {
        PageFetchStub stub = null;
        if(bean instanceof PageFetchStub) {
            stub = (PageFetchStub) bean;
        } else {
            if(bean != null) {
                stub = new PageFetchStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PageFetchStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of PageFetchStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PageFetchStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PageFetchStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PageFetchStub object as a string.", e);
        }
        
        return null;
    }
    public static PageFetchStub fromJsonString(String jsonStr)
    {
        try {
            PageFetchStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PageFetchStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PageFetchStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PageFetchStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PageFetchStub object.", e);
        }
        
        return null;
    }

}
