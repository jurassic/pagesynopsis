package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "audioSet")
@XmlType(propOrder = {"guid", "user", "fetchRequest", "targetUrl", "pageUrl", "queryString", "queryParamsStub", "lastFetchResult", "responseCode", "contentType", "contentLength", "language", "redirect", "location", "pageTitle", "note", "deferred", "status", "refreshStatus", "refreshInterval", "nextRefreshTime", "lastCheckedTime", "lastUpdatedTime", "mediaTypeFilter", "pageAudiosStub", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudioSetStub extends PageBaseStub implements AudioSet, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AudioSetStub.class.getName());

    private List<String> mediaTypeFilter;
    private Set<AudioStructStub> pageAudios;

    public AudioSetStub()
    {
        this(null);
    }
    public AudioSetStub(AudioSet bean)
    {
        super(bean);
        if(bean != null) {
            this.mediaTypeFilter = bean.getMediaTypeFilter();
            Set<AudioStructStub> _pageAudiosStubs = null;
            if(bean.getPageAudios() != null) {
                _pageAudiosStubs = new HashSet<AudioStructStub>();
                for(AudioStruct b : bean.getPageAudios()) {
                    _pageAudiosStubs.add(AudioStructStub.convertBeanToStub(b));
                }
            }
            this.pageAudios = _pageAudiosStubs;
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    @XmlElement
    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    @XmlElement
    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    @XmlElement
    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    @XmlElement
    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    @XmlElement(name = "queryParams")
    @JsonIgnore
    public List<KeyValuePairStructStub> getQueryParamsStub()
    {
        return super.getQueryParamsStub();
    }
    public void setQueryParamsStub(List<KeyValuePairStructStub> queryParams)
    {
        super.setQueryParamsStub(queryParams);
    }
    @XmlTransient
    @JsonDeserialize(contentAs=KeyValuePairStructStub.class)
    public List<KeyValuePairStruct> getQueryParams()
    {  
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    @XmlElement
    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    @XmlElement
    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    @XmlElement
    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    @XmlElement
    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    @XmlElement
    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    @XmlElement
    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    @XmlElement
    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    @XmlElement
    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    @XmlElement
    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    @XmlElement
    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    @XmlElement
    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    @XmlElement
    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    @XmlElement
    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    @XmlElement
    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    @XmlElement
    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    @XmlElement
    public List<String> getMediaTypeFilter()
    {
        return this.mediaTypeFilter;
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        this.mediaTypeFilter = mediaTypeFilter;
    }

    @XmlElement(name = "pageAudios")
    @JsonIgnore
    public Set<AudioStructStub> getPageAudiosStub()
    {
        return this.pageAudios;
    }
    public void setPageAudiosStub(Set<AudioStructStub> pageAudios)
    {
        this.pageAudios = pageAudios;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=AudioStructStub.class)
    public Set<AudioStruct> getPageAudios()
    {  
        return (Set<AudioStruct>) ((Set<?>) getPageAudiosStub());
    }
    public void setPageAudios(Set<AudioStruct> pageAudios)
    {
        // TBD
        //if(pageAudios instanceof Set<AudioStructStub>) {
            setPageAudiosStub((Set<AudioStructStub>) ((Set<?>) pageAudios));
        //} else {
        //    // TBD
        //    Set<AudioStructStub> _AudioStructList = new HashSet<AudioStructStub>(); // ???
        //    for(AudioStruct b : pageAudios) {
        //        _AudioStructList.add(AudioStructStub.convertBeanToStub(pageAudios));
        //    }
        //    setPageAudiosStub(_AudioStructList);
        //}
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("mediaTypeFilter", this.mediaTypeFilter);
        dataMap.put("pageAudios", this.pageAudios);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = mediaTypeFilter == null ? 0 : mediaTypeFilter.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageAudios == null ? 0 : pageAudios.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static AudioSetStub convertBeanToStub(AudioSet bean)
    {
        AudioSetStub stub = null;
        if(bean instanceof AudioSetStub) {
            stub = (AudioSetStub) bean;
        } else {
            if(bean != null) {
                stub = new AudioSetStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AudioSetStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AudioSetStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AudioSetStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AudioSetStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AudioSetStub object as a string.", e);
        }
        
        return null;
    }
    public static AudioSetStub fromJsonString(String jsonStr)
    {
        try {
            AudioSetStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AudioSetStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioSetStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioSetStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioSetStub object.", e);
        }
        
        return null;
    }

}
