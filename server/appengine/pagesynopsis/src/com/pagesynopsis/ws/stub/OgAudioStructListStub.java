package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogAudioStructs")
@XmlType(propOrder = {"ogAudioStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgAudioStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgAudioStructListStub.class.getName());

    private List<OgAudioStructStub> ogAudioStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgAudioStructListStub()
    {
        this(new ArrayList<OgAudioStructStub>());
    }
    public OgAudioStructListStub(List<OgAudioStructStub> ogAudioStructs)
    {
        this(ogAudioStructs, null);
    }
    public OgAudioStructListStub(List<OgAudioStructStub> ogAudioStructs, String forwardCursor)
    {
        this.ogAudioStructs = ogAudioStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogAudioStructs == null) {
            return true;
        } else {
            return ogAudioStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogAudioStructs == null) {
            return 0;
        } else {
            return ogAudioStructs.size();
        }
    }


    @XmlElement(name = "ogAudioStruct")
    public List<OgAudioStructStub> getOgAudioStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgAudioStructStub> getList()
    {
        return ogAudioStructs;
    }
    public void setList(List<OgAudioStructStub> ogAudioStructs)
    {
        this.ogAudioStructs = ogAudioStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgAudioStructStub> it = this.ogAudioStructs.iterator();
        while(it.hasNext()) {
            OgAudioStructStub ogAudioStruct = it.next();
            sb.append(ogAudioStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgAudioStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgAudioStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgAudioStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgAudioStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgAudioStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgAudioStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgAudioStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgAudioStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgAudioStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgAudioStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgAudioStructListStub object.", e);
        }
        
        return null;
    }

}
