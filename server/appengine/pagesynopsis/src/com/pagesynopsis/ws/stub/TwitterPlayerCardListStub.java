package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "twitterPlayerCards")
@XmlType(propOrder = {"twitterPlayerCard", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterPlayerCardListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterPlayerCardListStub.class.getName());

    private List<TwitterPlayerCardStub> twitterPlayerCards = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TwitterPlayerCardListStub()
    {
        this(new ArrayList<TwitterPlayerCardStub>());
    }
    public TwitterPlayerCardListStub(List<TwitterPlayerCardStub> twitterPlayerCards)
    {
        this(twitterPlayerCards, null);
    }
    public TwitterPlayerCardListStub(List<TwitterPlayerCardStub> twitterPlayerCards, String forwardCursor)
    {
        this.twitterPlayerCards = twitterPlayerCards;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(twitterPlayerCards == null) {
            return true;
        } else {
            return twitterPlayerCards.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterPlayerCards == null) {
            return 0;
        } else {
            return twitterPlayerCards.size();
        }
    }


    @XmlElement(name = "twitterPlayerCard")
    public List<TwitterPlayerCardStub> getTwitterPlayerCard()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterPlayerCardStub> getList()
    {
        return twitterPlayerCards;
    }
    public void setList(List<TwitterPlayerCardStub> twitterPlayerCards)
    {
        this.twitterPlayerCards = twitterPlayerCards;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TwitterPlayerCardStub> it = this.twitterPlayerCards.iterator();
        while(it.hasNext()) {
            TwitterPlayerCardStub twitterPlayerCard = it.next();
            sb.append(twitterPlayerCard.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterPlayerCardListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterPlayerCardListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterPlayerCardListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterPlayerCardListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterPlayerCardListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterPlayerCardListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterPlayerCardListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterPlayerCardListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPlayerCardListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPlayerCardListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterPlayerCardListStub object.", e);
        }
        
        return null;
    }

}
