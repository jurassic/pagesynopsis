package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "anchorStruct")
@XmlType(propOrder = {"uuid", "id", "name", "href", "hrefUrl", "urlScheme", "rel", "target", "innerHtml", "anchorText", "anchorTitle", "anchorImage", "anchorLegend", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnchorStructStub implements AnchorStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AnchorStructStub.class.getName());

    private String uuid;
    private String id;
    private String name;
    private String href;
    private String hrefUrl;
    private String urlScheme;
    private String rel;
    private String target;
    private String innerHtml;
    private String anchorText;
    private String anchorTitle;
    private String anchorImage;
    private String anchorLegend;
    private String note;

    public AnchorStructStub()
    {
        this(null);
    }
    public AnchorStructStub(AnchorStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.id = bean.getId();
            this.name = bean.getName();
            this.href = bean.getHref();
            this.hrefUrl = bean.getHrefUrl();
            this.urlScheme = bean.getUrlScheme();
            this.rel = bean.getRel();
            this.target = bean.getTarget();
            this.innerHtml = bean.getInnerHtml();
            this.anchorText = bean.getAnchorText();
            this.anchorTitle = bean.getAnchorTitle();
            this.anchorImage = bean.getAnchorImage();
            this.anchorLegend = bean.getAnchorLegend();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    @XmlElement
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement
    public String getHref()
    {
        return this.href;
    }
    public void setHref(String href)
    {
        this.href = href;
    }

    @XmlElement
    public String getHrefUrl()
    {
        return this.hrefUrl;
    }
    public void setHrefUrl(String hrefUrl)
    {
        this.hrefUrl = hrefUrl;
    }

    @XmlElement
    public String getUrlScheme()
    {
        return this.urlScheme;
    }
    public void setUrlScheme(String urlScheme)
    {
        this.urlScheme = urlScheme;
    }

    @XmlElement
    public String getRel()
    {
        return this.rel;
    }
    public void setRel(String rel)
    {
        this.rel = rel;
    }

    @XmlElement
    public String getTarget()
    {
        return this.target;
    }
    public void setTarget(String target)
    {
        this.target = target;
    }

    @XmlElement
    public String getInnerHtml()
    {
        return this.innerHtml;
    }
    public void setInnerHtml(String innerHtml)
    {
        this.innerHtml = innerHtml;
    }

    @XmlElement
    public String getAnchorText()
    {
        return this.anchorText;
    }
    public void setAnchorText(String anchorText)
    {
        this.anchorText = anchorText;
    }

    @XmlElement
    public String getAnchorTitle()
    {
        return this.anchorTitle;
    }
    public void setAnchorTitle(String anchorTitle)
    {
        this.anchorTitle = anchorTitle;
    }

    @XmlElement
    public String getAnchorImage()
    {
        return this.anchorImage;
    }
    public void setAnchorImage(String anchorImage)
    {
        this.anchorImage = anchorImage;
    }

    @XmlElement
    public String getAnchorLegend()
    {
        return this.anchorLegend;
    }
    public void setAnchorLegend(String anchorLegend)
    {
        this.anchorLegend = anchorLegend;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHrefUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrlScheme() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTarget() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getInnerHtml() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorLegend() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("id", this.id);
        dataMap.put("name", this.name);
        dataMap.put("href", this.href);
        dataMap.put("hrefUrl", this.hrefUrl);
        dataMap.put("urlScheme", this.urlScheme);
        dataMap.put("rel", this.rel);
        dataMap.put("target", this.target);
        dataMap.put("innerHtml", this.innerHtml);
        dataMap.put("anchorText", this.anchorText);
        dataMap.put("anchorTitle", this.anchorTitle);
        dataMap.put("anchorImage", this.anchorImage);
        dataMap.put("anchorLegend", this.anchorLegend);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = href == null ? 0 : href.hashCode();
        _hash = 31 * _hash + delta;
        delta = hrefUrl == null ? 0 : hrefUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = urlScheme == null ? 0 : urlScheme.hashCode();
        _hash = 31 * _hash + delta;
        delta = rel == null ? 0 : rel.hashCode();
        _hash = 31 * _hash + delta;
        delta = target == null ? 0 : target.hashCode();
        _hash = 31 * _hash + delta;
        delta = innerHtml == null ? 0 : innerHtml.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorText == null ? 0 : anchorText.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorTitle == null ? 0 : anchorTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorImage == null ? 0 : anchorImage.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorLegend == null ? 0 : anchorLegend.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static AnchorStructStub convertBeanToStub(AnchorStruct bean)
    {
        AnchorStructStub stub = null;
        if(bean instanceof AnchorStructStub) {
            stub = (AnchorStructStub) bean;
        } else {
            if(bean != null) {
                stub = new AnchorStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AnchorStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AnchorStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AnchorStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AnchorStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AnchorStructStub object as a string.", e);
        }
        
        return null;
    }
    public static AnchorStructStub fromJsonString(String jsonStr)
    {
        try {
            AnchorStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AnchorStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AnchorStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AnchorStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AnchorStructStub object.", e);
        }
        
        return null;
    }

}
