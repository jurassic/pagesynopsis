package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogVideos")
@XmlType(propOrder = {"ogVideo", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgVideoListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgVideoListStub.class.getName());

    private List<OgVideoStub> ogVideos = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgVideoListStub()
    {
        this(new ArrayList<OgVideoStub>());
    }
    public OgVideoListStub(List<OgVideoStub> ogVideos)
    {
        this(ogVideos, null);
    }
    public OgVideoListStub(List<OgVideoStub> ogVideos, String forwardCursor)
    {
        this.ogVideos = ogVideos;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogVideos == null) {
            return true;
        } else {
            return ogVideos.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogVideos == null) {
            return 0;
        } else {
            return ogVideos.size();
        }
    }


    @XmlElement(name = "ogVideo")
    public List<OgVideoStub> getOgVideo()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgVideoStub> getList()
    {
        return ogVideos;
    }
    public void setList(List<OgVideoStub> ogVideos)
    {
        this.ogVideos = ogVideos;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgVideoStub> it = this.ogVideos.iterator();
        while(it.hasNext()) {
            OgVideoStub ogVideo = it.next();
            sb.append(ogVideo.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgVideoListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgVideoListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgVideoListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgVideoListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgVideoListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgVideoListStub fromJsonString(String jsonStr)
    {
        try {
            OgVideoListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgVideoListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgVideoListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgVideoListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgVideoListStub object.", e);
        }
        
        return null;
    }

}
