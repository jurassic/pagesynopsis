package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "robotsTextGroups")
@XmlType(propOrder = {"robotsTextGroup", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextGroupListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextGroupListStub.class.getName());

    private List<RobotsTextGroupStub> robotsTextGroups = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public RobotsTextGroupListStub()
    {
        this(new ArrayList<RobotsTextGroupStub>());
    }
    public RobotsTextGroupListStub(List<RobotsTextGroupStub> robotsTextGroups)
    {
        this(robotsTextGroups, null);
    }
    public RobotsTextGroupListStub(List<RobotsTextGroupStub> robotsTextGroups, String forwardCursor)
    {
        this.robotsTextGroups = robotsTextGroups;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(robotsTextGroups == null) {
            return true;
        } else {
            return robotsTextGroups.isEmpty();
        }
    }
    public int getSize()
    {
        if(robotsTextGroups == null) {
            return 0;
        } else {
            return robotsTextGroups.size();
        }
    }


    @XmlElement(name = "robotsTextGroup")
    public List<RobotsTextGroupStub> getRobotsTextGroup()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RobotsTextGroupStub> getList()
    {
        return robotsTextGroups;
    }
    public void setList(List<RobotsTextGroupStub> robotsTextGroups)
    {
        this.robotsTextGroups = robotsTextGroups;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<RobotsTextGroupStub> it = this.robotsTextGroups.iterator();
        while(it.hasNext()) {
            RobotsTextGroupStub robotsTextGroup = it.next();
            sb.append(robotsTextGroup.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RobotsTextGroupListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RobotsTextGroupListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextGroupListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextGroupListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextGroupListStub object as a string.", e);
        }
        
        return null;
    }
    public static RobotsTextGroupListStub fromJsonString(String jsonStr)
    {
        try {
            RobotsTextGroupListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RobotsTextGroupListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextGroupListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextGroupListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextGroupListStub object.", e);
        }
        
        return null;
    }

}
