package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "fetchRequests")
@XmlType(propOrder = {"fetchRequest", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchRequestListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestListStub.class.getName());

    private List<FetchRequestStub> fetchRequests = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public FetchRequestListStub()
    {
        this(new ArrayList<FetchRequestStub>());
    }
    public FetchRequestListStub(List<FetchRequestStub> fetchRequests)
    {
        this(fetchRequests, null);
    }
    public FetchRequestListStub(List<FetchRequestStub> fetchRequests, String forwardCursor)
    {
        this.fetchRequests = fetchRequests;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(fetchRequests == null) {
            return true;
        } else {
            return fetchRequests.isEmpty();
        }
    }
    public int getSize()
    {
        if(fetchRequests == null) {
            return 0;
        } else {
            return fetchRequests.size();
        }
    }


    @XmlElement(name = "fetchRequest")
    public List<FetchRequestStub> getFetchRequest()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<FetchRequestStub> getList()
    {
        return fetchRequests;
    }
    public void setList(List<FetchRequestStub> fetchRequests)
    {
        this.fetchRequests = fetchRequests;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<FetchRequestStub> it = this.fetchRequests.iterator();
        while(it.hasNext()) {
            FetchRequestStub fetchRequest = it.next();
            sb.append(fetchRequest.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static FetchRequestListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of FetchRequestListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write FetchRequestListStub object as a string.", e);
        }
        
        return null;
    }
    public static FetchRequestListStub fromJsonString(String jsonStr)
    {
        try {
            FetchRequestListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, FetchRequestListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into FetchRequestListStub object.", e);
        }
        
        return null;
    }

}
