package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "urlStructs")
@XmlType(propOrder = {"urlStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlStructListStub.class.getName());

    private List<UrlStructStub> urlStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public UrlStructListStub()
    {
        this(new ArrayList<UrlStructStub>());
    }
    public UrlStructListStub(List<UrlStructStub> urlStructs)
    {
        this(urlStructs, null);
    }
    public UrlStructListStub(List<UrlStructStub> urlStructs, String forwardCursor)
    {
        this.urlStructs = urlStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(urlStructs == null) {
            return true;
        } else {
            return urlStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(urlStructs == null) {
            return 0;
        } else {
            return urlStructs.size();
        }
    }


    @XmlElement(name = "urlStruct")
    public List<UrlStructStub> getUrlStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UrlStructStub> getList()
    {
        return urlStructs;
    }
    public void setList(List<UrlStructStub> urlStructs)
    {
        this.urlStructs = urlStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<UrlStructStub> it = this.urlStructs.iterator();
        while(it.hasNext()) {
            UrlStructStub urlStruct = it.next();
            sb.append(urlStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UrlStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UrlStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UrlStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UrlStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UrlStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static UrlStructListStub fromJsonString(String jsonStr)
    {
        try {
            UrlStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UrlStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlStructListStub object.", e);
        }
        
        return null;
    }

}
