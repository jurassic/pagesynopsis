package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogVideoStructs")
@XmlType(propOrder = {"ogVideoStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgVideoStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgVideoStructListStub.class.getName());

    private List<OgVideoStructStub> ogVideoStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgVideoStructListStub()
    {
        this(new ArrayList<OgVideoStructStub>());
    }
    public OgVideoStructListStub(List<OgVideoStructStub> ogVideoStructs)
    {
        this(ogVideoStructs, null);
    }
    public OgVideoStructListStub(List<OgVideoStructStub> ogVideoStructs, String forwardCursor)
    {
        this.ogVideoStructs = ogVideoStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogVideoStructs == null) {
            return true;
        } else {
            return ogVideoStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogVideoStructs == null) {
            return 0;
        } else {
            return ogVideoStructs.size();
        }
    }


    @XmlElement(name = "ogVideoStruct")
    public List<OgVideoStructStub> getOgVideoStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgVideoStructStub> getList()
    {
        return ogVideoStructs;
    }
    public void setList(List<OgVideoStructStub> ogVideoStructs)
    {
        this.ogVideoStructs = ogVideoStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgVideoStructStub> it = this.ogVideoStructs.iterator();
        while(it.hasNext()) {
            OgVideoStructStub ogVideoStruct = it.next();
            sb.append(ogVideoStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgVideoStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgVideoStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgVideoStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgVideoStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgVideoStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgVideoStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgVideoStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgVideoStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgVideoStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgVideoStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgVideoStructListStub object.", e);
        }
        
        return null;
    }

}
