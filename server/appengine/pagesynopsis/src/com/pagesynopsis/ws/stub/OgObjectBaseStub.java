package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgObjectBase;
import com.pagesynopsis.ws.util.JsonUtil;


// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class OgObjectBaseStub implements OgObjectBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgObjectBaseStub.class.getName());

    private String guid;
    private String url;
    private String type;
    private String siteName;
    private String title;
    private String description;
    private List<String> fbAdmins;
    private List<String> fbAppId;
    private List<OgImageStructStub> image;
    private List<OgAudioStructStub> audio;
    private List<OgVideoStructStub> video;
    private String locale;
    private List<String> localeAlternate;
    private Long createdTime;
    private Long modifiedTime;

    public OgObjectBaseStub()
    {
        this(null);
    }
    public OgObjectBaseStub(OgObjectBase bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.url = bean.getUrl();
            this.type = bean.getType();
            this.siteName = bean.getSiteName();
            this.title = bean.getTitle();
            this.description = bean.getDescription();
            this.fbAdmins = bean.getFbAdmins();
            this.fbAppId = bean.getFbAppId();
            List<OgImageStructStub> _imageStubs = null;
            if(bean.getImage() != null) {
                _imageStubs = new ArrayList<OgImageStructStub>();
                for(OgImageStruct b : bean.getImage()) {
                    _imageStubs.add(OgImageStructStub.convertBeanToStub(b));
                }
            }
            this.image = _imageStubs;
            List<OgAudioStructStub> _audioStubs = null;
            if(bean.getAudio() != null) {
                _audioStubs = new ArrayList<OgAudioStructStub>();
                for(OgAudioStruct b : bean.getAudio()) {
                    _audioStubs.add(OgAudioStructStub.convertBeanToStub(b));
                }
            }
            this.audio = _audioStubs;
            List<OgVideoStructStub> _videoStubs = null;
            if(bean.getVideo() != null) {
                _videoStubs = new ArrayList<OgVideoStructStub>();
                for(OgVideoStruct b : bean.getVideo()) {
                    _videoStubs.add(OgVideoStructStub.convertBeanToStub(b));
                }
            }
            this.video = _videoStubs;
            this.locale = bean.getLocale();
            this.localeAlternate = bean.getLocaleAlternate();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlTransient
    //@JsonIgnore
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlTransient
    //@JsonIgnore
    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    @XmlTransient
    //@JsonIgnore
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlTransient
    //@JsonIgnore
    public String getSiteName()
    {
        return this.siteName;
    }
    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    @XmlTransient
    //@JsonIgnore
    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    @XmlTransient
    //@JsonIgnore
    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    @XmlTransient
    //@JsonIgnore
    public List<String> getFbAdmins()
    {
        return this.fbAdmins;
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        this.fbAdmins = fbAdmins;
    }

    @XmlTransient
    //@JsonIgnore
    public List<String> getFbAppId()
    {
        return this.fbAppId;
    }
    public void setFbAppId(List<String> fbAppId)
    {
        this.fbAppId = fbAppId;
    }

    @XmlTransient
    //@JsonIgnore
    public List<OgImageStructStub> getImageStub()
    {
        return this.image;
    }
    public void setImageStub(List<OgImageStructStub> image)
    {
        this.image = image;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgImageStructStub.class)
    public List<OgImageStruct> getImage()
    {  
        return (List<OgImageStruct>) ((List<?>) getImageStub());
    }
    public void setImage(List<OgImageStruct> image)
    {
        // TBD
        //if(image instanceof List<OgImageStructStub>) {
            setImageStub((List<OgImageStructStub>) ((List<?>) image));
        //} else {
        //    // TBD
        //    List<OgImageStructStub> _OgImageStructList = new ArrayList<OgImageStructStub>(); // ???
        //    for(OgImageStruct b : image) {
        //        _OgImageStructList.add(OgImageStructStub.convertBeanToStub(image));
        //    }
        //    setImageStub(_OgImageStructList);
        //}
    }

    @XmlTransient
    //@JsonIgnore
    public List<OgAudioStructStub> getAudioStub()
    {
        return this.audio;
    }
    public void setAudioStub(List<OgAudioStructStub> audio)
    {
        this.audio = audio;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgAudioStructStub.class)
    public List<OgAudioStruct> getAudio()
    {  
        return (List<OgAudioStruct>) ((List<?>) getAudioStub());
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        // TBD
        //if(audio instanceof List<OgAudioStructStub>) {
            setAudioStub((List<OgAudioStructStub>) ((List<?>) audio));
        //} else {
        //    // TBD
        //    List<OgAudioStructStub> _OgAudioStructList = new ArrayList<OgAudioStructStub>(); // ???
        //    for(OgAudioStruct b : audio) {
        //        _OgAudioStructList.add(OgAudioStructStub.convertBeanToStub(audio));
        //    }
        //    setAudioStub(_OgAudioStructList);
        //}
    }

    @XmlTransient
    //@JsonIgnore
    public List<OgVideoStructStub> getVideoStub()
    {
        return this.video;
    }
    public void setVideoStub(List<OgVideoStructStub> video)
    {
        this.video = video;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=OgVideoStructStub.class)
    public List<OgVideoStruct> getVideo()
    {  
        return (List<OgVideoStruct>) ((List<?>) getVideoStub());
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        // TBD
        //if(video instanceof List<OgVideoStructStub>) {
            setVideoStub((List<OgVideoStructStub>) ((List<?>) video));
        //} else {
        //    // TBD
        //    List<OgVideoStructStub> _OgVideoStructList = new ArrayList<OgVideoStructStub>(); // ???
        //    for(OgVideoStruct b : video) {
        //        _OgVideoStructList.add(OgVideoStructStub.convertBeanToStub(video));
        //    }
        //    setVideoStub(_OgVideoStructList);
        //}
    }

    @XmlTransient
    //@JsonIgnore
    public String getLocale()
    {
        return this.locale;
    }
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    @XmlTransient
    //@JsonIgnore
    public List<String> getLocaleAlternate()
    {
        return this.localeAlternate;
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        this.localeAlternate = localeAlternate;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlTransient
    //@JsonIgnore
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("url", this.url);
        dataMap.put("type", this.type);
        dataMap.put("siteName", this.siteName);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("fbAdmins", this.fbAdmins);
        dataMap.put("fbAppId", this.fbAppId);
        dataMap.put("image", this.image);
        dataMap.put("audio", this.audio);
        dataMap.put("video", this.video);
        dataMap.put("locale", this.locale);
        dataMap.put("localeAlternate", this.localeAlternate);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteName == null ? 0 : siteName.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = fbAdmins == null ? 0 : fbAdmins.hashCode();
        _hash = 31 * _hash + delta;
        delta = fbAppId == null ? 0 : fbAppId.hashCode();
        _hash = 31 * _hash + delta;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = audio == null ? 0 : audio.hashCode();
        _hash = 31 * _hash + delta;
        delta = video == null ? 0 : video.hashCode();
        _hash = 31 * _hash + delta;
        delta = locale == null ? 0 : locale.hashCode();
        _hash = 31 * _hash + delta;
        delta = localeAlternate == null ? 0 : localeAlternate.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgObjectBaseStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgObjectBaseStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgObjectBaseStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgObjectBaseStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgObjectBaseStub object as a string.", e);
        }
        
        return null;
    }
    public static OgObjectBaseStub fromJsonString(String jsonStr)
    {
        try {
            OgObjectBaseStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgObjectBaseStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgObjectBaseStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgObjectBaseStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgObjectBaseStub object.", e);
        }
        
        return null;
    }

}
