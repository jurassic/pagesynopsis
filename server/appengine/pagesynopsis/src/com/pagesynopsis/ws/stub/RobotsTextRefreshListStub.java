package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "robotsTextRefreshes")
@XmlType(propOrder = {"robotsTextRefresh", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextRefreshListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextRefreshListStub.class.getName());

    private List<RobotsTextRefreshStub> robotsTextRefreshes = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public RobotsTextRefreshListStub()
    {
        this(new ArrayList<RobotsTextRefreshStub>());
    }
    public RobotsTextRefreshListStub(List<RobotsTextRefreshStub> robotsTextRefreshes)
    {
        this(robotsTextRefreshes, null);
    }
    public RobotsTextRefreshListStub(List<RobotsTextRefreshStub> robotsTextRefreshes, String forwardCursor)
    {
        this.robotsTextRefreshes = robotsTextRefreshes;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(robotsTextRefreshes == null) {
            return true;
        } else {
            return robotsTextRefreshes.isEmpty();
        }
    }
    public int getSize()
    {
        if(robotsTextRefreshes == null) {
            return 0;
        } else {
            return robotsTextRefreshes.size();
        }
    }


    @XmlElement(name = "robotsTextRefresh")
    public List<RobotsTextRefreshStub> getRobotsTextRefresh()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<RobotsTextRefreshStub> getList()
    {
        return robotsTextRefreshes;
    }
    public void setList(List<RobotsTextRefreshStub> robotsTextRefreshes)
    {
        this.robotsTextRefreshes = robotsTextRefreshes;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<RobotsTextRefreshStub> it = this.robotsTextRefreshes.iterator();
        while(it.hasNext()) {
            RobotsTextRefreshStub robotsTextRefresh = it.next();
            sb.append(robotsTextRefresh.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RobotsTextRefreshListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RobotsTextRefreshListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextRefreshListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextRefreshListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextRefreshListStub object as a string.", e);
        }
        
        return null;
    }
    public static RobotsTextRefreshListStub fromJsonString(String jsonStr)
    {
        try {
            RobotsTextRefreshListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RobotsTextRefreshListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextRefreshListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextRefreshListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextRefreshListStub object.", e);
        }
        
        return null;
    }

}
