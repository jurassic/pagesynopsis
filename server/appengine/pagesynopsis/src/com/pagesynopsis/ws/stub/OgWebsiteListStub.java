package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogWebsites")
@XmlType(propOrder = {"ogWebsite", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgWebsiteListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgWebsiteListStub.class.getName());

    private List<OgWebsiteStub> ogWebsites = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgWebsiteListStub()
    {
        this(new ArrayList<OgWebsiteStub>());
    }
    public OgWebsiteListStub(List<OgWebsiteStub> ogWebsites)
    {
        this(ogWebsites, null);
    }
    public OgWebsiteListStub(List<OgWebsiteStub> ogWebsites, String forwardCursor)
    {
        this.ogWebsites = ogWebsites;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogWebsites == null) {
            return true;
        } else {
            return ogWebsites.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogWebsites == null) {
            return 0;
        } else {
            return ogWebsites.size();
        }
    }


    @XmlElement(name = "ogWebsite")
    public List<OgWebsiteStub> getOgWebsite()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgWebsiteStub> getList()
    {
        return ogWebsites;
    }
    public void setList(List<OgWebsiteStub> ogWebsites)
    {
        this.ogWebsites = ogWebsites;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgWebsiteStub> it = this.ogWebsites.iterator();
        while(it.hasNext()) {
            OgWebsiteStub ogWebsite = it.next();
            sb.append(ogWebsite.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgWebsiteListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgWebsiteListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgWebsiteListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgWebsiteListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgWebsiteListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgWebsiteListStub fromJsonString(String jsonStr)
    {
        try {
            OgWebsiteListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgWebsiteListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgWebsiteListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgWebsiteListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgWebsiteListStub object.", e);
        }
        
        return null;
    }

}
