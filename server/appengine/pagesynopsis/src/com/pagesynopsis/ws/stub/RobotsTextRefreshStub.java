package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "robotsTextRefresh")
@XmlType(propOrder = {"guid", "robotsTextFile", "refreshInterval", "note", "status", "refreshStatus", "result", "lastCheckedTime", "nextCheckedTime", "expirationTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextRefreshStub implements RobotsTextRefresh, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextRefreshStub.class.getName());

    private String guid;
    private String robotsTextFile;
    private Integer refreshInterval;
    private String note;
    private String status;
    private Integer refreshStatus;
    private String result;
    private Long lastCheckedTime;
    private Long nextCheckedTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    public RobotsTextRefreshStub()
    {
        this(null);
    }
    public RobotsTextRefreshStub(RobotsTextRefresh bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.robotsTextFile = bean.getRobotsTextFile();
            this.refreshInterval = bean.getRefreshInterval();
            this.note = bean.getNote();
            this.status = bean.getStatus();
            this.refreshStatus = bean.getRefreshStatus();
            this.result = bean.getResult();
            this.lastCheckedTime = bean.getLastCheckedTime();
            this.nextCheckedTime = bean.getNextCheckedTime();
            this.expirationTime = bean.getExpirationTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getRobotsTextFile()
    {
        return this.robotsTextFile;
    }
    public void setRobotsTextFile(String robotsTextFile)
    {
        this.robotsTextFile = robotsTextFile;
    }

    @XmlElement
    public Integer getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    @XmlElement
    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    @XmlElement
    public Integer getRefreshStatus()
    {
        return this.refreshStatus;
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        this.refreshStatus = refreshStatus;
    }

    @XmlElement
    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    @XmlElement
    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    @XmlElement
    public Long getNextCheckedTime()
    {
        return this.nextCheckedTime;
    }
    public void setNextCheckedTime(Long nextCheckedTime)
    {
        this.nextCheckedTime = nextCheckedTime;
    }

    @XmlElement
    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("robotsTextFile", this.robotsTextFile);
        dataMap.put("refreshInterval", this.refreshInterval);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("refreshStatus", this.refreshStatus);
        dataMap.put("result", this.result);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("nextCheckedTime", this.nextCheckedTime);
        dataMap.put("expirationTime", this.expirationTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = robotsTextFile == null ? 0 : robotsTextFile.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshStatus == null ? 0 : refreshStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextCheckedTime == null ? 0 : nextCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationTime == null ? 0 : expirationTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static RobotsTextRefreshStub convertBeanToStub(RobotsTextRefresh bean)
    {
        RobotsTextRefreshStub stub = null;
        if(bean instanceof RobotsTextRefreshStub) {
            stub = (RobotsTextRefreshStub) bean;
        } else {
            if(bean != null) {
                stub = new RobotsTextRefreshStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RobotsTextRefreshStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RobotsTextRefreshStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextRefreshStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextRefreshStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextRefreshStub object as a string.", e);
        }
        
        return null;
    }
    public static RobotsTextRefreshStub fromJsonString(String jsonStr)
    {
        try {
            RobotsTextRefreshStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RobotsTextRefreshStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextRefreshStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextRefreshStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextRefreshStub object.", e);
        }
        
        return null;
    }

}
