package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "twitterCardProductData")
@XmlType(propOrder = {"twitterCardProductData", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterCardProductDataListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardProductDataListStub.class.getName());

    private List<TwitterCardProductDataStub> twitterCardProductData = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public TwitterCardProductDataListStub()
    {
        this(new ArrayList<TwitterCardProductDataStub>());
    }
    public TwitterCardProductDataListStub(List<TwitterCardProductDataStub> twitterCardProductData)
    {
        this(twitterCardProductData, null);
    }
    public TwitterCardProductDataListStub(List<TwitterCardProductDataStub> twitterCardProductData, String forwardCursor)
    {
        this.twitterCardProductData = twitterCardProductData;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(twitterCardProductData == null) {
            return true;
        } else {
            return twitterCardProductData.isEmpty();
        }
    }
    public int getSize()
    {
        if(twitterCardProductData == null) {
            return 0;
        } else {
            return twitterCardProductData.size();
        }
    }


    @XmlElement(name = "twitterCardProductData")
    public List<TwitterCardProductDataStub> getTwitterCardProductData()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<TwitterCardProductDataStub> getList()
    {
        return twitterCardProductData;
    }
    public void setList(List<TwitterCardProductDataStub> twitterCardProductData)
    {
        this.twitterCardProductData = twitterCardProductData;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<TwitterCardProductDataStub> it = this.twitterCardProductData.iterator();
        while(it.hasNext()) {
            TwitterCardProductDataStub twitterCardProductData = it.next();
            sb.append(twitterCardProductData.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static TwitterCardProductDataListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of TwitterCardProductDataListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardProductDataListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardProductDataListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write TwitterCardProductDataListStub object as a string.", e);
        }
        
        return null;
    }
    public static TwitterCardProductDataListStub fromJsonString(String jsonStr)
    {
        try {
            TwitterCardProductDataListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, TwitterCardProductDataListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardProductDataListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardProductDataListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into TwitterCardProductDataListStub object.", e);
        }
        
        return null;
    }

}
