package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "videoSets")
@XmlType(propOrder = {"videoSet", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoSetListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VideoSetListStub.class.getName());

    private List<VideoSetStub> videoSets = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public VideoSetListStub()
    {
        this(new ArrayList<VideoSetStub>());
    }
    public VideoSetListStub(List<VideoSetStub> videoSets)
    {
        this(videoSets, null);
    }
    public VideoSetListStub(List<VideoSetStub> videoSets, String forwardCursor)
    {
        this.videoSets = videoSets;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(videoSets == null) {
            return true;
        } else {
            return videoSets.isEmpty();
        }
    }
    public int getSize()
    {
        if(videoSets == null) {
            return 0;
        } else {
            return videoSets.size();
        }
    }


    @XmlElement(name = "videoSet")
    public List<VideoSetStub> getVideoSet()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<VideoSetStub> getList()
    {
        return videoSets;
    }
    public void setList(List<VideoSetStub> videoSets)
    {
        this.videoSets = videoSets;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<VideoSetStub> it = this.videoSets.iterator();
        while(it.hasNext()) {
            VideoSetStub videoSet = it.next();
            sb.append(videoSet.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static VideoSetListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of VideoSetListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write VideoSetListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write VideoSetListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write VideoSetListStub object as a string.", e);
        }
        
        return null;
    }
    public static VideoSetListStub fromJsonString(String jsonStr)
    {
        try {
            VideoSetListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, VideoSetListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoSetListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoSetListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into VideoSetListStub object.", e);
        }
        
        return null;
    }

}
