package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "linkLists")
@XmlType(propOrder = {"linkList", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkListListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkListListStub.class.getName());

    private List<LinkListStub> linkLists = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public LinkListListStub()
    {
        this(new ArrayList<LinkListStub>());
    }
    public LinkListListStub(List<LinkListStub> linkLists)
    {
        this(linkLists, null);
    }
    public LinkListListStub(List<LinkListStub> linkLists, String forwardCursor)
    {
        this.linkLists = linkLists;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(linkLists == null) {
            return true;
        } else {
            return linkLists.isEmpty();
        }
    }
    public int getSize()
    {
        if(linkLists == null) {
            return 0;
        } else {
            return linkLists.size();
        }
    }


    @XmlElement(name = "linkList")
    public List<LinkListStub> getLinkList()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<LinkListStub> getList()
    {
        return linkLists;
    }
    public void setList(List<LinkListStub> linkLists)
    {
        this.linkLists = linkLists;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<LinkListStub> it = this.linkLists.iterator();
        while(it.hasNext()) {
            LinkListStub linkList = it.next();
            sb.append(linkList.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static LinkListListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of LinkListListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write LinkListListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write LinkListListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write LinkListListStub object as a string.", e);
        }
        
        return null;
    }
    public static LinkListListStub fromJsonString(String jsonStr)
    {
        try {
            LinkListListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, LinkListListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkListListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkListListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into LinkListListStub object.", e);
        }
        
        return null;
    }

}
