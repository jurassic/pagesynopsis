package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "decodedQueryParamStructs")
@XmlType(propOrder = {"decodedQueryParamStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DecodedQueryParamStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructListStub.class.getName());

    private List<DecodedQueryParamStructStub> decodedQueryParamStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public DecodedQueryParamStructListStub()
    {
        this(new ArrayList<DecodedQueryParamStructStub>());
    }
    public DecodedQueryParamStructListStub(List<DecodedQueryParamStructStub> decodedQueryParamStructs)
    {
        this(decodedQueryParamStructs, null);
    }
    public DecodedQueryParamStructListStub(List<DecodedQueryParamStructStub> decodedQueryParamStructs, String forwardCursor)
    {
        this.decodedQueryParamStructs = decodedQueryParamStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(decodedQueryParamStructs == null) {
            return true;
        } else {
            return decodedQueryParamStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(decodedQueryParamStructs == null) {
            return 0;
        } else {
            return decodedQueryParamStructs.size();
        }
    }


    @XmlElement(name = "decodedQueryParamStruct")
    public List<DecodedQueryParamStructStub> getDecodedQueryParamStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<DecodedQueryParamStructStub> getList()
    {
        return decodedQueryParamStructs;
    }
    public void setList(List<DecodedQueryParamStructStub> decodedQueryParamStructs)
    {
        this.decodedQueryParamStructs = decodedQueryParamStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<DecodedQueryParamStructStub> it = this.decodedQueryParamStructs.iterator();
        while(it.hasNext()) {
            DecodedQueryParamStructStub decodedQueryParamStruct = it.next();
            sb.append(decodedQueryParamStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static DecodedQueryParamStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of DecodedQueryParamStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write DecodedQueryParamStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write DecodedQueryParamStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write DecodedQueryParamStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static DecodedQueryParamStructListStub fromJsonString(String jsonStr)
    {
        try {
            DecodedQueryParamStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, DecodedQueryParamStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into DecodedQueryParamStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into DecodedQueryParamStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into DecodedQueryParamStructListStub object.", e);
        }
        
        return null;
    }

}
