package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "urlStruct")
@XmlType(propOrder = {"uuid", "statusCode", "redirectUrl", "absoluteUrl", "hashFragment", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlStructStub implements UrlStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlStructStub.class.getName());

    private String uuid;
    private Integer statusCode;
    private String redirectUrl;
    private String absoluteUrl;
    private String hashFragment;
    private String note;

    public UrlStructStub()
    {
        this(null);
    }
    public UrlStructStub(UrlStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.statusCode = bean.getStatusCode();
            this.redirectUrl = bean.getRedirectUrl();
            this.absoluteUrl = bean.getAbsoluteUrl();
            this.hashFragment = bean.getHashFragment();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public Integer getStatusCode()
    {
        return this.statusCode;
    }
    public void setStatusCode(Integer statusCode)
    {
        this.statusCode = statusCode;
    }

    @XmlElement
    public String getRedirectUrl()
    {
        return this.redirectUrl;
    }
    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }

    @XmlElement
    public String getAbsoluteUrl()
    {
        return this.absoluteUrl;
    }
    public void setAbsoluteUrl(String absoluteUrl)
    {
        this.absoluteUrl = absoluteUrl;
    }

    @XmlElement
    public String getHashFragment()
    {
        return this.hashFragment;
    }
    public void setHashFragment(String hashFragment)
    {
        this.hashFragment = hashFragment;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStatusCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAbsoluteUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashFragment() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("statusCode", this.statusCode);
        dataMap.put("redirectUrl", this.redirectUrl);
        dataMap.put("absoluteUrl", this.absoluteUrl);
        dataMap.put("hashFragment", this.hashFragment);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = statusCode == null ? 0 : statusCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectUrl == null ? 0 : redirectUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = absoluteUrl == null ? 0 : absoluteUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = hashFragment == null ? 0 : hashFragment.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UrlStructStub convertBeanToStub(UrlStruct bean)
    {
        UrlStructStub stub = null;
        if(bean instanceof UrlStructStub) {
            stub = (UrlStructStub) bean;
        } else {
            if(bean != null) {
                stub = new UrlStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UrlStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UrlStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UrlStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UrlStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UrlStructStub object as a string.", e);
        }
        
        return null;
    }
    public static UrlStructStub fromJsonString(String jsonStr)
    {
        try {
            UrlStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UrlStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UrlStructStub object.", e);
        }
        
        return null;
    }

}
