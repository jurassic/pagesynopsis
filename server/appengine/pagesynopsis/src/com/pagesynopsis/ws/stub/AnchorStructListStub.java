package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "anchorStructs")
@XmlType(propOrder = {"anchorStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnchorStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AnchorStructListStub.class.getName());

    private List<AnchorStructStub> anchorStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public AnchorStructListStub()
    {
        this(new ArrayList<AnchorStructStub>());
    }
    public AnchorStructListStub(List<AnchorStructStub> anchorStructs)
    {
        this(anchorStructs, null);
    }
    public AnchorStructListStub(List<AnchorStructStub> anchorStructs, String forwardCursor)
    {
        this.anchorStructs = anchorStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(anchorStructs == null) {
            return true;
        } else {
            return anchorStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(anchorStructs == null) {
            return 0;
        } else {
            return anchorStructs.size();
        }
    }


    @XmlElement(name = "anchorStruct")
    public List<AnchorStructStub> getAnchorStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AnchorStructStub> getList()
    {
        return anchorStructs;
    }
    public void setList(List<AnchorStructStub> anchorStructs)
    {
        this.anchorStructs = anchorStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<AnchorStructStub> it = this.anchorStructs.iterator();
        while(it.hasNext()) {
            AnchorStructStub anchorStruct = it.next();
            sb.append(anchorStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AnchorStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AnchorStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AnchorStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AnchorStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AnchorStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static AnchorStructListStub fromJsonString(String jsonStr)
    {
        try {
            AnchorStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AnchorStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AnchorStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AnchorStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AnchorStructListStub object.", e);
        }
        
        return null;
    }

}
