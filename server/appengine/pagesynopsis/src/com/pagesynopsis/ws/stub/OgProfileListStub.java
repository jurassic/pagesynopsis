package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogProfiles")
@XmlType(propOrder = {"ogProfile", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgProfileListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgProfileListStub.class.getName());

    private List<OgProfileStub> ogProfiles = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgProfileListStub()
    {
        this(new ArrayList<OgProfileStub>());
    }
    public OgProfileListStub(List<OgProfileStub> ogProfiles)
    {
        this(ogProfiles, null);
    }
    public OgProfileListStub(List<OgProfileStub> ogProfiles, String forwardCursor)
    {
        this.ogProfiles = ogProfiles;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogProfiles == null) {
            return true;
        } else {
            return ogProfiles.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogProfiles == null) {
            return 0;
        } else {
            return ogProfiles.size();
        }
    }


    @XmlElement(name = "ogProfile")
    public List<OgProfileStub> getOgProfile()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgProfileStub> getList()
    {
        return ogProfiles;
    }
    public void setList(List<OgProfileStub> ogProfiles)
    {
        this.ogProfiles = ogProfiles;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgProfileStub> it = this.ogProfiles.iterator();
        while(it.hasNext()) {
            OgProfileStub ogProfile = it.next();
            sb.append(ogProfile.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgProfileListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgProfileListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgProfileListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgProfileListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgProfileListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgProfileListStub fromJsonString(String jsonStr)
    {
        try {
            OgProfileListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgProfileListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgProfileListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgProfileListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgProfileListStub object.", e);
        }
        
        return null;
    }

}
