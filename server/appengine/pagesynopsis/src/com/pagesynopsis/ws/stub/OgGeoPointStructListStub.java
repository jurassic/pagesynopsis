package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogGeoPointStructs")
@XmlType(propOrder = {"ogGeoPointStruct", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgGeoPointStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgGeoPointStructListStub.class.getName());

    private List<OgGeoPointStructStub> ogGeoPointStructs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgGeoPointStructListStub()
    {
        this(new ArrayList<OgGeoPointStructStub>());
    }
    public OgGeoPointStructListStub(List<OgGeoPointStructStub> ogGeoPointStructs)
    {
        this(ogGeoPointStructs, null);
    }
    public OgGeoPointStructListStub(List<OgGeoPointStructStub> ogGeoPointStructs, String forwardCursor)
    {
        this.ogGeoPointStructs = ogGeoPointStructs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogGeoPointStructs == null) {
            return true;
        } else {
            return ogGeoPointStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogGeoPointStructs == null) {
            return 0;
        } else {
            return ogGeoPointStructs.size();
        }
    }


    @XmlElement(name = "ogGeoPointStruct")
    public List<OgGeoPointStructStub> getOgGeoPointStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgGeoPointStructStub> getList()
    {
        return ogGeoPointStructs;
    }
    public void setList(List<OgGeoPointStructStub> ogGeoPointStructs)
    {
        this.ogGeoPointStructs = ogGeoPointStructs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgGeoPointStructStub> it = this.ogGeoPointStructs.iterator();
        while(it.hasNext()) {
            OgGeoPointStructStub ogGeoPointStruct = it.next();
            sb.append(ogGeoPointStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgGeoPointStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgGeoPointStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgGeoPointStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgGeoPointStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgGeoPointStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgGeoPointStructListStub fromJsonString(String jsonStr)
    {
        try {
            OgGeoPointStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgGeoPointStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgGeoPointStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgGeoPointStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgGeoPointStructListStub object.", e);
        }
        
        return null;
    }

}
