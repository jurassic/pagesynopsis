package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "robotsTextFile")
@XmlType(propOrder = {"guid", "siteUrl", "groupsStub", "sitemaps", "content", "contentHash", "lastCheckedTime", "lastUpdatedTime", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextFileStub implements RobotsTextFile, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileStub.class.getName());

    private String guid;
    private String siteUrl;
    private List<RobotsTextGroupStub> groups;
    private List<String> sitemaps;
    private String content;
    private String contentHash;
    private Long lastCheckedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    public RobotsTextFileStub()
    {
        this(null);
    }
    public RobotsTextFileStub(RobotsTextFile bean)
    {
        if(bean != null) {
            this.guid = bean.getGuid();
            this.siteUrl = bean.getSiteUrl();
            List<RobotsTextGroupStub> _groupsStubs = null;
            if(bean.getGroups() != null) {
                _groupsStubs = new ArrayList<RobotsTextGroupStub>();
                for(RobotsTextGroup b : bean.getGroups()) {
                    _groupsStubs.add(RobotsTextGroupStub.convertBeanToStub(b));
                }
            }
            this.groups = _groupsStubs;
            this.sitemaps = bean.getSitemaps();
            this.content = bean.getContent();
            this.contentHash = bean.getContentHash();
            this.lastCheckedTime = bean.getLastCheckedTime();
            this.lastUpdatedTime = bean.getLastUpdatedTime();
            this.createdTime = bean.getCreatedTime();
            this.modifiedTime = bean.getModifiedTime();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @XmlElement
    public String getSiteUrl()
    {
        return this.siteUrl;
    }
    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    @XmlElement(name = "groups")
    @JsonIgnore
    public List<RobotsTextGroupStub> getGroupsStub()
    {
        return this.groups;
    }
    public void setGroupsStub(List<RobotsTextGroupStub> groups)
    {
        this.groups = groups;  // Clone???
    }
    @XmlTransient
    @JsonDeserialize(contentAs=RobotsTextGroupStub.class)
    public List<RobotsTextGroup> getGroups()
    {  
        return (List<RobotsTextGroup>) ((List<?>) getGroupsStub());
    }
    public void setGroups(List<RobotsTextGroup> groups)
    {
        // TBD
        //if(groups instanceof List<RobotsTextGroupStub>) {
            setGroupsStub((List<RobotsTextGroupStub>) ((List<?>) groups));
        //} else {
        //    // TBD
        //    List<RobotsTextGroupStub> _RobotsTextGroupList = new ArrayList<RobotsTextGroupStub>(); // ???
        //    for(RobotsTextGroup b : groups) {
        //        _RobotsTextGroupList.add(RobotsTextGroupStub.convertBeanToStub(groups));
        //    }
        //    setGroupsStub(_RobotsTextGroupList);
        //}
    }

    @XmlElement
    public List<String> getSitemaps()
    {
        return this.sitemaps;
    }
    public void setSitemaps(List<String> sitemaps)
    {
        this.sitemaps = sitemaps;
    }

    @XmlElement
    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    @XmlElement
    public String getContentHash()
    {
        return this.contentHash;
    }
    public void setContentHash(String contentHash)
    {
        this.contentHash = contentHash;
    }

    @XmlElement
    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    @XmlElement
    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("guid", this.guid);
        dataMap.put("siteUrl", this.siteUrl);
        dataMap.put("groups", this.groups);
        dataMap.put("sitemaps", this.sitemaps);
        dataMap.put("content", this.content);
        dataMap.put("contentHash", this.contentHash);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);
        dataMap.put("createdTime", this.createdTime);
        dataMap.put("modifiedTime", this.modifiedTime);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteUrl == null ? 0 : siteUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = groups == null ? 0 : groups.hashCode();
        _hash = 31 * _hash + delta;
        delta = sitemaps == null ? 0 : sitemaps.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentHash == null ? 0 : contentHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = createdTime == null ? 0 : createdTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static RobotsTextFileStub convertBeanToStub(RobotsTextFile bean)
    {
        RobotsTextFileStub stub = null;
        if(bean instanceof RobotsTextFileStub) {
            stub = (RobotsTextFileStub) bean;
        } else {
            if(bean != null) {
                stub = new RobotsTextFileStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static RobotsTextFileStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of RobotsTextFileStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextFileStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextFileStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write RobotsTextFileStub object as a string.", e);
        }
        
        return null;
    }
    public static RobotsTextFileStub fromJsonString(String jsonStr)
    {
        try {
            RobotsTextFileStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, RobotsTextFileStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextFileStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextFileStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into RobotsTextFileStub object.", e);
        }
        
        return null;
    }

}
