package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.UserWebsiteStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "userWebsiteStruct")
@XmlType(propOrder = {"uuid", "primarySite", "homePage", "blogSite", "portfolioSite", "twitterProfile", "facebookProfile", "googlePlusProfile", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWebsiteStructStub implements UserWebsiteStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserWebsiteStructStub.class.getName());

    private String uuid;
    private String primarySite;
    private String homePage;
    private String blogSite;
    private String portfolioSite;
    private String twitterProfile;
    private String facebookProfile;
    private String googlePlusProfile;
    private String note;

    public UserWebsiteStructStub()
    {
        this(null);
    }
    public UserWebsiteStructStub(UserWebsiteStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.primarySite = bean.getPrimarySite();
            this.homePage = bean.getHomePage();
            this.blogSite = bean.getBlogSite();
            this.portfolioSite = bean.getPortfolioSite();
            this.twitterProfile = bean.getTwitterProfile();
            this.facebookProfile = bean.getFacebookProfile();
            this.googlePlusProfile = bean.getGooglePlusProfile();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getPrimarySite()
    {
        return this.primarySite;
    }
    public void setPrimarySite(String primarySite)
    {
        this.primarySite = primarySite;
    }

    @XmlElement
    public String getHomePage()
    {
        return this.homePage;
    }
    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    @XmlElement
    public String getBlogSite()
    {
        return this.blogSite;
    }
    public void setBlogSite(String blogSite)
    {
        this.blogSite = blogSite;
    }

    @XmlElement
    public String getPortfolioSite()
    {
        return this.portfolioSite;
    }
    public void setPortfolioSite(String portfolioSite)
    {
        this.portfolioSite = portfolioSite;
    }

    @XmlElement
    public String getTwitterProfile()
    {
        return this.twitterProfile;
    }
    public void setTwitterProfile(String twitterProfile)
    {
        this.twitterProfile = twitterProfile;
    }

    @XmlElement
    public String getFacebookProfile()
    {
        return this.facebookProfile;
    }
    public void setFacebookProfile(String facebookProfile)
    {
        this.facebookProfile = facebookProfile;
    }

    @XmlElement
    public String getGooglePlusProfile()
    {
        return this.googlePlusProfile;
    }
    public void setGooglePlusProfile(String googlePlusProfile)
    {
        this.googlePlusProfile = googlePlusProfile;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPrimarySite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHomePage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getBlogSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPortfolioSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGooglePlusProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("primarySite", this.primarySite);
        dataMap.put("homePage", this.homePage);
        dataMap.put("blogSite", this.blogSite);
        dataMap.put("portfolioSite", this.portfolioSite);
        dataMap.put("twitterProfile", this.twitterProfile);
        dataMap.put("facebookProfile", this.facebookProfile);
        dataMap.put("googlePlusProfile", this.googlePlusProfile);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = primarySite == null ? 0 : primarySite.hashCode();
        _hash = 31 * _hash + delta;
        delta = homePage == null ? 0 : homePage.hashCode();
        _hash = 31 * _hash + delta;
        delta = blogSite == null ? 0 : blogSite.hashCode();
        _hash = 31 * _hash + delta;
        delta = portfolioSite == null ? 0 : portfolioSite.hashCode();
        _hash = 31 * _hash + delta;
        delta = twitterProfile == null ? 0 : twitterProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = facebookProfile == null ? 0 : facebookProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = googlePlusProfile == null ? 0 : googlePlusProfile.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserWebsiteStructStub convertBeanToStub(UserWebsiteStruct bean)
    {
        UserWebsiteStructStub stub = null;
        if(bean instanceof UserWebsiteStructStub) {
            stub = (UserWebsiteStructStub) bean;
        } else {
            if(bean != null) {
                stub = new UserWebsiteStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserWebsiteStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserWebsiteStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserWebsiteStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserWebsiteStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserWebsiteStructStub object as a string.", e);
        }
        
        return null;
    }
    public static UserWebsiteStructStub fromJsonString(String jsonStr)
    {
        try {
            UserWebsiteStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserWebsiteStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserWebsiteStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserWebsiteStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserWebsiteStructStub object.", e);
        }
        
        return null;
    }

}
