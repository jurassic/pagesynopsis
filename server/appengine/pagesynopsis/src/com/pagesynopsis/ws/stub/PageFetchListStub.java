package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "pageFetches")
@XmlType(propOrder = {"pageFetch", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageFetchListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageFetchListStub.class.getName());

    private List<PageFetchStub> pageFetches = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public PageFetchListStub()
    {
        this(new ArrayList<PageFetchStub>());
    }
    public PageFetchListStub(List<PageFetchStub> pageFetches)
    {
        this(pageFetches, null);
    }
    public PageFetchListStub(List<PageFetchStub> pageFetches, String forwardCursor)
    {
        this.pageFetches = pageFetches;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(pageFetches == null) {
            return true;
        } else {
            return pageFetches.isEmpty();
        }
    }
    public int getSize()
    {
        if(pageFetches == null) {
            return 0;
        } else {
            return pageFetches.size();
        }
    }


    @XmlElement(name = "pageFetch")
    public List<PageFetchStub> getPageFetch()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<PageFetchStub> getList()
    {
        return pageFetches;
    }
    public void setList(List<PageFetchStub> pageFetches)
    {
        this.pageFetches = pageFetches;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<PageFetchStub> it = this.pageFetches.iterator();
        while(it.hasNext()) {
            PageFetchStub pageFetch = it.next();
            sb.append(pageFetch.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static PageFetchListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of PageFetchListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write PageFetchListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write PageFetchListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write PageFetchListStub object as a string.", e);
        }
        
        return null;
    }
    public static PageFetchListStub fromJsonString(String jsonStr)
    {
        try {
            PageFetchListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, PageFetchListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into PageFetchListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into PageFetchListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into PageFetchListStub object.", e);
        }
        
        return null;
    }

}
