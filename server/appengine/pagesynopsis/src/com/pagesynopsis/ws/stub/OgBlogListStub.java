package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "ogBlogs")
@XmlType(propOrder = {"ogBlog", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OgBlogListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgBlogListStub.class.getName());

    private List<OgBlogStub> ogBlogs = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OgBlogListStub()
    {
        this(new ArrayList<OgBlogStub>());
    }
    public OgBlogListStub(List<OgBlogStub> ogBlogs)
    {
        this(ogBlogs, null);
    }
    public OgBlogListStub(List<OgBlogStub> ogBlogs, String forwardCursor)
    {
        this.ogBlogs = ogBlogs;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(ogBlogs == null) {
            return true;
        } else {
            return ogBlogs.isEmpty();
        }
    }
    public int getSize()
    {
        if(ogBlogs == null) {
            return 0;
        } else {
            return ogBlogs.size();
        }
    }


    @XmlElement(name = "ogBlog")
    public List<OgBlogStub> getOgBlog()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OgBlogStub> getList()
    {
        return ogBlogs;
    }
    public void setList(List<OgBlogStub> ogBlogs)
    {
        this.ogBlogs = ogBlogs;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OgBlogStub> it = this.ogBlogs.iterator();
        while(it.hasNext()) {
            OgBlogStub ogBlog = it.next();
            sb.append(ogBlog.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OgBlogListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OgBlogListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OgBlogListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OgBlogListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OgBlogListStub object as a string.", e);
        }
        
        return null;
    }
    public static OgBlogListStub fromJsonString(String jsonStr)
    {
        try {
            OgBlogListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OgBlogListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OgBlogListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OgBlogListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OgBlogListStub object.", e);
        }
        
        return null;
    }

}
