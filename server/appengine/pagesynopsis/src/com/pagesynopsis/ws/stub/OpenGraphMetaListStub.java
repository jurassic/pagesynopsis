package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "openGraphMetas")
@XmlType(propOrder = {"openGraphMeta", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenGraphMetaListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OpenGraphMetaListStub.class.getName());

    private List<OpenGraphMetaStub> openGraphMetas = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public OpenGraphMetaListStub()
    {
        this(new ArrayList<OpenGraphMetaStub>());
    }
    public OpenGraphMetaListStub(List<OpenGraphMetaStub> openGraphMetas)
    {
        this(openGraphMetas, null);
    }
    public OpenGraphMetaListStub(List<OpenGraphMetaStub> openGraphMetas, String forwardCursor)
    {
        this.openGraphMetas = openGraphMetas;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(openGraphMetas == null) {
            return true;
        } else {
            return openGraphMetas.isEmpty();
        }
    }
    public int getSize()
    {
        if(openGraphMetas == null) {
            return 0;
        } else {
            return openGraphMetas.size();
        }
    }


    @XmlElement(name = "openGraphMeta")
    public List<OpenGraphMetaStub> getOpenGraphMeta()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<OpenGraphMetaStub> getList()
    {
        return openGraphMetas;
    }
    public void setList(List<OpenGraphMetaStub> openGraphMetas)
    {
        this.openGraphMetas = openGraphMetas;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<OpenGraphMetaStub> it = this.openGraphMetas.iterator();
        while(it.hasNext()) {
            OpenGraphMetaStub openGraphMeta = it.next();
            sb.append(openGraphMeta.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static OpenGraphMetaListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of OpenGraphMetaListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write OpenGraphMetaListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write OpenGraphMetaListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write OpenGraphMetaListStub object as a string.", e);
        }
        
        return null;
    }
    public static OpenGraphMetaListStub fromJsonString(String jsonStr)
    {
        try {
            OpenGraphMetaListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, OpenGraphMetaListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into OpenGraphMetaListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into OpenGraphMetaListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into OpenGraphMetaListStub object.", e);
        }
        
        return null;
    }

}
