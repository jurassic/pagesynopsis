package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "audioSets")
@XmlType(propOrder = {"audioSet", "forwardCursor"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AudioSetListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AudioSetListStub.class.getName());

    private List<AudioSetStub> audioSets = null;
    private String forwardCursor = null;     // Points to the last element in a list, if any.

    public AudioSetListStub()
    {
        this(new ArrayList<AudioSetStub>());
    }
    public AudioSetListStub(List<AudioSetStub> audioSets)
    {
        this(audioSets, null);
    }
    public AudioSetListStub(List<AudioSetStub> audioSets, String forwardCursor)
    {
        this.audioSets = audioSets;
        this.forwardCursor = forwardCursor;
    }

    public boolean isEmpty()
    {
        if(audioSets == null) {
            return true;
        } else {
            return audioSets.isEmpty();
        }
    }
    public int getSize()
    {
        if(audioSets == null) {
            return 0;
        } else {
            return audioSets.size();
        }
    }


    @XmlElement(name = "audioSet")
    public List<AudioSetStub> getAudioSet()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<AudioSetStub> getList()
    {
        return audioSets;
    }
    public void setList(List<AudioSetStub> audioSets)
    {
        this.audioSets = audioSets;
    }

    @XmlElement(name = "forwardCursor")
    public String getForwardCursor()
    {
        return forwardCursor;
    }
    public void setForwardCursor(String forwardCursor)
    {
        this.forwardCursor = forwardCursor;
    }


    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{[");
        Iterator<AudioSetStub> it = this.audioSets.iterator();
        while(it.hasNext()) {
            AudioSetStub audioSet = it.next();
            sb.append(audioSet.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("],forwardCursor:");
        sb.append(forwardCursor);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static AudioSetListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of AudioSetListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write AudioSetListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write AudioSetListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write AudioSetListStub object as a string.", e);
        }
        
        return null;
    }
    public static AudioSetListStub fromJsonString(String jsonStr)
    {
        try {
            AudioSetListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, AudioSetListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioSetListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioSetListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into AudioSetListStub object.", e);
        }
        
        return null;
    }

}
