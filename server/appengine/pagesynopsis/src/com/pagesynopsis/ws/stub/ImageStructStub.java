package com.pagesynopsis.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.util.JsonUtil;


@XmlRootElement(name = "imageStruct")
@XmlType(propOrder = {"uuid", "id", "alt", "src", "srcUrl", "mediaType", "widthAttr", "width", "heightAttr", "height", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageStructStub implements ImageStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageStructStub.class.getName());

    private String uuid;
    private String id;
    private String alt;
    private String src;
    private String srcUrl;
    private String mediaType;
    private String widthAttr;
    private Integer width;
    private String heightAttr;
    private Integer height;
    private String note;

    public ImageStructStub()
    {
        this(null);
    }
    public ImageStructStub(ImageStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.id = bean.getId();
            this.alt = bean.getAlt();
            this.src = bean.getSrc();
            this.srcUrl = bean.getSrcUrl();
            this.mediaType = bean.getMediaType();
            this.widthAttr = bean.getWidthAttr();
            this.width = bean.getWidth();
            this.heightAttr = bean.getHeightAttr();
            this.height = bean.getHeight();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    @XmlElement
    public String getAlt()
    {
        return this.alt;
    }
    public void setAlt(String alt)
    {
        this.alt = alt;
    }

    @XmlElement
    public String getSrc()
    {
        return this.src;
    }
    public void setSrc(String src)
    {
        this.src = src;
    }

    @XmlElement
    public String getSrcUrl()
    {
        return this.srcUrl;
    }
    public void setSrcUrl(String srcUrl)
    {
        this.srcUrl = srcUrl;
    }

    @XmlElement
    public String getMediaType()
    {
        return this.mediaType;
    }
    public void setMediaType(String mediaType)
    {
        this.mediaType = mediaType;
    }

    @XmlElement
    public String getWidthAttr()
    {
        return this.widthAttr;
    }
    public void setWidthAttr(String widthAttr)
    {
        this.widthAttr = widthAttr;
    }

    @XmlElement
    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
    }

    @XmlElement
    public String getHeightAttr()
    {
        return this.heightAttr;
    }
    public void setHeightAttr(String heightAttr)
    {
        this.heightAttr = heightAttr;
    }

    @XmlElement
    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMediaType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidthAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeightAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("id", this.id);
        dataMap.put("alt", this.alt);
        dataMap.put("src", this.src);
        dataMap.put("srcUrl", this.srcUrl);
        dataMap.put("mediaType", this.mediaType);
        dataMap.put("widthAttr", this.widthAttr);
        dataMap.put("width", this.width);
        dataMap.put("heightAttr", this.heightAttr);
        dataMap.put("height", this.height);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = alt == null ? 0 : alt.hashCode();
        _hash = 31 * _hash + delta;
        delta = src == null ? 0 : src.hashCode();
        _hash = 31 * _hash + delta;
        delta = srcUrl == null ? 0 : srcUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = mediaType == null ? 0 : mediaType.hashCode();
        _hash = 31 * _hash + delta;
        delta = widthAttr == null ? 0 : widthAttr.hashCode();
        _hash = 31 * _hash + delta;
        delta = width == null ? 0 : width.hashCode();
        _hash = 31 * _hash + delta;
        delta = heightAttr == null ? 0 : heightAttr.hashCode();
        _hash = 31 * _hash + delta;
        delta = height == null ? 0 : height.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static ImageStructStub convertBeanToStub(ImageStruct bean)
    {
        ImageStructStub stub = null;
        if(bean instanceof ImageStructStub) {
            stub = (ImageStructStub) bean;
        } else {
            if(bean != null) {
                stub = new ImageStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static ImageStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of ImageStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write ImageStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write ImageStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write ImageStructStub object as a string.", e);
        }
        
        return null;
    }
    public static ImageStructStub fromJsonString(String jsonStr)
    {
        try {
            ImageStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, ImageStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into ImageStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into ImageStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into ImageStructStub object.", e);
        }
        
        return null;
    }

}
