package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.stub.UrlStructStub;


public class UrlStructResourceUtil
{
    private static final Logger log = Logger.getLogger(UrlStructResourceUtil.class.getName());

    // Static methods only.
    private UrlStructResourceUtil() {}

    public static UrlStructBean convertUrlStructStubToBean(UrlStruct stub)
    {
        UrlStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null UrlStructBean is returned.");
        } else {
            bean = new UrlStructBean();
            bean.setUuid(stub.getUuid());
            bean.setStatusCode(stub.getStatusCode());
            bean.setRedirectUrl(stub.getRedirectUrl());
            bean.setAbsoluteUrl(stub.getAbsoluteUrl());
            bean.setHashFragment(stub.getHashFragment());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
