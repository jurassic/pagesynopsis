package com.pagesynopsis.ws.resource.async;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.DataStoreException;
import com.pagesynopsis.ws.exception.ResourceGoneException;
import com.pagesynopsis.ws.exception.ResourceNotFoundException;
import com.pagesynopsis.ws.exception.ResourceAlreadyPresentException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.NotImplementedRsException;
import com.pagesynopsis.ws.resource.exception.RequestConflictRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.DataStoreRsException;
import com.pagesynopsis.ws.resource.exception.ResourceGoneRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ResourceAlreadyPresentRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.bean.ImageSetBean;
import com.pagesynopsis.ws.stub.ImageSetListStub;
import com.pagesynopsis.ws.stub.ImageSetStub;
import com.pagesynopsis.ws.resource.ServiceManager;
import com.pagesynopsis.ws.resource.ImageSetResource;
import com.pagesynopsis.ws.resource.util.UrlStructResourceUtil;
import com.pagesynopsis.ws.resource.util.ImageStructResourceUtil;
import com.pagesynopsis.ws.resource.util.AnchorStructResourceUtil;
import com.pagesynopsis.ws.resource.util.KeyValuePairStructResourceUtil;
import com.pagesynopsis.ws.resource.util.AudioStructResourceUtil;
import com.pagesynopsis.ws.resource.util.VideoStructResourceUtil;


@Path("/_task/w/imageSets/")
public class AsyncImageSetResource extends BaseAsyncResource implements ImageSetResource
{
    private static final Logger log = Logger.getLogger(AsyncImageSetResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private String queueName = null;
    private String taskName = null;
    private Integer retryCount = null;
    private boolean dummyPayload = false;

    public AsyncImageSetResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();
        List<String> qns = httpHeaders.getRequestHeader("X-AppEngine-QueueName");
        if(qns != null && qns.size() > 0) {
            this.queueName = qns.get(0);
        }
        List<String> tns = httpHeaders.getRequestHeader("X-AppEngine-TaskName");
        if(tns != null && tns.size() > 0) {
            this.taskName = tns.get(0);
        }
        List<String> rcs = httpHeaders.getRequestHeader("X-AppEngine-TaskRetryCount");
        if(rcs != null && rcs.size() > 0) {
            String strCount = rcs.get(0);
            try {
                this.retryCount = Integer.parseInt(strCount);
            } catch(NumberFormatException ex) {
                // ignore.
                //this.retryCount = 0;
            }
        }
        List<String> ats = httpHeaders.getRequestHeader("X-AsyncTask-Payload");
        if(ats != null && ats.size() > 0 && ats.get(0).equals("DummyPayload")) {
            this.dummyPayload = true;
        }
    }

    private Response getImageSetList(List<ImageSet> beans) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getImageSetKeys(List<String> guids) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getCount(String filter, String params, List<String> values, String aggregate) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getImageSet(String guid) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response getImageSet(String guid, String field) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response createImageSet(ImageSetStub imageSet) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createImageSet(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            ImageSetBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ImageSetStub realStub = (ImageSetStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertImageSetStubToBean(realStub);
            } else {
                bean = convertImageSetStubToBean(imageSet);
            }
            String guid = ServiceManager.getImageSetService().createImageSet(bean);
            URI createdUri = URI.create(resourceUri + "/" + guid);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createImageSet(): Successfully processed the request: createdUri = " + createdUri.toString());
            return Response.created(createdUri).entity(guid).type(MediaType.TEXT_PLAIN).build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(ResourceAlreadyPresentException ex) {
            throw new ResourceAlreadyPresentRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateImageSet(String guid, ImageSetStub imageSet) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateImageSet(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            if(imageSet == null || !guid.equals(imageSet.getGuid())) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Path param guid = " + guid + " is different from imageSet guid = " + imageSet.getGuid());
                throw new RequestForbiddenException("Failed to update the imageSet with guid = " + guid);
            }
            ImageSetBean bean = null;
            if(dummyPayload) {
                if(getCache() == null) {
                    throw new ServiceUnavailableRsException("Cache service is unavailable.", resourceUri);
                }
                ImageSetStub realStub = (ImageSetStub) getCache().get(taskName);
                if(realStub == null) {
                    // Cached object is likely gone/deleted. No point of retrying...
                    // throw new InternalServerErrorRsException("Failed to retrieve the real stub object from cache.", resourceUri);
                    // Just return 2xx status code so that there will be no more retries...
                    log.severe("Failed to retrieve the real stub object from cache. This task will be aborted.");
                    return Response.noContent().build();   // ????
                }
                if(log.isLoggable(Level.FINE)) log.fine("Real stub retrieved from memCache. realStub = " + realStub);
                bean = convertImageSetStubToBean(realStub);
            } else {
                bean = convertImageSetStubToBean(imageSet);
            }
            boolean suc = ServiceManager.getImageSetService().updateImageSet(bean);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update the imageSet with guid = " + guid);
                throw new InternalServerErrorException("Failed to update the imageSet with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateImageSet(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new RequestConflictRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<String> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<String> pageImages) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    @Override
    public Response deleteImageSet(String guid) throws BaseResourceException
    {
        if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteImageSet(): queueName=" + queueName + "; taskName=" + taskName + "; retryCount=" + retryCount);
        try {
            //guid = GUID.normalize(guid);    // TBD: Validate guid?
            boolean suc = ServiceManager.getImageSetService().deleteImageSet(guid);
            if(suc == false) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to delete the imageSet with guid = " + guid);
                throw new InternalServerErrorException("Failed to delete the imageSet with guid = " + guid);
            }
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "deleteImageSet(): Successfully processed the request: guid = " + guid);
            return Response.noContent().build();
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(ResourceNotFoundException ex) {
            throw new ResourceNotFoundRsException(ex, resourceUri);
        } catch(ResourceGoneException ex) {
            throw new ResourceGoneRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(DataStoreException ex) {
            throw new DataStoreRsException(ex, resourceUri);
        } catch(ServiceUnavailableException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    @Override
    public Response deleteImageSets(String filter, String params, List<String> values) throws BaseResourceException
    {
        // Note: This method should never be called.
        throw new NotImplementedRsException(resourceUri);
    }

    public static ImageSetBean convertImageSetStubToBean(ImageSet stub)
    {
        ImageSetBean bean = new ImageSetBean();
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean.setGuid(stub.getGuid());
            bean.setUser(stub.getUser());
            bean.setFetchRequest(stub.getFetchRequest());
            bean.setTargetUrl(stub.getTargetUrl());
            bean.setPageUrl(stub.getPageUrl());
            bean.setQueryString(stub.getQueryString());
            bean.setQueryParams(stub.getQueryParams());
            bean.setLastFetchResult(stub.getLastFetchResult());
            bean.setResponseCode(stub.getResponseCode());
            bean.setContentType(stub.getContentType());
            bean.setContentLength(stub.getContentLength());
            bean.setLanguage(stub.getLanguage());
            bean.setRedirect(stub.getRedirect());
            bean.setLocation(stub.getLocation());
            bean.setPageTitle(stub.getPageTitle());
            bean.setNote(stub.getNote());
            bean.setDeferred(stub.isDeferred());
            bean.setStatus(stub.getStatus());
            bean.setRefreshStatus(stub.getRefreshStatus());
            bean.setRefreshInterval(stub.getRefreshInterval());
            bean.setNextRefreshTime(stub.getNextRefreshTime());
            bean.setLastCheckedTime(stub.getLastCheckedTime());
            bean.setLastUpdatedTime(stub.getLastUpdatedTime());
            bean.setMediaTypeFilter(stub.getMediaTypeFilter());
            bean.setPageImages(stub.getPageImages());
            bean.setCreatedTime(stub.getCreatedTime());
            bean.setModifiedTime(stub.getModifiedTime());
        }
        return bean;
    }

}
