package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.PagerStateStruct;
import com.pagesynopsis.ws.bean.PagerStateStructBean;
import com.pagesynopsis.ws.stub.PagerStateStructStub;


public class PagerStateStructResourceUtil
{
    private static final Logger log = Logger.getLogger(PagerStateStructResourceUtil.class.getName());

    // Static methods only.
    private PagerStateStructResourceUtil() {}

    public static PagerStateStructBean convertPagerStateStructStubToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null PagerStateStructBean is returned.");
        } else {
            bean = new PagerStateStructBean();
            bean.setPagerMode(stub.getPagerMode());
            bean.setPrimaryOrdering(stub.getPrimaryOrdering());
            bean.setSecondaryOrdering(stub.getSecondaryOrdering());
            bean.setCurrentOffset(stub.getCurrentOffset());
            bean.setCurrentPage(stub.getCurrentPage());
            bean.setPageSize(stub.getPageSize());
            bean.setTotalCount(stub.getTotalCount());
            bean.setLowerBoundTotalCount(stub.getLowerBoundTotalCount());
            bean.setPreviousPageOffset(stub.getPreviousPageOffset());
            bean.setNextPageOffset(stub.getNextPageOffset());
            bean.setLastPageOffset(stub.getLastPageOffset());
            bean.setLastPageIndex(stub.getLastPageIndex());
            bean.setFirstActionEnabled(stub.isFirstActionEnabled());
            bean.setPreviousActionEnabled(stub.isPreviousActionEnabled());
            bean.setNextActionEnabled(stub.isNextActionEnabled());
            bean.setLastActionEnabled(stub.isLastActionEnabled());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
