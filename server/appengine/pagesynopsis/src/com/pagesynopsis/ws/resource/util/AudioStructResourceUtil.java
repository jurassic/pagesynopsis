package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.stub.AudioStructStub;


public class AudioStructResourceUtil
{
    private static final Logger log = Logger.getLogger(AudioStructResourceUtil.class.getName());

    // Static methods only.
    private AudioStructResourceUtil() {}

    public static AudioStructBean convertAudioStructStubToBean(AudioStruct stub)
    {
        AudioStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null AudioStructBean is returned.");
        } else {
            bean = new AudioStructBean();
            bean.setUuid(stub.getUuid());
            bean.setId(stub.getId());
            bean.setControls(stub.getControls());
            bean.setAutoplayEnabled(stub.isAutoplayEnabled());
            bean.setLoopEnabled(stub.isLoopEnabled());
            bean.setPreloadEnabled(stub.isPreloadEnabled());
            bean.setRemark(stub.getRemark());
            bean.setSource(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource()));
            bean.setSource1(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource1()));
            bean.setSource2(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource2()));
            bean.setSource3(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource3()));
            bean.setSource4(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource4()));
            bean.setSource5(MediaSourceStructResourceUtil.convertMediaSourceStructStubToBean(stub.getSource5()));
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
