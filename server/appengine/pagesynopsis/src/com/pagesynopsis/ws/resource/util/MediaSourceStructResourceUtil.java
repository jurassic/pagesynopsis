package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.bean.MediaSourceStructBean;
import com.pagesynopsis.ws.stub.MediaSourceStructStub;


public class MediaSourceStructResourceUtil
{
    private static final Logger log = Logger.getLogger(MediaSourceStructResourceUtil.class.getName());

    // Static methods only.
    private MediaSourceStructResourceUtil() {}

    public static MediaSourceStructBean convertMediaSourceStructStubToBean(MediaSourceStruct stub)
    {
        MediaSourceStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null MediaSourceStructBean is returned.");
        } else {
            bean = new MediaSourceStructBean();
            bean.setUuid(stub.getUuid());
            bean.setSrc(stub.getSrc());
            bean.setSrcUrl(stub.getSrcUrl());
            bean.setType(stub.getType());
            bean.setRemark(stub.getRemark());
        }
        return bean;
    }

}
