package com.pagesynopsis.ws.resource;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.service.ApiConsumerService;
import com.pagesynopsis.ws.service.impl.ApiConsumerServiceImpl;
import com.pagesynopsis.ws.service.UserService;
import com.pagesynopsis.ws.service.impl.UserServiceImpl;
import com.pagesynopsis.ws.service.RobotsTextFileService;
import com.pagesynopsis.ws.service.impl.RobotsTextFileServiceImpl;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;
import com.pagesynopsis.ws.service.impl.RobotsTextRefreshServiceImpl;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.impl.OgProfileServiceImpl;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.impl.OgWebsiteServiceImpl;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.impl.OgBlogServiceImpl;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.impl.OgArticleServiceImpl;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.impl.OgBookServiceImpl;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.impl.OgVideoServiceImpl;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.impl.OgMovieServiceImpl;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.impl.OgTvShowServiceImpl;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.impl.OgTvEpisodeServiceImpl;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.impl.TwitterSummaryCardServiceImpl;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.impl.TwitterPhotoCardServiceImpl;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.impl.TwitterGalleryCardServiceImpl;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.impl.TwitterAppCardServiceImpl;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.impl.TwitterPlayerCardServiceImpl;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.impl.TwitterProductCardServiceImpl;
import com.pagesynopsis.ws.service.FetchRequestService;
import com.pagesynopsis.ws.service.impl.FetchRequestServiceImpl;
import com.pagesynopsis.ws.service.PageInfoService;
import com.pagesynopsis.ws.service.impl.PageInfoServiceImpl;
import com.pagesynopsis.ws.service.PageFetchService;
import com.pagesynopsis.ws.service.impl.PageFetchServiceImpl;
import com.pagesynopsis.ws.service.LinkListService;
import com.pagesynopsis.ws.service.impl.LinkListServiceImpl;
import com.pagesynopsis.ws.service.ImageSetService;
import com.pagesynopsis.ws.service.impl.ImageSetServiceImpl;
import com.pagesynopsis.ws.service.AudioSetService;
import com.pagesynopsis.ws.service.impl.AudioSetServiceImpl;
import com.pagesynopsis.ws.service.VideoSetService;
import com.pagesynopsis.ws.service.impl.VideoSetServiceImpl;
import com.pagesynopsis.ws.service.OpenGraphMetaService;
import com.pagesynopsis.ws.service.impl.OpenGraphMetaServiceImpl;
import com.pagesynopsis.ws.service.TwitterCardMetaService;
import com.pagesynopsis.ws.service.impl.TwitterCardMetaServiceImpl;
import com.pagesynopsis.ws.service.DomainInfoService;
import com.pagesynopsis.ws.service.impl.DomainInfoServiceImpl;
import com.pagesynopsis.ws.service.UrlRatingService;
import com.pagesynopsis.ws.service.impl.UrlRatingServiceImpl;
import com.pagesynopsis.ws.service.ServiceInfoService;
import com.pagesynopsis.ws.service.impl.ServiceInfoServiceImpl;
import com.pagesynopsis.ws.service.FiveTenService;
import com.pagesynopsis.ws.service.impl.FiveTenServiceImpl;


// TBD:
// Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

	private static ApiConsumerService apiConsumerService = null;
	private static UserService userService = null;
	private static RobotsTextFileService robotsTextFileService = null;
	private static RobotsTextRefreshService robotsTextRefreshService = null;
	private static OgProfileService ogProfileService = null;
	private static OgWebsiteService ogWebsiteService = null;
	private static OgBlogService ogBlogService = null;
	private static OgArticleService ogArticleService = null;
	private static OgBookService ogBookService = null;
	private static OgVideoService ogVideoService = null;
	private static OgMovieService ogMovieService = null;
	private static OgTvShowService ogTvShowService = null;
	private static OgTvEpisodeService ogTvEpisodeService = null;
	private static TwitterSummaryCardService twitterSummaryCardService = null;
	private static TwitterPhotoCardService twitterPhotoCardService = null;
	private static TwitterGalleryCardService twitterGalleryCardService = null;
	private static TwitterAppCardService twitterAppCardService = null;
	private static TwitterPlayerCardService twitterPlayerCardService = null;
	private static TwitterProductCardService twitterProductCardService = null;
	private static FetchRequestService fetchRequestService = null;
	private static PageInfoService pageInfoService = null;
	private static PageFetchService pageFetchService = null;
	private static LinkListService linkListService = null;
	private static ImageSetService imageSetService = null;
	private static AudioSetService audioSetService = null;
	private static VideoSetService videoSetService = null;
	private static OpenGraphMetaService openGraphMetaService = null;
	private static TwitterCardMetaService twitterCardMetaService = null;
	private static DomainInfoService domainInfoService = null;
	private static UrlRatingService urlRatingService = null;
	private static ServiceInfoService serviceInfoService = null;
	private static FiveTenService fiveTenService = null;

    // Prevents instantiation.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(ServiceManager.apiConsumerService == null) {
            ServiceManager.apiConsumerService = new ApiConsumerServiceImpl();
        }
        return ServiceManager.apiConsumerService;
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(ServiceManager.userService == null) {
            ServiceManager.userService = new UserServiceImpl();
        }
        return ServiceManager.userService;
    }

    // Returns a RobotsTextFileService instance.
	public static RobotsTextFileService getRobotsTextFileService() 
    {
        if(ServiceManager.robotsTextFileService == null) {
            ServiceManager.robotsTextFileService = new RobotsTextFileServiceImpl();
        }
        return ServiceManager.robotsTextFileService;
    }

    // Returns a RobotsTextRefreshService instance.
	public static RobotsTextRefreshService getRobotsTextRefreshService() 
    {
        if(ServiceManager.robotsTextRefreshService == null) {
            ServiceManager.robotsTextRefreshService = new RobotsTextRefreshServiceImpl();
        }
        return ServiceManager.robotsTextRefreshService;
    }

    // Returns a OgProfileService instance.
	public static OgProfileService getOgProfileService() 
    {
        if(ServiceManager.ogProfileService == null) {
            ServiceManager.ogProfileService = new OgProfileServiceImpl();
        }
        return ServiceManager.ogProfileService;
    }

    // Returns a OgWebsiteService instance.
	public static OgWebsiteService getOgWebsiteService() 
    {
        if(ServiceManager.ogWebsiteService == null) {
            ServiceManager.ogWebsiteService = new OgWebsiteServiceImpl();
        }
        return ServiceManager.ogWebsiteService;
    }

    // Returns a OgBlogService instance.
	public static OgBlogService getOgBlogService() 
    {
        if(ServiceManager.ogBlogService == null) {
            ServiceManager.ogBlogService = new OgBlogServiceImpl();
        }
        return ServiceManager.ogBlogService;
    }

    // Returns a OgArticleService instance.
	public static OgArticleService getOgArticleService() 
    {
        if(ServiceManager.ogArticleService == null) {
            ServiceManager.ogArticleService = new OgArticleServiceImpl();
        }
        return ServiceManager.ogArticleService;
    }

    // Returns a OgBookService instance.
	public static OgBookService getOgBookService() 
    {
        if(ServiceManager.ogBookService == null) {
            ServiceManager.ogBookService = new OgBookServiceImpl();
        }
        return ServiceManager.ogBookService;
    }

    // Returns a OgVideoService instance.
	public static OgVideoService getOgVideoService() 
    {
        if(ServiceManager.ogVideoService == null) {
            ServiceManager.ogVideoService = new OgVideoServiceImpl();
        }
        return ServiceManager.ogVideoService;
    }

    // Returns a OgMovieService instance.
	public static OgMovieService getOgMovieService() 
    {
        if(ServiceManager.ogMovieService == null) {
            ServiceManager.ogMovieService = new OgMovieServiceImpl();
        }
        return ServiceManager.ogMovieService;
    }

    // Returns a OgTvShowService instance.
	public static OgTvShowService getOgTvShowService() 
    {
        if(ServiceManager.ogTvShowService == null) {
            ServiceManager.ogTvShowService = new OgTvShowServiceImpl();
        }
        return ServiceManager.ogTvShowService;
    }

    // Returns a OgTvEpisodeService instance.
	public static OgTvEpisodeService getOgTvEpisodeService() 
    {
        if(ServiceManager.ogTvEpisodeService == null) {
            ServiceManager.ogTvEpisodeService = new OgTvEpisodeServiceImpl();
        }
        return ServiceManager.ogTvEpisodeService;
    }

    // Returns a TwitterSummaryCardService instance.
	public static TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        if(ServiceManager.twitterSummaryCardService == null) {
            ServiceManager.twitterSummaryCardService = new TwitterSummaryCardServiceImpl();
        }
        return ServiceManager.twitterSummaryCardService;
    }

    // Returns a TwitterPhotoCardService instance.
	public static TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        if(ServiceManager.twitterPhotoCardService == null) {
            ServiceManager.twitterPhotoCardService = new TwitterPhotoCardServiceImpl();
        }
        return ServiceManager.twitterPhotoCardService;
    }

    // Returns a TwitterGalleryCardService instance.
	public static TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        if(ServiceManager.twitterGalleryCardService == null) {
            ServiceManager.twitterGalleryCardService = new TwitterGalleryCardServiceImpl();
        }
        return ServiceManager.twitterGalleryCardService;
    }

    // Returns a TwitterAppCardService instance.
	public static TwitterAppCardService getTwitterAppCardService() 
    {
        if(ServiceManager.twitterAppCardService == null) {
            ServiceManager.twitterAppCardService = new TwitterAppCardServiceImpl();
        }
        return ServiceManager.twitterAppCardService;
    }

    // Returns a TwitterPlayerCardService instance.
	public static TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        if(ServiceManager.twitterPlayerCardService == null) {
            ServiceManager.twitterPlayerCardService = new TwitterPlayerCardServiceImpl();
        }
        return ServiceManager.twitterPlayerCardService;
    }

    // Returns a TwitterProductCardService instance.
	public static TwitterProductCardService getTwitterProductCardService() 
    {
        if(ServiceManager.twitterProductCardService == null) {
            ServiceManager.twitterProductCardService = new TwitterProductCardServiceImpl();
        }
        return ServiceManager.twitterProductCardService;
    }

    // Returns a FetchRequestService instance.
	public static FetchRequestService getFetchRequestService() 
    {
        if(ServiceManager.fetchRequestService == null) {
            ServiceManager.fetchRequestService = new FetchRequestServiceImpl();
        }
        return ServiceManager.fetchRequestService;
    }

    // Returns a PageInfoService instance.
	public static PageInfoService getPageInfoService() 
    {
        if(ServiceManager.pageInfoService == null) {
            ServiceManager.pageInfoService = new PageInfoServiceImpl();
        }
        return ServiceManager.pageInfoService;
    }

    // Returns a PageFetchService instance.
	public static PageFetchService getPageFetchService() 
    {
        if(ServiceManager.pageFetchService == null) {
            ServiceManager.pageFetchService = new PageFetchServiceImpl();
        }
        return ServiceManager.pageFetchService;
    }

    // Returns a LinkListService instance.
	public static LinkListService getLinkListService() 
    {
        if(ServiceManager.linkListService == null) {
            ServiceManager.linkListService = new LinkListServiceImpl();
        }
        return ServiceManager.linkListService;
    }

    // Returns a ImageSetService instance.
	public static ImageSetService getImageSetService() 
    {
        if(ServiceManager.imageSetService == null) {
            ServiceManager.imageSetService = new ImageSetServiceImpl();
        }
        return ServiceManager.imageSetService;
    }

    // Returns a AudioSetService instance.
	public static AudioSetService getAudioSetService() 
    {
        if(ServiceManager.audioSetService == null) {
            ServiceManager.audioSetService = new AudioSetServiceImpl();
        }
        return ServiceManager.audioSetService;
    }

    // Returns a VideoSetService instance.
	public static VideoSetService getVideoSetService() 
    {
        if(ServiceManager.videoSetService == null) {
            ServiceManager.videoSetService = new VideoSetServiceImpl();
        }
        return ServiceManager.videoSetService;
    }

    // Returns a OpenGraphMetaService instance.
	public static OpenGraphMetaService getOpenGraphMetaService() 
    {
        if(ServiceManager.openGraphMetaService == null) {
            ServiceManager.openGraphMetaService = new OpenGraphMetaServiceImpl();
        }
        return ServiceManager.openGraphMetaService;
    }

    // Returns a TwitterCardMetaService instance.
	public static TwitterCardMetaService getTwitterCardMetaService() 
    {
        if(ServiceManager.twitterCardMetaService == null) {
            ServiceManager.twitterCardMetaService = new TwitterCardMetaServiceImpl();
        }
        return ServiceManager.twitterCardMetaService;
    }

    // Returns a DomainInfoService instance.
	public static DomainInfoService getDomainInfoService() 
    {
        if(ServiceManager.domainInfoService == null) {
            ServiceManager.domainInfoService = new DomainInfoServiceImpl();
        }
        return ServiceManager.domainInfoService;
    }

    // Returns a UrlRatingService instance.
	public static UrlRatingService getUrlRatingService() 
    {
        if(ServiceManager.urlRatingService == null) {
            ServiceManager.urlRatingService = new UrlRatingServiceImpl();
        }
        return ServiceManager.urlRatingService;
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(ServiceManager.serviceInfoService == null) {
            ServiceManager.serviceInfoService = new ServiceInfoServiceImpl();
        }
        return ServiceManager.serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(ServiceManager.fiveTenService == null) {
            ServiceManager.fiveTenService = new FiveTenServiceImpl();
        }
        return ServiceManager.fiveTenService;
    }

}
