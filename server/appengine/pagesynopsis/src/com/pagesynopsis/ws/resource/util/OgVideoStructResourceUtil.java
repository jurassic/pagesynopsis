package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.stub.OgVideoStructStub;


public class OgVideoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgVideoStructResourceUtil.class.getName());

    // Static methods only.
    private OgVideoStructResourceUtil() {}

    public static OgVideoStructBean convertOgVideoStructStubToBean(OgVideoStruct stub)
    {
        OgVideoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null OgVideoStructBean is returned.");
        } else {
            bean = new OgVideoStructBean();
            bean.setUuid(stub.getUuid());
            bean.setUrl(stub.getUrl());
            bean.setSecureUrl(stub.getSecureUrl());
            bean.setType(stub.getType());
            bean.setWidth(stub.getWidth());
            bean.setHeight(stub.getHeight());
        }
        return bean;
    }

}
