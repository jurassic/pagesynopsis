package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.bean.TwitterCardProductDataBean;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;


public class TwitterCardProductDataResourceUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataResourceUtil.class.getName());

    // Static methods only.
    private TwitterCardProductDataResourceUtil() {}

    public static TwitterCardProductDataBean convertTwitterCardProductDataStubToBean(TwitterCardProductData stub)
    {
        TwitterCardProductDataBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null TwitterCardProductDataBean is returned.");
        } else {
            bean = new TwitterCardProductDataBean();
            bean.setData(stub.getData());
            bean.setLabel(stub.getLabel());
        }
        return bean;
    }

}
