package com.pagesynopsis.ws.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;


public class TwitterCardAppInfoResourceUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoResourceUtil.class.getName());

    // Static methods only.
    private TwitterCardAppInfoResourceUtil() {}

    public static TwitterCardAppInfoBean convertTwitterCardAppInfoStubToBean(TwitterCardAppInfo stub)
    {
        TwitterCardAppInfoBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Null TwitterCardAppInfoBean is returned.");
        } else {
            bean = new TwitterCardAppInfoBean();
            bean.setName(stub.getName());
            bean.setId(stub.getId());
            bean.setUrl(stub.getUrl());
        }
        return bean;
    }

}
