package com.pagesynopsis.ws;

import java.util.List;


public interface RobotsTextGroup 
{
    String  getUuid();
    String  getUserAgent();
    List<String>  getAllows();
    List<String>  getDisallows();
    boolean isEmpty();
}
