package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgArticle extends OgObjectBase
{
    List<String>  getAuthor();
    String  getSection();
    List<String>  getTag();
    String  getPublishedDate();
    String  getModifiedDate();
    String  getExpirationDate();
}
