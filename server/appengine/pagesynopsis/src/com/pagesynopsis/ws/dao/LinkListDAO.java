package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.LinkListDataObject;


// TBD: Add offset/count to getAllLinkLists() and findLinkLists(), etc.
public interface LinkListDAO
{
    LinkListDataObject getLinkList(String guid) throws BaseException;
    List<LinkListDataObject> getLinkLists(List<String> guids) throws BaseException;
    List<LinkListDataObject> getAllLinkLists() throws BaseException;
    /* @Deprecated */ List<LinkListDataObject> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException;
    List<LinkListDataObject> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<LinkListDataObject> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createLinkList(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return LinkListDataObject?)
    String createLinkList(LinkListDataObject linkList) throws BaseException;          // Returns Guid.  (Return LinkListDataObject?)
    //Boolean updateLinkList(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateLinkList(LinkListDataObject linkList) throws BaseException;
    Boolean deleteLinkList(String guid) throws BaseException;
    Boolean deleteLinkList(LinkListDataObject linkList) throws BaseException;
    Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException;
}
