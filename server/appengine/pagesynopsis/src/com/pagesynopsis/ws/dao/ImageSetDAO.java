package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.ImageSetDataObject;


// TBD: Add offset/count to getAllImageSets() and findImageSets(), etc.
public interface ImageSetDAO
{
    ImageSetDataObject getImageSet(String guid) throws BaseException;
    List<ImageSetDataObject> getImageSets(List<String> guids) throws BaseException;
    List<ImageSetDataObject> getAllImageSets() throws BaseException;
    /* @Deprecated */ List<ImageSetDataObject> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException;
    List<ImageSetDataObject> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<ImageSetDataObject> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createImageSet(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return ImageSetDataObject?)
    String createImageSet(ImageSetDataObject imageSet) throws BaseException;          // Returns Guid.  (Return ImageSetDataObject?)
    //Boolean updateImageSet(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateImageSet(ImageSetDataObject imageSet) throws BaseException;
    Boolean deleteImageSet(String guid) throws BaseException;
    Boolean deleteImageSet(ImageSetDataObject imageSet) throws BaseException;
    Long deleteImageSets(String filter, String params, List<String> values) throws BaseException;
}
