package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.VideoSetDataObject;


// TBD: Add offset/count to getAllVideoSets() and findVideoSets(), etc.
public interface VideoSetDAO
{
    VideoSetDataObject getVideoSet(String guid) throws BaseException;
    List<VideoSetDataObject> getVideoSets(List<String> guids) throws BaseException;
    List<VideoSetDataObject> getAllVideoSets() throws BaseException;
    /* @Deprecated */ List<VideoSetDataObject> getAllVideoSets(String ordering, Long offset, Integer count) throws BaseException;
    List<VideoSetDataObject> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<VideoSetDataObject> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createVideoSet(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return VideoSetDataObject?)
    String createVideoSet(VideoSetDataObject videoSet) throws BaseException;          // Returns Guid.  (Return VideoSetDataObject?)
    //Boolean updateVideoSet(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateVideoSet(VideoSetDataObject videoSet) throws BaseException;
    Boolean deleteVideoSet(String guid) throws BaseException;
    Boolean deleteVideoSet(VideoSetDataObject videoSet) throws BaseException;
    Long deleteVideoSets(String filter, String params, List<String> values) throws BaseException;
}
