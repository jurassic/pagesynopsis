package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgBookDataObject;


// TBD: Add offset/count to getAllOgBooks() and findOgBooks(), etc.
public interface OgBookDAO
{
    OgBookDataObject getOgBook(String guid) throws BaseException;
    List<OgBookDataObject> getOgBooks(List<String> guids) throws BaseException;
    List<OgBookDataObject> getAllOgBooks() throws BaseException;
    /* @Deprecated */ List<OgBookDataObject> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException;
    List<OgBookDataObject> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgBookDataObject> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgBook(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgBookDataObject?)
    String createOgBook(OgBookDataObject ogBook) throws BaseException;          // Returns Guid.  (Return OgBookDataObject?)
    //Boolean updateOgBook(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgBook(OgBookDataObject ogBook) throws BaseException;
    Boolean deleteOgBook(String guid) throws BaseException;
    Boolean deleteOgBook(OgBookDataObject ogBook) throws BaseException;
    Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException;
}
