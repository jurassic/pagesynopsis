package com.pagesynopsis.ws.dao;

import java.util.List;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;


// TBD: Add offset/count to getAllTwitterAppCards() and findTwitterAppCards(), etc.
public interface TwitterAppCardDAO
{
    TwitterAppCardDataObject getTwitterAppCard(String guid) throws BaseException;
    List<TwitterAppCardDataObject> getTwitterAppCards(List<String> guids) throws BaseException;
    List<TwitterAppCardDataObject> getAllTwitterAppCards() throws BaseException;
    /* @Deprecated */ List<TwitterAppCardDataObject> getAllTwitterAppCards(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterAppCardDataObject> getAllTwitterAppCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterAppCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<TwitterAppCardDataObject> findTwitterAppCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterAppCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterAppCard(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterAppCardDataObject?)
    String createTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException;          // Returns Guid.  (Return TwitterAppCardDataObject?)
    //Boolean updateTwitterAppCard(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException;
    Boolean deleteTwitterAppCard(String guid) throws BaseException;
    Boolean deleteTwitterAppCard(TwitterAppCardDataObject twitterAppCard) throws BaseException;
    Long deleteTwitterAppCards(String filter, String params, List<String> values) throws BaseException;
}
