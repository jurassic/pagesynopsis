package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.TwitterCardMetaDataObject;


// TBD: Add offset/count to getAllTwitterCardMetas() and findTwitterCardMetas(), etc.
public interface TwitterCardMetaDAO
{
    TwitterCardMetaDataObject getTwitterCardMeta(String guid) throws BaseException;
    List<TwitterCardMetaDataObject> getTwitterCardMetas(List<String> guids) throws BaseException;
    List<TwitterCardMetaDataObject> getAllTwitterCardMetas() throws BaseException;
    /* @Deprecated */ List<TwitterCardMetaDataObject> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterCardMetaDataObject> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<TwitterCardMetaDataObject> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createTwitterCardMeta(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterCardMetaDataObject?)
    String createTwitterCardMeta(TwitterCardMetaDataObject twitterCardMeta) throws BaseException;          // Returns Guid.  (Return TwitterCardMetaDataObject?)
    //Boolean updateTwitterCardMeta(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterCardMeta(TwitterCardMetaDataObject twitterCardMeta) throws BaseException;
    Boolean deleteTwitterCardMeta(String guid) throws BaseException;
    Boolean deleteTwitterCardMeta(TwitterCardMetaDataObject twitterCardMeta) throws BaseException;
    Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException;
}
