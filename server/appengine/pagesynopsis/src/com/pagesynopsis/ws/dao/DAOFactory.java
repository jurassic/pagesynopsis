package com.pagesynopsis.ws.dao;

// Abstract factory.
public abstract class DAOFactory
{
    public abstract ApiConsumerDAO getApiConsumerDAO();
    public abstract UserDAO getUserDAO();
    public abstract RobotsTextFileDAO getRobotsTextFileDAO();
    public abstract RobotsTextRefreshDAO getRobotsTextRefreshDAO();
    public abstract OgProfileDAO getOgProfileDAO();
    public abstract OgWebsiteDAO getOgWebsiteDAO();
    public abstract OgBlogDAO getOgBlogDAO();
    public abstract OgArticleDAO getOgArticleDAO();
    public abstract OgBookDAO getOgBookDAO();
    public abstract OgVideoDAO getOgVideoDAO();
    public abstract OgMovieDAO getOgMovieDAO();
    public abstract OgTvShowDAO getOgTvShowDAO();
    public abstract OgTvEpisodeDAO getOgTvEpisodeDAO();
    public abstract TwitterSummaryCardDAO getTwitterSummaryCardDAO();
    public abstract TwitterPhotoCardDAO getTwitterPhotoCardDAO();
    public abstract TwitterGalleryCardDAO getTwitterGalleryCardDAO();
    public abstract TwitterAppCardDAO getTwitterAppCardDAO();
    public abstract TwitterPlayerCardDAO getTwitterPlayerCardDAO();
    public abstract TwitterProductCardDAO getTwitterProductCardDAO();
    public abstract FetchRequestDAO getFetchRequestDAO();
    public abstract PageInfoDAO getPageInfoDAO();
    public abstract PageFetchDAO getPageFetchDAO();
    public abstract LinkListDAO getLinkListDAO();
    public abstract ImageSetDAO getImageSetDAO();
    public abstract AudioSetDAO getAudioSetDAO();
    public abstract VideoSetDAO getVideoSetDAO();
    public abstract OpenGraphMetaDAO getOpenGraphMetaDAO();
    public abstract TwitterCardMetaDAO getTwitterCardMetaDAO();
    public abstract DomainInfoDAO getDomainInfoDAO();
    public abstract UrlRatingDAO getUrlRatingDAO();
    public abstract ServiceInfoDAO getServiceInfoDAO();
    public abstract FiveTenDAO getFiveTenDAO();
}
