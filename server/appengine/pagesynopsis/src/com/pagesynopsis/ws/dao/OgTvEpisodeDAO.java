package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;


// TBD: Add offset/count to getAllOgTvEpisodes() and findOgTvEpisodes(), etc.
public interface OgTvEpisodeDAO
{
    OgTvEpisodeDataObject getOgTvEpisode(String guid) throws BaseException;
    List<OgTvEpisodeDataObject> getOgTvEpisodes(List<String> guids) throws BaseException;
    List<OgTvEpisodeDataObject> getAllOgTvEpisodes() throws BaseException;
    /* @Deprecated */ List<OgTvEpisodeDataObject> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException;
    List<OgTvEpisodeDataObject> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgTvEpisodeDataObject> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createOgTvEpisode(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgTvEpisodeDataObject?)
    String createOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException;          // Returns Guid.  (Return OgTvEpisodeDataObject?)
    //Boolean updateOgTvEpisode(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException;
    Boolean deleteOgTvEpisode(String guid) throws BaseException;
    Boolean deleteOgTvEpisode(OgTvEpisodeDataObject ogTvEpisode) throws BaseException;
    Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException;
}
