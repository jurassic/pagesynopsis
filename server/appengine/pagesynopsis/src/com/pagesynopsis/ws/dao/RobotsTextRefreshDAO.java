package com.pagesynopsis.ws.dao;

import java.util.List;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.RobotsTextRefreshDataObject;


// TBD: Add offset/count to getAllRobotsTextRefreshes() and findRobotsTextRefreshes(), etc.
public interface RobotsTextRefreshDAO
{
    RobotsTextRefreshDataObject getRobotsTextRefresh(String guid) throws BaseException;
    List<RobotsTextRefreshDataObject> getRobotsTextRefreshes(List<String> guids) throws BaseException;
    List<RobotsTextRefreshDataObject> getAllRobotsTextRefreshes() throws BaseException;
    /* @Deprecated */ List<RobotsTextRefreshDataObject> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException;
    List<RobotsTextRefreshDataObject> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<RobotsTextRefreshDataObject> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createRobotsTextRefresh(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return RobotsTextRefreshDataObject?)
    String createRobotsTextRefresh(RobotsTextRefreshDataObject robotsTextRefresh) throws BaseException;          // Returns Guid.  (Return RobotsTextRefreshDataObject?)
    //Boolean updateRobotsTextRefresh(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateRobotsTextRefresh(RobotsTextRefreshDataObject robotsTextRefresh) throws BaseException;
    Boolean deleteRobotsTextRefresh(String guid) throws BaseException;
    Boolean deleteRobotsTextRefresh(RobotsTextRefreshDataObject robotsTextRefresh) throws BaseException;
    Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException;
}
