package com.pagesynopsis.ws.dao.base;

import java.util.logging.Logger;

import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.dao.ApiConsumerDAO;
import com.pagesynopsis.ws.dao.UserDAO;
import com.pagesynopsis.ws.dao.RobotsTextFileDAO;
import com.pagesynopsis.ws.dao.RobotsTextRefreshDAO;
import com.pagesynopsis.ws.dao.OgProfileDAO;
import com.pagesynopsis.ws.dao.OgWebsiteDAO;
import com.pagesynopsis.ws.dao.OgBlogDAO;
import com.pagesynopsis.ws.dao.OgArticleDAO;
import com.pagesynopsis.ws.dao.OgBookDAO;
import com.pagesynopsis.ws.dao.OgVideoDAO;
import com.pagesynopsis.ws.dao.OgMovieDAO;
import com.pagesynopsis.ws.dao.OgTvShowDAO;
import com.pagesynopsis.ws.dao.OgTvEpisodeDAO;
import com.pagesynopsis.ws.dao.TwitterSummaryCardDAO;
import com.pagesynopsis.ws.dao.TwitterPhotoCardDAO;
import com.pagesynopsis.ws.dao.TwitterGalleryCardDAO;
import com.pagesynopsis.ws.dao.TwitterAppCardDAO;
import com.pagesynopsis.ws.dao.TwitterPlayerCardDAO;
import com.pagesynopsis.ws.dao.TwitterProductCardDAO;
import com.pagesynopsis.ws.dao.FetchRequestDAO;
import com.pagesynopsis.ws.dao.PageInfoDAO;
import com.pagesynopsis.ws.dao.PageFetchDAO;
import com.pagesynopsis.ws.dao.LinkListDAO;
import com.pagesynopsis.ws.dao.ImageSetDAO;
import com.pagesynopsis.ws.dao.AudioSetDAO;
import com.pagesynopsis.ws.dao.VideoSetDAO;
import com.pagesynopsis.ws.dao.OpenGraphMetaDAO;
import com.pagesynopsis.ws.dao.TwitterCardMetaDAO;
import com.pagesynopsis.ws.dao.DomainInfoDAO;
import com.pagesynopsis.ws.dao.UrlRatingDAO;
import com.pagesynopsis.ws.dao.ServiceInfoDAO;
import com.pagesynopsis.ws.dao.FiveTenDAO;

// Default DAO factory uses JDO on Google AppEngine.
public class DefaultDAOFactory extends DAOFactory
{
    private static final Logger log = Logger.getLogger(DefaultDAOFactory.class.getName());

    // Ctor. Prevents instantiation.
    private DefaultDAOFactory()
    {
        // ...
    }

    // Initialization-on-demand holder.
    private static class DefaultDAOFactoryHolder
    {
        private static final DefaultDAOFactory INSTANCE = new DefaultDAOFactory();
    }

    // Singleton method
    public static DefaultDAOFactory getInstance()
    {
        return DefaultDAOFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerDAO getApiConsumerDAO()
    {
        return new DefaultApiConsumerDAO();
    }

    @Override
    public UserDAO getUserDAO()
    {
        return new DefaultUserDAO();
    }

    @Override
    public RobotsTextFileDAO getRobotsTextFileDAO()
    {
        return new DefaultRobotsTextFileDAO();
    }

    @Override
    public RobotsTextRefreshDAO getRobotsTextRefreshDAO()
    {
        return new DefaultRobotsTextRefreshDAO();
    }

    @Override
    public OgProfileDAO getOgProfileDAO()
    {
        return new DefaultOgProfileDAO();
    }

    @Override
    public OgWebsiteDAO getOgWebsiteDAO()
    {
        return new DefaultOgWebsiteDAO();
    }

    @Override
    public OgBlogDAO getOgBlogDAO()
    {
        return new DefaultOgBlogDAO();
    }

    @Override
    public OgArticleDAO getOgArticleDAO()
    {
        return new DefaultOgArticleDAO();
    }

    @Override
    public OgBookDAO getOgBookDAO()
    {
        return new DefaultOgBookDAO();
    }

    @Override
    public OgVideoDAO getOgVideoDAO()
    {
        return new DefaultOgVideoDAO();
    }

    @Override
    public OgMovieDAO getOgMovieDAO()
    {
        return new DefaultOgMovieDAO();
    }

    @Override
    public OgTvShowDAO getOgTvShowDAO()
    {
        return new DefaultOgTvShowDAO();
    }

    @Override
    public OgTvEpisodeDAO getOgTvEpisodeDAO()
    {
        return new DefaultOgTvEpisodeDAO();
    }

    @Override
    public TwitterSummaryCardDAO getTwitterSummaryCardDAO()
    {
        return new DefaultTwitterSummaryCardDAO();
    }

    @Override
    public TwitterPhotoCardDAO getTwitterPhotoCardDAO()
    {
        return new DefaultTwitterPhotoCardDAO();
    }

    @Override
    public TwitterGalleryCardDAO getTwitterGalleryCardDAO()
    {
        return new DefaultTwitterGalleryCardDAO();
    }

    @Override
    public TwitterAppCardDAO getTwitterAppCardDAO()
    {
        return new DefaultTwitterAppCardDAO();
    }

    @Override
    public TwitterPlayerCardDAO getTwitterPlayerCardDAO()
    {
        return new DefaultTwitterPlayerCardDAO();
    }

    @Override
    public TwitterProductCardDAO getTwitterProductCardDAO()
    {
        return new DefaultTwitterProductCardDAO();
    }

    @Override
    public FetchRequestDAO getFetchRequestDAO()
    {
        return new DefaultFetchRequestDAO();
    }

    @Override
    public PageInfoDAO getPageInfoDAO()
    {
        return new DefaultPageInfoDAO();
    }

    @Override
    public PageFetchDAO getPageFetchDAO()
    {
        return new DefaultPageFetchDAO();
    }

    @Override
    public LinkListDAO getLinkListDAO()
    {
        return new DefaultLinkListDAO();
    }

    @Override
    public ImageSetDAO getImageSetDAO()
    {
        return new DefaultImageSetDAO();
    }

    @Override
    public AudioSetDAO getAudioSetDAO()
    {
        return new DefaultAudioSetDAO();
    }

    @Override
    public VideoSetDAO getVideoSetDAO()
    {
        return new DefaultVideoSetDAO();
    }

    @Override
    public OpenGraphMetaDAO getOpenGraphMetaDAO()
    {
        return new DefaultOpenGraphMetaDAO();
    }

    @Override
    public TwitterCardMetaDAO getTwitterCardMetaDAO()
    {
        return new DefaultTwitterCardMetaDAO();
    }

    @Override
    public DomainInfoDAO getDomainInfoDAO()
    {
        return new DefaultDomainInfoDAO();
    }

    @Override
    public UrlRatingDAO getUrlRatingDAO()
    {
        return new DefaultUrlRatingDAO();
    }

    @Override
    public ServiceInfoDAO getServiceInfoDAO()
    {
        return new DefaultServiceInfoDAO();
    }

    @Override
    public FiveTenDAO getFiveTenDAO()
    {
        return new DefaultFiveTenDAO();
    }

}
