package com.pagesynopsis.ws.dao;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.data.RobotsTextFileDataObject;


// TBD: Add offset/count to getAllRobotsTextFiles() and findRobotsTextFiles(), etc.
public interface RobotsTextFileDAO
{
    RobotsTextFileDataObject getRobotsTextFile(String guid) throws BaseException;
    List<RobotsTextFileDataObject> getRobotsTextFiles(List<String> guids) throws BaseException;
    List<RobotsTextFileDataObject> getAllRobotsTextFiles() throws BaseException;
    /* @Deprecated */ List<RobotsTextFileDataObject> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException;
    List<RobotsTextFileDataObject> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, Long offset, Integer count) throws BaseException;
    /* @Deprecated */ List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<RobotsTextFileDataObject> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    //String createRobotsTextFile(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return RobotsTextFileDataObject?)
    String createRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException;          // Returns Guid.  (Return RobotsTextFileDataObject?)
    //Boolean updateRobotsTextFile(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException;
    Boolean deleteRobotsTextFile(String guid) throws BaseException;
    Boolean deleteRobotsTextFile(RobotsTextFileDataObject robotsTextFile) throws BaseException;
    Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException;
}
