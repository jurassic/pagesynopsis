package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface FetchRequest 
{
    String  getGuid();
    String  getUser();
    String  getTargetUrl();
    String  getPageUrl();
    String  getQueryString();
    List<KeyValuePairStruct>  getQueryParams();
    String  getVersion();
    String  getFetchObject();
    Integer  getFetchStatus();
    String  getResult();
    String  getNote();
    ReferrerInfoStruct  getReferrerInfo();
    String  getStatus();
    String  getNextPageUrl();
    Integer  getFollowPages();
    Integer  getFollowDepth();
    Boolean  isCreateVersion();
    Boolean  isDeferred();
    Boolean  isAlert();
    NotificationStruct  getNotificationPref();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
