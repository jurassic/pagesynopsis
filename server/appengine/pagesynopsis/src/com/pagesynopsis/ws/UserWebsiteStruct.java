package com.pagesynopsis.ws;



public interface UserWebsiteStruct 
{
    String  getUuid();
    String  getPrimarySite();
    String  getHomePage();
    String  getBlogSite();
    String  getPortfolioSite();
    String  getTwitterProfile();
    String  getFacebookProfile();
    String  getGooglePlusProfile();
    String  getNote();
    boolean isEmpty();
}
