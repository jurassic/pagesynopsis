package com.pagesynopsis.ws;



public interface ImageStruct 
{
    String  getUuid();
    String  getId();
    String  getAlt();
    String  getSrc();
    String  getSrcUrl();
    String  getMediaType();
    String  getWidthAttr();
    Integer  getWidth();
    String  getHeightAttr();
    Integer  getHeight();
    String  getNote();
    boolean isEmpty();
}
