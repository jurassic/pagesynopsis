package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgBlog extends OgObjectBase
{
    String  getNote();
}
