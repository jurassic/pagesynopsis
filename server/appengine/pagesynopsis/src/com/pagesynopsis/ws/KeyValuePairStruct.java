package com.pagesynopsis.ws;



public interface KeyValuePairStruct 
{
    String  getUuid();
    String  getKey();
    String  getValue();
    String  getNote();
    boolean isEmpty();
}
