package com.pagesynopsis.ws;



public interface MediaSourceStruct 
{
    String  getUuid();
    String  getSrc();
    String  getSrcUrl();
    String  getType();
    String  getRemark();
    boolean isEmpty();
}
