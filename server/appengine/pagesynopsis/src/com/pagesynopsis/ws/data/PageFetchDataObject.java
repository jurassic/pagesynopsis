package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class PageFetchDataObject extends PageBaseDataObject implements PageFetch
{
    private static final Logger log = Logger.getLogger(PageFetchDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(PageFetchDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(PageFetchDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private Integer inputMaxRedirects;

    @Persistent(defaultFetchGroup = "true")
    private Integer resultRedirectCount;

    @Persistent(defaultFetchGroup = "false")
    private List<UrlStructDataObject> redirectPages;

    @Persistent(defaultFetchGroup = "true")
    private String destinationUrl;

    @Persistent(defaultFetchGroup = "true")
    private String pageAuthor;

    @Persistent(defaultFetchGroup = "false")
    private Text pageSummary;

    @Persistent(defaultFetchGroup = "true")
    private String favicon;

    @Persistent(defaultFetchGroup = "true")
    private String faviconUrl;

    public PageFetchDataObject()
    {
        this(null);
    }
    public PageFetchDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PageFetchDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl, null, null);
    }
    public PageFetchDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.inputMaxRedirects = inputMaxRedirects;
        this.resultRedirectCount = resultRedirectCount;
        setRedirectPages(redirectPages);
        this.destinationUrl = destinationUrl;
        this.pageAuthor = pageAuthor;
        setPageSummary(pageSummary);
        this.favicon = favicon;
        this.faviconUrl = faviconUrl;
    }

//    @Override
//    protected Key createKey()
//    {
//        return PageFetchDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return PageFetchDataObject.composeKey(getGuid());
    }

    public Integer getInputMaxRedirects()
    {
        return this.inputMaxRedirects;
    }
    public void setInputMaxRedirects(Integer inputMaxRedirects)
    {
        this.inputMaxRedirects = inputMaxRedirects;
    }

    public Integer getResultRedirectCount()
    {
        return this.resultRedirectCount;
    }
    public void setResultRedirectCount(Integer resultRedirectCount)
    {
        this.resultRedirectCount = resultRedirectCount;
    }

    @SuppressWarnings("unchecked")
    public List<UrlStruct> getRedirectPages()
    {         
        return (List<UrlStruct>) ((List<?>) this.redirectPages);
    }
    public void setRedirectPages(List<UrlStruct> redirectPages)
    {
        if(redirectPages != null) {
            List<UrlStructDataObject> dataObj = new ArrayList<UrlStructDataObject>();
            for(UrlStruct urlStruct : redirectPages) {
                UrlStructDataObject elem = null;
                if(urlStruct instanceof UrlStructDataObject) {
                    elem = (UrlStructDataObject) urlStruct;
                } else if(urlStruct instanceof UrlStruct) {
                    elem = new UrlStructDataObject(urlStruct.getUuid(), urlStruct.getStatusCode(), urlStruct.getRedirectUrl(), urlStruct.getAbsoluteUrl(), urlStruct.getHashFragment(), urlStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.redirectPages = dataObj;
        } else {
            this.redirectPages = null;
        }
    }

    public String getDestinationUrl()
    {
        return this.destinationUrl;
    }
    public void setDestinationUrl(String destinationUrl)
    {
        this.destinationUrl = destinationUrl;
    }

    public String getPageAuthor()
    {
        return this.pageAuthor;
    }
    public void setPageAuthor(String pageAuthor)
    {
        this.pageAuthor = pageAuthor;
    }

    public String getPageSummary()
    {
        if(this.pageSummary == null) {
            return null;
        }    
        return this.pageSummary.getValue();
    }
    public void setPageSummary(String pageSummary)
    {
        if(pageSummary == null) {
            this.pageSummary = null;
        } else {
            this.pageSummary = new Text(pageSummary);
        }
    }

    public String getFavicon()
    {
        return this.favicon;
    }
    public void setFavicon(String favicon)
    {
        this.favicon = favicon;
    }

    public String getFaviconUrl()
    {
        return this.faviconUrl;
    }
    public void setFaviconUrl(String faviconUrl)
    {
        this.faviconUrl = faviconUrl;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("inputMaxRedirects", this.inputMaxRedirects);
        dataMap.put("resultRedirectCount", this.resultRedirectCount);
        dataMap.put("redirectPages", this.redirectPages);
        dataMap.put("destinationUrl", this.destinationUrl);
        dataMap.put("pageAuthor", this.pageAuthor);
        dataMap.put("pageSummary", this.pageSummary);
        dataMap.put("favicon", this.favicon);
        dataMap.put("faviconUrl", this.faviconUrl);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        PageFetch thatObj = (PageFetch) obj;
        if( (this.inputMaxRedirects == null && thatObj.getInputMaxRedirects() != null)
            || (this.inputMaxRedirects != null && thatObj.getInputMaxRedirects() == null)
            || !this.inputMaxRedirects.equals(thatObj.getInputMaxRedirects()) ) {
            return false;
        }
        if( (this.resultRedirectCount == null && thatObj.getResultRedirectCount() != null)
            || (this.resultRedirectCount != null && thatObj.getResultRedirectCount() == null)
            || !this.resultRedirectCount.equals(thatObj.getResultRedirectCount()) ) {
            return false;
        }
        if( (this.redirectPages == null && thatObj.getRedirectPages() != null)
            || (this.redirectPages != null && thatObj.getRedirectPages() == null)
            || !this.redirectPages.equals(thatObj.getRedirectPages()) ) {
            return false;
        }
        if( (this.destinationUrl == null && thatObj.getDestinationUrl() != null)
            || (this.destinationUrl != null && thatObj.getDestinationUrl() == null)
            || !this.destinationUrl.equals(thatObj.getDestinationUrl()) ) {
            return false;
        }
        if( (this.pageAuthor == null && thatObj.getPageAuthor() != null)
            || (this.pageAuthor != null && thatObj.getPageAuthor() == null)
            || !this.pageAuthor.equals(thatObj.getPageAuthor()) ) {
            return false;
        }
        if( (this.pageSummary == null && thatObj.getPageSummary() != null)
            || (this.pageSummary != null && thatObj.getPageSummary() == null)
            || !this.pageSummary.equals(thatObj.getPageSummary()) ) {
            return false;
        }
        if( (this.favicon == null && thatObj.getFavicon() != null)
            || (this.favicon != null && thatObj.getFavicon() == null)
            || !this.favicon.equals(thatObj.getFavicon()) ) {
            return false;
        }
        if( (this.faviconUrl == null && thatObj.getFaviconUrl() != null)
            || (this.faviconUrl != null && thatObj.getFaviconUrl() == null)
            || !this.faviconUrl.equals(thatObj.getFaviconUrl()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = inputMaxRedirects == null ? 0 : inputMaxRedirects.hashCode();
        _hash = 31 * _hash + delta;
        delta = resultRedirectCount == null ? 0 : resultRedirectCount.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectPages == null ? 0 : redirectPages.hashCode();
        _hash = 31 * _hash + delta;
        delta = destinationUrl == null ? 0 : destinationUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageAuthor == null ? 0 : pageAuthor.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageSummary == null ? 0 : pageSummary.hashCode();
        _hash = 31 * _hash + delta;
        delta = favicon == null ? 0 : favicon.hashCode();
        _hash = 31 * _hash + delta;
        delta = faviconUrl == null ? 0 : faviconUrl.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
