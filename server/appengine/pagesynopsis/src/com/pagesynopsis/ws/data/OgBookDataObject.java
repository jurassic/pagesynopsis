package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class OgBookDataObject extends OgObjectBaseDataObject implements OgBook
{
    private static final Logger log = Logger.getLogger(OgBookDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(OgBookDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(OgBookDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> author;

    @Persistent(defaultFetchGroup = "true")
    private String isbn;

    @Persistent(defaultFetchGroup = "false")
    private List<String> tag;

    @Persistent(defaultFetchGroup = "true")
    private String releaseDate;

    public OgBookDataObject()
    {
        this(null);
    }
    public OgBookDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgBookDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate, null, null);
    }
    public OgBookDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);
        this.author = author;  // ???
        this.isbn = isbn;
        this.tag = tag;  // ???
        this.releaseDate = releaseDate;
    }

//    @Override
//    protected Key createKey()
//    {
//        return OgBookDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return OgBookDataObject.composeKey(getGuid());
    }

    public List<String> getAuthor()
    {
        return this.author;   // ???
    }
    public void setAuthor(List<String> author)
    {
        this.author = author;
    }

    public String getIsbn()
    {
        return this.isbn;
    }
    public void setIsbn(String isbn)
    {
        this.isbn = isbn;
    }

    public List<String> getTag()
    {
        return this.tag;   // ???
    }
    public void setTag(List<String> tag)
    {
        this.tag = tag;
    }

    public String getReleaseDate()
    {
        return this.releaseDate;
    }
    public void setReleaseDate(String releaseDate)
    {
        this.releaseDate = releaseDate;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("author", this.author);
        dataMap.put("isbn", this.isbn);
        dataMap.put("tag", this.tag);
        dataMap.put("releaseDate", this.releaseDate);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OgBook thatObj = (OgBook) obj;
        if( (this.author == null && thatObj.getAuthor() != null)
            || (this.author != null && thatObj.getAuthor() == null)
            || !this.author.equals(thatObj.getAuthor()) ) {
            return false;
        }
        if( (this.isbn == null && thatObj.getIsbn() != null)
            || (this.isbn != null && thatObj.getIsbn() == null)
            || !this.isbn.equals(thatObj.getIsbn()) ) {
            return false;
        }
        if( (this.tag == null && thatObj.getTag() != null)
            || (this.tag != null && thatObj.getTag() == null)
            || !this.tag.equals(thatObj.getTag()) ) {
            return false;
        }
        if( (this.releaseDate == null && thatObj.getReleaseDate() != null)
            || (this.releaseDate != null && thatObj.getReleaseDate() == null)
            || !this.releaseDate.equals(thatObj.getReleaseDate()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = author == null ? 0 : author.hashCode();
        _hash = 31 * _hash + delta;
        delta = isbn == null ? 0 : isbn.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = releaseDate == null ? 0 : releaseDate.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
