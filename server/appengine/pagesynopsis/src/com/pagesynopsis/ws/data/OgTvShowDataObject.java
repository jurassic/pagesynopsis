package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class OgTvShowDataObject extends OgObjectBaseDataObject implements OgTvShow
{
    private static final Logger log = Logger.getLogger(OgTvShowDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(OgTvShowDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(OgTvShowDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> director;

    @Persistent(defaultFetchGroup = "false")
    private List<String> writer;

    @Persistent(defaultFetchGroup = "false")
    private List<OgActorStructDataObject> actor;

    @Persistent(defaultFetchGroup = "true")
    private Integer duration;

    @Persistent(defaultFetchGroup = "false")
    private List<String> tag;

    @Persistent(defaultFetchGroup = "true")
    private String releaseDate;

    public OgTvShowDataObject()
    {
        this(null);
    }
    public OgTvShowDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgTvShowDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate, null, null);
    }
    public OgTvShowDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);
        this.director = director;  // ???
        this.writer = writer;  // ???
        setActor(actor);
        this.duration = duration;
        this.tag = tag;  // ???
        this.releaseDate = releaseDate;
    }

//    @Override
//    protected Key createKey()
//    {
//        return OgTvShowDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return OgTvShowDataObject.composeKey(getGuid());
    }

    public List<String> getDirector()
    {
        return this.director;   // ???
    }
    public void setDirector(List<String> director)
    {
        this.director = director;
    }

    public List<String> getWriter()
    {
        return this.writer;   // ???
    }
    public void setWriter(List<String> writer)
    {
        this.writer = writer;
    }

    @SuppressWarnings("unchecked")
    public List<OgActorStruct> getActor()
    {         
        return (List<OgActorStruct>) ((List<?>) this.actor);
    }
    public void setActor(List<OgActorStruct> actor)
    {
        if(actor != null) {
            List<OgActorStructDataObject> dataObj = new ArrayList<OgActorStructDataObject>();
            for(OgActorStruct ogActorStruct : actor) {
                OgActorStructDataObject elem = null;
                if(ogActorStruct instanceof OgActorStructDataObject) {
                    elem = (OgActorStructDataObject) ogActorStruct;
                } else if(ogActorStruct instanceof OgActorStruct) {
                    elem = new OgActorStructDataObject(ogActorStruct.getUuid(), ogActorStruct.getProfile(), ogActorStruct.getRole());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.actor = dataObj;
        } else {
            this.actor = null;
        }
    }

    public Integer getDuration()
    {
        return this.duration;
    }
    public void setDuration(Integer duration)
    {
        this.duration = duration;
    }

    public List<String> getTag()
    {
        return this.tag;   // ???
    }
    public void setTag(List<String> tag)
    {
        this.tag = tag;
    }

    public String getReleaseDate()
    {
        return this.releaseDate;
    }
    public void setReleaseDate(String releaseDate)
    {
        this.releaseDate = releaseDate;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("director", this.director);
        dataMap.put("writer", this.writer);
        dataMap.put("actor", this.actor);
        dataMap.put("duration", this.duration);
        dataMap.put("tag", this.tag);
        dataMap.put("releaseDate", this.releaseDate);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OgTvShow thatObj = (OgTvShow) obj;
        if( (this.director == null && thatObj.getDirector() != null)
            || (this.director != null && thatObj.getDirector() == null)
            || !this.director.equals(thatObj.getDirector()) ) {
            return false;
        }
        if( (this.writer == null && thatObj.getWriter() != null)
            || (this.writer != null && thatObj.getWriter() == null)
            || !this.writer.equals(thatObj.getWriter()) ) {
            return false;
        }
        if( (this.actor == null && thatObj.getActor() != null)
            || (this.actor != null && thatObj.getActor() == null)
            || !this.actor.equals(thatObj.getActor()) ) {
            return false;
        }
        if( (this.duration == null && thatObj.getDuration() != null)
            || (this.duration != null && thatObj.getDuration() == null)
            || !this.duration.equals(thatObj.getDuration()) ) {
            return false;
        }
        if( (this.tag == null && thatObj.getTag() != null)
            || (this.tag != null && thatObj.getTag() == null)
            || !this.tag.equals(thatObj.getTag()) ) {
            return false;
        }
        if( (this.releaseDate == null && thatObj.getReleaseDate() != null)
            || (this.releaseDate != null && thatObj.getReleaseDate() == null)
            || !this.releaseDate.equals(thatObj.getReleaseDate()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = director == null ? 0 : director.hashCode();
        _hash = 31 * _hash + delta;
        delta = writer == null ? 0 : writer.hashCode();
        _hash = 31 * _hash + delta;
        delta = actor == null ? 0 : actor.hashCode();
        _hash = 31 * _hash + delta;
        delta = duration == null ? 0 : duration.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = releaseDate == null ? 0 : releaseDate.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
