package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class OgVideoStructDataObject implements OgVideoStruct, Serializable
{
    private static final Logger log = Logger.getLogger(OgVideoStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _ogvideostruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String url;

    @Persistent(defaultFetchGroup = "true")
    private String secureUrl;

    @Persistent(defaultFetchGroup = "true")
    private String type;

    @Persistent(defaultFetchGroup = "true")
    private Integer width;

    @Persistent(defaultFetchGroup = "true")
    private Integer height;

    public OgVideoStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null);
    }
    public OgVideoStructDataObject(String uuid, String url, String secureUrl, String type, Integer width, Integer height)
    {
        setUuid(uuid);
        setUrl(url);
        setSecureUrl(secureUrl);
        setType(type);
        setWidth(width);
        setHeight(height);
    }

    private void resetEncodedKey()
    {
        if(_ogvideostruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _ogvideostruct_encoded_key = KeyFactory.createKeyString(OgVideoStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _ogvideostruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _ogvideostruct_encoded_key = KeyFactory.createKeyString(OgVideoStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
        if(this.url != null) {
            resetEncodedKey();
        }
    }

    public String getSecureUrl()
    {
        return this.secureUrl;
    }
    public void setSecureUrl(String secureUrl)
    {
        this.secureUrl = secureUrl;
        if(this.secureUrl != null) {
            resetEncodedKey();
        }
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
        if(this.type != null) {
            resetEncodedKey();
        }
    }

    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
        if(this.width != null) {
            resetEncodedKey();
        }
    }

    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
        if(this.height != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecureUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("url", this.url);
        dataMap.put("secureUrl", this.secureUrl);
        dataMap.put("type", this.type);
        dataMap.put("width", this.width);
        dataMap.put("height", this.height);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OgVideoStruct thatObj = (OgVideoStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.url == null && thatObj.getUrl() != null)
            || (this.url != null && thatObj.getUrl() == null)
            || !this.url.equals(thatObj.getUrl()) ) {
            return false;
        }
        if( (this.secureUrl == null && thatObj.getSecureUrl() != null)
            || (this.secureUrl != null && thatObj.getSecureUrl() == null)
            || !this.secureUrl.equals(thatObj.getSecureUrl()) ) {
            return false;
        }
        if( (this.type == null && thatObj.getType() != null)
            || (this.type != null && thatObj.getType() == null)
            || !this.type.equals(thatObj.getType()) ) {
            return false;
        }
        if( (this.width == null && thatObj.getWidth() != null)
            || (this.width != null && thatObj.getWidth() == null)
            || !this.width.equals(thatObj.getWidth()) ) {
            return false;
        }
        if( (this.height == null && thatObj.getHeight() != null)
            || (this.height != null && thatObj.getHeight() == null)
            || !this.height.equals(thatObj.getHeight()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        delta = secureUrl == null ? 0 : secureUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = width == null ? 0 : width.hashCode();
        _hash = 31 * _hash + delta;
        delta = height == null ? 0 : height.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
