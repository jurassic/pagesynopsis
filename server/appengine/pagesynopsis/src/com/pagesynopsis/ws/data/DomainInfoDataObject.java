package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class DomainInfoDataObject extends KeyedDataObject implements DomainInfo
{
    private static final Logger log = Logger.getLogger(DomainInfoDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(DomainInfoDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(DomainInfoDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private Boolean excluded;

    @Persistent(defaultFetchGroup = "true")
    private String category;

    @Persistent(defaultFetchGroup = "true")
    private String reputation;

    @Persistent(defaultFetchGroup = "true")
    private String authority;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long lastCheckedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastUpdatedTime;

    public DomainInfoDataObject()
    {
        this(null);
    }
    public DomainInfoDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public DomainInfoDataObject(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public DomainInfoDataObject(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.domain = domain;
        this.excluded = excluded;
        this.category = category;
        this.reputation = reputation;
        this.authority = authority;
        this.note = note;
        this.status = status;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return DomainInfoDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return DomainInfoDataObject.composeKey(getGuid());
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public Boolean isExcluded()
    {
        return this.excluded;
    }
    public void setExcluded(Boolean excluded)
    {
        this.excluded = excluded;
    }

    public String getCategory()
    {
        return this.category;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getReputation()
    {
        return this.reputation;
    }
    public void setReputation(String reputation)
    {
        this.reputation = reputation;
    }

    public String getAuthority()
    {
        return this.authority;
    }
    public void setAuthority(String authority)
    {
        this.authority = authority;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("domain", this.domain);
        dataMap.put("excluded", this.excluded);
        dataMap.put("category", this.category);
        dataMap.put("reputation", this.reputation);
        dataMap.put("authority", this.authority);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        DomainInfo thatObj = (DomainInfo) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.excluded == null && thatObj.isExcluded() != null)
            || (this.excluded != null && thatObj.isExcluded() == null)
            || !this.excluded.equals(thatObj.isExcluded()) ) {
            return false;
        }
        if( (this.category == null && thatObj.getCategory() != null)
            || (this.category != null && thatObj.getCategory() == null)
            || !this.category.equals(thatObj.getCategory()) ) {
            return false;
        }
        if( (this.reputation == null && thatObj.getReputation() != null)
            || (this.reputation != null && thatObj.getReputation() == null)
            || !this.reputation.equals(thatObj.getReputation()) ) {
            return false;
        }
        if( (this.authority == null && thatObj.getAuthority() != null)
            || (this.authority != null && thatObj.getAuthority() == null)
            || !this.authority.equals(thatObj.getAuthority()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.lastCheckedTime == null && thatObj.getLastCheckedTime() != null)
            || (this.lastCheckedTime != null && thatObj.getLastCheckedTime() == null)
            || !this.lastCheckedTime.equals(thatObj.getLastCheckedTime()) ) {
            return false;
        }
        if( (this.lastUpdatedTime == null && thatObj.getLastUpdatedTime() != null)
            || (this.lastUpdatedTime != null && thatObj.getLastUpdatedTime() == null)
            || !this.lastUpdatedTime.equals(thatObj.getLastUpdatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = excluded == null ? 0 : excluded.hashCode();
        _hash = 31 * _hash + delta;
        delta = category == null ? 0 : category.hashCode();
        _hash = 31 * _hash + delta;
        delta = reputation == null ? 0 : reputation.hashCode();
        _hash = 31 * _hash + delta;
        delta = authority == null ? 0 : authority.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
