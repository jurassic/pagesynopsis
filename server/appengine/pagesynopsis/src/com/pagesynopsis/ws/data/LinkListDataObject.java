package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class LinkListDataObject extends PageBaseDataObject implements LinkList
{
    private static final Logger log = Logger.getLogger(LinkListDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(LinkListDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(LinkListDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> urlSchemeFilter;

    @Persistent(defaultFetchGroup = "false")
    private List<AnchorStructDataObject> pageAnchors;

    @Persistent(defaultFetchGroup = "true")
    private Boolean excludeRelativeUrls;

    @Persistent(defaultFetchGroup = "false")
    private List<String> excludedBaseUrls;

    public LinkListDataObject()
    {
        this(null);
    }
    public LinkListDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public LinkListDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls, null, null);
    }
    public LinkListDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.urlSchemeFilter = urlSchemeFilter;  // ???
        setPageAnchors(pageAnchors);
        this.excludeRelativeUrls = excludeRelativeUrls;
        this.excludedBaseUrls = excludedBaseUrls;  // ???
    }

//    @Override
//    protected Key createKey()
//    {
//        return LinkListDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return LinkListDataObject.composeKey(getGuid());
    }

    public List<String> getUrlSchemeFilter()
    {
        return this.urlSchemeFilter;   // ???
    }
    public void setUrlSchemeFilter(List<String> urlSchemeFilter)
    {
        this.urlSchemeFilter = urlSchemeFilter;
    }

    @SuppressWarnings("unchecked")
    public List<AnchorStruct> getPageAnchors()
    {         
        return (List<AnchorStruct>) ((List<?>) this.pageAnchors);
    }
    public void setPageAnchors(List<AnchorStruct> pageAnchors)
    {
        if(pageAnchors != null) {
            List<AnchorStructDataObject> dataObj = new ArrayList<AnchorStructDataObject>();
            for(AnchorStruct anchorStruct : pageAnchors) {
                AnchorStructDataObject elem = null;
                if(anchorStruct instanceof AnchorStructDataObject) {
                    elem = (AnchorStructDataObject) anchorStruct;
                } else if(anchorStruct instanceof AnchorStruct) {
                    elem = new AnchorStructDataObject(anchorStruct.getUuid(), anchorStruct.getId(), anchorStruct.getName(), anchorStruct.getHref(), anchorStruct.getHrefUrl(), anchorStruct.getUrlScheme(), anchorStruct.getRel(), anchorStruct.getTarget(), anchorStruct.getInnerHtml(), anchorStruct.getAnchorText(), anchorStruct.getAnchorTitle(), anchorStruct.getAnchorImage(), anchorStruct.getAnchorLegend(), anchorStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.pageAnchors = dataObj;
        } else {
            this.pageAnchors = null;
        }
    }

    public Boolean isExcludeRelativeUrls()
    {
        return this.excludeRelativeUrls;
    }
    public void setExcludeRelativeUrls(Boolean excludeRelativeUrls)
    {
        this.excludeRelativeUrls = excludeRelativeUrls;
    }

    public List<String> getExcludedBaseUrls()
    {
        return this.excludedBaseUrls;   // ???
    }
    public void setExcludedBaseUrls(List<String> excludedBaseUrls)
    {
        this.excludedBaseUrls = excludedBaseUrls;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("urlSchemeFilter", this.urlSchemeFilter);
        dataMap.put("pageAnchors", this.pageAnchors);
        dataMap.put("excludeRelativeUrls", this.excludeRelativeUrls);
        dataMap.put("excludedBaseUrls", this.excludedBaseUrls);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        LinkList thatObj = (LinkList) obj;
        if( (this.urlSchemeFilter == null && thatObj.getUrlSchemeFilter() != null)
            || (this.urlSchemeFilter != null && thatObj.getUrlSchemeFilter() == null)
            || !this.urlSchemeFilter.equals(thatObj.getUrlSchemeFilter()) ) {
            return false;
        }
        if( (this.pageAnchors == null && thatObj.getPageAnchors() != null)
            || (this.pageAnchors != null && thatObj.getPageAnchors() == null)
            || !this.pageAnchors.equals(thatObj.getPageAnchors()) ) {
            return false;
        }
        if( (this.excludeRelativeUrls == null && thatObj.isExcludeRelativeUrls() != null)
            || (this.excludeRelativeUrls != null && thatObj.isExcludeRelativeUrls() == null)
            || !this.excludeRelativeUrls.equals(thatObj.isExcludeRelativeUrls()) ) {
            return false;
        }
        if( (this.excludedBaseUrls == null && thatObj.getExcludedBaseUrls() != null)
            || (this.excludedBaseUrls != null && thatObj.getExcludedBaseUrls() == null)
            || !this.excludedBaseUrls.equals(thatObj.getExcludedBaseUrls()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = urlSchemeFilter == null ? 0 : urlSchemeFilter.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageAnchors == null ? 0 : pageAnchors.hashCode();
        _hash = 31 * _hash + delta;
        delta = excludeRelativeUrls == null ? 0 : excludeRelativeUrls.hashCode();
        _hash = 31 * _hash + delta;
        delta = excludedBaseUrls == null ? 0 : excludedBaseUrls.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
