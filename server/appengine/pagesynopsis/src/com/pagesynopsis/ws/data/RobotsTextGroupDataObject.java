package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;
import java.util.List;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class RobotsTextGroupDataObject implements RobotsTextGroup, Serializable
{
    private static final Logger log = Logger.getLogger(RobotsTextGroupDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _robotstextgroup_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String userAgent;

    @Persistent(defaultFetchGroup = "true")
    private List<String> allows;

    @Persistent(defaultFetchGroup = "true")
    private List<String> disallows;

    public RobotsTextGroupDataObject()
    {
        // ???
        // this(null, null, null, null);
    }
    public RobotsTextGroupDataObject(String uuid, String userAgent, List<String> allows, List<String> disallows)
    {
        setUuid(uuid);
        setUserAgent(userAgent);
        this.allows = allows;  // ???
        this.disallows = disallows;  // ???
    }

    private void resetEncodedKey()
    {
        if(_robotstextgroup_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _robotstextgroup_encoded_key = KeyFactory.createKeyString(RobotsTextGroupDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _robotstextgroup_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _robotstextgroup_encoded_key = KeyFactory.createKeyString(RobotsTextGroupDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
        if(this.userAgent != null) {
            resetEncodedKey();
        }
    }

    public List<String> getAllows()
    {
        return this.allows;   // ???
    }
    public void setAllows(List<String> allows)
    {
        this.allows = allows;
    }

    public List<String> getDisallows()
    {
        return this.disallows;   // ???
    }
    public void setDisallows(List<String> disallows)
    {
        this.disallows = disallows;
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAllows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisallows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("userAgent", this.userAgent);
        dataMap.put("allows", this.allows);
        dataMap.put("disallows", this.disallows);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RobotsTextGroup thatObj = (RobotsTextGroup) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.userAgent == null && thatObj.getUserAgent() != null)
            || (this.userAgent != null && thatObj.getUserAgent() == null)
            || !this.userAgent.equals(thatObj.getUserAgent()) ) {
            return false;
        }
        if( (this.allows == null && thatObj.getAllows() != null)
            || (this.allows != null && thatObj.getAllows() == null)
            || !this.allows.equals(thatObj.getAllows()) ) {
            return false;
        }
        if( (this.disallows == null && thatObj.getDisallows() != null)
            || (this.disallows != null && thatObj.getDisallows() == null)
            || !this.disallows.equals(thatObj.getDisallows()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = userAgent == null ? 0 : userAgent.hashCode();
        _hash = 31 * _hash + delta;
        delta = allows == null ? 0 : allows.hashCode();
        _hash = 31 * _hash + delta;
        delta = disallows == null ? 0 : disallows.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
