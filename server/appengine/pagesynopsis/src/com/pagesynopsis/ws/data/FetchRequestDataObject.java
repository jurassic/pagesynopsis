package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class FetchRequestDataObject extends KeyedDataObject implements FetchRequest
{
    private static final Logger log = Logger.getLogger(FetchRequestDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(FetchRequestDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(FetchRequestDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String targetUrl;

    @Persistent(defaultFetchGroup = "true")
    private String pageUrl;

    @Persistent(defaultFetchGroup = "true")
    private String queryString;

    @Persistent(defaultFetchGroup = "false")
    private List<KeyValuePairStructDataObject> queryParams;

    @Persistent(defaultFetchGroup = "true")
    private String version;

    @Persistent(defaultFetchGroup = "true")
    private String fetchObject;

    @Persistent(defaultFetchGroup = "true")
    private Integer fetchStatus;

    @Persistent(defaultFetchGroup = "true")
    private String result;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "false")
    @Embedded(members = {
        @Persistent(name="referer", columns=@Column(name="referrerInforeferer")),
        @Persistent(name="userAgent", columns=@Column(name="referrerInfouserAgent")),
        @Persistent(name="language", columns=@Column(name="referrerInfolanguage")),
        @Persistent(name="hostname", columns=@Column(name="referrerInfohostname")),
        @Persistent(name="ipAddress", columns=@Column(name="referrerInfoipAddress")),
        @Persistent(name="note", columns=@Column(name="referrerInfonote")),
    })
    private ReferrerInfoStructDataObject referrerInfo;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private String nextPageUrl;

    @Persistent(defaultFetchGroup = "true")
    private Integer followPages;

    @Persistent(defaultFetchGroup = "true")
    private Integer followDepth;

    @Persistent(defaultFetchGroup = "true")
    private Boolean createVersion;

    @Persistent(defaultFetchGroup = "true")
    private Boolean deferred;

    @Persistent(defaultFetchGroup = "true")
    private Boolean alert;

    @Persistent(defaultFetchGroup = "true")
    @Embedded(members = {
        @Persistent(name="preferredMode", columns=@Column(name="notificationPrefpreferredMode")),
        @Persistent(name="mobileNumber", columns=@Column(name="notificationPrefmobileNumber")),
        @Persistent(name="emailAddress", columns=@Column(name="notificationPrefemailAddress")),
        @Persistent(name="twitterUsername", columns=@Column(name="notificationPreftwitterUsername")),
        @Persistent(name="facebookId", columns=@Column(name="notificationPreffacebookId")),
        @Persistent(name="linkedinId", columns=@Column(name="notificationPreflinkedinId")),
        @Persistent(name="note", columns=@Column(name="notificationPrefnote")),
    })
    private NotificationStructDataObject notificationPref;

    public FetchRequestDataObject()
    {
        this(null);
    }
    public FetchRequestDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchRequestDataObject(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref)
    {
        this(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref, null, null);
    }
    public FetchRequestDataObject(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.targetUrl = targetUrl;
        this.pageUrl = pageUrl;
        this.queryString = queryString;
        setQueryParams(queryParams);
        this.version = version;
        this.fetchObject = fetchObject;
        this.fetchStatus = fetchStatus;
        this.result = result;
        this.note = note;
        if(referrerInfo != null) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = null;
        }
        this.status = status;
        this.nextPageUrl = nextPageUrl;
        this.followPages = followPages;
        this.followDepth = followDepth;
        this.createVersion = createVersion;
        this.deferred = deferred;
        this.alert = alert;
        if(notificationPref != null) {
            this.notificationPref = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            this.notificationPref = null;
        }
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return FetchRequestDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return FetchRequestDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getTargetUrl()
    {
        return this.targetUrl;
    }
    public void setTargetUrl(String targetUrl)
    {
        this.targetUrl = targetUrl;
    }

    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    @SuppressWarnings("unchecked")
    public List<KeyValuePairStruct> getQueryParams()
    {         
        return (List<KeyValuePairStruct>) ((List<?>) this.queryParams);
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        if(queryParams != null) {
            List<KeyValuePairStructDataObject> dataObj = new ArrayList<KeyValuePairStructDataObject>();
            for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                KeyValuePairStructDataObject elem = null;
                if(keyValuePairStruct instanceof KeyValuePairStructDataObject) {
                    elem = (KeyValuePairStructDataObject) keyValuePairStruct;
                } else if(keyValuePairStruct instanceof KeyValuePairStruct) {
                    elem = new KeyValuePairStructDataObject(keyValuePairStruct.getUuid(), keyValuePairStruct.getKey(), keyValuePairStruct.getValue(), keyValuePairStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.queryParams = dataObj;
        } else {
            this.queryParams = null;
        }
    }

    public String getVersion()
    {
        return this.version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getFetchObject()
    {
        return this.fetchObject;
    }
    public void setFetchObject(String fetchObject)
    {
        this.fetchObject = fetchObject;
    }

    public Integer getFetchStatus()
    {
        return this.fetchStatus;
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        this.fetchStatus = fetchStatus;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        return this.referrerInfo;
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(referrerInfo == null) {
            this.referrerInfo = null;
            log.log(Level.INFO, "FetchRequestDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is null.");            
        } else if(referrerInfo instanceof ReferrerInfoStructDataObject) {
            this.referrerInfo = (ReferrerInfoStructDataObject) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            this.referrerInfo = new ReferrerInfoStructDataObject(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            this.referrerInfo = new ReferrerInfoStructDataObject();   // ????
            log.log(Level.WARNING, "FetchRequestDataObject.setReferrerInfo(ReferrerInfoStruct referrerInfo): Arg referrerInfo is of an invalid type.");
        }
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getNextPageUrl()
    {
        return this.nextPageUrl;
    }
    public void setNextPageUrl(String nextPageUrl)
    {
        this.nextPageUrl = nextPageUrl;
    }

    public Integer getFollowPages()
    {
        return this.followPages;
    }
    public void setFollowPages(Integer followPages)
    {
        this.followPages = followPages;
    }

    public Integer getFollowDepth()
    {
        return this.followDepth;
    }
    public void setFollowDepth(Integer followDepth)
    {
        this.followDepth = followDepth;
    }

    public Boolean isCreateVersion()
    {
        return this.createVersion;
    }
    public void setCreateVersion(Boolean createVersion)
    {
        this.createVersion = createVersion;
    }

    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    public Boolean isAlert()
    {
        return this.alert;
    }
    public void setAlert(Boolean alert)
    {
        this.alert = alert;
    }

    public NotificationStruct getNotificationPref()
    {
        return this.notificationPref;
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(notificationPref == null) {
            this.notificationPref = null;
            log.log(Level.INFO, "FetchRequestDataObject.setNotificationPref(NotificationStruct notificationPref): Arg notificationPref is null.");            
        } else if(notificationPref instanceof NotificationStructDataObject) {
            this.notificationPref = (NotificationStructDataObject) notificationPref;
        } else if(notificationPref instanceof NotificationStruct) {
            this.notificationPref = new NotificationStructDataObject(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            this.notificationPref = new NotificationStructDataObject();   // ????
            log.log(Level.WARNING, "FetchRequestDataObject.setNotificationPref(NotificationStruct notificationPref): Arg notificationPref is of an invalid type.");
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("targetUrl", this.targetUrl);
        dataMap.put("pageUrl", this.pageUrl);
        dataMap.put("queryString", this.queryString);
        dataMap.put("queryParams", this.queryParams);
        dataMap.put("version", this.version);
        dataMap.put("fetchObject", this.fetchObject);
        dataMap.put("fetchStatus", this.fetchStatus);
        dataMap.put("result", this.result);
        dataMap.put("note", this.note);
        dataMap.put("referrerInfo", this.referrerInfo);
        dataMap.put("status", this.status);
        dataMap.put("nextPageUrl", this.nextPageUrl);
        dataMap.put("followPages", this.followPages);
        dataMap.put("followDepth", this.followDepth);
        dataMap.put("createVersion", this.createVersion);
        dataMap.put("deferred", this.deferred);
        dataMap.put("alert", this.alert);
        dataMap.put("notificationPref", this.notificationPref);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        FetchRequest thatObj = (FetchRequest) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.targetUrl == null && thatObj.getTargetUrl() != null)
            || (this.targetUrl != null && thatObj.getTargetUrl() == null)
            || !this.targetUrl.equals(thatObj.getTargetUrl()) ) {
            return false;
        }
        if( (this.pageUrl == null && thatObj.getPageUrl() != null)
            || (this.pageUrl != null && thatObj.getPageUrl() == null)
            || !this.pageUrl.equals(thatObj.getPageUrl()) ) {
            return false;
        }
        if( (this.queryString == null && thatObj.getQueryString() != null)
            || (this.queryString != null && thatObj.getQueryString() == null)
            || !this.queryString.equals(thatObj.getQueryString()) ) {
            return false;
        }
        if( (this.queryParams == null && thatObj.getQueryParams() != null)
            || (this.queryParams != null && thatObj.getQueryParams() == null)
            || !this.queryParams.equals(thatObj.getQueryParams()) ) {
            return false;
        }
        if( (this.version == null && thatObj.getVersion() != null)
            || (this.version != null && thatObj.getVersion() == null)
            || !this.version.equals(thatObj.getVersion()) ) {
            return false;
        }
        if( (this.fetchObject == null && thatObj.getFetchObject() != null)
            || (this.fetchObject != null && thatObj.getFetchObject() == null)
            || !this.fetchObject.equals(thatObj.getFetchObject()) ) {
            return false;
        }
        if( (this.fetchStatus == null && thatObj.getFetchStatus() != null)
            || (this.fetchStatus != null && thatObj.getFetchStatus() == null)
            || !this.fetchStatus.equals(thatObj.getFetchStatus()) ) {
            return false;
        }
        if( (this.result == null && thatObj.getResult() != null)
            || (this.result != null && thatObj.getResult() == null)
            || !this.result.equals(thatObj.getResult()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.referrerInfo == null && thatObj.getReferrerInfo() != null)
            || (this.referrerInfo != null && thatObj.getReferrerInfo() == null)
            || !this.referrerInfo.equals(thatObj.getReferrerInfo()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.nextPageUrl == null && thatObj.getNextPageUrl() != null)
            || (this.nextPageUrl != null && thatObj.getNextPageUrl() == null)
            || !this.nextPageUrl.equals(thatObj.getNextPageUrl()) ) {
            return false;
        }
        if( (this.followPages == null && thatObj.getFollowPages() != null)
            || (this.followPages != null && thatObj.getFollowPages() == null)
            || !this.followPages.equals(thatObj.getFollowPages()) ) {
            return false;
        }
        if( (this.followDepth == null && thatObj.getFollowDepth() != null)
            || (this.followDepth != null && thatObj.getFollowDepth() == null)
            || !this.followDepth.equals(thatObj.getFollowDepth()) ) {
            return false;
        }
        if( (this.createVersion == null && thatObj.isCreateVersion() != null)
            || (this.createVersion != null && thatObj.isCreateVersion() == null)
            || !this.createVersion.equals(thatObj.isCreateVersion()) ) {
            return false;
        }
        if( (this.deferred == null && thatObj.isDeferred() != null)
            || (this.deferred != null && thatObj.isDeferred() == null)
            || !this.deferred.equals(thatObj.isDeferred()) ) {
            return false;
        }
        if( (this.alert == null && thatObj.isAlert() != null)
            || (this.alert != null && thatObj.isAlert() == null)
            || !this.alert.equals(thatObj.isAlert()) ) {
            return false;
        }
        if( (this.notificationPref == null && thatObj.getNotificationPref() != null)
            || (this.notificationPref != null && thatObj.getNotificationPref() == null)
            || !this.notificationPref.equals(thatObj.getNotificationPref()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetUrl == null ? 0 : targetUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageUrl == null ? 0 : pageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryString == null ? 0 : queryString.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryParams == null ? 0 : queryParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = version == null ? 0 : version.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchObject == null ? 0 : fetchObject.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchStatus == null ? 0 : fetchStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = result == null ? 0 : result.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextPageUrl == null ? 0 : nextPageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = followPages == null ? 0 : followPages.hashCode();
        _hash = 31 * _hash + delta;
        delta = followDepth == null ? 0 : followDepth.hashCode();
        _hash = 31 * _hash + delta;
        delta = createVersion == null ? 0 : createVersion.hashCode();
        _hash = 31 * _hash + delta;
        delta = deferred == null ? 0 : deferred.hashCode();
        _hash = 31 * _hash + delta;
        delta = alert == null ? 0 : alert.hashCode();
        _hash = 31 * _hash + delta;
        delta = notificationPref == null ? 0 : notificationPref.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
