package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgObjectBase;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class OgObjectBaseDataObject extends KeyedDataObject implements OgObjectBase
{
    private static final Logger log = Logger.getLogger(OgObjectBaseDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(OgObjectBaseDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(OgObjectBaseDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String url;

    @Persistent(defaultFetchGroup = "true")
    private String type;

    @Persistent(defaultFetchGroup = "true")
    private String siteName;

    @Persistent(defaultFetchGroup = "true")
    private String title;

    @Persistent(defaultFetchGroup = "true")
    private String description;

    @Persistent(defaultFetchGroup = "true")
    private List<String> fbAdmins;

    @Persistent(defaultFetchGroup = "true")
    private List<String> fbAppId;

    @Persistent(defaultFetchGroup = "false")
    private List<OgImageStructDataObject> image;

    @Persistent(defaultFetchGroup = "false")
    private List<OgAudioStructDataObject> audio;

    @Persistent(defaultFetchGroup = "false")
    private List<OgVideoStructDataObject> video;

    @Persistent(defaultFetchGroup = "true")
    private String locale;

    @Persistent(defaultFetchGroup = "false")
    private List<String> localeAlternate;

    public OgObjectBaseDataObject()
    {
        this(null);
    }
    public OgObjectBaseDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgObjectBaseDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, null, null);
    }
    public OgObjectBaseDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.url = url;
        this.type = type;
        this.siteName = siteName;
        this.title = title;
        this.description = description;
        this.fbAdmins = fbAdmins;  // ???
        this.fbAppId = fbAppId;  // ???
        setImage(image);
        setAudio(audio);
        setVideo(video);
        this.locale = locale;
        this.localeAlternate = localeAlternate;  // ???
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return OgObjectBaseDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return OgObjectBaseDataObject.composeKey(getGuid());
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getSiteName()
    {
        return this.siteName;
    }
    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public List<String> getFbAdmins()
    {
        return this.fbAdmins;   // ???
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        this.fbAdmins = fbAdmins;
    }

    public List<String> getFbAppId()
    {
        return this.fbAppId;   // ???
    }
    public void setFbAppId(List<String> fbAppId)
    {
        this.fbAppId = fbAppId;
    }

    @SuppressWarnings("unchecked")
    public List<OgImageStruct> getImage()
    {         
        return (List<OgImageStruct>) ((List<?>) this.image);
    }
    public void setImage(List<OgImageStruct> image)
    {
        if(image != null) {
            List<OgImageStructDataObject> dataObj = new ArrayList<OgImageStructDataObject>();
            for(OgImageStruct ogImageStruct : image) {
                OgImageStructDataObject elem = null;
                if(ogImageStruct instanceof OgImageStructDataObject) {
                    elem = (OgImageStructDataObject) ogImageStruct;
                } else if(ogImageStruct instanceof OgImageStruct) {
                    elem = new OgImageStructDataObject(ogImageStruct.getUuid(), ogImageStruct.getUrl(), ogImageStruct.getSecureUrl(), ogImageStruct.getType(), ogImageStruct.getWidth(), ogImageStruct.getHeight());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.image = dataObj;
        } else {
            this.image = null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<OgAudioStruct> getAudio()
    {         
        return (List<OgAudioStruct>) ((List<?>) this.audio);
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        if(audio != null) {
            List<OgAudioStructDataObject> dataObj = new ArrayList<OgAudioStructDataObject>();
            for(OgAudioStruct ogAudioStruct : audio) {
                OgAudioStructDataObject elem = null;
                if(ogAudioStruct instanceof OgAudioStructDataObject) {
                    elem = (OgAudioStructDataObject) ogAudioStruct;
                } else if(ogAudioStruct instanceof OgAudioStruct) {
                    elem = new OgAudioStructDataObject(ogAudioStruct.getUuid(), ogAudioStruct.getUrl(), ogAudioStruct.getSecureUrl(), ogAudioStruct.getType());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.audio = dataObj;
        } else {
            this.audio = null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<OgVideoStruct> getVideo()
    {         
        return (List<OgVideoStruct>) ((List<?>) this.video);
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        if(video != null) {
            List<OgVideoStructDataObject> dataObj = new ArrayList<OgVideoStructDataObject>();
            for(OgVideoStruct ogVideoStruct : video) {
                OgVideoStructDataObject elem = null;
                if(ogVideoStruct instanceof OgVideoStructDataObject) {
                    elem = (OgVideoStructDataObject) ogVideoStruct;
                } else if(ogVideoStruct instanceof OgVideoStruct) {
                    elem = new OgVideoStructDataObject(ogVideoStruct.getUuid(), ogVideoStruct.getUrl(), ogVideoStruct.getSecureUrl(), ogVideoStruct.getType(), ogVideoStruct.getWidth(), ogVideoStruct.getHeight());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.video = dataObj;
        } else {
            this.video = null;
        }
    }

    public String getLocale()
    {
        return this.locale;
    }
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    public List<String> getLocaleAlternate()
    {
        return this.localeAlternate;   // ???
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        this.localeAlternate = localeAlternate;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("url", this.url);
        dataMap.put("type", this.type);
        dataMap.put("siteName", this.siteName);
        dataMap.put("title", this.title);
        dataMap.put("description", this.description);
        dataMap.put("fbAdmins", this.fbAdmins);
        dataMap.put("fbAppId", this.fbAppId);
        dataMap.put("image", this.image);
        dataMap.put("audio", this.audio);
        dataMap.put("video", this.video);
        dataMap.put("locale", this.locale);
        dataMap.put("localeAlternate", this.localeAlternate);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OgObjectBase thatObj = (OgObjectBase) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.url == null && thatObj.getUrl() != null)
            || (this.url != null && thatObj.getUrl() == null)
            || !this.url.equals(thatObj.getUrl()) ) {
            return false;
        }
        if( (this.type == null && thatObj.getType() != null)
            || (this.type != null && thatObj.getType() == null)
            || !this.type.equals(thatObj.getType()) ) {
            return false;
        }
        if( (this.siteName == null && thatObj.getSiteName() != null)
            || (this.siteName != null && thatObj.getSiteName() == null)
            || !this.siteName.equals(thatObj.getSiteName()) ) {
            return false;
        }
        if( (this.title == null && thatObj.getTitle() != null)
            || (this.title != null && thatObj.getTitle() == null)
            || !this.title.equals(thatObj.getTitle()) ) {
            return false;
        }
        if( (this.description == null && thatObj.getDescription() != null)
            || (this.description != null && thatObj.getDescription() == null)
            || !this.description.equals(thatObj.getDescription()) ) {
            return false;
        }
        if( (this.fbAdmins == null && thatObj.getFbAdmins() != null)
            || (this.fbAdmins != null && thatObj.getFbAdmins() == null)
            || !this.fbAdmins.equals(thatObj.getFbAdmins()) ) {
            return false;
        }
        if( (this.fbAppId == null && thatObj.getFbAppId() != null)
            || (this.fbAppId != null && thatObj.getFbAppId() == null)
            || !this.fbAppId.equals(thatObj.getFbAppId()) ) {
            return false;
        }
        if( (this.image == null && thatObj.getImage() != null)
            || (this.image != null && thatObj.getImage() == null)
            || !this.image.equals(thatObj.getImage()) ) {
            return false;
        }
        if( (this.audio == null && thatObj.getAudio() != null)
            || (this.audio != null && thatObj.getAudio() == null)
            || !this.audio.equals(thatObj.getAudio()) ) {
            return false;
        }
        if( (this.video == null && thatObj.getVideo() != null)
            || (this.video != null && thatObj.getVideo() == null)
            || !this.video.equals(thatObj.getVideo()) ) {
            return false;
        }
        if( (this.locale == null && thatObj.getLocale() != null)
            || (this.locale != null && thatObj.getLocale() == null)
            || !this.locale.equals(thatObj.getLocale()) ) {
            return false;
        }
        if( (this.localeAlternate == null && thatObj.getLocaleAlternate() != null)
            || (this.localeAlternate != null && thatObj.getLocaleAlternate() == null)
            || !this.localeAlternate.equals(thatObj.getLocaleAlternate()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = url == null ? 0 : url.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteName == null ? 0 : siteName.hashCode();
        _hash = 31 * _hash + delta;
        delta = title == null ? 0 : title.hashCode();
        _hash = 31 * _hash + delta;
        delta = description == null ? 0 : description.hashCode();
        _hash = 31 * _hash + delta;
        delta = fbAdmins == null ? 0 : fbAdmins.hashCode();
        _hash = 31 * _hash + delta;
        delta = fbAppId == null ? 0 : fbAppId.hashCode();
        _hash = 31 * _hash + delta;
        delta = image == null ? 0 : image.hashCode();
        _hash = 31 * _hash + delta;
        delta = audio == null ? 0 : audio.hashCode();
        _hash = 31 * _hash + delta;
        delta = video == null ? 0 : video.hashCode();
        _hash = 31 * _hash + delta;
        delta = locale == null ? 0 : locale.hashCode();
        _hash = 31 * _hash + delta;
        delta = localeAlternate == null ? 0 : localeAlternate.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
