package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageBase;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class PageBaseDataObject extends KeyedDataObject implements PageBase
{
    private static final Logger log = Logger.getLogger(PageBaseDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(PageBaseDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(PageBaseDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String user;

    @Persistent(defaultFetchGroup = "true")
    private String fetchRequest;

    @Persistent(defaultFetchGroup = "true")
    private String targetUrl;

    @Persistent(defaultFetchGroup = "true")
    private String pageUrl;

    @Persistent(defaultFetchGroup = "true")
    private String queryString;

    @Persistent(defaultFetchGroup = "false")
    private List<KeyValuePairStructDataObject> queryParams;

    @Persistent(defaultFetchGroup = "true")
    private String lastFetchResult;

    @Persistent(defaultFetchGroup = "true")
    private Integer responseCode;

    @Persistent(defaultFetchGroup = "true")
    private String contentType;

    @Persistent(defaultFetchGroup = "true")
    private Integer contentLength;

    @Persistent(defaultFetchGroup = "true")
    private String language;

    @Persistent(defaultFetchGroup = "true")
    private String redirect;

    @Persistent(defaultFetchGroup = "true")
    private String location;

    @Persistent(defaultFetchGroup = "true")
    private String pageTitle;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private Boolean deferred;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Integer refreshStatus;

    @Persistent(defaultFetchGroup = "true")
    private Long refreshInterval;

    @Persistent(defaultFetchGroup = "true")
    private Long nextRefreshTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastCheckedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastUpdatedTime;

    public PageBaseDataObject()
    {
        this(null);
    }
    public PageBaseDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PageBaseDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public PageBaseDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.user = user;
        this.fetchRequest = fetchRequest;
        this.targetUrl = targetUrl;
        this.pageUrl = pageUrl;
        this.queryString = queryString;
        setQueryParams(queryParams);
        this.lastFetchResult = lastFetchResult;
        this.responseCode = responseCode;
        this.contentType = contentType;
        this.contentLength = contentLength;
        this.language = language;
        this.redirect = redirect;
        this.location = location;
        this.pageTitle = pageTitle;
        this.note = note;
        this.deferred = deferred;
        this.status = status;
        this.refreshStatus = refreshStatus;
        this.refreshInterval = refreshInterval;
        this.nextRefreshTime = nextRefreshTime;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return PageBaseDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return PageBaseDataObject.composeKey(getGuid());
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    public String getTargetUrl()
    {
        return this.targetUrl;
    }
    public void setTargetUrl(String targetUrl)
    {
        this.targetUrl = targetUrl;
    }

    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    @SuppressWarnings("unchecked")
    public List<KeyValuePairStruct> getQueryParams()
    {         
        return (List<KeyValuePairStruct>) ((List<?>) this.queryParams);
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        if(queryParams != null) {
            List<KeyValuePairStructDataObject> dataObj = new ArrayList<KeyValuePairStructDataObject>();
            for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                KeyValuePairStructDataObject elem = null;
                if(keyValuePairStruct instanceof KeyValuePairStructDataObject) {
                    elem = (KeyValuePairStructDataObject) keyValuePairStruct;
                } else if(keyValuePairStruct instanceof KeyValuePairStruct) {
                    elem = new KeyValuePairStructDataObject(keyValuePairStruct.getUuid(), keyValuePairStruct.getKey(), keyValuePairStruct.getValue(), keyValuePairStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.queryParams = dataObj;
        } else {
            this.queryParams = null;
        }
    }

    public String getLastFetchResult()
    {
        return this.lastFetchResult;
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        this.lastFetchResult = lastFetchResult;
    }

    public Integer getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(Integer responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getContentType()
    {
        return this.contentType;
    }
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public Integer getContentLength()
    {
        return this.contentLength;
    }
    public void setContentLength(Integer contentLength)
    {
        this.contentLength = contentLength;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getRedirect()
    {
        return this.redirect;
    }
    public void setRedirect(String redirect)
    {
        this.redirect = redirect;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getPageTitle()
    {
        return this.pageTitle;
    }
    public void setPageTitle(String pageTitle)
    {
        this.pageTitle = pageTitle;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getRefreshStatus()
    {
        return this.refreshStatus;
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        this.refreshStatus = refreshStatus;
    }

    public Long getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    public Long getNextRefreshTime()
    {
        return this.nextRefreshTime;
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        this.nextRefreshTime = nextRefreshTime;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("user", this.user);
        dataMap.put("fetchRequest", this.fetchRequest);
        dataMap.put("targetUrl", this.targetUrl);
        dataMap.put("pageUrl", this.pageUrl);
        dataMap.put("queryString", this.queryString);
        dataMap.put("queryParams", this.queryParams);
        dataMap.put("lastFetchResult", this.lastFetchResult);
        dataMap.put("responseCode", this.responseCode);
        dataMap.put("contentType", this.contentType);
        dataMap.put("contentLength", this.contentLength);
        dataMap.put("language", this.language);
        dataMap.put("redirect", this.redirect);
        dataMap.put("location", this.location);
        dataMap.put("pageTitle", this.pageTitle);
        dataMap.put("note", this.note);
        dataMap.put("deferred", this.deferred);
        dataMap.put("status", this.status);
        dataMap.put("refreshStatus", this.refreshStatus);
        dataMap.put("refreshInterval", this.refreshInterval);
        dataMap.put("nextRefreshTime", this.nextRefreshTime);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        PageBase thatObj = (PageBase) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.user == null && thatObj.getUser() != null)
            || (this.user != null && thatObj.getUser() == null)
            || !this.user.equals(thatObj.getUser()) ) {
            return false;
        }
        if( (this.fetchRequest == null && thatObj.getFetchRequest() != null)
            || (this.fetchRequest != null && thatObj.getFetchRequest() == null)
            || !this.fetchRequest.equals(thatObj.getFetchRequest()) ) {
            return false;
        }
        if( (this.targetUrl == null && thatObj.getTargetUrl() != null)
            || (this.targetUrl != null && thatObj.getTargetUrl() == null)
            || !this.targetUrl.equals(thatObj.getTargetUrl()) ) {
            return false;
        }
        if( (this.pageUrl == null && thatObj.getPageUrl() != null)
            || (this.pageUrl != null && thatObj.getPageUrl() == null)
            || !this.pageUrl.equals(thatObj.getPageUrl()) ) {
            return false;
        }
        if( (this.queryString == null && thatObj.getQueryString() != null)
            || (this.queryString != null && thatObj.getQueryString() == null)
            || !this.queryString.equals(thatObj.getQueryString()) ) {
            return false;
        }
        if( (this.queryParams == null && thatObj.getQueryParams() != null)
            || (this.queryParams != null && thatObj.getQueryParams() == null)
            || !this.queryParams.equals(thatObj.getQueryParams()) ) {
            return false;
        }
        if( (this.lastFetchResult == null && thatObj.getLastFetchResult() != null)
            || (this.lastFetchResult != null && thatObj.getLastFetchResult() == null)
            || !this.lastFetchResult.equals(thatObj.getLastFetchResult()) ) {
            return false;
        }
        if( (this.responseCode == null && thatObj.getResponseCode() != null)
            || (this.responseCode != null && thatObj.getResponseCode() == null)
            || !this.responseCode.equals(thatObj.getResponseCode()) ) {
            return false;
        }
        if( (this.contentType == null && thatObj.getContentType() != null)
            || (this.contentType != null && thatObj.getContentType() == null)
            || !this.contentType.equals(thatObj.getContentType()) ) {
            return false;
        }
        if( (this.contentLength == null && thatObj.getContentLength() != null)
            || (this.contentLength != null && thatObj.getContentLength() == null)
            || !this.contentLength.equals(thatObj.getContentLength()) ) {
            return false;
        }
        if( (this.language == null && thatObj.getLanguage() != null)
            || (this.language != null && thatObj.getLanguage() == null)
            || !this.language.equals(thatObj.getLanguage()) ) {
            return false;
        }
        if( (this.redirect == null && thatObj.getRedirect() != null)
            || (this.redirect != null && thatObj.getRedirect() == null)
            || !this.redirect.equals(thatObj.getRedirect()) ) {
            return false;
        }
        if( (this.location == null && thatObj.getLocation() != null)
            || (this.location != null && thatObj.getLocation() == null)
            || !this.location.equals(thatObj.getLocation()) ) {
            return false;
        }
        if( (this.pageTitle == null && thatObj.getPageTitle() != null)
            || (this.pageTitle != null && thatObj.getPageTitle() == null)
            || !this.pageTitle.equals(thatObj.getPageTitle()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.deferred == null && thatObj.isDeferred() != null)
            || (this.deferred != null && thatObj.isDeferred() == null)
            || !this.deferred.equals(thatObj.isDeferred()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.refreshStatus == null && thatObj.getRefreshStatus() != null)
            || (this.refreshStatus != null && thatObj.getRefreshStatus() == null)
            || !this.refreshStatus.equals(thatObj.getRefreshStatus()) ) {
            return false;
        }
        if( (this.refreshInterval == null && thatObj.getRefreshInterval() != null)
            || (this.refreshInterval != null && thatObj.getRefreshInterval() == null)
            || !this.refreshInterval.equals(thatObj.getRefreshInterval()) ) {
            return false;
        }
        if( (this.nextRefreshTime == null && thatObj.getNextRefreshTime() != null)
            || (this.nextRefreshTime != null && thatObj.getNextRefreshTime() == null)
            || !this.nextRefreshTime.equals(thatObj.getNextRefreshTime()) ) {
            return false;
        }
        if( (this.lastCheckedTime == null && thatObj.getLastCheckedTime() != null)
            || (this.lastCheckedTime != null && thatObj.getLastCheckedTime() == null)
            || !this.lastCheckedTime.equals(thatObj.getLastCheckedTime()) ) {
            return false;
        }
        if( (this.lastUpdatedTime == null && thatObj.getLastUpdatedTime() != null)
            || (this.lastUpdatedTime != null && thatObj.getLastUpdatedTime() == null)
            || !this.lastUpdatedTime.equals(thatObj.getLastUpdatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
        _hash = 31 * _hash + delta;
        delta = targetUrl == null ? 0 : targetUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageUrl == null ? 0 : pageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryString == null ? 0 : queryString.hashCode();
        _hash = 31 * _hash + delta;
        delta = queryParams == null ? 0 : queryParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastFetchResult == null ? 0 : lastFetchResult.hashCode();
        _hash = 31 * _hash + delta;
        delta = responseCode == null ? 0 : responseCode.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentType == null ? 0 : contentType.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentLength == null ? 0 : contentLength.hashCode();
        _hash = 31 * _hash + delta;
        delta = language == null ? 0 : language.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirect == null ? 0 : redirect.hashCode();
        _hash = 31 * _hash + delta;
        delta = location == null ? 0 : location.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageTitle == null ? 0 : pageTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = deferred == null ? 0 : deferred.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshStatus == null ? 0 : refreshStatus.hashCode();
        _hash = 31 * _hash + delta;
        delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
        _hash = 31 * _hash + delta;
        delta = nextRefreshTime == null ? 0 : nextRefreshTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
