package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class ImageSetDataObject extends PageBaseDataObject implements ImageSet
{
    private static final Logger log = Logger.getLogger(ImageSetDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(ImageSetDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(ImageSetDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> mediaTypeFilter;

    @Persistent(defaultFetchGroup = "false")
    private Set<ImageStructDataObject> pageImages;

    public ImageSetDataObject()
    {
        this(null);
    }
    public ImageSetDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public ImageSetDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages, null, null);
    }
    public ImageSetDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.mediaTypeFilter = mediaTypeFilter;  // ???
        setPageImages(pageImages);
    }

//    @Override
//    protected Key createKey()
//    {
//        return ImageSetDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return ImageSetDataObject.composeKey(getGuid());
    }

    public List<String> getMediaTypeFilter()
    {
        return this.mediaTypeFilter;   // ???
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        this.mediaTypeFilter = mediaTypeFilter;
    }

    @SuppressWarnings("unchecked")
    public Set<ImageStruct> getPageImages()
    {         
        return (Set<ImageStruct>) ((Set<?>) this.pageImages);
    }
    public void setPageImages(Set<ImageStruct> pageImages)
    {
        if(pageImages != null) {
            Set<ImageStructDataObject> dataObj = new HashSet<ImageStructDataObject>();
            for(ImageStruct imageStruct : pageImages) {
                ImageStructDataObject elem = null;
                if(imageStruct instanceof ImageStructDataObject) {
                    elem = (ImageStructDataObject) imageStruct;
                } else if(imageStruct instanceof ImageStruct) {
                    elem = new ImageStructDataObject(imageStruct.getUuid(), imageStruct.getId(), imageStruct.getAlt(), imageStruct.getSrc(), imageStruct.getSrcUrl(), imageStruct.getMediaType(), imageStruct.getWidthAttr(), imageStruct.getWidth(), imageStruct.getHeightAttr(), imageStruct.getHeight(), imageStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.pageImages = dataObj;
        } else {
            this.pageImages = null;
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("mediaTypeFilter", this.mediaTypeFilter);
        dataMap.put("pageImages", this.pageImages);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ImageSet thatObj = (ImageSet) obj;
        if( (this.mediaTypeFilter == null && thatObj.getMediaTypeFilter() != null)
            || (this.mediaTypeFilter != null && thatObj.getMediaTypeFilter() == null)
            || !this.mediaTypeFilter.equals(thatObj.getMediaTypeFilter()) ) {
            return false;
        }
        if( (this.pageImages == null && thatObj.getPageImages() != null)
            || (this.pageImages != null && thatObj.getPageImages() == null)
            || !this.pageImages.equals(thatObj.getPageImages()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = mediaTypeFilter == null ? 0 : mediaTypeFilter.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageImages == null ? 0 : pageImages.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
