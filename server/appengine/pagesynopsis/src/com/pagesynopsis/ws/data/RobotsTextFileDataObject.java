package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class RobotsTextFileDataObject extends KeyedDataObject implements RobotsTextFile
{
    private static final Logger log = Logger.getLogger(RobotsTextFileDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(RobotsTextFileDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(RobotsTextFileDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String siteUrl;

    @Persistent(defaultFetchGroup = "false")
    private List<RobotsTextGroupDataObject> groups;

    @Persistent(defaultFetchGroup = "false")
    private List<String> sitemaps;

    @Persistent(defaultFetchGroup = "false")
    private Text content;

    @Persistent(defaultFetchGroup = "true")
    private String contentHash;

    @Persistent(defaultFetchGroup = "true")
    private Long lastCheckedTime;

    @Persistent(defaultFetchGroup = "true")
    private Long lastUpdatedTime;

    public RobotsTextFileDataObject()
    {
        this(null);
    }
    public RobotsTextFileDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public RobotsTextFileDataObject(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public RobotsTextFileDataObject(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.siteUrl = siteUrl;
        setGroups(groups);
        this.sitemaps = sitemaps;  // ???
        setContent(content);
        this.contentHash = contentHash;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return RobotsTextFileDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return RobotsTextFileDataObject.composeKey(getGuid());
    }

    public String getSiteUrl()
    {
        return this.siteUrl;
    }
    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    @SuppressWarnings("unchecked")
    public List<RobotsTextGroup> getGroups()
    {         
        return (List<RobotsTextGroup>) ((List<?>) this.groups);
    }
    public void setGroups(List<RobotsTextGroup> groups)
    {
        if(groups != null) {
            List<RobotsTextGroupDataObject> dataObj = new ArrayList<RobotsTextGroupDataObject>();
            for(RobotsTextGroup robotsTextGroup : groups) {
                RobotsTextGroupDataObject elem = null;
                if(robotsTextGroup instanceof RobotsTextGroupDataObject) {
                    elem = (RobotsTextGroupDataObject) robotsTextGroup;
                } else if(robotsTextGroup instanceof RobotsTextGroup) {
                    elem = new RobotsTextGroupDataObject(robotsTextGroup.getUuid(), robotsTextGroup.getUserAgent(), robotsTextGroup.getAllows(), robotsTextGroup.getDisallows());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.groups = dataObj;
        } else {
            this.groups = null;
        }
    }

    public List<String> getSitemaps()
    {
        return this.sitemaps;   // ???
    }
    public void setSitemaps(List<String> sitemaps)
    {
        this.sitemaps = sitemaps;
    }

    public String getContent()
    {
        if(this.content == null) {
            return null;
        }    
        return this.content.getValue();
    }
    public void setContent(String content)
    {
        if(content == null) {
            this.content = null;
        } else {
            this.content = new Text(content);
        }
    }

    public String getContentHash()
    {
        return this.contentHash;
    }
    public void setContentHash(String contentHash)
    {
        this.contentHash = contentHash;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("siteUrl", this.siteUrl);
        dataMap.put("groups", this.groups);
        dataMap.put("sitemaps", this.sitemaps);
        dataMap.put("content", this.content);
        dataMap.put("contentHash", this.contentHash);
        dataMap.put("lastCheckedTime", this.lastCheckedTime);
        dataMap.put("lastUpdatedTime", this.lastUpdatedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        RobotsTextFile thatObj = (RobotsTextFile) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.siteUrl == null && thatObj.getSiteUrl() != null)
            || (this.siteUrl != null && thatObj.getSiteUrl() == null)
            || !this.siteUrl.equals(thatObj.getSiteUrl()) ) {
            return false;
        }
        if( (this.groups == null && thatObj.getGroups() != null)
            || (this.groups != null && thatObj.getGroups() == null)
            || !this.groups.equals(thatObj.getGroups()) ) {
            return false;
        }
        if( (this.sitemaps == null && thatObj.getSitemaps() != null)
            || (this.sitemaps != null && thatObj.getSitemaps() == null)
            || !this.sitemaps.equals(thatObj.getSitemaps()) ) {
            return false;
        }
        if( (this.content == null && thatObj.getContent() != null)
            || (this.content != null && thatObj.getContent() == null)
            || !this.content.equals(thatObj.getContent()) ) {
            return false;
        }
        if( (this.contentHash == null && thatObj.getContentHash() != null)
            || (this.contentHash != null && thatObj.getContentHash() == null)
            || !this.contentHash.equals(thatObj.getContentHash()) ) {
            return false;
        }
        if( (this.lastCheckedTime == null && thatObj.getLastCheckedTime() != null)
            || (this.lastCheckedTime != null && thatObj.getLastCheckedTime() == null)
            || !this.lastCheckedTime.equals(thatObj.getLastCheckedTime()) ) {
            return false;
        }
        if( (this.lastUpdatedTime == null && thatObj.getLastUpdatedTime() != null)
            || (this.lastUpdatedTime != null && thatObj.getLastUpdatedTime() == null)
            || !this.lastUpdatedTime.equals(thatObj.getLastUpdatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = siteUrl == null ? 0 : siteUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = groups == null ? 0 : groups.hashCode();
        _hash = 31 * _hash + delta;
        delta = sitemaps == null ? 0 : sitemaps.hashCode();
        _hash = 31 * _hash + delta;
        delta = content == null ? 0 : content.hashCode();
        _hash = 31 * _hash + delta;
        delta = contentHash == null ? 0 : contentHash.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
