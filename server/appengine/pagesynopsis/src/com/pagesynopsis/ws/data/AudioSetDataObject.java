package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class AudioSetDataObject extends PageBaseDataObject implements AudioSet
{
    private static final Logger log = Logger.getLogger(AudioSetDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(AudioSetDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(AudioSetDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> mediaTypeFilter;

    @Persistent(defaultFetchGroup = "false")
    private Set<AudioStructDataObject> pageAudios;

    public AudioSetDataObject()
    {
        this(null);
    }
    public AudioSetDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AudioSetDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios, null, null);
    }
    public AudioSetDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.mediaTypeFilter = mediaTypeFilter;  // ???
        setPageAudios(pageAudios);
    }

//    @Override
//    protected Key createKey()
//    {
//        return AudioSetDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return AudioSetDataObject.composeKey(getGuid());
    }

    public List<String> getMediaTypeFilter()
    {
        return this.mediaTypeFilter;   // ???
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        this.mediaTypeFilter = mediaTypeFilter;
    }

    @SuppressWarnings("unchecked")
    public Set<AudioStruct> getPageAudios()
    {         
        return (Set<AudioStruct>) ((Set<?>) this.pageAudios);
    }
    public void setPageAudios(Set<AudioStruct> pageAudios)
    {
        if(pageAudios != null) {
            Set<AudioStructDataObject> dataObj = new HashSet<AudioStructDataObject>();
            for(AudioStruct audioStruct : pageAudios) {
                AudioStructDataObject elem = null;
                if(audioStruct instanceof AudioStructDataObject) {
                    elem = (AudioStructDataObject) audioStruct;
                } else if(audioStruct instanceof AudioStruct) {
                    elem = new AudioStructDataObject(audioStruct.getUuid(), audioStruct.getId(), audioStruct.getControls(), audioStruct.isAutoplayEnabled(), audioStruct.isLoopEnabled(), audioStruct.isPreloadEnabled(), audioStruct.getRemark(), audioStruct.getSource(), audioStruct.getSource1(), audioStruct.getSource2(), audioStruct.getSource3(), audioStruct.getSource4(), audioStruct.getSource5(), audioStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.pageAudios = dataObj;
        } else {
            this.pageAudios = null;
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("mediaTypeFilter", this.mediaTypeFilter);
        dataMap.put("pageAudios", this.pageAudios);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AudioSet thatObj = (AudioSet) obj;
        if( (this.mediaTypeFilter == null && thatObj.getMediaTypeFilter() != null)
            || (this.mediaTypeFilter != null && thatObj.getMediaTypeFilter() == null)
            || !this.mediaTypeFilter.equals(thatObj.getMediaTypeFilter()) ) {
            return false;
        }
        if( (this.pageAudios == null && thatObj.getPageAudios() != null)
            || (this.pageAudios != null && thatObj.getPageAudios() == null)
            || !this.pageAudios.equals(thatObj.getPageAudios()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = mediaTypeFilter == null ? 0 : mediaTypeFilter.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageAudios == null ? 0 : pageAudios.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
