package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class OgArticleDataObject extends OgObjectBaseDataObject implements OgArticle
{
    private static final Logger log = Logger.getLogger(OgArticleDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(OgArticleDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(OgArticleDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> author;

    @Persistent(defaultFetchGroup = "true")
    private String section;

    @Persistent(defaultFetchGroup = "false")
    private List<String> tag;

    @Persistent(defaultFetchGroup = "true")
    private String publishedDate;

    @Persistent(defaultFetchGroup = "true")
    private String modifiedDate;

    @Persistent(defaultFetchGroup = "true")
    private String expirationDate;

    public OgArticleDataObject()
    {
        this(null);
    }
    public OgArticleDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgArticleDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate, null, null);
    }
    public OgArticleDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);
        this.author = author;  // ???
        this.section = section;
        this.tag = tag;  // ???
        this.publishedDate = publishedDate;
        this.modifiedDate = modifiedDate;
        this.expirationDate = expirationDate;
    }

//    @Override
//    protected Key createKey()
//    {
//        return OgArticleDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return OgArticleDataObject.composeKey(getGuid());
    }

    public List<String> getAuthor()
    {
        return this.author;   // ???
    }
    public void setAuthor(List<String> author)
    {
        this.author = author;
    }

    public String getSection()
    {
        return this.section;
    }
    public void setSection(String section)
    {
        this.section = section;
    }

    public List<String> getTag()
    {
        return this.tag;   // ???
    }
    public void setTag(List<String> tag)
    {
        this.tag = tag;
    }

    public String getPublishedDate()
    {
        return this.publishedDate;
    }
    public void setPublishedDate(String publishedDate)
    {
        this.publishedDate = publishedDate;
    }

    public String getModifiedDate()
    {
        return this.modifiedDate;
    }
    public void setModifiedDate(String modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    public String getExpirationDate()
    {
        return this.expirationDate;
    }
    public void setExpirationDate(String expirationDate)
    {
        this.expirationDate = expirationDate;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("author", this.author);
        dataMap.put("section", this.section);
        dataMap.put("tag", this.tag);
        dataMap.put("publishedDate", this.publishedDate);
        dataMap.put("modifiedDate", this.modifiedDate);
        dataMap.put("expirationDate", this.expirationDate);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OgArticle thatObj = (OgArticle) obj;
        if( (this.author == null && thatObj.getAuthor() != null)
            || (this.author != null && thatObj.getAuthor() == null)
            || !this.author.equals(thatObj.getAuthor()) ) {
            return false;
        }
        if( (this.section == null && thatObj.getSection() != null)
            || (this.section != null && thatObj.getSection() == null)
            || !this.section.equals(thatObj.getSection()) ) {
            return false;
        }
        if( (this.tag == null && thatObj.getTag() != null)
            || (this.tag != null && thatObj.getTag() == null)
            || !this.tag.equals(thatObj.getTag()) ) {
            return false;
        }
        if( (this.publishedDate == null && thatObj.getPublishedDate() != null)
            || (this.publishedDate != null && thatObj.getPublishedDate() == null)
            || !this.publishedDate.equals(thatObj.getPublishedDate()) ) {
            return false;
        }
        if( (this.modifiedDate == null && thatObj.getModifiedDate() != null)
            || (this.modifiedDate != null && thatObj.getModifiedDate() == null)
            || !this.modifiedDate.equals(thatObj.getModifiedDate()) ) {
            return false;
        }
        if( (this.expirationDate == null && thatObj.getExpirationDate() != null)
            || (this.expirationDate != null && thatObj.getExpirationDate() == null)
            || !this.expirationDate.equals(thatObj.getExpirationDate()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = author == null ? 0 : author.hashCode();
        _hash = 31 * _hash + delta;
        delta = section == null ? 0 : section.hashCode();
        _hash = 31 * _hash + delta;
        delta = tag == null ? 0 : tag.hashCode();
        _hash = 31 * _hash + delta;
        delta = publishedDate == null ? 0 : publishedDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = modifiedDate == null ? 0 : modifiedDate.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationDate == null ? 0 : expirationDate.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
