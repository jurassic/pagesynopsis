package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class AnchorStructDataObject implements AnchorStruct, Serializable
{
    private static final Logger log = Logger.getLogger(AnchorStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _anchorstruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String id;

    @Persistent(defaultFetchGroup = "true")
    private String name;

    @Persistent(defaultFetchGroup = "true")
    private String href;

    @Persistent(defaultFetchGroup = "true")
    private String hrefUrl;

    @Persistent(defaultFetchGroup = "true")
    private String urlScheme;

    @Persistent(defaultFetchGroup = "true")
    private String rel;

    @Persistent(defaultFetchGroup = "true")
    private String target;

    @Persistent(defaultFetchGroup = "true")
    private String innerHtml;

    @Persistent(defaultFetchGroup = "true")
    private String anchorText;

    @Persistent(defaultFetchGroup = "true")
    private String anchorTitle;

    @Persistent(defaultFetchGroup = "true")
    private String anchorImage;

    @Persistent(defaultFetchGroup = "true")
    private String anchorLegend;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public AnchorStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public AnchorStructDataObject(String uuid, String id, String name, String href, String hrefUrl, String urlScheme, String rel, String target, String innerHtml, String anchorText, String anchorTitle, String anchorImage, String anchorLegend, String note)
    {
        setUuid(uuid);
        setId(id);
        setName(name);
        setHref(href);
        setHrefUrl(hrefUrl);
        setUrlScheme(urlScheme);
        setRel(rel);
        setTarget(target);
        setInnerHtml(innerHtml);
        setAnchorText(anchorText);
        setAnchorTitle(anchorTitle);
        setAnchorImage(anchorImage);
        setAnchorLegend(anchorLegend);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_anchorstruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _anchorstruct_encoded_key = KeyFactory.createKeyString(AnchorStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _anchorstruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _anchorstruct_encoded_key = KeyFactory.createKeyString(AnchorStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
        if(this.id != null) {
            resetEncodedKey();
        }
    }

    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
        if(this.name != null) {
            resetEncodedKey();
        }
    }

    public String getHref()
    {
        return this.href;
    }
    public void setHref(String href)
    {
        this.href = href;
        if(this.href != null) {
            resetEncodedKey();
        }
    }

    public String getHrefUrl()
    {
        return this.hrefUrl;
    }
    public void setHrefUrl(String hrefUrl)
    {
        this.hrefUrl = hrefUrl;
        if(this.hrefUrl != null) {
            resetEncodedKey();
        }
    }

    public String getUrlScheme()
    {
        return this.urlScheme;
    }
    public void setUrlScheme(String urlScheme)
    {
        this.urlScheme = urlScheme;
        if(this.urlScheme != null) {
            resetEncodedKey();
        }
    }

    public String getRel()
    {
        return this.rel;
    }
    public void setRel(String rel)
    {
        this.rel = rel;
        if(this.rel != null) {
            resetEncodedKey();
        }
    }

    public String getTarget()
    {
        return this.target;
    }
    public void setTarget(String target)
    {
        this.target = target;
        if(this.target != null) {
            resetEncodedKey();
        }
    }

    public String getInnerHtml()
    {
        return this.innerHtml;
    }
    public void setInnerHtml(String innerHtml)
    {
        this.innerHtml = innerHtml;
        if(this.innerHtml != null) {
            resetEncodedKey();
        }
    }

    public String getAnchorText()
    {
        return this.anchorText;
    }
    public void setAnchorText(String anchorText)
    {
        this.anchorText = anchorText;
        if(this.anchorText != null) {
            resetEncodedKey();
        }
    }

    public String getAnchorTitle()
    {
        return this.anchorTitle;
    }
    public void setAnchorTitle(String anchorTitle)
    {
        this.anchorTitle = anchorTitle;
        if(this.anchorTitle != null) {
            resetEncodedKey();
        }
    }

    public String getAnchorImage()
    {
        return this.anchorImage;
    }
    public void setAnchorImage(String anchorImage)
    {
        this.anchorImage = anchorImage;
        if(this.anchorImage != null) {
            resetEncodedKey();
        }
    }

    public String getAnchorLegend()
    {
        return this.anchorLegend;
    }
    public void setAnchorLegend(String anchorLegend)
    {
        this.anchorLegend = anchorLegend;
        if(this.anchorLegend != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHrefUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrlScheme() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTarget() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getInnerHtml() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorLegend() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("id", this.id);
        dataMap.put("name", this.name);
        dataMap.put("href", this.href);
        dataMap.put("hrefUrl", this.hrefUrl);
        dataMap.put("urlScheme", this.urlScheme);
        dataMap.put("rel", this.rel);
        dataMap.put("target", this.target);
        dataMap.put("innerHtml", this.innerHtml);
        dataMap.put("anchorText", this.anchorText);
        dataMap.put("anchorTitle", this.anchorTitle);
        dataMap.put("anchorImage", this.anchorImage);
        dataMap.put("anchorLegend", this.anchorLegend);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        AnchorStruct thatObj = (AnchorStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.id == null && thatObj.getId() != null)
            || (this.id != null && thatObj.getId() == null)
            || !this.id.equals(thatObj.getId()) ) {
            return false;
        }
        if( (this.name == null && thatObj.getName() != null)
            || (this.name != null && thatObj.getName() == null)
            || !this.name.equals(thatObj.getName()) ) {
            return false;
        }
        if( (this.href == null && thatObj.getHref() != null)
            || (this.href != null && thatObj.getHref() == null)
            || !this.href.equals(thatObj.getHref()) ) {
            return false;
        }
        if( (this.hrefUrl == null && thatObj.getHrefUrl() != null)
            || (this.hrefUrl != null && thatObj.getHrefUrl() == null)
            || !this.hrefUrl.equals(thatObj.getHrefUrl()) ) {
            return false;
        }
        if( (this.urlScheme == null && thatObj.getUrlScheme() != null)
            || (this.urlScheme != null && thatObj.getUrlScheme() == null)
            || !this.urlScheme.equals(thatObj.getUrlScheme()) ) {
            return false;
        }
        if( (this.rel == null && thatObj.getRel() != null)
            || (this.rel != null && thatObj.getRel() == null)
            || !this.rel.equals(thatObj.getRel()) ) {
            return false;
        }
        if( (this.target == null && thatObj.getTarget() != null)
            || (this.target != null && thatObj.getTarget() == null)
            || !this.target.equals(thatObj.getTarget()) ) {
            return false;
        }
        if( (this.innerHtml == null && thatObj.getInnerHtml() != null)
            || (this.innerHtml != null && thatObj.getInnerHtml() == null)
            || !this.innerHtml.equals(thatObj.getInnerHtml()) ) {
            return false;
        }
        if( (this.anchorText == null && thatObj.getAnchorText() != null)
            || (this.anchorText != null && thatObj.getAnchorText() == null)
            || !this.anchorText.equals(thatObj.getAnchorText()) ) {
            return false;
        }
        if( (this.anchorTitle == null && thatObj.getAnchorTitle() != null)
            || (this.anchorTitle != null && thatObj.getAnchorTitle() == null)
            || !this.anchorTitle.equals(thatObj.getAnchorTitle()) ) {
            return false;
        }
        if( (this.anchorImage == null && thatObj.getAnchorImage() != null)
            || (this.anchorImage != null && thatObj.getAnchorImage() == null)
            || !this.anchorImage.equals(thatObj.getAnchorImage()) ) {
            return false;
        }
        if( (this.anchorLegend == null && thatObj.getAnchorLegend() != null)
            || (this.anchorLegend != null && thatObj.getAnchorLegend() == null)
            || !this.anchorLegend.equals(thatObj.getAnchorLegend()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = name == null ? 0 : name.hashCode();
        _hash = 31 * _hash + delta;
        delta = href == null ? 0 : href.hashCode();
        _hash = 31 * _hash + delta;
        delta = hrefUrl == null ? 0 : hrefUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = urlScheme == null ? 0 : urlScheme.hashCode();
        _hash = 31 * _hash + delta;
        delta = rel == null ? 0 : rel.hashCode();
        _hash = 31 * _hash + delta;
        delta = target == null ? 0 : target.hashCode();
        _hash = 31 * _hash + delta;
        delta = innerHtml == null ? 0 : innerHtml.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorText == null ? 0 : anchorText.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorTitle == null ? 0 : anchorTitle.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorImage == null ? 0 : anchorImage.hashCode();
        _hash = 31 * _hash + delta;
        delta = anchorLegend == null ? 0 : anchorLegend.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
