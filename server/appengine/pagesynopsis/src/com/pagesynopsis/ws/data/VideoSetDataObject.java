package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class VideoSetDataObject extends PageBaseDataObject implements VideoSet
{
    private static final Logger log = Logger.getLogger(VideoSetDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(VideoSetDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(VideoSetDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "false")
    private List<String> mediaTypeFilter;

    @Persistent(defaultFetchGroup = "false")
    private Set<VideoStructDataObject> pageVideos;

    public VideoSetDataObject()
    {
        this(null);
    }
    public VideoSetDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public VideoSetDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos, null, null);
    }
    public VideoSetDataObject(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);
        this.mediaTypeFilter = mediaTypeFilter;  // ???
        setPageVideos(pageVideos);
    }

//    @Override
//    protected Key createKey()
//    {
//        return VideoSetDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return VideoSetDataObject.composeKey(getGuid());
    }

    public List<String> getMediaTypeFilter()
    {
        return this.mediaTypeFilter;   // ???
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        this.mediaTypeFilter = mediaTypeFilter;
    }

    @SuppressWarnings("unchecked")
    public Set<VideoStruct> getPageVideos()
    {         
        return (Set<VideoStruct>) ((Set<?>) this.pageVideos);
    }
    public void setPageVideos(Set<VideoStruct> pageVideos)
    {
        if(pageVideos != null) {
            Set<VideoStructDataObject> dataObj = new HashSet<VideoStructDataObject>();
            for(VideoStruct videoStruct : pageVideos) {
                VideoStructDataObject elem = null;
                if(videoStruct instanceof VideoStructDataObject) {
                    elem = (VideoStructDataObject) videoStruct;
                } else if(videoStruct instanceof VideoStruct) {
                    elem = new VideoStructDataObject(videoStruct.getUuid(), videoStruct.getId(), videoStruct.getWidth(), videoStruct.getHeight(), videoStruct.getControls(), videoStruct.isAutoplayEnabled(), videoStruct.isLoopEnabled(), videoStruct.isPreloadEnabled(), videoStruct.isMuted(), videoStruct.getRemark(), videoStruct.getSource(), videoStruct.getSource1(), videoStruct.getSource2(), videoStruct.getSource3(), videoStruct.getSource4(), videoStruct.getSource5(), videoStruct.getNote());
                }
                if(elem != null) {
                    dataObj.add(elem);
                }
            }
            this.pageVideos = dataObj;
        } else {
            this.pageVideos = null;
        }
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("mediaTypeFilter", this.mediaTypeFilter);
        dataMap.put("pageVideos", this.pageVideos);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        VideoSet thatObj = (VideoSet) obj;
        if( (this.mediaTypeFilter == null && thatObj.getMediaTypeFilter() != null)
            || (this.mediaTypeFilter != null && thatObj.getMediaTypeFilter() == null)
            || !this.mediaTypeFilter.equals(thatObj.getMediaTypeFilter()) ) {
            return false;
        }
        if( (this.pageVideos == null && thatObj.getPageVideos() != null)
            || (this.pageVideos != null && thatObj.getPageVideos() == null)
            || !this.pageVideos.equals(thatObj.getPageVideos()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = mediaTypeFilter == null ? 0 : mediaTypeFilter.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageVideos == null ? 0 : pageVideos.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
