package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
public class UrlRatingDataObject extends KeyedDataObject implements UrlRating
{
    private static final Logger log = Logger.getLogger(UrlRatingDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(UrlRatingDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(UrlRatingDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String guid;

    @Persistent(defaultFetchGroup = "true")
    private String domain;

    @Persistent(defaultFetchGroup = "true")
    private String pageUrl;

    @Persistent(defaultFetchGroup = "true")
    private String preview;

    @Persistent(defaultFetchGroup = "true")
    private String flag;

    @Persistent(defaultFetchGroup = "true")
    private Double rating;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    @Persistent(defaultFetchGroup = "true")
    private String status;

    @Persistent(defaultFetchGroup = "true")
    private Long ratedTime;

    public UrlRatingDataObject()
    {
        this(null);
    }
    public UrlRatingDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public UrlRatingDataObject(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime)
    {
        this(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime, null, null);
    }
    public UrlRatingDataObject(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime, Long createdTime, Long modifiedTime)
    {
        super(createdTime, modifiedTime);
        generatePK(guid);
        this.domain = domain;
        this.pageUrl = pageUrl;
        this.preview = preview;
        this.flag = flag;
        this.rating = rating;
        this.note = note;
        this.status = status;
        this.ratedTime = ratedTime;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        generatePK(guid);
    }
    protected void generatePK(String guid)
    {
        if(guid == null) {
            this.guid = GUID.generate();  // temporary
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to a new guid = " + this.guid);
        } else {
            this.guid = guid;
            if(log.isLoggable(Level.FINE)) log.fine("Guid has been set to guid = " + this.guid);
        }
        rebuildKey();        
    }

//    @Override
//    protected Key createKey()
//    {
//        return UrlRatingDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return UrlRatingDataObject.composeKey(getGuid());
    }

    public String getDomain()
    {
        return this.domain;
    }
    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    public String getPreview()
    {
        return this.preview;
    }
    public void setPreview(String preview)
    {
        this.preview = preview;
    }

    public String getFlag()
    {
        return this.flag;
    }
    public void setFlag(String flag)
    {
        this.flag = flag;
    }

    public Double getRating()
    {
        return this.rating;
    }
    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Long getRatedTime()
    {
        return this.ratedTime;
    }
    public void setRatedTime(Long ratedTime)
    {
        this.ratedTime = ratedTime;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("guid", this.guid);
        dataMap.put("domain", this.domain);
        dataMap.put("pageUrl", this.pageUrl);
        dataMap.put("preview", this.preview);
        dataMap.put("flag", this.flag);
        dataMap.put("rating", this.rating);
        dataMap.put("note", this.note);
        dataMap.put("status", this.status);
        dataMap.put("ratedTime", this.ratedTime);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        UrlRating thatObj = (UrlRating) obj;
        if( (this.guid == null && thatObj.getGuid() != null)
            || (this.guid != null && thatObj.getGuid() == null)
            || !this.guid.equals(thatObj.getGuid()) ) {
            return false;
        }
        if( (this.domain == null && thatObj.getDomain() != null)
            || (this.domain != null && thatObj.getDomain() == null)
            || !this.domain.equals(thatObj.getDomain()) ) {
            return false;
        }
        if( (this.pageUrl == null && thatObj.getPageUrl() != null)
            || (this.pageUrl != null && thatObj.getPageUrl() == null)
            || !this.pageUrl.equals(thatObj.getPageUrl()) ) {
            return false;
        }
        if( (this.preview == null && thatObj.getPreview() != null)
            || (this.preview != null && thatObj.getPreview() == null)
            || !this.preview.equals(thatObj.getPreview()) ) {
            return false;
        }
        if( (this.flag == null && thatObj.getFlag() != null)
            || (this.flag != null && thatObj.getFlag() == null)
            || !this.flag.equals(thatObj.getFlag()) ) {
            return false;
        }
        if( (this.rating == null && thatObj.getRating() != null)
            || (this.rating != null && thatObj.getRating() == null)
            || !this.rating.equals(thatObj.getRating()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }
        if( (this.status == null && thatObj.getStatus() != null)
            || (this.status != null && thatObj.getStatus() == null)
            || !this.status.equals(thatObj.getStatus()) ) {
            return false;
        }
        if( (this.ratedTime == null && thatObj.getRatedTime() != null)
            || (this.ratedTime != null && thatObj.getRatedTime() == null)
            || !this.ratedTime.equals(thatObj.getRatedTime()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = guid == null ? 0 : guid.hashCode();
        _hash = 31 * _hash + delta;
        delta = domain == null ? 0 : domain.hashCode();
        _hash = 31 * _hash + delta;
        delta = pageUrl == null ? 0 : pageUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = preview == null ? 0 : preview.hashCode();
        _hash = 31 * _hash + delta;
        delta = flag == null ? 0 : flag.hashCode();
        _hash = 31 * _hash + delta;
        delta = rating == null ? 0 : rating.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        delta = status == null ? 0 : status.hashCode();
        _hash = 31 * _hash + delta;
        delta = ratedTime == null ? 0 : ratedTime.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
