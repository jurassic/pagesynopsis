package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import java.util.List;
import java.util.ArrayList;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
@Inheritance(customStrategy = "complete-table")
public class OgProfileDataObject extends OgObjectBaseDataObject implements OgProfile
{
    private static final Logger log = Logger.getLogger(OgProfileDataObject.class.getName());

//    public static Key composeKey(String guid)
//    {
//        Key key = KeyFactory.createKey(OgProfileDataObject.class.getSimpleName(), guid);
//        return key; 
//    }
    public static String composeKey(String guid)
    {
        String key = KeyFactory.createKeyString(OgProfileDataObject.class.getSimpleName(), guid);
        return key; 
    }


    @Persistent(defaultFetchGroup = "true")
    private String profileId;

    @Persistent(defaultFetchGroup = "true")
    private String firstName;

    @Persistent(defaultFetchGroup = "true")
    private String lastName;

    @Persistent(defaultFetchGroup = "true")
    private String username;

    @Persistent(defaultFetchGroup = "true")
    private String gender;

    public OgProfileDataObject()
    {
        this(null);
    }
    public OgProfileDataObject(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgProfileDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender, null, null);
    }
    public OgProfileDataObject(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);
        this.profileId = profileId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.gender = gender;
    }

//    @Override
//    protected Key createKey()
//    {
//        return OgProfileDataObject.composeKey(getGuid());
//    }
    @Override
    protected String createKey()
    {
        return OgProfileDataObject.composeKey(getGuid());
    }

    public String getProfileId()
    {
        return this.profileId;
    }
    public void setProfileId(String profileId)
    {
        this.profileId = profileId;
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getUsername()
    {
        return this.username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getGender()
    {
        return this.gender;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }


    @Override
    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("profileId", this.profileId);
        dataMap.put("firstName", this.firstName);
        dataMap.put("lastName", this.lastName);
        dataMap.put("username", this.username);
        dataMap.put("gender", this.gender);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        OgProfile thatObj = (OgProfile) obj;
        if( (this.profileId == null && thatObj.getProfileId() != null)
            || (this.profileId != null && thatObj.getProfileId() == null)
            || !this.profileId.equals(thatObj.getProfileId()) ) {
            return false;
        }
        if( (this.firstName == null && thatObj.getFirstName() != null)
            || (this.firstName != null && thatObj.getFirstName() == null)
            || !this.firstName.equals(thatObj.getFirstName()) ) {
            return false;
        }
        if( (this.lastName == null && thatObj.getLastName() != null)
            || (this.lastName != null && thatObj.getLastName() == null)
            || !this.lastName.equals(thatObj.getLastName()) ) {
            return false;
        }
        if( (this.username == null && thatObj.getUsername() != null)
            || (this.username != null && thatObj.getUsername() == null)
            || !this.username.equals(thatObj.getUsername()) ) {
            return false;
        }
        if( (this.gender == null && thatObj.getGender() != null)
            || (this.gender != null && thatObj.getGender() == null)
            || !this.gender.equals(thatObj.getGender()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = profileId == null ? 0 : profileId.hashCode();
        _hash = 31 * _hash + delta;
        delta = firstName == null ? 0 : firstName.hashCode();
        _hash = 31 * _hash + delta;
        delta = lastName == null ? 0 : lastName.hashCode();
        _hash = 31 * _hash + delta;
        delta = username == null ? 0 : username.hashCode();
        _hash = 31 * _hash + delta;
        delta = gender == null ? 0 : gender.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

}
