package com.pagesynopsis.ws.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.jdo.annotations.Column;
import javax.jdo.annotations.Embedded;
import javax.jdo.annotations.EmbeddedOnly;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PrimaryKey;

//import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.util.CommonUtil;
import com.pagesynopsis.ws.core.GUID;


@PersistenceCapable(detachable="true")
// @EmbeddedOnly
public class ImageStructDataObject implements ImageStruct, Serializable
{
    private static final Logger log = Logger.getLogger(ImageStructDataObject.class.getName());

    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
    private String _imagestruct_encoded_key = null;   // Due to GAE limitation, the PK needs to be either Key or encoded string...

    @Persistent(defaultFetchGroup = "true")
    // @Extension(vendorName="datanucleus", key="gae.pk-name", value="true")
    private String uuid;

    @Persistent(defaultFetchGroup = "true")
    private String id;

    @Persistent(defaultFetchGroup = "true")
    private String alt;

    @Persistent(defaultFetchGroup = "true")
    private String src;

    @Persistent(defaultFetchGroup = "true")
    private String srcUrl;

    @Persistent(defaultFetchGroup = "true")
    private String mediaType;

    @Persistent(defaultFetchGroup = "true")
    private String widthAttr;

    @Persistent(defaultFetchGroup = "true")
    private Integer width;

    @Persistent(defaultFetchGroup = "true")
    private String heightAttr;

    @Persistent(defaultFetchGroup = "true")
    private Integer height;

    @Persistent(defaultFetchGroup = "true")
    private String note;

    public ImageStructDataObject()
    {
        // ???
        // this(null, null, null, null, null, null, null, null, null, null, null);
    }
    public ImageStructDataObject(String uuid, String id, String alt, String src, String srcUrl, String mediaType, String widthAttr, Integer width, String heightAttr, Integer height, String note)
    {
        setUuid(uuid);
        setId(id);
        setAlt(alt);
        setSrc(src);
        setSrcUrl(srcUrl);
        setMediaType(mediaType);
        setWidthAttr(widthAttr);
        setWidth(width);
        setHeightAttr(heightAttr);
        setHeight(height);
        setNote(note);
    }

    private void resetEncodedKey()
    {
        if(_imagestruct_encoded_key == null) {
            if(uuid == null || uuid.isEmpty()) {
                // Temporary solution. PK cannot be null.
                uuid = GUID.generate();
            }
            _imagestruct_encoded_key = KeyFactory.createKeyString(ImageStructDataObject.class.getSimpleName(), uuid);
        }
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        if(uuid == null || uuid.isEmpty()) {
            // Temporary solution. PK cannot be null. ???
            // this.uuid = GUID.generate();
            this.uuid = uuid;
            _imagestruct_encoded_key = null;   // ???
        } else {
            this.uuid = uuid;
            _imagestruct_encoded_key = KeyFactory.createKeyString(ImageStructDataObject.class.getSimpleName(), this.uuid);
        }
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
        if(this.id != null) {
            resetEncodedKey();
        }
    }

    public String getAlt()
    {
        return this.alt;
    }
    public void setAlt(String alt)
    {
        this.alt = alt;
        if(this.alt != null) {
            resetEncodedKey();
        }
    }

    public String getSrc()
    {
        return this.src;
    }
    public void setSrc(String src)
    {
        this.src = src;
        if(this.src != null) {
            resetEncodedKey();
        }
    }

    public String getSrcUrl()
    {
        return this.srcUrl;
    }
    public void setSrcUrl(String srcUrl)
    {
        this.srcUrl = srcUrl;
        if(this.srcUrl != null) {
            resetEncodedKey();
        }
    }

    public String getMediaType()
    {
        return this.mediaType;
    }
    public void setMediaType(String mediaType)
    {
        this.mediaType = mediaType;
        if(this.mediaType != null) {
            resetEncodedKey();
        }
    }

    public String getWidthAttr()
    {
        return this.widthAttr;
    }
    public void setWidthAttr(String widthAttr)
    {
        this.widthAttr = widthAttr;
        if(this.widthAttr != null) {
            resetEncodedKey();
        }
    }

    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
        if(this.width != null) {
            resetEncodedKey();
        }
    }

    public String getHeightAttr()
    {
        return this.heightAttr;
    }
    public void setHeightAttr(String heightAttr)
    {
        this.heightAttr = heightAttr;
        if(this.heightAttr != null) {
            resetEncodedKey();
        }
    }

    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
        if(this.height != null) {
            resetEncodedKey();
        }
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
        if(this.note != null) {
            resetEncodedKey();
        }
    }


    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMediaType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidthAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeightAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("id", this.id);
        dataMap.put("alt", this.alt);
        dataMap.put("src", this.src);
        dataMap.put("srcUrl", this.srcUrl);
        dataMap.put("mediaType", this.mediaType);
        dataMap.put("widthAttr", this.widthAttr);
        dataMap.put("width", this.width);
        dataMap.put("heightAttr", this.heightAttr);
        dataMap.put("height", this.height);
        dataMap.put("note", this.note);

        return dataMap;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) {
            return true;
        }
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        if( !super.equals(obj) ) {
            return false;
        }

        ImageStruct thatObj = (ImageStruct) obj;
        if( (this.uuid == null && thatObj.getUuid() != null)
            || (this.uuid != null && thatObj.getUuid() == null)
            || !this.uuid.equals(thatObj.getUuid()) ) {
            return false;
        }
        if( (this.id == null && thatObj.getId() != null)
            || (this.id != null && thatObj.getId() == null)
            || !this.id.equals(thatObj.getId()) ) {
            return false;
        }
        if( (this.alt == null && thatObj.getAlt() != null)
            || (this.alt != null && thatObj.getAlt() == null)
            || !this.alt.equals(thatObj.getAlt()) ) {
            return false;
        }
        if( (this.src == null && thatObj.getSrc() != null)
            || (this.src != null && thatObj.getSrc() == null)
            || !this.src.equals(thatObj.getSrc()) ) {
            return false;
        }
        if( (this.srcUrl == null && thatObj.getSrcUrl() != null)
            || (this.srcUrl != null && thatObj.getSrcUrl() == null)
            || !this.srcUrl.equals(thatObj.getSrcUrl()) ) {
            return false;
        }
        if( (this.mediaType == null && thatObj.getMediaType() != null)
            || (this.mediaType != null && thatObj.getMediaType() == null)
            || !this.mediaType.equals(thatObj.getMediaType()) ) {
            return false;
        }
        if( (this.widthAttr == null && thatObj.getWidthAttr() != null)
            || (this.widthAttr != null && thatObj.getWidthAttr() == null)
            || !this.widthAttr.equals(thatObj.getWidthAttr()) ) {
            return false;
        }
        if( (this.width == null && thatObj.getWidth() != null)
            || (this.width != null && thatObj.getWidth() == null)
            || !this.width.equals(thatObj.getWidth()) ) {
            return false;
        }
        if( (this.heightAttr == null && thatObj.getHeightAttr() != null)
            || (this.heightAttr != null && thatObj.getHeightAttr() == null)
            || !this.heightAttr.equals(thatObj.getHeightAttr()) ) {
            return false;
        }
        if( (this.height == null && thatObj.getHeight() != null)
            || (this.height != null && thatObj.getHeight() == null)
            || !this.height.equals(thatObj.getHeight()) ) {
            return false;
        }
        if( (this.note == null && thatObj.getNote() != null)
            || (this.note != null && thatObj.getNote() == null)
            || !this.note.equals(thatObj.getNote()) ) {
            return false;
        }

        // Since every corresponding fields are the same, the objects must be the same.
        return true;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = id == null ? 0 : id.hashCode();
        _hash = 31 * _hash + delta;
        delta = alt == null ? 0 : alt.hashCode();
        _hash = 31 * _hash + delta;
        delta = src == null ? 0 : src.hashCode();
        _hash = 31 * _hash + delta;
        delta = srcUrl == null ? 0 : srcUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = mediaType == null ? 0 : mediaType.hashCode();
        _hash = 31 * _hash + delta;
        delta = widthAttr == null ? 0 : widthAttr.hashCode();
        _hash = 31 * _hash + delta;
        delta = width == null ? 0 : width.hashCode();
        _hash = 31 * _hash + delta;
        delta = heightAttr == null ? 0 : heightAttr.hashCode();
        _hash = 31 * _hash + delta;
        delta = height == null ? 0 : height.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public String toString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuffer sb = new StringBuffer();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString(); // ???
            sb.append(k).append(":").append(v).append(";");
        }
        return sb.toString();
    }

    // TBD: Serialization methods?
    // ...

}
