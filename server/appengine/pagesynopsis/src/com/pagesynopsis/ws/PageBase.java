package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface PageBase 
{
    String  getGuid();
    String  getUser();
    String  getFetchRequest();
    String  getTargetUrl();
    String  getPageUrl();
    String  getQueryString();
    List<KeyValuePairStruct>  getQueryParams();
    String  getLastFetchResult();
    Integer  getResponseCode();
    String  getContentType();
    Integer  getContentLength();
    String  getLanguage();
    String  getRedirect();
    String  getLocation();
    String  getPageTitle();
    String  getNote();
    Boolean  isDeferred();
    String  getStatus();
    Integer  getRefreshStatus();
    Long  getRefreshInterval();
    Long  getNextRefreshTime();
    Long  getLastCheckedTime();
    Long  getLastUpdatedTime();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
