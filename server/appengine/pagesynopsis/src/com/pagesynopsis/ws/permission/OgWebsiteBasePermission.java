package com.pagesynopsis.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class OgWebsiteBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgWebsiteBasePermission.class.getName());

    public OgWebsiteBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "OgWebsite::" + action;
    }

    @Override
    public boolean isCreatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isReadPermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isUpdatePermissionRequired()
    {
        return true;
    }

    @Override
    public boolean isDeletePermissionRequired()
    {
        return true;
    }

}
