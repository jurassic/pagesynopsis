package com.pagesynopsis.ws.permission;

import java.util.logging.Logger;
import java.util.logging.Level;


public class RobotsTextRefreshBasePermission extends AbstractBasePermission
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextRefreshBasePermission.class.getName());

    public RobotsTextRefreshBasePermission()
    {
        super();
    }

    @Override
    public String getPermissionName(String action)
    {
        return "RobotsTextRefresh::" + action;
    }


}
