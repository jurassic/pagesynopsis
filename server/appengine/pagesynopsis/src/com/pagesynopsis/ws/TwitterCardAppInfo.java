package com.pagesynopsis.ws;



public interface TwitterCardAppInfo 
{
    String  getName();
    String  getId();
    String  getUrl();
    boolean isEmpty();
}
