package com.pagesynopsis.ws;



public interface AppBrandStruct 
{
    String  getBrand();
    String  getName();
    String  getDescription();
    boolean isEmpty();
}
