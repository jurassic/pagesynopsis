package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface ImageSet extends PageBase
{
    List<String>  getMediaTypeFilter();
    Set<ImageStruct>  getPageImages();
}
