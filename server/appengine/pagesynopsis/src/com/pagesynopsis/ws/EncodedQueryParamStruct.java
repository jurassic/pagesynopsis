package com.pagesynopsis.ws;



public interface EncodedQueryParamStruct 
{
    String  getParamType();
    String  getOriginalString();
    String  getEncodedString();
    String  getNote();
    boolean isEmpty();
}
