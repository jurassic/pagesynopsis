package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgBlogService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgBlogServiceImpl implements OgBlogService
{
    private static final Logger log = Logger.getLogger(OgBlogServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgBlog related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgBlog getOgBlog(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgBlogDataObject dataObj = getDAOFactory().getOgBlogDAO().getOgBlog(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgBlogDataObject for guid = " + guid);
            return null;  // ????
        }
        OgBlogBean bean = new OgBlogBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgBlog(String guid, String field) throws BaseException
    {
        OgBlogDataObject dataObj = getDAOFactory().getOgBlogDAO().getOgBlog(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgBlogDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgBlog> getOgBlogs(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgBlog> list = new ArrayList<OgBlog>();
        List<OgBlogDataObject> dataObjs = getDAOFactory().getOgBlogDAO().getOgBlogs(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBlogDataObject list.");
        } else {
            Iterator<OgBlogDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgBlogDataObject dataObj = (OgBlogDataObject) it.next();
                list.add(new OgBlogBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgBlog> getAllOgBlogs() throws BaseException
    {
        return getAllOgBlogs(null, null, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBlogs(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgBlog> list = new ArrayList<OgBlog>();
        List<OgBlogDataObject> dataObjs = getDAOFactory().getOgBlogDAO().getAllOgBlogs(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBlogDataObject list.");
        } else {
            Iterator<OgBlogDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgBlogDataObject dataObj = (OgBlogDataObject) it.next();
                list.add(new OgBlogBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgBlogKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgBlogDAO().getAllOgBlogKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBlog key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgBlogServiceImpl.findOgBlogs(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgBlog> list = new ArrayList<OgBlog>();
        List<OgBlogDataObject> dataObjs = getDAOFactory().getOgBlogDAO().findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogBlogs for the given criterion.");
        } else {
            Iterator<OgBlogDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgBlogDataObject dataObj = (OgBlogDataObject) it.next();
                list.add(new OgBlogBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgBlogServiceImpl.findOgBlogKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgBlogDAO().findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgBlog keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgBlogServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgBlogDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgBlogDataObject dataObj = new OgBlogDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        return createOgBlog(dataObj);
    }

    @Override
    public String createOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBlog cannot be null.....
        if(ogBlog == null) {
            log.log(Level.INFO, "Param ogBlog is null!");
            throw new BadRequestException("Param ogBlog object is null!");
        }
        OgBlogDataObject dataObj = null;
        if(ogBlog instanceof OgBlogDataObject) {
            dataObj = (OgBlogDataObject) ogBlog;
        } else if(ogBlog instanceof OgBlogBean) {
            dataObj = ((OgBlogBean) ogBlog).toDataObject();
        } else {  // if(ogBlog instanceof OgBlog)
            //dataObj = new OgBlogDataObject(null, ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote());
        }
        String guid = getDAOFactory().getOgBlogDAO().createOgBlog(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgBlogDataObject dataObj = new OgBlogDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        return updateOgBlog(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBlog cannot be null.....
        if(ogBlog == null || ogBlog.getGuid() == null) {
            log.log(Level.INFO, "Param ogBlog or its guid is null!");
            throw new BadRequestException("Param ogBlog object or its guid is null!");
        }
        OgBlogDataObject dataObj = null;
        if(ogBlog instanceof OgBlogDataObject) {
            dataObj = (OgBlogDataObject) ogBlog;
        } else if(ogBlog instanceof OgBlogBean) {
            dataObj = ((OgBlogBean) ogBlog).toDataObject();
        } else {  // if(ogBlog instanceof OgBlog)
            dataObj = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote());
        }
        Boolean suc = getDAOFactory().getOgBlogDAO().updateOgBlog(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgBlogDAO().deleteOgBlog(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBlog cannot be null.....
        if(ogBlog == null || ogBlog.getGuid() == null) {
            log.log(Level.INFO, "Param ogBlog or its guid is null!");
            throw new BadRequestException("Param ogBlog object or its guid is null!");
        }
        OgBlogDataObject dataObj = null;
        if(ogBlog instanceof OgBlogDataObject) {
            dataObj = (OgBlogDataObject) ogBlog;
        } else if(ogBlog instanceof OgBlogBean) {
            dataObj = ((OgBlogBean) ogBlog).toDataObject();
        } else {  // if(ogBlog instanceof OgBlog)
            dataObj = new OgBlogDataObject(ogBlog.getGuid(), ogBlog.getUrl(), ogBlog.getType(), ogBlog.getSiteName(), ogBlog.getTitle(), ogBlog.getDescription(), ogBlog.getFbAdmins(), ogBlog.getFbAppId(), ogBlog.getImage(), ogBlog.getAudio(), ogBlog.getVideo(), ogBlog.getLocale(), ogBlog.getLocaleAlternate(), ogBlog.getNote());
        }
        Boolean suc = getDAOFactory().getOgBlogDAO().deleteOgBlog(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgBlogDAO().deleteOgBlogs(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
