package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.LinkListBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.LinkListDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.LinkListService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkListServiceImpl implements LinkListService
{
    private static final Logger log = Logger.getLogger(LinkListServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // LinkList related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        log.finer("BEGIN");

        LinkListDataObject dataObj = getDAOFactory().getLinkListDAO().getLinkList(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkListDataObject for guid = " + guid);
            return null;  // ????
        }
        LinkListBean bean = new LinkListBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        LinkListDataObject dataObj = getDAOFactory().getLinkListDAO().getLinkList(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkListDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return dataObj.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("contentType")) {
            return dataObj.getContentType();
        } else if(field.equals("contentLength")) {
            return dataObj.getContentLength();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("redirect")) {
            return dataObj.getRedirect();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("pageTitle")) {
            return dataObj.getPageTitle();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("urlSchemeFilter")) {
            return dataObj.getUrlSchemeFilter();
        } else if(field.equals("pageAnchors")) {
            return dataObj.getPageAnchors();
        } else if(field.equals("excludeRelativeUrls")) {
            return dataObj.isExcludeRelativeUrls();
        } else if(field.equals("excludedBaseUrls")) {
            return dataObj.getExcludedBaseUrls();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<LinkList> list = new ArrayList<LinkList>();
        List<LinkListDataObject> dataObjs = getDAOFactory().getLinkListDAO().getLinkLists(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkListDataObject list.");
        } else {
            Iterator<LinkListDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkListDataObject dataObj = (LinkListDataObject) it.next();
                list.add(new LinkListBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkLists(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<LinkList> list = new ArrayList<LinkList>();
        List<LinkListDataObject> dataObjs = getDAOFactory().getLinkListDAO().getAllLinkLists(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkListDataObject list.");
        } else {
            Iterator<LinkListDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkListDataObject dataObj = (LinkListDataObject) it.next();
                list.add(new LinkListBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllLinkListKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getLinkListDAO().getAllLinkListKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkList key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkListServiceImpl.findLinkLists(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<LinkList> list = new ArrayList<LinkList>();
        List<LinkListDataObject> dataObjs = getDAOFactory().getLinkListDAO().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find linkLists for the given criterion.");
        } else {
            Iterator<LinkListDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                LinkListDataObject dataObj = (LinkListDataObject) it.next();
                list.add(new LinkListBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkListServiceImpl.findLinkListKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getLinkListDAO().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkList keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("LinkListServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getLinkListDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        LinkListDataObject dataObj = new LinkListDataObject(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
        return createLinkList(dataObj);
    }

    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkList cannot be null.....
        if(linkList == null) {
            log.log(Level.INFO, "Param linkList is null!");
            throw new BadRequestException("Param linkList object is null!");
        }
        LinkListDataObject dataObj = null;
        if(linkList instanceof LinkListDataObject) {
            dataObj = (LinkListDataObject) linkList;
        } else if(linkList instanceof LinkListBean) {
            dataObj = ((LinkListBean) linkList).toDataObject();
        } else {  // if(linkList instanceof LinkList)
            //dataObj = new LinkListDataObject(null, linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new LinkListDataObject(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        }
        String guid = getDAOFactory().getLinkListDAO().createLinkList(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkListDataObject dataObj = new LinkListDataObject(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
        return updateLinkList(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkList cannot be null.....
        if(linkList == null || linkList.getGuid() == null) {
            log.log(Level.INFO, "Param linkList or its guid is null!");
            throw new BadRequestException("Param linkList object or its guid is null!");
        }
        LinkListDataObject dataObj = null;
        if(linkList instanceof LinkListDataObject) {
            dataObj = (LinkListDataObject) linkList;
        } else if(linkList instanceof LinkListBean) {
            dataObj = ((LinkListBean) linkList).toDataObject();
        } else {  // if(linkList instanceof LinkList)
            dataObj = new LinkListDataObject(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        }
        Boolean suc = getDAOFactory().getLinkListDAO().updateLinkList(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getLinkListDAO().deleteLinkList(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkList cannot be null.....
        if(linkList == null || linkList.getGuid() == null) {
            log.log(Level.INFO, "Param linkList or its guid is null!");
            throw new BadRequestException("Param linkList object or its guid is null!");
        }
        LinkListDataObject dataObj = null;
        if(linkList instanceof LinkListDataObject) {
            dataObj = (LinkListDataObject) linkList;
        } else if(linkList instanceof LinkListBean) {
            dataObj = ((LinkListBean) linkList).toDataObject();
        } else {  // if(linkList instanceof LinkList)
            dataObj = new LinkListDataObject(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        }
        Boolean suc = getDAOFactory().getLinkListDAO().deleteLinkList(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getLinkListDAO().deleteLinkLists(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
