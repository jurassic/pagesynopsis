package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgWebsiteService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgWebsite getOgWebsite(String guid) throws BaseException;
    Object getOgWebsite(String guid, String field) throws BaseException;
    List<OgWebsite> getOgWebsites(List<String> guids) throws BaseException;
    List<OgWebsite> getAllOgWebsites() throws BaseException;
    /* @Deprecated */ List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count) throws BaseException;
    List<OgWebsite> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgWebsite> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException;
    //String createOgWebsite(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgWebsite?)
    String createOgWebsite(OgWebsite ogWebsite) throws BaseException;          // Returns Guid.  (Return OgWebsite?)
    Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException;
    //Boolean updateOgWebsite(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgWebsite(OgWebsite ogWebsite) throws BaseException;
    Boolean deleteOgWebsite(String guid) throws BaseException;
    Boolean deleteOgWebsite(OgWebsite ogWebsite) throws BaseException;
    Long deleteOgWebsites(String filter, String params, List<String> values) throws BaseException;

//    Integer createOgWebsites(List<OgWebsite> ogWebsites) throws BaseException;
//    Boolean updateeOgWebsites(List<OgWebsite> ogWebsites) throws BaseException;

}
