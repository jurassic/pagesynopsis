package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgTvEpisodeService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgTvEpisode getOgTvEpisode(String guid) throws BaseException;
    Object getOgTvEpisode(String guid, String field) throws BaseException;
    List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException;
    List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException;
    /* @Deprecated */ List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException;
    List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //String createOgTvEpisode(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgTvEpisode?)
    String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException;          // Returns Guid.  (Return OgTvEpisode?)
    Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //Boolean updateOgTvEpisode(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException;
    Boolean deleteOgTvEpisode(String guid) throws BaseException;
    Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException;
    Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException;

//    Integer createOgTvEpisodes(List<OgTvEpisode> ogTvEpisodes) throws BaseException;
//    Boolean updateeOgTvEpisodes(List<OgTvEpisode> ogTvEpisodes) throws BaseException;

}
