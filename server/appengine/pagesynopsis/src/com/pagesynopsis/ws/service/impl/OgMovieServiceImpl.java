package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgMovieService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgMovieServiceImpl implements OgMovieService
{
    private static final Logger log = Logger.getLogger(OgMovieServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgMovie related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgMovieDataObject dataObj = getDAOFactory().getOgMovieDAO().getOgMovie(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgMovieDataObject for guid = " + guid);
            return null;  // ????
        }
        OgMovieBean bean = new OgMovieBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        OgMovieDataObject dataObj = getDAOFactory().getOgMovieDAO().getOgMovie(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgMovieDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("director")) {
            return dataObj.getDirector();
        } else if(field.equals("writer")) {
            return dataObj.getWriter();
        } else if(field.equals("actor")) {
            return dataObj.getActor();
        } else if(field.equals("duration")) {
            return dataObj.getDuration();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("releaseDate")) {
            return dataObj.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgMovie> list = new ArrayList<OgMovie>();
        List<OgMovieDataObject> dataObjs = getDAOFactory().getOgMovieDAO().getOgMovies(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgMovieDataObject list.");
        } else {
            Iterator<OgMovieDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgMovieDataObject dataObj = (OgMovieDataObject) it.next();
                list.add(new OgMovieBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return getAllOgMovies(null, null, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovies(ordering, offset, count, null);
    }

    @Override
    public List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgMovies(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgMovie> list = new ArrayList<OgMovie>();
        List<OgMovieDataObject> dataObjs = getDAOFactory().getOgMovieDAO().getAllOgMovies(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgMovieDataObject list.");
        } else {
            Iterator<OgMovieDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgMovieDataObject dataObj = (OgMovieDataObject) it.next();
                list.add(new OgMovieBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgMovieKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgMovieDAO().getAllOgMovieKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgMovie key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgMovieServiceImpl.findOgMovies(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgMovie> list = new ArrayList<OgMovie>();
        List<OgMovieDataObject> dataObjs = getDAOFactory().getOgMovieDAO().findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogMovies for the given criterion.");
        } else {
            Iterator<OgMovieDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgMovieDataObject dataObj = (OgMovieDataObject) it.next();
                list.add(new OgMovieBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgMovieServiceImpl.findOgMovieKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgMovieDAO().findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgMovie keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgMovieServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgMovieDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgMovieDataObject dataObj = new OgMovieDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgMovie(dataObj);
    }

    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogMovie cannot be null.....
        if(ogMovie == null) {
            log.log(Level.INFO, "Param ogMovie is null!");
            throw new BadRequestException("Param ogMovie object is null!");
        }
        OgMovieDataObject dataObj = null;
        if(ogMovie instanceof OgMovieDataObject) {
            dataObj = (OgMovieDataObject) ogMovie;
        } else if(ogMovie instanceof OgMovieBean) {
            dataObj = ((OgMovieBean) ogMovie).toDataObject();
        } else {  // if(ogMovie instanceof OgMovie)
            //dataObj = new OgMovieDataObject(null, ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        }
        String guid = getDAOFactory().getOgMovieDAO().createOgMovie(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgMovieDataObject dataObj = new OgMovieDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgMovie(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogMovie cannot be null.....
        if(ogMovie == null || ogMovie.getGuid() == null) {
            log.log(Level.INFO, "Param ogMovie or its guid is null!");
            throw new BadRequestException("Param ogMovie object or its guid is null!");
        }
        OgMovieDataObject dataObj = null;
        if(ogMovie instanceof OgMovieDataObject) {
            dataObj = (OgMovieDataObject) ogMovie;
        } else if(ogMovie instanceof OgMovieBean) {
            dataObj = ((OgMovieBean) ogMovie).toDataObject();
        } else {  // if(ogMovie instanceof OgMovie)
            dataObj = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgMovieDAO().updateOgMovie(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgMovieDAO().deleteOgMovie(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogMovie cannot be null.....
        if(ogMovie == null || ogMovie.getGuid() == null) {
            log.log(Level.INFO, "Param ogMovie or its guid is null!");
            throw new BadRequestException("Param ogMovie object or its guid is null!");
        }
        OgMovieDataObject dataObj = null;
        if(ogMovie instanceof OgMovieDataObject) {
            dataObj = (OgMovieDataObject) ogMovie;
        } else if(ogMovie instanceof OgMovieBean) {
            dataObj = ((OgMovieBean) ogMovie).toDataObject();
        } else {  // if(ogMovie instanceof OgMovie)
            dataObj = new OgMovieDataObject(ogMovie.getGuid(), ogMovie.getUrl(), ogMovie.getType(), ogMovie.getSiteName(), ogMovie.getTitle(), ogMovie.getDescription(), ogMovie.getFbAdmins(), ogMovie.getFbAppId(), ogMovie.getImage(), ogMovie.getAudio(), ogMovie.getVideo(), ogMovie.getLocale(), ogMovie.getLocaleAlternate(), ogMovie.getDirector(), ogMovie.getWriter(), ogMovie.getActor(), ogMovie.getDuration(), ogMovie.getTag(), ogMovie.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgMovieDAO().deleteOgMovie(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgMovieDAO().deleteOgMovies(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
