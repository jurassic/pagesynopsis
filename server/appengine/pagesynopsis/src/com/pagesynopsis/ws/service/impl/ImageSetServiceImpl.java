package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.ImageSetBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.ImageSetDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.ImageSetService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ImageSetServiceImpl implements ImageSetService
{
    private static final Logger log = Logger.getLogger(ImageSetServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // ImageSet related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ImageSet getImageSet(String guid) throws BaseException
    {
        log.finer("BEGIN");

        ImageSetDataObject dataObj = getDAOFactory().getImageSetDAO().getImageSet(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ImageSetDataObject for guid = " + guid);
            return null;  // ????
        }
        ImageSetBean bean = new ImageSetBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getImageSet(String guid, String field) throws BaseException
    {
        ImageSetDataObject dataObj = getDAOFactory().getImageSetDAO().getImageSet(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ImageSetDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return dataObj.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("contentType")) {
            return dataObj.getContentType();
        } else if(field.equals("contentLength")) {
            return dataObj.getContentLength();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("redirect")) {
            return dataObj.getRedirect();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("pageTitle")) {
            return dataObj.getPageTitle();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("mediaTypeFilter")) {
            return dataObj.getMediaTypeFilter();
        } else if(field.equals("pageImages")) {
            return dataObj.getPageImages();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ImageSet> getImageSets(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<ImageSet> list = new ArrayList<ImageSet>();
        List<ImageSetDataObject> dataObjs = getDAOFactory().getImageSetDAO().getImageSets(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ImageSetDataObject list.");
        } else {
            Iterator<ImageSetDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ImageSetDataObject dataObj = (ImageSetDataObject) it.next();
                list.add(new ImageSetBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<ImageSet> getAllImageSets() throws BaseException
    {
        return getAllImageSets(null, null, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSets(ordering, offset, count, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllImageSets(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ImageSet> list = new ArrayList<ImageSet>();
        List<ImageSetDataObject> dataObjs = getDAOFactory().getImageSetDAO().getAllImageSets(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve ImageSetDataObject list.");
        } else {
            Iterator<ImageSetDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ImageSetDataObject dataObj = (ImageSetDataObject) it.next();
                list.add(new ImageSetBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllImageSetKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getImageSetDAO().getAllImageSetKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ImageSet key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ImageSetServiceImpl.findImageSets(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ImageSet> list = new ArrayList<ImageSet>();
        List<ImageSetDataObject> dataObjs = getDAOFactory().getImageSetDAO().findImageSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find imageSets for the given criterion.");
        } else {
            Iterator<ImageSetDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                ImageSetDataObject dataObj = (ImageSetDataObject) it.next();
                list.add(new ImageSetBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ImageSetServiceImpl.findImageSetKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getImageSetDAO().findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ImageSet keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("ImageSetServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getImageSetDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        ImageSetDataObject dataObj = new ImageSetDataObject(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
        return createImageSet(dataObj);
    }

    @Override
    public String createImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param imageSet cannot be null.....
        if(imageSet == null) {
            log.log(Level.INFO, "Param imageSet is null!");
            throw new BadRequestException("Param imageSet object is null!");
        }
        ImageSetDataObject dataObj = null;
        if(imageSet instanceof ImageSetDataObject) {
            dataObj = (ImageSetDataObject) imageSet;
        } else if(imageSet instanceof ImageSetBean) {
            dataObj = ((ImageSetBean) imageSet).toDataObject();
        } else {  // if(imageSet instanceof ImageSet)
            //dataObj = new ImageSetDataObject(null, imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new ImageSetDataObject(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        }
        String guid = getDAOFactory().getImageSetDAO().createImageSet(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ImageSetDataObject dataObj = new ImageSetDataObject(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
        return updateImageSet(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param imageSet cannot be null.....
        if(imageSet == null || imageSet.getGuid() == null) {
            log.log(Level.INFO, "Param imageSet or its guid is null!");
            throw new BadRequestException("Param imageSet object or its guid is null!");
        }
        ImageSetDataObject dataObj = null;
        if(imageSet instanceof ImageSetDataObject) {
            dataObj = (ImageSetDataObject) imageSet;
        } else if(imageSet instanceof ImageSetBean) {
            dataObj = ((ImageSetBean) imageSet).toDataObject();
        } else {  // if(imageSet instanceof ImageSet)
            dataObj = new ImageSetDataObject(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        }
        Boolean suc = getDAOFactory().getImageSetDAO().updateImageSet(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getImageSetDAO().deleteImageSet(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param imageSet cannot be null.....
        if(imageSet == null || imageSet.getGuid() == null) {
            log.log(Level.INFO, "Param imageSet or its guid is null!");
            throw new BadRequestException("Param imageSet object or its guid is null!");
        }
        ImageSetDataObject dataObj = null;
        if(imageSet instanceof ImageSetDataObject) {
            dataObj = (ImageSetDataObject) imageSet;
        } else if(imageSet instanceof ImageSetBean) {
            dataObj = ((ImageSetBean) imageSet).toDataObject();
        } else {  // if(imageSet instanceof ImageSet)
            dataObj = new ImageSetDataObject(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        }
        Boolean suc = getDAOFactory().getImageSetDAO().deleteImageSet(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteImageSets(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getImageSetDAO().deleteImageSets(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
