package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgTvEpisodeService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgTvEpisodeServiceImpl implements OgTvEpisodeService
{
    private static final Logger log = Logger.getLogger(OgTvEpisodeServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgTvEpisode related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgTvEpisode getOgTvEpisode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgTvEpisodeDataObject dataObj = getDAOFactory().getOgTvEpisodeDAO().getOgTvEpisode(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgTvEpisodeDataObject for guid = " + guid);
            return null;  // ????
        }
        OgTvEpisodeBean bean = new OgTvEpisodeBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgTvEpisode(String guid, String field) throws BaseException
    {
        OgTvEpisodeDataObject dataObj = getDAOFactory().getOgTvEpisodeDAO().getOgTvEpisode(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgTvEpisodeDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("director")) {
            return dataObj.getDirector();
        } else if(field.equals("writer")) {
            return dataObj.getWriter();
        } else if(field.equals("actor")) {
            return dataObj.getActor();
        } else if(field.equals("series")) {
            return dataObj.getSeries();
        } else if(field.equals("duration")) {
            return dataObj.getDuration();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("releaseDate")) {
            return dataObj.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgTvEpisode> list = new ArrayList<OgTvEpisode>();
        List<OgTvEpisodeDataObject> dataObjs = getDAOFactory().getOgTvEpisodeDAO().getOgTvEpisodes(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgTvEpisodeDataObject list.");
        } else {
            Iterator<OgTvEpisodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgTvEpisodeDataObject dataObj = (OgTvEpisodeDataObject) it.next();
                list.add(new OgTvEpisodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException
    {
        return getAllOgTvEpisodes(null, null, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvEpisodes(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgTvEpisode> list = new ArrayList<OgTvEpisode>();
        List<OgTvEpisodeDataObject> dataObjs = getDAOFactory().getOgTvEpisodeDAO().getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgTvEpisodeDataObject list.");
        } else {
            Iterator<OgTvEpisodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgTvEpisodeDataObject dataObj = (OgTvEpisodeDataObject) it.next();
                list.add(new OgTvEpisodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgTvEpisodeKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgTvEpisodeDAO().getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgTvEpisode key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgTvEpisodeServiceImpl.findOgTvEpisodes(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgTvEpisode> list = new ArrayList<OgTvEpisode>();
        List<OgTvEpisodeDataObject> dataObjs = getDAOFactory().getOgTvEpisodeDAO().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogTvEpisodes for the given criterion.");
        } else {
            Iterator<OgTvEpisodeDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgTvEpisodeDataObject dataObj = (OgTvEpisodeDataObject) it.next();
                list.add(new OgTvEpisodeBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgTvEpisodeServiceImpl.findOgTvEpisodeKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgTvEpisodeDAO().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgTvEpisode keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgTvEpisodeServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgTvEpisodeDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgTvEpisodeDataObject dataObj = new OgTvEpisodeDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        return createOgTvEpisode(dataObj);
    }

    @Override
    public String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogTvEpisode cannot be null.....
        if(ogTvEpisode == null) {
            log.log(Level.INFO, "Param ogTvEpisode is null!");
            throw new BadRequestException("Param ogTvEpisode object is null!");
        }
        OgTvEpisodeDataObject dataObj = null;
        if(ogTvEpisode instanceof OgTvEpisodeDataObject) {
            dataObj = (OgTvEpisodeDataObject) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisodeBean) {
            dataObj = ((OgTvEpisodeBean) ogTvEpisode).toDataObject();
        } else {  // if(ogTvEpisode instanceof OgTvEpisode)
            //dataObj = new OgTvEpisodeDataObject(null, ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate());
        }
        String guid = getDAOFactory().getOgTvEpisodeDAO().createOgTvEpisode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgTvEpisodeDataObject dataObj = new OgTvEpisodeDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        return updateOgTvEpisode(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogTvEpisode cannot be null.....
        if(ogTvEpisode == null || ogTvEpisode.getGuid() == null) {
            log.log(Level.INFO, "Param ogTvEpisode or its guid is null!");
            throw new BadRequestException("Param ogTvEpisode object or its guid is null!");
        }
        OgTvEpisodeDataObject dataObj = null;
        if(ogTvEpisode instanceof OgTvEpisodeDataObject) {
            dataObj = (OgTvEpisodeDataObject) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisodeBean) {
            dataObj = ((OgTvEpisodeBean) ogTvEpisode).toDataObject();
        } else {  // if(ogTvEpisode instanceof OgTvEpisode)
            dataObj = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgTvEpisodeDAO().updateOgTvEpisode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgTvEpisodeDAO().deleteOgTvEpisode(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogTvEpisode cannot be null.....
        if(ogTvEpisode == null || ogTvEpisode.getGuid() == null) {
            log.log(Level.INFO, "Param ogTvEpisode or its guid is null!");
            throw new BadRequestException("Param ogTvEpisode object or its guid is null!");
        }
        OgTvEpisodeDataObject dataObj = null;
        if(ogTvEpisode instanceof OgTvEpisodeDataObject) {
            dataObj = (OgTvEpisodeDataObject) ogTvEpisode;
        } else if(ogTvEpisode instanceof OgTvEpisodeBean) {
            dataObj = ((OgTvEpisodeBean) ogTvEpisode).toDataObject();
        } else {  // if(ogTvEpisode instanceof OgTvEpisode)
            dataObj = new OgTvEpisodeDataObject(ogTvEpisode.getGuid(), ogTvEpisode.getUrl(), ogTvEpisode.getType(), ogTvEpisode.getSiteName(), ogTvEpisode.getTitle(), ogTvEpisode.getDescription(), ogTvEpisode.getFbAdmins(), ogTvEpisode.getFbAppId(), ogTvEpisode.getImage(), ogTvEpisode.getAudio(), ogTvEpisode.getVideo(), ogTvEpisode.getLocale(), ogTvEpisode.getLocaleAlternate(), ogTvEpisode.getDirector(), ogTvEpisode.getWriter(), ogTvEpisode.getActor(), ogTvEpisode.getSeries(), ogTvEpisode.getDuration(), ogTvEpisode.getTag(), ogTvEpisode.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgTvEpisodeDAO().deleteOgTvEpisode(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgTvEpisodeDAO().deleteOgTvEpisodes(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
