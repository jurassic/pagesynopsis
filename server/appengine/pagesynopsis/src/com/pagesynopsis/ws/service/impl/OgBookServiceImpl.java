package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgBookService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgBookServiceImpl implements OgBookService
{
    private static final Logger log = Logger.getLogger(OgBookServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgBook related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgBook getOgBook(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgBookDataObject dataObj = getDAOFactory().getOgBookDAO().getOgBook(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgBookDataObject for guid = " + guid);
            return null;  // ????
        }
        OgBookBean bean = new OgBookBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgBook(String guid, String field) throws BaseException
    {
        OgBookDataObject dataObj = getDAOFactory().getOgBookDAO().getOgBook(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgBookDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("author")) {
            return dataObj.getAuthor();
        } else if(field.equals("isbn")) {
            return dataObj.getIsbn();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("releaseDate")) {
            return dataObj.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgBook> getOgBooks(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgBook> list = new ArrayList<OgBook>();
        List<OgBookDataObject> dataObjs = getDAOFactory().getOgBookDAO().getOgBooks(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBookDataObject list.");
        } else {
            Iterator<OgBookDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgBookDataObject dataObj = (OgBookDataObject) it.next();
                list.add(new OgBookBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgBook> getAllOgBooks() throws BaseException
    {
        return getAllOgBooks(null, null, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgBooks(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgBook> list = new ArrayList<OgBook>();
        List<OgBookDataObject> dataObjs = getDAOFactory().getOgBookDAO().getAllOgBooks(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBookDataObject list.");
        } else {
            Iterator<OgBookDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgBookDataObject dataObj = (OgBookDataObject) it.next();
                list.add(new OgBookBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgBookKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgBookDAO().getAllOgBookKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgBook key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgBookServiceImpl.findOgBooks(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgBook> list = new ArrayList<OgBook>();
        List<OgBookDataObject> dataObjs = getDAOFactory().getOgBookDAO().findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogBooks for the given criterion.");
        } else {
            Iterator<OgBookDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgBookDataObject dataObj = (OgBookDataObject) it.next();
                list.add(new OgBookBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgBookServiceImpl.findOgBookKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgBookDAO().findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgBook keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgBookServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgBookDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgBookDataObject dataObj = new OgBookDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        return createOgBook(dataObj);
    }

    @Override
    public String createOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBook cannot be null.....
        if(ogBook == null) {
            log.log(Level.INFO, "Param ogBook is null!");
            throw new BadRequestException("Param ogBook object is null!");
        }
        OgBookDataObject dataObj = null;
        if(ogBook instanceof OgBookDataObject) {
            dataObj = (OgBookDataObject) ogBook;
        } else if(ogBook instanceof OgBookBean) {
            dataObj = ((OgBookBean) ogBook).toDataObject();
        } else {  // if(ogBook instanceof OgBook)
            //dataObj = new OgBookDataObject(null, ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        }
        String guid = getDAOFactory().getOgBookDAO().createOgBook(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgBookDataObject dataObj = new OgBookDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, isbn, tag, releaseDate);
        return updateOgBook(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBook cannot be null.....
        if(ogBook == null || ogBook.getGuid() == null) {
            log.log(Level.INFO, "Param ogBook or its guid is null!");
            throw new BadRequestException("Param ogBook object or its guid is null!");
        }
        OgBookDataObject dataObj = null;
        if(ogBook instanceof OgBookDataObject) {
            dataObj = (OgBookDataObject) ogBook;
        } else if(ogBook instanceof OgBookBean) {
            dataObj = ((OgBookBean) ogBook).toDataObject();
        } else {  // if(ogBook instanceof OgBook)
            dataObj = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgBookDAO().updateOgBook(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgBookDAO().deleteOgBook(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgBook(OgBook ogBook) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogBook cannot be null.....
        if(ogBook == null || ogBook.getGuid() == null) {
            log.log(Level.INFO, "Param ogBook or its guid is null!");
            throw new BadRequestException("Param ogBook object or its guid is null!");
        }
        OgBookDataObject dataObj = null;
        if(ogBook instanceof OgBookDataObject) {
            dataObj = (OgBookDataObject) ogBook;
        } else if(ogBook instanceof OgBookBean) {
            dataObj = ((OgBookBean) ogBook).toDataObject();
        } else {  // if(ogBook instanceof OgBook)
            dataObj = new OgBookDataObject(ogBook.getGuid(), ogBook.getUrl(), ogBook.getType(), ogBook.getSiteName(), ogBook.getTitle(), ogBook.getDescription(), ogBook.getFbAdmins(), ogBook.getFbAppId(), ogBook.getImage(), ogBook.getAudio(), ogBook.getVideo(), ogBook.getLocale(), ogBook.getLocaleAlternate(), ogBook.getAuthor(), ogBook.getIsbn(), ogBook.getTag(), ogBook.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgBookDAO().deleteOgBook(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgBookDAO().deleteOgBooks(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
