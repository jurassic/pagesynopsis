package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.bean.OgVideoBean;
import com.pagesynopsis.ws.bean.TwitterProductCardBean;
import com.pagesynopsis.ws.bean.TwitterSummaryCardBean;
import com.pagesynopsis.ws.bean.OgBlogBean;
import com.pagesynopsis.ws.bean.TwitterPlayerCardBean;
import com.pagesynopsis.ws.bean.UrlStructBean;
import com.pagesynopsis.ws.bean.ImageStructBean;
import com.pagesynopsis.ws.bean.TwitterGalleryCardBean;
import com.pagesynopsis.ws.bean.TwitterPhotoCardBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.bean.OgBookBean;
import com.pagesynopsis.ws.bean.OgWebsiteBean;
import com.pagesynopsis.ws.bean.OgMovieBean;
import com.pagesynopsis.ws.bean.TwitterAppCardBean;
import com.pagesynopsis.ws.bean.AnchorStructBean;
import com.pagesynopsis.ws.bean.KeyValuePairStructBean;
import com.pagesynopsis.ws.bean.OgArticleBean;
import com.pagesynopsis.ws.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.bean.AudioStructBean;
import com.pagesynopsis.ws.bean.VideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.bean.TwitterCardMetaBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.TwitterCardMetaDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.TwitterCardMetaService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterCardMetaServiceImpl implements TwitterCardMetaService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // TwitterCardMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");

        TwitterCardMetaDataObject dataObj = getDAOFactory().getTwitterCardMetaDAO().getTwitterCardMeta(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaDataObject for guid = " + guid);
            return null;  // ????
        }
        TwitterCardMetaBean bean = new TwitterCardMetaBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterCardMeta(String guid, String field) throws BaseException
    {
        TwitterCardMetaDataObject dataObj = getDAOFactory().getTwitterCardMetaDAO().getTwitterCardMeta(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return dataObj.getUser();
        } else if(field.equals("fetchRequest")) {
            return dataObj.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return dataObj.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return dataObj.getPageUrl();
        } else if(field.equals("queryString")) {
            return dataObj.getQueryString();
        } else if(field.equals("queryParams")) {
            return dataObj.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return dataObj.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return dataObj.getResponseCode();
        } else if(field.equals("contentType")) {
            return dataObj.getContentType();
        } else if(field.equals("contentLength")) {
            return dataObj.getContentLength();
        } else if(field.equals("language")) {
            return dataObj.getLanguage();
        } else if(field.equals("redirect")) {
            return dataObj.getRedirect();
        } else if(field.equals("location")) {
            return dataObj.getLocation();
        } else if(field.equals("pageTitle")) {
            return dataObj.getPageTitle();
        } else if(field.equals("note")) {
            return dataObj.getNote();
        } else if(field.equals("deferred")) {
            return dataObj.isDeferred();
        } else if(field.equals("status")) {
            return dataObj.getStatus();
        } else if(field.equals("refreshStatus")) {
            return dataObj.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return dataObj.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return dataObj.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("cardType")) {
            return dataObj.getCardType();
        } else if(field.equals("summaryCard")) {
            return dataObj.getSummaryCard();
        } else if(field.equals("photoCard")) {
            return dataObj.getPhotoCard();
        } else if(field.equals("galleryCard")) {
            return dataObj.getGalleryCard();
        } else if(field.equals("appCard")) {
            return dataObj.getAppCard();
        } else if(field.equals("playerCard")) {
            return dataObj.getPlayerCard();
        } else if(field.equals("productCard")) {
            return dataObj.getProductCard();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<TwitterCardMeta> list = new ArrayList<TwitterCardMeta>();
        List<TwitterCardMetaDataObject> dataObjs = getDAOFactory().getTwitterCardMetaDAO().getTwitterCardMetas(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaDataObject list.");
        } else {
            Iterator<TwitterCardMetaDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterCardMetaDataObject dataObj = (TwitterCardMetaDataObject) it.next();
                list.add(new TwitterCardMetaBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException
    {
        return getAllTwitterCardMetas(null, null, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetas(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TwitterCardMeta> list = new ArrayList<TwitterCardMeta>();
        List<TwitterCardMetaDataObject> dataObjs = getDAOFactory().getTwitterCardMetaDAO().getAllTwitterCardMetas(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaDataObject list.");
        } else {
            Iterator<TwitterCardMetaDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterCardMetaDataObject dataObj = (TwitterCardMetaDataObject) it.next();
                list.add(new TwitterCardMetaBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllTwitterCardMetaKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getTwitterCardMetaDAO().getAllTwitterCardMetaKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterCardMeta key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterCardMetaServiceImpl.findTwitterCardMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TwitterCardMeta> list = new ArrayList<TwitterCardMeta>();
        List<TwitterCardMetaDataObject> dataObjs = getDAOFactory().getTwitterCardMetaDAO().findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find twitterCardMetas for the given criterion.");
        } else {
            Iterator<TwitterCardMetaDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                TwitterCardMetaDataObject dataObj = (TwitterCardMetaDataObject) it.next();
                list.add(new TwitterCardMetaBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterCardMetaServiceImpl.findTwitterCardMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getTwitterCardMetaDAO().findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterCardMeta keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("TwitterCardMetaServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getTwitterCardMetaDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        TwitterSummaryCardDataObject summaryCardDobj = null;
        if(summaryCard instanceof TwitterSummaryCardBean) {
            summaryCardDobj = ((TwitterSummaryCardBean) summaryCard).toDataObject();
        } else if(summaryCard instanceof TwitterSummaryCard) {
            summaryCardDobj = new TwitterSummaryCardDataObject(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            summaryCardDobj = null;   // ????
        }
        TwitterPhotoCardDataObject photoCardDobj = null;
        if(photoCard instanceof TwitterPhotoCardBean) {
            photoCardDobj = ((TwitterPhotoCardBean) photoCard).toDataObject();
        } else if(photoCard instanceof TwitterPhotoCard) {
            photoCardDobj = new TwitterPhotoCardDataObject(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            photoCardDobj = null;   // ????
        }
        TwitterGalleryCardDataObject galleryCardDobj = null;
        if(galleryCard instanceof TwitterGalleryCardBean) {
            galleryCardDobj = ((TwitterGalleryCardBean) galleryCard).toDataObject();
        } else if(galleryCard instanceof TwitterGalleryCard) {
            galleryCardDobj = new TwitterGalleryCardDataObject(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            galleryCardDobj = null;   // ????
        }
        TwitterAppCardDataObject appCardDobj = null;
        if(appCard instanceof TwitterAppCardBean) {
            appCardDobj = ((TwitterAppCardBean) appCard).toDataObject();
        } else if(appCard instanceof TwitterAppCard) {
            appCardDobj = new TwitterAppCardDataObject(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), appCard.getIphoneApp(), appCard.getIpadApp(), appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            appCardDobj = null;   // ????
        }
        TwitterPlayerCardDataObject playerCardDobj = null;
        if(playerCard instanceof TwitterPlayerCardBean) {
            playerCardDobj = ((TwitterPlayerCardBean) playerCard).toDataObject();
        } else if(playerCard instanceof TwitterPlayerCard) {
            playerCardDobj = new TwitterPlayerCardDataObject(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            playerCardDobj = null;   // ????
        }
        TwitterProductCardDataObject productCardDobj = null;
        if(productCard instanceof TwitterProductCardBean) {
            productCardDobj = ((TwitterProductCardBean) productCard).toDataObject();
        } else if(productCard instanceof TwitterProductCard) {
            productCardDobj = new TwitterProductCardDataObject(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), productCard.getData1(), productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            productCardDobj = null;   // ????
        }
        
        TwitterCardMetaDataObject dataObj = new TwitterCardMetaDataObject(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCardDobj, photoCardDobj, galleryCardDobj, appCardDobj, playerCardDobj, productCardDobj);
        return createTwitterCardMeta(dataObj);
    }

    @Override
    public String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null) {
            log.log(Level.INFO, "Param twitterCardMeta is null!");
            throw new BadRequestException("Param twitterCardMeta object is null!");
        }
        TwitterCardMetaDataObject dataObj = null;
        if(twitterCardMeta instanceof TwitterCardMetaDataObject) {
            dataObj = (TwitterCardMetaDataObject) twitterCardMeta;
        } else if(twitterCardMeta instanceof TwitterCardMetaBean) {
            dataObj = ((TwitterCardMetaBean) twitterCardMeta).toDataObject();
        } else {  // if(twitterCardMeta instanceof TwitterCardMeta)
            //dataObj = new TwitterCardMetaDataObject(null, twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardDataObject) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardDataObject) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardDataObject) twitterCardMeta.getGalleryCard(), (TwitterAppCardDataObject) twitterCardMeta.getAppCard(), (TwitterPlayerCardDataObject) twitterCardMeta.getPlayerCard(), (TwitterProductCardDataObject) twitterCardMeta.getProductCard());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new TwitterCardMetaDataObject(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardDataObject) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardDataObject) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardDataObject) twitterCardMeta.getGalleryCard(), (TwitterAppCardDataObject) twitterCardMeta.getAppCard(), (TwitterPlayerCardDataObject) twitterCardMeta.getPlayerCard(), (TwitterProductCardDataObject) twitterCardMeta.getProductCard());
        }
        String guid = getDAOFactory().getTwitterCardMetaDAO().createTwitterCardMeta(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        TwitterSummaryCardDataObject summaryCardDobj = null;
        if(summaryCard instanceof TwitterSummaryCardBean) {
            summaryCardDobj = ((TwitterSummaryCardBean) summaryCard).toDataObject();            
        } else if(summaryCard instanceof TwitterSummaryCard) {
            summaryCardDobj = new TwitterSummaryCardDataObject(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            summaryCardDobj = null;   // ????
        }
        TwitterPhotoCardDataObject photoCardDobj = null;
        if(photoCard instanceof TwitterPhotoCardBean) {
            photoCardDobj = ((TwitterPhotoCardBean) photoCard).toDataObject();            
        } else if(photoCard instanceof TwitterPhotoCard) {
            photoCardDobj = new TwitterPhotoCardDataObject(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            photoCardDobj = null;   // ????
        }
        TwitterGalleryCardDataObject galleryCardDobj = null;
        if(galleryCard instanceof TwitterGalleryCardBean) {
            galleryCardDobj = ((TwitterGalleryCardBean) galleryCard).toDataObject();            
        } else if(galleryCard instanceof TwitterGalleryCard) {
            galleryCardDobj = new TwitterGalleryCardDataObject(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            galleryCardDobj = null;   // ????
        }
        TwitterAppCardDataObject appCardDobj = null;
        if(appCard instanceof TwitterAppCardBean) {
            appCardDobj = ((TwitterAppCardBean) appCard).toDataObject();            
        } else if(appCard instanceof TwitterAppCard) {
            appCardDobj = new TwitterAppCardDataObject(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), appCard.getIphoneApp(), appCard.getIpadApp(), appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            appCardDobj = null;   // ????
        }
        TwitterPlayerCardDataObject playerCardDobj = null;
        if(playerCard instanceof TwitterPlayerCardBean) {
            playerCardDobj = ((TwitterPlayerCardBean) playerCard).toDataObject();            
        } else if(playerCard instanceof TwitterPlayerCard) {
            playerCardDobj = new TwitterPlayerCardDataObject(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            playerCardDobj = null;   // ????
        }
        TwitterProductCardDataObject productCardDobj = null;
        if(productCard instanceof TwitterProductCardBean) {
            productCardDobj = ((TwitterProductCardBean) productCard).toDataObject();            
        } else if(productCard instanceof TwitterProductCard) {
            productCardDobj = new TwitterProductCardDataObject(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), productCard.getData1(), productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            productCardDobj = null;   // ????
        }

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterCardMetaDataObject dataObj = new TwitterCardMetaDataObject(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCardDobj, photoCardDobj, galleryCardDobj, appCardDobj, playerCardDobj, productCardDobj);
        return updateTwitterCardMeta(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null || twitterCardMeta.getGuid() == null) {
            log.log(Level.INFO, "Param twitterCardMeta or its guid is null!");
            throw new BadRequestException("Param twitterCardMeta object or its guid is null!");
        }
        TwitterCardMetaDataObject dataObj = null;
        if(twitterCardMeta instanceof TwitterCardMetaDataObject) {
            dataObj = (TwitterCardMetaDataObject) twitterCardMeta;
        } else if(twitterCardMeta instanceof TwitterCardMetaBean) {
            dataObj = ((TwitterCardMetaBean) twitterCardMeta).toDataObject();
        } else {  // if(twitterCardMeta instanceof TwitterCardMeta)
            dataObj = new TwitterCardMetaDataObject(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), twitterCardMeta.getSummaryCard(), twitterCardMeta.getPhotoCard(), twitterCardMeta.getGalleryCard(), twitterCardMeta.getAppCard(), twitterCardMeta.getPlayerCard(), twitterCardMeta.getProductCard());
        }
        Boolean suc = getDAOFactory().getTwitterCardMetaDAO().updateTwitterCardMeta(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getTwitterCardMetaDAO().deleteTwitterCardMeta(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null || twitterCardMeta.getGuid() == null) {
            log.log(Level.INFO, "Param twitterCardMeta or its guid is null!");
            throw new BadRequestException("Param twitterCardMeta object or its guid is null!");
        }
        TwitterCardMetaDataObject dataObj = null;
        if(twitterCardMeta instanceof TwitterCardMetaDataObject) {
            dataObj = (TwitterCardMetaDataObject) twitterCardMeta;
        } else if(twitterCardMeta instanceof TwitterCardMetaBean) {
            dataObj = ((TwitterCardMetaBean) twitterCardMeta).toDataObject();
        } else {  // if(twitterCardMeta instanceof TwitterCardMeta)
            dataObj = new TwitterCardMetaDataObject(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), twitterCardMeta.getSummaryCard(), twitterCardMeta.getPhotoCard(), twitterCardMeta.getGalleryCard(), twitterCardMeta.getAppCard(), twitterCardMeta.getPlayerCard(), twitterCardMeta.getProductCard());
        }
        Boolean suc = getDAOFactory().getTwitterCardMetaDAO().deleteTwitterCardMeta(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getTwitterCardMetaDAO().deleteTwitterCardMetas(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
