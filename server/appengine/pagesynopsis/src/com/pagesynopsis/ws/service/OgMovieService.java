package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgMovieService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgMovie getOgMovie(String guid) throws BaseException;
    Object getOgMovie(String guid, String field) throws BaseException;
    List<OgMovie> getOgMovies(List<String> guids) throws BaseException;
    List<OgMovie> getAllOgMovies() throws BaseException;
    /* @Deprecated */ List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count) throws BaseException;
    List<OgMovie> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //String createOgMovie(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgMovie?)
    String createOgMovie(OgMovie ogMovie) throws BaseException;          // Returns Guid.  (Return OgMovie?)
    Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //Boolean updateOgMovie(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgMovie(OgMovie ogMovie) throws BaseException;
    Boolean deleteOgMovie(String guid) throws BaseException;
    Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException;
    Long deleteOgMovies(String filter, String params, List<String> values) throws BaseException;

//    Integer createOgMovies(List<OgMovie> ogMovies) throws BaseException;
//    Boolean updateeOgMovies(List<OgMovie> ogMovies) throws BaseException;

}
