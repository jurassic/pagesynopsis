package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgTvShowBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgTvShowService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgTvShowServiceImpl implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgTvShow related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgTvShow getOgTvShow(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgTvShowDataObject dataObj = getDAOFactory().getOgTvShowDAO().getOgTvShow(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgTvShowDataObject for guid = " + guid);
            return null;  // ????
        }
        OgTvShowBean bean = new OgTvShowBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgTvShow(String guid, String field) throws BaseException
    {
        OgTvShowDataObject dataObj = getDAOFactory().getOgTvShowDAO().getOgTvShow(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgTvShowDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("director")) {
            return dataObj.getDirector();
        } else if(field.equals("writer")) {
            return dataObj.getWriter();
        } else if(field.equals("actor")) {
            return dataObj.getActor();
        } else if(field.equals("duration")) {
            return dataObj.getDuration();
        } else if(field.equals("tag")) {
            return dataObj.getTag();
        } else if(field.equals("releaseDate")) {
            return dataObj.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgTvShow> getOgTvShows(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgTvShow> list = new ArrayList<OgTvShow>();
        List<OgTvShowDataObject> dataObjs = getDAOFactory().getOgTvShowDAO().getOgTvShows(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgTvShowDataObject list.");
        } else {
            Iterator<OgTvShowDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgTvShowDataObject dataObj = (OgTvShowDataObject) it.next();
                list.add(new OgTvShowBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgTvShow> getAllOgTvShows() throws BaseException
    {
        return getAllOgTvShows(null, null, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    @Override
    public List<OgTvShow> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgTvShows(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgTvShow> list = new ArrayList<OgTvShow>();
        List<OgTvShowDataObject> dataObjs = getDAOFactory().getOgTvShowDAO().getAllOgTvShows(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgTvShowDataObject list.");
        } else {
            Iterator<OgTvShowDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgTvShowDataObject dataObj = (OgTvShowDataObject) it.next();
                list.add(new OgTvShowBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgTvShowKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgTvShowDAO().getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgTvShow key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgTvShow> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgTvShowServiceImpl.findOgTvShows(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgTvShow> list = new ArrayList<OgTvShow>();
        List<OgTvShowDataObject> dataObjs = getDAOFactory().getOgTvShowDAO().findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogTvShows for the given criterion.");
        } else {
            Iterator<OgTvShowDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgTvShowDataObject dataObj = (OgTvShowDataObject) it.next();
                list.add(new OgTvShowBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgTvShowServiceImpl.findOgTvShowKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgTvShowDAO().findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgTvShow keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgTvShowServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgTvShowDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgTvShowDataObject dataObj = new OgTvShowDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgTvShow(dataObj);
    }

    @Override
    public String createOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogTvShow cannot be null.....
        if(ogTvShow == null) {
            log.log(Level.INFO, "Param ogTvShow is null!");
            throw new BadRequestException("Param ogTvShow object is null!");
        }
        OgTvShowDataObject dataObj = null;
        if(ogTvShow instanceof OgTvShowDataObject) {
            dataObj = (OgTvShowDataObject) ogTvShow;
        } else if(ogTvShow instanceof OgTvShowBean) {
            dataObj = ((OgTvShowBean) ogTvShow).toDataObject();
        } else {  // if(ogTvShow instanceof OgTvShow)
            //dataObj = new OgTvShowDataObject(null, ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate());
        }
        String guid = getDAOFactory().getOgTvShowDAO().createOgTvShow(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgTvShowDataObject dataObj = new OgTvShowDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgTvShow(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogTvShow cannot be null.....
        if(ogTvShow == null || ogTvShow.getGuid() == null) {
            log.log(Level.INFO, "Param ogTvShow or its guid is null!");
            throw new BadRequestException("Param ogTvShow object or its guid is null!");
        }
        OgTvShowDataObject dataObj = null;
        if(ogTvShow instanceof OgTvShowDataObject) {
            dataObj = (OgTvShowDataObject) ogTvShow;
        } else if(ogTvShow instanceof OgTvShowBean) {
            dataObj = ((OgTvShowBean) ogTvShow).toDataObject();
        } else {  // if(ogTvShow instanceof OgTvShow)
            dataObj = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgTvShowDAO().updateOgTvShow(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgTvShow(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgTvShowDAO().deleteOgTvShow(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgTvShow(OgTvShow ogTvShow) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogTvShow cannot be null.....
        if(ogTvShow == null || ogTvShow.getGuid() == null) {
            log.log(Level.INFO, "Param ogTvShow or its guid is null!");
            throw new BadRequestException("Param ogTvShow object or its guid is null!");
        }
        OgTvShowDataObject dataObj = null;
        if(ogTvShow instanceof OgTvShowDataObject) {
            dataObj = (OgTvShowDataObject) ogTvShow;
        } else if(ogTvShow instanceof OgTvShowBean) {
            dataObj = ((OgTvShowBean) ogTvShow).toDataObject();
        } else {  // if(ogTvShow instanceof OgTvShow)
            dataObj = new OgTvShowDataObject(ogTvShow.getGuid(), ogTvShow.getUrl(), ogTvShow.getType(), ogTvShow.getSiteName(), ogTvShow.getTitle(), ogTvShow.getDescription(), ogTvShow.getFbAdmins(), ogTvShow.getFbAppId(), ogTvShow.getImage(), ogTvShow.getAudio(), ogTvShow.getVideo(), ogTvShow.getLocale(), ogTvShow.getLocaleAlternate(), ogTvShow.getDirector(), ogTvShow.getWriter(), ogTvShow.getActor(), ogTvShow.getDuration(), ogTvShow.getTag(), ogTvShow.getReleaseDate());
        }
        Boolean suc = getDAOFactory().getOgTvShowDAO().deleteOgTvShow(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgTvShows(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgTvShowDAO().deleteOgTvShows(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
