package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgVideoService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgVideo getOgVideo(String guid) throws BaseException;
    Object getOgVideo(String guid, String field) throws BaseException;
    List<OgVideo> getOgVideos(List<String> guids) throws BaseException;
    List<OgVideo> getAllOgVideos() throws BaseException;
    /* @Deprecated */ List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException;
    List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgVideo> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //String createOgVideo(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgVideo?)
    String createOgVideo(OgVideo ogVideo) throws BaseException;          // Returns Guid.  (Return OgVideo?)
    Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException;
    //Boolean updateOgVideo(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgVideo(OgVideo ogVideo) throws BaseException;
    Boolean deleteOgVideo(String guid) throws BaseException;
    Boolean deleteOgVideo(OgVideo ogVideo) throws BaseException;
    Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException;

//    Integer createOgVideos(List<OgVideo> ogVideos) throws BaseException;
//    Boolean updateeOgVideos(List<OgVideo> ogVideos) throws BaseException;

}
