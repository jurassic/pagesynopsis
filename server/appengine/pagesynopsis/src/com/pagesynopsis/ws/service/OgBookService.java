package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgBookService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgBook getOgBook(String guid) throws BaseException;
    Object getOgBook(String guid, String field) throws BaseException;
    List<OgBook> getOgBooks(List<String> guids) throws BaseException;
    List<OgBook> getAllOgBooks() throws BaseException;
    /* @Deprecated */ List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException;
    List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgBook(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException;
    //String createOgBook(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgBook?)
    String createOgBook(OgBook ogBook) throws BaseException;          // Returns Guid.  (Return OgBook?)
    Boolean updateOgBook(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String isbn, List<String> tag, String releaseDate) throws BaseException;
    //Boolean updateOgBook(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgBook(OgBook ogBook) throws BaseException;
    Boolean deleteOgBook(String guid) throws BaseException;
    Boolean deleteOgBook(OgBook ogBook) throws BaseException;
    Long deleteOgBooks(String filter, String params, List<String> values) throws BaseException;

//    Integer createOgBooks(List<OgBook> ogBooks) throws BaseException;
//    Boolean updateeOgBooks(List<OgBook> ogBooks) throws BaseException;

}
