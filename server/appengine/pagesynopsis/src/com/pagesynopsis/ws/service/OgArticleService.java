package com.pagesynopsis.ws.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgArticleService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgArticle getOgArticle(String guid) throws BaseException;
    Object getOgArticle(String guid, String field) throws BaseException;
    List<OgArticle> getOgArticles(List<String> guids) throws BaseException;
    List<OgArticle> getAllOgArticles() throws BaseException;
    /* @Deprecated */ List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException;
    List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException;
    //String createOgArticle(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgArticle?)
    String createOgArticle(OgArticle ogArticle) throws BaseException;          // Returns Guid.  (Return OgArticle?)
    Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException;
    //Boolean updateOgArticle(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgArticle(OgArticle ogArticle) throws BaseException;
    Boolean deleteOgArticle(String guid) throws BaseException;
    Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException;
    Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException;

//    Integer createOgArticles(List<OgArticle> ogArticles) throws BaseException;
//    Boolean updateeOgArticles(List<OgArticle> ogArticles) throws BaseException;

}
