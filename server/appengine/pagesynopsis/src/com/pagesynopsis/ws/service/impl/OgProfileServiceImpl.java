package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.bean.OgAudioStructBean;
import com.pagesynopsis.ws.bean.OgImageStructBean;
import com.pagesynopsis.ws.bean.OgActorStructBean;
import com.pagesynopsis.ws.bean.OgVideoStructBean;
import com.pagesynopsis.ws.bean.OgProfileBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.OgProfileService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgProfileServiceImpl implements OgProfileService
{
    private static final Logger log = Logger.getLogger(OgProfileServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // OgProfile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgProfileDataObject dataObj = getDAOFactory().getOgProfileDAO().getOgProfile(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgProfileDataObject for guid = " + guid);
            return null;  // ????
        }
        OgProfileBean bean = new OgProfileBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        OgProfileDataObject dataObj = getDAOFactory().getOgProfileDAO().getOgProfile(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgProfileDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return dataObj.getUrl();
        } else if(field.equals("type")) {
            return dataObj.getType();
        } else if(field.equals("siteName")) {
            return dataObj.getSiteName();
        } else if(field.equals("title")) {
            return dataObj.getTitle();
        } else if(field.equals("description")) {
            return dataObj.getDescription();
        } else if(field.equals("fbAdmins")) {
            return dataObj.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return dataObj.getFbAppId();
        } else if(field.equals("image")) {
            return dataObj.getImage();
        } else if(field.equals("audio")) {
            return dataObj.getAudio();
        } else if(field.equals("video")) {
            return dataObj.getVideo();
        } else if(field.equals("locale")) {
            return dataObj.getLocale();
        } else if(field.equals("localeAlternate")) {
            return dataObj.getLocaleAlternate();
        } else if(field.equals("profileId")) {
            return dataObj.getProfileId();
        } else if(field.equals("firstName")) {
            return dataObj.getFirstName();
        } else if(field.equals("lastName")) {
            return dataObj.getLastName();
        } else if(field.equals("username")) {
            return dataObj.getUsername();
        } else if(field.equals("gender")) {
            return dataObj.getGender();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<OgProfile> list = new ArrayList<OgProfile>();
        List<OgProfileDataObject> dataObjs = getDAOFactory().getOgProfileDAO().getOgProfiles(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgProfileDataObject list.");
        } else {
            Iterator<OgProfileDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgProfileDataObject dataObj = (OgProfileDataObject) it.next();
                list.add(new OgProfileBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfiles(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgProfile> list = new ArrayList<OgProfile>();
        List<OgProfileDataObject> dataObjs = getDAOFactory().getOgProfileDAO().getAllOgProfiles(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve OgProfileDataObject list.");
        } else {
            Iterator<OgProfileDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgProfileDataObject dataObj = (OgProfileDataObject) it.next();
                list.add(new OgProfileBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllOgProfileKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgProfileDAO().getAllOgProfileKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgProfile key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgProfileServiceImpl.findOgProfiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgProfile> list = new ArrayList<OgProfile>();
        List<OgProfileDataObject> dataObjs = getDAOFactory().getOgProfileDAO().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find ogProfiles for the given criterion.");
        } else {
            Iterator<OgProfileDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                OgProfileDataObject dataObj = (OgProfileDataObject) it.next();
                list.add(new OgProfileBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgProfileServiceImpl.findOgProfileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getOgProfileDAO().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgProfile keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("OgProfileServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getOgProfileDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        OgProfileDataObject dataObj = new OgProfileDataObject(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
        return createOgProfile(dataObj);
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogProfile cannot be null.....
        if(ogProfile == null) {
            log.log(Level.INFO, "Param ogProfile is null!");
            throw new BadRequestException("Param ogProfile object is null!");
        }
        OgProfileDataObject dataObj = null;
        if(ogProfile instanceof OgProfileDataObject) {
            dataObj = (OgProfileDataObject) ogProfile;
        } else if(ogProfile instanceof OgProfileBean) {
            dataObj = ((OgProfileBean) ogProfile).toDataObject();
        } else {  // if(ogProfile instanceof OgProfile)
            //dataObj = new OgProfileDataObject(null, ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        }
        String guid = getDAOFactory().getOgProfileDAO().createOgProfile(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgProfileDataObject dataObj = new OgProfileDataObject(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
        return updateOgProfile(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogProfile cannot be null.....
        if(ogProfile == null || ogProfile.getGuid() == null) {
            log.log(Level.INFO, "Param ogProfile or its guid is null!");
            throw new BadRequestException("Param ogProfile object or its guid is null!");
        }
        OgProfileDataObject dataObj = null;
        if(ogProfile instanceof OgProfileDataObject) {
            dataObj = (OgProfileDataObject) ogProfile;
        } else if(ogProfile instanceof OgProfileBean) {
            dataObj = ((OgProfileBean) ogProfile).toDataObject();
        } else {  // if(ogProfile instanceof OgProfile)
            dataObj = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        }
        Boolean suc = getDAOFactory().getOgProfileDAO().updateOgProfile(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getOgProfileDAO().deleteOgProfile(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogProfile cannot be null.....
        if(ogProfile == null || ogProfile.getGuid() == null) {
            log.log(Level.INFO, "Param ogProfile or its guid is null!");
            throw new BadRequestException("Param ogProfile object or its guid is null!");
        }
        OgProfileDataObject dataObj = null;
        if(ogProfile instanceof OgProfileDataObject) {
            dataObj = (OgProfileDataObject) ogProfile;
        } else if(ogProfile instanceof OgProfileBean) {
            dataObj = ((OgProfileBean) ogProfile).toDataObject();
        } else {  // if(ogProfile instanceof OgProfile)
            dataObj = new OgProfileDataObject(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        }
        Boolean suc = getDAOFactory().getOgProfileDAO().deleteOgProfile(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getOgProfileDAO().deleteOgProfiles(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
