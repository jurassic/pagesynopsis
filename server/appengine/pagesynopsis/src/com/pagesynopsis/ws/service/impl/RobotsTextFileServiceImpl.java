package com.pagesynopsis.ws.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.bean.RobotsTextGroupBean;
import com.pagesynopsis.ws.bean.RobotsTextFileBean;
import com.pagesynopsis.ws.dao.DAOFactory;
import com.pagesynopsis.ws.data.RobotsTextGroupDataObject;
import com.pagesynopsis.ws.data.RobotsTextFileDataObject;
import com.pagesynopsis.ws.service.DAOFactoryManager;
import com.pagesynopsis.ws.service.RobotsTextFileService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RobotsTextFileServiceImpl implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(RobotsTextFileServiceImpl.class.getName());
    //private static final DAOFactory daoFactory = DAOFactoryManager.getDAOFactory();

    private static DAOFactory getDAOFactory()
    {
        return DAOFactoryManager.getDAOFactory();
    }

    
    //////////////////////////////////////////////////////////////////////////
    // RobotsTextFile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        RobotsTextFileDataObject dataObj = getDAOFactory().getRobotsTextFileDAO().getRobotsTextFile(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextFileDataObject for guid = " + guid);
            return null;  // ????
        }
        RobotsTextFileBean bean = new RobotsTextFileBean(dataObj);

        log.finer("END");
        return bean;
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        RobotsTextFileDataObject dataObj = getDAOFactory().getRobotsTextFileDAO().getRobotsTextFile(guid);
        if(dataObj == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve RobotsTextFileDataObject for guid = " + guid);
            return null;  // ????
        }
        
        // TBD
        if(field.equals("guid")) {
            return dataObj.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("siteUrl")) {
            return dataObj.getSiteUrl();
        } else if(field.equals("groups")) {
            return dataObj.getGroups();
        } else if(field.equals("sitemaps")) {
            return dataObj.getSitemaps();
        } else if(field.equals("content")) {
            return dataObj.getContent();
        } else if(field.equals("contentHash")) {
            return dataObj.getContentHash();
        } else if(field.equals("lastCheckedTime")) {
            return dataObj.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return dataObj.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return dataObj.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return dataObj.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        log.finer("BEGIN");

        // TBD: Is there a better way????
        List<RobotsTextFile> list = new ArrayList<RobotsTextFile>();
        List<RobotsTextFileDataObject> dataObjs = getDAOFactory().getRobotsTextFileDAO().getRobotsTextFiles(guids);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextFileDataObject list.");
        } else {
            Iterator<RobotsTextFileDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RobotsTextFileDataObject dataObj = (RobotsTextFileDataObject) it.next();
                list.add(new RobotsTextFileBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllRobotsTextFiles(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextFile> list = new ArrayList<RobotsTextFile>();
        List<RobotsTextFileDataObject> dataObjs = getDAOFactory().getRobotsTextFileDAO().getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextFileDataObject list.");
        } else {
            Iterator<RobotsTextFileDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RobotsTextFileDataObject dataObj = (RobotsTextFileDataObject) it.next();
                list.add(new RobotsTextFileBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer(" getAllRobotsTextFileKeys(): ordering=" + ordering + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getRobotsTextFileDAO().getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve RobotsTextFile key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }
    
    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    
    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RobotsTextFileServiceImpl.findRobotsTextFiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<RobotsTextFile> list = new ArrayList<RobotsTextFile>();
        List<RobotsTextFileDataObject> dataObjs = getDAOFactory().getRobotsTextFileDAO().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(dataObjs == null) {
            log.log(Level.WARNING, "Failed to find robotsTextFiles for the given criterion.");
        } else {
            Iterator<RobotsTextFileDataObject> it = dataObjs.iterator();
            while(it.hasNext()) {
                RobotsTextFileDataObject dataObj = (RobotsTextFileDataObject) it.next();
                list.add(new RobotsTextFileBean(dataObj));
            }
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RobotsTextFileServiceImpl.findRobotsTextFileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        List<String> keys = getDAOFactory().getRobotsTextFileDAO().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find RobotsTextFile keys for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINE)) log.fine("RobotsTextFileServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getDAOFactory().getRobotsTextFileDAO().getCount(filter, params, values, aggregate);

        log.finer("END");
        return count;
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }
        
        
        RobotsTextFileDataObject dataObj = new RobotsTextFileDataObject(null, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        return createRobotsTextFile(dataObj);
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null) {
            log.log(Level.INFO, "Param robotsTextFile is null!");
            throw new BadRequestException("Param robotsTextFile object is null!");
        }
        RobotsTextFileDataObject dataObj = null;
        if(robotsTextFile instanceof RobotsTextFileDataObject) {
            dataObj = (RobotsTextFileDataObject) robotsTextFile;
        } else if(robotsTextFile instanceof RobotsTextFileBean) {
            dataObj = ((RobotsTextFileBean) robotsTextFile).toDataObject();
        } else {  // if(robotsTextFile instanceof RobotsTextFile)
            //dataObj = new RobotsTextFileDataObject(null, robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            dataObj = new RobotsTextFileDataObject(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        }
        String guid = getDAOFactory().getRobotsTextFileDAO().createRobotsTextFile(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {

        // Note: We can retrive the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        RobotsTextFileDataObject dataObj = new RobotsTextFileDataObject(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        return updateRobotsTextFile(dataObj);
    }
        
    // ???
    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null || robotsTextFile.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextFile or its guid is null!");
            throw new BadRequestException("Param robotsTextFile object or its guid is null!");
        }
        RobotsTextFileDataObject dataObj = null;
        if(robotsTextFile instanceof RobotsTextFileDataObject) {
            dataObj = (RobotsTextFileDataObject) robotsTextFile;
        } else if(robotsTextFile instanceof RobotsTextFileBean) {
            dataObj = ((RobotsTextFileBean) robotsTextFile).toDataObject();
        } else {  // if(robotsTextFile instanceof RobotsTextFile)
            dataObj = new RobotsTextFileDataObject(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getRobotsTextFileDAO().updateRobotsTextFile(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        Boolean suc = getDAOFactory().getRobotsTextFileDAO().deleteRobotsTextFile(guid);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }
    
    // ???
    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        // Param robotsTextFile cannot be null.....
        if(robotsTextFile == null || robotsTextFile.getGuid() == null) {
            log.log(Level.INFO, "Param robotsTextFile or its guid is null!");
            throw new BadRequestException("Param robotsTextFile object or its guid is null!");
        }
        RobotsTextFileDataObject dataObj = null;
        if(robotsTextFile instanceof RobotsTextFileDataObject) {
            dataObj = (RobotsTextFileDataObject) robotsTextFile;
        } else if(robotsTextFile instanceof RobotsTextFileBean) {
            dataObj = ((RobotsTextFileBean) robotsTextFile).toDataObject();
        } else {  // if(robotsTextFile instanceof RobotsTextFile)
            dataObj = new RobotsTextFileDataObject(robotsTextFile.getGuid(), robotsTextFile.getSiteUrl(), robotsTextFile.getGroups(), robotsTextFile.getSitemaps(), robotsTextFile.getContent(), robotsTextFile.getContentHash(), robotsTextFile.getLastCheckedTime(), robotsTextFile.getLastUpdatedTime());
        }
        Boolean suc = getDAOFactory().getRobotsTextFileDAO().deleteRobotsTextFile(dataObj);

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = getDAOFactory().getRobotsTextFileDAO().deleteRobotsTextFiles(filter, params, values);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
