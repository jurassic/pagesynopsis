package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgProfile extends OgObjectBase
{
    String  getProfileId();
    String  getFirstName();
    String  getLastName();
    String  getUsername();
    String  getGender();
}
