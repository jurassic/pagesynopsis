package com.pagesynopsis.ws.platform.gae;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.platform.PlatformServiceFactory;
import com.pagesynopsis.ws.platform.AppIdentityPlatformService;
import com.pagesynopsis.ws.platform.ConfigPlatformService;
import com.pagesynopsis.ws.platform.UserPlatformService;
import com.pagesynopsis.ws.platform.OAuthPlatformService;
import com.pagesynopsis.ws.platform.MemcachePlatformService;
import com.pagesynopsis.ws.platform.MailPlatformService;
import com.pagesynopsis.ws.platform.MessagingPlatformService;


public class GaePlatformServiceFactory extends PlatformServiceFactory
{
    private static final Logger log = Logger.getLogger(GaePlatformServiceFactory.class.getName());

    private GaePlatformServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class GaePlatformServiceFactoryHolder
    {
        private static final GaePlatformServiceFactory INSTANCE = new GaePlatformServiceFactory();
    }

    // Singleton method
    public static GaePlatformServiceFactory getInstance()
    {
        return GaePlatformServiceFactoryHolder.INSTANCE;
    }


    // Platform Services

    public AppIdentityPlatformService getAppIdentityPlatformService()
    {
        return GaeAppIdentityPlatformService.getInstance();
    }

    public ConfigPlatformService getConfigPlatformService()
    {
        return new GaeConfigPlatformService();
    }

    public UserPlatformService getUserPlatformService()
    {
        return GaeUserPlatformService.getInstance();
    }

    public OAuthPlatformService getOAuthPlatformService()
    {
        return GaeOAuthPlatformService.getInstance();
    }

    public MemcachePlatformService getMemcachePlatformService()
    {
        return new GaeMemcachePlatformService();
    }

    public MailPlatformService getMailPlatformService()
    {
        return new GaeMailPlatformService();
    }

    public MessagingPlatformService getMessagingPlatformService()
    {
        return new GaeMessagingPlatformService();
    }


}
