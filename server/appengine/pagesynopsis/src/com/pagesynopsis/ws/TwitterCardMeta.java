package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface TwitterCardMeta extends PageBase
{
    String  getCardType();
    TwitterSummaryCard  getSummaryCard();
    TwitterPhotoCard  getPhotoCard();
    TwitterGalleryCard  getGalleryCard();
    TwitterAppCard  getAppCard();
    TwitterPlayerCard  getPlayerCard();
    TwitterProductCard  getProductCard();
}
