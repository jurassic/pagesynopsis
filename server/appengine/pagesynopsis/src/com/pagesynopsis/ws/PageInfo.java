package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface PageInfo extends PageBase
{
    String  getPageAuthor();
    String  getPageDescription();
    String  getFavicon();
    String  getFaviconUrl();
}
