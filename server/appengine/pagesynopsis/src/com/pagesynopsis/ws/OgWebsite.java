package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgWebsite extends OgObjectBase
{
    String  getNote();
}
