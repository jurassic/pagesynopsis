package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgBook extends OgObjectBase
{
    List<String>  getAuthor();
    String  getIsbn();
    List<String>  getTag();
    String  getReleaseDate();
}
