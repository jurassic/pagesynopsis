package com.pagesynopsis.ws;



public interface UrlStruct 
{
    String  getUuid();
    Integer  getStatusCode();
    String  getRedirectUrl();
    String  getAbsoluteUrl();
    String  getHashFragment();
    String  getNote();
    boolean isEmpty();
}
