package com.pagesynopsis.ws;



public interface AnchorStruct 
{
    String  getUuid();
    String  getId();
    String  getName();
    String  getHref();
    String  getHrefUrl();
    String  getUrlScheme();
    String  getRel();
    String  getTarget();
    String  getInnerHtml();
    String  getAnchorText();
    String  getAnchorTitle();
    String  getAnchorImage();
    String  getAnchorLegend();
    String  getNote();
    boolean isEmpty();
}
