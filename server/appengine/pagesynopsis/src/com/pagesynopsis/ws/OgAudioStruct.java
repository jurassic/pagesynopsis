package com.pagesynopsis.ws;



public interface OgAudioStruct 
{
    String  getUuid();
    String  getUrl();
    String  getSecureUrl();
    String  getType();
    boolean isEmpty();
}
