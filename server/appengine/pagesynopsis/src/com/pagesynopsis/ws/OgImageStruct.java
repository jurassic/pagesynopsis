package com.pagesynopsis.ws;



public interface OgImageStruct 
{
    String  getUuid();
    String  getUrl();
    String  getSecureUrl();
    String  getType();
    Integer  getWidth();
    Integer  getHeight();
    boolean isEmpty();
}
