package com.pagesynopsis.ws;



public interface GeoCoordinateStruct extends GeoPointStruct
{
    String  getAccuracy();
    String  getAltitudeAccuracy();
    Double  getHeading();
    Double  getSpeed();
    String  getNote();
    boolean isEmpty();
}
