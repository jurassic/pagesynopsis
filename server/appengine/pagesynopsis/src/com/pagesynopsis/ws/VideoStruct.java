package com.pagesynopsis.ws;



public interface VideoStruct 
{
    String  getUuid();
    String  getId();
    Integer  getWidth();
    Integer  getHeight();
    String  getControls();
    Boolean  isAutoplayEnabled();
    Boolean  isLoopEnabled();
    Boolean  isPreloadEnabled();
    Boolean  isMuted();
    String  getRemark();
    MediaSourceStruct  getSource();
    MediaSourceStruct  getSource1();
    MediaSourceStruct  getSource2();
    MediaSourceStruct  getSource3();
    MediaSourceStruct  getSource4();
    MediaSourceStruct  getSource5();
    String  getNote();
    boolean isEmpty();
}
