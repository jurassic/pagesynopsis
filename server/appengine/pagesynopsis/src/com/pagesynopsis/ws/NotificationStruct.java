package com.pagesynopsis.ws;



public interface NotificationStruct 
{
    String  getPreferredMode();
    String  getMobileNumber();
    String  getEmailAddress();
    String  getTwitterUsername();
    Long  getFacebookId();
    String  getLinkedinId();
    String  getNote();
    boolean isEmpty();
}
