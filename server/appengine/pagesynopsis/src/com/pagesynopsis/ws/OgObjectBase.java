package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;


public interface OgObjectBase 
{
    String  getGuid();
    String  getUrl();
    String  getType();
    String  getSiteName();
    String  getTitle();
    String  getDescription();
    List<String>  getFbAdmins();
    List<String>  getFbAppId();
    List<OgImageStruct>  getImage();
    List<OgAudioStruct>  getAudio();
    List<OgVideoStruct>  getVideo();
    String  getLocale();
    List<String>  getLocaleAlternate();
    Long  getCreatedTime();
    Long  getModifiedTime();
}
