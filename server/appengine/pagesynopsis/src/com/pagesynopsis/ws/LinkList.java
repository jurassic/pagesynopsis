package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface LinkList extends PageBase
{
    List<String>  getUrlSchemeFilter();
    List<AnchorStruct>  getPageAnchors();
    Boolean  isExcludeRelativeUrls();
    List<String>  getExcludedBaseUrls();
}
