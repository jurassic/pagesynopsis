package com.pagesynopsis.ws;



public interface OgContactInfoStruct 
{
    String  getUuid();
    String  getStreetAddress();
    String  getLocality();
    String  getRegion();
    String  getPostalCode();
    String  getCountryName();
    String  getEmailAddress();
    String  getPhoneNumber();
    String  getFaxNumber();
    String  getWebsite();
    boolean isEmpty();
}
