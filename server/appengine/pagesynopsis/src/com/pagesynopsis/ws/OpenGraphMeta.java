package com.pagesynopsis.ws;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;


public interface OpenGraphMeta extends PageBase
{
    String  getOgType();
    OgProfile  getOgProfile();
    OgWebsite  getOgWebsite();
    OgBlog  getOgBlog();
    OgArticle  getOgArticle();
    OgBook  getOgBook();
    OgVideo  getOgVideo();
    OgMovie  getOgMovie();
    OgTvShow  getOgTvShow();
    OgTvEpisode  getOgTvEpisode();
}
