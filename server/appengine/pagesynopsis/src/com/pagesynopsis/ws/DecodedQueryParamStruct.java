package com.pagesynopsis.ws;



public interface DecodedQueryParamStruct 
{
    String  getParamType();
    String  getOriginalString();
    String  getDecodedString();
    String  getNote();
    boolean isEmpty();
}
