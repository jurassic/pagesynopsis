package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.data.UrlRatingDataObject;

public class UrlRatingBean extends BeanBase implements UrlRating
{
    private static final Logger log = Logger.getLogger(UrlRatingBean.class.getName());

    // Embedded data object.
    private UrlRatingDataObject dobj = null;

    public UrlRatingBean()
    {
        this(new UrlRatingDataObject());
    }
    public UrlRatingBean(String guid)
    {
        this(new UrlRatingDataObject(guid));
    }
    public UrlRatingBean(UrlRatingDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public UrlRatingDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public String getDomain()
    {
        if(getDataObject() != null) {
            return getDataObject().getDomain();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setDomain(String domain)
    {
        if(getDataObject() != null) {
            getDataObject().setDomain(domain);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public String getPageUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageUrl(String pageUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setPageUrl(pageUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public String getPreview()
    {
        if(getDataObject() != null) {
            return getDataObject().getPreview();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreview(String preview)
    {
        if(getDataObject() != null) {
            getDataObject().setPreview(preview);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public String getFlag()
    {
        if(getDataObject() != null) {
            return getDataObject().getFlag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setFlag(String flag)
    {
        if(getDataObject() != null) {
            getDataObject().setFlag(flag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public Double getRating()
    {
        if(getDataObject() != null) {
            return getDataObject().getRating();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setRating(Double rating)
    {
        if(getDataObject() != null) {
            getDataObject().setRating(rating);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }

    public Long getRatedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getRatedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
            return null;   // ???
        }
    }
    public void setRatedTime(Long ratedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setRatedTime(ratedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlRatingDataObject is null!");
        }
    }


    // TBD
    public UrlRatingDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
