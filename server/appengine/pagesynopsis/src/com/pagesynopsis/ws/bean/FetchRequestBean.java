package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.data.NotificationStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.ReferrerInfoStructDataObject;
import com.pagesynopsis.ws.data.FetchRequestDataObject;

public class FetchRequestBean extends BeanBase implements FetchRequest
{
    private static final Logger log = Logger.getLogger(FetchRequestBean.class.getName());

    // Embedded data object.
    private FetchRequestDataObject dobj = null;

    public FetchRequestBean()
    {
        this(new FetchRequestDataObject());
    }
    public FetchRequestBean(String guid)
    {
        this(new FetchRequestDataObject(guid));
    }
    public FetchRequestBean(FetchRequestDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public FetchRequestDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getUser()
    {
        if(getDataObject() != null) {
            return getDataObject().getUser();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setUser(String user)
    {
        if(getDataObject() != null) {
            getDataObject().setUser(user);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getTargetUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getTargetUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setTargetUrl(String targetUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setTargetUrl(targetUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getPageUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getPageUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setPageUrl(String pageUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setPageUrl(pageUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getQueryString()
    {
        if(getDataObject() != null) {
            return getDataObject().getQueryString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setQueryString(String queryString)
    {
        if(getDataObject() != null) {
            getDataObject().setQueryString(queryString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        if(getDataObject() != null) {
            List<KeyValuePairStruct> list = getDataObject().getQueryParams();
            if(list != null) {
                List<KeyValuePairStruct> bean = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : list) {
                    KeyValuePairStructBean elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = (KeyValuePairStructBean) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStructDataObject) {
                        elem = new KeyValuePairStructBean((KeyValuePairStructDataObject) keyValuePairStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;
        }
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        if(queryParams != null) {
            if(getDataObject() != null) {
                List<KeyValuePairStruct> dataObj = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                    KeyValuePairStructDataObject elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = ((KeyValuePairStructBean) keyValuePairStruct).toDataObject();
                    } else if(keyValuePairStruct instanceof KeyValuePairStructDataObject) {
                        elem = (KeyValuePairStructDataObject) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStruct) {
                        elem = new KeyValuePairStructDataObject(keyValuePairStruct.getUuid(), keyValuePairStruct.getKey(), keyValuePairStruct.getValue(), keyValuePairStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setQueryParams(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setQueryParams(queryParams);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            }
        }
    }

    public String getVersion()
    {
        if(getDataObject() != null) {
            return getDataObject().getVersion();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setVersion(String version)
    {
        if(getDataObject() != null) {
            getDataObject().setVersion(version);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getFetchObject()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchObject();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchObject(String fetchObject)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchObject(fetchObject);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getFetchStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getFetchStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setFetchStatus(fetchStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getResult()
    {
        if(getDataObject() != null) {
            return getDataObject().getResult();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setResult(String result)
    {
        if(getDataObject() != null) {
            getDataObject().setResult(result);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {
        if(getDataObject() != null) {
            ReferrerInfoStruct _field = getDataObject().getReferrerInfo();
            if(_field == null) {
                log.log(Level.INFO, "referrerInfo is null.");
                return null;
            } else {
                return new ReferrerInfoStructBean((ReferrerInfoStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getDataObject() != null) {
            getDataObject().setReferrerInfo(
                (referrerInfo instanceof ReferrerInfoStructBean) ?
                ((ReferrerInfoStructBean) referrerInfo).toDataObject() :
                ((referrerInfo instanceof ReferrerInfoStructDataObject) ? referrerInfo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public String getNextPageUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getNextPageUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextPageUrl(String nextPageUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setNextPageUrl(nextPageUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getFollowPages()
    {
        if(getDataObject() != null) {
            return getDataObject().getFollowPages();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setFollowPages(Integer followPages)
    {
        if(getDataObject() != null) {
            getDataObject().setFollowPages(followPages);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Integer getFollowDepth()
    {
        if(getDataObject() != null) {
            return getDataObject().getFollowDepth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setFollowDepth(Integer followDepth)
    {
        if(getDataObject() != null) {
            getDataObject().setFollowDepth(followDepth);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Boolean isCreateVersion()
    {
        if(getDataObject() != null) {
            return getDataObject().isCreateVersion();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setCreateVersion(Boolean createVersion)
    {
        if(getDataObject() != null) {
            getDataObject().setCreateVersion(createVersion);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Boolean isDeferred()
    {
        if(getDataObject() != null) {
            return getDataObject().isDeferred();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setDeferred(Boolean deferred)
    {
        if(getDataObject() != null) {
            getDataObject().setDeferred(deferred);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public Boolean isAlert()
    {
        if(getDataObject() != null) {
            return getDataObject().isAlert();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setAlert(Boolean alert)
    {
        if(getDataObject() != null) {
            getDataObject().setAlert(alert);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }

    public NotificationStruct getNotificationPref()
    {
        if(getDataObject() != null) {
            NotificationStruct _field = getDataObject().getNotificationPref();
            if(_field == null) {
                log.log(Level.INFO, "notificationPref is null.");
                return null;
            } else {
                return new NotificationStructBean((NotificationStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
            return null;   // ???
        }
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(getDataObject() != null) {
            getDataObject().setNotificationPref(
                (notificationPref instanceof NotificationStructBean) ?
                ((NotificationStructBean) notificationPref).toDataObject() :
                ((notificationPref instanceof NotificationStructDataObject) ? notificationPref : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded FetchRequestDataObject is null!");
        }
    }


    // TBD
    public FetchRequestDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
