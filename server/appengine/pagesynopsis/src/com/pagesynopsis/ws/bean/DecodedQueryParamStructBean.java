package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.ws.data.DecodedQueryParamStructDataObject;

public class DecodedQueryParamStructBean implements DecodedQueryParamStruct
{
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructBean.class.getName());

    // Embedded data object.
    private DecodedQueryParamStructDataObject dobj = null;

    public DecodedQueryParamStructBean()
    {
        this(new DecodedQueryParamStructDataObject());
    }
    public DecodedQueryParamStructBean(DecodedQueryParamStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public DecodedQueryParamStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getParamType()
    {
        if(getDataObject() != null) {
            return getDataObject().getParamType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setParamType(String paramType)
    {
        if(getDataObject() != null) {
            getDataObject().setParamType(paramType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
        }
    }

    public String getOriginalString()
    {
        if(getDataObject() != null) {
            return getDataObject().getOriginalString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setOriginalString(String originalString)
    {
        if(getDataObject() != null) {
            getDataObject().setOriginalString(originalString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
        }
    }

    public String getDecodedString()
    {
        if(getDataObject() != null) {
            return getDataObject().getDecodedString();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setDecodedString(String decodedString)
    {
        if(getDataObject() != null) {
            getDataObject().setDecodedString(decodedString);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded DecodedQueryParamStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getParamType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getOriginalString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDecodedString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public DecodedQueryParamStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
