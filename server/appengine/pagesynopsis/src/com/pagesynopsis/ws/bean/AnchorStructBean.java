package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.data.AnchorStructDataObject;

public class AnchorStructBean implements AnchorStruct
{
    private static final Logger log = Logger.getLogger(AnchorStructBean.class.getName());

    // Embedded data object.
    private AnchorStructDataObject dobj = null;

    public AnchorStructBean()
    {
        this(new AnchorStructDataObject());
    }
    public AnchorStructBean(AnchorStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public AnchorStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getId()
    {
        if(getDataObject() != null) {
            return getDataObject().getId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setId(String id)
    {
        if(getDataObject() != null) {
            getDataObject().setId(id);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getName()
    {
        if(getDataObject() != null) {
            return getDataObject().getName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setName(String name)
    {
        if(getDataObject() != null) {
            getDataObject().setName(name);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getHref()
    {
        if(getDataObject() != null) {
            return getDataObject().getHref();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHref(String href)
    {
        if(getDataObject() != null) {
            getDataObject().setHref(href);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getHrefUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getHrefUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHrefUrl(String hrefUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setHrefUrl(hrefUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getUrlScheme()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrlScheme();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrlScheme(String urlScheme)
    {
        if(getDataObject() != null) {
            getDataObject().setUrlScheme(urlScheme);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getRel()
    {
        if(getDataObject() != null) {
            return getDataObject().getRel();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRel(String rel)
    {
        if(getDataObject() != null) {
            getDataObject().setRel(rel);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getTarget()
    {
        if(getDataObject() != null) {
            return getDataObject().getTarget();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setTarget(String target)
    {
        if(getDataObject() != null) {
            getDataObject().setTarget(target);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getInnerHtml()
    {
        if(getDataObject() != null) {
            return getDataObject().getInnerHtml();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setInnerHtml(String innerHtml)
    {
        if(getDataObject() != null) {
            getDataObject().setInnerHtml(innerHtml);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getAnchorText()
    {
        if(getDataObject() != null) {
            return getDataObject().getAnchorText();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAnchorText(String anchorText)
    {
        if(getDataObject() != null) {
            getDataObject().setAnchorText(anchorText);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getAnchorTitle()
    {
        if(getDataObject() != null) {
            return getDataObject().getAnchorTitle();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAnchorTitle(String anchorTitle)
    {
        if(getDataObject() != null) {
            getDataObject().setAnchorTitle(anchorTitle);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getAnchorImage()
    {
        if(getDataObject() != null) {
            return getDataObject().getAnchorImage();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAnchorImage(String anchorImage)
    {
        if(getDataObject() != null) {
            getDataObject().setAnchorImage(anchorImage);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getAnchorLegend()
    {
        if(getDataObject() != null) {
            return getDataObject().getAnchorLegend();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAnchorLegend(String anchorLegend)
    {
        if(getDataObject() != null) {
            getDataObject().setAnchorLegend(anchorLegend);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AnchorStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHrefUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrlScheme() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTarget() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getInnerHtml() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorLegend() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public AnchorStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
