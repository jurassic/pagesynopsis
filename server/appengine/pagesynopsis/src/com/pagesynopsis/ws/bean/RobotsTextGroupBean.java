package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.data.RobotsTextGroupDataObject;

public class RobotsTextGroupBean implements RobotsTextGroup
{
    private static final Logger log = Logger.getLogger(RobotsTextGroupBean.class.getName());

    // Embedded data object.
    private RobotsTextGroupDataObject dobj = null;

    public RobotsTextGroupBean()
    {
        this(new RobotsTextGroupDataObject());
    }
    public RobotsTextGroupBean(RobotsTextGroupDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public RobotsTextGroupDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
        }
    }

    public String getUserAgent()
    {
        if(getDataObject() != null) {
            return getDataObject().getUserAgent();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
            return null;   // ???
        }
    }
    public void setUserAgent(String userAgent)
    {
        if(getDataObject() != null) {
            getDataObject().setUserAgent(userAgent);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
        }
    }

    public List<String> getAllows()
    {
        if(getDataObject() != null) {
            return getDataObject().getAllows();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
            return null;   // ???
        }
    }
    public void setAllows(List<String> allows)
    {
        if(getDataObject() != null) {
            getDataObject().setAllows(allows);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
        }
    }

    public List<String> getDisallows()
    {
        if(getDataObject() != null) {
            return getDataObject().getDisallows();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
            return null;   // ???
        }
    }
    public void setDisallows(List<String> disallows)
    {
        if(getDataObject() != null) {
            getDataObject().setDisallows(disallows);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextGroupDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAllows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisallows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public RobotsTextGroupDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
