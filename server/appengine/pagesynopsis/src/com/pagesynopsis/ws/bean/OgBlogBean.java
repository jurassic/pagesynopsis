package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;

public class OgBlogBean extends OgObjectBaseBean implements OgBlog
{
    private static final Logger log = Logger.getLogger(OgBlogBean.class.getName());

    // Embedded data object.
    private OgBlogDataObject dobj = null;

    public OgBlogBean()
    {
        this(new OgBlogDataObject());
    }
    public OgBlogBean(String guid)
    {
        this(new OgBlogDataObject(guid));
    }
    public OgBlogBean(OgBlogDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public OgBlogDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBlogDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgBlogDataObject is null!");
        }
    }


    // TBD
    public OgBlogDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
