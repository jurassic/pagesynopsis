package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.data.UrlStructDataObject;

public class UrlStructBean implements UrlStruct
{
    private static final Logger log = Logger.getLogger(UrlStructBean.class.getName());

    // Embedded data object.
    private UrlStructDataObject dobj = null;

    public UrlStructBean()
    {
        this(new UrlStructDataObject());
    }
    public UrlStructBean(UrlStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public UrlStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
        }
    }

    public Integer getStatusCode()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatusCode();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatusCode(Integer statusCode)
    {
        if(getDataObject() != null) {
            getDataObject().setStatusCode(statusCode);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
        }
    }

    public String getRedirectUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getRedirectUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRedirectUrl(String redirectUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setRedirectUrl(redirectUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
        }
    }

    public String getAbsoluteUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getAbsoluteUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAbsoluteUrl(String absoluteUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setAbsoluteUrl(absoluteUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
        }
    }

    public String getHashFragment()
    {
        if(getDataObject() != null) {
            return getDataObject().getHashFragment();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHashFragment(String hashFragment)
    {
        if(getDataObject() != null) {
            getDataObject().setHashFragment(hashFragment);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded UrlStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStatusCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAbsoluteUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashFragment() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public UrlStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
