package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.OpenGraphMetaDataObject;

public class OpenGraphMetaBean extends PageBaseBean implements OpenGraphMeta
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaBean.class.getName());

    // Embedded data object.
    private OpenGraphMetaDataObject dobj = null;

    public OpenGraphMetaBean()
    {
        this(new OpenGraphMetaDataObject());
    }
    public OpenGraphMetaBean(String guid)
    {
        this(new OpenGraphMetaDataObject(guid));
    }
    public OpenGraphMetaBean(OpenGraphMetaDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public OpenGraphMetaDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getOgType()
    {
        if(getDataObject() != null) {
            return getDataObject().getOgType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgType(String ogType)
    {
        if(getDataObject() != null) {
            getDataObject().setOgType(ogType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgProfile getOgProfile()
    {
        if(getDataObject() != null) {
            OgProfile _field = getDataObject().getOgProfile();
            if(_field == null) {
                log.log(Level.INFO, "ogProfile is null.");
                return null;
            } else {
                return new OgProfileBean((OgProfileDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgProfile(OgProfile ogProfile)
    {
        if(getDataObject() != null) {
            getDataObject().setOgProfile(
                (ogProfile instanceof OgProfileBean) ?
                ((OgProfileBean) ogProfile).toDataObject() :
                ((ogProfile instanceof OgProfileDataObject) ? ogProfile : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgWebsite getOgWebsite()
    {
        if(getDataObject() != null) {
            OgWebsite _field = getDataObject().getOgWebsite();
            if(_field == null) {
                log.log(Level.INFO, "ogWebsite is null.");
                return null;
            } else {
                return new OgWebsiteBean((OgWebsiteDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgWebsite(OgWebsite ogWebsite)
    {
        if(getDataObject() != null) {
            getDataObject().setOgWebsite(
                (ogWebsite instanceof OgWebsiteBean) ?
                ((OgWebsiteBean) ogWebsite).toDataObject() :
                ((ogWebsite instanceof OgWebsiteDataObject) ? ogWebsite : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgBlog getOgBlog()
    {
        if(getDataObject() != null) {
            OgBlog _field = getDataObject().getOgBlog();
            if(_field == null) {
                log.log(Level.INFO, "ogBlog is null.");
                return null;
            } else {
                return new OgBlogBean((OgBlogDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgBlog(OgBlog ogBlog)
    {
        if(getDataObject() != null) {
            getDataObject().setOgBlog(
                (ogBlog instanceof OgBlogBean) ?
                ((OgBlogBean) ogBlog).toDataObject() :
                ((ogBlog instanceof OgBlogDataObject) ? ogBlog : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgArticle getOgArticle()
    {
        if(getDataObject() != null) {
            OgArticle _field = getDataObject().getOgArticle();
            if(_field == null) {
                log.log(Level.INFO, "ogArticle is null.");
                return null;
            } else {
                return new OgArticleBean((OgArticleDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgArticle(OgArticle ogArticle)
    {
        if(getDataObject() != null) {
            getDataObject().setOgArticle(
                (ogArticle instanceof OgArticleBean) ?
                ((OgArticleBean) ogArticle).toDataObject() :
                ((ogArticle instanceof OgArticleDataObject) ? ogArticle : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgBook getOgBook()
    {
        if(getDataObject() != null) {
            OgBook _field = getDataObject().getOgBook();
            if(_field == null) {
                log.log(Level.INFO, "ogBook is null.");
                return null;
            } else {
                return new OgBookBean((OgBookDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgBook(OgBook ogBook)
    {
        if(getDataObject() != null) {
            getDataObject().setOgBook(
                (ogBook instanceof OgBookBean) ?
                ((OgBookBean) ogBook).toDataObject() :
                ((ogBook instanceof OgBookDataObject) ? ogBook : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgVideo getOgVideo()
    {
        if(getDataObject() != null) {
            OgVideo _field = getDataObject().getOgVideo();
            if(_field == null) {
                log.log(Level.INFO, "ogVideo is null.");
                return null;
            } else {
                return new OgVideoBean((OgVideoDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgVideo(OgVideo ogVideo)
    {
        if(getDataObject() != null) {
            getDataObject().setOgVideo(
                (ogVideo instanceof OgVideoBean) ?
                ((OgVideoBean) ogVideo).toDataObject() :
                ((ogVideo instanceof OgVideoDataObject) ? ogVideo : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgMovie getOgMovie()
    {
        if(getDataObject() != null) {
            OgMovie _field = getDataObject().getOgMovie();
            if(_field == null) {
                log.log(Level.INFO, "ogMovie is null.");
                return null;
            } else {
                return new OgMovieBean((OgMovieDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgMovie(OgMovie ogMovie)
    {
        if(getDataObject() != null) {
            getDataObject().setOgMovie(
                (ogMovie instanceof OgMovieBean) ?
                ((OgMovieBean) ogMovie).toDataObject() :
                ((ogMovie instanceof OgMovieDataObject) ? ogMovie : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgTvShow getOgTvShow()
    {
        if(getDataObject() != null) {
            OgTvShow _field = getDataObject().getOgTvShow();
            if(_field == null) {
                log.log(Level.INFO, "ogTvShow is null.");
                return null;
            } else {
                return new OgTvShowBean((OgTvShowDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgTvShow(OgTvShow ogTvShow)
    {
        if(getDataObject() != null) {
            getDataObject().setOgTvShow(
                (ogTvShow instanceof OgTvShowBean) ?
                ((OgTvShowBean) ogTvShow).toDataObject() :
                ((ogTvShow instanceof OgTvShowDataObject) ? ogTvShow : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }

    public OgTvEpisode getOgTvEpisode()
    {
        if(getDataObject() != null) {
            OgTvEpisode _field = getDataObject().getOgTvEpisode();
            if(_field == null) {
                log.log(Level.INFO, "ogTvEpisode is null.");
                return null;
            } else {
                return new OgTvEpisodeBean((OgTvEpisodeDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setOgTvEpisode(OgTvEpisode ogTvEpisode)
    {
        if(getDataObject() != null) {
            getDataObject().setOgTvEpisode(
                (ogTvEpisode instanceof OgTvEpisodeBean) ?
                ((OgTvEpisodeBean) ogTvEpisode).toDataObject() :
                ((ogTvEpisode instanceof OgTvEpisodeDataObject) ? ogTvEpisode : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OpenGraphMetaDataObject is null!");
        }
    }


    // TBD
    public OpenGraphMetaDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
