package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.data.MediaSourceStructDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;

public class AudioStructBean implements AudioStruct
{
    private static final Logger log = Logger.getLogger(AudioStructBean.class.getName());

    // Embedded data object.
    private AudioStructDataObject dobj = null;

    public AudioStructBean()
    {
        this(new AudioStructDataObject());
    }
    public AudioStructBean(AudioStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public AudioStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public String getId()
    {
        if(getDataObject() != null) {
            return getDataObject().getId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setId(String id)
    {
        if(getDataObject() != null) {
            getDataObject().setId(id);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public String getControls()
    {
        if(getDataObject() != null) {
            return getDataObject().getControls();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setControls(String controls)
    {
        if(getDataObject() != null) {
            getDataObject().setControls(controls);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public Boolean isAutoplayEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isAutoplayEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAutoplayEnabled(Boolean autoplayEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setAutoplayEnabled(autoplayEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public Boolean isLoopEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isLoopEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLoopEnabled(Boolean loopEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setLoopEnabled(loopEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public Boolean isPreloadEnabled()
    {
        if(getDataObject() != null) {
            return getDataObject().isPreloadEnabled();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setPreloadEnabled(Boolean preloadEnabled)
    {
        if(getDataObject() != null) {
            getDataObject().setPreloadEnabled(preloadEnabled);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public String getRemark()
    {
        if(getDataObject() != null) {
            return getDataObject().getRemark();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRemark(String remark)
    {
        if(getDataObject() != null) {
            getDataObject().setRemark(remark);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public MediaSourceStruct getSource()
    {
        if(getDataObject() != null) {
            MediaSourceStruct _field = getDataObject().getSource();
            if(_field == null) {
                log.log(Level.INFO, "source is null.");
                return null;
            } else {
                return new MediaSourceStructBean((MediaSourceStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource(MediaSourceStruct source)
    {
        if(getDataObject() != null) {
            getDataObject().setSource(
                (source instanceof MediaSourceStructBean) ?
                ((MediaSourceStructBean) source).toDataObject() :
                ((source instanceof MediaSourceStructDataObject) ? source : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public MediaSourceStruct getSource1()
    {
        if(getDataObject() != null) {
            MediaSourceStruct _field = getDataObject().getSource1();
            if(_field == null) {
                log.log(Level.INFO, "source1 is null.");
                return null;
            } else {
                return new MediaSourceStructBean((MediaSourceStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource1(MediaSourceStruct source1)
    {
        if(getDataObject() != null) {
            getDataObject().setSource1(
                (source1 instanceof MediaSourceStructBean) ?
                ((MediaSourceStructBean) source1).toDataObject() :
                ((source1 instanceof MediaSourceStructDataObject) ? source1 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public MediaSourceStruct getSource2()
    {
        if(getDataObject() != null) {
            MediaSourceStruct _field = getDataObject().getSource2();
            if(_field == null) {
                log.log(Level.INFO, "source2 is null.");
                return null;
            } else {
                return new MediaSourceStructBean((MediaSourceStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource2(MediaSourceStruct source2)
    {
        if(getDataObject() != null) {
            getDataObject().setSource2(
                (source2 instanceof MediaSourceStructBean) ?
                ((MediaSourceStructBean) source2).toDataObject() :
                ((source2 instanceof MediaSourceStructDataObject) ? source2 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public MediaSourceStruct getSource3()
    {
        if(getDataObject() != null) {
            MediaSourceStruct _field = getDataObject().getSource3();
            if(_field == null) {
                log.log(Level.INFO, "source3 is null.");
                return null;
            } else {
                return new MediaSourceStructBean((MediaSourceStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource3(MediaSourceStruct source3)
    {
        if(getDataObject() != null) {
            getDataObject().setSource3(
                (source3 instanceof MediaSourceStructBean) ?
                ((MediaSourceStructBean) source3).toDataObject() :
                ((source3 instanceof MediaSourceStructDataObject) ? source3 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public MediaSourceStruct getSource4()
    {
        if(getDataObject() != null) {
            MediaSourceStruct _field = getDataObject().getSource4();
            if(_field == null) {
                log.log(Level.INFO, "source4 is null.");
                return null;
            } else {
                return new MediaSourceStructBean((MediaSourceStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource4(MediaSourceStruct source4)
    {
        if(getDataObject() != null) {
            getDataObject().setSource4(
                (source4 instanceof MediaSourceStructBean) ?
                ((MediaSourceStructBean) source4).toDataObject() :
                ((source4 instanceof MediaSourceStructDataObject) ? source4 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public MediaSourceStruct getSource5()
    {
        if(getDataObject() != null) {
            MediaSourceStruct _field = getDataObject().getSource5();
            if(_field == null) {
                log.log(Level.INFO, "source5 is null.");
                return null;
            } else {
                return new MediaSourceStructBean((MediaSourceStructDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSource5(MediaSourceStruct source5)
    {
        if(getDataObject() != null) {
            getDataObject().setSource5(
                (source5 instanceof MediaSourceStructBean) ?
                ((MediaSourceStructBean) source5).toDataObject() :
                ((source5 instanceof MediaSourceStructDataObject) ? source5 : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded AudioStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getControls() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isAutoplayEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isLoopEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && isPreloadEnabled() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource1() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource2() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource3() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource4() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSource5() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public AudioStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
