package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.data.RobotsTextRefreshDataObject;

public class RobotsTextRefreshBean extends BeanBase implements RobotsTextRefresh
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshBean.class.getName());

    // Embedded data object.
    private RobotsTextRefreshDataObject dobj = null;

    public RobotsTextRefreshBean()
    {
        this(new RobotsTextRefreshDataObject());
    }
    public RobotsTextRefreshBean(String guid)
    {
        this(new RobotsTextRefreshDataObject(guid));
    }
    public RobotsTextRefreshBean(RobotsTextRefreshDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public RobotsTextRefreshDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getGuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getGuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;
        }
    }
    public void setGuid(String guid)
    {
        if(getDataObject() != null) {
            // Unfortunately, due to the way Stub object is instantiated (e.g., from web service request)
            // we should allow setting guid after the object has been constructed.
            if(log.isLoggable(Level.INFO)) log.info("Bean.guid is being set to a new guid = " + guid);
            getDataObject().setGuid(guid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public String getRobotsTextFile()
    {
        if(getDataObject() != null) {
            return getDataObject().getRobotsTextFile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setRobotsTextFile(String robotsTextFile)
    {
        if(getDataObject() != null) {
            getDataObject().setRobotsTextFile(robotsTextFile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public Integer getRefreshInterval()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshInterval();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshInterval(refreshInterval);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public String getStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setStatus(String status)
    {
        if(getDataObject() != null) {
            getDataObject().setStatus(status);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public Integer getRefreshStatus()
    {
        if(getDataObject() != null) {
            return getDataObject().getRefreshStatus();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        if(getDataObject() != null) {
            getDataObject().setRefreshStatus(refreshStatus);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public String getResult()
    {
        if(getDataObject() != null) {
            return getDataObject().getResult();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setResult(String result)
    {
        if(getDataObject() != null) {
            getDataObject().setResult(result);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public Long getLastCheckedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastCheckedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setLastCheckedTime(lastCheckedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public Long getNextCheckedTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getNextCheckedTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setNextCheckedTime(Long nextCheckedTime)
    {
        if(getDataObject() != null) {
            getDataObject().setNextCheckedTime(nextCheckedTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }

    public Long getExpirationTime()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationTime();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationTime(expirationTime);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded RobotsTextRefreshDataObject is null!");
        }
    }


    // TBD
    public RobotsTextRefreshDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
