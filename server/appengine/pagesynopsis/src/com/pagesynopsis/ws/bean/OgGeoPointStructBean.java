package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.ws.data.OgGeoPointStructDataObject;

public class OgGeoPointStructBean implements OgGeoPointStruct
{
    private static final Logger log = Logger.getLogger(OgGeoPointStructBean.class.getName());

    // Embedded data object.
    private OgGeoPointStructDataObject dobj = null;

    public OgGeoPointStructBean()
    {
        this(new OgGeoPointStructDataObject());
    }
    public OgGeoPointStructBean(OgGeoPointStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public OgGeoPointStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
        }
    }

    public Float getLatitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLatitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLatitude(Float latitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLatitude(latitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
        }
    }

    public Float getLongitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getLongitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setLongitude(Float longitude)
    {
        if(getDataObject() != null) {
            getDataObject().setLongitude(longitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
        }
    }

    public Float getAltitude()
    {
        if(getDataObject() != null) {
            return getDataObject().getAltitude();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAltitude(Float altitude)
    {
        if(getDataObject() != null) {
            getDataObject().setAltitude(altitude);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgGeoPointStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public OgGeoPointStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
