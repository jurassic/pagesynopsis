package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.data.ImageStructDataObject;

public class ImageStructBean implements ImageStruct
{
    private static final Logger log = Logger.getLogger(ImageStructBean.class.getName());

    // Embedded data object.
    private ImageStructDataObject dobj = null;

    public ImageStructBean()
    {
        this(new ImageStructDataObject());
    }
    public ImageStructBean(ImageStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public ImageStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getId()
    {
        if(getDataObject() != null) {
            return getDataObject().getId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setId(String id)
    {
        if(getDataObject() != null) {
            getDataObject().setId(id);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getAlt()
    {
        if(getDataObject() != null) {
            return getDataObject().getAlt();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setAlt(String alt)
    {
        if(getDataObject() != null) {
            getDataObject().setAlt(alt);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getSrc()
    {
        if(getDataObject() != null) {
            return getDataObject().getSrc();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSrc(String src)
    {
        if(getDataObject() != null) {
            getDataObject().setSrc(src);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getSrcUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getSrcUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSrcUrl(String srcUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setSrcUrl(srcUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getMediaType()
    {
        if(getDataObject() != null) {
            return getDataObject().getMediaType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setMediaType(String mediaType)
    {
        if(getDataObject() != null) {
            getDataObject().setMediaType(mediaType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getWidthAttr()
    {
        if(getDataObject() != null) {
            return getDataObject().getWidthAttr();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setWidthAttr(String widthAttr)
    {
        if(getDataObject() != null) {
            getDataObject().setWidthAttr(widthAttr);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public Integer getWidth()
    {
        if(getDataObject() != null) {
            return getDataObject().getWidth();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setWidth(Integer width)
    {
        if(getDataObject() != null) {
            getDataObject().setWidth(width);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getHeightAttr()
    {
        if(getDataObject() != null) {
            return getDataObject().getHeightAttr();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHeightAttr(String heightAttr)
    {
        if(getDataObject() != null) {
            getDataObject().setHeightAttr(heightAttr);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public Integer getHeight()
    {
        if(getDataObject() != null) {
            return getDataObject().getHeight();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setHeight(Integer height)
    {
        if(getDataObject() != null) {
            getDataObject().setHeight(height);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }

    public String getNote()
    {
        if(getDataObject() != null) {
            return getDataObject().getNote();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setNote(String note)
    {
        if(getDataObject() != null) {
            getDataObject().setNote(note);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded ImageStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMediaType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidthAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeightAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public ImageStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
