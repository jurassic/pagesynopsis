package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;

public class OgProfileBean extends OgObjectBaseBean implements OgProfile
{
    private static final Logger log = Logger.getLogger(OgProfileBean.class.getName());

    // Embedded data object.
    private OgProfileDataObject dobj = null;

    public OgProfileBean()
    {
        this(new OgProfileDataObject());
    }
    public OgProfileBean(String guid)
    {
        this(new OgProfileDataObject(guid));
    }
    public OgProfileBean(OgProfileDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public OgProfileDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getProfileId()
    {
        if(getDataObject() != null) {
            return getDataObject().getProfileId();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
            return null;   // ???
        }
    }
    public void setProfileId(String profileId)
    {
        if(getDataObject() != null) {
            getDataObject().setProfileId(profileId);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
        }
    }

    public String getFirstName()
    {
        if(getDataObject() != null) {
            return getDataObject().getFirstName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
            return null;   // ???
        }
    }
    public void setFirstName(String firstName)
    {
        if(getDataObject() != null) {
            getDataObject().setFirstName(firstName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
        }
    }

    public String getLastName()
    {
        if(getDataObject() != null) {
            return getDataObject().getLastName();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
            return null;   // ???
        }
    }
    public void setLastName(String lastName)
    {
        if(getDataObject() != null) {
            getDataObject().setLastName(lastName);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
        }
    }

    public String getUsername()
    {
        if(getDataObject() != null) {
            return getDataObject().getUsername();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
            return null;   // ???
        }
    }
    public void setUsername(String username)
    {
        if(getDataObject() != null) {
            getDataObject().setUsername(username);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
        }
    }

    public String getGender()
    {
        if(getDataObject() != null) {
            return getDataObject().getGender();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
            return null;   // ???
        }
    }
    public void setGender(String gender)
    {
        if(getDataObject() != null) {
            getDataObject().setGender(gender);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgProfileDataObject is null!");
        }
    }


    // TBD
    public OgProfileDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
