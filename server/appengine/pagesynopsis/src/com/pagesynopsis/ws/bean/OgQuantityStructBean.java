package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.data.OgQuantityStructDataObject;

public class OgQuantityStructBean implements OgQuantityStruct
{
    private static final Logger log = Logger.getLogger(OgQuantityStructBean.class.getName());

    // Embedded data object.
    private OgQuantityStructDataObject dobj = null;

    public OgQuantityStructBean()
    {
        this(new OgQuantityStructDataObject());
    }
    public OgQuantityStructBean(OgQuantityStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public OgQuantityStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
        }
    }

    public Float getValue()
    {
        if(getDataObject() != null) {
            return getDataObject().getValue();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setValue(Float value)
    {
        if(getDataObject() != null) {
            getDataObject().setValue(value);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
        }
    }

    public String getUnits()
    {
        if(getDataObject() != null) {
            return getDataObject().getUnits();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUnits(String units)
    {
        if(getDataObject() != null) {
            getDataObject().setUnits(units);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgQuantityStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUnits() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public OgQuantityStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
