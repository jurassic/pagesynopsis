package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;

public class OgAudioStructBean implements OgAudioStruct
{
    private static final Logger log = Logger.getLogger(OgAudioStructBean.class.getName());

    // Embedded data object.
    private OgAudioStructDataObject dobj = null;

    public OgAudioStructBean()
    {
        this(new OgAudioStructDataObject());
    }
    public OgAudioStructBean(OgAudioStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public OgAudioStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
        }
    }

    public String getUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUrl(String url)
    {
        if(getDataObject() != null) {
            getDataObject().setUrl(url);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
        }
    }

    public String getSecureUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getSecureUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSecureUrl(String secureUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setSecureUrl(secureUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgAudioStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecureUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public OgAudioStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
