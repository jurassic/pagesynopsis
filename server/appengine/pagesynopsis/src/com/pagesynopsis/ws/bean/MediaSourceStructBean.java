package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.data.MediaSourceStructDataObject;

public class MediaSourceStructBean implements MediaSourceStruct
{
    private static final Logger log = Logger.getLogger(MediaSourceStructBean.class.getName());

    // Embedded data object.
    private MediaSourceStructDataObject dobj = null;

    public MediaSourceStructBean()
    {
        this(new MediaSourceStructDataObject());
    }
    public MediaSourceStructBean(MediaSourceStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public MediaSourceStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
        }
    }

    public String getSrc()
    {
        if(getDataObject() != null) {
            return getDataObject().getSrc();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSrc(String src)
    {
        if(getDataObject() != null) {
            getDataObject().setSrc(src);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
        }
    }

    public String getSrcUrl()
    {
        if(getDataObject() != null) {
            return getDataObject().getSrcUrl();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setSrcUrl(String srcUrl)
    {
        if(getDataObject() != null) {
            getDataObject().setSrcUrl(srcUrl);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
        }
    }

    public String getType()
    {
        if(getDataObject() != null) {
            return getDataObject().getType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setType(String type)
    {
        if(getDataObject() != null) {
            getDataObject().setType(type);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
        }
    }

    public String getRemark()
    {
        if(getDataObject() != null) {
            return getDataObject().getRemark();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRemark(String remark)
    {
        if(getDataObject() != null) {
            getDataObject().setRemark(remark);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded MediaSourceStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public MediaSourceStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
