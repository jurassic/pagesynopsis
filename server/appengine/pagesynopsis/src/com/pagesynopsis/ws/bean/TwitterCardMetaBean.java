package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.TwitterCardMetaDataObject;

public class TwitterCardMetaBean extends PageBaseBean implements TwitterCardMeta
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaBean.class.getName());

    // Embedded data object.
    private TwitterCardMetaDataObject dobj = null;

    public TwitterCardMetaBean()
    {
        this(new TwitterCardMetaDataObject());
    }
    public TwitterCardMetaBean(String guid)
    {
        this(new TwitterCardMetaDataObject(guid));
    }
    public TwitterCardMetaBean(TwitterCardMetaDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public TwitterCardMetaDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getCardType()
    {
        if(getDataObject() != null) {
            return getDataObject().getCardType();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setCardType(String cardType)
    {
        if(getDataObject() != null) {
            getDataObject().setCardType(cardType);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }

    public TwitterSummaryCard getSummaryCard()
    {
        if(getDataObject() != null) {
            TwitterSummaryCard _field = getDataObject().getSummaryCard();
            if(_field == null) {
                log.log(Level.INFO, "summaryCard is null.");
                return null;
            } else {
                return new TwitterSummaryCardBean((TwitterSummaryCardDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setSummaryCard(TwitterSummaryCard summaryCard)
    {
        if(getDataObject() != null) {
            getDataObject().setSummaryCard(
                (summaryCard instanceof TwitterSummaryCardBean) ?
                ((TwitterSummaryCardBean) summaryCard).toDataObject() :
                ((summaryCard instanceof TwitterSummaryCardDataObject) ? summaryCard : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }

    public TwitterPhotoCard getPhotoCard()
    {
        if(getDataObject() != null) {
            TwitterPhotoCard _field = getDataObject().getPhotoCard();
            if(_field == null) {
                log.log(Level.INFO, "photoCard is null.");
                return null;
            } else {
                return new TwitterPhotoCardBean((TwitterPhotoCardDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setPhotoCard(TwitterPhotoCard photoCard)
    {
        if(getDataObject() != null) {
            getDataObject().setPhotoCard(
                (photoCard instanceof TwitterPhotoCardBean) ?
                ((TwitterPhotoCardBean) photoCard).toDataObject() :
                ((photoCard instanceof TwitterPhotoCardDataObject) ? photoCard : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }

    public TwitterGalleryCard getGalleryCard()
    {
        if(getDataObject() != null) {
            TwitterGalleryCard _field = getDataObject().getGalleryCard();
            if(_field == null) {
                log.log(Level.INFO, "galleryCard is null.");
                return null;
            } else {
                return new TwitterGalleryCardBean((TwitterGalleryCardDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setGalleryCard(TwitterGalleryCard galleryCard)
    {
        if(getDataObject() != null) {
            getDataObject().setGalleryCard(
                (galleryCard instanceof TwitterGalleryCardBean) ?
                ((TwitterGalleryCardBean) galleryCard).toDataObject() :
                ((galleryCard instanceof TwitterGalleryCardDataObject) ? galleryCard : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }

    public TwitterAppCard getAppCard()
    {
        if(getDataObject() != null) {
            TwitterAppCard _field = getDataObject().getAppCard();
            if(_field == null) {
                log.log(Level.INFO, "appCard is null.");
                return null;
            } else {
                return new TwitterAppCardBean((TwitterAppCardDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setAppCard(TwitterAppCard appCard)
    {
        if(getDataObject() != null) {
            getDataObject().setAppCard(
                (appCard instanceof TwitterAppCardBean) ?
                ((TwitterAppCardBean) appCard).toDataObject() :
                ((appCard instanceof TwitterAppCardDataObject) ? appCard : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }

    public TwitterPlayerCard getPlayerCard()
    {
        if(getDataObject() != null) {
            TwitterPlayerCard _field = getDataObject().getPlayerCard();
            if(_field == null) {
                log.log(Level.INFO, "playerCard is null.");
                return null;
            } else {
                return new TwitterPlayerCardBean((TwitterPlayerCardDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setPlayerCard(TwitterPlayerCard playerCard)
    {
        if(getDataObject() != null) {
            getDataObject().setPlayerCard(
                (playerCard instanceof TwitterPlayerCardBean) ?
                ((TwitterPlayerCardBean) playerCard).toDataObject() :
                ((playerCard instanceof TwitterPlayerCardDataObject) ? playerCard : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }

    public TwitterProductCard getProductCard()
    {
        if(getDataObject() != null) {
            TwitterProductCard _field = getDataObject().getProductCard();
            if(_field == null) {
                log.log(Level.INFO, "productCard is null.");
                return null;
            } else {
                return new TwitterProductCardBean((TwitterProductCardDataObject) _field);
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
            return null;   // ???
        }
    }
    public void setProductCard(TwitterProductCard productCard)
    {
        if(getDataObject() != null) {
            getDataObject().setProductCard(
                (productCard instanceof TwitterProductCardBean) ?
                ((TwitterProductCardBean) productCard).toDataObject() :
                ((productCard instanceof TwitterProductCardDataObject) ? productCard : null)
            );
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded TwitterCardMetaDataObject is null!");
        }
    }


    // TBD
    public TwitterCardMetaDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
