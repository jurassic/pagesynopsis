package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.data.OgAudioStructDataObject;
import com.pagesynopsis.ws.data.OgImageStructDataObject;
import com.pagesynopsis.ws.data.OgActorStructDataObject;
import com.pagesynopsis.ws.data.OgVideoStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;

public class OgArticleBean extends OgObjectBaseBean implements OgArticle
{
    private static final Logger log = Logger.getLogger(OgArticleBean.class.getName());

    // Embedded data object.
    private OgArticleDataObject dobj = null;

    public OgArticleBean()
    {
        this(new OgArticleDataObject());
    }
    public OgArticleBean(String guid)
    {
        this(new OgArticleDataObject(guid));
    }
    public OgArticleBean(OgArticleDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public OgArticleDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getAuthor()
    {
        if(getDataObject() != null) {
            return getDataObject().getAuthor();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
            return null;   // ???
        }
    }
    public void setAuthor(List<String> author)
    {
        if(getDataObject() != null) {
            getDataObject().setAuthor(author);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
        }
    }

    public String getSection()
    {
        if(getDataObject() != null) {
            return getDataObject().getSection();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
            return null;   // ???
        }
    }
    public void setSection(String section)
    {
        if(getDataObject() != null) {
            getDataObject().setSection(section);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
        }
    }

    public List<String> getTag()
    {
        if(getDataObject() != null) {
            return getDataObject().getTag();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
            return null;   // ???
        }
    }
    public void setTag(List<String> tag)
    {
        if(getDataObject() != null) {
            getDataObject().setTag(tag);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
        }
    }

    public String getPublishedDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getPublishedDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
            return null;   // ???
        }
    }
    public void setPublishedDate(String publishedDate)
    {
        if(getDataObject() != null) {
            getDataObject().setPublishedDate(publishedDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
        }
    }

    public String getModifiedDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getModifiedDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
            return null;   // ???
        }
    }
    public void setModifiedDate(String modifiedDate)
    {
        if(getDataObject() != null) {
            getDataObject().setModifiedDate(modifiedDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
        }
    }

    public String getExpirationDate()
    {
        if(getDataObject() != null) {
            return getDataObject().getExpirationDate();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
            return null;   // ???
        }
    }
    public void setExpirationDate(String expirationDate)
    {
        if(getDataObject() != null) {
            getDataObject().setExpirationDate(expirationDate);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgArticleDataObject is null!");
        }
    }


    // TBD
    public OgArticleDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
