package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.data.OgVideoDataObject;
import com.pagesynopsis.ws.data.TwitterProductCardDataObject;
import com.pagesynopsis.ws.data.TwitterSummaryCardDataObject;
import com.pagesynopsis.ws.data.OgBlogDataObject;
import com.pagesynopsis.ws.data.TwitterPlayerCardDataObject;
import com.pagesynopsis.ws.data.UrlStructDataObject;
import com.pagesynopsis.ws.data.ImageStructDataObject;
import com.pagesynopsis.ws.data.TwitterGalleryCardDataObject;
import com.pagesynopsis.ws.data.TwitterPhotoCardDataObject;
import com.pagesynopsis.ws.data.OgTvShowDataObject;
import com.pagesynopsis.ws.data.OgBookDataObject;
import com.pagesynopsis.ws.data.OgWebsiteDataObject;
import com.pagesynopsis.ws.data.OgMovieDataObject;
import com.pagesynopsis.ws.data.TwitterAppCardDataObject;
import com.pagesynopsis.ws.data.AnchorStructDataObject;
import com.pagesynopsis.ws.data.KeyValuePairStructDataObject;
import com.pagesynopsis.ws.data.OgArticleDataObject;
import com.pagesynopsis.ws.data.OgTvEpisodeDataObject;
import com.pagesynopsis.ws.data.AudioStructDataObject;
import com.pagesynopsis.ws.data.VideoStructDataObject;
import com.pagesynopsis.ws.data.OgProfileDataObject;
import com.pagesynopsis.ws.data.VideoSetDataObject;

public class VideoSetBean extends PageBaseBean implements VideoSet
{
    private static final Logger log = Logger.getLogger(VideoSetBean.class.getName());

    // Embedded data object.
    private VideoSetDataObject dobj = null;

    public VideoSetBean()
    {
        this(new VideoSetDataObject());
    }
    public VideoSetBean(String guid)
    {
        this(new VideoSetDataObject(guid));
    }
    public VideoSetBean(VideoSetDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    @Override
    public VideoSetDataObject getDataObject()
    {
        return this.dobj;
    }

    public List<String> getMediaTypeFilter()
    {
        if(getDataObject() != null) {
            return getDataObject().getMediaTypeFilter();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VideoSetDataObject is null!");
            return null;   // ???
        }
    }
    public void setMediaTypeFilter(List<String> mediaTypeFilter)
    {
        if(getDataObject() != null) {
            getDataObject().setMediaTypeFilter(mediaTypeFilter);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VideoSetDataObject is null!");
        }
    }

    public Set<VideoStruct> getPageVideos()
    {
        if(getDataObject() != null) {
            Set<VideoStruct> list = getDataObject().getPageVideos();
            if(list != null) {
                Set<VideoStruct> bean = new HashSet<VideoStruct>();
                for(VideoStruct videoStruct : list) {
                    VideoStructBean elem = null;
                    if(videoStruct instanceof VideoStructBean) {
                        elem = (VideoStructBean) videoStruct;
                    } else if(videoStruct instanceof VideoStructDataObject) {
                        elem = new VideoStructBean((VideoStructDataObject) videoStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded VideoSetDataObject is null!");
            return null;
        }
    }
    public void setPageVideos(Set<VideoStruct> pageVideos)
    {
        if(pageVideos != null) {
            if(getDataObject() != null) {
                Set<VideoStruct> dataObj = new HashSet<VideoStruct>();
                for(VideoStruct videoStruct : pageVideos) {
                    VideoStructDataObject elem = null;
                    if(videoStruct instanceof VideoStructBean) {
                        elem = ((VideoStructBean) videoStruct).toDataObject();
                    } else if(videoStruct instanceof VideoStructDataObject) {
                        elem = (VideoStructDataObject) videoStruct;
                    } else if(videoStruct instanceof VideoStruct) {
                        elem = new VideoStructDataObject(videoStruct.getUuid(), videoStruct.getId(), videoStruct.getWidth(), videoStruct.getHeight(), videoStruct.getControls(), videoStruct.isAutoplayEnabled(), videoStruct.isLoopEnabled(), videoStruct.isPreloadEnabled(), videoStruct.isMuted(), videoStruct.getRemark(), videoStruct.getSource(), videoStruct.getSource1(), videoStruct.getSource2(), videoStruct.getSource3(), videoStruct.getSource4(), videoStruct.getSource5(), videoStruct.getNote());
                    }
                    if(elem != null) {
                        dataObj.add(elem);
                    }
                }
                getDataObject().setPageVideos(dataObj);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded VideoSetDataObject is null!");
            }
        } else {
            if(getDataObject() != null) {
                getDataObject().setPageVideos(pageVideos);
            } else {
                // Can this happen? Log it.
                log.log(Level.WARNING, "Embedded VideoSetDataObject is null!");
            }
        }
    }


    // TBD
    public VideoSetDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
