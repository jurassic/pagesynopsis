package com.pagesynopsis.ws.bean;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.data.OgActorStructDataObject;

public class OgActorStructBean implements OgActorStruct
{
    private static final Logger log = Logger.getLogger(OgActorStructBean.class.getName());

    // Embedded data object.
    private OgActorStructDataObject dobj = null;

    public OgActorStructBean()
    {
        this(new OgActorStructDataObject());
    }
    public OgActorStructBean(OgActorStructDataObject dobj)
    {
        super();
        // TBD: What if dobj is null????
        this.dobj = dobj;
    }

    public OgActorStructDataObject getDataObject()
    {
        return this.dobj;
    }

    public String getUuid()
    {
        if(getDataObject() != null) {
            return getDataObject().getUuid();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setUuid(String uuid)
    {
        if(getDataObject() != null) {
            getDataObject().setUuid(uuid);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
        }
    }

    public String getProfile()
    {
        if(getDataObject() != null) {
            return getDataObject().getProfile();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setProfile(String profile)
    {
        if(getDataObject() != null) {
            getDataObject().setProfile(profile);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
        }
    }

    public String getRole()
    {
        if(getDataObject() != null) {
            return getDataObject().getRole();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
            return null;   // ???
        }
    }
    public void setRole(String role)
    {
        if(getDataObject() != null) {
            getDataObject().setRole(role);
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        if(getDataObject() != null) {
            return getDataObject().isEmpty();
        } else {
            // Can this happen? Log it.
            log.log(Level.WARNING, "Embedded OgActorStructDataObject is null!");
            boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRole() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
            return (atLeastOneFieldNonEmpty == false);
        }
    }

    // TBD
    public OgActorStructDataObject toDataObject()
    {
        return getDataObject();
    }

    @Override
    public String toString()
    {
        // TBD
        if(getDataObject() != null) {
            return getDataObject().toString();
        } else {
            return ""; // ???
        }
    }

    @Override
    public int hashCode()
    {
        if(getDataObject() == null) {
            return 0; // ???
        } else {
            return getDataObject().hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
