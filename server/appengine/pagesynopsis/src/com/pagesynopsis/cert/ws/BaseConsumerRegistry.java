package com.pagesynopsis.cert.ws;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


// OAuth consumer key registry.
public abstract class BaseConsumerRegistry
{
    private static final Logger log = Logger.getLogger(BaseConsumerRegistry.class.getName());

    // Consumer key-secret map.
    // TBD: Use a better data structure which reflects OAuthConsumerInfo.
    private Map<String, String> consumerSecretMap;
    
    protected Map<String, String> getBaseConsumerSecretMap()
    {
        consumerSecretMap = new HashMap<String, String>();

        // TBD...
        consumerSecretMap.put("14e72223-08b9-42e5-810a-03363677a722", "c2944b41-304d-48db-9ea8-405469998d7d");  // PageSynopsis + PageSynopsisApp
        consumerSecretMap.put("a8d36bfb-c747-40c1-8249-b12f0a8ba49c", "dc1414a4-7acd-479d-b741-51c56d5d6bca");  // PageSynopsis + QueryClient
        // ...

        return consumerSecretMap;
    }
    protected Map<String, String> getConsumerSecretMap()
    {
        return consumerSecretMap;
    }

}
