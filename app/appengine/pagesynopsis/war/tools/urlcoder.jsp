<%@ page import="com.pagesynopsis.ws.core.*, com.pagesynopsis.af.auth.*, com.pagesynopsis.fe.*, com.pagesynopsis.fe.core.*, com.pagesynopsis.fe.bean.*, com.pagesynopsis.fe.util.*, com.pagesynopsis.wa.service.*, com.pagesynopsis.util.*, com.pagesynopsis.app.util.*, com.pagesynopsis.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%
//{1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
// ...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
%><%
//[3] Local "global" variables.
//EncodedQueryParamStructJsBean encodedQueryParamBean = null;
//DecodedQueryParamStructJsBean decodedQueryParamBean = null;
// ...
%><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | URL Encoder - URL Encoding, URL Decoding</title>
    <meta name="author" content="Aery software">
    <meta name="description" content="<%=brandDisplayName%> Service is a web page summary service. Service is exposed through REST Web services API. Currently, it uses HTML page meta tags.">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/bootstrap/blueridge/bootstrap-2.0.css">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/jquery-ui-1.9.2/pepper-grinder/jquery-ui-1.9.2.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-<%=appBrand%>.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-mobile.css" media="only screen and (max-device-width: 480px)"/>
<!-- 
    <link href="/css/bootstrap.responsive-2.0.css" rel="stylesheet">
 -->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-pagesynopsis.ico" />
	<link rel="icon" href="/img/favicon-pagesynopsis.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://www.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=aQv8kescAfPXIVijKI63Iw&v=1" type="text/javascript"></script>
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/"><%=brandDisplayName%><sup>&beta;</sup></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/page/pageinfo">Page Info</a></li>
              <li><a href="/page/linklist">Link List</a></li>
              <li><a href="/page/imageset">Image Set</a></li>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
<%
}
%>
              <li class="active"><a href="/urlcoder">URL Encoder</a></li>
            </ul>
            <p class="navbar-text pull-right">
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+pagesynopsis+com?subject=Re: <%=brandDisplayName%>" title="Email us"><i class="icon-envelope icon-white"></i></a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
<h1>URL Encode/Decode Form</h1>

  <div id="main">
    <form id="form_urlcoder">

      <div id="main_urlcoder">
      <fieldset name="fieldset_main_urlcoder">

<table>

  <tr>
    <td colspan="3">
       <label class="input_label_text" for="input_queryparam_paramtype" id="input_queryparam_paramtype_label">Input String Type:</label>
       <select id="input_queryparam_paramtype">
		   <option value="param_value">Query Parameter Key or Value</option>
		   <option value="param_pair">Query Parameter Pair (key=value)</option>
		   <option value="param_query">Query String</option>
		   <option value="param_url">Full URL</option>
       </select>
    </td>
  </tr>

  <tr>
    <td>&nbsp;
    </td>
    <td>&nbsp;
    </td>
    <td>&nbsp;
    </td>
  </tr>

  <tr>
    <td>
       <label class="input_label_text" for="input_preencoded_queryparam" id="input_preencoded_label" title="Query parameter string to be encoded">Original String:</label>
       <input type="text" name="input_preencoded_queryparam" id="input_preencoded_queryparam" value="" size="50"></input>
    </td>
    <td style="vertical-align:bottom; padding:0px 12px;">
       <button type="button" name="button_preencoded_encode" id="button_preencoded_encode" title="Encode the parameter" style="width:90px">Encode &raquo;</button> 
    </td>
    <td>
       <label class="input_label_text" for="input_postencoded_queryparam" id="input_postencoded_label" title="URL-encoded query parameter string">URL Encoded:</label>
       <input type="text" name="input_postencoded_queryparam" id="input_postencoded_queryparam" value="" readonly="readonly" size="50"></input>
    </td>
  </tr>

  <tr>
    <td>&nbsp;
    </td>
    <td>&nbsp;
    </td>
    <td>&nbsp;
    </td>
  </tr>

  <tr>
    <td>
       <label class="input_label_text" for="input_predecoded_queryparam" id="input_predecoded_label" title="Query parameter string to be decoded">Original String:</label>
       <input type="text" name="input_predecoded_queryparam" id="input_predecoded_queryparam" value="" size="50"></input>
    </td>
    <td style="vertical-align:bottom; padding:0px 12px;">
       <button type="button" name="button_predecoded_decode" id="button_predecoded_decode" title="Decode the parameter" style="width:90px">Decode &raquo;</button> 
    </td>
    <td>
       <label class="input_label_text" for="input_postdecoded_queryparam" id="input_postdecoded_label" title="URL-decoded query parameter string">URL Decoded:</label>
       <input type="text" name="input_postdecoded_queryparam" id="input_postdecoded_queryparam" value="" readonly="readonly" size="50"></input>
    </td>
  </tr>

</table>


      </fieldset>
      </div>

    </form>
  </div>



<!--
        <p><a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" title="Please provide your email address, and we will keep you posted on upcoming pre-release trials at <%=brandDisplayName%>.">Sign up &raquo;</a></p>
-->
      </div>

      <!-- Example row of columns -->
      <div class="row-fluid">
        <div class="span12">



<h3>"Input String Type"</h3>
<p>
This URL Encoding Form can be used, among other things, to encode a parameter value (e.g., the query param value of "targetUrl"),
or to encode the whole URL in certain limited cases (e.g., <i>http://www.pagesynopsis.com/pageinfo?targetUrl=http://www.google.com/?a=b</i>), etc.
The input param type combo box allows one to select this "context".
See the note at the end of this page for meore information.
</p>
<br/>


<h2>URL Encoding/Decoding</h2>

<h3>What is URL encoding?</h3>
<p>
URL encoding is a mechanism for encoding information in a URL (or, more broadly, URI).
It is also used in the preparation of data of the "application/x-www-form-urlencoded" media type,
which is often used in the submission of HTML form data in HTTP requests.
The generic URI syntax mandates that URI schemes that provide for the representation of character data in a URI must,
in effect, represent characters from the unreserved set without translation,
and should convert all other characters to bytes according to UTF-8 (or, other encoding scheme), 
and then percent-encode those values. 
This requirement was introduced in January 2005 with the publication of RFC 3986.
</p>


<h3>How to URL encode a character?</h3>
<p>
URL encoding (or, "percent encoding") of a character consists of
 a "%" symbol, followed by the two-digit hexadecimal representation 
 of the ISO-Latin code point for the character.
 For example, the hexadecimal value of a reserved character, equal sign ("="), is 3D.
 Hence, the equal sign is URL encoded to <i>%3D</i>.
 In general (e.g., including non-Latin chars),
 characters are first converted into one or more bytes using some encoding scheme. 
 Then each byte is represented by the 3-character string "%xy", where xy is the two-digit hexadecimal representation of the byte. 
 The recommended encoding scheme to use is UTF-8.
 (<a href="/">PageSynopsis</a> assumes that the input chars are encoded in UTF-8.
 The above URL encoding and URL decoding utility also uses UTF-8.)
It should be noted that URL encoding is not "unique".
While some characters must be URL encoded (e.g., reserved characters),
some characters may or may not need to be encoded.
(In an extreme case, every character including English alphabets can be URL encoded.)
Therefore, a string can be URL-encoded in a number of different ways.
The most important thing to note is that when these encoded strings are URL-decoded in reverse,
they all result in the same string, i.e., the original input string. 
</p>

<h3>How to URL encode query string (or, URL)?</h3>
<p>
A question mark ("?"), if any, separates the query string from the URL path part.
A query string may contain zero or more query parameters separated by an ampersand ("&").
Each query parameter may be a key-value pair separated by an equal sign ("=").
A URL, or its query string, need to be parsed according to this rule,
and each of the query parameter, or key, value, should be separately URL encoded.
Once each part has been URL encoded, they can be re-assembled to the full URL (or, query string)
in the reverse order.
The above URL encoding tool encodes the input string based on this context (as specified by the "input string type" combo box).
Note that encoding a full URL through this type of generic tool is not generally possible 
(e.g., due to the ambiguity as to where the "&" belongs, to the root URL or to the parameter value?, etc.).
When in doubt, follow the algorithm as specified here by separately URL encoding each parameter 
and combining them to create a full encoded URL (or, query string).
</p>

<p>
Note that there appears to be some confusion as to what "URL encoding" exactly is.
A quick Web search shows that there are a lot of questions on the Web regarding "how to encode a URL?", etc.
The confusion often stems from the ambiguity as to what "URL encoding" means in a particular context.
(For example, Javascript has built-in encodeURI() and encodeURIComponent() functions, which are to be used in different contexts.)
The URL encode/decode form above clearly differentiates different types of "URL encodings".  
Further information can be found on the Web: For example, refer to <a href="http://www.ietf.org/rfc/rfc3986.txt">RFC 3986</a>
or <a href="http://en.wikipedia.org/wiki/Url_encoding">Percent encoding</a>, among others. 
</p>


<h2>Notes</h2>

<h3>One final word on "encoded URL" and "decoded URL"</h3>
<p>
As stated earlier, there is no such thing as "decoded URL".
All valid URLs are "encoded URLs".
"URL encoding" is a part of a process of building a valid URL from its "components" (e.g., a scheme, hostname, path, query string), etc.
Each URL component may need to be "encoded" in some way (e.g., to avoid ambiguity, etc.).
Once a URL is constructed through a proper process (including proper "URL encoding"), the URL is valid.
You cannot "decode a URL" in a literal sense.
URL decoding means to decode components of the URL, each of which has been encoded to create a valid URL.
Decoding different types of components follow different rules.
(They are the "reverse" of the corresponding encoding rules.)  
</p>

<h3>Input String Type</h3>

<dl class="dl-horizontal">
<dt>Query Parameter Key or Value</dt>
<dd>
<i>URL encoding and decoding URL parameters</i> are the most commonly used meanings of
"URL encoding" and "URL decoding".
These are the most commonly used use cases as well.
For encoding, a full URL should be built by encoding each component,
and this is the recommended option for encoding URL param key or value. 
</dd>
<dt>Query Parameter Pair (key=value)</dt>
<dd>
The first equal sign in the input string is interpreted as the literal equal sign that separates a key and a value.
each of which will be encoded/decoded separately.
</dd>
<dt>Query String</dt>
<dd>
The whole query string can be encoded and decoded as well.
This option is most useful (and produces the correct result) 
for decoding an encoded query string (from an encoded/valid URL).
Encoding an entire "query string" (which is formed from un-encoded query keys and values, etc.),
on the other hand,
may or may not produce a correct result.
(We interpret "&" and "=" as special characters in this implementation. 
That is, no (un-encoded) key and value can include these characters.)
</dd>
<dt>Full URL</dt>
<dd>
A URL cannot be encoded or decoded in the literal sense.
We provide this as a convenience option.
The part of the input string after the first question mark ("?"), if any,
is interpreted as a query string and encoded/decoded accordingly.
The path part of the URL is to be encoded/decoded following a different rule 
(cf. <a href="http://www.ietf.org/rfc/rfc3986.txt">RFC 3986</a>).
Unfortunately, no programming languages (that we know of) provide convenience methods 
to encode/decode URL path (partly because there is really no common use cases unlike encoding/decoding query params).
In our simple implmentation provided here,
we assume that the URL part exclduing the query string is already valid (e.g., properly encoded),
and we will not encode/decode this part.
(This option is provided for convience so that one can copy and paste a full URL 
to encode/decode query parameters without having to parse the URL into its components, etc.)
</dd>

</dl>


<!--
          <p><a class="btn" href="/reminder">View details &raquo;</a></p>
-->
       </div>
      </div>

      <hr>

      <footer>
        <p>
        &copy; <%=brandDisplayName%> 2012&nbsp; 
        <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a>
        <span class="pull-right">
<!-- 
              <a href="/resource">Resource</a> | 
              <a href="/about">About</a> | 
-->
        <a href="/contact">Contact</a> | <a href="http://blog.pagesynopsis.com/">Blog</a>
        </span>
        </p>
      </footer>

    </div> <!-- /container -->



    <div id="div_status_floater" style="display: none;" class="alert">
    </div>


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/bean/encodedqueryparamstructjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/bean/decodedqueryparamstructjsbean-1.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/fiveten-1.0.js"></script>


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, http://www.pagesynopsis.com/, is a Web service for fetching HTML page title and description. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_tweet").click(function() {
            if(DEBUG_ENABLED) console.log("Tweet button clicked."); 

            if(!tweetHelper) {
                tweetHelper = new webstoa.TweetHelper();
            }
        	if(tweetHelper) {
	            var tweetTitle = '<%=brandDisplayName%> Message';
	            var tweetMessage = '<%=brandDisplayName%>, http://www.pagesynopsis.com/, is a Web service for fetching HTML page title and description. Please check it out.';
	            tweetHelper.tweet(tweetTitle, tweetMessage);
        	} else {
        		// ????
        	}
		    return false;
        });
    });
    </script>


    <script>
    // Function definitions.
    function updateStatus(msg, type, time) {
    	if(!time) {
    		time = 5000;   // milliseconds.
    	}
    	if(!type) {
    		// type = "info";  // warning, error, etc....
    	}
    	//$("#div_status_floater").text(msg);
    	//$("#div_status_floater").fadeIn().delay(time).fadeOut();
		////$("#div_status_floater").textInOut(msg, type, time);
    }    
    </script>


    <script>
    // "Init"
    $(function() {
        $("#input_preencoded_queryparam").focus();

        // TBD: initially hide $("#div_status_floater") ...
    	$("#div_status_floater").text("").hide();

        // TBD: Location...
    	var params = {x: 350, y: 250, speed: 'normal' };
    	$("#div_status_floater").makeFloat(params);
    	
    	// Peeng?
    	//var fiveTenTimer = new fiveten.FiveTenTimer(9753, 5);   // ???
    	//fiveTenTimer.start();
    });
    </script>

    <script>
    // App-related vars.
    var userGuid;
<%
if(userId != null) {
%>
    userGuid = "<%=userId%>";
<%
}
%>
    var encodedQueryParamBean = new pagesynopsis.wa.bean.EncodedQueryParamStructJsBean();
    var decodedQueryParamBean = new pagesynopsis.wa.bean.DecodedQueryParamStructJsBean();
    </script>

    <script>
    // Ajax form handlers.
    $(function() {
	    $("#button_preencoded_encode").click(function() {
	        if(DEBUG_ENABLED) console.log("button_preencoded_encode button clicked.");
	        updateStatus('Encoding the string...', 'info', 5550);
	        $("#input_postencoded_queryparam").val('');
	        postQueryParamJsBeans();
	      });
	    $("#button_predecoded_decode").click(function() {
	        if(DEBUG_ENABLED) console.log("button_predecoded_decode button clicked.");
	        updateStatus('Decoding the string...', 'info', 5550);
	        $("#input_postdecoded_queryparam").val('');
	        postQueryParamJsBeans();
	      });
    });
    </script>

  <script>
  var postQueryParamJsBeans = function() {
      var preencoded = $("#input_preencoded_queryparam").val().trim();
      if(DEBUG_ENABLED) console.log("preencoded = " + preencoded);
      var predecoded = $("#input_predecoded_queryparam").val().trim();
      if(DEBUG_ENABLED) console.log("predecoded = " + predecoded);

      if(preencoded == '' && predecoded == '') {
          updateStatus('Input is empty.', 'error', 5550);
    	  return false;
      }
      
      var paramType = $("#input_queryparam_paramtype").val().trim();
      if(DEBUG_ENABLED) console.log("paramType = " + paramType);
      // etc....
      
      var preencodedJsonStr;
      //if(preencoded != '') {
          encodedQueryParamBean.setOriginalString(preencoded);
          encodedQueryParamBean.setParamType(paramType);
          preencodedJsonStr = JSON.stringify(encodedQueryParamBean);
          if(DEBUG_ENABLED) console.log("preencodedJsonStr = " + preencodedJsonStr);
      //}
      var predecodedJsonStr;
      //if(predecoded != '') {
          decodedQueryParamBean.setOriginalString(predecoded);
          decodedQueryParamBean.setParamType(paramType);    // Curretly, this is not being used....
          predecodedJsonStr = JSON.stringify(decodedQueryParamBean);
          if(DEBUG_ENABLED) console.log("predecodedJsonStr = " + predecodedJsonStr);
      //} 
      
      var payload = "{"
      if(preencodedJsonStr) {
          payload += "\"encodedQueryParamStruct\":" + preencodedJsonStr;
      }
      if(predecodedJsonStr) {
          payload += ",\"decodedQueryParamStruct\":" + predecodedJsonStr;
      }
      payload += "}";
      if(DEBUG_ENABLED) console.log("payload = " + payload);

      var postEndPoint = "<%=topLevelUrl%>" + "ajax/urlcode";
      if(DEBUG_ENABLED) console.log("postEndPoint =" + postEndPoint);

      $.ajax({
  	    type: "POST",
  	    url: postEndPoint,
  	    data: payload,
  	    dataType: "json",
  	    contentType: "application/json",
  	    success: function(data, textStatus) {
  	      // TBD
  	      if(DEBUG_ENABLED) console.log("URL encode/decode successfully processed. textStatus = " + textStatus);
  	      if(DEBUG_ENABLED) console.log("data = " + data);
  	      if(DEBUG_ENABLED) console.log(data);

  	      // Parse data...
  	      if(data) {   // POST only..
	    	      var postencodedObj = data["encodedQueryParamStruct"];
	    	      if(DEBUG_ENABLED) console.log("postencodedObj = " + postencodedObj);
	    	      if(DEBUG_ENABLED) console.log(postencodedObj);
	    	      if(postencodedObj) {
    	    	      encodedQueryParamBean = pagesynopsis.wa.bean.EncodedQueryParamStructJsBean.create(postencodedObj);
	    	      } else {
	    	    	  // ????
	    	      }
	    	      var postdecodedObj = data["decodedQueryParamStruct"];
	    	      if(DEBUG_ENABLED) console.log("postdecodedObj = " + postdecodedObj);
	    	      if(DEBUG_ENABLED) console.log(postdecodedObj);
	    	      if(postdecodedObj) {
    	    	      decodedQueryParamBean = pagesynopsis.wa.bean.DecodedQueryParamStructJsBean.create(postdecodedObj);
	    	      } else {
	    	    	  // ????
	    	      }

	    	      window.setTimeout(refreshFormFields, 350);
	              updateStatus('Request successfully processed.', 'info', 5550);
  	      } else {
  	    	  // ignore
  	      } 
  	    },
  	    error: function(req, textStatus) {
            if(DEBUG_ENABLED) console.log('Failed to process the request. status = ' + textStatus);
  	    	updateStatus('Failed to process the request. status = ' + textStatus, 'error', 5550);
  	    }
	  });
      
  };
  </script>

  <script>
  // Refresh input fields...
  var refreshFormFields = function() {
	if(DEBUG_ENABLED) console.log("refreshFormFields() called......");
	
	var postencoded = encodedQueryParamBean.getEncodedString();
	$("#input_postencoded_queryparam").val(postencoded);
	var postdecoded = decodedQueryParamBean.getDecodedString();
	$("#input_postdecoded_queryparam").val(postdecoded);
	
  };
  </script>



<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
