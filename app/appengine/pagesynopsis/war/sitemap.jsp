<%@ page import="com.pagesynopsis.ws.core.*, com.pagesynopsis.af.auth.*, com.pagesynopsis.fe.*, com.pagesynopsis.fe.core.*, com.pagesynopsis.fe.bean.*, com.pagesynopsis.fe.util.*, com.pagesynopsis.wa.service.*, com.pagesynopsis.util.*, com.pagesynopsis.helper.*" 
%><%@ page contentType="application/xml; charset=UTF-8" 
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
%><%
// ...
%><?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<%
//String requestUrl = request.getRequestURL().toString();
//String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
String weekAgo = SitemapHelper.formatDate(new java.util.Date().getTime() - 24*7*3600000L);
if(weekAgo == null || weekAgo.isEmpty()) {  // This should not happen.
   weekAgo = "2011-11-11";   // Just use an arbitrary date...
}
%>

   <url>
      <loc>http://www.pagesynopsis.com/</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.8</priority>
   </url>

   <url>
      <loc>http://www.pagesynopsis.com/page/pageinfo</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.5</priority>
   </url>
   <url>
      <loc>http://www.pagesynopsis.com/page/linklist</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.5</priority>
   </url>
   <url>
      <loc>http://www.pagesynopsis.com/page/imageset</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.5</priority>
   </url>

   <url>
      <loc>http://www.pagesynopsis.com/urlcoder</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.5</priority>
   </url>

   <url>
      <loc>http://www.pagesynopsis.com/contact</loc>
      <lastmod><%=weekAgo%></lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.5</priority>
   </url>

</urlset>
