//////////////////////////////////////////////////////////
// <script src="/js/form/fetchrequestformbean-1.0.js"></script>
// Last modified time: 1374536953981.
// Place holder...
//////////////////////////////////////////////////////////


var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.form = pagesynopsis.wa.form || {};
pagesynopsis.wa.form.FetchRequestFormBean = ( function() {


  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var isEmpty = function(obj) {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop)) {
        return false;
      }
    }
    return true;
  };


  /////////////////////////////
  // Constructor
  // Note: We use "parasitic inheritance"
  //       http://www.crockford.com/javascript/inheritance.html
  /////////////////////////////

  var cls = function(jsBean) {

    // Private vars.
    var errorMap = {};

    // Inheritance
    var that;
    if(jsBean) {
      that = jsBean;
    } else {
      that = new pagesynopsis.wa.bean.FetchRequestJsBean();
    }


    /////////////////////////////
    // Constants
    /////////////////////////////

    // To indicate the "global" errors...
    that.FIELD_BEANWIDE = "_beanwide_";


    /////////////////////////////
    // Subclass methods
    /////////////////////////////

    that.hasErrors = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList && errorList.length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        if(isEmpty(errorMap)) {
          return false;  
        } else {
          return true;
        }
      }
    };

    that.getLastError = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList && errorList.length > 0) {
          return errorList[errorList.length - 1];
        } else {
          return null;
        }
      } else {
        // ???
    	return null;
      }
    };
    that.getErrors = function(f) {
      if(f) {
        var errorList = errorMap[f];
        if(errorList) {
          return errorList;
          //return errorList.slice(0);
        } else {
          return [];
        }
      } else {
        // ???
      	return null;
      }
    };

    that.addError = function(f, error) {
      if(f && error) {
        var errorList = errorMap[f];
        if(!errorList) {
          errorList = [];
        }
        errorList.push(error);
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };
    that.setError = function(f, error) {
      if(f && error) {
        var errorList = [];
        errorList.push(error);
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };

    that.addErrors = function(f, errors) {
      if(f && errors && errors.length > 0) {
        var errorList = errorMap[f];
        if(!errorList) {
          errorList = [];
        }
        errorList = errorList.concat(errors);
        //errorList = errorList.concat(errors.slice(0));
        errorMap[f] = errorList;
      } else {
        // ???
      }
    };
    that.setErrors = function(f, errors) {
      if(f) {
        if(errors) {
          errorMap[f] = errors;
          //errorMap[f] = errors.slice(0);
        } else {
          delete errorMap[f];
        }
      } else {
        // ???
      }
    };

    that.resetErrors = function(f) { 
      if(f) {
        delete errorMap[f]; 
      } else {
        errorMap = {}; 
      }
    };


    that.validate = function() { 
      var allOK = true;

//      // TBD
//      if(!this.getGuid()) {
//    	  this.addError("guid", "guid is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getUser()) {
//    	  this.addError("user", "user is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getTargetUrl()) {
//    	  this.addError("targetUrl", "targetUrl is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getPageUrl()) {
//    	  this.addError("pageUrl", "pageUrl is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getQueryString()) {
//    	  this.addError("queryString", "queryString is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getQueryParams()) {
//    	  this.addError("queryParams", "queryParams is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getVersion()) {
//    	  this.addError("version", "version is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getFetchObject()) {
//    	  this.addError("fetchObject", "fetchObject is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getFetchStatus()) {
//    	  this.addError("fetchStatus", "fetchStatus is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getResult()) {
//    	  this.addError("result", "result is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getNote()) {
//    	  this.addError("note", "note is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getReferrerInfo()) {
//    	  this.addError("referrerInfo", "referrerInfo is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getStatus()) {
//    	  this.addError("status", "status is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getNextPageUrl()) {
//    	  this.addError("nextPageUrl", "nextPageUrl is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getFollowPages()) {
//    	  this.addError("followPages", "followPages is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getFollowDepth()) {
//    	  this.addError("followDepth", "followDepth is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getCreateVersion()) {
//    	  this.addError("createVersion", "createVersion is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getDeferred()) {
//    	  this.addError("deferred", "deferred is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getAlert()) {
//    	  this.addError("alert", "alert is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getNotificationPref()) {
//    	  this.addError("notificationPref", "notificationPref is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getCreatedTime()) {
//    	  this.addError("createdTime", "createdTime is null");
//        allOK = false;
//      }
//      // TBD
//      if(!this.getModifiedTime()) {
//    	  this.addError("modifiedTime", "modifiedTime is null");
//        allOK = false;
//      }

      return allOK; 
    };

    
    /////////////////////////////
    // Convenience methods
    // Note that we are "overriding" superclass methods
    // but reusing their implementations 
    /////////////////////////////
    
    // Clone this bean.
    that.clone = function() {
      var jsBean = this._clone();
      var o = new pagesynopsis.wa.form.FetchRequestFormBean(jsBean);

      if(errorMap) {
        for(var f in errorMap) {
          var errorList = errorMap[f];
          o.setErrors(f, errorList.slice(0));
        }
      }
    
      return o;
    };

    // This will be called by JSON.stringify().
    that.toJSON = function() {
      var jsonObj = this._toJSON();

      // ???
      if(!isEmpty(errorMap)) {
          jsonObj.errorMap = errorMap;
      }
      // ...

      return jsonObj;
    };


    /////////////////////////////
    // For debugging.
    /////////////////////////////

    that.toString = function() {
      var str = this._toString();

      str += "errors: {";
      for(var f in errorMap) {
        if(errorMap.hasOwnProperty(f)) {
          var errorList = errorMap[f];
          if(errorList) {
            str += f + ": [";
            for(var i=0; i<errorList.length; i++) {
              str += errorList[i] + ", ";
            }
            str += "];";
          }
        }
      }
      str += "};";

      return str;
    };

    return that;
  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.form.FetchRequestFormBean.create = function(obj) {
  var jsBean = pagesynopsis.wa.bean.FetchRequestJsBean.create(obj);
  var o = new pagesynopsis.wa.form.FetchRequestFormBean(jsBean);

  if(obj.errorMap) {
    for(var f in obj.errorMap) {
      var errorList = obj.errorMap[f];
      o.setErrors(f, errorList.slice(0));
    }
  }

  return o;
};

pagesynopsis.wa.form.FetchRequestFormBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.form.FetchRequestFormBean.create(jsonObj);
  return obj;
};

