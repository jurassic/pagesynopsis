/*
FiveTen timer.
*/

var fiveten = fiveten || {};
fiveten.FiveTenTimer = ( function() {

  var getCurrentTime = function() {
	return (new Date()).getTime();
  }

  var cls = function(pm, mp) {
    var counter = 0;
    var intervalMillis;
    var maxPeengs;
    if(pm) {
    	intervalMillis = pm;    // TBD: validation?	
    } else {
    	intervalMillis = 5000;  // 5 secs by default
    }
    if(mp) {
    	maxPeengs = mp;       // TBD: validation?
    } else {
    	maxPeengs = 10;       // 10 times by default.
    }

    this.reset = function() {
        counter = maxPeengs;  // ???
    };

    this.start = function() {
    	if(DEBUG_ENABLED) console.log("Starting peeng: intervalMillis = " + intervalMillis + "; maxPeengs = " + maxPeengs);

    	counter = 0;
    	this.peeng();
    };

    this.peeng = function() {
        ++counter;
    	if(counter > maxPeengs) {
    		return;
    	}
        var that = this;
    	$.get('/ajax/peeng', function(data, textStatus) {
    		var tis = that;
    		if(DEBUG_ENABLED) console.log("peeng(): counter = " + counter + "; textStatus = " + textStatus);
    		window.setTimeout(function() { tis.peeng(); }, intervalMillis);	
    	});
    };

  };

  return cls;
})();

