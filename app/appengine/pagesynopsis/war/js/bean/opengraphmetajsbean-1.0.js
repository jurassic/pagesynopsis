//////////////////////////////////////////////////////////
// <script src="/js/bean/opengraphmetajsbean-1.0.js"></script>
// Last modified time: 1374536953352.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.OpenGraphMetaJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var fetchRequest;
    var targetUrl;
    var pageUrl;
    var queryString;
    var queryParams;
    var lastFetchResult;
    var responseCode;
    var contentType;
    var contentLength;
    var language;
    var redirect;
    var location;
    var pageTitle;
    var note;
    var deferred;
    var status;
    var refreshStatus;
    var refreshInterval;
    var nextRefreshTime;
    var lastCheckedTime;
    var lastUpdatedTime;
    var ogType;
    var ogProfile;
    var ogWebsite;
    var ogBlog;
    var ogArticle;
    var ogBook;
    var ogVideo;
    var ogMovie;
    var ogTvShow;
    var ogTvEpisode;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getFetchRequest = function() { return fetchRequest; };
    this.setFetchRequest = function(value) { fetchRequest = value; };
    this.getTargetUrl = function() { return targetUrl; };
    this.setTargetUrl = function(value) { targetUrl = value; };
    this.getPageUrl = function() { return pageUrl; };
    this.setPageUrl = function(value) { pageUrl = value; };
    this.getQueryString = function() { return queryString; };
    this.setQueryString = function(value) { queryString = value; };
    this.getQueryParams = function() { return queryParams; };
    this.setQueryParams = function(value) { queryParams = value; };
    this.getLastFetchResult = function() { return lastFetchResult; };
    this.setLastFetchResult = function(value) { lastFetchResult = value; };
    this.getResponseCode = function() { return responseCode; };
    this.setResponseCode = function(value) { responseCode = value; };
    this.getContentType = function() { return contentType; };
    this.setContentType = function(value) { contentType = value; };
    this.getContentLength = function() { return contentLength; };
    this.setContentLength = function(value) { contentLength = value; };
    this.getLanguage = function() { return language; };
    this.setLanguage = function(value) { language = value; };
    this.getRedirect = function() { return redirect; };
    this.setRedirect = function(value) { redirect = value; };
    this.getLocation = function() { return location; };
    this.setLocation = function(value) { location = value; };
    this.getPageTitle = function() { return pageTitle; };
    this.setPageTitle = function(value) { pageTitle = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getDeferred = function() { return deferred; };
    this.setDeferred = function(value) { deferred = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getRefreshStatus = function() { return refreshStatus; };
    this.setRefreshStatus = function(value) { refreshStatus = value; };
    this.getRefreshInterval = function() { return refreshInterval; };
    this.setRefreshInterval = function(value) { refreshInterval = value; };
    this.getNextRefreshTime = function() { return nextRefreshTime; };
    this.setNextRefreshTime = function(value) { nextRefreshTime = value; };
    this.getLastCheckedTime = function() { return lastCheckedTime; };
    this.setLastCheckedTime = function(value) { lastCheckedTime = value; };
    this.getLastUpdatedTime = function() { return lastUpdatedTime; };
    this.setLastUpdatedTime = function(value) { lastUpdatedTime = value; };
    this.getOgType = function() { return ogType; };
    this.setOgType = function(value) { ogType = value; };
    this.getOgProfile = function() { return ogProfile; };
    this.setOgProfile = function(value) { ogProfile = value; };
    this.getOgWebsite = function() { return ogWebsite; };
    this.setOgWebsite = function(value) { ogWebsite = value; };
    this.getOgBlog = function() { return ogBlog; };
    this.setOgBlog = function(value) { ogBlog = value; };
    this.getOgArticle = function() { return ogArticle; };
    this.setOgArticle = function(value) { ogArticle = value; };
    this.getOgBook = function() { return ogBook; };
    this.setOgBook = function(value) { ogBook = value; };
    this.getOgVideo = function() { return ogVideo; };
    this.setOgVideo = function(value) { ogVideo = value; };
    this.getOgMovie = function() { return ogMovie; };
    this.setOgMovie = function(value) { ogMovie = value; };
    this.getOgTvShow = function() { return ogTvShow; };
    this.setOgTvShow = function(value) { ogTvShow = value; };
    this.getOgTvEpisode = function() { return ogTvEpisode; };
    this.setOgTvEpisode = function(value) { ogTvEpisode = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.OpenGraphMetaJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(fetchRequest !== undefined && fetchRequest != null) {
        o.setFetchRequest(fetchRequest);
      }
      if(targetUrl !== undefined && targetUrl != null) {
        o.setTargetUrl(targetUrl);
      }
      if(pageUrl !== undefined && pageUrl != null) {
        o.setPageUrl(pageUrl);
      }
      if(queryString !== undefined && queryString != null) {
        o.setQueryString(queryString);
      }
      if(queryParams !== undefined && queryParams != null) {
        o.setQueryParams(queryParams);
      }
      if(lastFetchResult !== undefined && lastFetchResult != null) {
        o.setLastFetchResult(lastFetchResult);
      }
      if(responseCode !== undefined && responseCode != null) {
        o.setResponseCode(responseCode);
      }
      if(contentType !== undefined && contentType != null) {
        o.setContentType(contentType);
      }
      if(contentLength !== undefined && contentLength != null) {
        o.setContentLength(contentLength);
      }
      if(language !== undefined && language != null) {
        o.setLanguage(language);
      }
      if(redirect !== undefined && redirect != null) {
        o.setRedirect(redirect);
      }
      if(location !== undefined && location != null) {
        o.setLocation(location);
      }
      if(pageTitle !== undefined && pageTitle != null) {
        o.setPageTitle(pageTitle);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(deferred !== undefined && deferred != null) {
        o.setDeferred(deferred);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(refreshStatus !== undefined && refreshStatus != null) {
        o.setRefreshStatus(refreshStatus);
      }
      if(refreshInterval !== undefined && refreshInterval != null) {
        o.setRefreshInterval(refreshInterval);
      }
      if(nextRefreshTime !== undefined && nextRefreshTime != null) {
        o.setNextRefreshTime(nextRefreshTime);
      }
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        o.setLastCheckedTime(lastCheckedTime);
      }
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        o.setLastUpdatedTime(lastUpdatedTime);
      }
      if(ogType !== undefined && ogType != null) {
        o.setOgType(ogType);
      }
      //o.setOgProfile(ogProfile.clone());
      if(ogProfile !== undefined && ogProfile != null) {
        o.setOgProfile(ogProfile);
      }
      //o.setOgWebsite(ogWebsite.clone());
      if(ogWebsite !== undefined && ogWebsite != null) {
        o.setOgWebsite(ogWebsite);
      }
      //o.setOgBlog(ogBlog.clone());
      if(ogBlog !== undefined && ogBlog != null) {
        o.setOgBlog(ogBlog);
      }
      //o.setOgArticle(ogArticle.clone());
      if(ogArticle !== undefined && ogArticle != null) {
        o.setOgArticle(ogArticle);
      }
      //o.setOgBook(ogBook.clone());
      if(ogBook !== undefined && ogBook != null) {
        o.setOgBook(ogBook);
      }
      //o.setOgVideo(ogVideo.clone());
      if(ogVideo !== undefined && ogVideo != null) {
        o.setOgVideo(ogVideo);
      }
      //o.setOgMovie(ogMovie.clone());
      if(ogMovie !== undefined && ogMovie != null) {
        o.setOgMovie(ogMovie);
      }
      //o.setOgTvShow(ogTvShow.clone());
      if(ogTvShow !== undefined && ogTvShow != null) {
        o.setOgTvShow(ogTvShow);
      }
      //o.setOgTvEpisode(ogTvEpisode.clone());
      if(ogTvEpisode !== undefined && ogTvEpisode != null) {
        o.setOgTvEpisode(ogTvEpisode);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(fetchRequest !== undefined && fetchRequest != null) {
        jsonObj.fetchRequest = fetchRequest;
      } // Otherwise ignore...
      if(targetUrl !== undefined && targetUrl != null) {
        jsonObj.targetUrl = targetUrl;
      } // Otherwise ignore...
      if(pageUrl !== undefined && pageUrl != null) {
        jsonObj.pageUrl = pageUrl;
      } // Otherwise ignore...
      if(queryString !== undefined && queryString != null) {
        jsonObj.queryString = queryString;
      } // Otherwise ignore...
      if(queryParams !== undefined && queryParams != null) {
        jsonObj.queryParams = queryParams;
      } // Otherwise ignore...
      if(lastFetchResult !== undefined && lastFetchResult != null) {
        jsonObj.lastFetchResult = lastFetchResult;
      } // Otherwise ignore...
      if(responseCode !== undefined && responseCode != null) {
        jsonObj.responseCode = responseCode;
      } // Otherwise ignore...
      if(contentType !== undefined && contentType != null) {
        jsonObj.contentType = contentType;
      } // Otherwise ignore...
      if(contentLength !== undefined && contentLength != null) {
        jsonObj.contentLength = contentLength;
      } // Otherwise ignore...
      if(language !== undefined && language != null) {
        jsonObj.language = language;
      } // Otherwise ignore...
      if(redirect !== undefined && redirect != null) {
        jsonObj.redirect = redirect;
      } // Otherwise ignore...
      if(location !== undefined && location != null) {
        jsonObj.location = location;
      } // Otherwise ignore...
      if(pageTitle !== undefined && pageTitle != null) {
        jsonObj.pageTitle = pageTitle;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(deferred !== undefined && deferred != null) {
        jsonObj.deferred = deferred;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(refreshStatus !== undefined && refreshStatus != null) {
        jsonObj.refreshStatus = refreshStatus;
      } // Otherwise ignore...
      if(refreshInterval !== undefined && refreshInterval != null) {
        jsonObj.refreshInterval = refreshInterval;
      } // Otherwise ignore...
      if(nextRefreshTime !== undefined && nextRefreshTime != null) {
        jsonObj.nextRefreshTime = nextRefreshTime;
      } // Otherwise ignore...
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        jsonObj.lastCheckedTime = lastCheckedTime;
      } // Otherwise ignore...
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        jsonObj.lastUpdatedTime = lastUpdatedTime;
      } // Otherwise ignore...
      if(ogType !== undefined && ogType != null) {
        jsonObj.ogType = ogType;
      } // Otherwise ignore...
      if(ogProfile !== undefined && ogProfile != null) {
        jsonObj.ogProfile = ogProfile;
      } // Otherwise ignore...
      if(ogWebsite !== undefined && ogWebsite != null) {
        jsonObj.ogWebsite = ogWebsite;
      } // Otherwise ignore...
      if(ogBlog !== undefined && ogBlog != null) {
        jsonObj.ogBlog = ogBlog;
      } // Otherwise ignore...
      if(ogArticle !== undefined && ogArticle != null) {
        jsonObj.ogArticle = ogArticle;
      } // Otherwise ignore...
      if(ogBook !== undefined && ogBook != null) {
        jsonObj.ogBook = ogBook;
      } // Otherwise ignore...
      if(ogVideo !== undefined && ogVideo != null) {
        jsonObj.ogVideo = ogVideo;
      } // Otherwise ignore...
      if(ogMovie !== undefined && ogMovie != null) {
        jsonObj.ogMovie = ogMovie;
      } // Otherwise ignore...
      if(ogTvShow !== undefined && ogTvShow != null) {
        jsonObj.ogTvShow = ogTvShow;
      } // Otherwise ignore...
      if(ogTvEpisode !== undefined && ogTvEpisode != null) {
        jsonObj.ogTvEpisode = ogTvEpisode;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(fetchRequest) {
        str += "\"fetchRequest\":\"" + fetchRequest + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchRequest\":null, ";
      }
      if(targetUrl) {
        str += "\"targetUrl\":\"" + targetUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"targetUrl\":null, ";
      }
      if(pageUrl) {
        str += "\"pageUrl\":\"" + pageUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pageUrl\":null, ";
      }
      if(queryString) {
        str += "\"queryString\":\"" + queryString + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryString\":null, ";
      }
      if(queryParams) {
        str += "\"queryParams\":\"" + queryParams + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryParams\":null, ";
      }
      if(lastFetchResult) {
        str += "\"lastFetchResult\":\"" + lastFetchResult + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastFetchResult\":null, ";
      }
      if(responseCode) {
        str += "\"responseCode\":" + responseCode + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"responseCode\":null, ";
      }
      if(contentType) {
        str += "\"contentType\":\"" + contentType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contentType\":null, ";
      }
      if(contentLength) {
        str += "\"contentLength\":" + contentLength + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contentLength\":null, ";
      }
      if(language) {
        str += "\"language\":\"" + language + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"language\":null, ";
      }
      if(redirect) {
        str += "\"redirect\":\"" + redirect + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirect\":null, ";
      }
      if(location) {
        str += "\"location\":\"" + location + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"location\":null, ";
      }
      if(pageTitle) {
        str += "\"pageTitle\":\"" + pageTitle + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pageTitle\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(deferred) {
        str += "\"deferred\":" + deferred + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"deferred\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(refreshStatus) {
        str += "\"refreshStatus\":" + refreshStatus + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshStatus\":null, ";
      }
      if(refreshInterval) {
        str += "\"refreshInterval\":" + refreshInterval + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshInterval\":null, ";
      }
      if(nextRefreshTime) {
        str += "\"nextRefreshTime\":" + nextRefreshTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextRefreshTime\":null, ";
      }
      if(lastCheckedTime) {
        str += "\"lastCheckedTime\":" + lastCheckedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastCheckedTime\":null, ";
      }
      if(lastUpdatedTime) {
        str += "\"lastUpdatedTime\":" + lastUpdatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastUpdatedTime\":null, ";
      }
      if(ogType) {
        str += "\"ogType\":\"" + ogType + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"ogType\":null, ";
      }
      str += "\"ogProfile\":" + ogProfile.toJsonString() + ", ";
      str += "\"ogWebsite\":" + ogWebsite.toJsonString() + ", ";
      str += "\"ogBlog\":" + ogBlog.toJsonString() + ", ";
      str += "\"ogArticle\":" + ogArticle.toJsonString() + ", ";
      str += "\"ogBook\":" + ogBook.toJsonString() + ", ";
      str += "\"ogVideo\":" + ogVideo.toJsonString() + ", ";
      str += "\"ogMovie\":" + ogMovie.toJsonString() + ", ";
      str += "\"ogTvShow\":" + ogTvShow.toJsonString() + ", ";
      str += "\"ogTvEpisode\":" + ogTvEpisode.toJsonString() + ", ";
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "fetchRequest:" + fetchRequest + ", ";
      str += "targetUrl:" + targetUrl + ", ";
      str += "pageUrl:" + pageUrl + ", ";
      str += "queryString:" + queryString + ", ";
      str += "queryParams:" + queryParams + ", ";
      str += "lastFetchResult:" + lastFetchResult + ", ";
      str += "responseCode:" + responseCode + ", ";
      str += "contentType:" + contentType + ", ";
      str += "contentLength:" + contentLength + ", ";
      str += "language:" + language + ", ";
      str += "redirect:" + redirect + ", ";
      str += "location:" + location + ", ";
      str += "pageTitle:" + pageTitle + ", ";
      str += "note:" + note + ", ";
      str += "deferred:" + deferred + ", ";
      str += "status:" + status + ", ";
      str += "refreshStatus:" + refreshStatus + ", ";
      str += "refreshInterval:" + refreshInterval + ", ";
      str += "nextRefreshTime:" + nextRefreshTime + ", ";
      str += "lastCheckedTime:" + lastCheckedTime + ", ";
      str += "lastUpdatedTime:" + lastUpdatedTime + ", ";
      str += "ogType:" + ogType + ", ";
      str += "ogProfile:" + ogProfile + ", ";
      str += "ogWebsite:" + ogWebsite + ", ";
      str += "ogBlog:" + ogBlog + ", ";
      str += "ogArticle:" + ogArticle + ", ";
      str += "ogBook:" + ogBook + ", ";
      str += "ogVideo:" + ogVideo + ", ";
      str += "ogMovie:" + ogMovie + ", ";
      str += "ogTvShow:" + ogTvShow + ", ";
      str += "ogTvEpisode:" + ogTvEpisode + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.OpenGraphMetaJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.OpenGraphMetaJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.fetchRequest !== undefined && obj.fetchRequest != null) {
    o.setFetchRequest(obj.fetchRequest);
  }
  if(obj.targetUrl !== undefined && obj.targetUrl != null) {
    o.setTargetUrl(obj.targetUrl);
  }
  if(obj.pageUrl !== undefined && obj.pageUrl != null) {
    o.setPageUrl(obj.pageUrl);
  }
  if(obj.queryString !== undefined && obj.queryString != null) {
    o.setQueryString(obj.queryString);
  }
  if(obj.queryParams !== undefined && obj.queryParams != null) {
    o.setQueryParams(obj.queryParams);
  }
  if(obj.lastFetchResult !== undefined && obj.lastFetchResult != null) {
    o.setLastFetchResult(obj.lastFetchResult);
  }
  if(obj.responseCode !== undefined && obj.responseCode != null) {
    o.setResponseCode(obj.responseCode);
  }
  if(obj.contentType !== undefined && obj.contentType != null) {
    o.setContentType(obj.contentType);
  }
  if(obj.contentLength !== undefined && obj.contentLength != null) {
    o.setContentLength(obj.contentLength);
  }
  if(obj.language !== undefined && obj.language != null) {
    o.setLanguage(obj.language);
  }
  if(obj.redirect !== undefined && obj.redirect != null) {
    o.setRedirect(obj.redirect);
  }
  if(obj.location !== undefined && obj.location != null) {
    o.setLocation(obj.location);
  }
  if(obj.pageTitle !== undefined && obj.pageTitle != null) {
    o.setPageTitle(obj.pageTitle);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.deferred !== undefined && obj.deferred != null) {
    o.setDeferred(obj.deferred);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.refreshStatus !== undefined && obj.refreshStatus != null) {
    o.setRefreshStatus(obj.refreshStatus);
  }
  if(obj.refreshInterval !== undefined && obj.refreshInterval != null) {
    o.setRefreshInterval(obj.refreshInterval);
  }
  if(obj.nextRefreshTime !== undefined && obj.nextRefreshTime != null) {
    o.setNextRefreshTime(obj.nextRefreshTime);
  }
  if(obj.lastCheckedTime !== undefined && obj.lastCheckedTime != null) {
    o.setLastCheckedTime(obj.lastCheckedTime);
  }
  if(obj.lastUpdatedTime !== undefined && obj.lastUpdatedTime != null) {
    o.setLastUpdatedTime(obj.lastUpdatedTime);
  }
  if(obj.ogType !== undefined && obj.ogType != null) {
    o.setOgType(obj.ogType);
  }
  if(obj.ogProfile !== undefined && obj.ogProfile != null) {
    o.setOgProfile(obj.ogProfile);
  }
  if(obj.ogWebsite !== undefined && obj.ogWebsite != null) {
    o.setOgWebsite(obj.ogWebsite);
  }
  if(obj.ogBlog !== undefined && obj.ogBlog != null) {
    o.setOgBlog(obj.ogBlog);
  }
  if(obj.ogArticle !== undefined && obj.ogArticle != null) {
    o.setOgArticle(obj.ogArticle);
  }
  if(obj.ogBook !== undefined && obj.ogBook != null) {
    o.setOgBook(obj.ogBook);
  }
  if(obj.ogVideo !== undefined && obj.ogVideo != null) {
    o.setOgVideo(obj.ogVideo);
  }
  if(obj.ogMovie !== undefined && obj.ogMovie != null) {
    o.setOgMovie(obj.ogMovie);
  }
  if(obj.ogTvShow !== undefined && obj.ogTvShow != null) {
    o.setOgTvShow(obj.ogTvShow);
  }
  if(obj.ogTvEpisode !== undefined && obj.ogTvEpisode != null) {
    o.setOgTvEpisode(obj.ogTvEpisode);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

pagesynopsis.wa.bean.OpenGraphMetaJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.OpenGraphMetaJsBean.create(jsonObj);
  return obj;
};
