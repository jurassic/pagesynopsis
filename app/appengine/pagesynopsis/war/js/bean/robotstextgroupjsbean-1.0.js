//////////////////////////////////////////////////////////
// <script src="/js/bean/robotstextgroupjsbean-1.0.js"></script>
// Last modified time: 1374536952775.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.RobotsTextGroupJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var userAgent;
    var allows;
    var disallows;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getUserAgent = function() { return userAgent; };
    this.setUserAgent = function(value) { userAgent = value; };
    this.getAllows = function() { return allows; };
    this.setAllows = function(value) { allows = value; };
    this.getDisallows = function() { return disallows; };
    this.setDisallows = function(value) { disallows = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.RobotsTextGroupJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(userAgent !== undefined && userAgent != null) {
        o.setUserAgent(userAgent);
      }
      if(allows !== undefined && allows != null) {
        o.setAllows(allows);
      }
      if(disallows !== undefined && disallows != null) {
        o.setDisallows(disallows);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(userAgent !== undefined && userAgent != null) {
        jsonObj.userAgent = userAgent;
      } // Otherwise ignore...
      if(allows !== undefined && allows != null) {
        jsonObj.allows = allows;
      } // Otherwise ignore...
      if(disallows !== undefined && disallows != null) {
        jsonObj.disallows = disallows;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(userAgent) {
        str += "\"userAgent\":\"" + userAgent + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"userAgent\":null, ";
      }
      if(allows) {
        str += "\"allows\":\"" + allows + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"allows\":null, ";
      }
      if(disallows) {
        str += "\"disallows\":\"" + disallows + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"disallows\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "userAgent:" + userAgent + ", ";
      str += "allows:" + allows + ", ";
      str += "disallows:" + disallows + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.RobotsTextGroupJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.RobotsTextGroupJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.userAgent !== undefined && obj.userAgent != null) {
    o.setUserAgent(obj.userAgent);
  }
  if(obj.allows !== undefined && obj.allows != null) {
    o.setAllows(obj.allows);
  }
  if(obj.disallows !== undefined && obj.disallows != null) {
    o.setDisallows(obj.disallows);
  }
    
  return o;
};

pagesynopsis.wa.bean.RobotsTextGroupJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.RobotsTextGroupJsBean.create(jsonObj);
  return obj;
};
