//////////////////////////////////////////////////////////
// <script src="/js/bean/robotstextfilejsbean-1.0.js"></script>
// Last modified time: 1374536952804.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.RobotsTextFileJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var siteUrl;
    var groups;
    var sitemaps;
    var content;
    var contentHash;
    var lastCheckedTime;
    var lastUpdatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getSiteUrl = function() { return siteUrl; };
    this.setSiteUrl = function(value) { siteUrl = value; };
    this.getGroups = function() { return groups; };
    this.setGroups = function(value) { groups = value; };
    this.getSitemaps = function() { return sitemaps; };
    this.setSitemaps = function(value) { sitemaps = value; };
    this.getContent = function() { return content; };
    this.setContent = function(value) { content = value; };
    this.getContentHash = function() { return contentHash; };
    this.setContentHash = function(value) { contentHash = value; };
    this.getLastCheckedTime = function() { return lastCheckedTime; };
    this.setLastCheckedTime = function(value) { lastCheckedTime = value; };
    this.getLastUpdatedTime = function() { return lastUpdatedTime; };
    this.setLastUpdatedTime = function(value) { lastUpdatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.RobotsTextFileJsBean();

      o.setGuid(generateUuid());
      if(siteUrl !== undefined && siteUrl != null) {
        o.setSiteUrl(siteUrl);
      }
      if(groups !== undefined && groups != null) {
        o.setGroups(groups);
      }
      if(sitemaps !== undefined && sitemaps != null) {
        o.setSitemaps(sitemaps);
      }
      if(content !== undefined && content != null) {
        o.setContent(content);
      }
      if(contentHash !== undefined && contentHash != null) {
        o.setContentHash(contentHash);
      }
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        o.setLastCheckedTime(lastCheckedTime);
      }
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        o.setLastUpdatedTime(lastUpdatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(siteUrl !== undefined && siteUrl != null) {
        jsonObj.siteUrl = siteUrl;
      } // Otherwise ignore...
      if(groups !== undefined && groups != null) {
        jsonObj.groups = groups;
      } // Otherwise ignore...
      if(sitemaps !== undefined && sitemaps != null) {
        jsonObj.sitemaps = sitemaps;
      } // Otherwise ignore...
      if(content !== undefined && content != null) {
        jsonObj.content = content;
      } // Otherwise ignore...
      if(contentHash !== undefined && contentHash != null) {
        jsonObj.contentHash = contentHash;
      } // Otherwise ignore...
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        jsonObj.lastCheckedTime = lastCheckedTime;
      } // Otherwise ignore...
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        jsonObj.lastUpdatedTime = lastUpdatedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(siteUrl) {
        str += "\"siteUrl\":\"" + siteUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"siteUrl\":null, ";
      }
      if(groups) {
        str += "\"groups\":\"" + groups + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"groups\":null, ";
      }
      if(sitemaps) {
        str += "\"sitemaps\":\"" + sitemaps + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"sitemaps\":null, ";
      }
      if(content) {
        str += "\"content\":\"" + content + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"content\":null, ";
      }
      if(contentHash) {
        str += "\"contentHash\":\"" + contentHash + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"contentHash\":null, ";
      }
      if(lastCheckedTime) {
        str += "\"lastCheckedTime\":" + lastCheckedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastCheckedTime\":null, ";
      }
      if(lastUpdatedTime) {
        str += "\"lastUpdatedTime\":" + lastUpdatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastUpdatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "siteUrl:" + siteUrl + ", ";
      str += "groups:" + groups + ", ";
      str += "sitemaps:" + sitemaps + ", ";
      str += "content:" + content + ", ";
      str += "contentHash:" + contentHash + ", ";
      str += "lastCheckedTime:" + lastCheckedTime + ", ";
      str += "lastUpdatedTime:" + lastUpdatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.RobotsTextFileJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.RobotsTextFileJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.siteUrl !== undefined && obj.siteUrl != null) {
    o.setSiteUrl(obj.siteUrl);
  }
  if(obj.groups !== undefined && obj.groups != null) {
    o.setGroups(obj.groups);
  }
  if(obj.sitemaps !== undefined && obj.sitemaps != null) {
    o.setSitemaps(obj.sitemaps);
  }
  if(obj.content !== undefined && obj.content != null) {
    o.setContent(obj.content);
  }
  if(obj.contentHash !== undefined && obj.contentHash != null) {
    o.setContentHash(obj.contentHash);
  }
  if(obj.lastCheckedTime !== undefined && obj.lastCheckedTime != null) {
    o.setLastCheckedTime(obj.lastCheckedTime);
  }
  if(obj.lastUpdatedTime !== undefined && obj.lastUpdatedTime != null) {
    o.setLastUpdatedTime(obj.lastUpdatedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

pagesynopsis.wa.bean.RobotsTextFileJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.RobotsTextFileJsBean.create(jsonObj);
  return obj;
};
