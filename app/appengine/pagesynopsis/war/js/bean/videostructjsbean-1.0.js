//////////////////////////////////////////////////////////
// <script src="/js/bean/videostructjsbean-1.0.js"></script>
// Last modified time: 1374536952765.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.VideoStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var id;
    var width;
    var height;
    var controls;
    var autoplayEnabled;
    var loopEnabled;
    var preloadEnabled;
    var muted;
    var remark;
    var source;
    var source1;
    var source2;
    var source3;
    var source4;
    var source5;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getId = function() { return id; };
    this.setId = function(value) { id = value; };
    this.getWidth = function() { return width; };
    this.setWidth = function(value) { width = value; };
    this.getHeight = function() { return height; };
    this.setHeight = function(value) { height = value; };
    this.getControls = function() { return controls; };
    this.setControls = function(value) { controls = value; };
    this.getAutoplayEnabled = function() { return autoplayEnabled; };
    this.setAutoplayEnabled = function(value) { autoplayEnabled = value; };
    this.getLoopEnabled = function() { return loopEnabled; };
    this.setLoopEnabled = function(value) { loopEnabled = value; };
    this.getPreloadEnabled = function() { return preloadEnabled; };
    this.setPreloadEnabled = function(value) { preloadEnabled = value; };
    this.getMuted = function() { return muted; };
    this.setMuted = function(value) { muted = value; };
    this.getRemark = function() { return remark; };
    this.setRemark = function(value) { remark = value; };
    this.getSource = function() { return source; };
    this.setSource = function(value) { source = value; };
    this.getSource1 = function() { return source1; };
    this.setSource1 = function(value) { source1 = value; };
    this.getSource2 = function() { return source2; };
    this.setSource2 = function(value) { source2 = value; };
    this.getSource3 = function() { return source3; };
    this.setSource3 = function(value) { source3 = value; };
    this.getSource4 = function() { return source4; };
    this.setSource4 = function(value) { source4 = value; };
    this.getSource5 = function() { return source5; };
    this.setSource5 = function(value) { source5 = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.VideoStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(id !== undefined && id != null) {
        o.setId(id);
      }
      if(width !== undefined && width != null) {
        o.setWidth(width);
      }
      if(height !== undefined && height != null) {
        o.setHeight(height);
      }
      if(controls !== undefined && controls != null) {
        o.setControls(controls);
      }
      if(autoplayEnabled !== undefined && autoplayEnabled != null) {
        o.setAutoplayEnabled(autoplayEnabled);
      }
      if(loopEnabled !== undefined && loopEnabled != null) {
        o.setLoopEnabled(loopEnabled);
      }
      if(preloadEnabled !== undefined && preloadEnabled != null) {
        o.setPreloadEnabled(preloadEnabled);
      }
      if(muted !== undefined && muted != null) {
        o.setMuted(muted);
      }
      if(remark !== undefined && remark != null) {
        o.setRemark(remark);
      }
      //o.setSource(source.clone());
      if(source !== undefined && source != null) {
        o.setSource(source);
      }
      //o.setSource1(source1.clone());
      if(source1 !== undefined && source1 != null) {
        o.setSource1(source1);
      }
      //o.setSource2(source2.clone());
      if(source2 !== undefined && source2 != null) {
        o.setSource2(source2);
      }
      //o.setSource3(source3.clone());
      if(source3 !== undefined && source3 != null) {
        o.setSource3(source3);
      }
      //o.setSource4(source4.clone());
      if(source4 !== undefined && source4 != null) {
        o.setSource4(source4);
      }
      //o.setSource5(source5.clone());
      if(source5 !== undefined && source5 != null) {
        o.setSource5(source5);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(id !== undefined && id != null) {
        jsonObj.id = id;
      } // Otherwise ignore...
      if(width !== undefined && width != null) {
        jsonObj.width = width;
      } // Otherwise ignore...
      if(height !== undefined && height != null) {
        jsonObj.height = height;
      } // Otherwise ignore...
      if(controls !== undefined && controls != null) {
        jsonObj.controls = controls;
      } // Otherwise ignore...
      if(autoplayEnabled !== undefined && autoplayEnabled != null) {
        jsonObj.autoplayEnabled = autoplayEnabled;
      } // Otherwise ignore...
      if(loopEnabled !== undefined && loopEnabled != null) {
        jsonObj.loopEnabled = loopEnabled;
      } // Otherwise ignore...
      if(preloadEnabled !== undefined && preloadEnabled != null) {
        jsonObj.preloadEnabled = preloadEnabled;
      } // Otherwise ignore...
      if(muted !== undefined && muted != null) {
        jsonObj.muted = muted;
      } // Otherwise ignore...
      if(remark !== undefined && remark != null) {
        jsonObj.remark = remark;
      } // Otherwise ignore...
      if(source !== undefined && source != null) {
        jsonObj.source = source;
      } // Otherwise ignore...
      if(source1 !== undefined && source1 != null) {
        jsonObj.source1 = source1;
      } // Otherwise ignore...
      if(source2 !== undefined && source2 != null) {
        jsonObj.source2 = source2;
      } // Otherwise ignore...
      if(source3 !== undefined && source3 != null) {
        jsonObj.source3 = source3;
      } // Otherwise ignore...
      if(source4 !== undefined && source4 != null) {
        jsonObj.source4 = source4;
      } // Otherwise ignore...
      if(source5 !== undefined && source5 != null) {
        jsonObj.source5 = source5;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(id) {
        str += "\"id\":\"" + id + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"id\":null, ";
      }
      if(width) {
        str += "\"width\":" + width + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"width\":null, ";
      }
      if(height) {
        str += "\"height\":" + height + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"height\":null, ";
      }
      if(controls) {
        str += "\"controls\":\"" + controls + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"controls\":null, ";
      }
      if(autoplayEnabled) {
        str += "\"autoplayEnabled\":" + autoplayEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"autoplayEnabled\":null, ";
      }
      if(loopEnabled) {
        str += "\"loopEnabled\":" + loopEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"loopEnabled\":null, ";
      }
      if(preloadEnabled) {
        str += "\"preloadEnabled\":" + preloadEnabled + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"preloadEnabled\":null, ";
      }
      if(muted) {
        str += "\"muted\":" + muted + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"muted\":null, ";
      }
      if(remark) {
        str += "\"remark\":\"" + remark + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"remark\":null, ";
      }
      str += "\"source\":" + source.toJsonString() + ", ";
      str += "\"source1\":" + source1.toJsonString() + ", ";
      str += "\"source2\":" + source2.toJsonString() + ", ";
      str += "\"source3\":" + source3.toJsonString() + ", ";
      str += "\"source4\":" + source4.toJsonString() + ", ";
      str += "\"source5\":" + source5.toJsonString() + ", ";
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "id:" + id + ", ";
      str += "width:" + width + ", ";
      str += "height:" + height + ", ";
      str += "controls:" + controls + ", ";
      str += "autoplayEnabled:" + autoplayEnabled + ", ";
      str += "loopEnabled:" + loopEnabled + ", ";
      str += "preloadEnabled:" + preloadEnabled + ", ";
      str += "muted:" + muted + ", ";
      str += "remark:" + remark + ", ";
      str += "source:" + source + ", ";
      str += "source1:" + source1 + ", ";
      str += "source2:" + source2 + ", ";
      str += "source3:" + source3 + ", ";
      str += "source4:" + source4 + ", ";
      str += "source5:" + source5 + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.VideoStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.VideoStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.id !== undefined && obj.id != null) {
    o.setId(obj.id);
  }
  if(obj.width !== undefined && obj.width != null) {
    o.setWidth(obj.width);
  }
  if(obj.height !== undefined && obj.height != null) {
    o.setHeight(obj.height);
  }
  if(obj.controls !== undefined && obj.controls != null) {
    o.setControls(obj.controls);
  }
  if(obj.autoplayEnabled !== undefined && obj.autoplayEnabled != null) {
    o.setAutoplayEnabled(obj.autoplayEnabled);
  }
  if(obj.loopEnabled !== undefined && obj.loopEnabled != null) {
    o.setLoopEnabled(obj.loopEnabled);
  }
  if(obj.preloadEnabled !== undefined && obj.preloadEnabled != null) {
    o.setPreloadEnabled(obj.preloadEnabled);
  }
  if(obj.muted !== undefined && obj.muted != null) {
    o.setMuted(obj.muted);
  }
  if(obj.remark !== undefined && obj.remark != null) {
    o.setRemark(obj.remark);
  }
  if(obj.source !== undefined && obj.source != null) {
    o.setSource(obj.source);
  }
  if(obj.source1 !== undefined && obj.source1 != null) {
    o.setSource1(obj.source1);
  }
  if(obj.source2 !== undefined && obj.source2 != null) {
    o.setSource2(obj.source2);
  }
  if(obj.source3 !== undefined && obj.source3 != null) {
    o.setSource3(obj.source3);
  }
  if(obj.source4 !== undefined && obj.source4 != null) {
    o.setSource4(obj.source4);
  }
  if(obj.source5 !== undefined && obj.source5 != null) {
    o.setSource5(obj.source5);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

pagesynopsis.wa.bean.VideoStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.VideoStructJsBean.create(jsonObj);
  return obj;
};
