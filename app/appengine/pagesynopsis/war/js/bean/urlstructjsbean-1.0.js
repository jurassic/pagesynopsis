//////////////////////////////////////////////////////////
// <script src="/js/bean/urlstructjsbean-1.0.js"></script>
// Last modified time: 1374536952725.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.UrlStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var statusCode;
    var redirectUrl;
    var absoluteUrl;
    var hashFragment;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getStatusCode = function() { return statusCode; };
    this.setStatusCode = function(value) { statusCode = value; };
    this.getRedirectUrl = function() { return redirectUrl; };
    this.setRedirectUrl = function(value) { redirectUrl = value; };
    this.getAbsoluteUrl = function() { return absoluteUrl; };
    this.setAbsoluteUrl = function(value) { absoluteUrl = value; };
    this.getHashFragment = function() { return hashFragment; };
    this.setHashFragment = function(value) { hashFragment = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.UrlStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(statusCode !== undefined && statusCode != null) {
        o.setStatusCode(statusCode);
      }
      if(redirectUrl !== undefined && redirectUrl != null) {
        o.setRedirectUrl(redirectUrl);
      }
      if(absoluteUrl !== undefined && absoluteUrl != null) {
        o.setAbsoluteUrl(absoluteUrl);
      }
      if(hashFragment !== undefined && hashFragment != null) {
        o.setHashFragment(hashFragment);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(statusCode !== undefined && statusCode != null) {
        jsonObj.statusCode = statusCode;
      } // Otherwise ignore...
      if(redirectUrl !== undefined && redirectUrl != null) {
        jsonObj.redirectUrl = redirectUrl;
      } // Otherwise ignore...
      if(absoluteUrl !== undefined && absoluteUrl != null) {
        jsonObj.absoluteUrl = absoluteUrl;
      } // Otherwise ignore...
      if(hashFragment !== undefined && hashFragment != null) {
        jsonObj.hashFragment = hashFragment;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(statusCode) {
        str += "\"statusCode\":" + statusCode + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"statusCode\":null, ";
      }
      if(redirectUrl) {
        str += "\"redirectUrl\":\"" + redirectUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"redirectUrl\":null, ";
      }
      if(absoluteUrl) {
        str += "\"absoluteUrl\":\"" + absoluteUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"absoluteUrl\":null, ";
      }
      if(hashFragment) {
        str += "\"hashFragment\":\"" + hashFragment + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"hashFragment\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "statusCode:" + statusCode + ", ";
      str += "redirectUrl:" + redirectUrl + ", ";
      str += "absoluteUrl:" + absoluteUrl + ", ";
      str += "hashFragment:" + hashFragment + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.UrlStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.UrlStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.statusCode !== undefined && obj.statusCode != null) {
    o.setStatusCode(obj.statusCode);
  }
  if(obj.redirectUrl !== undefined && obj.redirectUrl != null) {
    o.setRedirectUrl(obj.redirectUrl);
  }
  if(obj.absoluteUrl !== undefined && obj.absoluteUrl != null) {
    o.setAbsoluteUrl(obj.absoluteUrl);
  }
  if(obj.hashFragment !== undefined && obj.hashFragment != null) {
    o.setHashFragment(obj.hashFragment);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

pagesynopsis.wa.bean.UrlStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.UrlStructJsBean.create(jsonObj);
  return obj;
};
