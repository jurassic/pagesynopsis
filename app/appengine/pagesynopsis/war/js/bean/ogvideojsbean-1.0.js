//////////////////////////////////////////////////////////
// <script src="/js/bean/ogvideojsbean-1.0.js"></script>
// Last modified time: 1374536953042.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.OgVideoJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var url;
    var type;
    var siteName;
    var title;
    var description;
    var fbAdmins;
    var fbAppId;
    var image;
    var audio;
    var video;
    var locale;
    var localeAlternate;
    var director;
    var writer;
    var actor;
    var duration;
    var tag;
    var releaseDate;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUrl = function() { return url; };
    this.setUrl = function(value) { url = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getSiteName = function() { return siteName; };
    this.setSiteName = function(value) { siteName = value; };
    this.getTitle = function() { return title; };
    this.setTitle = function(value) { title = value; };
    this.getDescription = function() { return description; };
    this.setDescription = function(value) { description = value; };
    this.getFbAdmins = function() { return fbAdmins; };
    this.setFbAdmins = function(value) { fbAdmins = value; };
    this.getFbAppId = function() { return fbAppId; };
    this.setFbAppId = function(value) { fbAppId = value; };
    this.getImage = function() { return image; };
    this.setImage = function(value) { image = value; };
    this.getAudio = function() { return audio; };
    this.setAudio = function(value) { audio = value; };
    this.getVideo = function() { return video; };
    this.setVideo = function(value) { video = value; };
    this.getLocale = function() { return locale; };
    this.setLocale = function(value) { locale = value; };
    this.getLocaleAlternate = function() { return localeAlternate; };
    this.setLocaleAlternate = function(value) { localeAlternate = value; };
    this.getDirector = function() { return director; };
    this.setDirector = function(value) { director = value; };
    this.getWriter = function() { return writer; };
    this.setWriter = function(value) { writer = value; };
    this.getActor = function() { return actor; };
    this.setActor = function(value) { actor = value; };
    this.getDuration = function() { return duration; };
    this.setDuration = function(value) { duration = value; };
    this.getTag = function() { return tag; };
    this.setTag = function(value) { tag = value; };
    this.getReleaseDate = function() { return releaseDate; };
    this.setReleaseDate = function(value) { releaseDate = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.OgVideoJsBean();

      o.setGuid(generateUuid());
      if(url !== undefined && url != null) {
        o.setUrl(url);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(siteName !== undefined && siteName != null) {
        o.setSiteName(siteName);
      }
      if(title !== undefined && title != null) {
        o.setTitle(title);
      }
      if(description !== undefined && description != null) {
        o.setDescription(description);
      }
      if(fbAdmins !== undefined && fbAdmins != null) {
        o.setFbAdmins(fbAdmins);
      }
      if(fbAppId !== undefined && fbAppId != null) {
        o.setFbAppId(fbAppId);
      }
      if(image !== undefined && image != null) {
        o.setImage(image);
      }
      if(audio !== undefined && audio != null) {
        o.setAudio(audio);
      }
      if(video !== undefined && video != null) {
        o.setVideo(video);
      }
      if(locale !== undefined && locale != null) {
        o.setLocale(locale);
      }
      if(localeAlternate !== undefined && localeAlternate != null) {
        o.setLocaleAlternate(localeAlternate);
      }
      if(director !== undefined && director != null) {
        o.setDirector(director);
      }
      if(writer !== undefined && writer != null) {
        o.setWriter(writer);
      }
      if(actor !== undefined && actor != null) {
        o.setActor(actor);
      }
      if(duration !== undefined && duration != null) {
        o.setDuration(duration);
      }
      if(tag !== undefined && tag != null) {
        o.setTag(tag);
      }
      if(releaseDate !== undefined && releaseDate != null) {
        o.setReleaseDate(releaseDate);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(url !== undefined && url != null) {
        jsonObj.url = url;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(siteName !== undefined && siteName != null) {
        jsonObj.siteName = siteName;
      } // Otherwise ignore...
      if(title !== undefined && title != null) {
        jsonObj.title = title;
      } // Otherwise ignore...
      if(description !== undefined && description != null) {
        jsonObj.description = description;
      } // Otherwise ignore...
      if(fbAdmins !== undefined && fbAdmins != null) {
        jsonObj.fbAdmins = fbAdmins;
      } // Otherwise ignore...
      if(fbAppId !== undefined && fbAppId != null) {
        jsonObj.fbAppId = fbAppId;
      } // Otherwise ignore...
      if(image !== undefined && image != null) {
        jsonObj.image = image;
      } // Otherwise ignore...
      if(audio !== undefined && audio != null) {
        jsonObj.audio = audio;
      } // Otherwise ignore...
      if(video !== undefined && video != null) {
        jsonObj.video = video;
      } // Otherwise ignore...
      if(locale !== undefined && locale != null) {
        jsonObj.locale = locale;
      } // Otherwise ignore...
      if(localeAlternate !== undefined && localeAlternate != null) {
        jsonObj.localeAlternate = localeAlternate;
      } // Otherwise ignore...
      if(director !== undefined && director != null) {
        jsonObj.director = director;
      } // Otherwise ignore...
      if(writer !== undefined && writer != null) {
        jsonObj.writer = writer;
      } // Otherwise ignore...
      if(actor !== undefined && actor != null) {
        jsonObj.actor = actor;
      } // Otherwise ignore...
      if(duration !== undefined && duration != null) {
        jsonObj.duration = duration;
      } // Otherwise ignore...
      if(tag !== undefined && tag != null) {
        jsonObj.tag = tag;
      } // Otherwise ignore...
      if(releaseDate !== undefined && releaseDate != null) {
        jsonObj.releaseDate = releaseDate;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(url) {
        str += "\"url\":\"" + url + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"url\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(siteName) {
        str += "\"siteName\":\"" + siteName + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"siteName\":null, ";
      }
      if(title) {
        str += "\"title\":\"" + title + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"title\":null, ";
      }
      if(description) {
        str += "\"description\":\"" + description + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"description\":null, ";
      }
      if(fbAdmins) {
        str += "\"fbAdmins\":\"" + fbAdmins + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fbAdmins\":null, ";
      }
      if(fbAppId) {
        str += "\"fbAppId\":\"" + fbAppId + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fbAppId\":null, ";
      }
      if(image) {
        str += "\"image\":\"" + image + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"image\":null, ";
      }
      if(audio) {
        str += "\"audio\":\"" + audio + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"audio\":null, ";
      }
      if(video) {
        str += "\"video\":\"" + video + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"video\":null, ";
      }
      if(locale) {
        str += "\"locale\":\"" + locale + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"locale\":null, ";
      }
      if(localeAlternate) {
        str += "\"localeAlternate\":\"" + localeAlternate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"localeAlternate\":null, ";
      }
      if(director) {
        str += "\"director\":\"" + director + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"director\":null, ";
      }
      if(writer) {
        str += "\"writer\":\"" + writer + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"writer\":null, ";
      }
      if(actor) {
        str += "\"actor\":\"" + actor + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"actor\":null, ";
      }
      if(duration) {
        str += "\"duration\":" + duration + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"duration\":null, ";
      }
      if(tag) {
        str += "\"tag\":\"" + tag + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"tag\":null, ";
      }
      if(releaseDate) {
        str += "\"releaseDate\":\"" + releaseDate + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"releaseDate\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "url:" + url + ", ";
      str += "type:" + type + ", ";
      str += "siteName:" + siteName + ", ";
      str += "title:" + title + ", ";
      str += "description:" + description + ", ";
      str += "fbAdmins:" + fbAdmins + ", ";
      str += "fbAppId:" + fbAppId + ", ";
      str += "image:" + image + ", ";
      str += "audio:" + audio + ", ";
      str += "video:" + video + ", ";
      str += "locale:" + locale + ", ";
      str += "localeAlternate:" + localeAlternate + ", ";
      str += "director:" + director + ", ";
      str += "writer:" + writer + ", ";
      str += "actor:" + actor + ", ";
      str += "duration:" + duration + ", ";
      str += "tag:" + tag + ", ";
      str += "releaseDate:" + releaseDate + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.OgVideoJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.OgVideoJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.url !== undefined && obj.url != null) {
    o.setUrl(obj.url);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.siteName !== undefined && obj.siteName != null) {
    o.setSiteName(obj.siteName);
  }
  if(obj.title !== undefined && obj.title != null) {
    o.setTitle(obj.title);
  }
  if(obj.description !== undefined && obj.description != null) {
    o.setDescription(obj.description);
  }
  if(obj.fbAdmins !== undefined && obj.fbAdmins != null) {
    o.setFbAdmins(obj.fbAdmins);
  }
  if(obj.fbAppId !== undefined && obj.fbAppId != null) {
    o.setFbAppId(obj.fbAppId);
  }
  if(obj.image !== undefined && obj.image != null) {
    o.setImage(obj.image);
  }
  if(obj.audio !== undefined && obj.audio != null) {
    o.setAudio(obj.audio);
  }
  if(obj.video !== undefined && obj.video != null) {
    o.setVideo(obj.video);
  }
  if(obj.locale !== undefined && obj.locale != null) {
    o.setLocale(obj.locale);
  }
  if(obj.localeAlternate !== undefined && obj.localeAlternate != null) {
    o.setLocaleAlternate(obj.localeAlternate);
  }
  if(obj.director !== undefined && obj.director != null) {
    o.setDirector(obj.director);
  }
  if(obj.writer !== undefined && obj.writer != null) {
    o.setWriter(obj.writer);
  }
  if(obj.actor !== undefined && obj.actor != null) {
    o.setActor(obj.actor);
  }
  if(obj.duration !== undefined && obj.duration != null) {
    o.setDuration(obj.duration);
  }
  if(obj.tag !== undefined && obj.tag != null) {
    o.setTag(obj.tag);
  }
  if(obj.releaseDate !== undefined && obj.releaseDate != null) {
    o.setReleaseDate(obj.releaseDate);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

pagesynopsis.wa.bean.OgVideoJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.OgVideoJsBean.create(jsonObj);
  return obj;
};
