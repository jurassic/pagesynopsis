//////////////////////////////////////////////////////////
// <script src="/js/bean/mediasourcestructjsbean-1.0.js"></script>
// Last modified time: 1374536952748.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.MediaSourceStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var src;
    var srcUrl;
    var type;
    var remark;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getSrc = function() { return src; };
    this.setSrc = function(value) { src = value; };
    this.getSrcUrl = function() { return srcUrl; };
    this.setSrcUrl = function(value) { srcUrl = value; };
    this.getType = function() { return type; };
    this.setType = function(value) { type = value; };
    this.getRemark = function() { return remark; };
    this.setRemark = function(value) { remark = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.MediaSourceStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(src !== undefined && src != null) {
        o.setSrc(src);
      }
      if(srcUrl !== undefined && srcUrl != null) {
        o.setSrcUrl(srcUrl);
      }
      if(type !== undefined && type != null) {
        o.setType(type);
      }
      if(remark !== undefined && remark != null) {
        o.setRemark(remark);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(src !== undefined && src != null) {
        jsonObj.src = src;
      } // Otherwise ignore...
      if(srcUrl !== undefined && srcUrl != null) {
        jsonObj.srcUrl = srcUrl;
      } // Otherwise ignore...
      if(type !== undefined && type != null) {
        jsonObj.type = type;
      } // Otherwise ignore...
      if(remark !== undefined && remark != null) {
        jsonObj.remark = remark;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(src) {
        str += "\"src\":\"" + src + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"src\":null, ";
      }
      if(srcUrl) {
        str += "\"srcUrl\":\"" + srcUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"srcUrl\":null, ";
      }
      if(type) {
        str += "\"type\":\"" + type + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"type\":null, ";
      }
      if(remark) {
        str += "\"remark\":\"" + remark + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"remark\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "src:" + src + ", ";
      str += "srcUrl:" + srcUrl + ", ";
      str += "type:" + type + ", ";
      str += "remark:" + remark + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.MediaSourceStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.MediaSourceStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.src !== undefined && obj.src != null) {
    o.setSrc(obj.src);
  }
  if(obj.srcUrl !== undefined && obj.srcUrl != null) {
    o.setSrcUrl(obj.srcUrl);
  }
  if(obj.type !== undefined && obj.type != null) {
    o.setType(obj.type);
  }
  if(obj.remark !== undefined && obj.remark != null) {
    o.setRemark(obj.remark);
  }
    
  return o;
};

pagesynopsis.wa.bean.MediaSourceStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.MediaSourceStructJsBean.create(jsonObj);
  return obj;
};
