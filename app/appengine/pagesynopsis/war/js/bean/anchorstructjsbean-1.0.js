//////////////////////////////////////////////////////////
// <script src="/js/bean/anchorstructjsbean-1.0.js"></script>
// Last modified time: 1374536952732.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.AnchorStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var id;
    var name;
    var href;
    var hrefUrl;
    var urlScheme;
    var rel;
    var target;
    var innerHtml;
    var anchorText;
    var anchorTitle;
    var anchorImage;
    var anchorLegend;
    var note;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getId = function() { return id; };
    this.setId = function(value) { id = value; };
    this.getName = function() { return name; };
    this.setName = function(value) { name = value; };
    this.getHref = function() { return href; };
    this.setHref = function(value) { href = value; };
    this.getHrefUrl = function() { return hrefUrl; };
    this.setHrefUrl = function(value) { hrefUrl = value; };
    this.getUrlScheme = function() { return urlScheme; };
    this.setUrlScheme = function(value) { urlScheme = value; };
    this.getRel = function() { return rel; };
    this.setRel = function(value) { rel = value; };
    this.getTarget = function() { return target; };
    this.setTarget = function(value) { target = value; };
    this.getInnerHtml = function() { return innerHtml; };
    this.setInnerHtml = function(value) { innerHtml = value; };
    this.getAnchorText = function() { return anchorText; };
    this.setAnchorText = function(value) { anchorText = value; };
    this.getAnchorTitle = function() { return anchorTitle; };
    this.setAnchorTitle = function(value) { anchorTitle = value; };
    this.getAnchorImage = function() { return anchorImage; };
    this.setAnchorImage = function(value) { anchorImage = value; };
    this.getAnchorLegend = function() { return anchorLegend; };
    this.setAnchorLegend = function(value) { anchorLegend = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.AnchorStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(id !== undefined && id != null) {
        o.setId(id);
      }
      if(name !== undefined && name != null) {
        o.setName(name);
      }
      if(href !== undefined && href != null) {
        o.setHref(href);
      }
      if(hrefUrl !== undefined && hrefUrl != null) {
        o.setHrefUrl(hrefUrl);
      }
      if(urlScheme !== undefined && urlScheme != null) {
        o.setUrlScheme(urlScheme);
      }
      if(rel !== undefined && rel != null) {
        o.setRel(rel);
      }
      if(target !== undefined && target != null) {
        o.setTarget(target);
      }
      if(innerHtml !== undefined && innerHtml != null) {
        o.setInnerHtml(innerHtml);
      }
      if(anchorText !== undefined && anchorText != null) {
        o.setAnchorText(anchorText);
      }
      if(anchorTitle !== undefined && anchorTitle != null) {
        o.setAnchorTitle(anchorTitle);
      }
      if(anchorImage !== undefined && anchorImage != null) {
        o.setAnchorImage(anchorImage);
      }
      if(anchorLegend !== undefined && anchorLegend != null) {
        o.setAnchorLegend(anchorLegend);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(id !== undefined && id != null) {
        jsonObj.id = id;
      } // Otherwise ignore...
      if(name !== undefined && name != null) {
        jsonObj.name = name;
      } // Otherwise ignore...
      if(href !== undefined && href != null) {
        jsonObj.href = href;
      } // Otherwise ignore...
      if(hrefUrl !== undefined && hrefUrl != null) {
        jsonObj.hrefUrl = hrefUrl;
      } // Otherwise ignore...
      if(urlScheme !== undefined && urlScheme != null) {
        jsonObj.urlScheme = urlScheme;
      } // Otherwise ignore...
      if(rel !== undefined && rel != null) {
        jsonObj.rel = rel;
      } // Otherwise ignore...
      if(target !== undefined && target != null) {
        jsonObj.target = target;
      } // Otherwise ignore...
      if(innerHtml !== undefined && innerHtml != null) {
        jsonObj.innerHtml = innerHtml;
      } // Otherwise ignore...
      if(anchorText !== undefined && anchorText != null) {
        jsonObj.anchorText = anchorText;
      } // Otherwise ignore...
      if(anchorTitle !== undefined && anchorTitle != null) {
        jsonObj.anchorTitle = anchorTitle;
      } // Otherwise ignore...
      if(anchorImage !== undefined && anchorImage != null) {
        jsonObj.anchorImage = anchorImage;
      } // Otherwise ignore...
      if(anchorLegend !== undefined && anchorLegend != null) {
        jsonObj.anchorLegend = anchorLegend;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(id) {
        str += "\"id\":\"" + id + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"id\":null, ";
      }
      if(name) {
        str += "\"name\":\"" + name + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"name\":null, ";
      }
      if(href) {
        str += "\"href\":\"" + href + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"href\":null, ";
      }
      if(hrefUrl) {
        str += "\"hrefUrl\":\"" + hrefUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"hrefUrl\":null, ";
      }
      if(urlScheme) {
        str += "\"urlScheme\":\"" + urlScheme + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"urlScheme\":null, ";
      }
      if(rel) {
        str += "\"rel\":\"" + rel + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"rel\":null, ";
      }
      if(target) {
        str += "\"target\":\"" + target + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"target\":null, ";
      }
      if(innerHtml) {
        str += "\"innerHtml\":\"" + innerHtml + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"innerHtml\":null, ";
      }
      if(anchorText) {
        str += "\"anchorText\":\"" + anchorText + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"anchorText\":null, ";
      }
      if(anchorTitle) {
        str += "\"anchorTitle\":\"" + anchorTitle + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"anchorTitle\":null, ";
      }
      if(anchorImage) {
        str += "\"anchorImage\":\"" + anchorImage + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"anchorImage\":null, ";
      }
      if(anchorLegend) {
        str += "\"anchorLegend\":\"" + anchorLegend + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"anchorLegend\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "id:" + id + ", ";
      str += "name:" + name + ", ";
      str += "href:" + href + ", ";
      str += "hrefUrl:" + hrefUrl + ", ";
      str += "urlScheme:" + urlScheme + ", ";
      str += "rel:" + rel + ", ";
      str += "target:" + target + ", ";
      str += "innerHtml:" + innerHtml + ", ";
      str += "anchorText:" + anchorText + ", ";
      str += "anchorTitle:" + anchorTitle + ", ";
      str += "anchorImage:" + anchorImage + ", ";
      str += "anchorLegend:" + anchorLegend + ", ";
      str += "note:" + note + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.AnchorStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.AnchorStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.id !== undefined && obj.id != null) {
    o.setId(obj.id);
  }
  if(obj.name !== undefined && obj.name != null) {
    o.setName(obj.name);
  }
  if(obj.href !== undefined && obj.href != null) {
    o.setHref(obj.href);
  }
  if(obj.hrefUrl !== undefined && obj.hrefUrl != null) {
    o.setHrefUrl(obj.hrefUrl);
  }
  if(obj.urlScheme !== undefined && obj.urlScheme != null) {
    o.setUrlScheme(obj.urlScheme);
  }
  if(obj.rel !== undefined && obj.rel != null) {
    o.setRel(obj.rel);
  }
  if(obj.target !== undefined && obj.target != null) {
    o.setTarget(obj.target);
  }
  if(obj.innerHtml !== undefined && obj.innerHtml != null) {
    o.setInnerHtml(obj.innerHtml);
  }
  if(obj.anchorText !== undefined && obj.anchorText != null) {
    o.setAnchorText(obj.anchorText);
  }
  if(obj.anchorTitle !== undefined && obj.anchorTitle != null) {
    o.setAnchorTitle(obj.anchorTitle);
  }
  if(obj.anchorImage !== undefined && obj.anchorImage != null) {
    o.setAnchorImage(obj.anchorImage);
  }
  if(obj.anchorLegend !== undefined && obj.anchorLegend != null) {
    o.setAnchorLegend(obj.anchorLegend);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
    
  return o;
};

pagesynopsis.wa.bean.AnchorStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.AnchorStructJsBean.create(jsonObj);
  return obj;
};
