//////////////////////////////////////////////////////////
// <script src="/js/bean/robotstextrefreshjsbean-1.0.js"></script>
// Last modified time: 1374536952812.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.RobotsTextRefreshJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var robotsTextFile;
    var refreshInterval;
    var note;
    var status;
    var refreshStatus;
    var result;
    var lastCheckedTime;
    var nextCheckedTime;
    var expirationTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getRobotsTextFile = function() { return robotsTextFile; };
    this.setRobotsTextFile = function(value) { robotsTextFile = value; };
    this.getRefreshInterval = function() { return refreshInterval; };
    this.setRefreshInterval = function(value) { refreshInterval = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getRefreshStatus = function() { return refreshStatus; };
    this.setRefreshStatus = function(value) { refreshStatus = value; };
    this.getResult = function() { return result; };
    this.setResult = function(value) { result = value; };
    this.getLastCheckedTime = function() { return lastCheckedTime; };
    this.setLastCheckedTime = function(value) { lastCheckedTime = value; };
    this.getNextCheckedTime = function() { return nextCheckedTime; };
    this.setNextCheckedTime = function(value) { nextCheckedTime = value; };
    this.getExpirationTime = function() { return expirationTime; };
    this.setExpirationTime = function(value) { expirationTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.RobotsTextRefreshJsBean();

      o.setGuid(generateUuid());
      if(robotsTextFile !== undefined && robotsTextFile != null) {
        o.setRobotsTextFile(robotsTextFile);
      }
      if(refreshInterval !== undefined && refreshInterval != null) {
        o.setRefreshInterval(refreshInterval);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(refreshStatus !== undefined && refreshStatus != null) {
        o.setRefreshStatus(refreshStatus);
      }
      if(result !== undefined && result != null) {
        o.setResult(result);
      }
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        o.setLastCheckedTime(lastCheckedTime);
      }
      if(nextCheckedTime !== undefined && nextCheckedTime != null) {
        o.setNextCheckedTime(nextCheckedTime);
      }
      if(expirationTime !== undefined && expirationTime != null) {
        o.setExpirationTime(expirationTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(robotsTextFile !== undefined && robotsTextFile != null) {
        jsonObj.robotsTextFile = robotsTextFile;
      } // Otherwise ignore...
      if(refreshInterval !== undefined && refreshInterval != null) {
        jsonObj.refreshInterval = refreshInterval;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(refreshStatus !== undefined && refreshStatus != null) {
        jsonObj.refreshStatus = refreshStatus;
      } // Otherwise ignore...
      if(result !== undefined && result != null) {
        jsonObj.result = result;
      } // Otherwise ignore...
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        jsonObj.lastCheckedTime = lastCheckedTime;
      } // Otherwise ignore...
      if(nextCheckedTime !== undefined && nextCheckedTime != null) {
        jsonObj.nextCheckedTime = nextCheckedTime;
      } // Otherwise ignore...
      if(expirationTime !== undefined && expirationTime != null) {
        jsonObj.expirationTime = expirationTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(robotsTextFile) {
        str += "\"robotsTextFile\":\"" + robotsTextFile + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"robotsTextFile\":null, ";
      }
      if(refreshInterval) {
        str += "\"refreshInterval\":" + refreshInterval + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshInterval\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(refreshStatus) {
        str += "\"refreshStatus\":" + refreshStatus + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"refreshStatus\":null, ";
      }
      if(result) {
        str += "\"result\":\"" + result + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"result\":null, ";
      }
      if(lastCheckedTime) {
        str += "\"lastCheckedTime\":" + lastCheckedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastCheckedTime\":null, ";
      }
      if(nextCheckedTime) {
        str += "\"nextCheckedTime\":" + nextCheckedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextCheckedTime\":null, ";
      }
      if(expirationTime) {
        str += "\"expirationTime\":" + expirationTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"expirationTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "robotsTextFile:" + robotsTextFile + ", ";
      str += "refreshInterval:" + refreshInterval + ", ";
      str += "note:" + note + ", ";
      str += "status:" + status + ", ";
      str += "refreshStatus:" + refreshStatus + ", ";
      str += "result:" + result + ", ";
      str += "lastCheckedTime:" + lastCheckedTime + ", ";
      str += "nextCheckedTime:" + nextCheckedTime + ", ";
      str += "expirationTime:" + expirationTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.RobotsTextRefreshJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.RobotsTextRefreshJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.robotsTextFile !== undefined && obj.robotsTextFile != null) {
    o.setRobotsTextFile(obj.robotsTextFile);
  }
  if(obj.refreshInterval !== undefined && obj.refreshInterval != null) {
    o.setRefreshInterval(obj.refreshInterval);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.refreshStatus !== undefined && obj.refreshStatus != null) {
    o.setRefreshStatus(obj.refreshStatus);
  }
  if(obj.result !== undefined && obj.result != null) {
    o.setResult(obj.result);
  }
  if(obj.lastCheckedTime !== undefined && obj.lastCheckedTime != null) {
    o.setLastCheckedTime(obj.lastCheckedTime);
  }
  if(obj.nextCheckedTime !== undefined && obj.nextCheckedTime != null) {
    o.setNextCheckedTime(obj.nextCheckedTime);
  }
  if(obj.expirationTime !== undefined && obj.expirationTime != null) {
    o.setExpirationTime(obj.expirationTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

pagesynopsis.wa.bean.RobotsTextRefreshJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.RobotsTextRefreshJsBean.create(jsonObj);
  return obj;
};
