//////////////////////////////////////////////////////////
// <script src="/js/bean/fetchrequestjsbean-1.0.js"></script>
// Last modified time: 1374536953268.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.FetchRequestJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var user;
    var targetUrl;
    var pageUrl;
    var queryString;
    var queryParams;
    var version;
    var fetchObject;
    var fetchStatus;
    var result;
    var note;
    var referrerInfo;
    var status;
    var nextPageUrl;
    var followPages;
    var followDepth;
    var createVersion;
    var deferred;
    var alert;
    var notificationPref;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getUser = function() { return user; };
    this.setUser = function(value) { user = value; };
    this.getTargetUrl = function() { return targetUrl; };
    this.setTargetUrl = function(value) { targetUrl = value; };
    this.getPageUrl = function() { return pageUrl; };
    this.setPageUrl = function(value) { pageUrl = value; };
    this.getQueryString = function() { return queryString; };
    this.setQueryString = function(value) { queryString = value; };
    this.getQueryParams = function() { return queryParams; };
    this.setQueryParams = function(value) { queryParams = value; };
    this.getVersion = function() { return version; };
    this.setVersion = function(value) { version = value; };
    this.getFetchObject = function() { return fetchObject; };
    this.setFetchObject = function(value) { fetchObject = value; };
    this.getFetchStatus = function() { return fetchStatus; };
    this.setFetchStatus = function(value) { fetchStatus = value; };
    this.getResult = function() { return result; };
    this.setResult = function(value) { result = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getReferrerInfo = function() { return referrerInfo; };
    this.setReferrerInfo = function(value) { referrerInfo = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getNextPageUrl = function() { return nextPageUrl; };
    this.setNextPageUrl = function(value) { nextPageUrl = value; };
    this.getFollowPages = function() { return followPages; };
    this.setFollowPages = function(value) { followPages = value; };
    this.getFollowDepth = function() { return followDepth; };
    this.setFollowDepth = function(value) { followDepth = value; };
    this.getCreateVersion = function() { return createVersion; };
    this.setCreateVersion = function(value) { createVersion = value; };
    this.getDeferred = function() { return deferred; };
    this.setDeferred = function(value) { deferred = value; };
    this.getAlert = function() { return alert; };
    this.setAlert = function(value) { alert = value; };
    this.getNotificationPref = function() { return notificationPref; };
    this.setNotificationPref = function(value) { notificationPref = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.FetchRequestJsBean();

      o.setGuid(generateUuid());
      if(user !== undefined && user != null) {
        o.setUser(user);
      }
      if(targetUrl !== undefined && targetUrl != null) {
        o.setTargetUrl(targetUrl);
      }
      if(pageUrl !== undefined && pageUrl != null) {
        o.setPageUrl(pageUrl);
      }
      if(queryString !== undefined && queryString != null) {
        o.setQueryString(queryString);
      }
      if(queryParams !== undefined && queryParams != null) {
        o.setQueryParams(queryParams);
      }
      if(version !== undefined && version != null) {
        o.setVersion(version);
      }
      if(fetchObject !== undefined && fetchObject != null) {
        o.setFetchObject(fetchObject);
      }
      if(fetchStatus !== undefined && fetchStatus != null) {
        o.setFetchStatus(fetchStatus);
      }
      if(result !== undefined && result != null) {
        o.setResult(result);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      //o.setReferrerInfo(referrerInfo.clone());
      if(referrerInfo !== undefined && referrerInfo != null) {
        o.setReferrerInfo(referrerInfo);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(nextPageUrl !== undefined && nextPageUrl != null) {
        o.setNextPageUrl(nextPageUrl);
      }
      if(followPages !== undefined && followPages != null) {
        o.setFollowPages(followPages);
      }
      if(followDepth !== undefined && followDepth != null) {
        o.setFollowDepth(followDepth);
      }
      if(createVersion !== undefined && createVersion != null) {
        o.setCreateVersion(createVersion);
      }
      if(deferred !== undefined && deferred != null) {
        o.setDeferred(deferred);
      }
      if(alert !== undefined && alert != null) {
        o.setAlert(alert);
      }
      //o.setNotificationPref(notificationPref.clone());
      if(notificationPref !== undefined && notificationPref != null) {
        o.setNotificationPref(notificationPref);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(user !== undefined && user != null) {
        jsonObj.user = user;
      } // Otherwise ignore...
      if(targetUrl !== undefined && targetUrl != null) {
        jsonObj.targetUrl = targetUrl;
      } // Otherwise ignore...
      if(pageUrl !== undefined && pageUrl != null) {
        jsonObj.pageUrl = pageUrl;
      } // Otherwise ignore...
      if(queryString !== undefined && queryString != null) {
        jsonObj.queryString = queryString;
      } // Otherwise ignore...
      if(queryParams !== undefined && queryParams != null) {
        jsonObj.queryParams = queryParams;
      } // Otherwise ignore...
      if(version !== undefined && version != null) {
        jsonObj.version = version;
      } // Otherwise ignore...
      if(fetchObject !== undefined && fetchObject != null) {
        jsonObj.fetchObject = fetchObject;
      } // Otherwise ignore...
      if(fetchStatus !== undefined && fetchStatus != null) {
        jsonObj.fetchStatus = fetchStatus;
      } // Otherwise ignore...
      if(result !== undefined && result != null) {
        jsonObj.result = result;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(referrerInfo !== undefined && referrerInfo != null) {
        jsonObj.referrerInfo = referrerInfo;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(nextPageUrl !== undefined && nextPageUrl != null) {
        jsonObj.nextPageUrl = nextPageUrl;
      } // Otherwise ignore...
      if(followPages !== undefined && followPages != null) {
        jsonObj.followPages = followPages;
      } // Otherwise ignore...
      if(followDepth !== undefined && followDepth != null) {
        jsonObj.followDepth = followDepth;
      } // Otherwise ignore...
      if(createVersion !== undefined && createVersion != null) {
        jsonObj.createVersion = createVersion;
      } // Otherwise ignore...
      if(deferred !== undefined && deferred != null) {
        jsonObj.deferred = deferred;
      } // Otherwise ignore...
      if(alert !== undefined && alert != null) {
        jsonObj.alert = alert;
      } // Otherwise ignore...
      if(notificationPref !== undefined && notificationPref != null) {
        jsonObj.notificationPref = notificationPref;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(user) {
        str += "\"user\":\"" + user + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"user\":null, ";
      }
      if(targetUrl) {
        str += "\"targetUrl\":\"" + targetUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"targetUrl\":null, ";
      }
      if(pageUrl) {
        str += "\"pageUrl\":\"" + pageUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"pageUrl\":null, ";
      }
      if(queryString) {
        str += "\"queryString\":\"" + queryString + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryString\":null, ";
      }
      if(queryParams) {
        str += "\"queryParams\":\"" + queryParams + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"queryParams\":null, ";
      }
      if(version) {
        str += "\"version\":\"" + version + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"version\":null, ";
      }
      if(fetchObject) {
        str += "\"fetchObject\":\"" + fetchObject + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchObject\":null, ";
      }
      if(fetchStatus) {
        str += "\"fetchStatus\":" + fetchStatus + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"fetchStatus\":null, ";
      }
      if(result) {
        str += "\"result\":\"" + result + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"result\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      str += "\"referrerInfo\":" + referrerInfo.toJsonString() + ", ";
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(nextPageUrl) {
        str += "\"nextPageUrl\":\"" + nextPageUrl + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"nextPageUrl\":null, ";
      }
      if(followPages) {
        str += "\"followPages\":" + followPages + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"followPages\":null, ";
      }
      if(followDepth) {
        str += "\"followDepth\":" + followDepth + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"followDepth\":null, ";
      }
      if(createVersion) {
        str += "\"createVersion\":" + createVersion + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createVersion\":null, ";
      }
      if(deferred) {
        str += "\"deferred\":" + deferred + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"deferred\":null, ";
      }
      if(alert) {
        str += "\"alert\":" + alert + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"alert\":null, ";
      }
      str += "\"notificationPref\":" + notificationPref.toJsonString() + ", ";
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "user:" + user + ", ";
      str += "targetUrl:" + targetUrl + ", ";
      str += "pageUrl:" + pageUrl + ", ";
      str += "queryString:" + queryString + ", ";
      str += "queryParams:" + queryParams + ", ";
      str += "version:" + version + ", ";
      str += "fetchObject:" + fetchObject + ", ";
      str += "fetchStatus:" + fetchStatus + ", ";
      str += "result:" + result + ", ";
      str += "note:" + note + ", ";
      str += "referrerInfo:" + referrerInfo + ", ";
      str += "status:" + status + ", ";
      str += "nextPageUrl:" + nextPageUrl + ", ";
      str += "followPages:" + followPages + ", ";
      str += "followDepth:" + followDepth + ", ";
      str += "createVersion:" + createVersion + ", ";
      str += "deferred:" + deferred + ", ";
      str += "alert:" + alert + ", ";
      str += "notificationPref:" + notificationPref + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.FetchRequestJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.FetchRequestJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.user !== undefined && obj.user != null) {
    o.setUser(obj.user);
  }
  if(obj.targetUrl !== undefined && obj.targetUrl != null) {
    o.setTargetUrl(obj.targetUrl);
  }
  if(obj.pageUrl !== undefined && obj.pageUrl != null) {
    o.setPageUrl(obj.pageUrl);
  }
  if(obj.queryString !== undefined && obj.queryString != null) {
    o.setQueryString(obj.queryString);
  }
  if(obj.queryParams !== undefined && obj.queryParams != null) {
    o.setQueryParams(obj.queryParams);
  }
  if(obj.version !== undefined && obj.version != null) {
    o.setVersion(obj.version);
  }
  if(obj.fetchObject !== undefined && obj.fetchObject != null) {
    o.setFetchObject(obj.fetchObject);
  }
  if(obj.fetchStatus !== undefined && obj.fetchStatus != null) {
    o.setFetchStatus(obj.fetchStatus);
  }
  if(obj.result !== undefined && obj.result != null) {
    o.setResult(obj.result);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.referrerInfo !== undefined && obj.referrerInfo != null) {
    o.setReferrerInfo(obj.referrerInfo);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.nextPageUrl !== undefined && obj.nextPageUrl != null) {
    o.setNextPageUrl(obj.nextPageUrl);
  }
  if(obj.followPages !== undefined && obj.followPages != null) {
    o.setFollowPages(obj.followPages);
  }
  if(obj.followDepth !== undefined && obj.followDepth != null) {
    o.setFollowDepth(obj.followDepth);
  }
  if(obj.createVersion !== undefined && obj.createVersion != null) {
    o.setCreateVersion(obj.createVersion);
  }
  if(obj.deferred !== undefined && obj.deferred != null) {
    o.setDeferred(obj.deferred);
  }
  if(obj.alert !== undefined && obj.alert != null) {
    o.setAlert(obj.alert);
  }
  if(obj.notificationPref !== undefined && obj.notificationPref != null) {
    o.setNotificationPref(obj.notificationPref);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

pagesynopsis.wa.bean.FetchRequestJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.FetchRequestJsBean.create(jsonObj);
  return obj;
};
