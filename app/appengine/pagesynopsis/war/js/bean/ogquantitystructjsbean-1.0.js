//////////////////////////////////////////////////////////
// <script src="/js/bean/ogquantitystructjsbean-1.0.js"></script>
// Last modified time: 1374536952881.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.OgQuantityStructJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var uuid;
    var value;
    var units;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getUuid = function() { return uuid; };
    this.setUuid = function(value) { uuid = value; };
    this.getValue = function() { return value; };
    this.setValue = function(value) { value = value; };
    this.getUnits = function() { return units; };
    this.setUnits = function(value) { units = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.OgQuantityStructJsBean();

      if(uuid !== undefined && uuid != null) {
        o.setUuid(uuid);
      }
      if(value !== undefined && value != null) {
        o.setValue(value);
      }
      if(units !== undefined && units != null) {
        o.setUnits(units);
      }
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(uuid !== undefined && uuid != null) {
        jsonObj.uuid = uuid;
      } // Otherwise ignore...
      if(value !== undefined && value != null) {
        jsonObj.value = value;
      } // Otherwise ignore...
      if(units !== undefined && units != null) {
        jsonObj.units = units;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(uuid) {
        str += "\"uuid\":\"" + uuid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"uuid\":null, ";
      }
      if(value) {
        str += "\"value\":" + value + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"value\":null, ";
      }
      if(units) {
        str += "\"units\":\"" + units + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"units\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "uuid:" + uuid + ", ";
      str += "value:" + value + ", ";
      str += "units:" + units + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.OgQuantityStructJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.OgQuantityStructJsBean();

  if(obj.uuid !== undefined && obj.uuid != null) {
    o.setUuid(obj.uuid);
  }
  if(obj.value !== undefined && obj.value != null) {
    o.setValue(obj.value);
  }
  if(obj.units !== undefined && obj.units != null) {
    o.setUnits(obj.units);
  }
    
  return o;
};

pagesynopsis.wa.bean.OgQuantityStructJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.OgQuantityStructJsBean.create(jsonObj);
  return obj;
};
