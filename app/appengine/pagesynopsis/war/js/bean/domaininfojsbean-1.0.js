//////////////////////////////////////////////////////////
// <script src="/js/bean/domaininfojsbean-1.0.js"></script>
// Last modified time: 1374536953380.
//////////////////////////////////////////////////////////

var pagesynopsis = pagesynopsis || {};
pagesynopsis.wa = pagesynopsis.wa || {};
pagesynopsis.wa.bean = pagesynopsis.wa.bean || {};
pagesynopsis.wa.bean.DomainInfoJsBean = ( function() {

  /////////////////////////////
  // Utility methods
  /////////////////////////////

  var generateUuid = function() {
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
    return uuid;
  };
  
  var getCurrentTime = function() {
	return (new Date()).getTime();
  };


  /////////////////////////////
  // Constructor
  /////////////////////////////

  var cls = function() {

    // Private vars.
    var guid = generateUuid();
    var domain;
    var excluded;
    var category;
    var reputation;
    var authority;
    var note;
    var status;
    var lastCheckedTime;
    var lastUpdatedTime;
    var createdTime = getCurrentTime();
    var modifiedTime;


    /////////////////////////////
    // Getters/Setters.
    /////////////////////////////

    this.getGuid = function() { return guid; };
    this.setGuid = function(value) { guid = value; };
    this.getDomain = function() { return domain; };
    this.setDomain = function(value) { domain = value; };
    this.getExcluded = function() { return excluded; };
    this.setExcluded = function(value) { excluded = value; };
    this.getCategory = function() { return category; };
    this.setCategory = function(value) { category = value; };
    this.getReputation = function() { return reputation; };
    this.setReputation = function(value) { reputation = value; };
    this.getAuthority = function() { return authority; };
    this.setAuthority = function(value) { authority = value; };
    this.getNote = function() { return note; };
    this.setNote = function(value) { note = value; };
    this.getStatus = function() { return status; };
    this.setStatus = function(value) { status = value; };
    this.getLastCheckedTime = function() { return lastCheckedTime; };
    this.setLastCheckedTime = function(value) { lastCheckedTime = value; };
    this.getLastUpdatedTime = function() { return lastUpdatedTime; };
    this.setLastUpdatedTime = function(value) { lastUpdatedTime = value; };
    this.getCreatedTime = function() { return createdTime; };
    this.setCreatedTime = function(value) { createdTime = value; };
    this.getModifiedTime = function() { return modifiedTime; };
    this.setModifiedTime = function(value) { modifiedTime = value; };


    /////////////////////////////
    // Convenience methods
    /////////////////////////////
    
    // Clone this bean.
    this._clone = function() {
      var o = new pagesynopsis.wa.bean.DomainInfoJsBean();

      o.setGuid(generateUuid());
      if(domain !== undefined && domain != null) {
        o.setDomain(domain);
      }
      if(excluded !== undefined && excluded != null) {
        o.setExcluded(excluded);
      }
      if(category !== undefined && category != null) {
        o.setCategory(category);
      }
      if(reputation !== undefined && reputation != null) {
        o.setReputation(reputation);
      }
      if(authority !== undefined && authority != null) {
        o.setAuthority(authority);
      }
      if(note !== undefined && note != null) {
        o.setNote(note);
      }
      if(status !== undefined && status != null) {
        o.setStatus(status);
      }
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        o.setLastCheckedTime(lastCheckedTime);
      }
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        o.setLastUpdatedTime(lastUpdatedTime);
      }
      o.setCreatedTime(getCurrentTime());
    
      return o;
    };
    this.clone = function() {
        return this._clone();
    };

    // This will be called by JSON.stringify().
    this._toJSON = function() {
      var jsonObj = {};

      if(guid !== undefined && guid != null) {
        jsonObj.guid = guid;
      } // Otherwise ignore...
      if(domain !== undefined && domain != null) {
        jsonObj.domain = domain;
      } // Otherwise ignore...
      if(excluded !== undefined && excluded != null) {
        jsonObj.excluded = excluded;
      } // Otherwise ignore...
      if(category !== undefined && category != null) {
        jsonObj.category = category;
      } // Otherwise ignore...
      if(reputation !== undefined && reputation != null) {
        jsonObj.reputation = reputation;
      } // Otherwise ignore...
      if(authority !== undefined && authority != null) {
        jsonObj.authority = authority;
      } // Otherwise ignore...
      if(note !== undefined && note != null) {
        jsonObj.note = note;
      } // Otherwise ignore...
      if(status !== undefined && status != null) {
        jsonObj.status = status;
      } // Otherwise ignore...
      if(lastCheckedTime !== undefined && lastCheckedTime != null) {
        jsonObj.lastCheckedTime = lastCheckedTime;
      } // Otherwise ignore...
      if(lastUpdatedTime !== undefined && lastUpdatedTime != null) {
        jsonObj.lastUpdatedTime = lastUpdatedTime;
      } // Otherwise ignore...
      if(createdTime !== undefined && createdTime != null) {
        jsonObj.createdTime = createdTime;
      } // Otherwise ignore...
      if(modifiedTime !== undefined && modifiedTime != null) {
        jsonObj.modifiedTime = modifiedTime;
      } // Otherwise ignore...

      return jsonObj;
    };
    this.toJSON = function() {
        return this._toJSON();
    };

/*
    // TBD: Use Jackson Json parser/generator?
    this.toJsonString = function() {
      var str = "{ ";

      if(guid) {
        str += "\"guid\":\"" + guid + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"guid\":null, ";
      }
      if(domain) {
        str += "\"domain\":\"" + domain + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"domain\":null, ";
      }
      if(excluded) {
        str += "\"excluded\":" + excluded + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"excluded\":null, ";
      }
      if(category) {
        str += "\"category\":\"" + category + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"category\":null, ";
      }
      if(reputation) {
        str += "\"reputation\":\"" + reputation + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"reputation\":null, ";
      }
      if(authority) {
        str += "\"authority\":\"" + authority + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"authority\":null, ";
      }
      if(note) {
        str += "\"note\":\"" + note + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"note\":null, ";
      }
      if(status) {
        str += "\"status\":\"" + status + "\", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"status\":null, ";
      }
      if(lastCheckedTime) {
        str += "\"lastCheckedTime\":" + lastCheckedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastCheckedTime\":null, ";
      }
      if(lastUpdatedTime) {
        str += "\"lastUpdatedTime\":" + lastUpdatedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"lastUpdatedTime\":null, ";
      }
      if(createdTime) {
        str += "\"createdTime\":" + createdTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"createdTime\":null, ";
      }
      if(modifiedTime) {
        str += "\"modifiedTime\":" + modifiedTime + ", ";
      } else {
        // TBD: Omit or include these fields?
        // str += "\"modifiedTime\":null, ";
      }

      // Remove the trailing comma.
      if(str.match(/, $/)) {
        str = str.substr(0, str.length - 2);
      }

      str += " }";
      return str;
    };
*/

    /////////////////////////////
    // For debugging.
    /////////////////////////////

    this._toString = function() {
      var str = "";
    
      str += "guid:" + guid + ", ";
      str += "domain:" + domain + ", ";
      str += "excluded:" + excluded + ", ";
      str += "category:" + category + ", ";
      str += "reputation:" + reputation + ", ";
      str += "authority:" + authority + ", ";
      str += "note:" + note + ", ";
      str += "status:" + status + ", ";
      str += "lastCheckedTime:" + lastCheckedTime + ", ";
      str += "lastUpdatedTime:" + lastUpdatedTime + ", ";
      str += "createdTime:" + createdTime + ", ";
      str += "modifiedTime:" + modifiedTime + ", ";

      return str;
    };
    this.toString = function() {
        return this._toString();
    };

  };

  return cls;
})();


/////////////////////////////
// Create
/////////////////////////////

pagesynopsis.wa.bean.DomainInfoJsBean.create = function(obj) {
  var o = new pagesynopsis.wa.bean.DomainInfoJsBean();

  if(obj.guid !== undefined && obj.guid != null) {
    o.setGuid(obj.guid);
  }
  if(obj.domain !== undefined && obj.domain != null) {
    o.setDomain(obj.domain);
  }
  if(obj.excluded !== undefined && obj.excluded != null) {
    o.setExcluded(obj.excluded);
  }
  if(obj.category !== undefined && obj.category != null) {
    o.setCategory(obj.category);
  }
  if(obj.reputation !== undefined && obj.reputation != null) {
    o.setReputation(obj.reputation);
  }
  if(obj.authority !== undefined && obj.authority != null) {
    o.setAuthority(obj.authority);
  }
  if(obj.note !== undefined && obj.note != null) {
    o.setNote(obj.note);
  }
  if(obj.status !== undefined && obj.status != null) {
    o.setStatus(obj.status);
  }
  if(obj.lastCheckedTime !== undefined && obj.lastCheckedTime != null) {
    o.setLastCheckedTime(obj.lastCheckedTime);
  }
  if(obj.lastUpdatedTime !== undefined && obj.lastUpdatedTime != null) {
    o.setLastUpdatedTime(obj.lastUpdatedTime);
  }
  if(obj.createdTime !== undefined && obj.createdTime != null) {
    o.setCreatedTime(obj.createdTime);
  }
  if(obj.modifiedTime !== undefined && obj.modifiedTime != null) {
    o.setModifiedTime(obj.modifiedTime);
  }
    
  return o;
};

pagesynopsis.wa.bean.DomainInfoJsBean.fromJSON = function(jsonStr) {
  var jsonObj = JSON.parse(jsonStr);
  var obj = pagesynopsis.wa.bean.DomainInfoJsBean.create(jsonObj);
  return obj;
};
