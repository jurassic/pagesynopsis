<%@ page import="com.pagesynopsis.ws.core.*, com.pagesynopsis.af.auth.*, com.pagesynopsis.fe.*, com.pagesynopsis.fe.core.*, com.pagesynopsis.fe.bean.*, com.pagesynopsis.fe.util.*, com.pagesynopsis.wa.service.*, com.pagesynopsis.util.*, com.pagesynopsis.app.util.*, com.pagesynopsis.helper.*" 
%><%@ page contentType="text/html; charset=UTF-8" 
%><%
//{1] Session
SessionBean sessionBean = UserSessionManager.getInstance().setSessionBean(request, response);
String sessionToken = sessionBean.getToken();
String userId = sessionBean.getUserId();

// [2] Parse url.
String requestUrl = request.getRequestURL().toString();
String topLevelUrl = URLHelper.getInstance().getTopLevelURLFromRequestURL(requestUrl);
// ...
%><%
// "Branding"
String appBrand = BrandingHelper.getInstance().getAppBrand();
String brandDisplayName = BrandingHelper.getInstance().getBrandDisplayName();
%><%
//[3] Local "global" variables.
//EncodedQueryParamStructJsBean encodedQueryParamBean = null;
//DecodedQueryParamStructJsBean decodedQueryParamBean = null;
// ...
%><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><%=brandDisplayName%> | Home - Fetch Web page title and description through Web Services API</title>
    <meta name="author" content="Aery software">
    <meta name="description" content="<%=brandDisplayName%> Service is a web page summary service. Service is exposed through REST Web services API. Currently, it uses HTML page meta tags.">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/bootstrap/blueridge/bootstrap-2.0.css">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="http://www.filestoa.com/css/jquery-ui-1.9.2/pepper-grinder/jquery-ui-1.9.2.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-<%=appBrand%>.css"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-screen.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/css/pagesynopsis-mobile.css" media="only screen and (max-device-width: 480px)"/>
<!-- 
    <link href="/css/bootstrap.responsive-2.0.css" rel="stylesheet">
 -->

    <!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="/img/favicon-pagesynopsis.ico" />
	<link rel="icon" href="/img/favicon-pagesynopsis.ico" type="image/x-icon" />

    <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
    <script src="http://www.filestoa.com/js/modernizr/modernizr-2.0.6.min.js"></script>

    <!--  Twitter @anywhere API  -->
    <script src="http://platform.twitter.com/anywhere.js?id=aQv8kescAfPXIVijKI63Iw&v=1" type="text/javascript"></script>
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="/"><%=brandDisplayName%><sup>&beta;</sup></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/page/pageinfo">Page Info</a></li>
              <li><a href="/page/linklist">Link List</a></li>
              <li><a href="/page/imageset">Image Set</a></li>
<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
<%
}
%>
              <li><a href="/urlcoder">URL Encoder</a></li>
            </ul>
            <p class="navbar-text pull-right">
              <a id="anchor_topmenu_tweet" href="#" title="Share it on Twitter"><i class="icon-retweet icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_share" href="#" title="Email it to your friend"><i class="icon-share icon-white"></i></a>&nbsp;
              <a id="anchor_topmenu_email" href="contact:info+pagesynopsis+com?subject=Re: <%=brandDisplayName%>" title="Email us"><i class="icon-envelope icon-white"></i></a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
<h1><%=brandDisplayName%><sup>&beta;</sup></h1>

<p>
PageSynopsis is a simple web service/API for retrieving Web page title and description, among other things.
(Note: It is a service for developers/coders and it is not an application intended for end users.)
</p>
        <p><a id="anchor_mainmenu_signup" class="btn btn-primary btn-large" title="Please provide your email address, and we will keep you posted on upcoming pre-release trials at <%=brandDisplayName%>.">Sign up &raquo;</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row-fluid">
        <div class="span6">
<h2>Page Info</h2>

<p>
<%=brandDisplayName%> (tm) can be used to programmatically retrieve Web page information such as title and meta description, etc.
The page information is updated every 7 days (as of this writing). 
It exposes its functionalities through REST-based Web services API.
Currently, all APIs, except for one GET method, are protected, through two-legged OAuth.
Please let us know if you would like your own consumer key/secret pair.
Full API includes all four REST methods (GET, POST, PUT, and DELETE)
for various resource types (undocumented, at this point).
It supports XML and JSON formats for both input (through Accept header) and output (through Content-type header).
</p>


<h2>How to Fetch HTML Page Title?</h2>

<p>
In general, retrieving Web page title and description (or, other DOM elements)
is a straightforward task.
At least, in principle. Here's the gist:<br/>
(1) First fetch the web page for the given URL; and<br/>
(2) Parse the output stream (e.g, through DOM or SAX APIs etc.), 
and find the title, meta "description", and other desired elements.<br/>
<%=brandDisplayName%> provides this functionality as a Web service.
</p>
<p>
There seem to be a number of decent HTML parsers in almost every (widely-used) programming language.
PageSynopsis Web Service is implemented in Java,
using <a href="http://jersey.java.net/">Java Jersey</a> (JAX-RS) as Web services layer,
and it relies on <a href="http://htmlunit.sourceforge.net/">HtmlUnit</a> for parsing HTML documents.
HtmlUnit supports <a href="http://www.w3.org/TR/xpath/">XPath</a> syntax for retrieving desired DOM elements. 
</p>

<!--
          <p><a class="btn" href="/reminder">View details &raquo;</a></p>
-->
       </div>
 
        <div class="span6">


<!-- 
<div class="alert alert-error alert-block">
  <a class="close" data-dismiss="alert" href="#">×</a>
  <h3 class="alert-heading">NOTICE</h3>
<p>
Due to the limitations in our framework (which relies on Google App Engine DataStore),
the "LinkList" and "ImageSet" APIs can no longer be supported at this point.
We are waiting for the GAE team to release the JDO 3.0 enhancement.
</p>
</div>
 -->



<h2>Link List (Experimental)</h2>
<p>
<%=brandDisplayName%> can also be used to automatically retrieve all anchors/links from a given Web page.
It exposes its functionalities through the same REST-based Web services API.
Currently, all APIs, except for one GET method, are protected, through two-legged OAuth.
Full API includes all four REST methods (GET, POST, PUT, and DELETE)
for various resource types (undocumented, at this point).
It supports XML and JSON formats for both input (through Accept header) and output (through Content-type header).
Fetching all anchors/links from a generic HTML page turns out to be 
a substantially more complicated task than retrieving just the page title and description.
This API is experimental, and it is currently under development.
</p>

<h2>Image Set (Experimental)</h2>
<p>
We have added one more "public API" for fetching image tags from HTML pages.
The public API supports JSON format as an output, and it takes a mandatory query param, "targetUrl".
(You can use an optional param "fetch=false" to stop the target Web page being retrieved.
That is, only the cached data from database, if any, will be returned.) 
This API is experimental, and it is currently under development.
</p>

<%
if(DebugUtil.isDevelFeatureEnabled()) {
%>
<%
}
%>


<h2>Caveat</h2>

<p>
PageSynopsis is a work in progress, and it has many limitations.
First, and probably most importantly, the current implementation does not honor the Web site's "robots" policy.
This is on top of our to-do list, 
but meanwhile please beware of this when you use <%=brandDisplayName%> API and exercise caution.
</p>


<h2>Future APIs</h2>

<p>
Writing a general purpose, and robust, screen scraper for HTML pages "in the wild"
is a rather complicated task, almost comparable to writing a Web browser, in some sense.
Extracting certain particular DOM elements appear easier than others.
If you have any need for retrieving certain subset of elements from Web pages,
please let us know, and we will incorporate it into our road map.
(Please note that this is a very low priority project for us, however.)
</p>

        <p><a id="anchor_bodymenu_signup" class="btn" title="Please let us know if you have any feature request. If you sign up, we will also keep you posted on our future development.">API Request &raquo;</a></p>

        </div>
      </div>

      <hr>

      <footer>
        <p>
        &copy; <%=brandDisplayName%> 2012&nbsp; 
        <a id="anchor_feedback_footer" href="#" title="Feedback"><i class="icon-comment"></i></a>
        <span class="pull-right">
<!-- 
              <a href="/resource">Resource</a> | 
              <a href="/about">About</a> | 
-->
        <a href="/contact">Contact</a> | <a href="http://blog.pagesynopsis.com/">Blog</a>
        </span>
        </p>
      </footer>

    </div> <!-- /container -->



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="http://www.filestoa.com/js/jquery/jquery-1.8.3.min.js"><\/script>')</script>
	<script src="http://www.filestoa.com/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="http://www.filestoa.com/js/jquery/plugin/jquery.floatobject-1.4.js"></script>
    <script src="http://www.filestoa.com/js/bootstrap/bootstrap-2.0.js"></script>
    <script type="text/javascript" src="/js/core/debugutil.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/datetimeutil-1.0.js"></script>
    <script type="text/javascript" src="http://www.filestoa.com/js/core/emailutil-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/statushelper-1.1.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/tweethelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/signuphelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/feedbackhelper-1.0.js"></script>
    <script defer type="text/javascript" src="http://www.filestoa.com/js/util/emailhelper-1.0.js"></script>


    <script>
        // Global vars
    	var statusHelper;
    	var tweetHelper;
    	var signupHelper;
    	var feedbackHelper;
    	var emailHelper;
    </script>
    <script>
    $(function() {
    	// Init...
    	statusHelper = new webstoa.StatusHelper(400, 65);
    	// test.
    	//statusHelper.update('test status message');

    	var servicename = "<%=brandDisplayName%>";
    	var launchname = "Initial pre-launch trial";
    	var pageid = "home";
    	signupHelper = new webstoa.SignupHelper(servicename, launchname, pageid);

    	var targetService = "<%=brandDisplayName%>";
    	var targetPage = "home";
    	feedbackHelper = new webstoa.FeedbackHelper(targetService, targetPage);

    	var senderEmail = webstoa.EmailUtil.decodeEmailAddress('gaeemail+gmail' + '+com');
    	var defaultSenderName = "<%=brandDisplayName%>";
    	var subject = "Interesting site: <%=brandDisplayName%>";
    	var defaultMessage = "Hi,\n\n<%=brandDisplayName%>, http://www.pagesynopsis.com/, is a Web service for fetching HTML page title and description. Please check it out.\n\n-me\n";
    	emailHelper = new webstoa.EmailHelper(senderEmail, subject, defaultMessage, defaultSenderName);
    });
    </script>

    <script>
    // Email address "decoding"...
    $(function() {
        $("a").each(function() {
        	var anchor = $(this);
        	webstoa.EmailUtil.decodeMailtoAnchor(anchor);
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_mainmenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
        $("#anchor_bodymenu_signup").click(function() {
            if(DEBUG_ENABLED) console.log("Body Signup button clicked."); 

       	    signupHelper.signup();
       	    //statusHelper.update('Thanks for signing up.');
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_share").click(function() {
            if(DEBUG_ENABLED) console.log("Share button clicked."); 
       	    emailHelper.email();
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_feedback_footer").click(function() {
            if(DEBUG_ENABLED) console.log("Feedback button clicked."); 

            var targetItem = 'footer';
            feedbackHelper.feedback(targetItem);
		    return false;
        });
    });
    </script>

    <script>
    $(function() {
        $("#anchor_topmenu_tweet").click(function() {
            if(DEBUG_ENABLED) console.log("Tweet button clicked."); 

            if(!tweetHelper) {
                tweetHelper = new webstoa.TweetHelper();
            }
        	if(tweetHelper) {
	            var tweetTitle = '<%=brandDisplayName%> Message';
	            var tweetMessage = '<%=brandDisplayName%>, http://www.pagesynopsis.com/, is a Web service for fetching HTML page title and description. Please check it out.';
	            tweetHelper.tweet(tweetTitle, tweetMessage);
        	} else {
        		// ????
        	}
		    return false;
        });
    });
    </script>



<%
String googleAnalyticsCode = AnalyticsHelper.getInstance().getGoogleAnalyticsCode();
if(googleAnalyticsCode != null && !googleAnalyticsCode.isEmpty()) {    // And, if it is enabled (e.g. through config). TBD...
%>
    <!-- Change UA-XXXXX-X to be your site's ID -->
    <script>
      window._gaq = [['_setAccount','<%=googleAnalyticsCode%>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
<%
}
%>

  </body>
</html>
