package com.pagesynopsis.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.af.util.URLUtil;
import com.pagesynopsis.fe.bean.DecodedQueryParamStructJsBean;
import com.pagesynopsis.fe.bean.EncodedQueryParamStructJsBean;
import com.pagesynopsis.util.JsonBeanUtil;
import com.pagesynopsis.wa.service.UserWebService;
import com.pagesynopsis.ws.core.StatusCode;


// Mainly, for ajax calls....
public class URLCoderServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(URLCoderServlet.class.getName());

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    // etc. ...

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");

        
        // TBD: Check Accept header. Etc...
        // For now, both input and output are application/json.
        EncodedQueryParamStructJsBean outEncodedQueryParamStructBean = null;
        DecodedQueryParamStructJsBean outDecodedQueryParamStructBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();

            EncodedQueryParamStructJsBean inEncodedQueryParamStructBean = null;
            DecodedQueryParamStructJsBean inDecodedQueryParamStructBean = null;
            Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
            for(String key : jsonObjectMap.keySet()) {
                String json = jsonObjectMap.get(key);
                log.info("key = " + key + "; json = " + json);
                
                if(key.equals("encodedQueryParamStruct")) {
                    inEncodedQueryParamStructBean = EncodedQueryParamStructJsBean.fromJsonString(json);
                } else if(key.equals("decodedQueryParamStruct")) {
                        inDecodedQueryParamStructBean = DecodedQueryParamStructJsBean.fromJsonString(json);
                } else {
                    // TBD:
                    // This should not happen, for now.
                    log.warning("Unregconized key in the json input string. key = " + key);
                }
            }

            // TBD: Make sure at least one is not null???
            // Or, just process what is inputted....
            
            // temporary!!!
            if(inEncodedQueryParamStructBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // ????

                String originalString = inEncodedQueryParamStructBean.getOriginalString();
                if(originalString == null || originalString.isEmpty()) {
                    // What to do???
                }

                if(originalString != null && !originalString.isEmpty()) {
                    // Clone??
                    outEncodedQueryParamStructBean = new EncodedQueryParamStructJsBean(inEncodedQueryParamStructBean);

                    // TBD:
                    // Check paramType.
                    // URL encode the original string based on paramType...
                    // ...
                    
                    String paramType = outEncodedQueryParamStructBean.getParamType();
                    log.info("paramType = " + paramType);

                    String encodedString = null;
                    if("param_value".equals(paramType)) {
                        encodedString = URLEncoder.encode(originalString, "UTF-8");                        
                    } else if("param_pair".equals(paramType)) {
                        String[] pair = originalString.split("=", 2);
                        encodedString = URLEncoder.encode(pair[0], "UTF-8");
                        if(pair.length > 1) {
                            encodedString += "=" + URLEncoder.encode(pair[1], "UTF-8");
                        }
                    } else if("param_query".equals(paramType)) {
                        encodedString = encodeQueryString(originalString);
                    } else if("param_url".equals(paramType)) {
//                        String[] parts = originalString.split("\\?", 2);
//                        if(parts.length < 2) {
//                            // ????
//                            if(URLUtil.isValidUrl(originalString)) {
//                                encodedString = originalString;
//                            } else {
//                                // ???
//                                encodedString = encodeQueryString(originalString);
//                            }
//                        } else {
//                            // ????
//                            encodedString = parts[0] + "?" + encodeQueryString(parts[1]);
//                        }
                        
//                        String[] parts = originalString.split("\\?", 2);
//                        String base = parts[0];
//                        String queryStr = null;
//                        if(parts.length == 2) {
//                            queryStr = parts[1];
//                        }
//                        URI baseUri = new URI(base);
//                        if(queryStr == null) {
//                            encodedString = baseUri.toString();
//                        } else {
//                            // Hack... URI has such a horrendous API....
//                            URI fullUri = new URI(baseUri.getScheme(), baseUri.getAuthority(), baseUri.getPath(), queryStr, null); 
//                            if(fullUri != null) {
//                                encodedString = fullUri.toString();
//                            }
//                        }                        

                        String[] parts = originalString.split("\\?", 2);
                        String base = parts[0];
                        String encodedQuery = null;
                        if(parts.length == 2) {
                            encodedQuery = encodeQueryString(parts[1]);
                        }
                        // temporary
                        // String encodedBase =  base.replaceAll("\\s", "%20");
                        String encodedBase =  base;
                        // etc...
                        // temporary
                        if(encodedQuery != null) {
                            encodedString = encodedBase + "?" + encodedQuery;
                        } else {
                            encodedString = encodedBase;
                        }
                        
                    } else {
                        // ???
                        log.warning("Unrecognized param type: " + paramType);
                        encodedString = "";   // ????
                    }

                    outEncodedQueryParamStructBean.setEncodedString(encodedString);
                } else {
                    // else ignore... ????           
                }
            }

            // temporary!!!
            if(inDecodedQueryParamStructBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // ????

                String originalString = inDecodedQueryParamStructBean.getOriginalString();
                if(originalString == null || originalString.isEmpty()) {
                    // What to do???
                }

                if(originalString != null && !originalString.isEmpty()) {
                    // Clone??
                    outDecodedQueryParamStructBean = new DecodedQueryParamStructJsBean(inDecodedQueryParamStructBean);

                    // TBD:
                    // Check paramType.
                    // Param type is currently ignored.
                    // (Do we ever need it during decoding????)
                    // ...
                    
                    String paramType = outDecodedQueryParamStructBean.getParamType();
                    log.info("paramType = " + paramType);

                    String decodedString = null;
                    if("param_value".equals(paramType)) {
                        decodedString = URLDecoder.decode(originalString, "UTF-8");                        
                    } else if("param_pair".equals(paramType)) {
                        String[] pair = originalString.split("=", 2);
                        decodedString = URLDecoder.decode(pair[0], "UTF-8");
                        if(pair.length > 1) {
                            decodedString += "=" + URLDecoder.decode(pair[1], "UTF-8");
                        }
                    } else if("param_query".equals(paramType)) {
                        // decodedString = decodeQueryString(originalString);
                        decodedString = URLDecoder.decode(originalString, "UTF-8");
                    } else if("param_url".equals(paramType)) {
//                        String[] parts = originalString.split("\\?", 2);
//                        if(parts.length < 2) {
//                            // ????
//                            if(URLUtil.isValidUrl(originalString)) {
//                                decodedString = originalString;
//                            } else {
//                                // decodedString = decodeQueryString(originalString);
//                                decodedString = URLDecoder.decode(originalString, "UTF-8");
//                            }
//                        } else {
//                            // decodedString = parts[0] + "?" + decodeQueryString(parts[1]);
//                            decodedString = parts[0] + "?" + URLDecoder.decode(parts[1], "UTF-8");
//                        }

                        String[] parts = originalString.split("\\?", 2);
                        String base = parts[0];
                        String decodedQuery = null;
                        if(parts.length == 2) {
                            decodedQuery = URLDecoder.decode(parts[1], "UTF-8");
                        }
                        // temporary
                        // String decodedBase = base.replaceAll("\\%20", " ");
                        String decodedBase = base;
                        // etc...
                        // temporary
                        if(decodedQuery != null) {
                            decodedString = decodedBase + "?" + decodedQuery;
                        } else {
                            decodedString = decodedBase;
                        }
                    
                    } else {
                        // ???
                        log.warning("Unrecognized param type: " + paramType);
                        decodedString = "";   // ????
                    }

                    // TBD:
                    // Error handling????
                    // Note that decoding, as opoosed to encoding, can be more error-prone...
                    // for example, originalString might not be a properly URL encoded string....
                    // ...

                    outDecodedQueryParamStructBean.setDecodedString(decodedString);
                } else {
                    // else ignore... ????           
                }
            }

            // ...

        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to save encodedQueryParamStruct.", e);
        }
        
        // temporary
        boolean suc = false;
        if(outEncodedQueryParamStructBean != null || outDecodedQueryParamStructBean != null) {  // ???
            suc = true;
        }

        // Response.
        if(suc == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            //resp.setHeader("Cache-Control", "no-cache");    // ?????

            // Location header???
            //String guid = outEncodedQueryParamStructBean.getGuid();
            
            // Construct output
            Map<String, String> jsonMap = new HashMap<String, String>();
            if(outEncodedQueryParamStructBean != null) {
                jsonMap.put("encodedQueryParamStruct", outEncodedQueryParamStructBean.toJsonString());
            }
            if(outDecodedQueryParamStructBean != null) {
                jsonMap.put("decodedQueryParamStruct", outDecodedQueryParamStructBean.toJsonString());
            }
            String output = JsonBeanUtil.generateJsonObjectString(jsonMap);

            PrintWriter out = resp.getWriter();
            out.write(output);
        } else {
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
        }

 
    
        log.info("doPost(): BOTTOM");
    }
    

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
        
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    private String encodeQueryString(String originalString)
    {
        String[] params = originalString.split("&");
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<params.length; i++) {
            String[] pair = params[i].split("=", 2);
            try {
                String row = URLEncoder.encode(pair[0], "UTF-8");
                if(pair.length > 1) {
                    row += "=" + URLEncoder.encode(pair[1], "UTF-8");
                }
                sb.append(row);
            } catch (UnsupportedEncodingException e) {
                log.log(Level.WARNING, "Error, ignored", e);
            }
            if(i < params.length-1) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

}
