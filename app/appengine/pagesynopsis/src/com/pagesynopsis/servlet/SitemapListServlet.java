package com.pagesynopsis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.app.robotcheck.RobotChecker;
import com.pagesynopsis.ws.core.StatusCode;


// Mainly, for ajax calls....
public class SitemapListServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SitemapListServlet.class.getName());

    // temporary
    private static final String QUERY_PARAM_SITE_URL = "siteUrl";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doGet() called.");

        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String siteUrl = null;
        String[] siteUrls = req.getParameterValues(QUERY_PARAM_SITE_URL);
        if(siteUrls != null && siteUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            siteUrl = URLDecoder.decode(siteUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the siteUrl???
            // ....
        } else {
            // error...
            log.warning("Required arg, siteUrl, is missing");
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_SITE_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_SITE_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("siteUrl = " + siteUrl);

        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        
        
        List<String> sitemaps = RobotChecker.getInstance().getSitemapList(siteUrl);
        if(log.isLoggable(Level.INFO)) log.info("sitemaps = " + sitemaps + " for siteUrl = " + siteUrl);

        if(sitemaps != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for(int i=0; i<sitemaps.size(); i++) {
                sb.append("\"").append(sitemaps.get(i)).append("\"");
                if(i < sitemaps.size()-1 ) {
                    sb.append(",");
                }
            }
            sb.append("]");
            String jsonStr = sb.toString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            // Arbitrary. Can this happen???
            resp.setStatus(StatusCode.NOT_FOUND);
        }
    }

}
