package com.pagesynopsis.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.af.core.JerseyClient;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.app.util.QueryStringUtil;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.PageFetchJsBean;
import com.pagesynopsis.helper.PageFetchHelper;
import com.pagesynopsis.helper.QueryParamHelper;
import com.pagesynopsis.helper.URLHelper;
import com.pagesynopsis.util.JsonBeanUtil;
import com.pagesynopsis.wa.service.PageFetchWebService;
import com.pagesynopsis.wa.service.UserWebService;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StatusCode;


// Mainly, for ajax calls....
// Payload: { "pageFetch": pageFetchBean, "trialLaunch": trialLaunchBean }
public class PageFetchServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageFetchServlet.class.getName());

    // Arbitrary time cutoff...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    // Arbitrary cutoff. Note: 500 is the max length for String type...
    //private static final int SUMMARY_LENGTH_CUTOFF = 450;

    // temporary
    private static final String QUERY_PARAM_TARGET_URL = "targetUrl";
    private static final String QUERY_PARAM_FETCH = "fetch";          // default: true. If false, do not fetch it if the data does not exist in the DB.
    private static final String QUERY_PARAM_REFRESH = "refresh";      // In seconds.. If set, always fetch/refresh. Do not check DB first.
    private static final String QUERY_PARAM_REDIRECTS = "redirects";  // 0 means no redirect follow...
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private PageFetchWebService pageFetchWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private PageFetchWebService getPageFetchService()
    {
        if(pageFetchWebService == null) {
            pageFetchWebService = new PageFetchWebService();
        }
        return pageFetchWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
        
        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String targetUrl = null;
        String[] targetUrls = req.getParameterValues(QUERY_PARAM_TARGET_URL);
        if(targetUrls != null && targetUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            targetUrl = URLDecoder.decode(targetUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the targetUrl???
            // ....
        } else {
            // error...
            log.warning("Required arg, targetUrl, is missing");
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_TARGET_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("targetUrl = " + targetUrl);
        
        // TBD: Truncate or Throw error????
        if(targetUrl.length() > 500) {
            if(log.isLoggable(Level.WARNING)) log.warning("targetUrl is too long: " + targetUrl);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;            
        }
        
        boolean fetch = true;   // default value.
        String[] fetchStrs = req.getParameterValues(QUERY_PARAM_FETCH);
        if(fetchStrs != null && fetchStrs.length > 0) {
            if(fetchStrs[0].equals("0") || fetchStrs[0].equalsIgnoreCase("false")) {  // temporary
                fetch = false;
            }
            // else ignore.
        }
        log.fine("fetch = " + fetch);

        boolean redirectParamFound = false;
        int maxRedirects = 0;
        if(req.getParameterMap().containsKey(QUERY_PARAM_REDIRECTS)) {   // TBD: Is there a better way?
            redirectParamFound = true;
            String[] redirectStrs = req.getParameterValues(QUERY_PARAM_REDIRECTS);
            if(redirectStrs != null && redirectStrs.length > 0) {
                try {
                    int inputRedirects = Integer.parseInt(redirectStrs[0]);
                    maxRedirects = inputRedirects;
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.WARNING, "Redirect caount param: " + redirectStrs[0], e);
                }
            }
            if(maxRedirects < 0) {
                maxRedirects = 0;
            }
        }
        log.fine("maxRedirects = " + maxRedirects);
        
        boolean refreshFirst = false;
        Long refreshInterval = null;
        int refreshSecs = 0;
        if(req.getParameterMap().containsKey(QUERY_PARAM_REFRESH)) {   // TBD: Is there a better way?
            refreshFirst = true;
            String[] refreshStrs = req.getParameterValues(QUERY_PARAM_REFRESH);
            if(refreshStrs != null && refreshStrs.length > 0) {
                try {
                    refreshSecs = Integer.parseInt(refreshStrs[0]);   // This could be 0 or -1....
                    if(refreshSecs > 0) {
                        refreshInterval = refreshSecs * 1000L;   // In millisecs...
                    }
                } catch(NumberFormatException e) {
                    // ignore
                    log.log(Level.WARNING, "Invalid refresh param: " + refreshStrs[0], e);
                }
            }
            // else ignore...
        }
        log.fine("refreshFirst = " + refreshFirst + "; refreshInterval = " + refreshInterval);

        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        Long createdTimeCutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        PageFetchJsBean pageFetch = null;
        if(refreshFirst == false || refreshSecs > -1) {
            pageFetch = PageFetchHelper.getInstance().getPageFetchByTargetUrl(targetUrl, createdTimeCutoff);
        }
        if(pageFetch != null) {
            if(redirectParamFound) {
                pageFetch.setInputMaxRedirects(maxRedirects);
            }
        }

        // TBD:
        // Check current refreshStatus? Or, at least last refreshed/checked time, etc.... ???
        // Otherwise we may keep returning very old record (which is never being updated/refresh....)
        // .....

        // Whether to create/update records in the DB...
        boolean update = false;   // Do not update by default...
        boolean create = true;    // Create, if update==false, by default... If update==true, this flag is not used...

        if(pageFetch != null) {
            create = false;
            if(refreshFirst == true && refreshInterval != null) {
                update = true;
            } else {
                if(redirectParamFound) {    // ????
                    // update = true;       // ????     Do NOT do this...
                } else {
                    // update = false;  // default...
                }
            }
        } else {
            if(refreshFirst == true && refreshSecs == -1) {
                create = false;
                // update = false;  // default...
            } else {
                // create = true;   // default....
            }
        }

        if(refreshFirst == true || pageFetch == null) {
            if(refreshFirst == true || fetch == true) {
                if(update == false) {
                    pageFetch = new PageFetchJsBean();
                    String guid = GUID.generate();
                    pageFetch.setGuid(guid);
                    pageFetch.setTargetUrl(targetUrl);
                    pageFetch.setInputMaxRedirects(maxRedirects);
                    try {
                        URL url = new URL(targetUrl);  // ???
                        String query = url.getQuery();
                        String pageUrl = targetUrl;
                        if(query != null) {
                            int qidx = targetUrl.indexOf("?");
                            if(qidx > 0) {
                                pageUrl = targetUrl.substring(0, qidx);
                            }
                        }
                        pageFetch.setPageUrl(pageUrl);
                        if(query != null && !query.isEmpty()) {
                            pageFetch.setQueryString(query); 
                            List<KeyValuePairStructJsBean> parameters = QueryParamHelper.getInstance().parseQueryString(query);
                            pageFetch.setQueryParams(parameters);
                        }
                    } catch(MalformedURLException e) {
                        log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                        // ignore and continue???? or, bail out???
                        resp.setStatus(StatusCode.BAD_REQUEST);
                        return;
                    }
                }
                if(redirectParamFound==true || (update==false && create==true)) {    // ????
                    // Either redirects explicitly set in the request, or we are creating a new fetchRequest.
                    pageFetch.setInputMaxRedirects(maxRedirects);  // Could be zero...
                } else {
                    // Don't change...
                }
                pageFetch.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  /// ???
                if(refreshInterval != null) {
                    pageFetch.setRefreshInterval(refreshInterval);
                }
                
                PageFetch bean = PageFetchWebService.convertPageFetchJsBeanToBean(pageFetch);
                try {
                    bean = FetchProcessor.getInstance().processPageFetchFetch(bean);
                    pageFetch = PageFetchWebService.convertPageFetchToJsBean(bean);
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Failed to fetch the page: targetUrl = " + targetUrl, e);
                    pageFetch = null;
                }
                
                if(pageFetch != null) {
                    // Need to to do this to avoid double processing.
                    pageFetch.setDeferred(true);
                    // ....
                    try {
                        if(update) {
                            pageFetch = getPageFetchService().refreshPageFetch(pageFetch);
                        } else {
                            if(create) {
                                pageFetch = getPageFetchService().constructPageFetch(pageFetch);
                            }
                        }
                    } catch (WebException e) {
                        log.log(Level.WARNING, "Failed to save the pageFetch with targetUrl = " + targetUrl, e);
                        pageFetch = null;  // ???
                    }
                } 
            } else {
                // ???
            }
        }
        
        if(pageFetch != null) {
            String jsonStr = pageFetch.toJsonString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    
    
    // Note:
    // Currently, only GET is "officially" supported...
    // ...
    
    
    // TBD:
    // depending on pageFetch.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");

        // TBD: Check Accept header. Etc...
        // For now, both input and output are application/json.
        PageFetchJsBean outPageFetchBean = null;
        try {
            BufferedReader reader = req.getReader();
            String jsonStr = reader.readLine();

            PageFetchJsBean inPageFetchBean = null;
            Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
            for(String key : jsonObjectMap.keySet()) {
                String json = jsonObjectMap.get(key);
                log.info("key = " + key + "; json = " + json);
                
                if(key.equals("pageFetch")) {
                    inPageFetchBean = PageFetchJsBean.fromJsonString(json);
                } else {
                    // TBD:
                    // This should not happen, for now.
                    log.warning("Unregconized key in the json input string. key = " + key);
                }
            }

            // TBD: Make sure neither is null???
            // Or, just process what is inputted....
            
            // temporary!!!
            if(inPageFetchBean != null) {
                // TBD: Validate and populate some missing fields, etc...
                // ????
                // temporary....
                // etc. ...
                // ...

                outPageFetchBean = getPageFetchService().constructPageFetch(inPageFetchBean);
            } else {
                // ????
                
            }
            
            // ...

        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to save pageFetch.", e);
        }
        
        // temporary
        boolean suc = false;
        if(outPageFetchBean != null) {
            suc = true;
        }

        // Response.
        if(suc == true) {
            resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            resp.setContentType("application/json;charset=UTF-8");
            //resp.setHeader("Cache-Control", "no-cache");    // ?????

            // Location header???
            //String guid = outPageFetchBean.getGuid();
            
            // Construct output
            Map<String, String> jsonMap = new HashMap<String, String>();
            if(outPageFetchBean != null) {
                jsonMap.put("pageFetch", outPageFetchBean.toJsonString());
            }
            String output = JsonBeanUtil.generateJsonObjectString(jsonMap);

            PrintWriter out = resp.getWriter();
            out.write(output);
        } else {
            resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
        }

        log.info("doPost(): BOTTOM");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPut(): TOP");

        // TBD: Check Accept header. Etc...

        String pathInfo = req.getPathInfo();
        String guid = URLHelper.getInstance().getGuidFromPathInfo(pathInfo);        

        // guid should not be null!!!
        if(guid != null && !guid.isEmpty()) {
            Boolean suc = null;
            try {
                BufferedReader reader = req.getReader();
                String jsonStr = reader.readLine();
                
                PageFetchJsBean inPageFetchBean = null;
                Map<String,String> jsonObjectMap = JsonBeanUtil.parseJsonObjectString(jsonStr);
                for(String key : jsonObjectMap.keySet()) {
                    String json = jsonObjectMap.get(key);
                    log.info("key = " + key + "; json = " + json);
                    
                    if(key.equals("pageFetch")) {
                        inPageFetchBean = PageFetchJsBean.fromJsonString(json);
                    } else {
                        // TBD:
                        // This should not happen, for now.
                        log.warning("Unregconized key in the json input string. key = " + key);
                    }
                }

                // TBD: Make sure neither is null???
                // Or, just process what is inputted....
                // ....
                

                Boolean sucPageFetch = null;
                if(inPageFetchBean != null) {
                    String beanGuid = inPageFetchBean.getGuid();
                    if(guid.equals(beanGuid)) {
                        inPageFetchBean.setModifiedTime(System.currentTimeMillis());
                        sucPageFetch = getPageFetchService().updatePageFetch(inPageFetchBean);
                        log.finer("sucPageFetch = " + sucPageFetch);
                    } else {
                        log.log(Level.WARNING, "Inconsistent input. pathGuid = " + guid + "; beanGuid = " + beanGuid);
                        // ???
                    }
                }

                // TBD: ...
                if(Boolean.TRUE.equals(sucPageFetch)) {  // ????
                    suc = Boolean.TRUE;
                }
                // ...

            } catch (WebException e) {
                log.log(Level.WARNING, "Failed.", e);
            }
            if(Boolean.TRUE.equals(suc)) {
                resp.setStatus(StatusCode.OK);   // StatusCode.ACCEPTED ?
            } else {
                resp.setStatus(StatusCode.INTERNAL_SERVER_ERROR);  // ????
            }
        } else {
            // ???
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }

        log.info("doPut(): BOTTOM");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
