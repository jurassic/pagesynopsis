package com.pagesynopsis.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.af.core.JerseyClient;
import com.pagesynopsis.af.resource.impl.RobotsTextFileResourceImpl;
import com.pagesynopsis.app.fetch.RobotsTextProcessor;
import com.pagesynopsis.app.util.QueryStringUtil;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.KeyValuePairStructJsBean;
import com.pagesynopsis.fe.bean.RobotsTextFileJsBean;
import com.pagesynopsis.helper.RobotsTextFileHelper;
import com.pagesynopsis.helper.QueryParamHelper;
import com.pagesynopsis.helper.URLHelper;
import com.pagesynopsis.util.JsonBeanUtil;
import com.pagesynopsis.util.RobotsTextConstants;
import com.pagesynopsis.wa.service.RobotsTextFileWebService;
import com.pagesynopsis.wa.service.UserWebService;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;


// Mainly, for ajax calls....
// Payload: { "robotsTextFile": robotsTextFileBean, "trialLaunch": trialLaunchBean }
public class RobotsTextFileServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileServlet.class.getName());

    // temporary
    private static final String QUERY_PARAM_SITE_URL = "siteUrl";
    private static final String QUERY_PARAM_JSONP_CALLBACK1 = "jsonp";
    private static final String QUERY_PARAM_JSONP_CALLBACK2 = "callback";

    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private RobotsTextFileWebService robotsTextFileWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private RobotsTextFileWebService getRobotsTextFileService()
    {
        if(robotsTextFileWebService == null) {
            robotsTextFileWebService = new RobotsTextFileWebService();
        }
        return robotsTextFileWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singleton instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //throw new ServletException("Not implemented.");
        log.info("doGet(): TOP");
        
        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        log.fine("requestUrl = " + requestUrl);
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);
        log.fine("queryString = " + queryString);

        String siteUrl = null;
        String[] siteUrls = req.getParameterValues(QUERY_PARAM_SITE_URL);
        if(siteUrls != null && siteUrls.length > 0) {
            // TBD: Support multiple target urls???
            
            // TBD: the query param is already url decoded!!!
            // ...
            // TBD: comment this out on the next deployment
            // ....
            siteUrl = URLDecoder.decode(siteUrls[0], "UTF-8");  // ?????
            // TBD:
            // "canonicalize" the siteUrl???
            // ....
        } else {
            // error...
            log.warning("Required arg, siteUrl, is missing");
            //throw new ResourceNotFoundRsException("");
            //throw new BadRequestRsException("Query parameter is missing, " + QUERY_PARAM_SITE_URL);
            //throw new ServletException("Query parameter is missing, " + QUERY_PARAM_SITE_URL);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("siteUrl = " + siteUrl);
        // TBD: siteUrl validation???
        if( !(siteUrl.startsWith("http://") || siteUrl.startsWith("https://")) ) {
            if(log.isLoggable(Level.WARNING)) log.warning("Invalid siteUrl = " + siteUrl);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        // TBD: Pick domain part only?
        //      Or, throw error if includes path???
        // ????
        // We store siteUrl without robots.txt part....
        if(siteUrl.endsWith("robots.txt")) {
            siteUrl = siteUrl.substring(0, siteUrl.lastIndexOf("robots.txt"));
            if(log.isLoggable(Level.INFO)) log.info("siteUrl modified: " + siteUrl);
        }
        // ...
        // TBD: Truncate or Throw error????
        if(siteUrl.length() > 500) {
            if(log.isLoggable(Level.WARNING)) log.warning("siteUrl is too long: " + siteUrl);
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;            
        }

        // JSONP support
        String jsonpCallback = null;
        String[] callbacks1 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK1);
        if(callbacks1 != null && callbacks1.length > 0) {
            jsonpCallback = callbacks1[0];  // ???
        }
        if(jsonpCallback == null || jsonpCallback.isEmpty()) {
            String[] callbacks2 = req.getParameterValues(QUERY_PARAM_JSONP_CALLBACK2);
            if(callbacks2 != null && callbacks2.length > 0) {
                jsonpCallback = callbacks2[0];  // ???
            }
        }        

        // Find an existing RobotsText, if any...
        RobotsTextFileJsBean robotsTextFile = RobotsTextFileHelper.getInstance().getRobotsTextFileBySiteUrl(siteUrl);


        Long lastCheckedTime = null;
        if(robotsTextFile != null) {
            lastCheckedTime = robotsTextFile.getLastCheckedTime();
            // String contentHash = stub.getContentHash();
        } else {
            // create = true;   // default....
        }

        long now = System.currentTimeMillis();
        if(lastCheckedTime != null && lastCheckedTime > now - RobotsTextConstants.MAX_AGE_MILLIS) {
            // just return the exiting stub
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Returning robotsTextFile from DB: siteUrl = " + siteUrl);
        } else {
            try {
                RobotsTextFile bean = null;
                if(robotsTextFile != null) {
                    bean = RobotsTextFileWebService.convertRobotsTextFileJsBeanToBean(robotsTextFile);
                    bean = RobotsTextProcessor.getInstance().processRobotsTextFile(bean);
                    if(bean != null) {
                        robotsTextFile = RobotsTextFileWebService.convertRobotsTextFileToJsBean(bean);
                    }
                    // Update, because it's pretty old...
                    if(robotsTextFile != null) {
//                        // Need to to do this to avoid double processing.
//                        robotsTextFile.setDeferred(true);
//                        // ....
                        robotsTextFile = getRobotsTextFileService().refreshRobotsTextFile(robotsTextFile);   
                    }                    
                } else {
                    bean = RobotsTextProcessor.getInstance().processRobotsTextFetch(siteUrl);
                    if(bean != null) {
                        robotsTextFile = RobotsTextFileWebService.convertRobotsTextFileToJsBean(bean);
                    }
                    if(robotsTextFile != null) {
//                        // Need to to do this to avoid double processing.
//                        robotsTextFile.setDeferred(true);
//                        // ....
                        robotsTextFile = getRobotsTextFileService().constructRobotsTextFile(robotsTextFile);
                    }
                }
                // ...
            } catch (BaseException e) {
                // Ignore
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process robotsTextFile for siteUrl = " + siteUrl, e);
            } catch (WebException e) {
                // Ignore
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process/create robotsTextFile for siteUrl = " + siteUrl, e);
            }
        }
        
        if(robotsTextFile != null) {
            String jsonStr = robotsTextFile.toJsonString();
            if(jsonpCallback != null && !jsonpCallback.isEmpty()) {
                jsonStr = jsonpCallback + "(" + jsonStr + ")";
                //resp.setContentType("application/javascript");  // ???
                resp.setContentType("application/javascript;charset=UTF-8");
            } else {
                //resp.setContentType("application/json");  // ????
                resp.setContentType("application/json;charset=UTF-8");  // ???                
            }
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doGet(): BOTTOM");
    }

    
    
    // Note:
    // Currently, only GET is "officially" supported...
    // ...
    
    
    // TBD:
    // depending on robotsTextFile.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of title, etc.
    // ...
    
    
    
}
