package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class OgObjectBaseJsBean implements Serializable  //, OgObjectBase
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgObjectBaseJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String url;
    private String type;
    private String siteName;
    private String title;
    private String description;
    private List<String> fbAdmins;
    private List<String> fbAppId;
    private List<OgImageStructJsBean> image;
    private List<OgAudioStructJsBean> audio;
    private List<OgVideoStructJsBean> video;
    private String locale;
    private List<String> localeAlternate;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public OgObjectBaseJsBean()
    {
        //this((String) null);
    }
    public OgObjectBaseJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgObjectBaseJsBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStructJsBean> image, List<OgAudioStructJsBean> audio, List<OgVideoStructJsBean> video, String locale, List<String> localeAlternate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, null, null);
    }
    public OgObjectBaseJsBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStructJsBean> image, List<OgAudioStructJsBean> audio, List<OgVideoStructJsBean> video, String locale, List<String> localeAlternate, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.url = url;
        this.type = type;
        this.siteName = siteName;
        this.title = title;
        this.description = description;
        this.fbAdmins = fbAdmins;
        this.fbAppId = fbAppId;
        setImage(getImage());
        setAudio(getAudio());
        setVideo(getVideo());
        this.locale = locale;
        this.localeAlternate = localeAlternate;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public OgObjectBaseJsBean(OgObjectBaseJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUrl(bean.getUrl());
            setType(bean.getType());
            setSiteName(bean.getSiteName());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setFbAdmins(bean.getFbAdmins());
            setFbAppId(bean.getFbAppId());
            setImage(bean.getImage());
            setAudio(bean.getAudio());
            setVideo(bean.getVideo());
            setLocale(bean.getLocale());
            setLocaleAlternate(bean.getLocaleAlternate());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static OgObjectBaseJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        OgObjectBaseJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(OgObjectBaseJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, OgObjectBaseJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUrl()
    {
        return this.url;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getSiteName()
    {
        return this.siteName;
    }
    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getTitle()
    {
        return this.title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return this.description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }

    public List<String> getFbAdmins()
    {
        return this.fbAdmins;
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        this.fbAdmins = fbAdmins;
    }

    public List<String> getFbAppId()
    {
        return this.fbAppId;
    }
    public void setFbAppId(List<String> fbAppId)
    {
        this.fbAppId = fbAppId;
    }

    public List<OgImageStructJsBean> getImage()
    {  
        return this.image;
    }
    public void setImage(List<OgImageStructJsBean> image)
    {
        this.image = image;
    }

    public List<OgAudioStructJsBean> getAudio()
    {  
        return this.audio;
    }
    public void setAudio(List<OgAudioStructJsBean> audio)
    {
        this.audio = audio;
    }

    public List<OgVideoStructJsBean> getVideo()
    {  
        return this.video;
    }
    public void setVideo(List<OgVideoStructJsBean> video)
    {
        this.video = video;
    }

    public String getLocale()
    {
        return this.locale;
    }
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    public List<String> getLocaleAlternate()
    {
        return this.localeAlternate;
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        this.localeAlternate = localeAlternate;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("url:null, ");
        sb.append("type:null, ");
        sb.append("siteName:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("fbAdmins:null, ");
        sb.append("fbAppId:null, ");
        sb.append("image:null, ");
        sb.append("audio:null, ");
        sb.append("video:null, ");
        sb.append("locale:null, ");
        sb.append("localeAlternate:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("url:");
        if(this.getUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrl()).append("\", ");
        }
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("siteName:");
        if(this.getSiteName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSiteName()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("fbAdmins:");
        if(this.getFbAdmins() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFbAdmins()).append("\", ");
        }
        sb.append("fbAppId:");
        if(this.getFbAppId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFbAppId()).append("\", ");
        }
        sb.append("image:");
        if(this.getImage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage()).append("\", ");
        }
        sb.append("audio:");
        if(this.getAudio() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAudio()).append("\", ");
        }
        sb.append("video:");
        if(this.getVideo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getVideo()).append("\", ");
        }
        sb.append("locale:");
        if(this.getLocale() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocale()).append("\", ");
        }
        sb.append("localeAlternate:");
        if(this.getLocaleAlternate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocaleAlternate()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUrl() != null) {
            sb.append("\"url\":").append("\"").append(this.getUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"url\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getSiteName() != null) {
            sb.append("\"siteName\":").append("\"").append(this.getSiteName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"siteName\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getFbAdmins() != null) {
            sb.append("\"fbAdmins\":").append("\"").append(this.getFbAdmins()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fbAdmins\":").append("null, ");
        }
        if(this.getFbAppId() != null) {
            sb.append("\"fbAppId\":").append("\"").append(this.getFbAppId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fbAppId\":").append("null, ");
        }
        if(this.getImage() != null) {
            sb.append("\"image\":").append("\"").append(this.getImage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"image\":").append("null, ");
        }
        if(this.getAudio() != null) {
            sb.append("\"audio\":").append("\"").append(this.getAudio()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"audio\":").append("null, ");
        }
        if(this.getVideo() != null) {
            sb.append("\"video\":").append("\"").append(this.getVideo()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"video\":").append("null, ");
        }
        if(this.getLocale() != null) {
            sb.append("\"locale\":").append("\"").append(this.getLocale()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"locale\":").append("null, ");
        }
        if(this.getLocaleAlternate() != null) {
            sb.append("\"localeAlternate\":").append("\"").append(this.getLocaleAlternate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"localeAlternate\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("url = " + this.url).append(";");
        sb.append("type = " + this.type).append(";");
        sb.append("siteName = " + this.siteName).append(";");
        sb.append("title = " + this.title).append(";");
        sb.append("description = " + this.description).append(";");
        sb.append("fbAdmins = " + this.fbAdmins).append(";");
        sb.append("fbAppId = " + this.fbAppId).append(";");
        sb.append("image = " + this.image).append(";");
        sb.append("audio = " + this.audio).append(";");
        sb.append("video = " + this.video).append(";");
        sb.append("locale = " + this.locale).append(";");
        sb.append("localeAlternate = " + this.localeAlternate).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
