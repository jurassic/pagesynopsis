package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextRefreshJsBean implements Serializable, Cloneable  //, RobotsTextRefresh
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextRefreshJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String robotsTextFile;
    private Integer refreshInterval;
    private String note;
    private String status;
    private Integer refreshStatus;
    private String result;
    private Long lastCheckedTime;
    private Long nextCheckedTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public RobotsTextRefreshJsBean()
    {
        //this((String) null);
    }
    public RobotsTextRefreshJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public RobotsTextRefreshJsBean(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime)
    {
        this(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime, null, null);
    }
    public RobotsTextRefreshJsBean(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.robotsTextFile = robotsTextFile;
        this.refreshInterval = refreshInterval;
        this.note = note;
        this.status = status;
        this.refreshStatus = refreshStatus;
        this.result = result;
        this.lastCheckedTime = lastCheckedTime;
        this.nextCheckedTime = nextCheckedTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public RobotsTextRefreshJsBean(RobotsTextRefreshJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setRobotsTextFile(bean.getRobotsTextFile());
            setRefreshInterval(bean.getRefreshInterval());
            setNote(bean.getNote());
            setStatus(bean.getStatus());
            setRefreshStatus(bean.getRefreshStatus());
            setResult(bean.getResult());
            setLastCheckedTime(bean.getLastCheckedTime());
            setNextCheckedTime(bean.getNextCheckedTime());
            setExpirationTime(bean.getExpirationTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RobotsTextRefreshJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RobotsTextRefreshJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RobotsTextRefreshJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RobotsTextRefreshJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getRobotsTextFile()
    {
        return this.robotsTextFile;
    }
    public void setRobotsTextFile(String robotsTextFile)
    {
        this.robotsTextFile = robotsTextFile;
    }

    public Integer getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getRefreshStatus()
    {
        return this.refreshStatus;
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        this.refreshStatus = refreshStatus;
    }

    public String getResult()
    {
        return this.result;
    }
    public void setResult(String result)
    {
        this.result = result;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getNextCheckedTime()
    {
        return this.nextCheckedTime;
    }
    public void setNextCheckedTime(Long nextCheckedTime)
    {
        this.nextCheckedTime = nextCheckedTime;
    }

    public Long getExpirationTime()
    {
        return this.expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("robotsTextFile:null, ");
        sb.append("refreshInterval:0, ");
        sb.append("note:null, ");
        sb.append("status:null, ");
        sb.append("refreshStatus:0, ");
        sb.append("result:null, ");
        sb.append("lastCheckedTime:0, ");
        sb.append("nextCheckedTime:0, ");
        sb.append("expirationTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("robotsTextFile:");
        if(this.getRobotsTextFile() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRobotsTextFile()).append("\", ");
        }
        sb.append("refreshInterval:" + this.getRefreshInterval()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("refreshStatus:" + this.getRefreshStatus()).append(", ");
        sb.append("result:");
        if(this.getResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getResult()).append("\", ");
        }
        sb.append("lastCheckedTime:" + this.getLastCheckedTime()).append(", ");
        sb.append("nextCheckedTime:" + this.getNextCheckedTime()).append(", ");
        sb.append("expirationTime:" + this.getExpirationTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getRobotsTextFile() != null) {
            sb.append("\"robotsTextFile\":").append("\"").append(this.getRobotsTextFile()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"robotsTextFile\":").append("null, ");
        }
        if(this.getRefreshInterval() != null) {
            sb.append("\"refreshInterval\":").append("").append(this.getRefreshInterval()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshInterval\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getRefreshStatus() != null) {
            sb.append("\"refreshStatus\":").append("").append(this.getRefreshStatus()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshStatus\":").append("null, ");
        }
        if(this.getResult() != null) {
            sb.append("\"result\":").append("\"").append(this.getResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"result\":").append("null, ");
        }
        if(this.getLastCheckedTime() != null) {
            sb.append("\"lastCheckedTime\":").append("").append(this.getLastCheckedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastCheckedTime\":").append("null, ");
        }
        if(this.getNextCheckedTime() != null) {
            sb.append("\"nextCheckedTime\":").append("").append(this.getNextCheckedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextCheckedTime\":").append("null, ");
        }
        if(this.getExpirationTime() != null) {
            sb.append("\"expirationTime\":").append("").append(this.getExpirationTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"expirationTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("robotsTextFile = " + this.robotsTextFile).append(";");
        sb.append("refreshInterval = " + this.refreshInterval).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("refreshStatus = " + this.refreshStatus).append(";");
        sb.append("result = " + this.result).append(";");
        sb.append("lastCheckedTime = " + this.lastCheckedTime).append(";");
        sb.append("nextCheckedTime = " + this.nextCheckedTime).append(";");
        sb.append("expirationTime = " + this.expirationTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RobotsTextRefreshJsBean cloned = new RobotsTextRefreshJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setRobotsTextFile(this.getRobotsTextFile());   
        cloned.setRefreshInterval(this.getRefreshInterval());   
        cloned.setNote(this.getNote());   
        cloned.setStatus(this.getStatus());   
        cloned.setRefreshStatus(this.getRefreshStatus());   
        cloned.setResult(this.getResult());   
        cloned.setLastCheckedTime(this.getLastCheckedTime());   
        cloned.setNextCheckedTime(this.getNextCheckedTime());   
        cloned.setExpirationTime(this.getExpirationTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
