package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class PageBaseJsBean implements Serializable  //, PageBase
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageBaseJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String user;
    private String fetchRequest;
    private String targetUrl;
    private String pageUrl;
    private String queryString;
    private List<KeyValuePairStructJsBean> queryParams;
    private String lastFetchResult;
    private Integer responseCode;
    private String contentType;
    private Integer contentLength;
    private String language;
    private String redirect;
    private String location;
    private String pageTitle;
    private String note;
    private Boolean deferred;
    private String status;
    private Integer refreshStatus;
    private Long refreshInterval;
    private Long nextRefreshTime;
    private Long lastCheckedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public PageBaseJsBean()
    {
        //this((String) null);
    }
    public PageBaseJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PageBaseJsBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStructJsBean> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public PageBaseJsBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStructJsBean> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.fetchRequest = fetchRequest;
        this.targetUrl = targetUrl;
        this.pageUrl = pageUrl;
        this.queryString = queryString;
        setQueryParams(getQueryParams());
        this.lastFetchResult = lastFetchResult;
        this.responseCode = responseCode;
        this.contentType = contentType;
        this.contentLength = contentLength;
        this.language = language;
        this.redirect = redirect;
        this.location = location;
        this.pageTitle = pageTitle;
        this.note = note;
        this.deferred = deferred;
        this.status = status;
        this.refreshStatus = refreshStatus;
        this.refreshInterval = refreshInterval;
        this.nextRefreshTime = nextRefreshTime;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public PageBaseJsBean(PageBaseJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setFetchRequest(bean.getFetchRequest());
            setTargetUrl(bean.getTargetUrl());
            setPageUrl(bean.getPageUrl());
            setQueryString(bean.getQueryString());
            setQueryParams(bean.getQueryParams());
            setLastFetchResult(bean.getLastFetchResult());
            setResponseCode(bean.getResponseCode());
            setContentType(bean.getContentType());
            setContentLength(bean.getContentLength());
            setLanguage(bean.getLanguage());
            setRedirect(bean.getRedirect());
            setLocation(bean.getLocation());
            setPageTitle(bean.getPageTitle());
            setNote(bean.getNote());
            setDeferred(bean.isDeferred());
            setStatus(bean.getStatus());
            setRefreshStatus(bean.getRefreshStatus());
            setRefreshInterval(bean.getRefreshInterval());
            setNextRefreshTime(bean.getNextRefreshTime());
            setLastCheckedTime(bean.getLastCheckedTime());
            setLastUpdatedTime(bean.getLastUpdatedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static PageBaseJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        PageBaseJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(PageBaseJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, PageBaseJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getFetchRequest()
    {
        return this.fetchRequest;
    }
    public void setFetchRequest(String fetchRequest)
    {
        this.fetchRequest = fetchRequest;
    }

    public String getTargetUrl()
    {
        return this.targetUrl;
    }
    public void setTargetUrl(String targetUrl)
    {
        this.targetUrl = targetUrl;
    }

    public String getPageUrl()
    {
        return this.pageUrl;
    }
    public void setPageUrl(String pageUrl)
    {
        this.pageUrl = pageUrl;
    }

    public String getQueryString()
    {
        return this.queryString;
    }
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public List<KeyValuePairStructJsBean> getQueryParams()
    {  
        return this.queryParams;
    }
    public void setQueryParams(List<KeyValuePairStructJsBean> queryParams)
    {
        this.queryParams = queryParams;
    }

    public String getLastFetchResult()
    {
        return this.lastFetchResult;
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        this.lastFetchResult = lastFetchResult;
    }

    public Integer getResponseCode()
    {
        return this.responseCode;
    }
    public void setResponseCode(Integer responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getContentType()
    {
        return this.contentType;
    }
    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public Integer getContentLength()
    {
        return this.contentLength;
    }
    public void setContentLength(Integer contentLength)
    {
        this.contentLength = contentLength;
    }

    public String getLanguage()
    {
        return this.language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getRedirect()
    {
        return this.redirect;
    }
    public void setRedirect(String redirect)
    {
        this.redirect = redirect;
    }

    public String getLocation()
    {
        return this.location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getPageTitle()
    {
        return this.pageTitle;
    }
    public void setPageTitle(String pageTitle)
    {
        this.pageTitle = pageTitle;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }

    public Boolean isDeferred()
    {
        return this.deferred;
    }
    public void setDeferred(Boolean deferred)
    {
        this.deferred = deferred;
    }

    public String getStatus()
    {
        return this.status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public Integer getRefreshStatus()
    {
        return this.refreshStatus;
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        this.refreshStatus = refreshStatus;
    }

    public Long getRefreshInterval()
    {
        return this.refreshInterval;
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        this.refreshInterval = refreshInterval;
    }

    public Long getNextRefreshTime()
    {
        return this.nextRefreshTime;
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        this.nextRefreshTime = nextRefreshTime;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("fetchRequest:null, ");
        sb.append("targetUrl:null, ");
        sb.append("pageUrl:null, ");
        sb.append("queryString:null, ");
        sb.append("queryParams:null, ");
        sb.append("lastFetchResult:null, ");
        sb.append("responseCode:0, ");
        sb.append("contentType:null, ");
        sb.append("contentLength:0, ");
        sb.append("language:null, ");
        sb.append("redirect:null, ");
        sb.append("location:null, ");
        sb.append("pageTitle:null, ");
        sb.append("note:null, ");
        sb.append("deferred:false, ");
        sb.append("status:null, ");
        sb.append("refreshStatus:0, ");
        sb.append("refreshInterval:0, ");
        sb.append("nextRefreshTime:0, ");
        sb.append("lastCheckedTime:0, ");
        sb.append("lastUpdatedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("fetchRequest:");
        if(this.getFetchRequest() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchRequest()).append("\", ");
        }
        sb.append("targetUrl:");
        if(this.getTargetUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetUrl()).append("\", ");
        }
        sb.append("pageUrl:");
        if(this.getPageUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPageUrl()).append("\", ");
        }
        sb.append("queryString:");
        if(this.getQueryString() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryString()).append("\", ");
        }
        sb.append("queryParams:");
        if(this.getQueryParams() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryParams()).append("\", ");
        }
        sb.append("lastFetchResult:");
        if(this.getLastFetchResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastFetchResult()).append("\", ");
        }
        sb.append("responseCode:" + this.getResponseCode()).append(", ");
        sb.append("contentType:");
        if(this.getContentType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContentType()).append("\", ");
        }
        sb.append("contentLength:" + this.getContentLength()).append(", ");
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("redirect:");
        if(this.getRedirect() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirect()).append("\", ");
        }
        sb.append("location:");
        if(this.getLocation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocation()).append("\", ");
        }
        sb.append("pageTitle:");
        if(this.getPageTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPageTitle()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("deferred:" + this.isDeferred()).append(", ");
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("refreshStatus:" + this.getRefreshStatus()).append(", ");
        sb.append("refreshInterval:" + this.getRefreshInterval()).append(", ");
        sb.append("nextRefreshTime:" + this.getNextRefreshTime()).append(", ");
        sb.append("lastCheckedTime:" + this.getLastCheckedTime()).append(", ");
        sb.append("lastUpdatedTime:" + this.getLastUpdatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getFetchRequest() != null) {
            sb.append("\"fetchRequest\":").append("\"").append(this.getFetchRequest()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchRequest\":").append("null, ");
        }
        if(this.getTargetUrl() != null) {
            sb.append("\"targetUrl\":").append("\"").append(this.getTargetUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetUrl\":").append("null, ");
        }
        if(this.getPageUrl() != null) {
            sb.append("\"pageUrl\":").append("\"").append(this.getPageUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pageUrl\":").append("null, ");
        }
        if(this.getQueryString() != null) {
            sb.append("\"queryString\":").append("\"").append(this.getQueryString()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryString\":").append("null, ");
        }
        if(this.getQueryParams() != null) {
            sb.append("\"queryParams\":").append("\"").append(this.getQueryParams()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryParams\":").append("null, ");
        }
        if(this.getLastFetchResult() != null) {
            sb.append("\"lastFetchResult\":").append("\"").append(this.getLastFetchResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastFetchResult\":").append("null, ");
        }
        if(this.getResponseCode() != null) {
            sb.append("\"responseCode\":").append("").append(this.getResponseCode()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"responseCode\":").append("null, ");
        }
        if(this.getContentType() != null) {
            sb.append("\"contentType\":").append("\"").append(this.getContentType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentType\":").append("null, ");
        }
        if(this.getContentLength() != null) {
            sb.append("\"contentLength\":").append("").append(this.getContentLength()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentLength\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getRedirect() != null) {
            sb.append("\"redirect\":").append("\"").append(this.getRedirect()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirect\":").append("null, ");
        }
        if(this.getLocation() != null) {
            sb.append("\"location\":").append("\"").append(this.getLocation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"location\":").append("null, ");
        }
        if(this.getPageTitle() != null) {
            sb.append("\"pageTitle\":").append("\"").append(this.getPageTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pageTitle\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.isDeferred() != null) {
            sb.append("\"deferred\":").append("").append(this.isDeferred()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"deferred\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getRefreshStatus() != null) {
            sb.append("\"refreshStatus\":").append("").append(this.getRefreshStatus()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshStatus\":").append("null, ");
        }
        if(this.getRefreshInterval() != null) {
            sb.append("\"refreshInterval\":").append("").append(this.getRefreshInterval()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshInterval\":").append("null, ");
        }
        if(this.getNextRefreshTime() != null) {
            sb.append("\"nextRefreshTime\":").append("").append(this.getNextRefreshTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextRefreshTime\":").append("null, ");
        }
        if(this.getLastCheckedTime() != null) {
            sb.append("\"lastCheckedTime\":").append("").append(this.getLastCheckedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastCheckedTime\":").append("null, ");
        }
        if(this.getLastUpdatedTime() != null) {
            sb.append("\"lastUpdatedTime\":").append("").append(this.getLastUpdatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastUpdatedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("user = " + this.user).append(";");
        sb.append("fetchRequest = " + this.fetchRequest).append(";");
        sb.append("targetUrl = " + this.targetUrl).append(";");
        sb.append("pageUrl = " + this.pageUrl).append(";");
        sb.append("queryString = " + this.queryString).append(";");
        sb.append("queryParams = " + this.queryParams).append(";");
        sb.append("lastFetchResult = " + this.lastFetchResult).append(";");
        sb.append("responseCode = " + this.responseCode).append(";");
        sb.append("contentType = " + this.contentType).append(";");
        sb.append("contentLength = " + this.contentLength).append(";");
        sb.append("language = " + this.language).append(";");
        sb.append("redirect = " + this.redirect).append(";");
        sb.append("location = " + this.location).append(";");
        sb.append("pageTitle = " + this.pageTitle).append(";");
        sb.append("note = " + this.note).append(";");
        sb.append("deferred = " + this.deferred).append(";");
        sb.append("status = " + this.status).append(";");
        sb.append("refreshStatus = " + this.refreshStatus).append(";");
        sb.append("refreshInterval = " + this.refreshInterval).append(";");
        sb.append("nextRefreshTime = " + this.nextRefreshTime).append(";");
        sb.append("lastCheckedTime = " + this.lastCheckedTime).append(";");
        sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }


}
