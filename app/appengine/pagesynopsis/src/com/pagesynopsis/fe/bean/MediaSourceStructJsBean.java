package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MediaSourceStructJsBean implements Serializable, Cloneable  //, MediaSourceStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MediaSourceStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String src;
    private String srcUrl;
    private String type;
    private String remark;

    // Ctors.
    public MediaSourceStructJsBean()
    {
        //this((String) null);
    }
    public MediaSourceStructJsBean(String uuid, String src, String srcUrl, String type, String remark)
    {
        this.uuid = uuid;
        this.src = src;
        this.srcUrl = srcUrl;
        this.type = type;
        this.remark = remark;
    }
    public MediaSourceStructJsBean(MediaSourceStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setSrc(bean.getSrc());
            setSrcUrl(bean.getSrcUrl());
            setType(bean.getType());
            setRemark(bean.getRemark());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static MediaSourceStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        MediaSourceStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(MediaSourceStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, MediaSourceStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getSrc()
    {
        return this.src;
    }
    public void setSrc(String src)
    {
        this.src = src;
    }

    public String getSrcUrl()
    {
        return this.srcUrl;
    }
    public void setSrcUrl(String srcUrl)
    {
        this.srcUrl = srcUrl;
    }

    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getRemark()
    {
        return this.remark;
    }
    public void setRemark(String remark)
    {
        this.remark = remark;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("src:null, ");
        sb.append("srcUrl:null, ");
        sb.append("type:null, ");
        sb.append("remark:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("src:");
        if(this.getSrc() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSrc()).append("\", ");
        }
        sb.append("srcUrl:");
        if(this.getSrcUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSrcUrl()).append("\", ");
        }
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("remark:");
        if(this.getRemark() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRemark()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getSrc() != null) {
            sb.append("\"src\":").append("\"").append(this.getSrc()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"src\":").append("null, ");
        }
        if(this.getSrcUrl() != null) {
            sb.append("\"srcUrl\":").append("\"").append(this.getSrcUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"srcUrl\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getRemark() != null) {
            sb.append("\"remark\":").append("\"").append(this.getRemark()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"remark\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("src = " + this.src).append(";");
        sb.append("srcUrl = " + this.srcUrl).append(";");
        sb.append("type = " + this.type).append(";");
        sb.append("remark = " + this.remark).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        MediaSourceStructJsBean cloned = new MediaSourceStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setSrc(this.getSrc());   
        cloned.setSrcUrl(this.getSrcUrl());   
        cloned.setType(this.getType());   
        cloned.setRemark(this.getRemark());   
        return cloned;
    }

}
