package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextFileJsBean implements Serializable, Cloneable  //, RobotsTextFile
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextFileJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String guid;
    private String siteUrl;
    private List<RobotsTextGroupJsBean> groups;
    private List<String> sitemaps;
    private String content;
    private String contentHash;
    private Long lastCheckedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public RobotsTextFileJsBean()
    {
        //this((String) null);
    }
    public RobotsTextFileJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null);
    }
    public RobotsTextFileJsBean(String guid, String siteUrl, List<RobotsTextGroupJsBean> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public RobotsTextFileJsBean(String guid, String siteUrl, List<RobotsTextGroupJsBean> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.siteUrl = siteUrl;
        setGroups(getGroups());
        this.sitemaps = sitemaps;
        this.content = content;
        this.contentHash = contentHash;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public RobotsTextFileJsBean(RobotsTextFileJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setSiteUrl(bean.getSiteUrl());
            setGroups(bean.getGroups());
            setSitemaps(bean.getSitemaps());
            setContent(bean.getContent());
            setContentHash(bean.getContentHash());
            setLastCheckedTime(bean.getLastCheckedTime());
            setLastUpdatedTime(bean.getLastUpdatedTime());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RobotsTextFileJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RobotsTextFileJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RobotsTextFileJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RobotsTextFileJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return this.guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getSiteUrl()
    {
        return this.siteUrl;
    }
    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    public List<RobotsTextGroupJsBean> getGroups()
    {  
        return this.groups;
    }
    public void setGroups(List<RobotsTextGroupJsBean> groups)
    {
        this.groups = groups;
    }

    public List<String> getSitemaps()
    {
        return this.sitemaps;
    }
    public void setSitemaps(List<String> sitemaps)
    {
        this.sitemaps = sitemaps;
    }

    public String getContent()
    {
        return this.content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContentHash()
    {
        return this.contentHash;
    }
    public void setContentHash(String contentHash)
    {
        this.contentHash = contentHash;
    }

    public Long getLastCheckedTime()
    {
        return this.lastCheckedTime;
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        this.lastCheckedTime = lastCheckedTime;
    }

    public Long getLastUpdatedTime()
    {
        return this.lastUpdatedTime;
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Long getCreatedTime()
    {
        return this.createdTime;
    }
    public void setCreatedTime(Long createdTime)
    {
        this.createdTime = createdTime;
    }

    public Long getModifiedTime()
    {
        return this.modifiedTime;
    }
    public void setModifiedTime(Long modifiedTime)
    {
        this.modifiedTime = modifiedTime;
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("siteUrl:null, ");
        sb.append("groups:null, ");
        sb.append("sitemaps:null, ");
        sb.append("content:null, ");
        sb.append("contentHash:null, ");
        sb.append("lastCheckedTime:0, ");
        sb.append("lastUpdatedTime:0, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("siteUrl:");
        if(this.getSiteUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSiteUrl()).append("\", ");
        }
        sb.append("groups:");
        if(this.getGroups() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGroups()).append("\", ");
        }
        sb.append("sitemaps:");
        if(this.getSitemaps() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSitemaps()).append("\", ");
        }
        sb.append("content:");
        if(this.getContent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContent()).append("\", ");
        }
        sb.append("contentHash:");
        if(this.getContentHash() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContentHash()).append("\", ");
        }
        sb.append("lastCheckedTime:" + this.getLastCheckedTime()).append(", ");
        sb.append("lastUpdatedTime:" + this.getLastUpdatedTime()).append(", ");
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getSiteUrl() != null) {
            sb.append("\"siteUrl\":").append("\"").append(this.getSiteUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"siteUrl\":").append("null, ");
        }
        if(this.getGroups() != null) {
            sb.append("\"groups\":").append("\"").append(this.getGroups()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"groups\":").append("null, ");
        }
        if(this.getSitemaps() != null) {
            sb.append("\"sitemaps\":").append("\"").append(this.getSitemaps()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"sitemaps\":").append("null, ");
        }
        if(this.getContent() != null) {
            sb.append("\"content\":").append("\"").append(this.getContent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"content\":").append("null, ");
        }
        if(this.getContentHash() != null) {
            sb.append("\"contentHash\":").append("\"").append(this.getContentHash()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentHash\":").append("null, ");
        }
        if(this.getLastCheckedTime() != null) {
            sb.append("\"lastCheckedTime\":").append("").append(this.getLastCheckedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastCheckedTime\":").append("null, ");
        }
        if(this.getLastUpdatedTime() != null) {
            sb.append("\"lastUpdatedTime\":").append("").append(this.getLastUpdatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastUpdatedTime\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("guid = " + this.guid).append(";");
        sb.append("siteUrl = " + this.siteUrl).append(";");
        sb.append("groups = " + this.groups).append(";");
        sb.append("sitemaps = " + this.sitemaps).append(";");
        sb.append("content = " + this.content).append(";");
        sb.append("contentHash = " + this.contentHash).append(";");
        sb.append("lastCheckedTime = " + this.lastCheckedTime).append(";");
        sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
        sb.append("createdTime = " + this.createdTime).append(";");
        sb.append("modifiedTime = " + this.modifiedTime).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RobotsTextFileJsBean cloned = new RobotsTextFileJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setSiteUrl(this.getSiteUrl());   
        cloned.setGroups(this.getGroups());   
        cloned.setSitemaps(this.getSitemaps());   
        cloned.setContent(this.getContent());   
        cloned.setContentHash(this.getContentHash());   
        cloned.setLastCheckedTime(this.getLastCheckedTime());   
        cloned.setLastUpdatedTime(this.getLastUpdatedTime());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
