package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotsTextGroupJsBean implements Serializable, Cloneable  //, RobotsTextGroup
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextGroupJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String userAgent;
    private List<String> allows;
    private List<String> disallows;

    // Ctors.
    public RobotsTextGroupJsBean()
    {
        //this((String) null);
    }
    public RobotsTextGroupJsBean(String uuid, String userAgent, List<String> allows, List<String> disallows)
    {
        this.uuid = uuid;
        this.userAgent = userAgent;
        this.allows = allows;
        this.disallows = disallows;
    }
    public RobotsTextGroupJsBean(RobotsTextGroupJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setUserAgent(bean.getUserAgent());
            setAllows(bean.getAllows());
            setDisallows(bean.getDisallows());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static RobotsTextGroupJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        RobotsTextGroupJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(RobotsTextGroupJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, RobotsTextGroupJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUserAgent()
    {
        return this.userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }

    public List<String> getAllows()
    {
        return this.allows;
    }
    public void setAllows(List<String> allows)
    {
        this.allows = allows;
    }

    public List<String> getDisallows()
    {
        return this.disallows;
    }
    public void setDisallows(List<String> disallows)
    {
        this.disallows = disallows;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAllows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisallows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("userAgent:null, ");
        sb.append("allows:null, ");
        sb.append("disallows:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("userAgent:");
        if(this.getUserAgent() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUserAgent()).append("\", ");
        }
        sb.append("allows:");
        if(this.getAllows() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAllows()).append("\", ");
        }
        sb.append("disallows:");
        if(this.getDisallows() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDisallows()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getUserAgent() != null) {
            sb.append("\"userAgent\":").append("\"").append(this.getUserAgent()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"userAgent\":").append("null, ");
        }
        if(this.getAllows() != null) {
            sb.append("\"allows\":").append("\"").append(this.getAllows()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"allows\":").append("null, ");
        }
        if(this.getDisallows() != null) {
            sb.append("\"disallows\":").append("\"").append(this.getDisallows()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"disallows\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("userAgent = " + this.userAgent).append(";");
        sb.append("allows = " + this.allows).append(";");
        sb.append("disallows = " + this.disallows).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        RobotsTextGroupJsBean cloned = new RobotsTextGroupJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setUserAgent(this.getUserAgent());   
        cloned.setAllows(this.getAllows());   
        cloned.setDisallows(this.getDisallows());   
        return cloned;
    }

}
