package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageStructJsBean implements Serializable, Cloneable  //, ImageStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String id;
    private String alt;
    private String src;
    private String srcUrl;
    private String mediaType;
    private String widthAttr;
    private Integer width;
    private String heightAttr;
    private Integer height;
    private String note;

    // Ctors.
    public ImageStructJsBean()
    {
        //this((String) null);
    }
    public ImageStructJsBean(String uuid, String id, String alt, String src, String srcUrl, String mediaType, String widthAttr, Integer width, String heightAttr, Integer height, String note)
    {
        this.uuid = uuid;
        this.id = id;
        this.alt = alt;
        this.src = src;
        this.srcUrl = srcUrl;
        this.mediaType = mediaType;
        this.widthAttr = widthAttr;
        this.width = width;
        this.heightAttr = heightAttr;
        this.height = height;
        this.note = note;
    }
    public ImageStructJsBean(ImageStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setId(bean.getId());
            setAlt(bean.getAlt());
            setSrc(bean.getSrc());
            setSrcUrl(bean.getSrcUrl());
            setMediaType(bean.getMediaType());
            setWidthAttr(bean.getWidthAttr());
            setWidth(bean.getWidth());
            setHeightAttr(bean.getHeightAttr());
            setHeight(bean.getHeight());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static ImageStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        ImageStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(ImageStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, ImageStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getId()
    {
        return this.id;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getAlt()
    {
        return this.alt;
    }
    public void setAlt(String alt)
    {
        this.alt = alt;
    }

    public String getSrc()
    {
        return this.src;
    }
    public void setSrc(String src)
    {
        this.src = src;
    }

    public String getSrcUrl()
    {
        return this.srcUrl;
    }
    public void setSrcUrl(String srcUrl)
    {
        this.srcUrl = srcUrl;
    }

    public String getMediaType()
    {
        return this.mediaType;
    }
    public void setMediaType(String mediaType)
    {
        this.mediaType = mediaType;
    }

    public String getWidthAttr()
    {
        return this.widthAttr;
    }
    public void setWidthAttr(String widthAttr)
    {
        this.widthAttr = widthAttr;
    }

    public Integer getWidth()
    {
        return this.width;
    }
    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public String getHeightAttr()
    {
        return this.heightAttr;
    }
    public void setHeightAttr(String heightAttr)
    {
        this.heightAttr = heightAttr;
    }

    public Integer getHeight()
    {
        return this.height;
    }
    public void setHeight(Integer height)
    {
        this.height = height;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMediaType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidthAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeightAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("id:null, ");
        sb.append("alt:null, ");
        sb.append("src:null, ");
        sb.append("srcUrl:null, ");
        sb.append("mediaType:null, ");
        sb.append("widthAttr:null, ");
        sb.append("width:0, ");
        sb.append("heightAttr:null, ");
        sb.append("height:0, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("id:");
        if(this.getId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getId()).append("\", ");
        }
        sb.append("alt:");
        if(this.getAlt() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAlt()).append("\", ");
        }
        sb.append("src:");
        if(this.getSrc() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSrc()).append("\", ");
        }
        sb.append("srcUrl:");
        if(this.getSrcUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSrcUrl()).append("\", ");
        }
        sb.append("mediaType:");
        if(this.getMediaType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getMediaType()).append("\", ");
        }
        sb.append("widthAttr:");
        if(this.getWidthAttr() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getWidthAttr()).append("\", ");
        }
        sb.append("width:" + this.getWidth()).append(", ");
        sb.append("heightAttr:");
        if(this.getHeightAttr() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHeightAttr()).append("\", ");
        }
        sb.append("height:" + this.getHeight()).append(", ");
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getId() != null) {
            sb.append("\"id\":").append("\"").append(this.getId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"id\":").append("null, ");
        }
        if(this.getAlt() != null) {
            sb.append("\"alt\":").append("\"").append(this.getAlt()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"alt\":").append("null, ");
        }
        if(this.getSrc() != null) {
            sb.append("\"src\":").append("\"").append(this.getSrc()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"src\":").append("null, ");
        }
        if(this.getSrcUrl() != null) {
            sb.append("\"srcUrl\":").append("\"").append(this.getSrcUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"srcUrl\":").append("null, ");
        }
        if(this.getMediaType() != null) {
            sb.append("\"mediaType\":").append("\"").append(this.getMediaType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"mediaType\":").append("null, ");
        }
        if(this.getWidthAttr() != null) {
            sb.append("\"widthAttr\":").append("\"").append(this.getWidthAttr()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"widthAttr\":").append("null, ");
        }
        if(this.getWidth() != null) {
            sb.append("\"width\":").append("").append(this.getWidth()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"width\":").append("null, ");
        }
        if(this.getHeightAttr() != null) {
            sb.append("\"heightAttr\":").append("\"").append(this.getHeightAttr()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"heightAttr\":").append("null, ");
        }
        if(this.getHeight() != null) {
            sb.append("\"height\":").append("").append(this.getHeight()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"height\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("id = " + this.id).append(";");
        sb.append("alt = " + this.alt).append(";");
        sb.append("src = " + this.src).append(";");
        sb.append("srcUrl = " + this.srcUrl).append(";");
        sb.append("mediaType = " + this.mediaType).append(";");
        sb.append("widthAttr = " + this.widthAttr).append(";");
        sb.append("width = " + this.width).append(";");
        sb.append("heightAttr = " + this.heightAttr).append(";");
        sb.append("height = " + this.height).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        ImageStructJsBean cloned = new ImageStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setId(this.getId());   
        cloned.setAlt(this.getAlt());   
        cloned.setSrc(this.getSrc());   
        cloned.setSrcUrl(this.getSrcUrl());   
        cloned.setMediaType(this.getMediaType());   
        cloned.setWidthAttr(this.getWidthAttr());   
        cloned.setWidth(this.getWidth());   
        cloned.setHeightAttr(this.getHeightAttr());   
        cloned.setHeight(this.getHeight());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
