package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitterCardMetaJsBean extends PageBaseJsBean implements Serializable, Cloneable  //, TwitterCardMeta
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardMetaJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String cardType;
    private TwitterSummaryCardJsBean summaryCard;
    private TwitterPhotoCardJsBean photoCard;
    private TwitterGalleryCardJsBean galleryCard;
    private TwitterAppCardJsBean appCard;
    private TwitterPlayerCardJsBean playerCard;
    private TwitterProductCardJsBean productCard;

    // Ctors.
    public TwitterCardMetaJsBean()
    {
        //this((String) null);
    }
    public TwitterCardMetaJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterCardMetaJsBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStructJsBean> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCardJsBean summaryCard, TwitterPhotoCardJsBean photoCard, TwitterGalleryCardJsBean galleryCard, TwitterAppCardJsBean appCard, TwitterPlayerCardJsBean playerCard, TwitterProductCardJsBean productCard)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCard, photoCard, galleryCard, appCard, playerCard, productCard, null, null);
    }
    public TwitterCardMetaJsBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStructJsBean> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCardJsBean summaryCard, TwitterPhotoCardJsBean photoCard, TwitterGalleryCardJsBean galleryCard, TwitterAppCardJsBean appCard, TwitterPlayerCardJsBean playerCard, TwitterProductCardJsBean productCard, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);

        this.cardType = cardType;
        this.summaryCard = summaryCard;
        this.photoCard = photoCard;
        this.galleryCard = galleryCard;
        this.appCard = appCard;
        this.playerCard = playerCard;
        this.productCard = productCard;
    }
    public TwitterCardMetaJsBean(TwitterCardMetaJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUser(bean.getUser());
            setFetchRequest(bean.getFetchRequest());
            setTargetUrl(bean.getTargetUrl());
            setPageUrl(bean.getPageUrl());
            setQueryString(bean.getQueryString());
            setQueryParams(bean.getQueryParams());
            setLastFetchResult(bean.getLastFetchResult());
            setResponseCode(bean.getResponseCode());
            setContentType(bean.getContentType());
            setContentLength(bean.getContentLength());
            setLanguage(bean.getLanguage());
            setRedirect(bean.getRedirect());
            setLocation(bean.getLocation());
            setPageTitle(bean.getPageTitle());
            setNote(bean.getNote());
            setDeferred(bean.isDeferred());
            setStatus(bean.getStatus());
            setRefreshStatus(bean.getRefreshStatus());
            setRefreshInterval(bean.getRefreshInterval());
            setNextRefreshTime(bean.getNextRefreshTime());
            setLastCheckedTime(bean.getLastCheckedTime());
            setLastUpdatedTime(bean.getLastUpdatedTime());
            setCardType(bean.getCardType());
            setSummaryCard(bean.getSummaryCard());
            setPhotoCard(bean.getPhotoCard());
            setGalleryCard(bean.getGalleryCard());
            setAppCard(bean.getAppCard());
            setPlayerCard(bean.getPlayerCard());
            setProductCard(bean.getProductCard());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static TwitterCardMetaJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        TwitterCardMetaJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(TwitterCardMetaJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, TwitterCardMetaJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    public List<KeyValuePairStructJsBean> getQueryParams()
    {  
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStructJsBean> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    public String getCardType()
    {
        return this.cardType;
    }
    public void setCardType(String cardType)
    {
        this.cardType = cardType;
    }

    public TwitterSummaryCardJsBean getSummaryCard()
    {  
        return this.summaryCard;
    }
    public void setSummaryCard(TwitterSummaryCardJsBean summaryCard)
    {
        this.summaryCard = summaryCard;
    }

    public TwitterPhotoCardJsBean getPhotoCard()
    {  
        return this.photoCard;
    }
    public void setPhotoCard(TwitterPhotoCardJsBean photoCard)
    {
        this.photoCard = photoCard;
    }

    public TwitterGalleryCardJsBean getGalleryCard()
    {  
        return this.galleryCard;
    }
    public void setGalleryCard(TwitterGalleryCardJsBean galleryCard)
    {
        this.galleryCard = galleryCard;
    }

    public TwitterAppCardJsBean getAppCard()
    {  
        return this.appCard;
    }
    public void setAppCard(TwitterAppCardJsBean appCard)
    {
        this.appCard = appCard;
    }

    public TwitterPlayerCardJsBean getPlayerCard()
    {  
        return this.playerCard;
    }
    public void setPlayerCard(TwitterPlayerCardJsBean playerCard)
    {
        this.playerCard = playerCard;
    }

    public TwitterProductCardJsBean getProductCard()
    {  
        return this.productCard;
    }
    public void setProductCard(TwitterProductCardJsBean productCard)
    {
        this.productCard = productCard;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("user:null, ");
        sb.append("fetchRequest:null, ");
        sb.append("targetUrl:null, ");
        sb.append("pageUrl:null, ");
        sb.append("queryString:null, ");
        sb.append("queryParams:null, ");
        sb.append("lastFetchResult:null, ");
        sb.append("responseCode:0, ");
        sb.append("contentType:null, ");
        sb.append("contentLength:0, ");
        sb.append("language:null, ");
        sb.append("redirect:null, ");
        sb.append("location:null, ");
        sb.append("pageTitle:null, ");
        sb.append("note:null, ");
        sb.append("deferred:false, ");
        sb.append("status:null, ");
        sb.append("refreshStatus:0, ");
        sb.append("refreshInterval:0, ");
        sb.append("nextRefreshTime:0, ");
        sb.append("lastCheckedTime:0, ");
        sb.append("lastUpdatedTime:0, ");
        sb.append("cardType:null, ");
        sb.append("summaryCard:{}, ");
        sb.append("photoCard:{}, ");
        sb.append("galleryCard:{}, ");
        sb.append("appCard:{}, ");
        sb.append("playerCard:{}, ");
        sb.append("productCard:{}, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("user:");
        if(this.getUser() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUser()).append("\", ");
        }
        sb.append("fetchRequest:");
        if(this.getFetchRequest() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFetchRequest()).append("\", ");
        }
        sb.append("targetUrl:");
        if(this.getTargetUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTargetUrl()).append("\", ");
        }
        sb.append("pageUrl:");
        if(this.getPageUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPageUrl()).append("\", ");
        }
        sb.append("queryString:");
        if(this.getQueryString() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryString()).append("\", ");
        }
        sb.append("queryParams:");
        if(this.getQueryParams() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getQueryParams()).append("\", ");
        }
        sb.append("lastFetchResult:");
        if(this.getLastFetchResult() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLastFetchResult()).append("\", ");
        }
        sb.append("responseCode:" + this.getResponseCode()).append(", ");
        sb.append("contentType:");
        if(this.getContentType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getContentType()).append("\", ");
        }
        sb.append("contentLength:" + this.getContentLength()).append(", ");
        sb.append("language:");
        if(this.getLanguage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLanguage()).append("\", ");
        }
        sb.append("redirect:");
        if(this.getRedirect() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getRedirect()).append("\", ");
        }
        sb.append("location:");
        if(this.getLocation() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocation()).append("\", ");
        }
        sb.append("pageTitle:");
        if(this.getPageTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPageTitle()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        sb.append("deferred:" + this.isDeferred()).append(", ");
        sb.append("status:");
        if(this.getStatus() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getStatus()).append("\", ");
        }
        sb.append("refreshStatus:" + this.getRefreshStatus()).append(", ");
        sb.append("refreshInterval:" + this.getRefreshInterval()).append(", ");
        sb.append("nextRefreshTime:" + this.getNextRefreshTime()).append(", ");
        sb.append("lastCheckedTime:" + this.getLastCheckedTime()).append(", ");
        sb.append("lastUpdatedTime:" + this.getLastUpdatedTime()).append(", ");
        sb.append("cardType:");
        if(this.getCardType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getCardType()).append("\", ");
        }
        sb.append("summaryCard:");
        if(this.getSummaryCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSummaryCard()).append("\", ");
        }
        sb.append("photoCard:");
        if(this.getPhotoCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPhotoCard()).append("\", ");
        }
        sb.append("galleryCard:");
        if(this.getGalleryCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGalleryCard()).append("\", ");
        }
        sb.append("appCard:");
        if(this.getAppCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAppCard()).append("\", ");
        }
        sb.append("playerCard:");
        if(this.getPlayerCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPlayerCard()).append("\", ");
        }
        sb.append("productCard:");
        if(this.getProductCard() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getProductCard()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUser() != null) {
            sb.append("\"user\":").append("\"").append(this.getUser()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"user\":").append("null, ");
        }
        if(this.getFetchRequest() != null) {
            sb.append("\"fetchRequest\":").append("\"").append(this.getFetchRequest()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fetchRequest\":").append("null, ");
        }
        if(this.getTargetUrl() != null) {
            sb.append("\"targetUrl\":").append("\"").append(this.getTargetUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"targetUrl\":").append("null, ");
        }
        if(this.getPageUrl() != null) {
            sb.append("\"pageUrl\":").append("\"").append(this.getPageUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pageUrl\":").append("null, ");
        }
        if(this.getQueryString() != null) {
            sb.append("\"queryString\":").append("\"").append(this.getQueryString()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryString\":").append("null, ");
        }
        if(this.getQueryParams() != null) {
            sb.append("\"queryParams\":").append("\"").append(this.getQueryParams()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"queryParams\":").append("null, ");
        }
        if(this.getLastFetchResult() != null) {
            sb.append("\"lastFetchResult\":").append("\"").append(this.getLastFetchResult()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastFetchResult\":").append("null, ");
        }
        if(this.getResponseCode() != null) {
            sb.append("\"responseCode\":").append("").append(this.getResponseCode()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"responseCode\":").append("null, ");
        }
        if(this.getContentType() != null) {
            sb.append("\"contentType\":").append("\"").append(this.getContentType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentType\":").append("null, ");
        }
        if(this.getContentLength() != null) {
            sb.append("\"contentLength\":").append("").append(this.getContentLength()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"contentLength\":").append("null, ");
        }
        if(this.getLanguage() != null) {
            sb.append("\"language\":").append("\"").append(this.getLanguage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"language\":").append("null, ");
        }
        if(this.getRedirect() != null) {
            sb.append("\"redirect\":").append("\"").append(this.getRedirect()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"redirect\":").append("null, ");
        }
        if(this.getLocation() != null) {
            sb.append("\"location\":").append("\"").append(this.getLocation()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"location\":").append("null, ");
        }
        if(this.getPageTitle() != null) {
            sb.append("\"pageTitle\":").append("\"").append(this.getPageTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"pageTitle\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        if(this.isDeferred() != null) {
            sb.append("\"deferred\":").append("").append(this.isDeferred()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"deferred\":").append("null, ");
        }
        if(this.getStatus() != null) {
            sb.append("\"status\":").append("\"").append(this.getStatus()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"status\":").append("null, ");
        }
        if(this.getRefreshStatus() != null) {
            sb.append("\"refreshStatus\":").append("").append(this.getRefreshStatus()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshStatus\":").append("null, ");
        }
        if(this.getRefreshInterval() != null) {
            sb.append("\"refreshInterval\":").append("").append(this.getRefreshInterval()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"refreshInterval\":").append("null, ");
        }
        if(this.getNextRefreshTime() != null) {
            sb.append("\"nextRefreshTime\":").append("").append(this.getNextRefreshTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"nextRefreshTime\":").append("null, ");
        }
        if(this.getLastCheckedTime() != null) {
            sb.append("\"lastCheckedTime\":").append("").append(this.getLastCheckedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastCheckedTime\":").append("null, ");
        }
        if(this.getLastUpdatedTime() != null) {
            sb.append("\"lastUpdatedTime\":").append("").append(this.getLastUpdatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"lastUpdatedTime\":").append("null, ");
        }
        if(this.getCardType() != null) {
            sb.append("\"cardType\":").append("\"").append(this.getCardType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"cardType\":").append("null, ");
        }
        sb.append("\"summaryCard\":").append(this.summaryCard.toJsonString()).append(", ");
        sb.append("\"photoCard\":").append(this.photoCard.toJsonString()).append(", ");
        sb.append("\"galleryCard\":").append(this.galleryCard.toJsonString()).append(", ");
        sb.append("\"appCard\":").append(this.appCard.toJsonString()).append(", ");
        sb.append("\"playerCard\":").append(this.playerCard.toJsonString()).append(", ");
        sb.append("\"productCard\":").append(this.productCard.toJsonString()).append(", ");
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("cardType = " + this.cardType).append(";");
        sb.append("summaryCard = " + this.summaryCard).append(";");
        sb.append("photoCard = " + this.photoCard).append(";");
        sb.append("galleryCard = " + this.galleryCard).append(";");
        sb.append("appCard = " + this.appCard).append(";");
        sb.append("playerCard = " + this.playerCard).append(";");
        sb.append("productCard = " + this.productCard).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        TwitterCardMetaJsBean cloned = new TwitterCardMetaJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUser(this.getUser());   
        cloned.setFetchRequest(this.getFetchRequest());   
        cloned.setTargetUrl(this.getTargetUrl());   
        cloned.setPageUrl(this.getPageUrl());   
        cloned.setQueryString(this.getQueryString());   
        cloned.setQueryParams(this.getQueryParams());   
        cloned.setLastFetchResult(this.getLastFetchResult());   
        cloned.setResponseCode(this.getResponseCode());   
        cloned.setContentType(this.getContentType());   
        cloned.setContentLength(this.getContentLength());   
        cloned.setLanguage(this.getLanguage());   
        cloned.setRedirect(this.getRedirect());   
        cloned.setLocation(this.getLocation());   
        cloned.setPageTitle(this.getPageTitle());   
        cloned.setNote(this.getNote());   
        cloned.setDeferred(this.isDeferred());   
        cloned.setStatus(this.getStatus());   
        cloned.setRefreshStatus(this.getRefreshStatus());   
        cloned.setRefreshInterval(this.getRefreshInterval());   
        cloned.setNextRefreshTime(this.getNextRefreshTime());   
        cloned.setLastCheckedTime(this.getLastCheckedTime());   
        cloned.setLastUpdatedTime(this.getLastUpdatedTime());   
        cloned.setCardType(this.getCardType());   
        cloned.setSummaryCard( (TwitterSummaryCardJsBean) this.getSummaryCard().clone() );
        cloned.setPhotoCard( (TwitterPhotoCardJsBean) this.getPhotoCard().clone() );
        cloned.setGalleryCard( (TwitterGalleryCardJsBean) this.getGalleryCard().clone() );
        cloned.setAppCard( (TwitterAppCardJsBean) this.getAppCard().clone() );
        cloned.setPlayerCard( (TwitterPlayerCardJsBean) this.getPlayerCard().clone() );
        cloned.setProductCard( (TwitterProductCardJsBean) this.getProductCard().clone() );
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
