package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OgVideoJsBean extends OgObjectBaseJsBean implements Serializable, Cloneable  //, OgVideo
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgVideoJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private List<String> director;
    private List<String> writer;
    private List<OgActorStructJsBean> actor;
    private Integer duration;
    private List<String> tag;
    private String releaseDate;

    // Ctors.
    public OgVideoJsBean()
    {
        //this((String) null);
    }
    public OgVideoJsBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgVideoJsBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStructJsBean> image, List<OgAudioStructJsBean> audio, List<OgVideoStructJsBean> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStructJsBean> actor, Integer duration, List<String> tag, String releaseDate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate, null, null);
    }
    public OgVideoJsBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStructJsBean> image, List<OgAudioStructJsBean> audio, List<OgVideoStructJsBean> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStructJsBean> actor, Integer duration, List<String> tag, String releaseDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);

        this.director = director;
        this.writer = writer;
        setActor(getActor());
        this.duration = duration;
        this.tag = tag;
        this.releaseDate = releaseDate;
    }
    public OgVideoJsBean(OgVideoJsBean bean)
    {
        if(bean != null) {
            setGuid(bean.getGuid());
            setUrl(bean.getUrl());
            setType(bean.getType());
            setSiteName(bean.getSiteName());
            setTitle(bean.getTitle());
            setDescription(bean.getDescription());
            setFbAdmins(bean.getFbAdmins());
            setFbAppId(bean.getFbAppId());
            setImage(bean.getImage());
            setAudio(bean.getAudio());
            setVideo(bean.getVideo());
            setLocale(bean.getLocale());
            setLocaleAlternate(bean.getLocaleAlternate());
            setDirector(bean.getDirector());
            setWriter(bean.getWriter());
            setActor(bean.getActor());
            setDuration(bean.getDuration());
            setTag(bean.getTag());
            setReleaseDate(bean.getReleaseDate());
            setCreatedTime(bean.getCreatedTime());
            setModifiedTime(bean.getModifiedTime());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static OgVideoJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        OgVideoJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(OgVideoJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, OgVideoJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    public String getSiteName()
    {
        return super.getSiteName();
    }
    public void setSiteName(String siteName)
    {
        super.setSiteName(siteName);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public List<String> getFbAdmins()
    {
        return super.getFbAdmins();
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        super.setFbAdmins(fbAdmins);
    }

    public List<String> getFbAppId()
    {
        return super.getFbAppId();
    }
    public void setFbAppId(List<String> fbAppId)
    {
        super.setFbAppId(fbAppId);
    }

    public List<OgImageStructJsBean> getImage()
    {  
        return super.getImage();
    }
    public void setImage(List<OgImageStructJsBean> image)
    {
        super.setImage(image);
    }

    public List<OgAudioStructJsBean> getAudio()
    {  
        return super.getAudio();
    }
    public void setAudio(List<OgAudioStructJsBean> audio)
    {
        super.setAudio(audio);
    }

    public List<OgVideoStructJsBean> getVideo()
    {  
        return super.getVideo();
    }
    public void setVideo(List<OgVideoStructJsBean> video)
    {
        super.setVideo(video);
    }

    public String getLocale()
    {
        return super.getLocale();
    }
    public void setLocale(String locale)
    {
        super.setLocale(locale);
    }

    public List<String> getLocaleAlternate()
    {
        return super.getLocaleAlternate();
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        super.setLocaleAlternate(localeAlternate);
    }

    public List<String> getDirector()
    {
        return this.director;
    }
    public void setDirector(List<String> director)
    {
        this.director = director;
    }

    public List<String> getWriter()
    {
        return this.writer;
    }
    public void setWriter(List<String> writer)
    {
        this.writer = writer;
    }

    public List<OgActorStructJsBean> getActor()
    {  
        return this.actor;
    }
    public void setActor(List<OgActorStructJsBean> actor)
    {
        this.actor = actor;
    }

    public Integer getDuration()
    {
        return this.duration;
    }
    public void setDuration(Integer duration)
    {
        this.duration = duration;
    }

    public List<String> getTag()
    {
        return this.tag;
    }
    public void setTag(List<String> tag)
    {
        this.tag = tag;
    }

    public String getReleaseDate()
    {
        return this.releaseDate;
    }
    public void setReleaseDate(String releaseDate)
    {
        this.releaseDate = releaseDate;
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:null, ");
        sb.append("url:null, ");
        sb.append("type:null, ");
        sb.append("siteName:null, ");
        sb.append("title:null, ");
        sb.append("description:null, ");
        sb.append("fbAdmins:null, ");
        sb.append("fbAppId:null, ");
        sb.append("image:null, ");
        sb.append("audio:null, ");
        sb.append("video:null, ");
        sb.append("locale:null, ");
        sb.append("localeAlternate:null, ");
        sb.append("director:null, ");
        sb.append("writer:null, ");
        sb.append("actor:null, ");
        sb.append("duration:0, ");
        sb.append("tag:null, ");
        sb.append("releaseDate:null, ");
        sb.append("createdTime:0, ");
        sb.append("modifiedTime:0, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("guid:");
        if(this.getGuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGuid()).append("\", ");
        }
        sb.append("url:");
        if(this.getUrl() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUrl()).append("\", ");
        }
        sb.append("type:");
        if(this.getType() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getType()).append("\", ");
        }
        sb.append("siteName:");
        if(this.getSiteName() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getSiteName()).append("\", ");
        }
        sb.append("title:");
        if(this.getTitle() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTitle()).append("\", ");
        }
        sb.append("description:");
        if(this.getDescription() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDescription()).append("\", ");
        }
        sb.append("fbAdmins:");
        if(this.getFbAdmins() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFbAdmins()).append("\", ");
        }
        sb.append("fbAppId:");
        if(this.getFbAppId() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFbAppId()).append("\", ");
        }
        sb.append("image:");
        if(this.getImage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getImage()).append("\", ");
        }
        sb.append("audio:");
        if(this.getAudio() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getAudio()).append("\", ");
        }
        sb.append("video:");
        if(this.getVideo() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getVideo()).append("\", ");
        }
        sb.append("locale:");
        if(this.getLocale() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocale()).append("\", ");
        }
        sb.append("localeAlternate:");
        if(this.getLocaleAlternate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getLocaleAlternate()).append("\", ");
        }
        sb.append("director:");
        if(this.getDirector() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getDirector()).append("\", ");
        }
        sb.append("writer:");
        if(this.getWriter() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getWriter()).append("\", ");
        }
        sb.append("actor:");
        if(this.getActor() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getActor()).append("\", ");
        }
        sb.append("duration:" + this.getDuration()).append(", ");
        sb.append("tag:");
        if(this.getTag() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTag()).append("\", ");
        }
        sb.append("releaseDate:");
        if(this.getReleaseDate() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getReleaseDate()).append("\", ");
        }
        sb.append("createdTime:" + this.getCreatedTime()).append(", ");
        sb.append("modifiedTime:" + this.getModifiedTime()).append(", ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getGuid() != null) {
            sb.append("\"guid\":").append("\"").append(this.getGuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"guid\":").append("null, ");
        }
        if(this.getUrl() != null) {
            sb.append("\"url\":").append("\"").append(this.getUrl()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"url\":").append("null, ");
        }
        if(this.getType() != null) {
            sb.append("\"type\":").append("\"").append(this.getType()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"type\":").append("null, ");
        }
        if(this.getSiteName() != null) {
            sb.append("\"siteName\":").append("\"").append(this.getSiteName()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"siteName\":").append("null, ");
        }
        if(this.getTitle() != null) {
            sb.append("\"title\":").append("\"").append(this.getTitle()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"title\":").append("null, ");
        }
        if(this.getDescription() != null) {
            sb.append("\"description\":").append("\"").append(this.getDescription()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"description\":").append("null, ");
        }
        if(this.getFbAdmins() != null) {
            sb.append("\"fbAdmins\":").append("\"").append(this.getFbAdmins()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fbAdmins\":").append("null, ");
        }
        if(this.getFbAppId() != null) {
            sb.append("\"fbAppId\":").append("\"").append(this.getFbAppId()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"fbAppId\":").append("null, ");
        }
        if(this.getImage() != null) {
            sb.append("\"image\":").append("\"").append(this.getImage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"image\":").append("null, ");
        }
        if(this.getAudio() != null) {
            sb.append("\"audio\":").append("\"").append(this.getAudio()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"audio\":").append("null, ");
        }
        if(this.getVideo() != null) {
            sb.append("\"video\":").append("\"").append(this.getVideo()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"video\":").append("null, ");
        }
        if(this.getLocale() != null) {
            sb.append("\"locale\":").append("\"").append(this.getLocale()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"locale\":").append("null, ");
        }
        if(this.getLocaleAlternate() != null) {
            sb.append("\"localeAlternate\":").append("\"").append(this.getLocaleAlternate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"localeAlternate\":").append("null, ");
        }
        if(this.getDirector() != null) {
            sb.append("\"director\":").append("\"").append(this.getDirector()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"director\":").append("null, ");
        }
        if(this.getWriter() != null) {
            sb.append("\"writer\":").append("\"").append(this.getWriter()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"writer\":").append("null, ");
        }
        if(this.getActor() != null) {
            sb.append("\"actor\":").append("\"").append(this.getActor()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"actor\":").append("null, ");
        }
        if(this.getDuration() != null) {
            sb.append("\"duration\":").append("").append(this.getDuration()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"duration\":").append("null, ");
        }
        if(this.getTag() != null) {
            sb.append("\"tag\":").append("\"").append(this.getTag()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"tag\":").append("null, ");
        }
        if(this.getReleaseDate() != null) {
            sb.append("\"releaseDate\":").append("\"").append(this.getReleaseDate()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"releaseDate\":").append("null, ");
        }
        if(this.getCreatedTime() != null) {
            sb.append("\"createdTime\":").append("").append(this.getCreatedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"createdTime\":").append("null, ");
        }
        if(this.getModifiedTime() != null) {
            sb.append("\"modifiedTime\":").append("").append(this.getModifiedTime()).append(", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"modifiedTime\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("director = " + this.director).append(";");
        sb.append("writer = " + this.writer).append(";");
        sb.append("actor = " + this.actor).append(";");
        sb.append("duration = " + this.duration).append(";");
        sb.append("tag = " + this.tag).append(";");
        sb.append("releaseDate = " + this.releaseDate).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        OgVideoJsBean cloned = new OgVideoJsBean();
        cloned.setGuid(null);          // Do not clone the guid.
        cloned.setUrl(this.getUrl());   
        cloned.setType(this.getType());   
        cloned.setSiteName(this.getSiteName());   
        cloned.setTitle(this.getTitle());   
        cloned.setDescription(this.getDescription());   
        cloned.setFbAdmins(this.getFbAdmins());   
        cloned.setFbAppId(this.getFbAppId());   
        cloned.setImage(this.getImage());   
        cloned.setAudio(this.getAudio());   
        cloned.setVideo(this.getVideo());   
        cloned.setLocale(this.getLocale());   
        cloned.setLocaleAlternate(this.getLocaleAlternate());   
        cloned.setDirector(this.getDirector());   
        cloned.setWriter(this.getWriter());   
        cloned.setActor(this.getActor());   
        cloned.setDuration(this.getDuration());   
        cloned.setTag(this.getTag());   
        cloned.setReleaseDate(this.getReleaseDate());   
        cloned.setCreatedTime(null);   // Reset the timestamp.
        cloned.setModifiedTime(null);   // Reset the timestamp.
        return cloned;
    }

}
