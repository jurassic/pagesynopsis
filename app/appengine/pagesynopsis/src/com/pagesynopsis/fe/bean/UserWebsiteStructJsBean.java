package com.pagesynopsis.fe.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.pagesynopsis.fe.core.StringEscapeUtil;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWebsiteStructJsBean implements Serializable, Cloneable  //, UserWebsiteStruct
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserWebsiteStructJsBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            // sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
            sObjectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        }
        return sObjectMapper;
    }

    private String uuid;
    private String primarySite;
    private String homePage;
    private String blogSite;
    private String portfolioSite;
    private String twitterProfile;
    private String facebookProfile;
    private String googlePlusProfile;
    private String note;

    // Ctors.
    public UserWebsiteStructJsBean()
    {
        //this((String) null);
    }
    public UserWebsiteStructJsBean(String uuid, String primarySite, String homePage, String blogSite, String portfolioSite, String twitterProfile, String facebookProfile, String googlePlusProfile, String note)
    {
        this.uuid = uuid;
        this.primarySite = primarySite;
        this.homePage = homePage;
        this.blogSite = blogSite;
        this.portfolioSite = portfolioSite;
        this.twitterProfile = twitterProfile;
        this.facebookProfile = facebookProfile;
        this.googlePlusProfile = googlePlusProfile;
        this.note = note;
    }
    public UserWebsiteStructJsBean(UserWebsiteStructJsBean bean)
    {
        if(bean != null) {
            setUuid(bean.getUuid());
            setPrimarySite(bean.getPrimarySite());
            setHomePage(bean.getHomePage());
            setBlogSite(bean.getBlogSite());
            setPortfolioSite(bean.getPortfolioSite());
            setTwitterProfile(bean.getTwitterProfile());
            setFacebookProfile(bean.getFacebookProfile());
            setGooglePlusProfile(bean.getGooglePlusProfile());
            setNote(bean.getNote());
        } else {
            log.log(Level.WARNING, "The arg bean object is null.");
        }
    }

    public static UserWebsiteStructJsBean fromJsonString(String jsonStr)
    {
        // TBD: readValueAs() has not been tested...
        UserWebsiteStructJsBean bean = null;
        try {
            // TBD:
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonParser parser = factory.createJsonParser(jsonStr);
//            bean = parser.readValueAs(UserWebsiteStructJsBean.class);

            bean = getObjectMapper().readValue(jsonStr, UserWebsiteStructJsBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getPrimarySite()
    {
        return this.primarySite;
    }
    public void setPrimarySite(String primarySite)
    {
        this.primarySite = primarySite;
    }

    public String getHomePage()
    {
        return this.homePage;
    }
    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public String getBlogSite()
    {
        return this.blogSite;
    }
    public void setBlogSite(String blogSite)
    {
        this.blogSite = blogSite;
    }

    public String getPortfolioSite()
    {
        return this.portfolioSite;
    }
    public void setPortfolioSite(String portfolioSite)
    {
        this.portfolioSite = portfolioSite;
    }

    public String getTwitterProfile()
    {
        return this.twitterProfile;
    }
    public void setTwitterProfile(String twitterProfile)
    {
        this.twitterProfile = twitterProfile;
    }

    public String getFacebookProfile()
    {
        return this.facebookProfile;
    }
    public void setFacebookProfile(String facebookProfile)
    {
        this.facebookProfile = facebookProfile;
    }

    public String getGooglePlusProfile()
    {
        return this.googlePlusProfile;
    }
    public void setGooglePlusProfile(String googlePlusProfile)
    {
        this.googlePlusProfile = googlePlusProfile;
    }

    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPrimarySite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHomePage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getBlogSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPortfolioSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGooglePlusProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // To be used as an "object" template in (dynamically generated) JavaScript.
    public static String toNewJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:null, ");
        sb.append("primarySite:null, ");
        sb.append("homePage:null, ");
        sb.append("blogSite:null, ");
        sb.append("portfolioSite:null, ");
        sb.append("twitterProfile:null, ");
        sb.append("facebookProfile:null, ");
        sb.append("googlePlusProfile:null, ");
        sb.append("note:null, ");
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // To be used as an "object" in (dynamically generated) JavaScript.
    public String toJsonObjectString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        sb.append("uuid:");
        if(this.getUuid() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getUuid()).append("\", ");
        }
        sb.append("primarySite:");
        if(this.getPrimarySite() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPrimarySite()).append("\", ");
        }
        sb.append("homePage:");
        if(this.getHomePage() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getHomePage()).append("\", ");
        }
        sb.append("blogSite:");
        if(this.getBlogSite() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getBlogSite()).append("\", ");
        }
        sb.append("portfolioSite:");
        if(this.getPortfolioSite() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getPortfolioSite()).append("\", ");
        }
        sb.append("twitterProfile:");
        if(this.getTwitterProfile() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getTwitterProfile()).append("\", ");
        }
        sb.append("facebookProfile:");
        if(this.getFacebookProfile() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getFacebookProfile()).append("\", ");
        }
        sb.append("googlePlusProfile:");
        if(this.getGooglePlusProfile() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getGooglePlusProfile()).append("\", ");
        }
        sb.append("note:");
        if(this.getNote() == null) {
            sb.append("null, ");
        } else {
            sb.append("\"").append(this.getNote()).append("\", ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
    }

    // Returns the Json string representation of this object.
    public String toJsonString()
    {
        // TBD: Hmm... writeObject() has not been fully tested....
        String jsonStr = null;
        try {
            // TBD: 
//            StringWriter writer = new StringWriter();
//            JsonFactory factory = new JsonFactory();
//            ObjectMapper om = new ObjectMapper();  // ????
//            factory.setCodec(om);
//            JsonGenerator generator =  factory.createJsonGenerator(writer);
//            generator.writeObject(this);
//            jsonStr = writer.toString();

            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;

/*
        StringBuffer sb = new StringBuffer();
        sb.append("{ ");
        if(this.getUuid() != null) {
            sb.append("\"uuid\":").append("\"").append(this.getUuid()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"uuid\":").append("null, ");
        }
        if(this.getPrimarySite() != null) {
            sb.append("\"primarySite\":").append("\"").append(this.getPrimarySite()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"primarySite\":").append("null, ");
        }
        if(this.getHomePage() != null) {
            sb.append("\"homePage\":").append("\"").append(this.getHomePage()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"homePage\":").append("null, ");
        }
        if(this.getBlogSite() != null) {
            sb.append("\"blogSite\":").append("\"").append(this.getBlogSite()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"blogSite\":").append("null, ");
        }
        if(this.getPortfolioSite() != null) {
            sb.append("\"portfolioSite\":").append("\"").append(this.getPortfolioSite()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"portfolioSite\":").append("null, ");
        }
        if(this.getTwitterProfile() != null) {
            sb.append("\"twitterProfile\":").append("\"").append(this.getTwitterProfile()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"twitterProfile\":").append("null, ");
        }
        if(this.getFacebookProfile() != null) {
            sb.append("\"facebookProfile\":").append("\"").append(this.getFacebookProfile()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"facebookProfile\":").append("null, ");
        }
        if(this.getGooglePlusProfile() != null) {
            sb.append("\"googlePlusProfile\":").append("\"").append(this.getGooglePlusProfile()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"googlePlusProfile\":").append("null, ");
        }
        if(this.getNote() != null) {
            sb.append("\"note\":").append("\"").append(this.getNote()).append("\", ");
        } else {
            // TBD: Omit or include these fields?
            // sb.append("\"note\":").append("null, ");
        }
        // TBD: Need to remove the trailing comma.
        sb.append(" }");
        return sb.toString();
*/
    }

    // Returns the Json string representation of this object (to be used in Javascript).
    public String toEscapedJsonStringForJavascript()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJavascript(jsonStr);
        return ecapedStr;
    }

    // Returns the Json string representation of this object (to be used in Java).
    public String toEscapedJsonStringForJava()
    {
        String jsonStr = toJsonString();
        String ecapedStr = StringEscapeUtil.escapeForJava(jsonStr);
        return ecapedStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("uuid = " + this.uuid).append(";");
        sb.append("primarySite = " + this.primarySite).append(";");
        sb.append("homePage = " + this.homePage).append(";");
        sb.append("blogSite = " + this.blogSite).append(";");
        sb.append("portfolioSite = " + this.portfolioSite).append(";");
        sb.append("twitterProfile = " + this.twitterProfile).append(";");
        sb.append("facebookProfile = " + this.facebookProfile).append(";");
        sb.append("googlePlusProfile = " + this.googlePlusProfile).append(";");
        sb.append("note = " + this.note).append(";");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        UserWebsiteStructJsBean cloned = new UserWebsiteStructJsBean();
        cloned.setUuid(this.getUuid());   
        cloned.setPrimarySite(this.getPrimarySite());   
        cloned.setHomePage(this.getHomePage());   
        cloned.setBlogSite(this.getBlogSite());   
        cloned.setPortfolioSite(this.getPortfolioSite());   
        cloned.setTwitterProfile(this.getTwitterProfile());   
        cloned.setFacebookProfile(this.getFacebookProfile());   
        cloned.setGooglePlusProfile(this.getGooglePlusProfile());   
        cloned.setNote(this.getNote());   
        return cloned;
    }

}
