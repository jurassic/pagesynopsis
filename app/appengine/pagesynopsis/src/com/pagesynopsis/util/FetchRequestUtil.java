package com.pagesynopsis.util;

import java.util.logging.Logger;


// Temporary
public final class FetchRequestUtil
{
    private static final Logger log = Logger.getLogger(FetchRequestUtil.class.getName());

    private FetchRequestUtil() {}
    
    
//    // TBD: This should be read from config, etc....
//    public static String getDefaultFetchRequestType()
//    {
//        // temporary
//        return FetchRequestContentType.ITEM_TYPE_TEXT;
//    }

    // TBD:
    public static String createDefaultFetchRequestTitle()
    {
        // temporary
        String title = "FetchRequest " + System.currentTimeMillis();
        return title;
    }

    // TBD:
    public static String createHtmlPageTitle(String memoTitle)
    {
        // temporary
        if(memoTitle == null) {
            memoTitle = "";
        }
        String title = "FetchRequest - " + memoTitle;
        return title;
    }
    
    
}
