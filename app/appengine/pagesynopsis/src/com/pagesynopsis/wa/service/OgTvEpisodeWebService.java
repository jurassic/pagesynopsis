package com.pagesynopsis.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgTvEpisodeJsBean;
import com.pagesynopsis.wa.util.OgAudioStructWebUtil;
import com.pagesynopsis.wa.util.OgImageStructWebUtil;
import com.pagesynopsis.wa.util.OgActorStructWebUtil;
import com.pagesynopsis.wa.util.OgVideoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgTvEpisodeWebService // implements OgTvEpisodeService
{
    private static final Logger log = Logger.getLogger(OgTvEpisodeWebService.class.getName());
     
    // Af service interface.
    private OgTvEpisodeService mService = null;

    public OgTvEpisodeWebService()
    {
        this(ServiceManager.getOgTvEpisodeService());
    }
    public OgTvEpisodeWebService(OgTvEpisodeService service)
    {
        mService = service;
    }
    
    private OgTvEpisodeService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getOgTvEpisodeService();
        }
        return mService;
    }
    
    
    public OgTvEpisodeJsBean getOgTvEpisode(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvEpisode ogTvEpisode = getService().getOgTvEpisode(guid);
            OgTvEpisodeJsBean bean = convertOgTvEpisodeToJsBean(ogTvEpisode);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgTvEpisode(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getOgTvEpisode(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvEpisodeJsBean> getOgTvEpisodes(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvEpisodeJsBean> jsBeans = new ArrayList<OgTvEpisodeJsBean>();
            List<OgTvEpisode> ogTvEpisodes = getService().getOgTvEpisodes(guids);
            if(ogTvEpisodes != null) {
                for(OgTvEpisode ogTvEpisode : ogTvEpisodes) {
                    jsBeans.add(convertOgTvEpisodeToJsBean(ogTvEpisode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvEpisodeJsBean> getAllOgTvEpisodes() throws WebException
    {
        return getAllOgTvEpisodes(null, null, null);
    }

    // @Deprecated
    public List<OgTvEpisodeJsBean> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    public List<OgTvEpisodeJsBean> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvEpisodeJsBean> jsBeans = new ArrayList<OgTvEpisodeJsBean>();
            List<OgTvEpisode> ogTvEpisodes = getService().getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
            if(ogTvEpisodes != null) {
                for(OgTvEpisode ogTvEpisode : ogTvEpisodes) {
                    jsBeans.add(convertOgTvEpisodeToJsBean(ogTvEpisode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgTvEpisodeJsBean> findOgTvEpisodes(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgTvEpisodeJsBean> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgTvEpisodeJsBean> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvEpisodeJsBean> jsBeans = new ArrayList<OgTvEpisodeJsBean>();
            List<OgTvEpisode> ogTvEpisodes = getService().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogTvEpisodes != null) {
                for(OgTvEpisode ogTvEpisode : ogTvEpisodes) {
                    jsBeans.add(convertOgTvEpisodeToJsBean(ogTvEpisode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().createOgTvEpisode(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvEpisode(String jsonStr) throws WebException
    {
        return createOgTvEpisode(OgTvEpisodeJsBean.fromJsonString(jsonStr));
    }

    public String createOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            return getService().createOgTvEpisode(ogTvEpisode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvEpisodeJsBean constructOgTvEpisode(String jsonStr) throws WebException
    {
        return constructOgTvEpisode(OgTvEpisodeJsBean.fromJsonString(jsonStr));
    }

    public OgTvEpisodeJsBean constructOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            ogTvEpisode = getService().constructOgTvEpisode(ogTvEpisode);
            jsBean = convertOgTvEpisodeToJsBean(ogTvEpisode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().updateOgTvEpisode(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgTvEpisode(String jsonStr) throws WebException
    {
        return updateOgTvEpisode(OgTvEpisodeJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            return getService().updateOgTvEpisode(ogTvEpisode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvEpisodeJsBean refreshOgTvEpisode(String jsonStr) throws WebException
    {
        return refreshOgTvEpisode(OgTvEpisodeJsBean.fromJsonString(jsonStr));
    }

    public OgTvEpisodeJsBean refreshOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            ogTvEpisode = getService().refreshOgTvEpisode(ogTvEpisode);
            jsBean = convertOgTvEpisodeToJsBean(ogTvEpisode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvEpisode(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgTvEpisode(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvEpisode(OgTvEpisodeJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvEpisode ogTvEpisode = convertOgTvEpisodeJsBeanToBean(jsBean);
            return getService().deleteOgTvEpisode(ogTvEpisode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgTvEpisodes(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static OgTvEpisodeJsBean convertOgTvEpisodeToJsBean(OgTvEpisode ogTvEpisode)
    {
        OgTvEpisodeJsBean jsBean = null;
        if(ogTvEpisode != null) {
            jsBean = new OgTvEpisodeJsBean();
            jsBean.setGuid(ogTvEpisode.getGuid());
            jsBean.setUrl(ogTvEpisode.getUrl());
            jsBean.setType(ogTvEpisode.getType());
            jsBean.setSiteName(ogTvEpisode.getSiteName());
            jsBean.setTitle(ogTvEpisode.getTitle());
            jsBean.setDescription(ogTvEpisode.getDescription());
            jsBean.setFbAdmins(ogTvEpisode.getFbAdmins());
            jsBean.setFbAppId(ogTvEpisode.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogTvEpisode.getImage();
            if(imageBeans != null) {
                for(OgImageStruct image : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebUtil.convertOgImageStructToJsBean(image);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogTvEpisode.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct audio : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebUtil.convertOgAudioStructToJsBean(audio);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogTvEpisode.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct video : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebUtil.convertOgVideoStructToJsBean(video);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogTvEpisode.getLocale());
            jsBean.setLocaleAlternate(ogTvEpisode.getLocaleAlternate());
            jsBean.setDirector(ogTvEpisode.getDirector());
            jsBean.setWriter(ogTvEpisode.getWriter());
            List<OgActorStructJsBean> actorJsBeans = new ArrayList<OgActorStructJsBean>();
            List<OgActorStruct> actorBeans = ogTvEpisode.getActor();
            if(actorBeans != null) {
                for(OgActorStruct actor : actorBeans) {
                    OgActorStructJsBean jB = OgActorStructWebUtil.convertOgActorStructToJsBean(actor);
                    actorJsBeans.add(jB); 
                }
            }
            jsBean.setActor(actorJsBeans);
            jsBean.setSeries(ogTvEpisode.getSeries());
            jsBean.setDuration(ogTvEpisode.getDuration());
            jsBean.setTag(ogTvEpisode.getTag());
            jsBean.setReleaseDate(ogTvEpisode.getReleaseDate());
            jsBean.setCreatedTime(ogTvEpisode.getCreatedTime());
            jsBean.setModifiedTime(ogTvEpisode.getModifiedTime());
        }
        return jsBean;
    }

    public static OgTvEpisode convertOgTvEpisodeJsBeanToBean(OgTvEpisodeJsBean jsBean)
    {
        OgTvEpisodeBean ogTvEpisode = null;
        if(jsBean != null) {
            ogTvEpisode = new OgTvEpisodeBean();
            ogTvEpisode.setGuid(jsBean.getGuid());
            ogTvEpisode.setUrl(jsBean.getUrl());
            ogTvEpisode.setType(jsBean.getType());
            ogTvEpisode.setSiteName(jsBean.getSiteName());
            ogTvEpisode.setTitle(jsBean.getTitle());
            ogTvEpisode.setDescription(jsBean.getDescription());
            ogTvEpisode.setFbAdmins(jsBean.getFbAdmins());
            ogTvEpisode.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean image : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebUtil.convertOgImageStructJsBeanToBean(image);
                    imageBeans.add(b); 
                }
            }
            ogTvEpisode.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean audio : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebUtil.convertOgAudioStructJsBeanToBean(audio);
                    audioBeans.add(b); 
                }
            }
            ogTvEpisode.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean video : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebUtil.convertOgVideoStructJsBeanToBean(video);
                    videoBeans.add(b); 
                }
            }
            ogTvEpisode.setVideo(videoBeans);
            ogTvEpisode.setLocale(jsBean.getLocale());
            ogTvEpisode.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogTvEpisode.setDirector(jsBean.getDirector());
            ogTvEpisode.setWriter(jsBean.getWriter());
            List<OgActorStruct> actorBeans = new ArrayList<OgActorStruct>();
            List<OgActorStructJsBean> actorJsBeans = jsBean.getActor();
            if(actorJsBeans != null) {
                for(OgActorStructJsBean actor : actorJsBeans) {
                    OgActorStruct b = OgActorStructWebUtil.convertOgActorStructJsBeanToBean(actor);
                    actorBeans.add(b); 
                }
            }
            ogTvEpisode.setActor(actorBeans);
            ogTvEpisode.setSeries(jsBean.getSeries());
            ogTvEpisode.setDuration(jsBean.getDuration());
            ogTvEpisode.setTag(jsBean.getTag());
            ogTvEpisode.setReleaseDate(jsBean.getReleaseDate());
            ogTvEpisode.setCreatedTime(jsBean.getCreatedTime());
            ogTvEpisode.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogTvEpisode;
    }

}
