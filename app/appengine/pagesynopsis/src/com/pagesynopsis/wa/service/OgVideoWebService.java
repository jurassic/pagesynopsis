package com.pagesynopsis.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoJsBean;
import com.pagesynopsis.wa.util.OgAudioStructWebUtil;
import com.pagesynopsis.wa.util.OgImageStructWebUtil;
import com.pagesynopsis.wa.util.OgActorStructWebUtil;
import com.pagesynopsis.wa.util.OgVideoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgVideoWebService // implements OgVideoService
{
    private static final Logger log = Logger.getLogger(OgVideoWebService.class.getName());
     
    // Af service interface.
    private OgVideoService mService = null;

    public OgVideoWebService()
    {
        this(ServiceManager.getOgVideoService());
    }
    public OgVideoWebService(OgVideoService service)
    {
        mService = service;
    }
    
    private OgVideoService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getOgVideoService();
        }
        return mService;
    }
    
    
    public OgVideoJsBean getOgVideo(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgVideo ogVideo = getService().getOgVideo(guid);
            OgVideoJsBean bean = convertOgVideoToJsBean(ogVideo);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgVideo(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getOgVideo(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgVideoJsBean> getOgVideos(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgVideoJsBean> jsBeans = new ArrayList<OgVideoJsBean>();
            List<OgVideo> ogVideos = getService().getOgVideos(guids);
            if(ogVideos != null) {
                for(OgVideo ogVideo : ogVideos) {
                    jsBeans.add(convertOgVideoToJsBean(ogVideo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgVideoJsBean> getAllOgVideos() throws WebException
    {
        return getAllOgVideos(null, null, null);
    }

    // @Deprecated
    public List<OgVideoJsBean> getAllOgVideos(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgVideos(ordering, offset, count, null);
    }

    public List<OgVideoJsBean> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgVideoJsBean> jsBeans = new ArrayList<OgVideoJsBean>();
            List<OgVideo> ogVideos = getService().getAllOgVideos(ordering, offset, count, forwardCursor);
            if(ogVideos != null) {
                for(OgVideo ogVideo : ogVideos) {
                    jsBeans.add(convertOgVideoToJsBean(ogVideo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgVideoKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgVideoJsBean> findOgVideos(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgVideos(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgVideoJsBean> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgVideoJsBean> findOgVideos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgVideoJsBean> jsBeans = new ArrayList<OgVideoJsBean>();
            List<OgVideo> ogVideos = getService().findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogVideos != null) {
                for(OgVideo ogVideo : ogVideos) {
                    jsBeans.add(convertOgVideoToJsBean(ogVideo));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().createOgVideo(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgVideo(String jsonStr) throws WebException
    {
        return createOgVideo(OgVideoJsBean.fromJsonString(jsonStr));
    }

    public String createOgVideo(OgVideoJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgVideo ogVideo = convertOgVideoJsBeanToBean(jsBean);
            return getService().createOgVideo(ogVideo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgVideoJsBean constructOgVideo(String jsonStr) throws WebException
    {
        return constructOgVideo(OgVideoJsBean.fromJsonString(jsonStr));
    }

    public OgVideoJsBean constructOgVideo(OgVideoJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgVideo ogVideo = convertOgVideoJsBeanToBean(jsBean);
            ogVideo = getService().constructOgVideo(ogVideo);
            jsBean = convertOgVideoToJsBean(ogVideo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().updateOgVideo(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgVideo(String jsonStr) throws WebException
    {
        return updateOgVideo(OgVideoJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateOgVideo(OgVideoJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgVideo ogVideo = convertOgVideoJsBeanToBean(jsBean);
            return getService().updateOgVideo(ogVideo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgVideoJsBean refreshOgVideo(String jsonStr) throws WebException
    {
        return refreshOgVideo(OgVideoJsBean.fromJsonString(jsonStr));
    }

    public OgVideoJsBean refreshOgVideo(OgVideoJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgVideo ogVideo = convertOgVideoJsBeanToBean(jsBean);
            ogVideo = getService().refreshOgVideo(ogVideo);
            jsBean = convertOgVideoToJsBean(ogVideo);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgVideo(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgVideo(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgVideo(OgVideoJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgVideo ogVideo = convertOgVideoJsBeanToBean(jsBean);
            return getService().deleteOgVideo(ogVideo);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgVideos(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgVideos(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static OgVideoJsBean convertOgVideoToJsBean(OgVideo ogVideo)
    {
        OgVideoJsBean jsBean = null;
        if(ogVideo != null) {
            jsBean = new OgVideoJsBean();
            jsBean.setGuid(ogVideo.getGuid());
            jsBean.setUrl(ogVideo.getUrl());
            jsBean.setType(ogVideo.getType());
            jsBean.setSiteName(ogVideo.getSiteName());
            jsBean.setTitle(ogVideo.getTitle());
            jsBean.setDescription(ogVideo.getDescription());
            jsBean.setFbAdmins(ogVideo.getFbAdmins());
            jsBean.setFbAppId(ogVideo.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogVideo.getImage();
            if(imageBeans != null) {
                for(OgImageStruct image : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebUtil.convertOgImageStructToJsBean(image);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogVideo.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct audio : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebUtil.convertOgAudioStructToJsBean(audio);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogVideo.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct video : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebUtil.convertOgVideoStructToJsBean(video);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogVideo.getLocale());
            jsBean.setLocaleAlternate(ogVideo.getLocaleAlternate());
            jsBean.setDirector(ogVideo.getDirector());
            jsBean.setWriter(ogVideo.getWriter());
            List<OgActorStructJsBean> actorJsBeans = new ArrayList<OgActorStructJsBean>();
            List<OgActorStruct> actorBeans = ogVideo.getActor();
            if(actorBeans != null) {
                for(OgActorStruct actor : actorBeans) {
                    OgActorStructJsBean jB = OgActorStructWebUtil.convertOgActorStructToJsBean(actor);
                    actorJsBeans.add(jB); 
                }
            }
            jsBean.setActor(actorJsBeans);
            jsBean.setDuration(ogVideo.getDuration());
            jsBean.setTag(ogVideo.getTag());
            jsBean.setReleaseDate(ogVideo.getReleaseDate());
            jsBean.setCreatedTime(ogVideo.getCreatedTime());
            jsBean.setModifiedTime(ogVideo.getModifiedTime());
        }
        return jsBean;
    }

    public static OgVideo convertOgVideoJsBeanToBean(OgVideoJsBean jsBean)
    {
        OgVideoBean ogVideo = null;
        if(jsBean != null) {
            ogVideo = new OgVideoBean();
            ogVideo.setGuid(jsBean.getGuid());
            ogVideo.setUrl(jsBean.getUrl());
            ogVideo.setType(jsBean.getType());
            ogVideo.setSiteName(jsBean.getSiteName());
            ogVideo.setTitle(jsBean.getTitle());
            ogVideo.setDescription(jsBean.getDescription());
            ogVideo.setFbAdmins(jsBean.getFbAdmins());
            ogVideo.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean image : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebUtil.convertOgImageStructJsBeanToBean(image);
                    imageBeans.add(b); 
                }
            }
            ogVideo.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean audio : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebUtil.convertOgAudioStructJsBeanToBean(audio);
                    audioBeans.add(b); 
                }
            }
            ogVideo.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean video : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebUtil.convertOgVideoStructJsBeanToBean(video);
                    videoBeans.add(b); 
                }
            }
            ogVideo.setVideo(videoBeans);
            ogVideo.setLocale(jsBean.getLocale());
            ogVideo.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogVideo.setDirector(jsBean.getDirector());
            ogVideo.setWriter(jsBean.getWriter());
            List<OgActorStruct> actorBeans = new ArrayList<OgActorStruct>();
            List<OgActorStructJsBean> actorJsBeans = jsBean.getActor();
            if(actorJsBeans != null) {
                for(OgActorStructJsBean actor : actorJsBeans) {
                    OgActorStruct b = OgActorStructWebUtil.convertOgActorStructJsBeanToBean(actor);
                    actorBeans.add(b); 
                }
            }
            ogVideo.setActor(actorBeans);
            ogVideo.setDuration(jsBean.getDuration());
            ogVideo.setTag(jsBean.getTag());
            ogVideo.setReleaseDate(jsBean.getReleaseDate());
            ogVideo.setCreatedTime(jsBean.getCreatedTime());
            ogVideo.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogVideo;
    }

}
