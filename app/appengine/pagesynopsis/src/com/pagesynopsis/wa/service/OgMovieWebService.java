package com.pagesynopsis.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;
import com.pagesynopsis.wa.util.OgAudioStructWebUtil;
import com.pagesynopsis.wa.util.OgImageStructWebUtil;
import com.pagesynopsis.wa.util.OgActorStructWebUtil;
import com.pagesynopsis.wa.util.OgVideoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgMovieWebService // implements OgMovieService
{
    private static final Logger log = Logger.getLogger(OgMovieWebService.class.getName());
     
    // Af service interface.
    private OgMovieService mService = null;

    public OgMovieWebService()
    {
        this(ServiceManager.getOgMovieService());
    }
    public OgMovieWebService(OgMovieService service)
    {
        mService = service;
    }
    
    private OgMovieService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getOgMovieService();
        }
        return mService;
    }
    
    
    public OgMovieJsBean getOgMovie(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgMovie ogMovie = getService().getOgMovie(guid);
            OgMovieJsBean bean = convertOgMovieToJsBean(ogMovie);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgMovie(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getOgMovie(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgMovieJsBean> getOgMovies(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgMovieJsBean> jsBeans = new ArrayList<OgMovieJsBean>();
            List<OgMovie> ogMovies = getService().getOgMovies(guids);
            if(ogMovies != null) {
                for(OgMovie ogMovie : ogMovies) {
                    jsBeans.add(convertOgMovieToJsBean(ogMovie));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgMovieJsBean> getAllOgMovies() throws WebException
    {
        return getAllOgMovies(null, null, null);
    }

    // @Deprecated
    public List<OgMovieJsBean> getAllOgMovies(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgMovies(ordering, offset, count, null);
    }

    public List<OgMovieJsBean> getAllOgMovies(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgMovieJsBean> jsBeans = new ArrayList<OgMovieJsBean>();
            List<OgMovie> ogMovies = getService().getAllOgMovies(ordering, offset, count, forwardCursor);
            if(ogMovies != null) {
                for(OgMovie ogMovie : ogMovies) {
                    jsBeans.add(convertOgMovieToJsBean(ogMovie));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgMovieKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgMovieKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgMovieJsBean> findOgMovies(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgMovies(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgMovieJsBean> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgMovieJsBean> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgMovieJsBean> jsBeans = new ArrayList<OgMovieJsBean>();
            List<OgMovie> ogMovies = getService().findOgMovies(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogMovies != null) {
                for(OgMovie ogMovie : ogMovies) {
                    jsBeans.add(convertOgMovieToJsBean(ogMovie));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgMovie(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().createOgMovie(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgMovie(String jsonStr) throws WebException
    {
        return createOgMovie(OgMovieJsBean.fromJsonString(jsonStr));
    }

    public String createOgMovie(OgMovieJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgMovie ogMovie = convertOgMovieJsBeanToBean(jsBean);
            return getService().createOgMovie(ogMovie);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgMovieJsBean constructOgMovie(String jsonStr) throws WebException
    {
        return constructOgMovie(OgMovieJsBean.fromJsonString(jsonStr));
    }

    public OgMovieJsBean constructOgMovie(OgMovieJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgMovie ogMovie = convertOgMovieJsBeanToBean(jsBean);
            ogMovie = getService().constructOgMovie(ogMovie);
            jsBean = convertOgMovieToJsBean(ogMovie);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgMovie(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().updateOgMovie(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgMovie(String jsonStr) throws WebException
    {
        return updateOgMovie(OgMovieJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateOgMovie(OgMovieJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgMovie ogMovie = convertOgMovieJsBeanToBean(jsBean);
            return getService().updateOgMovie(ogMovie);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgMovieJsBean refreshOgMovie(String jsonStr) throws WebException
    {
        return refreshOgMovie(OgMovieJsBean.fromJsonString(jsonStr));
    }

    public OgMovieJsBean refreshOgMovie(OgMovieJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgMovie ogMovie = convertOgMovieJsBeanToBean(jsBean);
            ogMovie = getService().refreshOgMovie(ogMovie);
            jsBean = convertOgMovieToJsBean(ogMovie);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgMovie(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgMovie(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgMovie(OgMovieJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgMovie ogMovie = convertOgMovieJsBeanToBean(jsBean);
            return getService().deleteOgMovie(ogMovie);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgMovies(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgMovies(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static OgMovieJsBean convertOgMovieToJsBean(OgMovie ogMovie)
    {
        OgMovieJsBean jsBean = null;
        if(ogMovie != null) {
            jsBean = new OgMovieJsBean();
            jsBean.setGuid(ogMovie.getGuid());
            jsBean.setUrl(ogMovie.getUrl());
            jsBean.setType(ogMovie.getType());
            jsBean.setSiteName(ogMovie.getSiteName());
            jsBean.setTitle(ogMovie.getTitle());
            jsBean.setDescription(ogMovie.getDescription());
            jsBean.setFbAdmins(ogMovie.getFbAdmins());
            jsBean.setFbAppId(ogMovie.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogMovie.getImage();
            if(imageBeans != null) {
                for(OgImageStruct image : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebUtil.convertOgImageStructToJsBean(image);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogMovie.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct audio : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebUtil.convertOgAudioStructToJsBean(audio);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogMovie.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct video : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebUtil.convertOgVideoStructToJsBean(video);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogMovie.getLocale());
            jsBean.setLocaleAlternate(ogMovie.getLocaleAlternate());
            jsBean.setDirector(ogMovie.getDirector());
            jsBean.setWriter(ogMovie.getWriter());
            List<OgActorStructJsBean> actorJsBeans = new ArrayList<OgActorStructJsBean>();
            List<OgActorStruct> actorBeans = ogMovie.getActor();
            if(actorBeans != null) {
                for(OgActorStruct actor : actorBeans) {
                    OgActorStructJsBean jB = OgActorStructWebUtil.convertOgActorStructToJsBean(actor);
                    actorJsBeans.add(jB); 
                }
            }
            jsBean.setActor(actorJsBeans);
            jsBean.setDuration(ogMovie.getDuration());
            jsBean.setTag(ogMovie.getTag());
            jsBean.setReleaseDate(ogMovie.getReleaseDate());
            jsBean.setCreatedTime(ogMovie.getCreatedTime());
            jsBean.setModifiedTime(ogMovie.getModifiedTime());
        }
        return jsBean;
    }

    public static OgMovie convertOgMovieJsBeanToBean(OgMovieJsBean jsBean)
    {
        OgMovieBean ogMovie = null;
        if(jsBean != null) {
            ogMovie = new OgMovieBean();
            ogMovie.setGuid(jsBean.getGuid());
            ogMovie.setUrl(jsBean.getUrl());
            ogMovie.setType(jsBean.getType());
            ogMovie.setSiteName(jsBean.getSiteName());
            ogMovie.setTitle(jsBean.getTitle());
            ogMovie.setDescription(jsBean.getDescription());
            ogMovie.setFbAdmins(jsBean.getFbAdmins());
            ogMovie.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean image : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebUtil.convertOgImageStructJsBeanToBean(image);
                    imageBeans.add(b); 
                }
            }
            ogMovie.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean audio : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebUtil.convertOgAudioStructJsBeanToBean(audio);
                    audioBeans.add(b); 
                }
            }
            ogMovie.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean video : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebUtil.convertOgVideoStructJsBeanToBean(video);
                    videoBeans.add(b); 
                }
            }
            ogMovie.setVideo(videoBeans);
            ogMovie.setLocale(jsBean.getLocale());
            ogMovie.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogMovie.setDirector(jsBean.getDirector());
            ogMovie.setWriter(jsBean.getWriter());
            List<OgActorStruct> actorBeans = new ArrayList<OgActorStruct>();
            List<OgActorStructJsBean> actorJsBeans = jsBean.getActor();
            if(actorJsBeans != null) {
                for(OgActorStructJsBean actor : actorJsBeans) {
                    OgActorStruct b = OgActorStructWebUtil.convertOgActorStructJsBeanToBean(actor);
                    actorBeans.add(b); 
                }
            }
            ogMovie.setActor(actorBeans);
            ogMovie.setDuration(jsBean.getDuration());
            ogMovie.setTag(jsBean.getTag());
            ogMovie.setReleaseDate(jsBean.getReleaseDate());
            ogMovie.setCreatedTime(jsBean.getCreatedTime());
            ogMovie.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogMovie;
    }

}
