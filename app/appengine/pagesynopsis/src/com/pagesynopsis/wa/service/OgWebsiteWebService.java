package com.pagesynopsis.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgWebsiteJsBean;
import com.pagesynopsis.wa.util.OgAudioStructWebUtil;
import com.pagesynopsis.wa.util.OgImageStructWebUtil;
import com.pagesynopsis.wa.util.OgActorStructWebUtil;
import com.pagesynopsis.wa.util.OgVideoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgWebsiteWebService // implements OgWebsiteService
{
    private static final Logger log = Logger.getLogger(OgWebsiteWebService.class.getName());
     
    // Af service interface.
    private OgWebsiteService mService = null;

    public OgWebsiteWebService()
    {
        this(ServiceManager.getOgWebsiteService());
    }
    public OgWebsiteWebService(OgWebsiteService service)
    {
        mService = service;
    }
    
    private OgWebsiteService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getOgWebsiteService();
        }
        return mService;
    }
    
    
    public OgWebsiteJsBean getOgWebsite(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgWebsite ogWebsite = getService().getOgWebsite(guid);
            OgWebsiteJsBean bean = convertOgWebsiteToJsBean(ogWebsite);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgWebsite(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getOgWebsite(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgWebsiteJsBean> getOgWebsites(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgWebsiteJsBean> jsBeans = new ArrayList<OgWebsiteJsBean>();
            List<OgWebsite> ogWebsites = getService().getOgWebsites(guids);
            if(ogWebsites != null) {
                for(OgWebsite ogWebsite : ogWebsites) {
                    jsBeans.add(convertOgWebsiteToJsBean(ogWebsite));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgWebsiteJsBean> getAllOgWebsites() throws WebException
    {
        return getAllOgWebsites(null, null, null);
    }

    // @Deprecated
    public List<OgWebsiteJsBean> getAllOgWebsites(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgWebsites(ordering, offset, count, null);
    }

    public List<OgWebsiteJsBean> getAllOgWebsites(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgWebsiteJsBean> jsBeans = new ArrayList<OgWebsiteJsBean>();
            List<OgWebsite> ogWebsites = getService().getAllOgWebsites(ordering, offset, count, forwardCursor);
            if(ogWebsites != null) {
                for(OgWebsite ogWebsite : ogWebsites) {
                    jsBeans.add(convertOgWebsiteToJsBean(ogWebsite));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgWebsiteKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgWebsiteKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgWebsiteKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgWebsiteJsBean> findOgWebsites(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgWebsites(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgWebsiteJsBean> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgWebsiteJsBean> findOgWebsites(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgWebsiteJsBean> jsBeans = new ArrayList<OgWebsiteJsBean>();
            List<OgWebsite> ogWebsites = getService().findOgWebsites(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogWebsites != null) {
                for(OgWebsite ogWebsite : ogWebsites) {
                    jsBeans.add(convertOgWebsiteToJsBean(ogWebsite));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgWebsiteKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgWebsiteKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgWebsite(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws WebException
    {
        try {
            return getService().createOgWebsite(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgWebsite(String jsonStr) throws WebException
    {
        return createOgWebsite(OgWebsiteJsBean.fromJsonString(jsonStr));
    }

    public String createOgWebsite(OgWebsiteJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgWebsite ogWebsite = convertOgWebsiteJsBeanToBean(jsBean);
            return getService().createOgWebsite(ogWebsite);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgWebsiteJsBean constructOgWebsite(String jsonStr) throws WebException
    {
        return constructOgWebsite(OgWebsiteJsBean.fromJsonString(jsonStr));
    }

    public OgWebsiteJsBean constructOgWebsite(OgWebsiteJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgWebsite ogWebsite = convertOgWebsiteJsBeanToBean(jsBean);
            ogWebsite = getService().constructOgWebsite(ogWebsite);
            jsBean = convertOgWebsiteToJsBean(ogWebsite);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgWebsite(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws WebException
    {
        try {
            return getService().updateOgWebsite(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, note);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgWebsite(String jsonStr) throws WebException
    {
        return updateOgWebsite(OgWebsiteJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateOgWebsite(OgWebsiteJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgWebsite ogWebsite = convertOgWebsiteJsBeanToBean(jsBean);
            return getService().updateOgWebsite(ogWebsite);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgWebsiteJsBean refreshOgWebsite(String jsonStr) throws WebException
    {
        return refreshOgWebsite(OgWebsiteJsBean.fromJsonString(jsonStr));
    }

    public OgWebsiteJsBean refreshOgWebsite(OgWebsiteJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgWebsite ogWebsite = convertOgWebsiteJsBeanToBean(jsBean);
            ogWebsite = getService().refreshOgWebsite(ogWebsite);
            jsBean = convertOgWebsiteToJsBean(ogWebsite);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgWebsite(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgWebsite(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgWebsite(OgWebsiteJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgWebsite ogWebsite = convertOgWebsiteJsBeanToBean(jsBean);
            return getService().deleteOgWebsite(ogWebsite);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgWebsites(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgWebsites(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static OgWebsiteJsBean convertOgWebsiteToJsBean(OgWebsite ogWebsite)
    {
        OgWebsiteJsBean jsBean = null;
        if(ogWebsite != null) {
            jsBean = new OgWebsiteJsBean();
            jsBean.setGuid(ogWebsite.getGuid());
            jsBean.setUrl(ogWebsite.getUrl());
            jsBean.setType(ogWebsite.getType());
            jsBean.setSiteName(ogWebsite.getSiteName());
            jsBean.setTitle(ogWebsite.getTitle());
            jsBean.setDescription(ogWebsite.getDescription());
            jsBean.setFbAdmins(ogWebsite.getFbAdmins());
            jsBean.setFbAppId(ogWebsite.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogWebsite.getImage();
            if(imageBeans != null) {
                for(OgImageStruct image : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebUtil.convertOgImageStructToJsBean(image);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogWebsite.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct audio : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebUtil.convertOgAudioStructToJsBean(audio);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogWebsite.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct video : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebUtil.convertOgVideoStructToJsBean(video);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogWebsite.getLocale());
            jsBean.setLocaleAlternate(ogWebsite.getLocaleAlternate());
            jsBean.setNote(ogWebsite.getNote());
            jsBean.setCreatedTime(ogWebsite.getCreatedTime());
            jsBean.setModifiedTime(ogWebsite.getModifiedTime());
        }
        return jsBean;
    }

    public static OgWebsite convertOgWebsiteJsBeanToBean(OgWebsiteJsBean jsBean)
    {
        OgWebsiteBean ogWebsite = null;
        if(jsBean != null) {
            ogWebsite = new OgWebsiteBean();
            ogWebsite.setGuid(jsBean.getGuid());
            ogWebsite.setUrl(jsBean.getUrl());
            ogWebsite.setType(jsBean.getType());
            ogWebsite.setSiteName(jsBean.getSiteName());
            ogWebsite.setTitle(jsBean.getTitle());
            ogWebsite.setDescription(jsBean.getDescription());
            ogWebsite.setFbAdmins(jsBean.getFbAdmins());
            ogWebsite.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean image : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebUtil.convertOgImageStructJsBeanToBean(image);
                    imageBeans.add(b); 
                }
            }
            ogWebsite.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean audio : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebUtil.convertOgAudioStructJsBeanToBean(audio);
                    audioBeans.add(b); 
                }
            }
            ogWebsite.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean video : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebUtil.convertOgVideoStructJsBeanToBean(video);
                    videoBeans.add(b); 
                }
            }
            ogWebsite.setVideo(videoBeans);
            ogWebsite.setLocale(jsBean.getLocale());
            ogWebsite.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogWebsite.setNote(jsBean.getNote());
            ogWebsite.setCreatedTime(jsBean.getCreatedTime());
            ogWebsite.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogWebsite;
    }

}
