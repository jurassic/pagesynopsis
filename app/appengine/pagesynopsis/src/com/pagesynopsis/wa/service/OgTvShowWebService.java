package com.pagesynopsis.wa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.manager.ServiceManager;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgTvShowJsBean;
import com.pagesynopsis.wa.util.OgAudioStructWebUtil;
import com.pagesynopsis.wa.util.OgImageStructWebUtil;
import com.pagesynopsis.wa.util.OgActorStructWebUtil;
import com.pagesynopsis.wa.util.OgVideoStructWebUtil;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgTvShowWebService // implements OgTvShowService
{
    private static final Logger log = Logger.getLogger(OgTvShowWebService.class.getName());
     
    // Af service interface.
    private OgTvShowService mService = null;

    public OgTvShowWebService()
    {
        this(ServiceManager.getOgTvShowService());
    }
    public OgTvShowWebService(OgTvShowService service)
    {
        mService = service;
    }
    
    private OgTvShowService getService()
    {
        if(mService == null) {
            mService = ServiceManager.getOgTvShowService();
        }
        return mService;
    }
    
    
    public OgTvShowJsBean getOgTvShow(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvShow ogTvShow = getService().getOgTvShow(guid);
            OgTvShowJsBean bean = convertOgTvShowToJsBean(ogTvShow);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getOgTvShow(String guid, String field) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().getOgTvShow(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvShowJsBean> getOgTvShows(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvShowJsBean> jsBeans = new ArrayList<OgTvShowJsBean>();
            List<OgTvShow> ogTvShows = getService().getOgTvShows(guids);
            if(ogTvShows != null) {
                for(OgTvShow ogTvShow : ogTvShows) {
                    jsBeans.add(convertOgTvShowToJsBean(ogTvShow));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<OgTvShowJsBean> getAllOgTvShows() throws WebException
    {
        return getAllOgTvShows(null, null, null);
    }

    // @Deprecated
    public List<OgTvShowJsBean> getAllOgTvShows(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvShows(ordering, offset, count, null);
    }

    public List<OgTvShowJsBean> getAllOgTvShows(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvShowJsBean> jsBeans = new ArrayList<OgTvShowJsBean>();
            List<OgTvShow> ogTvShows = getService().getAllOgTvShows(ordering, offset, count, forwardCursor);
            if(ogTvShows != null) {
                for(OgTvShow ogTvShow : ogTvShows) {
                    jsBeans.add(convertOgTvShowToJsBean(ogTvShow));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count) throws WebException
    {
        return getAllOgTvShowKeys(ordering, offset, count, null);
    }

    public List<String> getAllOgTvShowKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllOgTvShowKeys(ordering, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<OgTvShowJsBean> findOgTvShows(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findOgTvShows(filter, ordering, params, values, null, null, null, null);
    }

    // @Deprecated
    public List<OgTvShowJsBean> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<OgTvShowJsBean> findOgTvShows(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<OgTvShowJsBean> jsBeans = new ArrayList<OgTvShowJsBean>();
            List<OgTvShow> ogTvShows = getService().findOgTvShows(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            if(ogTvShows != null) {
                for(OgTvShow ogTvShow : ogTvShows) {
                    jsBeans.add(convertOgTvShowToJsBean(ogTvShow));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    // @Deprecated
    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        return findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    public List<String> findOgTvShowKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findOgTvShowKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        log.finer("BEGIN");

        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvShow(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().createOgTvShow(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createOgTvShow(String jsonStr) throws WebException
    {
        return createOgTvShow(OgTvShowJsBean.fromJsonString(jsonStr));
    }

    public String createOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            return getService().createOgTvShow(ogTvShow);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvShowJsBean constructOgTvShow(String jsonStr) throws WebException
    {
        return constructOgTvShow(OgTvShowJsBean.fromJsonString(jsonStr));
    }

    public OgTvShowJsBean constructOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            ogTvShow = getService().constructOgTvShow(ogTvShow);
            jsBean = convertOgTvShowToJsBean(ogTvShow);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateOgTvShow(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws WebException
    {
        try {
            return getService().updateOgTvShow(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateOgTvShow(String jsonStr) throws WebException
    {
        return updateOgTvShow(OgTvShowJsBean.fromJsonString(jsonStr));
    }

    public Boolean updateOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            return getService().updateOgTvShow(ogTvShow);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public OgTvShowJsBean refreshOgTvShow(String jsonStr) throws WebException
    {
        return refreshOgTvShow(OgTvShowJsBean.fromJsonString(jsonStr));
    }

    public OgTvShowJsBean refreshOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            ogTvShow = getService().refreshOgTvShow(ogTvShow);
            jsBean = convertOgTvShowToJsBean(ogTvShow);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvShow(String guid) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgTvShow(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteOgTvShow(OgTvShowJsBean jsBean) throws WebException
    {
        log.finer("BEGIN");

        try {
            OgTvShow ogTvShow = convertOgTvShowJsBeanToBean(jsBean);
            return getService().deleteOgTvShow(ogTvShow);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteOgTvShows(String filter, String params, List<String> values) throws WebException
    {
        log.finer("BEGIN");

        try {
            return getService().deleteOgTvShows(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }


    public static OgTvShowJsBean convertOgTvShowToJsBean(OgTvShow ogTvShow)
    {
        OgTvShowJsBean jsBean = null;
        if(ogTvShow != null) {
            jsBean = new OgTvShowJsBean();
            jsBean.setGuid(ogTvShow.getGuid());
            jsBean.setUrl(ogTvShow.getUrl());
            jsBean.setType(ogTvShow.getType());
            jsBean.setSiteName(ogTvShow.getSiteName());
            jsBean.setTitle(ogTvShow.getTitle());
            jsBean.setDescription(ogTvShow.getDescription());
            jsBean.setFbAdmins(ogTvShow.getFbAdmins());
            jsBean.setFbAppId(ogTvShow.getFbAppId());
            List<OgImageStructJsBean> imageJsBeans = new ArrayList<OgImageStructJsBean>();
            List<OgImageStruct> imageBeans = ogTvShow.getImage();
            if(imageBeans != null) {
                for(OgImageStruct image : imageBeans) {
                    OgImageStructJsBean jB = OgImageStructWebUtil.convertOgImageStructToJsBean(image);
                    imageJsBeans.add(jB); 
                }
            }
            jsBean.setImage(imageJsBeans);
            List<OgAudioStructJsBean> audioJsBeans = new ArrayList<OgAudioStructJsBean>();
            List<OgAudioStruct> audioBeans = ogTvShow.getAudio();
            if(audioBeans != null) {
                for(OgAudioStruct audio : audioBeans) {
                    OgAudioStructJsBean jB = OgAudioStructWebUtil.convertOgAudioStructToJsBean(audio);
                    audioJsBeans.add(jB); 
                }
            }
            jsBean.setAudio(audioJsBeans);
            List<OgVideoStructJsBean> videoJsBeans = new ArrayList<OgVideoStructJsBean>();
            List<OgVideoStruct> videoBeans = ogTvShow.getVideo();
            if(videoBeans != null) {
                for(OgVideoStruct video : videoBeans) {
                    OgVideoStructJsBean jB = OgVideoStructWebUtil.convertOgVideoStructToJsBean(video);
                    videoJsBeans.add(jB); 
                }
            }
            jsBean.setVideo(videoJsBeans);
            jsBean.setLocale(ogTvShow.getLocale());
            jsBean.setLocaleAlternate(ogTvShow.getLocaleAlternate());
            jsBean.setDirector(ogTvShow.getDirector());
            jsBean.setWriter(ogTvShow.getWriter());
            List<OgActorStructJsBean> actorJsBeans = new ArrayList<OgActorStructJsBean>();
            List<OgActorStruct> actorBeans = ogTvShow.getActor();
            if(actorBeans != null) {
                for(OgActorStruct actor : actorBeans) {
                    OgActorStructJsBean jB = OgActorStructWebUtil.convertOgActorStructToJsBean(actor);
                    actorJsBeans.add(jB); 
                }
            }
            jsBean.setActor(actorJsBeans);
            jsBean.setDuration(ogTvShow.getDuration());
            jsBean.setTag(ogTvShow.getTag());
            jsBean.setReleaseDate(ogTvShow.getReleaseDate());
            jsBean.setCreatedTime(ogTvShow.getCreatedTime());
            jsBean.setModifiedTime(ogTvShow.getModifiedTime());
        }
        return jsBean;
    }

    public static OgTvShow convertOgTvShowJsBeanToBean(OgTvShowJsBean jsBean)
    {
        OgTvShowBean ogTvShow = null;
        if(jsBean != null) {
            ogTvShow = new OgTvShowBean();
            ogTvShow.setGuid(jsBean.getGuid());
            ogTvShow.setUrl(jsBean.getUrl());
            ogTvShow.setType(jsBean.getType());
            ogTvShow.setSiteName(jsBean.getSiteName());
            ogTvShow.setTitle(jsBean.getTitle());
            ogTvShow.setDescription(jsBean.getDescription());
            ogTvShow.setFbAdmins(jsBean.getFbAdmins());
            ogTvShow.setFbAppId(jsBean.getFbAppId());
            List<OgImageStruct> imageBeans = new ArrayList<OgImageStruct>();
            List<OgImageStructJsBean> imageJsBeans = jsBean.getImage();
            if(imageJsBeans != null) {
                for(OgImageStructJsBean image : imageJsBeans) {
                    OgImageStruct b = OgImageStructWebUtil.convertOgImageStructJsBeanToBean(image);
                    imageBeans.add(b); 
                }
            }
            ogTvShow.setImage(imageBeans);
            List<OgAudioStruct> audioBeans = new ArrayList<OgAudioStruct>();
            List<OgAudioStructJsBean> audioJsBeans = jsBean.getAudio();
            if(audioJsBeans != null) {
                for(OgAudioStructJsBean audio : audioJsBeans) {
                    OgAudioStruct b = OgAudioStructWebUtil.convertOgAudioStructJsBeanToBean(audio);
                    audioBeans.add(b); 
                }
            }
            ogTvShow.setAudio(audioBeans);
            List<OgVideoStruct> videoBeans = new ArrayList<OgVideoStruct>();
            List<OgVideoStructJsBean> videoJsBeans = jsBean.getVideo();
            if(videoJsBeans != null) {
                for(OgVideoStructJsBean video : videoJsBeans) {
                    OgVideoStruct b = OgVideoStructWebUtil.convertOgVideoStructJsBeanToBean(video);
                    videoBeans.add(b); 
                }
            }
            ogTvShow.setVideo(videoBeans);
            ogTvShow.setLocale(jsBean.getLocale());
            ogTvShow.setLocaleAlternate(jsBean.getLocaleAlternate());
            ogTvShow.setDirector(jsBean.getDirector());
            ogTvShow.setWriter(jsBean.getWriter());
            List<OgActorStruct> actorBeans = new ArrayList<OgActorStruct>();
            List<OgActorStructJsBean> actorJsBeans = jsBean.getActor();
            if(actorJsBeans != null) {
                for(OgActorStructJsBean actor : actorJsBeans) {
                    OgActorStruct b = OgActorStructWebUtil.convertOgActorStructJsBeanToBean(actor);
                    actorBeans.add(b); 
                }
            }
            ogTvShow.setActor(actorBeans);
            ogTvShow.setDuration(jsBean.getDuration());
            ogTvShow.setTag(jsBean.getTag());
            ogTvShow.setReleaseDate(jsBean.getReleaseDate());
            ogTvShow.setCreatedTime(jsBean.getCreatedTime());
            ogTvShow.setModifiedTime(jsBean.getModifiedTime());
        }
        return ogTvShow;
    }

}
