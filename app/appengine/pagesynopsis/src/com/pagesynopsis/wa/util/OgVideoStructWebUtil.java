package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;


public class OgVideoStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgVideoStructWebUtil.class.getName());

    // Static methods only.
    private OgVideoStructWebUtil() {}
    

    public static OgVideoStructJsBean convertOgVideoStructToJsBean(OgVideoStruct ogVideoStruct)
    {
        OgVideoStructJsBean jsBean = null;
        if(ogVideoStruct != null) {
            jsBean = new OgVideoStructJsBean();
            jsBean.setUuid(ogVideoStruct.getUuid());
            jsBean.setUrl(ogVideoStruct.getUrl());
            jsBean.setSecureUrl(ogVideoStruct.getSecureUrl());
            jsBean.setType(ogVideoStruct.getType());
            jsBean.setWidth(ogVideoStruct.getWidth());
            jsBean.setHeight(ogVideoStruct.getHeight());
        }
        return jsBean;
    }

    public static OgVideoStruct convertOgVideoStructJsBeanToBean(OgVideoStructJsBean jsBean)
    {
        OgVideoStructBean ogVideoStruct = null;
        if(jsBean != null) {
            ogVideoStruct = new OgVideoStructBean();
            ogVideoStruct.setUuid(jsBean.getUuid());
            ogVideoStruct.setUrl(jsBean.getUrl());
            ogVideoStruct.setSecureUrl(jsBean.getSecureUrl());
            ogVideoStruct.setType(jsBean.getType());
            ogVideoStruct.setWidth(jsBean.getWidth());
            ogVideoStruct.setHeight(jsBean.getHeight());
        }
        return ogVideoStruct;
    }

}
