package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;


public class OgImageStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgImageStructWebUtil.class.getName());

    // Static methods only.
    private OgImageStructWebUtil() {}
    

    public static OgImageStructJsBean convertOgImageStructToJsBean(OgImageStruct ogImageStruct)
    {
        OgImageStructJsBean jsBean = null;
        if(ogImageStruct != null) {
            jsBean = new OgImageStructJsBean();
            jsBean.setUuid(ogImageStruct.getUuid());
            jsBean.setUrl(ogImageStruct.getUrl());
            jsBean.setSecureUrl(ogImageStruct.getSecureUrl());
            jsBean.setType(ogImageStruct.getType());
            jsBean.setWidth(ogImageStruct.getWidth());
            jsBean.setHeight(ogImageStruct.getHeight());
        }
        return jsBean;
    }

    public static OgImageStruct convertOgImageStructJsBeanToBean(OgImageStructJsBean jsBean)
    {
        OgImageStructBean ogImageStruct = null;
        if(jsBean != null) {
            ogImageStruct = new OgImageStructBean();
            ogImageStruct.setUuid(jsBean.getUuid());
            ogImageStruct.setUrl(jsBean.getUrl());
            ogImageStruct.setSecureUrl(jsBean.getSecureUrl());
            ogImageStruct.setType(jsBean.getType());
            ogImageStruct.setWidth(jsBean.getWidth());
            ogImageStruct.setHeight(jsBean.getHeight());
        }
        return ogImageStruct;
    }

}
