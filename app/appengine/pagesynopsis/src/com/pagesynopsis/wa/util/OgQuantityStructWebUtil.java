package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.fe.bean.OgQuantityStructJsBean;


public class OgQuantityStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgQuantityStructWebUtil.class.getName());

    // Static methods only.
    private OgQuantityStructWebUtil() {}
    

    public static OgQuantityStructJsBean convertOgQuantityStructToJsBean(OgQuantityStruct ogQuantityStruct)
    {
        OgQuantityStructJsBean jsBean = null;
        if(ogQuantityStruct != null) {
            jsBean = new OgQuantityStructJsBean();
            jsBean.setUuid(ogQuantityStruct.getUuid());
            jsBean.setValue(ogQuantityStruct.getValue());
            jsBean.setUnits(ogQuantityStruct.getUnits());
        }
        return jsBean;
    }

    public static OgQuantityStruct convertOgQuantityStructJsBeanToBean(OgQuantityStructJsBean jsBean)
    {
        OgQuantityStructBean ogQuantityStruct = null;
        if(jsBean != null) {
            ogQuantityStruct = new OgQuantityStructBean();
            ogQuantityStruct.setUuid(jsBean.getUuid());
            ogQuantityStruct.setValue(jsBean.getValue());
            ogQuantityStruct.setUnits(jsBean.getUnits());
        }
        return ogQuantityStruct;
    }

}
