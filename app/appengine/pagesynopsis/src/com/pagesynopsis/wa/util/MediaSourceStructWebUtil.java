package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.fe.bean.MediaSourceStructJsBean;


public class MediaSourceStructWebUtil
{
    private static final Logger log = Logger.getLogger(MediaSourceStructWebUtil.class.getName());

    // Static methods only.
    private MediaSourceStructWebUtil() {}
    

    public static MediaSourceStructJsBean convertMediaSourceStructToJsBean(MediaSourceStruct mediaSourceStruct)
    {
        MediaSourceStructJsBean jsBean = null;
        if(mediaSourceStruct != null) {
            jsBean = new MediaSourceStructJsBean();
            jsBean.setUuid(mediaSourceStruct.getUuid());
            jsBean.setSrc(mediaSourceStruct.getSrc());
            jsBean.setSrcUrl(mediaSourceStruct.getSrcUrl());
            jsBean.setType(mediaSourceStruct.getType());
            jsBean.setRemark(mediaSourceStruct.getRemark());
        }
        return jsBean;
    }

    public static MediaSourceStruct convertMediaSourceStructJsBeanToBean(MediaSourceStructJsBean jsBean)
    {
        MediaSourceStructBean mediaSourceStruct = null;
        if(jsBean != null) {
            mediaSourceStruct = new MediaSourceStructBean();
            mediaSourceStruct.setUuid(jsBean.getUuid());
            mediaSourceStruct.setSrc(jsBean.getSrc());
            mediaSourceStruct.setSrcUrl(jsBean.getSrcUrl());
            mediaSourceStruct.setType(jsBean.getType());
            mediaSourceStruct.setRemark(jsBean.getRemark());
        }
        return mediaSourceStruct;
    }

}
