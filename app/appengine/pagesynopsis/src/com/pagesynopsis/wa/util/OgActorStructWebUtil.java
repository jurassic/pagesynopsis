package com.pagesynopsis.wa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;


public class OgActorStructWebUtil
{
    private static final Logger log = Logger.getLogger(OgActorStructWebUtil.class.getName());

    // Static methods only.
    private OgActorStructWebUtil() {}
    

    public static OgActorStructJsBean convertOgActorStructToJsBean(OgActorStruct ogActorStruct)
    {
        OgActorStructJsBean jsBean = null;
        if(ogActorStruct != null) {
            jsBean = new OgActorStructJsBean();
            jsBean.setUuid(ogActorStruct.getUuid());
            jsBean.setProfile(ogActorStruct.getProfile());
            jsBean.setRole(ogActorStruct.getRole());
        }
        return jsBean;
    }

    public static OgActorStruct convertOgActorStructJsBeanToBean(OgActorStructJsBean jsBean)
    {
        OgActorStructBean ogActorStruct = null;
        if(jsBean != null) {
            ogActorStruct = new OgActorStructBean();
            ogActorStruct.setUuid(jsBean.getUuid());
            ogActorStruct.setProfile(jsBean.getProfile());
            ogActorStruct.setRole(jsBean.getRole());
        }
        return ogActorStruct;
    }

}
