package com.pagesynopsis.form.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.fe.Validateable;
import com.pagesynopsis.fe.core.StringEscapeUtil;
import com.pagesynopsis.fe.bean.OgAudioStructJsBean;
import com.pagesynopsis.fe.bean.OgImageStructJsBean;
import com.pagesynopsis.fe.bean.OgActorStructJsBean;
import com.pagesynopsis.fe.bean.OgVideoStructJsBean;
import com.pagesynopsis.fe.bean.OgMovieJsBean;


// Place holder...
public class OgMovieFormBean extends OgMovieJsBean implements Serializable, Cloneable, Validateable  //, OgMovie
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgMovieFormBean.class.getName());

    private static ObjectMapper sObjectMapper = null;
    private static ObjectMapper getObjectMapper()
    {
        if(sObjectMapper == null) {
            sObjectMapper = new ObjectMapper(); // can reuse, share globally
            sObjectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
        }
        return sObjectMapper;
    }

    // Error map: "field name" -> List<"error message">.
    private Map<String,List<String>> errorMap = new HashMap<String,List<String>>();

    // Ctors.
    public OgMovieFormBean()
    {
        super();
    }
    public OgMovieFormBean(String guid)
    {
       super(guid);
    }
    public OgMovieFormBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStructJsBean> image, List<OgAudioStructJsBean> audio, List<OgVideoStructJsBean> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStructJsBean> actor, Integer duration, List<String> tag, String releaseDate)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
    }
    public OgMovieFormBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStructJsBean> image, List<OgAudioStructJsBean> audio, List<OgVideoStructJsBean> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStructJsBean> actor, Integer duration, List<String> tag, String releaseDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate, createdTime, modifiedTime);
    }
    public OgMovieFormBean(OgMovieJsBean bean)
    {
        super(bean);
    }

    public static OgMovieFormBean fromJsonString(String jsonStr)
    {
        OgMovieFormBean bean = null;
        try {
            // TBD:
            bean = getObjectMapper().readValue(jsonStr, OgMovieFormBean.class);
        } catch (JsonParseException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to parse jsonStr = " + jsonStr, e);
        } catch (JsonProcessingException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to process jsonStr = " + jsonStr, e);
        } catch (IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Exception while processing jsonStr = " + jsonStr, e);
        }
        return bean;
    }

    
    public Map<String,List<String>> getErrorMap()
    {
        return errorMap;
    }

    public boolean hasErrors() 
    {
        // temporary. (An error without error message?)
        if(errorMap.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    public boolean hasErrors(String f) 
    {
        // temporary. (An error without error message?)
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return false;
        } else {
            return true;
        }        
    }

    public String getLastError(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null || errorList.isEmpty()) {
            return null;
        } else {
            return errorList.get(errorList.size() - 1);
        }
    }
    public List<String> getErrors(String f) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            return new ArrayList<String>();
        } else {
            return errorList;
        }
    }

    public List<String> addError(String f, String error) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.add(error);
        return errorList;
    }
    public void setError(String f, String error) 
    {
        List<String> errorList = new ArrayList<String>();
        errorList.add(error);
        errorMap.put(f, errorList);
    }
    public List<String> addErrors(String f, List<String> errors) 
    {
        List<String> errorList = errorMap.get(f);
        if(errorList == null) {
            errorList = new ArrayList<String>();
            errorMap.put(f, errorList);
        }
        errorList.addAll(errors);
        return errorList;
    }
    public void setErrors(String f, List<String> errors) 
    {
        errorMap.put(f, errors);
    }

    public void resetErrors()
    {
        errorMap.clear();
    }
    public void resetErrors(String f)
    {
        errorMap.remove(f);
    }


    public boolean validate()
    {
        boolean allOK = true;
       
//        // TBD
//        if(getDirector() == null) {
//            addError("director", "director is null");
//            allOK = false;
//        }
//        // TBD
//        if(getWriter() == null) {
//            addError("writer", "writer is null");
//            allOK = false;
//        }
//        // TBD
//        if(getActor() == null) {
//            addError("actor", "actor is null");
//            allOK = false;
//        }
//        // TBD
//        if(getDuration() == null) {
//            addError("duration", "duration is null");
//            allOK = false;
//        }
//        // TBD
//        if(getTag() == null) {
//            addError("tag", "tag is null");
//            allOK = false;
//        }
//        // TBD
//        if(getReleaseDate() == null) {
//            addError("releaseDate", "releaseDate is null");
//            allOK = false;
//        }

        return allOK;
    }


    public String toJsonString()
    {
        String jsonStr = null;
        try {
            // TBD: 
            StringWriter writer = new StringWriter();
            getObjectMapper().writeValue(writer, this);
            jsonStr = writer.toString();
        } catch (IOException e) {
            log.log(Level.WARNING, "Exception while writing jsonString.", e);
        }
        return jsonStr;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( super.toString() );
        sb.append("error = {");
        for(String f : errorMap.keySet()) {
            List<String> errorList = errorMap.get(f);
            if(errorList != null && !errorList.isEmpty()) {
                sb.append(f).append(": [");
                for(String e : errorList) {
                    sb.append(e).append("; ");
                }
                sb.append("];");
            }
        }
        sb.append("};");
        return sb.toString();
    }

    public Object clone() // throws CloneNotSupportedException
    {
        OgMovieFormBean cloned = new OgMovieFormBean((OgMovieJsBean) super.clone());
        return cloned;
    }

}
