package com.pagesynopsis.af.cert.proxy;

import com.pagesynopsis.ws.cert.service.PublicCertificateInfoService;

public interface PublicCertificateInfoServiceProxy extends PublicCertificateInfoService
{

}
