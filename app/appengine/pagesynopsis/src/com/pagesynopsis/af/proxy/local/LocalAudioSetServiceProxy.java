package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.AudioSetService;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;

public class LocalAudioSetServiceProxy extends BaseLocalServiceProxy implements AudioSetServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalAudioSetServiceProxy.class.getName());

    public LocalAudioSetServiceProxy()
    {
    }

    @Override
    public AudioSet getAudioSet(String guid) throws BaseException
    {
        return getAudioSetService().getAudioSet(guid);
    }

    @Override
    public Object getAudioSet(String guid, String field) throws BaseException
    {
        return getAudioSetService().getAudioSet(guid, field);       
    }

    @Override
    public List<AudioSet> getAudioSets(List<String> guids) throws BaseException
    {
        return getAudioSetService().getAudioSets(guids);
    }

    @Override
    public List<AudioSet> getAllAudioSets() throws BaseException
    {
        return getAllAudioSets(null, null, null);
    }

    @Override
    public List<AudioSet> getAllAudioSets(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getAudioSetService().getAllAudioSets(ordering, offset, count);
        return getAllAudioSets(ordering, offset, count, null);
    }

    @Override
    public List<AudioSet> getAllAudioSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAudioSetService().getAllAudioSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getAudioSetService().getAllAudioSetKeys(ordering, offset, count);
        return getAllAudioSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAudioSetService().getAllAudioSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findAudioSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getAudioSetService().findAudioSets(filter, ordering, params, values, grouping, unique, offset, count);
        return findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAudioSetService().findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getAudioSetService().findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getAudioSetService().findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getAudioSetService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createAudioSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios) throws BaseException
    {
        return getAudioSetService().createAudioSet(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios);
    }

    @Override
    public String createAudioSet(AudioSet audioSet) throws BaseException
    {
        return getAudioSetService().createAudioSet(audioSet);
    }

    @Override
    public Boolean updateAudioSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<AudioStruct> pageAudios) throws BaseException
    {
        return getAudioSetService().updateAudioSet(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageAudios);
    }

    @Override
    public Boolean updateAudioSet(AudioSet audioSet) throws BaseException
    {
        return getAudioSetService().updateAudioSet(audioSet);
    }

    @Override
    public Boolean deleteAudioSet(String guid) throws BaseException
    {
        return getAudioSetService().deleteAudioSet(guid);
    }

    @Override
    public Boolean deleteAudioSet(AudioSet audioSet) throws BaseException
    {
        return getAudioSetService().deleteAudioSet(audioSet);
    }

    @Override
    public Long deleteAudioSets(String filter, String params, List<String> values) throws BaseException
    {
        return getAudioSetService().deleteAudioSets(filter, params, values);
    }

}
