package com.pagesynopsis.af.proxy.async;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.ApiConsumerServiceProxy;
import com.pagesynopsis.af.proxy.UserServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.af.proxy.TwitterSummaryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPlayerCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;
import com.pagesynopsis.af.proxy.VideoSetServiceProxy;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.af.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;
import com.pagesynopsis.af.proxy.ServiceInfoServiceProxy;
import com.pagesynopsis.af.proxy.FiveTenServiceProxy;

public class AsyncProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(AsyncProxyFactory.class.getName());

    private AsyncProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AsyncProxyFactoryHolder
    {
        private static final AsyncProxyFactory INSTANCE = new AsyncProxyFactory();
    }

    // Singleton method
    public static AsyncProxyFactory getInstance()
    {
        return AsyncProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new AsyncApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new AsyncUserServiceProxy();
    }

    @Override
    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        return new AsyncRobotsTextFileServiceProxy();
    }

    @Override
    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        return new AsyncRobotsTextRefreshServiceProxy();
    }

    @Override
    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        return new AsyncOgProfileServiceProxy();
    }

    @Override
    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        return new AsyncOgWebsiteServiceProxy();
    }

    @Override
    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        return new AsyncOgBlogServiceProxy();
    }

    @Override
    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        return new AsyncOgArticleServiceProxy();
    }

    @Override
    public OgBookServiceProxy getOgBookServiceProxy()
    {
        return new AsyncOgBookServiceProxy();
    }

    @Override
    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        return new AsyncOgVideoServiceProxy();
    }

    @Override
    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        return new AsyncOgMovieServiceProxy();
    }

    @Override
    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        return new AsyncOgTvShowServiceProxy();
    }

    @Override
    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        return new AsyncOgTvEpisodeServiceProxy();
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new AsyncTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new AsyncTwitterPhotoCardServiceProxy();
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new AsyncTwitterGalleryCardServiceProxy();
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new AsyncTwitterAppCardServiceProxy();
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new AsyncTwitterPlayerCardServiceProxy();
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new AsyncTwitterProductCardServiceProxy();
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        return new AsyncFetchRequestServiceProxy();
    }

    @Override
    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        return new AsyncPageInfoServiceProxy();
    }

    @Override
    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        return new AsyncPageFetchServiceProxy();
    }

    @Override
    public LinkListServiceProxy getLinkListServiceProxy()
    {
        return new AsyncLinkListServiceProxy();
    }

    @Override
    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        return new AsyncImageSetServiceProxy();
    }

    @Override
    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        return new AsyncAudioSetServiceProxy();
    }

    @Override
    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        return new AsyncVideoSetServiceProxy();
    }

    @Override
    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        return new AsyncOpenGraphMetaServiceProxy();
    }

    @Override
    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        return new AsyncTwitterCardMetaServiceProxy();
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new AsyncDomainInfoServiceProxy();
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new AsyncUrlRatingServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new AsyncServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new AsyncFiveTenServiceProxy();
    }

}
