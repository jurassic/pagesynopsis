package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.service.RobotsTextFileService;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;

public class LocalRobotsTextFileServiceProxy extends BaseLocalServiceProxy implements RobotsTextFileServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalRobotsTextFileServiceProxy.class.getName());

    public LocalRobotsTextFileServiceProxy()
    {
    }

    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        return getRobotsTextFileService().getRobotsTextFile(guid);
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        return getRobotsTextFileService().getRobotsTextFile(guid, field);       
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        return getRobotsTextFileService().getRobotsTextFiles(guids);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().getAllRobotsTextFiles(ordering, offset, count);
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextFileService().getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().getAllRobotsTextFileKeys(ordering, offset, count);
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextFileService().getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextFileService().findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getRobotsTextFileService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getRobotsTextFileService().createRobotsTextFile(siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        return getRobotsTextFileService().createRobotsTextFile(robotsTextFile);
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getRobotsTextFileService().updateRobotsTextFile(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        return getRobotsTextFileService().updateRobotsTextFile(robotsTextFile);
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        return getRobotsTextFileService().deleteRobotsTextFile(guid);
    }

    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        return getRobotsTextFileService().deleteRobotsTextFile(robotsTextFile);
    }

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        return getRobotsTextFileService().deleteRobotsTextFiles(filter, params, values);
    }

}
