package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.service.FetchRequestService;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;

public class LocalFetchRequestServiceProxy extends BaseLocalServiceProxy implements FetchRequestServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalFetchRequestServiceProxy.class.getName());

    public LocalFetchRequestServiceProxy()
    {
    }

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        return getFetchRequestService().getFetchRequest(guid);
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return getFetchRequestService().getFetchRequest(guid, field);       
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        return getFetchRequestService().getFetchRequests(guids);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().getAllFetchRequests(ordering, offset, count);
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().getAllFetchRequests(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().getAllFetchRequestKeys(ordering, offset, count);
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getFetchRequestService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getFetchRequestService().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getFetchRequestService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        return getFetchRequestService().createFetchRequest(user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return getFetchRequestService().createFetchRequest(fetchRequest);
    }

    @Override
    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        return getFetchRequestService().updateFetchRequest(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref);
    }

    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return getFetchRequestService().updateFetchRequest(fetchRequest);
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequest(guid);
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequest(fetchRequest);
    }

    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        return getFetchRequestService().deleteFetchRequests(filter, params, values);
    }

}
