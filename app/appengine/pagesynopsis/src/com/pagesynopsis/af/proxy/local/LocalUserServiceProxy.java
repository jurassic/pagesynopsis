package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.ws.service.UserService;
import com.pagesynopsis.af.proxy.UserServiceProxy;

public class LocalUserServiceProxy extends BaseLocalServiceProxy implements UserServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalUserServiceProxy.class.getName());

    public LocalUserServiceProxy()
    {
    }

    @Override
    public User getUser(String guid) throws BaseException
    {
        return getUserService().getUser(guid);
    }

    @Override
    public Object getUser(String guid, String field) throws BaseException
    {
        return getUserService().getUser(guid, field);       
    }

    @Override
    public List<User> getUsers(List<String> guids) throws BaseException
    {
        return getUserService().getUsers(guids);
    }

    @Override
    public List<User> getAllUsers() throws BaseException
    {
        return getAllUsers(null, null, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserService().getAllUsers(ordering, offset, count);
        return getAllUsers(ordering, offset, count, null);
    }

    @Override
    public List<User> getAllUsers(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().getAllUsers(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getUserService().getAllUserKeys(ordering, offset, count);
        return getAllUserKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllUserKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().getAllUserKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findUsers(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count);
        return findUsers(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<User> findUsers(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().findUsers(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findUserKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getUserService().findUserKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getUserService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createUser(String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().createUser(managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public String createUser(User user) throws BaseException
    {
        return getUserService().createUser(user);
    }

    @Override
    public Boolean updateUser(String guid, String managerApp, Long appAcl, GaeAppStruct gaeApp, String aeryId, String sessionId, String ancestorGuid, FullNameStruct name, String usercode, String username, String nickname, String avatar, String email, String openId, GaeUserStruct gaeUser, String entityType, Boolean surrogate, Boolean obsolete, String timeZone, String location, StreetAddressStruct streetAddress, GeoPointStruct geoPoint, String ipAddress, String referer, String status, Long emailVerifiedTime, Long openIdVerifiedTime, Long authenticatedTime) throws BaseException
    {
        return getUserService().updateUser(guid, managerApp, appAcl, gaeApp, aeryId, sessionId, ancestorGuid, name, usercode, username, nickname, avatar, email, openId, gaeUser, entityType, surrogate, obsolete, timeZone, location, streetAddress, geoPoint, ipAddress, referer, status, emailVerifiedTime, openIdVerifiedTime, authenticatedTime);
    }

    @Override
    public Boolean updateUser(User user) throws BaseException
    {
        return getUserService().updateUser(user);
    }

    @Override
    public Boolean deleteUser(String guid) throws BaseException
    {
        return getUserService().deleteUser(guid);
    }

    @Override
    public Boolean deleteUser(User user) throws BaseException
    {
        return getUserService().deleteUser(user);
    }

    @Override
    public Long deleteUsers(String filter, String params, List<String> values) throws BaseException
    {
        return getUserService().deleteUsers(filter, params, values);
    }

}
