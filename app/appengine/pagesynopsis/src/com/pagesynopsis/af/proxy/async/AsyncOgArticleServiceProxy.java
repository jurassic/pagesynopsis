package com.pagesynopsis.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;
import com.pagesynopsis.af.proxy.remote.RemoteOgArticleServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncOgArticleServiceProxy extends BaseAsyncServiceProxy implements OgArticleServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncOgArticleServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteOgArticleServiceProxy remoteProxy;

    public AsyncOgArticleServiceProxy()
    {
        remoteProxy = new RemoteOgArticleServiceProxy();
    }

    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        return remoteProxy.getOgArticle(guid);
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        return remoteProxy.getOgArticle(guid, field);       
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        return remoteProxy.getOgArticles(guids);
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllOgArticles(ordering, offset, count);
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllOgArticles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllOgArticleKeys(ordering, offset, count);
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findOgArticles(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        OgArticleBean bean = new OgArticleBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        return createOgArticle(bean);        
    }

    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        String guid = ogArticle.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((OgArticleBean) ogArticle).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateOgArticle-" + guid;
        String taskName = "RsCreateOgArticle-" + guid + "-" + (new Date()).getTime();
        OgArticleStub stub = MarshalHelper.convertOgArticleToStub(ogArticle);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(OgArticleStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = ogArticle.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    OgArticleStub dummyStub = new OgArticleStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createOgArticle(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogArticles/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createOgArticle(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogArticles/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgArticle guid is invalid.");
        	throw new BaseException("OgArticle guid is invalid.");
        }
        OgArticleBean bean = new OgArticleBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        return updateOgArticle(bean);        
    }

    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        String guid = ogArticle.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgArticle object is invalid.");
        	throw new BaseException("OgArticle object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateOgArticle-" + guid;
        String taskName = "RsUpdateOgArticle-" + guid + "-" + (new Date()).getTime();
        OgArticleStub stub = MarshalHelper.convertOgArticleToStub(ogArticle);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(OgArticleStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = ogArticle.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    OgArticleStub dummyStub = new OgArticleStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateOgArticle(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogArticles/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateOgArticle(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogArticles/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteOgArticle-" + guid;
        String taskName = "RsDeleteOgArticle-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogArticles/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        String guid = ogArticle.getGuid();
        return deleteOgArticle(guid);
    }

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteOgArticles(filter, params, values);
    }

}
