package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;

public class LocalOgProfileServiceProxy extends BaseLocalServiceProxy implements OgProfileServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgProfileServiceProxy.class.getName());

    public LocalOgProfileServiceProxy()
    {
    }

    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        return getOgProfileService().getOgProfile(guid);
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        return getOgProfileService().getOgProfile(guid, field);       
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        return getOgProfileService().getOgProfiles(guids);
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().getAllOgProfiles(ordering, offset, count);
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgProfileService().getAllOgProfiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().getAllOgProfileKeys(ordering, offset, count);
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgProfileService().getAllOgProfileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgProfileService().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgProfileService().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgProfileService().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgProfileService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return getOgProfileService().createOgProfile(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        return getOgProfileService().createOgProfile(ogProfile);
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        return getOgProfileService().updateOgProfile(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
    }

    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        return getOgProfileService().updateOgProfile(ogProfile);
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        return getOgProfileService().deleteOgProfile(guid);
    }

    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        return getOgProfileService().deleteOgProfile(ogProfile);
    }

    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        return getOgProfileService().deleteOgProfiles(filter, params, values);
    }

}
