package com.pagesynopsis.af.proxy.local;

import com.pagesynopsis.ws.service.ApiConsumerService;
import com.pagesynopsis.ws.service.UserService;
import com.pagesynopsis.ws.service.RobotsTextFileService;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.FetchRequestService;
import com.pagesynopsis.ws.service.PageInfoService;
import com.pagesynopsis.ws.service.PageFetchService;
import com.pagesynopsis.ws.service.LinkListService;
import com.pagesynopsis.ws.service.ImageSetService;
import com.pagesynopsis.ws.service.AudioSetService;
import com.pagesynopsis.ws.service.VideoSetService;
import com.pagesynopsis.ws.service.OpenGraphMetaService;
import com.pagesynopsis.ws.service.TwitterCardMetaService;
import com.pagesynopsis.ws.service.DomainInfoService;
import com.pagesynopsis.ws.service.UrlRatingService;
import com.pagesynopsis.ws.service.ServiceInfoService;
import com.pagesynopsis.ws.service.FiveTenService;

// TBD: How to best inject the service instances?
public abstract class BaseLocalServiceProxy
{
    private ApiConsumerService apiConsumerService;
    private UserService userService;
    private RobotsTextFileService robotsTextFileService;
    private RobotsTextRefreshService robotsTextRefreshService;
    private OgProfileService ogProfileService;
    private OgWebsiteService ogWebsiteService;
    private OgBlogService ogBlogService;
    private OgArticleService ogArticleService;
    private OgBookService ogBookService;
    private OgVideoService ogVideoService;
    private OgMovieService ogMovieService;
    private OgTvShowService ogTvShowService;
    private OgTvEpisodeService ogTvEpisodeService;
    private TwitterSummaryCardService twitterSummaryCardService;
    private TwitterPhotoCardService twitterPhotoCardService;
    private TwitterGalleryCardService twitterGalleryCardService;
    private TwitterAppCardService twitterAppCardService;
    private TwitterPlayerCardService twitterPlayerCardService;
    private TwitterProductCardService twitterProductCardService;
    private FetchRequestService fetchRequestService;
    private PageInfoService pageInfoService;
    private PageFetchService pageFetchService;
    private LinkListService linkListService;
    private ImageSetService imageSetService;
    private AudioSetService audioSetService;
    private VideoSetService videoSetService;
    private OpenGraphMetaService openGraphMetaService;
    private TwitterCardMetaService twitterCardMetaService;
    private DomainInfoService domainInfoService;
    private UrlRatingService urlRatingService;
    private ServiceInfoService serviceInfoService;
    private FiveTenService fiveTenService;

    public BaseLocalServiceProxy()
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService)
    {
        this(apiConsumerService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(UserService userService)
    {
        this(null, userService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(RobotsTextFileService robotsTextFileService)
    {
        this(null, null, robotsTextFileService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(RobotsTextRefreshService robotsTextRefreshService)
    {
        this(null, null, null, robotsTextRefreshService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgProfileService ogProfileService)
    {
        this(null, null, null, null, ogProfileService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgWebsiteService ogWebsiteService)
    {
        this(null, null, null, null, null, ogWebsiteService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgBlogService ogBlogService)
    {
        this(null, null, null, null, null, null, ogBlogService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgArticleService ogArticleService)
    {
        this(null, null, null, null, null, null, null, ogArticleService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgBookService ogBookService)
    {
        this(null, null, null, null, null, null, null, null, ogBookService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgVideoService ogVideoService)
    {
        this(null, null, null, null, null, null, null, null, null, ogVideoService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgMovieService ogMovieService)
    {
        this(null, null, null, null, null, null, null, null, null, null, ogMovieService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgTvShowService ogTvShowService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, ogTvShowService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OgTvEpisodeService ogTvEpisodeService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, ogTvEpisodeService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterSummaryCardService twitterSummaryCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, twitterSummaryCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterPhotoCardService twitterPhotoCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterPhotoCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterGalleryCardService twitterGalleryCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterGalleryCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterAppCardService twitterAppCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterAppCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterPlayerCardService twitterPlayerCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterPlayerCardService, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterProductCardService twitterProductCardService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterProductCardService, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(FetchRequestService fetchRequestService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, fetchRequestService, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(PageInfoService pageInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, pageInfoService, null, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(PageFetchService pageFetchService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, pageFetchService, null, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(LinkListService linkListService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, linkListService, null, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(ImageSetService imageSetService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, imageSetService, null, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(AudioSetService audioSetService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, audioSetService, null, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(VideoSetService videoSetService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, videoSetService, null, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(OpenGraphMetaService openGraphMetaService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, openGraphMetaService, null, null, null, null, null);
    }
    public BaseLocalServiceProxy(TwitterCardMetaService twitterCardMetaService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, twitterCardMetaService, null, null, null, null);
    }
    public BaseLocalServiceProxy(DomainInfoService domainInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, domainInfoService, null, null, null);
    }
    public BaseLocalServiceProxy(UrlRatingService urlRatingService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, urlRatingService, null, null);
    }
    public BaseLocalServiceProxy(ServiceInfoService serviceInfoService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, serviceInfoService, null);
    }
    public BaseLocalServiceProxy(FiveTenService fiveTenService)
    {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, fiveTenService);
    }
    public BaseLocalServiceProxy(ApiConsumerService apiConsumerService, UserService userService, RobotsTextFileService robotsTextFileService, RobotsTextRefreshService robotsTextRefreshService, OgProfileService ogProfileService, OgWebsiteService ogWebsiteService, OgBlogService ogBlogService, OgArticleService ogArticleService, OgBookService ogBookService, OgVideoService ogVideoService, OgMovieService ogMovieService, OgTvShowService ogTvShowService, OgTvEpisodeService ogTvEpisodeService, TwitterSummaryCardService twitterSummaryCardService, TwitterPhotoCardService twitterPhotoCardService, TwitterGalleryCardService twitterGalleryCardService, TwitterAppCardService twitterAppCardService, TwitterPlayerCardService twitterPlayerCardService, TwitterProductCardService twitterProductCardService, FetchRequestService fetchRequestService, PageInfoService pageInfoService, PageFetchService pageFetchService, LinkListService linkListService, ImageSetService imageSetService, AudioSetService audioSetService, VideoSetService videoSetService, OpenGraphMetaService openGraphMetaService, TwitterCardMetaService twitterCardMetaService, DomainInfoService domainInfoService, UrlRatingService urlRatingService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.robotsTextFileService = robotsTextFileService;
        this.robotsTextRefreshService = robotsTextRefreshService;
        this.ogProfileService = ogProfileService;
        this.ogWebsiteService = ogWebsiteService;
        this.ogBlogService = ogBlogService;
        this.ogArticleService = ogArticleService;
        this.ogBookService = ogBookService;
        this.ogVideoService = ogVideoService;
        this.ogMovieService = ogMovieService;
        this.ogTvShowService = ogTvShowService;
        this.ogTvEpisodeService = ogTvEpisodeService;
        this.twitterSummaryCardService = twitterSummaryCardService;
        this.twitterPhotoCardService = twitterPhotoCardService;
        this.twitterGalleryCardService = twitterGalleryCardService;
        this.twitterAppCardService = twitterAppCardService;
        this.twitterPlayerCardService = twitterPlayerCardService;
        this.twitterProductCardService = twitterProductCardService;
        this.fetchRequestService = fetchRequestService;
        this.pageInfoService = pageInfoService;
        this.pageFetchService = pageFetchService;
        this.linkListService = linkListService;
        this.imageSetService = imageSetService;
        this.audioSetService = audioSetService;
        this.videoSetService = videoSetService;
        this.openGraphMetaService = openGraphMetaService;
        this.twitterCardMetaService = twitterCardMetaService;
        this.domainInfoService = domainInfoService;
        this.urlRatingService = urlRatingService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }
    
    // Inject dependencies.
    public void setApiConsumerService(ApiConsumerService apiConsumerService)
    {
        this.apiConsumerService = apiConsumerService;
    }
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }
    public void setRobotsTextFileService(RobotsTextFileService robotsTextFileService)
    {
        this.robotsTextFileService = robotsTextFileService;
    }
    public void setRobotsTextRefreshService(RobotsTextRefreshService robotsTextRefreshService)
    {
        this.robotsTextRefreshService = robotsTextRefreshService;
    }
    public void setOgProfileService(OgProfileService ogProfileService)
    {
        this.ogProfileService = ogProfileService;
    }
    public void setOgWebsiteService(OgWebsiteService ogWebsiteService)
    {
        this.ogWebsiteService = ogWebsiteService;
    }
    public void setOgBlogService(OgBlogService ogBlogService)
    {
        this.ogBlogService = ogBlogService;
    }
    public void setOgArticleService(OgArticleService ogArticleService)
    {
        this.ogArticleService = ogArticleService;
    }
    public void setOgBookService(OgBookService ogBookService)
    {
        this.ogBookService = ogBookService;
    }
    public void setOgVideoService(OgVideoService ogVideoService)
    {
        this.ogVideoService = ogVideoService;
    }
    public void setOgMovieService(OgMovieService ogMovieService)
    {
        this.ogMovieService = ogMovieService;
    }
    public void setOgTvShowService(OgTvShowService ogTvShowService)
    {
        this.ogTvShowService = ogTvShowService;
    }
    public void setOgTvEpisodeService(OgTvEpisodeService ogTvEpisodeService)
    {
        this.ogTvEpisodeService = ogTvEpisodeService;
    }
    public void setTwitterSummaryCardService(TwitterSummaryCardService twitterSummaryCardService)
    {
        this.twitterSummaryCardService = twitterSummaryCardService;
    }
    public void setTwitterPhotoCardService(TwitterPhotoCardService twitterPhotoCardService)
    {
        this.twitterPhotoCardService = twitterPhotoCardService;
    }
    public void setTwitterGalleryCardService(TwitterGalleryCardService twitterGalleryCardService)
    {
        this.twitterGalleryCardService = twitterGalleryCardService;
    }
    public void setTwitterAppCardService(TwitterAppCardService twitterAppCardService)
    {
        this.twitterAppCardService = twitterAppCardService;
    }
    public void setTwitterPlayerCardService(TwitterPlayerCardService twitterPlayerCardService)
    {
        this.twitterPlayerCardService = twitterPlayerCardService;
    }
    public void setTwitterProductCardService(TwitterProductCardService twitterProductCardService)
    {
        this.twitterProductCardService = twitterProductCardService;
    }
    public void setFetchRequestService(FetchRequestService fetchRequestService)
    {
        this.fetchRequestService = fetchRequestService;
    }
    public void setPageInfoService(PageInfoService pageInfoService)
    {
        this.pageInfoService = pageInfoService;
    }
    public void setPageFetchService(PageFetchService pageFetchService)
    {
        this.pageFetchService = pageFetchService;
    }
    public void setLinkListService(LinkListService linkListService)
    {
        this.linkListService = linkListService;
    }
    public void setImageSetService(ImageSetService imageSetService)
    {
        this.imageSetService = imageSetService;
    }
    public void setAudioSetService(AudioSetService audioSetService)
    {
        this.audioSetService = audioSetService;
    }
    public void setVideoSetService(VideoSetService videoSetService)
    {
        this.videoSetService = videoSetService;
    }
    public void setOpenGraphMetaService(OpenGraphMetaService openGraphMetaService)
    {
        this.openGraphMetaService = openGraphMetaService;
    }
    public void setTwitterCardMetaService(TwitterCardMetaService twitterCardMetaService)
    {
        this.twitterCardMetaService = twitterCardMetaService;
    }
    public void setDomainInfoService(DomainInfoService domainInfoService)
    {
        this.domainInfoService = domainInfoService;
    }
    public void setUrlRatingService(UrlRatingService urlRatingService)
    {
        this.urlRatingService = urlRatingService;
    }
    public void setServiceInfoService(ServiceInfoService serviceInfoService)
    {
        this.serviceInfoService = serviceInfoService;
    }
    public void setFiveTenService(FiveTenService fiveTenService)
    {
        this.fiveTenService = fiveTenService;
    }
   
    // Returns a ApiConsumerService instance.
    public ApiConsumerService getApiConsumerService() 
    {
        return apiConsumerService;
    }

    // Returns a UserService instance.
    public UserService getUserService() 
    {
        return userService;
    }

    // Returns a RobotsTextFileService instance.
    public RobotsTextFileService getRobotsTextFileService() 
    {
        return robotsTextFileService;
    }

    // Returns a RobotsTextRefreshService instance.
    public RobotsTextRefreshService getRobotsTextRefreshService() 
    {
        return robotsTextRefreshService;
    }

    // Returns a OgProfileService instance.
    public OgProfileService getOgProfileService() 
    {
        return ogProfileService;
    }

    // Returns a OgWebsiteService instance.
    public OgWebsiteService getOgWebsiteService() 
    {
        return ogWebsiteService;
    }

    // Returns a OgBlogService instance.
    public OgBlogService getOgBlogService() 
    {
        return ogBlogService;
    }

    // Returns a OgArticleService instance.
    public OgArticleService getOgArticleService() 
    {
        return ogArticleService;
    }

    // Returns a OgBookService instance.
    public OgBookService getOgBookService() 
    {
        return ogBookService;
    }

    // Returns a OgVideoService instance.
    public OgVideoService getOgVideoService() 
    {
        return ogVideoService;
    }

    // Returns a OgMovieService instance.
    public OgMovieService getOgMovieService() 
    {
        return ogMovieService;
    }

    // Returns a OgTvShowService instance.
    public OgTvShowService getOgTvShowService() 
    {
        return ogTvShowService;
    }

    // Returns a OgTvEpisodeService instance.
    public OgTvEpisodeService getOgTvEpisodeService() 
    {
        return ogTvEpisodeService;
    }

    // Returns a TwitterSummaryCardService instance.
    public TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        return twitterSummaryCardService;
    }

    // Returns a TwitterPhotoCardService instance.
    public TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        return twitterPhotoCardService;
    }

    // Returns a TwitterGalleryCardService instance.
    public TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        return twitterGalleryCardService;
    }

    // Returns a TwitterAppCardService instance.
    public TwitterAppCardService getTwitterAppCardService() 
    {
        return twitterAppCardService;
    }

    // Returns a TwitterPlayerCardService instance.
    public TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        return twitterPlayerCardService;
    }

    // Returns a TwitterProductCardService instance.
    public TwitterProductCardService getTwitterProductCardService() 
    {
        return twitterProductCardService;
    }

    // Returns a FetchRequestService instance.
    public FetchRequestService getFetchRequestService() 
    {
        return fetchRequestService;
    }

    // Returns a PageInfoService instance.
    public PageInfoService getPageInfoService() 
    {
        return pageInfoService;
    }

    // Returns a PageFetchService instance.
    public PageFetchService getPageFetchService() 
    {
        return pageFetchService;
    }

    // Returns a LinkListService instance.
    public LinkListService getLinkListService() 
    {
        return linkListService;
    }

    // Returns a ImageSetService instance.
    public ImageSetService getImageSetService() 
    {
        return imageSetService;
    }

    // Returns a AudioSetService instance.
    public AudioSetService getAudioSetService() 
    {
        return audioSetService;
    }

    // Returns a VideoSetService instance.
    public VideoSetService getVideoSetService() 
    {
        return videoSetService;
    }

    // Returns a OpenGraphMetaService instance.
    public OpenGraphMetaService getOpenGraphMetaService() 
    {
        return openGraphMetaService;
    }

    // Returns a TwitterCardMetaService instance.
    public TwitterCardMetaService getTwitterCardMetaService() 
    {
        return twitterCardMetaService;
    }

    // Returns a DomainInfoService instance.
    public DomainInfoService getDomainInfoService() 
    {
        return domainInfoService;
    }

    // Returns a UrlRatingService instance.
    public UrlRatingService getUrlRatingService() 
    {
        return urlRatingService;
    }

    // Returns a ServiceInfoService instance.
    public ServiceInfoService getServiceInfoService() 
    {
        return serviceInfoService;
    }

    // Returns a FiveTenService instance.
    public FiveTenService getFiveTenService() 
    {
        return fiveTenService;
    }

}
