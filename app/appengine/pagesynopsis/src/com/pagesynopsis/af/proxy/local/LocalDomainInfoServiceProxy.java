package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.service.DomainInfoService;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;

public class LocalDomainInfoServiceProxy extends BaseLocalServiceProxy implements DomainInfoServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalDomainInfoServiceProxy.class.getName());

    public LocalDomainInfoServiceProxy()
    {
    }

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        return getDomainInfoService().getDomainInfo(guid);
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        return getDomainInfoService().getDomainInfo(guid, field);       
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        return getDomainInfoService().getDomainInfos(guids);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().getAllDomainInfos(ordering, offset, count);
        return getAllDomainInfos(ordering, offset, count, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDomainInfoService().getAllDomainInfos(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().getAllDomainInfoKeys(ordering, offset, count);
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDomainInfoService().getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count);
        return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDomainInfoService().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getDomainInfoService().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getDomainInfoService().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getDomainInfoService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createDomainInfo(String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getDomainInfoService().createDomainInfo(domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return getDomainInfoService().createDomainInfo(domainInfo);
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        return getDomainInfoService().updateDomainInfo(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
    }

    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return getDomainInfoService().updateDomainInfo(domainInfo);
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfo(guid);
    }

    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfo(domainInfo);
    }

    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        return getDomainInfoService().deleteDomainInfos(filter, params, values);
    }

}
