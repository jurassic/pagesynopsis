package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.service.RobotsTextRefreshService;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;

public class LocalRobotsTextRefreshServiceProxy extends BaseLocalServiceProxy implements RobotsTextRefreshServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalRobotsTextRefreshServiceProxy.class.getName());

    public LocalRobotsTextRefreshServiceProxy()
    {
    }

    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        return getRobotsTextRefreshService().getRobotsTextRefresh(guid);
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        return getRobotsTextRefreshService().getRobotsTextRefresh(guid, field);       
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        return getRobotsTextRefreshService().getRobotsTextRefreshes(guids);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return getAllRobotsTextRefreshes(null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().getAllRobotsTextRefreshes(ordering, offset, count);
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextRefreshService().getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().getAllRobotsTextRefreshKeys(ordering, offset, count);
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextRefreshService().getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getRobotsTextRefreshService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return getRobotsTextRefreshService().createRobotsTextRefresh(robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return getRobotsTextRefreshService().createRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException
    {
        return getRobotsTextRefreshService().updateRobotsTextRefresh(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime);
    }

    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return getRobotsTextRefreshService().updateRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return getRobotsTextRefreshService().deleteRobotsTextRefresh(guid);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return getRobotsTextRefreshService().deleteRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException
    {
        return getRobotsTextRefreshService().deleteRobotsTextRefreshes(filter, params, values);
    }

}
