package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.PageFetchService;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;

public class LocalPageFetchServiceProxy extends BaseLocalServiceProxy implements PageFetchServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalPageFetchServiceProxy.class.getName());

    public LocalPageFetchServiceProxy()
    {
    }

    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        return getPageFetchService().getPageFetch(guid);
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        return getPageFetchService().getPageFetch(guid, field);       
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        return getPageFetchService().getPageFetches(guids);
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return getAllPageFetches(null, null, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().getAllPageFetches(ordering, offset, count);
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageFetchService().getAllPageFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().getAllPageFetchKeys(ordering, offset, count);
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageFetchService().getAllPageFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageFetchService().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getPageFetchService().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getPageFetchService().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getPageFetchService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return getPageFetchService().createPageFetch(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        return getPageFetchService().createPageFetch(pageFetch);
    }

    @Override
    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        return getPageFetchService().updatePageFetch(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
    }

    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        return getPageFetchService().updatePageFetch(pageFetch);
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        return getPageFetchService().deletePageFetch(guid);
    }

    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        return getPageFetchService().deletePageFetch(pageFetch);
    }

    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        return getPageFetchService().deletePageFetches(filter, params, values);
    }

}
