package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;

public class LocalOgTvEpisodeServiceProxy extends BaseLocalServiceProxy implements OgTvEpisodeServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgTvEpisodeServiceProxy.class.getName());

    public LocalOgTvEpisodeServiceProxy()
    {
    }

    @Override
    public OgTvEpisode getOgTvEpisode(String guid) throws BaseException
    {
        return getOgTvEpisodeService().getOgTvEpisode(guid);
    }

    @Override
    public Object getOgTvEpisode(String guid, String field) throws BaseException
    {
        return getOgTvEpisodeService().getOgTvEpisode(guid, field);       
    }

    @Override
    public List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        return getOgTvEpisodeService().getOgTvEpisodes(guids);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException
    {
        return getAllOgTvEpisodes(null, null, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().getAllOgTvEpisodes(ordering, offset, count);
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvEpisodeService().getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().getAllOgTvEpisodeKeys(ordering, offset, count);
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvEpisodeService().getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvEpisodeService().findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgTvEpisodeService().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgTvEpisodeService().findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgTvEpisodeService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgTvEpisodeService().createOgTvEpisode(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }

    @Override
    public String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return getOgTvEpisodeService().createOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        return getOgTvEpisodeService().updateOgTvEpisode(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
    }

    @Override
    public Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return getOgTvEpisodeService().updateOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        return getOgTvEpisodeService().deleteOgTvEpisode(guid);
    }

    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return getOgTvEpisodeService().deleteOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        return getOgTvEpisodeService().deleteOgTvEpisodes(filter, params, values);
    }

}
