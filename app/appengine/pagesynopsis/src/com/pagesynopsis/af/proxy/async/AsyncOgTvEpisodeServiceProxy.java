package com.pagesynopsis.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.af.proxy.remote.RemoteOgTvEpisodeServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncOgTvEpisodeServiceProxy extends BaseAsyncServiceProxy implements OgTvEpisodeServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncOgTvEpisodeServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteOgTvEpisodeServiceProxy remoteProxy;

    public AsyncOgTvEpisodeServiceProxy()
    {
        remoteProxy = new RemoteOgTvEpisodeServiceProxy();
    }

    @Override
    public OgTvEpisode getOgTvEpisode(String guid) throws BaseException
    {
        return remoteProxy.getOgTvEpisode(guid);
    }

    @Override
    public Object getOgTvEpisode(String guid, String field) throws BaseException
    {
        return remoteProxy.getOgTvEpisode(guid, field);       
    }

    @Override
    public List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        return remoteProxy.getOgTvEpisodes(guids);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException
    {
        return getAllOgTvEpisodes(null, null, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllOgTvEpisodes(ordering, offset, count);
        return getAllOgTvEpisodes(ordering, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllOgTvEpisodes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllOgTvEpisodeKeys(ordering, offset, count);
        return getAllOgTvEpisodeKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllOgTvEpisodeKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgTvEpisodes(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgTvEpisode(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        OgTvEpisodeBean bean = new OgTvEpisodeBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        return createOgTvEpisode(bean);        
    }

    @Override
    public String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");

        String guid = ogTvEpisode.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((OgTvEpisodeBean) ogTvEpisode).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateOgTvEpisode-" + guid;
        String taskName = "RsCreateOgTvEpisode-" + guid + "-" + (new Date()).getTime();
        OgTvEpisodeStub stub = MarshalHelper.convertOgTvEpisodeToStub(ogTvEpisode);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(OgTvEpisodeStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = ogTvEpisode.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    OgTvEpisodeStub dummyStub = new OgTvEpisodeStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createOgTvEpisode(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogTvEpisodes/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createOgTvEpisode(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogTvEpisodes/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateOgTvEpisode(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, String series, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgTvEpisode guid is invalid.");
        	throw new BaseException("OgTvEpisode guid is invalid.");
        }
        OgTvEpisodeBean bean = new OgTvEpisodeBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, series, duration, tag, releaseDate);
        return updateOgTvEpisode(bean);        
    }

    @Override
    public Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        log.finer("BEGIN");

        String guid = ogTvEpisode.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgTvEpisode object is invalid.");
        	throw new BaseException("OgTvEpisode object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateOgTvEpisode-" + guid;
        String taskName = "RsUpdateOgTvEpisode-" + guid + "-" + (new Date()).getTime();
        OgTvEpisodeStub stub = MarshalHelper.convertOgTvEpisodeToStub(ogTvEpisode);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(OgTvEpisodeStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = ogTvEpisode.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    OgTvEpisodeStub dummyStub = new OgTvEpisodeStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateOgTvEpisode(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogTvEpisodes/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateOgTvEpisode(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogTvEpisodes/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteOgTvEpisode-" + guid;
        String taskName = "RsDeleteOgTvEpisode-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "ogTvEpisodes/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        String guid = ogTvEpisode.getGuid();
        return deleteOgTvEpisode(guid);
    }

    @Override
    public Long deleteOgTvEpisodes(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteOgTvEpisodes(filter, params, values);
    }

}
