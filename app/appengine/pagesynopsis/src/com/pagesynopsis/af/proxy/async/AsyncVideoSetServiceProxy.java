package com.pagesynopsis.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.OgVideoListStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.OgBlogListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.UrlStructListStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.ImageStructListStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.AnchorStructListStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.AudioStructListStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.VideoStructListStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.stub.OgProfileListStub;
import com.pagesynopsis.ws.stub.VideoSetStub;
import com.pagesynopsis.ws.stub.VideoSetListStub;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.VideoSetBean;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.VideoSetService;
import com.pagesynopsis.af.proxy.VideoSetServiceProxy;
import com.pagesynopsis.af.proxy.remote.RemoteVideoSetServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncVideoSetServiceProxy extends BaseAsyncServiceProxy implements VideoSetServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncVideoSetServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteVideoSetServiceProxy remoteProxy;

    public AsyncVideoSetServiceProxy()
    {
        remoteProxy = new RemoteVideoSetServiceProxy();
    }

    @Override
    public VideoSet getVideoSet(String guid) throws BaseException
    {
        return remoteProxy.getVideoSet(guid);
    }

    @Override
    public Object getVideoSet(String guid, String field) throws BaseException
    {
        return remoteProxy.getVideoSet(guid, field);       
    }

    @Override
    public List<VideoSet> getVideoSets(List<String> guids) throws BaseException
    {
        return remoteProxy.getVideoSets(guids);
    }

    @Override
    public List<VideoSet> getAllVideoSets() throws BaseException
    {
        return getAllVideoSets(null, null, null);
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllVideoSets(ordering, offset, count);
        return getAllVideoSets(ordering, offset, count, null);
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllVideoSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllVideoSetKeys(ordering, offset, count);
        return getAllVideoSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllVideoSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findVideoSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findVideoSets(filter, ordering, params, values, grouping, unique, offset, count);
        return findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createVideoSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws BaseException
    {
        VideoSetBean bean = new VideoSetBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos);
        return createVideoSet(bean);        
    }

    @Override
    public String createVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("BEGIN");

        String guid = videoSet.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((VideoSetBean) videoSet).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateVideoSet-" + guid;
        String taskName = "RsCreateVideoSet-" + guid + "-" + (new Date()).getTime();
        VideoSetStub stub = MarshalHelper.convertVideoSetToStub(videoSet);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(VideoSetStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = videoSet.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    VideoSetStub dummyStub = new VideoSetStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createVideoSet(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "videoSets/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createVideoSet(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "videoSets/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateVideoSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<VideoStruct> pageVideos) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VideoSet guid is invalid.");
        	throw new BaseException("VideoSet guid is invalid.");
        }
        VideoSetBean bean = new VideoSetBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageVideos);
        return updateVideoSet(bean);        
    }

    @Override
    public Boolean updateVideoSet(VideoSet videoSet) throws BaseException
    {
        log.finer("BEGIN");

        String guid = videoSet.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "VideoSet object is invalid.");
        	throw new BaseException("VideoSet object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateVideoSet-" + guid;
        String taskName = "RsUpdateVideoSet-" + guid + "-" + (new Date()).getTime();
        VideoSetStub stub = MarshalHelper.convertVideoSetToStub(videoSet);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(VideoSetStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = videoSet.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    VideoSetStub dummyStub = new VideoSetStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateVideoSet(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "videoSets/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateVideoSet(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "videoSets/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteVideoSet(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteVideoSet-" + guid;
        String taskName = "RsDeleteVideoSet-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "videoSets/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteVideoSet(VideoSet videoSet) throws BaseException
    {
        String guid = videoSet.getGuid();
        return deleteVideoSet(guid);
    }

    @Override
    public Long deleteVideoSets(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteVideoSets(filter, params, values);
    }

}
