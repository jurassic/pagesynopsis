package com.pagesynopsis.af.proxy.remote;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.ServiceUnavailableException;
import com.pagesynopsis.ws.exception.NotImplementedException;
import com.pagesynopsis.ws.core.StatusCode;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.KeyListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.OgVideoListStub;
import com.pagesynopsis.af.auth.TwoLeggedOAuthClientUtil;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.util.StringUtil;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;

import com.pagesynopsis.af.config.Config;


// Note on retries, upon arbitrary app engine urlfetch timeout (5 secs):
// In our current implementation, create (POST) and update (PUT) have essentially the same semantics.
// If an object with the client-supplied guid does not exist in the data store, it will be created.
// If an object with the given guid already exists, it will be replaced with the supplied object.
// The only difference is, the update operation (PUT) will modify the modifiedTime field.
// (Note: Ccreate does not require guid (pk) although we will generally try to supply guid from the client side.)
// Therefore, retries are relatively safe for POST and PUT.
// GET is by definition idempotent, and hence retry is safe.
// DELETE, on the other hand, will likely cause an error if the same request is repeated.
// The consequence of this is that, a user may be told there was an error deleting an object
// when in fact the object was already deleted during the retry sequence.
public class RemoteOgVideoServiceProxy extends BaseRemoteServiceProxy implements OgVideoServiceProxy
{
    private static final Logger log = Logger.getLogger(RemoteOgVideoServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_OGVIDEO = "ogVideos";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public RemoteOgVideoServiceProxy()
    {
        initCache();
    }


    protected WebResource getOgVideoWebResource()
    {
        return getWebResource(RESOURCE_OGVIDEO);
    }
    protected WebResource getOgVideoWebResource(String path)
    {
        return getWebResource(RESOURCE_OGVIDEO, path);
    }
    protected WebResource getOgVideoWebResourceByGuid(String guid)
    {
        return getOgVideoWebResource(guid);
    }


    @Override
    public OgVideo getOgVideo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        OgVideo bean = null;

        String key = getResourcePath(RESOURCE_OGVIDEO, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getOgVideoWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
 	            clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgVideoStub stub = clientResponse.getEntity(OgVideoStub.class);
                bean = MarshalHelper.convertOgVideoToBean(stub);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "OgVideo bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgVideo(String guid, String field) throws BaseException
    {
        OgVideo bean = getOgVideo(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("director")) {
            return bean.getDirector();
        } else if(field.equals("writer")) {
            return bean.getWriter();
        } else if(field.equals("actor")) {
            return bean.getActor();
        } else if(field.equals("duration")) {
            return bean.getDuration();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("releaseDate")) {
            return bean.getReleaseDate();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgVideo> getOgVideos(List<String> guids) throws BaseException
    {
        // Temporary implementation
        if(guids == null || guids.isEmpty()) {
            return null;
        }
        List<OgVideo> beans = new ArrayList<OgVideo>();
        for(String guid : guids) {
            OgVideo bean = getOgVideo(guid);
            beans.add(bean);
        }
        return beans;
    }

    @Override
    public List<OgVideo> getAllOgVideos() throws BaseException
    {
        return getAllOgVideos(null, null, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideos(ordering, offset, count, null);
    }

    @Override
    public List<OgVideo> getAllOgVideos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<OgVideo> list = null;

     	WebResource webResource = getOgVideoWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgVideoListStub stub = clientResponse.getEntity(OgVideoListStub.class);
                list = MarshalHelper.convertOgVideoListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "OgVideo list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgVideoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgVideoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

     	WebResource webResource = getOgVideoWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
    	
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "OgVideo key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgVideo> findOgVideos(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<OgVideo> list = null;
    	
//        ClientResponse clientResponse = getOgVideoWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getOgVideoWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                OgVideoListStub stub = clientResponse.getEntity(OgVideoListStub.class);
                list = MarshalHelper.convertOgVideoListStubToBeanList(stub, forwardCursor);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "OgVideo list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgVideoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgVideoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        log.finer("BEGIN");

    	List<String> list = null;

    	WebResource webResource = getOgVideoWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }
        if(forwardCursor != null && !forwardCursor.isEmpty()) {
            webResource = webResource.queryParam("cursor", forwardCursor.getWebSafeString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(forwardCursor != null) {
                    String webSafeString = stub.getForwardCursor();
                    forwardCursor.setWebSafeString(webSafeString);
                }
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "OgVideo key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END");
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getOgVideoWebResource(RESOURCE_PATH_COUNT);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime type ???
                //clientResponse = webResource.accept(MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML).get(ClientResponse.class);
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).get(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries.");
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries.");
        }
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "OgVideo count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);  // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgVideo(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        OgVideoBean bean = new OgVideoBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return createOgVideo(bean);        
    }

    @Override
    public String createOgVideo(OgVideo bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = null;
        OgVideoStub stub = MarshalHelper.convertOgVideoToStub(bean);
        WebResource webResource = getOgVideoWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).post(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "New OgVideo guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New OgVideo resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public Boolean updateOgVideo(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> director, List<String> writer, List<OgActorStruct> actor, Integer duration, List<String> tag, String releaseDate) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgVideo guid is invalid.");
        	throw new BaseException("OgVideo guid is invalid.");
        }
        OgVideoBean bean = new OgVideoBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, director, writer, actor, duration, tag, releaseDate);
        return updateOgVideo(bean);        
    }

    @Override
    public Boolean updateOgVideo(OgVideo bean) throws BaseException
    {
        log.finer("BEGIN");

        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgVideo object is invalid.");
        	throw new BaseException("OgVideo object is invalid.");
        }
        OgVideoStub stub = MarshalHelper.convertOgVideoToStub(bean);

        WebResource webResource = getOgVideoWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);
        
        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: bean = " + bean);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully updated the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteOgVideo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "OgVideo guid is invalid.");
        	throw new BaseException("OgVideo guid is invalid.");
        }

        WebResource webResource = getOgVideoWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                clientResponse = webResource.delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: guid = " + guid);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Successfully deleted the object with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        log.finer("END: false");
        return false;
    }

    @Override
    public Boolean deleteOgVideo(OgVideo bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteOgVideo(guid);
    }

    @Override
    public Long deleteOgVideos(String filter, String params, List<String> values) throws BaseException
    {
        log.finer("BEGIN");

        Long count = 0L;
     	WebResource webResource = getOgVideoWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exceptionif the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = null;
        int numTries = 0;
        while(clientResponse == null && numTries++ < MAX_RETRIES) {
            try {
                // Order of the mime types ???
                clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
            } catch(Exception ex) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Request failed (" + numTries + ").", ex);
            }
        }
        if(clientResponse == null) {
            if(log.isLoggable(Level.SEVERE)) log.log(Level.SEVERE, "Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
            throw new ServiceUnavailableException("Request failed after " + MAX_RETRIES + " tries: delete-filter = " + filter);
        }

        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINER)) log.log(Level.FINER, "Deleted OgVideos: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class); // ???
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_OGVIDEO);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

}
