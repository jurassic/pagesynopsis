package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.service.OgVideoService;
import com.pagesynopsis.ws.service.TwitterProductCardService;
import com.pagesynopsis.ws.service.TwitterSummaryCardService;
import com.pagesynopsis.ws.service.OgBlogService;
import com.pagesynopsis.ws.service.TwitterPlayerCardService;
import com.pagesynopsis.ws.service.TwitterGalleryCardService;
import com.pagesynopsis.ws.service.TwitterPhotoCardService;
import com.pagesynopsis.ws.service.OgTvShowService;
import com.pagesynopsis.ws.service.OgBookService;
import com.pagesynopsis.ws.service.OgWebsiteService;
import com.pagesynopsis.ws.service.OgMovieService;
import com.pagesynopsis.ws.service.TwitterAppCardService;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.ws.service.OgTvEpisodeService;
import com.pagesynopsis.ws.service.OgProfileService;
import com.pagesynopsis.ws.service.LinkListService;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;

public class LocalLinkListServiceProxy extends BaseLocalServiceProxy implements LinkListServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalLinkListServiceProxy.class.getName());

    public LocalLinkListServiceProxy()
    {
    }

    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        return getLinkListService().getLinkList(guid);
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        return getLinkListService().getLinkList(guid, field);       
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        return getLinkListService().getLinkLists(guids);
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().getAllLinkLists(ordering, offset, count);
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getLinkListService().getAllLinkLists(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().getAllLinkListKeys(ordering, offset, count);
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getLinkListService().getAllLinkListKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count);
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getLinkListService().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getLinkListService().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getLinkListService().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getLinkListService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return getLinkListService().createLinkList(user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        return getLinkListService().createLinkList(linkList);
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        return getLinkListService().updateLinkList(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
    }

    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        return getLinkListService().updateLinkList(linkList);
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        return getLinkListService().deleteLinkList(guid);
    }

    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        return getLinkListService().deleteLinkList(linkList);
    }

    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        return getLinkListService().deleteLinkLists(filter, params, values);
    }

}
