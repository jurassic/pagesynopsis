package com.pagesynopsis.af.proxy.local;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.service.OgArticleService;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;

public class LocalOgArticleServiceProxy extends BaseLocalServiceProxy implements OgArticleServiceProxy
{
    private static final Logger log = Logger.getLogger(LocalOgArticleServiceProxy.class.getName());

    public LocalOgArticleServiceProxy()
    {
    }

    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        return getOgArticleService().getOgArticle(guid);
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        return getOgArticleService().getOgArticle(guid, field);       
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        return getOgArticleService().getOgArticles(guids);
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().getAllOgArticles(ordering, offset, count);
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgArticleService().getAllOgArticles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().getAllOgArticleKeys(ordering, offset, count);
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgArticleService().getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgArticleService().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return getOgArticleService().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return getOgArticleService().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return getOgArticleService().getCount(filter, params, values, aggregate);
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        return getOgArticleService().createOgArticle(url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        return getOgArticleService().createOgArticle(ogArticle);
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        return getOgArticleService().updateOgArticle(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
    }

    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        return getOgArticleService().updateOgArticle(ogArticle);
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        return getOgArticleService().deleteOgArticle(guid);
    }

    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        return getOgArticleService().deleteOgArticle(ogArticle);
    }

    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        return getOgArticleService().deleteOgArticles(filter, params, values);
    }

}
