package com.pagesynopsis.af.proxy.local;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.ApiConsumerServiceProxy;
import com.pagesynopsis.af.proxy.UserServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.af.proxy.RobotsTextRefreshServiceProxy;
import com.pagesynopsis.af.proxy.OgProfileServiceProxy;
import com.pagesynopsis.af.proxy.OgWebsiteServiceProxy;
import com.pagesynopsis.af.proxy.OgBlogServiceProxy;
import com.pagesynopsis.af.proxy.OgArticleServiceProxy;
import com.pagesynopsis.af.proxy.OgBookServiceProxy;
import com.pagesynopsis.af.proxy.OgVideoServiceProxy;
import com.pagesynopsis.af.proxy.OgMovieServiceProxy;
import com.pagesynopsis.af.proxy.OgTvShowServiceProxy;
import com.pagesynopsis.af.proxy.OgTvEpisodeServiceProxy;
import com.pagesynopsis.af.proxy.TwitterSummaryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPhotoCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterGalleryCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterAppCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterPlayerCardServiceProxy;
import com.pagesynopsis.af.proxy.TwitterProductCardServiceProxy;
import com.pagesynopsis.af.proxy.FetchRequestServiceProxy;
import com.pagesynopsis.af.proxy.PageInfoServiceProxy;
import com.pagesynopsis.af.proxy.PageFetchServiceProxy;
import com.pagesynopsis.af.proxy.LinkListServiceProxy;
import com.pagesynopsis.af.proxy.ImageSetServiceProxy;
import com.pagesynopsis.af.proxy.AudioSetServiceProxy;
import com.pagesynopsis.af.proxy.VideoSetServiceProxy;
import com.pagesynopsis.af.proxy.OpenGraphMetaServiceProxy;
import com.pagesynopsis.af.proxy.TwitterCardMetaServiceProxy;
import com.pagesynopsis.af.proxy.DomainInfoServiceProxy;
import com.pagesynopsis.af.proxy.UrlRatingServiceProxy;
import com.pagesynopsis.af.proxy.ServiceInfoServiceProxy;
import com.pagesynopsis.af.proxy.FiveTenServiceProxy;

public class LocalProxyFactory extends AbstractProxyFactory
{
    private static final Logger log = Logger.getLogger(LocalProxyFactory.class.getName());

    private LocalProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class LocalProxyFactoryHolder
    {
        private static final LocalProxyFactory INSTANCE = new LocalProxyFactory();
    }

    // Singleton method
    public static LocalProxyFactory getInstance()
    {
        return LocalProxyFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerServiceProxy getApiConsumerServiceProxy()
    {
        return new LocalApiConsumerServiceProxy();
    }

    @Override
    public UserServiceProxy getUserServiceProxy()
    {
        return new LocalUserServiceProxy();
    }

    @Override
    public RobotsTextFileServiceProxy getRobotsTextFileServiceProxy()
    {
        return new LocalRobotsTextFileServiceProxy();
    }

    @Override
    public RobotsTextRefreshServiceProxy getRobotsTextRefreshServiceProxy()
    {
        return new LocalRobotsTextRefreshServiceProxy();
    }

    @Override
    public OgProfileServiceProxy getOgProfileServiceProxy()
    {
        return new LocalOgProfileServiceProxy();
    }

    @Override
    public OgWebsiteServiceProxy getOgWebsiteServiceProxy()
    {
        return new LocalOgWebsiteServiceProxy();
    }

    @Override
    public OgBlogServiceProxy getOgBlogServiceProxy()
    {
        return new LocalOgBlogServiceProxy();
    }

    @Override
    public OgArticleServiceProxy getOgArticleServiceProxy()
    {
        return new LocalOgArticleServiceProxy();
    }

    @Override
    public OgBookServiceProxy getOgBookServiceProxy()
    {
        return new LocalOgBookServiceProxy();
    }

    @Override
    public OgVideoServiceProxy getOgVideoServiceProxy()
    {
        return new LocalOgVideoServiceProxy();
    }

    @Override
    public OgMovieServiceProxy getOgMovieServiceProxy()
    {
        return new LocalOgMovieServiceProxy();
    }

    @Override
    public OgTvShowServiceProxy getOgTvShowServiceProxy()
    {
        return new LocalOgTvShowServiceProxy();
    }

    @Override
    public OgTvEpisodeServiceProxy getOgTvEpisodeServiceProxy()
    {
        return new LocalOgTvEpisodeServiceProxy();
    }

    @Override
    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        return new LocalTwitterSummaryCardServiceProxy();
    }

    @Override
    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        return new LocalTwitterPhotoCardServiceProxy();
    }

    @Override
    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        return new LocalTwitterGalleryCardServiceProxy();
    }

    @Override
    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        return new LocalTwitterAppCardServiceProxy();
    }

    @Override
    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        return new LocalTwitterPlayerCardServiceProxy();
    }

    @Override
    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        return new LocalTwitterProductCardServiceProxy();
    }

    @Override
    public FetchRequestServiceProxy getFetchRequestServiceProxy()
    {
        return new LocalFetchRequestServiceProxy();
    }

    @Override
    public PageInfoServiceProxy getPageInfoServiceProxy()
    {
        return new LocalPageInfoServiceProxy();
    }

    @Override
    public PageFetchServiceProxy getPageFetchServiceProxy()
    {
        return new LocalPageFetchServiceProxy();
    }

    @Override
    public LinkListServiceProxy getLinkListServiceProxy()
    {
        return new LocalLinkListServiceProxy();
    }

    @Override
    public ImageSetServiceProxy getImageSetServiceProxy()
    {
        return new LocalImageSetServiceProxy();
    }

    @Override
    public AudioSetServiceProxy getAudioSetServiceProxy()
    {
        return new LocalAudioSetServiceProxy();
    }

    @Override
    public VideoSetServiceProxy getVideoSetServiceProxy()
    {
        return new LocalVideoSetServiceProxy();
    }

    @Override
    public OpenGraphMetaServiceProxy getOpenGraphMetaServiceProxy()
    {
        return new LocalOpenGraphMetaServiceProxy();
    }

    @Override
    public TwitterCardMetaServiceProxy getTwitterCardMetaServiceProxy()
    {
        return new LocalTwitterCardMetaServiceProxy();
    }

    @Override
    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        return new LocalDomainInfoServiceProxy();
    }

    @Override
    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        return new LocalUrlRatingServiceProxy();
    }

    @Override
    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        return new LocalServiceInfoServiceProxy();
    }

    @Override
    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        return new LocalFiveTenServiceProxy();
    }

}
