package com.pagesynopsis.af.proxy.async;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.stub.ErrorStub;
import com.pagesynopsis.ws.stub.RobotsTextGroupStub;
import com.pagesynopsis.ws.stub.RobotsTextGroupListStub;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.pagesynopsis.ws.stub.RobotsTextFileListStub;
import com.pagesynopsis.af.util.MarshalHelper;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.ws.service.RobotsTextFileService;
import com.pagesynopsis.af.proxy.RobotsTextFileServiceProxy;
import com.pagesynopsis.af.proxy.remote.RemoteRobotsTextFileServiceProxy;


// Implements the asynchronous call logic (using GAE task queue).
// GET:             Directly calls the remoteProxy interface.
// POST/PUT/DELETE: Creates a task and puts in a queue.
public class AsyncRobotsTextFileServiceProxy extends BaseAsyncServiceProxy implements RobotsTextFileServiceProxy
{
    private static final Logger log = Logger.getLogger(AsyncRobotsTextFileServiceProxy.class.getName());

    // Actual implementation is delegated to the corresponding remote proxy class.
    private RemoteRobotsTextFileServiceProxy remoteProxy;

    public AsyncRobotsTextFileServiceProxy()
    {
        remoteProxy = new RemoteRobotsTextFileServiceProxy();
    }

    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        return remoteProxy.getRobotsTextFile(guid);
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        return remoteProxy.getRobotsTextFile(guid, field);       
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        return remoteProxy.getRobotsTextFiles(guids);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return getAllRobotsTextFiles(null, null, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllRobotsTextFiles(ordering, offset, count);
        return getAllRobotsTextFiles(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllRobotsTextFiles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.getAllRobotsTextFileKeys(ordering, offset, count);
        return getAllRobotsTextFileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.getAllRobotsTextFileKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findRobotsTextFiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        // return remoteProxy.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count);
        return findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return remoteProxy.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return remoteProxy.getCount(filter, params, values, aggregate);
    }

    @Override
    public String createRobotsTextFile(String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        RobotsTextFileBean bean = new RobotsTextFileBean(null, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        return createRobotsTextFile(bean);        
    }

    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        String guid = robotsTextFile.getGuid();
        if(guid == null) {  // Needs guid in order to be able to make an asynchronous call.
            guid = GUID.generate();
            ((RobotsTextFileBean) robotsTextFile).setGuid(guid);
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsCreateRobotsTextFile-" + guid;
        String taskName = "RsCreateRobotsTextFile-" + guid + "-" + (new Date()).getTime();
        RobotsTextFileStub stub = MarshalHelper.convertRobotsTextFileToStub(robotsTextFile);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(RobotsTextFileStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = robotsTextFile.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    RobotsTextFileStub dummyStub = new RobotsTextFileStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "createRobotsTextFile(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "robotsTextFiles/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "createRobotsTextFile(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "robotsTextFiles/").method(Method.POST).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;  // ???
    }

    @Override
    public Boolean updateRobotsTextFile(String guid, String siteUrl, List<RobotsTextGroup> groups, List<String> sitemaps, String content, String contentHash, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "RobotsTextFile guid is invalid.");
        	throw new BaseException("RobotsTextFile guid is invalid.");
        }
        RobotsTextFileBean bean = new RobotsTextFileBean(guid, siteUrl, groups, sitemaps, content, contentHash, lastCheckedTime, lastUpdatedTime);
        return updateRobotsTextFile(bean);        
    }

    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("BEGIN");

        String guid = robotsTextFile.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "RobotsTextFile object is invalid.");
        	throw new BaseException("RobotsTextFile object is invalid.");
        }
        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsUpdateRobotsTextFile-" + guid;
        String taskName = "RsUpdateRobotsTextFile-" + guid + "-" + (new Date()).getTime();
        RobotsTextFileStub stub = MarshalHelper.convertRobotsTextFileToStub(robotsTextFile);
        TaskOptions taskOpt = null;
        try {
            final JAXBContext jAXBContext = JAXBContext.newInstance(RobotsTextFileStub.class);
            Marshaller marshaller = null;
            StringWriter writer = null;

            // Note that the actual size will be bigger than this in xml or json format...
            int approxPayloadSize = robotsTextFile.toString().length() * 2;  // 2 bytes per char
            if(log.isLoggable(Level.FINE)) log.fine("approxPayloadSize = " + approxPayloadSize);
            if(isAlwaysUseDummyPayload() || approxPayloadSize > getMaxPayloadSize()) {  // temporary
                if(getCache() != null) {
                    getCache().put(taskName, stub);
                    
                    RobotsTextFileStub dummyStub = new RobotsTextFileStub();
                    dummyStub.setGuid(stub.getGuid());
                    marshaller = jAXBContext.createMarshaller();
                    //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                    writer = new StringWriter();
                    marshaller.marshal(dummyStub, writer);
                    String dummyPayload = writer.toString();
                    if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "updateRobotsTextFile(): dummyPayload = " + dummyPayload);

                    taskOpt = withUrl(TASK_URIPATH_PREFIX + "robotsTextFiles/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).header("X-AsyncTask-Payload", "DummyPayload").taskName(taskName).payload(dummyPayload);
                } else {
                    throw new BaseException("Marshaling failed during task enqueing because the memcache service is not avaiable.");
                }
            } else {
                marshaller = jAXBContext.createMarshaller();
                //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
                writer = new StringWriter();
                marshaller.marshal(stub, writer);
                String payload = writer.toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "updateRobotsTextFile(): payload = " + payload);

                taskOpt = withUrl(TASK_URIPATH_PREFIX + "robotsTextFiles/" + guid).method(Method.PUT).header("Content-Type", MediaType.APPLICATION_XML).taskName(taskName).payload(payload);
            }
        } catch (JAXBException e) {
            log.log(Level.WARNING, "Marshaling failed during task enqueing.");
            throw new BaseException("Marshaling failed during task enqueing.", e);
        } catch (Exception e) {
            log.log(Level.WARNING, "Payload generation failed during task enqueing.");
            throw new BaseException("Payload generation failed during task enqueing.", e);
        }
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        Queue queue = QueueFactory.getQueue(getAsyncTaskQueueName());
        //String taskName = "RsDeleteRobotsTextFile-" + guid;
        String taskName = "RsDeleteRobotsTextFile-" + guid + "-" + (new Date()).getTime();
        TaskOptions taskOpt = withUrl(TASK_URIPATH_PREFIX + "robotsTextFiles/" + guid).method(Method.DELETE).taskName(taskName);
        queue.add(taskOpt);

        log.finer("END");
        return true;  // ???
    }

    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        String guid = robotsTextFile.getGuid();
        return deleteRobotsTextFile(guid);
    }

    @Override
    public Long deleteRobotsTextFiles(String filter, String params, List<String> values) throws BaseException
    {
        return remoteProxy.deleteRobotsTextFiles(filter, params, values);
    }

}
