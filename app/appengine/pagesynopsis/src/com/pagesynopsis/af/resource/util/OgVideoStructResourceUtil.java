package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.af.bean.OgVideoStructBean;


public class OgVideoStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgVideoStructResourceUtil.class.getName());

    // Static methods only.
    private OgVideoStructResourceUtil() {}

    public static OgVideoStructBean convertOgVideoStructStubToBean(OgVideoStruct stub)
    {
        OgVideoStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new OgVideoStructBean();
            bean.setUuid(stub.getUuid());
            bean.setUrl(stub.getUrl());
            bean.setSecureUrl(stub.getSecureUrl());
            bean.setType(stub.getType());
            bean.setWidth(stub.getWidth());
            bean.setHeight(stub.getHeight());
        }
        return bean;
    }

}
