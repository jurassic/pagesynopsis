package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.af.bean.OgAudioStructBean;


public class OgAudioStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgAudioStructResourceUtil.class.getName());

    // Static methods only.
    private OgAudioStructResourceUtil() {}

    public static OgAudioStructBean convertOgAudioStructStubToBean(OgAudioStruct stub)
    {
        OgAudioStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new OgAudioStructBean();
            bean.setUuid(stub.getUuid());
            bean.setUrl(stub.getUrl());
            bean.setSecureUrl(stub.getSecureUrl());
            bean.setType(stub.getType());
        }
        return bean;
    }

}
