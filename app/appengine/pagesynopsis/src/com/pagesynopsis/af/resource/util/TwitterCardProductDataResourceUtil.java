package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;


public class TwitterCardProductDataResourceUtil
{
    private static final Logger log = Logger.getLogger(TwitterCardProductDataResourceUtil.class.getName());

    // Static methods only.
    private TwitterCardProductDataResourceUtil() {}

    public static TwitterCardProductDataBean convertTwitterCardProductDataStubToBean(TwitterCardProductData stub)
    {
        TwitterCardProductDataBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new TwitterCardProductDataBean();
            bean.setData(stub.getData());
            bean.setLabel(stub.getLabel());
        }
        return bean;
    }

}
