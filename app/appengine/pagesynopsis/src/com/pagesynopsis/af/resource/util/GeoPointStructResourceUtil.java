package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.stub.GeoPointStructStub;
import com.pagesynopsis.af.bean.GeoPointStructBean;


public class GeoPointStructResourceUtil
{
    private static final Logger log = Logger.getLogger(GeoPointStructResourceUtil.class.getName());

    // Static methods only.
    private GeoPointStructResourceUtil() {}

    public static GeoPointStructBean convertGeoPointStructStubToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new GeoPointStructBean();
            bean.setUuid(stub.getUuid());
            bean.setLatitude(stub.getLatitude());
            bean.setLongitude(stub.getLongitude());
            bean.setAltitude(stub.getAltitude());
            bean.setSensorUsed(stub.isSensorUsed());
        }
        return bean;
    }

}
