package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.stub.RobotsTextGroupStub;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;


public class RobotsTextGroupResourceUtil
{
    private static final Logger log = Logger.getLogger(RobotsTextGroupResourceUtil.class.getName());

    // Static methods only.
    private RobotsTextGroupResourceUtil() {}

    public static RobotsTextGroupBean convertRobotsTextGroupStubToBean(RobotsTextGroup stub)
    {
        RobotsTextGroupBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new RobotsTextGroupBean();
            bean.setUuid(stub.getUuid());
            bean.setUserAgent(stub.getUserAgent());
            bean.setAllows(stub.getAllows());
            bean.setDisallows(stub.getDisallows());
        }
        return bean;
    }

}
