package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.ws.stub.EncodedQueryParamStructStub;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;


public class EncodedQueryParamStructResourceUtil
{
    private static final Logger log = Logger.getLogger(EncodedQueryParamStructResourceUtil.class.getName());

    // Static methods only.
    private EncodedQueryParamStructResourceUtil() {}

    public static EncodedQueryParamStructBean convertEncodedQueryParamStructStubToBean(EncodedQueryParamStruct stub)
    {
        EncodedQueryParamStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new EncodedQueryParamStructBean();
            bean.setParamType(stub.getParamType());
            bean.setOriginalString(stub.getOriginalString());
            bean.setEncodedString(stub.getEncodedString());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
