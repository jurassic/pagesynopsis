package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.af.bean.AnchorStructBean;


public class AnchorStructResourceUtil
{
    private static final Logger log = Logger.getLogger(AnchorStructResourceUtil.class.getName());

    // Static methods only.
    private AnchorStructResourceUtil() {}

    public static AnchorStructBean convertAnchorStructStubToBean(AnchorStruct stub)
    {
        AnchorStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new AnchorStructBean();
            bean.setUuid(stub.getUuid());
            bean.setId(stub.getId());
            bean.setName(stub.getName());
            bean.setHref(stub.getHref());
            bean.setHrefUrl(stub.getHrefUrl());
            bean.setUrlScheme(stub.getUrlScheme());
            bean.setRel(stub.getRel());
            bean.setTarget(stub.getTarget());
            bean.setInnerHtml(stub.getInnerHtml());
            bean.setAnchorText(stub.getAnchorText());
            bean.setAnchorTitle(stub.getAnchorTitle());
            bean.setAnchorImage(stub.getAnchorImage());
            bean.setAnchorLegend(stub.getAnchorLegend());
            bean.setNote(stub.getNote());
        }
        return bean;
    }

}
