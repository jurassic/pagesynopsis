package com.pagesynopsis.af.resource.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.CommonConstants;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.af.bean.OgImageStructBean;


public class OgImageStructResourceUtil
{
    private static final Logger log = Logger.getLogger(OgImageStructResourceUtil.class.getName());

    // Static methods only.
    private OgImageStructResourceUtil() {}

    public static OgImageStructBean convertOgImageStructStubToBean(OgImageStruct stub)
    {
        OgImageStructBean bean = null;
        if(stub == null) {
            log.log(Level.INFO, "Stub is null. Empty bean is returned.");
        } else {
            bean = new OgImageStructBean();
            bean.setUuid(stub.getUuid());
            bean.setUrl(stub.getUrl());
            bean.setSecureUrl(stub.getSecureUrl());
            bean.setType(stub.getType());
            bean.setWidth(stub.getWidth());
            bean.setHeight(stub.getHeight());
        }
        return bean;
    }

}
