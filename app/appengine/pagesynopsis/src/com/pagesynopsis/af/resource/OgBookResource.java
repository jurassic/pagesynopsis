package com.pagesynopsis.af.resource;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;

public interface OgBookResource
{

    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllOgBooks(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("allkeys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getAllOgBookKeys(@QueryParam("ordering") String ordering, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Path("keys")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findOgBookKeys(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response findOgBooks(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor) throws BaseResourceException;

    @GET
    @Produces({ "application/x-javascript" })
    Response findOgBooksAsJsonp(@QueryParam("filter") String filter, @QueryParam("ordering") String ordering, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("grouping") String grouping, @QueryParam("unique") Boolean unique, @QueryParam("offset") Long offset, @QueryParam("count") Integer count, @QueryParam("cursor") StringCursor forwardCursor, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    @Path("count")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getCount(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values, @QueryParam("aggregate") String aggregate) throws BaseResourceException;

//    @GET
//    @Path("{guid : [0-9a-fA-F\\-]+}")
//    @Produces({MediaType.TEXT_HTML})
//    Response getOgBookAsHtml(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    // @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Support both guid and key ???  (Note: We have to be consistent!)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getOgBook(@PathParam("guid") String guid) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({ "application/x-javascript" })
    Response getOgBookAsJsonp(@PathParam("guid") String guid, @DefaultValue("callback") @QueryParam("callback") String callback) throws BaseResourceException;

    @GET
    //@Path("{guid : [0-9a-fA-F\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Path("{guid : [0-9a-f\\-]+}/{field : [a-zA-Z_][0-9a-zA-Z_]*}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response getOgBook(@PathParam("guid") String guid, @PathParam("field") String field) throws BaseResourceException;

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response constructOgBook(OgBookStub ogBook) throws BaseResourceException;

    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createOgBook(OgBookStub ogBook) throws BaseResourceException;

//    @POST
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response createOgBook(MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response refreshOgBook(@PathParam("guid") String guid, OgBookStub ogBook) throws BaseResourceException;

    @PUT
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    //@Produces({MediaType.TEXT_PLAIN})    // ??? updateOgBook() returns 204 (No Content)
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateOgBook(@PathParam("guid") String guid, OgBookStub ogBook) throws BaseResourceException;

    //@PUT
    //@Path("{guid : [0-9a-fA-F\\-]+}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    //Response updateOgBook(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    //@PUT  ???
    @POST   // We can adhere to semantics of PUT=Replace, POST=update. PUT is supported in HTML form in HTML5 only.
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response updateOgBook(@PathParam("guid") String guid, @QueryParam("url") String url, @QueryParam("type") String type, @QueryParam("siteName") String siteName, @QueryParam("title") String title, @QueryParam("description") String description, @QueryParam("fbAdmins") List<String> fbAdmins, @QueryParam("fbAppId") List<String> fbAppId, @QueryParam("image") List<String> image, @QueryParam("audio") List<String> audio, @QueryParam("video") List<String> video, @QueryParam("locale") String locale, @QueryParam("localeAlternate") List<String> localeAlternate, @QueryParam("author") List<String> author, @QueryParam("isbn") String isbn, @QueryParam("tag") List<String> tag, @QueryParam("releaseDate") String releaseDate) throws BaseResourceException;

//    @POST
//    //@Path("{guid : [0-9a-fA-F\\-]+}")
//    @Path("{guid : [0-9a-f\\-]+}")     // Lower case only.
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    Response updateOgBook(@PathParam("guid") String guid, MultivaluedMap<String, String> formParams) throws BaseResourceException;

    @DELETE
    @Path("{guid: [a-zA-Z0-9\\-_]+}")     // Due to the way Jersey defines "resources", @Path("{guid: [a-f0-9\\-]+}") cannot be used...
    Response deleteOgBook(@PathParam("guid") String guid) throws BaseResourceException;

    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response deleteOgBooks(@QueryParam("filter") String filter, @QueryParam("params") String params, @QueryParam("values") List<String> values) throws BaseResourceException;

    @POST
    @Path("bulk")
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    Response createOgBooks(OgBookListStub ogBooks) throws BaseResourceException;

//    @PUT
//    @Path("bulk")
//    @Produces({MediaType.TEXT_PLAIN})
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    Response updateeOgBooks(OgBookListStub ogBooks) throws BaseResourceException;

}
