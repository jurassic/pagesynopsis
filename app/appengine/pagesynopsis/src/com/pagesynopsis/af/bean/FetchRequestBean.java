package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.stub.NotificationStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.ReferrerInfoStructStub;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.stub.FetchRequestStub;


// Wrapper class + bean combo.
public class FetchRequestBean implements FetchRequest, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(FetchRequestBean.class.getName());

    // [1] With an embedded object.
    private FetchRequestStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String targetUrl;
    private String pageUrl;
    private String queryString;
    private List<KeyValuePairStruct> queryParams;
    private String version;
    private String fetchObject;
    private Integer fetchStatus;
    private String result;
    private String note;
    private ReferrerInfoStructBean referrerInfo;
    private String status;
    private String nextPageUrl;
    private Integer followPages;
    private Integer followDepth;
    private Boolean createVersion;
    private Boolean deferred;
    private Boolean alert;
    private NotificationStructBean notificationPref;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public FetchRequestBean()
    {
        //this((String) null);
    }
    public FetchRequestBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public FetchRequestBean(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStructBean referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStructBean notificationPref)
    {
        this(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfo, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPref, null, null);
    }
    public FetchRequestBean(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStructBean referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStructBean notificationPref, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.targetUrl = targetUrl;
        this.pageUrl = pageUrl;
        this.queryString = queryString;
        this.queryParams = queryParams;
        this.version = version;
        this.fetchObject = fetchObject;
        this.fetchStatus = fetchStatus;
        this.result = result;
        this.note = note;
        this.referrerInfo = referrerInfo;
        this.status = status;
        this.nextPageUrl = nextPageUrl;
        this.followPages = followPages;
        this.followDepth = followDepth;
        this.createVersion = createVersion;
        this.deferred = deferred;
        this.alert = alert;
        this.notificationPref = notificationPref;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public FetchRequestBean(FetchRequest stub)
    {
        if(stub instanceof FetchRequestStub) {
            this.stub = (FetchRequestStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setTargetUrl(stub.getTargetUrl());   
            setPageUrl(stub.getPageUrl());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setVersion(stub.getVersion());   
            setFetchObject(stub.getFetchObject());   
            setFetchStatus(stub.getFetchStatus());   
            setResult(stub.getResult());   
            setNote(stub.getNote());   
            ReferrerInfoStruct referrerInfo = stub.getReferrerInfo();
            if(referrerInfo instanceof ReferrerInfoStructBean) {
                setReferrerInfo((ReferrerInfoStructBean) referrerInfo);   
            } else {
                setReferrerInfo(new ReferrerInfoStructBean(referrerInfo));   
            }
            setStatus(stub.getStatus());   
            setNextPageUrl(stub.getNextPageUrl());   
            setFollowPages(stub.getFollowPages());   
            setFollowDepth(stub.getFollowDepth());   
            setCreateVersion(stub.isCreateVersion());   
            setDeferred(stub.isDeferred());   
            setAlert(stub.isAlert());   
            NotificationStruct notificationPref = stub.getNotificationPref();
            if(notificationPref instanceof NotificationStructBean) {
                setNotificationPref((NotificationStructBean) notificationPref);   
            } else {
                setNotificationPref(new NotificationStructBean(notificationPref));   
            }
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getTargetUrl()
    {
        if(getStub() != null) {
            return getStub().getTargetUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.targetUrl;
        }
    }
    public void setTargetUrl(String targetUrl)
    {
        if(getStub() != null) {
            getStub().setTargetUrl(targetUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.targetUrl = targetUrl;
        }
    }

    public String getPageUrl()
    {
        if(getStub() != null) {
            return getStub().getPageUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageUrl;
        }
    }
    public void setPageUrl(String pageUrl)
    {
        if(getStub() != null) {
            getStub().setPageUrl(pageUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageUrl = pageUrl;
        }
    }

    public String getQueryString()
    {
        if(getStub() != null) {
            return getStub().getQueryString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryString;
        }
    }
    public void setQueryString(String queryString)
    {
        if(getStub() != null) {
            getStub().setQueryString(queryString);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.queryString = queryString;
        }
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        if(getStub() != null) {
            List<KeyValuePairStruct> list = getStub().getQueryParams();
            if(list != null) {
                List<KeyValuePairStruct> bean = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : list) {
                    KeyValuePairStructBean elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = (KeyValuePairStructBean) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStructStub) {
                        elem = new KeyValuePairStructBean(keyValuePairStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryParams;
        }
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        if(queryParams != null) {
            if(getStub() != null) {
                List<KeyValuePairStruct> stub = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                    KeyValuePairStructStub elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructStub) {
                        elem = (KeyValuePairStructStub) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = ((KeyValuePairStructBean) keyValuePairStruct).getStub();
                    } else if(keyValuePairStruct instanceof KeyValuePairStruct) {
                        elem = new KeyValuePairStructStub(keyValuePairStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setQueryParams(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.queryParams = queryParams;  // ???

                List<KeyValuePairStruct> beans = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                    KeyValuePairStructBean elem = null;
                    if(keyValuePairStruct != null) {
                        if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                            elem = (KeyValuePairStructBean) keyValuePairStruct;
                        } else {
                            elem = new KeyValuePairStructBean(keyValuePairStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.queryParams = beans;
            }
        } else {
            this.queryParams = null;
        }
    }

    public String getVersion()
    {
        if(getStub() != null) {
            return getStub().getVersion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.version;
        }
    }
    public void setVersion(String version)
    {
        if(getStub() != null) {
            getStub().setVersion(version);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.version = version;
        }
    }

    public String getFetchObject()
    {
        if(getStub() != null) {
            return getStub().getFetchObject();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchObject;
        }
    }
    public void setFetchObject(String fetchObject)
    {
        if(getStub() != null) {
            getStub().setFetchObject(fetchObject);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchObject = fetchObject;
        }
    }

    public Integer getFetchStatus()
    {
        if(getStub() != null) {
            return getStub().getFetchStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchStatus;
        }
    }
    public void setFetchStatus(Integer fetchStatus)
    {
        if(getStub() != null) {
            getStub().setFetchStatus(fetchStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchStatus = fetchStatus;
        }
    }

    public String getResult()
    {
        if(getStub() != null) {
            return getStub().getResult();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.result;
        }
    }
    public void setResult(String result)
    {
        if(getStub() != null) {
            getStub().setResult(result);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.result = result;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public ReferrerInfoStruct getReferrerInfo()
    {  
        if(getStub() != null) {
            // Note the object type.
            ReferrerInfoStruct _stub_field = getStub().getReferrerInfo();
            if(_stub_field == null) {
                return null;
            } else {
                return new ReferrerInfoStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.referrerInfo;
        }
    }
    public void setReferrerInfo(ReferrerInfoStruct referrerInfo)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setReferrerInfo(referrerInfo);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(referrerInfo == null) {
                this.referrerInfo = null;
            } else {
                if(referrerInfo instanceof ReferrerInfoStructBean) {
                    this.referrerInfo = (ReferrerInfoStructBean) referrerInfo;
                } else {
                    this.referrerInfo = new ReferrerInfoStructBean(referrerInfo);
                }
            }
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public String getNextPageUrl()
    {
        if(getStub() != null) {
            return getStub().getNextPageUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextPageUrl;
        }
    }
    public void setNextPageUrl(String nextPageUrl)
    {
        if(getStub() != null) {
            getStub().setNextPageUrl(nextPageUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextPageUrl = nextPageUrl;
        }
    }

    public Integer getFollowPages()
    {
        if(getStub() != null) {
            return getStub().getFollowPages();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.followPages;
        }
    }
    public void setFollowPages(Integer followPages)
    {
        if(getStub() != null) {
            getStub().setFollowPages(followPages);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.followPages = followPages;
        }
    }

    public Integer getFollowDepth()
    {
        if(getStub() != null) {
            return getStub().getFollowDepth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.followDepth;
        }
    }
    public void setFollowDepth(Integer followDepth)
    {
        if(getStub() != null) {
            getStub().setFollowDepth(followDepth);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.followDepth = followDepth;
        }
    }

    public Boolean isCreateVersion()
    {
        if(getStub() != null) {
            return getStub().isCreateVersion();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createVersion;
        }
    }
    public void setCreateVersion(Boolean createVersion)
    {
        if(getStub() != null) {
            getStub().setCreateVersion(createVersion);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createVersion = createVersion;
        }
    }

    public Boolean isDeferred()
    {
        if(getStub() != null) {
            return getStub().isDeferred();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.deferred;
        }
    }
    public void setDeferred(Boolean deferred)
    {
        if(getStub() != null) {
            getStub().setDeferred(deferred);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.deferred = deferred;
        }
    }

    public Boolean isAlert()
    {
        if(getStub() != null) {
            return getStub().isAlert();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.alert;
        }
    }
    public void setAlert(Boolean alert)
    {
        if(getStub() != null) {
            getStub().setAlert(alert);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.alert = alert;
        }
    }

    public NotificationStruct getNotificationPref()
    {  
        if(getStub() != null) {
            // Note the object type.
            NotificationStruct _stub_field = getStub().getNotificationPref();
            if(_stub_field == null) {
                return null;
            } else {
                return new NotificationStructBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.notificationPref;
        }
    }
    public void setNotificationPref(NotificationStruct notificationPref)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setNotificationPref(notificationPref);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(notificationPref == null) {
                this.notificationPref = null;
            } else {
                if(notificationPref instanceof NotificationStructBean) {
                    this.notificationPref = (NotificationStructBean) notificationPref;
                } else {
                    this.notificationPref = new NotificationStructBean(notificationPref);
                }
            }
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public FetchRequestStub getStub()
    {
        return this.stub;
    }
    protected void setStub(FetchRequestStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("targetUrl = " + this.targetUrl).append(";");
            sb.append("pageUrl = " + this.pageUrl).append(";");
            sb.append("queryString = " + this.queryString).append(";");
            sb.append("queryParams = " + this.queryParams).append(";");
            sb.append("version = " + this.version).append(";");
            sb.append("fetchObject = " + this.fetchObject).append(";");
            sb.append("fetchStatus = " + this.fetchStatus).append(";");
            sb.append("result = " + this.result).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("referrerInfo = " + this.referrerInfo).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("nextPageUrl = " + this.nextPageUrl).append(";");
            sb.append("followPages = " + this.followPages).append(";");
            sb.append("followDepth = " + this.followDepth).append(";");
            sb.append("createVersion = " + this.createVersion).append(";");
            sb.append("deferred = " + this.deferred).append(";");
            sb.append("alert = " + this.alert).append(";");
            sb.append("notificationPref = " + this.notificationPref).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = targetUrl == null ? 0 : targetUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageUrl == null ? 0 : pageUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryString == null ? 0 : queryString.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryParams == null ? 0 : queryParams.hashCode();
            _hash = 31 * _hash + delta;
            delta = version == null ? 0 : version.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchObject == null ? 0 : fetchObject.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchStatus == null ? 0 : fetchStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = result == null ? 0 : result.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = referrerInfo == null ? 0 : referrerInfo.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextPageUrl == null ? 0 : nextPageUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = followPages == null ? 0 : followPages.hashCode();
            _hash = 31 * _hash + delta;
            delta = followDepth == null ? 0 : followDepth.hashCode();
            _hash = 31 * _hash + delta;
            delta = createVersion == null ? 0 : createVersion.hashCode();
            _hash = 31 * _hash + delta;
            delta = deferred == null ? 0 : deferred.hashCode();
            _hash = 31 * _hash + delta;
            delta = alert == null ? 0 : alert.hashCode();
            _hash = 31 * _hash + delta;
            delta = notificationPref == null ? 0 : notificationPref.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
