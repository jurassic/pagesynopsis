package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.PageBase;
import com.pagesynopsis.ws.stub.PageBaseStub;


// Wrapper class + bean combo.
public abstract class PageBaseBean implements PageBase, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageBaseBean.class.getName());

    // [1] With an embedded object.
    private PageBaseStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String user;
    private String fetchRequest;
    private String targetUrl;
    private String pageUrl;
    private String queryString;
    private List<KeyValuePairStruct> queryParams;
    private String lastFetchResult;
    private Integer responseCode;
    private String contentType;
    private Integer contentLength;
    private String language;
    private String redirect;
    private String location;
    private String pageTitle;
    private String note;
    private Boolean deferred;
    private String status;
    private Integer refreshStatus;
    private Long refreshInterval;
    private Long nextRefreshTime;
    private Long lastCheckedTime;
    private Long lastUpdatedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public PageBaseBean()
    {
        //this((String) null);
    }
    public PageBaseBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PageBaseBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, null, null);
    }
    public PageBaseBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.user = user;
        this.fetchRequest = fetchRequest;
        this.targetUrl = targetUrl;
        this.pageUrl = pageUrl;
        this.queryString = queryString;
        this.queryParams = queryParams;
        this.lastFetchResult = lastFetchResult;
        this.responseCode = responseCode;
        this.contentType = contentType;
        this.contentLength = contentLength;
        this.language = language;
        this.redirect = redirect;
        this.location = location;
        this.pageTitle = pageTitle;
        this.note = note;
        this.deferred = deferred;
        this.status = status;
        this.refreshStatus = refreshStatus;
        this.refreshInterval = refreshInterval;
        this.nextRefreshTime = nextRefreshTime;
        this.lastCheckedTime = lastCheckedTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public PageBaseBean(PageBase stub)
    {
        if(stub instanceof PageBaseStub) {
            this.stub = (PageBaseStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setFetchRequest(stub.getFetchRequest());   
            setTargetUrl(stub.getTargetUrl());   
            setPageUrl(stub.getPageUrl());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setLastFetchResult(stub.getLastFetchResult());   
            setResponseCode(stub.getResponseCode());   
            setContentType(stub.getContentType());   
            setContentLength(stub.getContentLength());   
            setLanguage(stub.getLanguage());   
            setRedirect(stub.getRedirect());   
            setLocation(stub.getLocation());   
            setPageTitle(stub.getPageTitle());   
            setNote(stub.getNote());   
            setDeferred(stub.isDeferred());   
            setStatus(stub.getStatus());   
            setRefreshStatus(stub.getRefreshStatus());   
            setRefreshInterval(stub.getRefreshInterval());   
            setNextRefreshTime(stub.getNextRefreshTime());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getUser()
    {
        if(getStub() != null) {
            return getStub().getUser();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.user;
        }
    }
    public void setUser(String user)
    {
        if(getStub() != null) {
            getStub().setUser(user);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.user = user;
        }
    }

    public String getFetchRequest()
    {
        if(getStub() != null) {
            return getStub().getFetchRequest();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.fetchRequest;
        }
    }
    public void setFetchRequest(String fetchRequest)
    {
        if(getStub() != null) {
            getStub().setFetchRequest(fetchRequest);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.fetchRequest = fetchRequest;
        }
    }

    public String getTargetUrl()
    {
        if(getStub() != null) {
            return getStub().getTargetUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.targetUrl;
        }
    }
    public void setTargetUrl(String targetUrl)
    {
        if(getStub() != null) {
            getStub().setTargetUrl(targetUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.targetUrl = targetUrl;
        }
    }

    public String getPageUrl()
    {
        if(getStub() != null) {
            return getStub().getPageUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageUrl;
        }
    }
    public void setPageUrl(String pageUrl)
    {
        if(getStub() != null) {
            getStub().setPageUrl(pageUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageUrl = pageUrl;
        }
    }

    public String getQueryString()
    {
        if(getStub() != null) {
            return getStub().getQueryString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryString;
        }
    }
    public void setQueryString(String queryString)
    {
        if(getStub() != null) {
            getStub().setQueryString(queryString);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.queryString = queryString;
        }
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        if(getStub() != null) {
            List<KeyValuePairStruct> list = getStub().getQueryParams();
            if(list != null) {
                List<KeyValuePairStruct> bean = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : list) {
                    KeyValuePairStructBean elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = (KeyValuePairStructBean) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStructStub) {
                        elem = new KeyValuePairStructBean(keyValuePairStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.queryParams;
        }
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        if(queryParams != null) {
            if(getStub() != null) {
                List<KeyValuePairStruct> stub = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                    KeyValuePairStructStub elem = null;
                    if(keyValuePairStruct instanceof KeyValuePairStructStub) {
                        elem = (KeyValuePairStructStub) keyValuePairStruct;
                    } else if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                        elem = ((KeyValuePairStructBean) keyValuePairStruct).getStub();
                    } else if(keyValuePairStruct instanceof KeyValuePairStruct) {
                        elem = new KeyValuePairStructStub(keyValuePairStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setQueryParams(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.queryParams = queryParams;  // ???

                List<KeyValuePairStruct> beans = new ArrayList<KeyValuePairStruct>();
                for(KeyValuePairStruct keyValuePairStruct : queryParams) {
                    KeyValuePairStructBean elem = null;
                    if(keyValuePairStruct != null) {
                        if(keyValuePairStruct instanceof KeyValuePairStructBean) {
                            elem = (KeyValuePairStructBean) keyValuePairStruct;
                        } else {
                            elem = new KeyValuePairStructBean(keyValuePairStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.queryParams = beans;
            }
        } else {
            this.queryParams = null;
        }
    }

    public String getLastFetchResult()
    {
        if(getStub() != null) {
            return getStub().getLastFetchResult();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastFetchResult;
        }
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        if(getStub() != null) {
            getStub().setLastFetchResult(lastFetchResult);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastFetchResult = lastFetchResult;
        }
    }

    public Integer getResponseCode()
    {
        if(getStub() != null) {
            return getStub().getResponseCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.responseCode;
        }
    }
    public void setResponseCode(Integer responseCode)
    {
        if(getStub() != null) {
            getStub().setResponseCode(responseCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.responseCode = responseCode;
        }
    }

    public String getContentType()
    {
        if(getStub() != null) {
            return getStub().getContentType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contentType;
        }
    }
    public void setContentType(String contentType)
    {
        if(getStub() != null) {
            getStub().setContentType(contentType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.contentType = contentType;
        }
    }

    public Integer getContentLength()
    {
        if(getStub() != null) {
            return getStub().getContentLength();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.contentLength;
        }
    }
    public void setContentLength(Integer contentLength)
    {
        if(getStub() != null) {
            getStub().setContentLength(contentLength);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.contentLength = contentLength;
        }
    }

    public String getLanguage()
    {
        if(getStub() != null) {
            return getStub().getLanguage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.language;
        }
    }
    public void setLanguage(String language)
    {
        if(getStub() != null) {
            getStub().setLanguage(language);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.language = language;
        }
    }

    public String getRedirect()
    {
        if(getStub() != null) {
            return getStub().getRedirect();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirect;
        }
    }
    public void setRedirect(String redirect)
    {
        if(getStub() != null) {
            getStub().setRedirect(redirect);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirect = redirect;
        }
    }

    public String getLocation()
    {
        if(getStub() != null) {
            return getStub().getLocation();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.location;
        }
    }
    public void setLocation(String location)
    {
        if(getStub() != null) {
            getStub().setLocation(location);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.location = location;
        }
    }

    public String getPageTitle()
    {
        if(getStub() != null) {
            return getStub().getPageTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageTitle;
        }
    }
    public void setPageTitle(String pageTitle)
    {
        if(getStub() != null) {
            getStub().setPageTitle(pageTitle);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageTitle = pageTitle;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public Boolean isDeferred()
    {
        if(getStub() != null) {
            return getStub().isDeferred();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.deferred;
        }
    }
    public void setDeferred(Boolean deferred)
    {
        if(getStub() != null) {
            getStub().setDeferred(deferred);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.deferred = deferred;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Integer getRefreshStatus()
    {
        if(getStub() != null) {
            return getStub().getRefreshStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshStatus;
        }
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        if(getStub() != null) {
            getStub().setRefreshStatus(refreshStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshStatus = refreshStatus;
        }
    }

    public Long getRefreshInterval()
    {
        if(getStub() != null) {
            return getStub().getRefreshInterval();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshInterval;
        }
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        if(getStub() != null) {
            getStub().setRefreshInterval(refreshInterval);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshInterval = refreshInterval;
        }
    }

    public Long getNextRefreshTime()
    {
        if(getStub() != null) {
            return getStub().getNextRefreshTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextRefreshTime;
        }
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        if(getStub() != null) {
            getStub().setNextRefreshTime(nextRefreshTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextRefreshTime = nextRefreshTime;
        }
    }

    public Long getLastCheckedTime()
    {
        if(getStub() != null) {
            return getStub().getLastCheckedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastCheckedTime;
        }
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        if(getStub() != null) {
            getStub().setLastCheckedTime(lastCheckedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastCheckedTime = lastCheckedTime;
        }
    }

    public Long getLastUpdatedTime()
    {
        if(getStub() != null) {
            return getStub().getLastUpdatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastUpdatedTime;
        }
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        if(getStub() != null) {
            getStub().setLastUpdatedTime(lastUpdatedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastUpdatedTime = lastUpdatedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public PageBaseStub getStub()
    {
        return this.stub;
    }
    protected void setStub(PageBaseStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("user = " + this.user).append(";");
            sb.append("fetchRequest = " + this.fetchRequest).append(";");
            sb.append("targetUrl = " + this.targetUrl).append(";");
            sb.append("pageUrl = " + this.pageUrl).append(";");
            sb.append("queryString = " + this.queryString).append(";");
            sb.append("queryParams = " + this.queryParams).append(";");
            sb.append("lastFetchResult = " + this.lastFetchResult).append(";");
            sb.append("responseCode = " + this.responseCode).append(";");
            sb.append("contentType = " + this.contentType).append(";");
            sb.append("contentLength = " + this.contentLength).append(";");
            sb.append("language = " + this.language).append(";");
            sb.append("redirect = " + this.redirect).append(";");
            sb.append("location = " + this.location).append(";");
            sb.append("pageTitle = " + this.pageTitle).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("deferred = " + this.deferred).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("refreshStatus = " + this.refreshStatus).append(";");
            sb.append("refreshInterval = " + this.refreshInterval).append(";");
            sb.append("nextRefreshTime = " + this.nextRefreshTime).append(";");
            sb.append("lastCheckedTime = " + this.lastCheckedTime).append(";");
            sb.append("lastUpdatedTime = " + this.lastUpdatedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = user == null ? 0 : user.hashCode();
            _hash = 31 * _hash + delta;
            delta = fetchRequest == null ? 0 : fetchRequest.hashCode();
            _hash = 31 * _hash + delta;
            delta = targetUrl == null ? 0 : targetUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageUrl == null ? 0 : pageUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryString == null ? 0 : queryString.hashCode();
            _hash = 31 * _hash + delta;
            delta = queryParams == null ? 0 : queryParams.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastFetchResult == null ? 0 : lastFetchResult.hashCode();
            _hash = 31 * _hash + delta;
            delta = responseCode == null ? 0 : responseCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = contentType == null ? 0 : contentType.hashCode();
            _hash = 31 * _hash + delta;
            delta = contentLength == null ? 0 : contentLength.hashCode();
            _hash = 31 * _hash + delta;
            delta = language == null ? 0 : language.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirect == null ? 0 : redirect.hashCode();
            _hash = 31 * _hash + delta;
            delta = location == null ? 0 : location.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageTitle == null ? 0 : pageTitle.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = deferred == null ? 0 : deferred.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshStatus == null ? 0 : refreshStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextRefreshTime == null ? 0 : nextRefreshTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastUpdatedTime == null ? 0 : lastUpdatedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
