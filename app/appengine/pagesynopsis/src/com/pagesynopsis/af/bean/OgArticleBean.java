package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.stub.OgObjectBaseStub;
import com.pagesynopsis.ws.stub.OgArticleStub;


// Wrapper class + bean combo.
public class OgArticleBean extends OgObjectBaseBean implements OgArticle, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgArticleBean.class.getName());


    // [2] Or, without an embedded object.
    private List<String> author;
    private String section;
    private List<String> tag;
    private String publishedDate;
    private String modifiedDate;
    private String expirationDate;

    // Ctors.
    public OgArticleBean()
    {
        //this((String) null);
    }
    public OgArticleBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgArticleBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate, null, null);
    }
    public OgArticleBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);

        this.author = author;
        this.section = section;
        this.tag = tag;
        this.publishedDate = publishedDate;
        this.modifiedDate = modifiedDate;
        this.expirationDate = expirationDate;
    }
    public OgArticleBean(OgArticle stub)
    {
        if(stub instanceof OgArticleStub) {
            super.setStub((OgObjectBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUrl(stub.getUrl());   
            setType(stub.getType());   
            setSiteName(stub.getSiteName());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFbAdmins(stub.getFbAdmins());   
            setFbAppId(stub.getFbAppId());   
            setImage(stub.getImage());   
            setAudio(stub.getAudio());   
            setVideo(stub.getVideo());   
            setLocale(stub.getLocale());   
            setLocaleAlternate(stub.getLocaleAlternate());   
            setAuthor(stub.getAuthor());   
            setSection(stub.getSection());   
            setTag(stub.getTag());   
            setPublishedDate(stub.getPublishedDate());   
            setModifiedDate(stub.getModifiedDate());   
            setExpirationDate(stub.getExpirationDate());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    public String getSiteName()
    {
        return super.getSiteName();
    }
    public void setSiteName(String siteName)
    {
        super.setSiteName(siteName);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public List<String> getFbAdmins()
    {
        return super.getFbAdmins();
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        super.setFbAdmins(fbAdmins);
    }

    public List<String> getFbAppId()
    {
        return super.getFbAppId();
    }
    public void setFbAppId(List<String> fbAppId)
    {
        super.setFbAppId(fbAppId);
    }

    public List<OgImageStruct> getImage()
    {
        return super.getImage();
    }
    public void setImage(List<OgImageStruct> image)
    {
        super.setImage(image);
    }

    public List<OgAudioStruct> getAudio()
    {
        return super.getAudio();
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        super.setAudio(audio);
    }

    public List<OgVideoStruct> getVideo()
    {
        return super.getVideo();
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        super.setVideo(video);
    }

    public String getLocale()
    {
        return super.getLocale();
    }
    public void setLocale(String locale)
    {
        super.setLocale(locale);
    }

    public List<String> getLocaleAlternate()
    {
        return super.getLocaleAlternate();
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        super.setLocaleAlternate(localeAlternate);
    }

    public List<String> getAuthor()
    {
        if(getStub() != null) {
            return getStub().getAuthor();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.author;
        }
    }
    public void setAuthor(List<String> author)
    {
        if(getStub() != null) {
            getStub().setAuthor(author);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.author = author;
        }
    }

    public String getSection()
    {
        if(getStub() != null) {
            return getStub().getSection();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.section;
        }
    }
    public void setSection(String section)
    {
        if(getStub() != null) {
            getStub().setSection(section);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.section = section;
        }
    }

    public List<String> getTag()
    {
        if(getStub() != null) {
            return getStub().getTag();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.tag;
        }
    }
    public void setTag(List<String> tag)
    {
        if(getStub() != null) {
            getStub().setTag(tag);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.tag = tag;
        }
    }

    public String getPublishedDate()
    {
        if(getStub() != null) {
            return getStub().getPublishedDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.publishedDate;
        }
    }
    public void setPublishedDate(String publishedDate)
    {
        if(getStub() != null) {
            getStub().setPublishedDate(publishedDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.publishedDate = publishedDate;
        }
    }

    public String getModifiedDate()
    {
        if(getStub() != null) {
            return getStub().getModifiedDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedDate;
        }
    }
    public void setModifiedDate(String modifiedDate)
    {
        if(getStub() != null) {
            getStub().setModifiedDate(modifiedDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedDate = modifiedDate;
        }
    }

    public String getExpirationDate()
    {
        if(getStub() != null) {
            return getStub().getExpirationDate();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationDate;
        }
    }
    public void setExpirationDate(String expirationDate)
    {
        if(getStub() != null) {
            getStub().setExpirationDate(expirationDate);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationDate = expirationDate;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public OgArticleStub getStub()
    {
        return (OgArticleStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("author = " + this.author).append(";");
            sb.append("section = " + this.section).append(";");
            sb.append("tag = " + this.tag).append(";");
            sb.append("publishedDate = " + this.publishedDate).append(";");
            sb.append("modifiedDate = " + this.modifiedDate).append(";");
            sb.append("expirationDate = " + this.expirationDate).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = author == null ? 0 : author.hashCode();
            _hash = 31 * _hash + delta;
            delta = section == null ? 0 : section.hashCode();
            _hash = 31 * _hash + delta;
            delta = tag == null ? 0 : tag.hashCode();
            _hash = 31 * _hash + delta;
            delta = publishedDate == null ? 0 : publishedDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedDate == null ? 0 : modifiedDate.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationDate == null ? 0 : expirationDate.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
