package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.stub.UrlStructStub;


// Wrapper class + bean combo.
public class UrlStructBean implements UrlStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlStructBean.class.getName());

    // [1] With an embedded object.
    private UrlStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private Integer statusCode;
    private String redirectUrl;
    private String absoluteUrl;
    private String hashFragment;
    private String note;

    // Ctors.
    public UrlStructBean()
    {
        //this((String) null);
    }
    public UrlStructBean(String uuid, Integer statusCode, String redirectUrl, String absoluteUrl, String hashFragment, String note)
    {
        this.uuid = uuid;
        this.statusCode = statusCode;
        this.redirectUrl = redirectUrl;
        this.absoluteUrl = absoluteUrl;
        this.hashFragment = hashFragment;
        this.note = note;
    }
    public UrlStructBean(UrlStruct stub)
    {
        if(stub instanceof UrlStructStub) {
            this.stub = (UrlStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setStatusCode(stub.getStatusCode());   
            setRedirectUrl(stub.getRedirectUrl());   
            setAbsoluteUrl(stub.getAbsoluteUrl());   
            setHashFragment(stub.getHashFragment());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public Integer getStatusCode()
    {
        if(getStub() != null) {
            return getStub().getStatusCode();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.statusCode;
        }
    }
    public void setStatusCode(Integer statusCode)
    {
        if(getStub() != null) {
            getStub().setStatusCode(statusCode);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.statusCode = statusCode;
        }
    }

    public String getRedirectUrl()
    {
        if(getStub() != null) {
            return getStub().getRedirectUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectUrl;
        }
    }
    public void setRedirectUrl(String redirectUrl)
    {
        if(getStub() != null) {
            getStub().setRedirectUrl(redirectUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.redirectUrl = redirectUrl;
        }
    }

    public String getAbsoluteUrl()
    {
        if(getStub() != null) {
            return getStub().getAbsoluteUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.absoluteUrl;
        }
    }
    public void setAbsoluteUrl(String absoluteUrl)
    {
        if(getStub() != null) {
            getStub().setAbsoluteUrl(absoluteUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.absoluteUrl = absoluteUrl;
        }
    }

    public String getHashFragment()
    {
        if(getStub() != null) {
            return getStub().getHashFragment();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hashFragment;
        }
    }
    public void setHashFragment(String hashFragment)
    {
        if(getStub() != null) {
            getStub().setHashFragment(hashFragment);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hashFragment = hashFragment;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getStatusCode() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRedirectUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAbsoluteUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHashFragment() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public UrlStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UrlStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("statusCode = " + this.statusCode).append(";");
            sb.append("redirectUrl = " + this.redirectUrl).append(";");
            sb.append("absoluteUrl = " + this.absoluteUrl).append(";");
            sb.append("hashFragment = " + this.hashFragment).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = statusCode == null ? 0 : statusCode.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirectUrl == null ? 0 : redirectUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = absoluteUrl == null ? 0 : absoluteUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = hashFragment == null ? 0 : hashFragment.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
