package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.stub.OgAudioStructStub;


// Wrapper class + bean combo.
public class OgAudioStructBean implements OgAudioStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgAudioStructBean.class.getName());

    // [1] With an embedded object.
    private OgAudioStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String url;
    private String secureUrl;
    private String type;

    // Ctors.
    public OgAudioStructBean()
    {
        //this((String) null);
    }
    public OgAudioStructBean(String uuid, String url, String secureUrl, String type)
    {
        this.uuid = uuid;
        this.url = url;
        this.secureUrl = secureUrl;
        this.type = type;
    }
    public OgAudioStructBean(OgAudioStruct stub)
    {
        if(stub instanceof OgAudioStructStub) {
            this.stub = (OgAudioStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setUrl(stub.getUrl());   
            setSecureUrl(stub.getSecureUrl());   
            setType(stub.getType());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getUrl()
    {
        if(getStub() != null) {
            return getStub().getUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.url;
        }
    }
    public void setUrl(String url)
    {
        if(getStub() != null) {
            getStub().setUrl(url);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.url = url;
        }
    }

    public String getSecureUrl()
    {
        if(getStub() != null) {
            return getStub().getSecureUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.secureUrl;
        }
    }
    public void setSecureUrl(String secureUrl)
    {
        if(getStub() != null) {
            getStub().setSecureUrl(secureUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.secureUrl = secureUrl;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSecureUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public OgAudioStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(OgAudioStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("url = " + this.url).append(";");
            sb.append("secureUrl = " + this.secureUrl).append(";");
            sb.append("type = " + this.type).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = url == null ? 0 : url.hashCode();
            _hash = 31 * _hash + delta;
            delta = secureUrl == null ? 0 : secureUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
