package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.stub.PageBaseStub;
import com.pagesynopsis.ws.stub.LinkListStub;


// Wrapper class + bean combo.
public class LinkListBean extends PageBaseBean implements LinkList, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(LinkListBean.class.getName());


    // [2] Or, without an embedded object.
    private List<String> urlSchemeFilter;
    private List<AnchorStruct> pageAnchors;
    private Boolean excludeRelativeUrls;
    private List<String> excludedBaseUrls;

    // Ctors.
    public LinkListBean()
    {
        //this((String) null);
    }
    public LinkListBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public LinkListBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls, null, null);
    }
    public LinkListBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);

        this.urlSchemeFilter = urlSchemeFilter;
        this.pageAnchors = pageAnchors;
        this.excludeRelativeUrls = excludeRelativeUrls;
        this.excludedBaseUrls = excludedBaseUrls;
    }
    public LinkListBean(LinkList stub)
    {
        if(stub instanceof LinkListStub) {
            super.setStub((PageBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setFetchRequest(stub.getFetchRequest());   
            setTargetUrl(stub.getTargetUrl());   
            setPageUrl(stub.getPageUrl());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setLastFetchResult(stub.getLastFetchResult());   
            setResponseCode(stub.getResponseCode());   
            setContentType(stub.getContentType());   
            setContentLength(stub.getContentLength());   
            setLanguage(stub.getLanguage());   
            setRedirect(stub.getRedirect());   
            setLocation(stub.getLocation());   
            setPageTitle(stub.getPageTitle());   
            setNote(stub.getNote());   
            setDeferred(stub.isDeferred());   
            setStatus(stub.getStatus());   
            setRefreshStatus(stub.getRefreshStatus());   
            setRefreshInterval(stub.getRefreshInterval());   
            setNextRefreshTime(stub.getNextRefreshTime());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setUrlSchemeFilter(stub.getUrlSchemeFilter());   
            setPageAnchors(stub.getPageAnchors());   
            setExcludeRelativeUrls(stub.isExcludeRelativeUrls());   
            setExcludedBaseUrls(stub.getExcludedBaseUrls());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    public List<String> getUrlSchemeFilter()
    {
        if(getStub() != null) {
            return getStub().getUrlSchemeFilter();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.urlSchemeFilter;
        }
    }
    public void setUrlSchemeFilter(List<String> urlSchemeFilter)
    {
        if(getStub() != null) {
            getStub().setUrlSchemeFilter(urlSchemeFilter);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.urlSchemeFilter = urlSchemeFilter;
        }
    }

    public List<AnchorStruct> getPageAnchors()
    {
        if(getStub() != null) {
            List<AnchorStruct> list = getStub().getPageAnchors();
            if(list != null) {
                List<AnchorStruct> bean = new ArrayList<AnchorStruct>();
                for(AnchorStruct anchorStruct : list) {
                    AnchorStructBean elem = null;
                    if(anchorStruct instanceof AnchorStructBean) {
                        elem = (AnchorStructBean) anchorStruct;
                    } else if(anchorStruct instanceof AnchorStructStub) {
                        elem = new AnchorStructBean(anchorStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageAnchors;
        }
    }
    public void setPageAnchors(List<AnchorStruct> pageAnchors)
    {
        if(pageAnchors != null) {
            if(getStub() != null) {
                List<AnchorStruct> stub = new ArrayList<AnchorStruct>();
                for(AnchorStruct anchorStruct : pageAnchors) {
                    AnchorStructStub elem = null;
                    if(anchorStruct instanceof AnchorStructStub) {
                        elem = (AnchorStructStub) anchorStruct;
                    } else if(anchorStruct instanceof AnchorStructBean) {
                        elem = ((AnchorStructBean) anchorStruct).getStub();
                    } else if(anchorStruct instanceof AnchorStruct) {
                        elem = new AnchorStructStub(anchorStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setPageAnchors(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.pageAnchors = pageAnchors;  // ???

                List<AnchorStruct> beans = new ArrayList<AnchorStruct>();
                for(AnchorStruct anchorStruct : pageAnchors) {
                    AnchorStructBean elem = null;
                    if(anchorStruct != null) {
                        if(anchorStruct instanceof AnchorStructBean) {
                            elem = (AnchorStructBean) anchorStruct;
                        } else {
                            elem = new AnchorStructBean(anchorStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.pageAnchors = beans;
            }
        } else {
            this.pageAnchors = null;
        }
    }

    public Boolean isExcludeRelativeUrls()
    {
        if(getStub() != null) {
            return getStub().isExcludeRelativeUrls();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.excludeRelativeUrls;
        }
    }
    public void setExcludeRelativeUrls(Boolean excludeRelativeUrls)
    {
        if(getStub() != null) {
            getStub().setExcludeRelativeUrls(excludeRelativeUrls);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.excludeRelativeUrls = excludeRelativeUrls;
        }
    }

    public List<String> getExcludedBaseUrls()
    {
        if(getStub() != null) {
            return getStub().getExcludedBaseUrls();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.excludedBaseUrls;
        }
    }
    public void setExcludedBaseUrls(List<String> excludedBaseUrls)
    {
        if(getStub() != null) {
            getStub().setExcludedBaseUrls(excludedBaseUrls);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.excludedBaseUrls = excludedBaseUrls;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public LinkListStub getStub()
    {
        return (LinkListStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("urlSchemeFilter = " + this.urlSchemeFilter).append(";");
            sb.append("pageAnchors = " + this.pageAnchors).append(";");
            sb.append("excludeRelativeUrls = " + this.excludeRelativeUrls).append(";");
            sb.append("excludedBaseUrls = " + this.excludedBaseUrls).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = urlSchemeFilter == null ? 0 : urlSchemeFilter.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageAnchors == null ? 0 : pageAnchors.hashCode();
            _hash = 31 * _hash + delta;
            delta = excludeRelativeUrls == null ? 0 : excludeRelativeUrls.hashCode();
            _hash = 31 * _hash + delta;
            delta = excludedBaseUrls == null ? 0 : excludedBaseUrls.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
