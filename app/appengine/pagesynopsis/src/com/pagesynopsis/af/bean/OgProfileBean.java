package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgObjectBaseStub;
import com.pagesynopsis.ws.stub.OgProfileStub;


// Wrapper class + bean combo.
public class OgProfileBean extends OgObjectBaseBean implements OgProfile, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgProfileBean.class.getName());


    // [2] Or, without an embedded object.
    private String profileId;
    private String firstName;
    private String lastName;
    private String username;
    private String gender;

    // Ctors.
    public OgProfileBean()
    {
        //this((String) null);
    }
    public OgProfileBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public OgProfileBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender)
    {
        this(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender, null, null);
    }
    public OgProfileBean(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender, Long createdTime, Long modifiedTime)
    {
        super(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, createdTime, modifiedTime);

        this.profileId = profileId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.gender = gender;
    }
    public OgProfileBean(OgProfile stub)
    {
        if(stub instanceof OgProfileStub) {
            super.setStub((OgObjectBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUrl(stub.getUrl());   
            setType(stub.getType());   
            setSiteName(stub.getSiteName());   
            setTitle(stub.getTitle());   
            setDescription(stub.getDescription());   
            setFbAdmins(stub.getFbAdmins());   
            setFbAppId(stub.getFbAppId());   
            setImage(stub.getImage());   
            setAudio(stub.getAudio());   
            setVideo(stub.getVideo());   
            setLocale(stub.getLocale());   
            setLocaleAlternate(stub.getLocaleAlternate());   
            setProfileId(stub.getProfileId());   
            setFirstName(stub.getFirstName());   
            setLastName(stub.getLastName());   
            setUsername(stub.getUsername());   
            setGender(stub.getGender());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUrl()
    {
        return super.getUrl();
    }
    public void setUrl(String url)
    {
        super.setUrl(url);
    }

    public String getType()
    {
        return super.getType();
    }
    public void setType(String type)
    {
        super.setType(type);
    }

    public String getSiteName()
    {
        return super.getSiteName();
    }
    public void setSiteName(String siteName)
    {
        super.setSiteName(siteName);
    }

    public String getTitle()
    {
        return super.getTitle();
    }
    public void setTitle(String title)
    {
        super.setTitle(title);
    }

    public String getDescription()
    {
        return super.getDescription();
    }
    public void setDescription(String description)
    {
        super.setDescription(description);
    }

    public List<String> getFbAdmins()
    {
        return super.getFbAdmins();
    }
    public void setFbAdmins(List<String> fbAdmins)
    {
        super.setFbAdmins(fbAdmins);
    }

    public List<String> getFbAppId()
    {
        return super.getFbAppId();
    }
    public void setFbAppId(List<String> fbAppId)
    {
        super.setFbAppId(fbAppId);
    }

    public List<OgImageStruct> getImage()
    {
        return super.getImage();
    }
    public void setImage(List<OgImageStruct> image)
    {
        super.setImage(image);
    }

    public List<OgAudioStruct> getAudio()
    {
        return super.getAudio();
    }
    public void setAudio(List<OgAudioStruct> audio)
    {
        super.setAudio(audio);
    }

    public List<OgVideoStruct> getVideo()
    {
        return super.getVideo();
    }
    public void setVideo(List<OgVideoStruct> video)
    {
        super.setVideo(video);
    }

    public String getLocale()
    {
        return super.getLocale();
    }
    public void setLocale(String locale)
    {
        super.setLocale(locale);
    }

    public List<String> getLocaleAlternate()
    {
        return super.getLocaleAlternate();
    }
    public void setLocaleAlternate(List<String> localeAlternate)
    {
        super.setLocaleAlternate(localeAlternate);
    }

    public String getProfileId()
    {
        if(getStub() != null) {
            return getStub().getProfileId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.profileId;
        }
    }
    public void setProfileId(String profileId)
    {
        if(getStub() != null) {
            getStub().setProfileId(profileId);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.profileId = profileId;
        }
    }

    public String getFirstName()
    {
        if(getStub() != null) {
            return getStub().getFirstName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.firstName;
        }
    }
    public void setFirstName(String firstName)
    {
        if(getStub() != null) {
            getStub().setFirstName(firstName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.firstName = firstName;
        }
    }

    public String getLastName()
    {
        if(getStub() != null) {
            return getStub().getLastName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastName;
        }
    }
    public void setLastName(String lastName)
    {
        if(getStub() != null) {
            getStub().setLastName(lastName);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastName = lastName;
        }
    }

    public String getUsername()
    {
        if(getStub() != null) {
            return getStub().getUsername();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.username;
        }
    }
    public void setUsername(String username)
    {
        if(getStub() != null) {
            getStub().setUsername(username);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.username = username;
        }
    }

    public String getGender()
    {
        if(getStub() != null) {
            return getStub().getGender();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.gender;
        }
    }
    public void setGender(String gender)
    {
        if(getStub() != null) {
            getStub().setGender(gender);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.gender = gender;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public OgProfileStub getStub()
    {
        return (OgProfileStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("profileId = " + this.profileId).append(";");
            sb.append("firstName = " + this.firstName).append(";");
            sb.append("lastName = " + this.lastName).append(";");
            sb.append("username = " + this.username).append(";");
            sb.append("gender = " + this.gender).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = profileId == null ? 0 : profileId.hashCode();
            _hash = 31 * _hash + delta;
            delta = firstName == null ? 0 : firstName.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastName == null ? 0 : lastName.hashCode();
            _hash = 31 * _hash + delta;
            delta = username == null ? 0 : username.hashCode();
            _hash = 31 * _hash + delta;
            delta = gender == null ? 0 : gender.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
