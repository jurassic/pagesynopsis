package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.stub.RobotsTextGroupStub;


// Wrapper class + bean combo.
public class RobotsTextGroupBean implements RobotsTextGroup, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextGroupBean.class.getName());

    // [1] With an embedded object.
    private RobotsTextGroupStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String userAgent;
    private List<String> allows;
    private List<String> disallows;

    // Ctors.
    public RobotsTextGroupBean()
    {
        //this((String) null);
    }
    public RobotsTextGroupBean(String uuid, String userAgent, List<String> allows, List<String> disallows)
    {
        this.uuid = uuid;
        this.userAgent = userAgent;
        this.allows = allows;
        this.disallows = disallows;
    }
    public RobotsTextGroupBean(RobotsTextGroup stub)
    {
        if(stub instanceof RobotsTextGroupStub) {
            this.stub = (RobotsTextGroupStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setUserAgent(stub.getUserAgent());   
            setAllows(stub.getAllows());   
            setDisallows(stub.getDisallows());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getUserAgent()
    {
        if(getStub() != null) {
            return getStub().getUserAgent();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.userAgent;
        }
    }
    public void setUserAgent(String userAgent)
    {
        if(getStub() != null) {
            getStub().setUserAgent(userAgent);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.userAgent = userAgent;
        }
    }

    public List<String> getAllows()
    {
        if(getStub() != null) {
            return getStub().getAllows();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.allows;
        }
    }
    public void setAllows(List<String> allows)
    {
        if(getStub() != null) {
            getStub().setAllows(allows);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.allows = allows;
        }
    }

    public List<String> getDisallows()
    {
        if(getStub() != null) {
            return getStub().getDisallows();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.disallows;
        }
    }
    public void setDisallows(List<String> disallows)
    {
        if(getStub() != null) {
            getStub().setDisallows(disallows);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.disallows = disallows;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getUserAgent() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAllows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDisallows() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public RobotsTextGroupStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RobotsTextGroupStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("userAgent = " + this.userAgent).append(";");
            sb.append("allows = " + this.allows).append(";");
            sb.append("disallows = " + this.disallows).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = userAgent == null ? 0 : userAgent.hashCode();
            _hash = 31 * _hash + delta;
            delta = allows == null ? 0 : allows.hashCode();
            _hash = 31 * _hash + delta;
            delta = disallows == null ? 0 : disallows.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
