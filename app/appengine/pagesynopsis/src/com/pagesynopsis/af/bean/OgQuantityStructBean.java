package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.stub.OgQuantityStructStub;


// Wrapper class + bean combo.
public class OgQuantityStructBean implements OgQuantityStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgQuantityStructBean.class.getName());

    // [1] With an embedded object.
    private OgQuantityStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private Float value;
    private String units;

    // Ctors.
    public OgQuantityStructBean()
    {
        //this((String) null);
    }
    public OgQuantityStructBean(String uuid, Float value, String units)
    {
        this.uuid = uuid;
        this.value = value;
        this.units = units;
    }
    public OgQuantityStructBean(OgQuantityStruct stub)
    {
        if(stub instanceof OgQuantityStructStub) {
            this.stub = (OgQuantityStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setValue(stub.getValue());   
            setUnits(stub.getUnits());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public Float getValue()
    {
        if(getStub() != null) {
            return getStub().getValue();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.value;
        }
    }
    public void setValue(Float value)
    {
        if(getStub() != null) {
            getStub().setValue(value);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.value = value;
        }
    }

    public String getUnits()
    {
        if(getStub() != null) {
            return getStub().getUnits();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.units;
        }
    }
    public void setUnits(String units)
    {
        if(getStub() != null) {
            getStub().setUnits(units);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.units = units;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getValue() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUnits() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public OgQuantityStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(OgQuantityStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("value = " + this.value).append(";");
            sb.append("units = " + this.units).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = value == null ? 0 : value.hashCode();
            _hash = 31 * _hash + delta;
            delta = units == null ? 0 : units.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
