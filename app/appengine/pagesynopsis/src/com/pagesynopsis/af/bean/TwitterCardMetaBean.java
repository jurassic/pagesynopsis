package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.stub.PageBaseStub;
import com.pagesynopsis.ws.stub.TwitterCardMetaStub;


// Wrapper class + bean combo.
public class TwitterCardMetaBean extends PageBaseBean implements TwitterCardMeta, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(TwitterCardMetaBean.class.getName());


    // [2] Or, without an embedded object.
    private String cardType;
    private TwitterSummaryCardBean summaryCard;
    private TwitterPhotoCardBean photoCard;
    private TwitterGalleryCardBean galleryCard;
    private TwitterAppCardBean appCard;
    private TwitterPlayerCardBean playerCard;
    private TwitterProductCardBean productCard;

    // Ctors.
    public TwitterCardMetaBean()
    {
        //this((String) null);
    }
    public TwitterCardMetaBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public TwitterCardMetaBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCardBean summaryCard, TwitterPhotoCardBean photoCard, TwitterGalleryCardBean galleryCard, TwitterAppCardBean appCard, TwitterPlayerCardBean playerCard, TwitterProductCardBean productCard)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCard, photoCard, galleryCard, appCard, playerCard, productCard, null, null);
    }
    public TwitterCardMetaBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCardBean summaryCard, TwitterPhotoCardBean photoCard, TwitterGalleryCardBean galleryCard, TwitterAppCardBean appCard, TwitterPlayerCardBean playerCard, TwitterProductCardBean productCard, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);

        this.cardType = cardType;
        this.summaryCard = summaryCard;
        this.photoCard = photoCard;
        this.galleryCard = galleryCard;
        this.appCard = appCard;
        this.playerCard = playerCard;
        this.productCard = productCard;
    }
    public TwitterCardMetaBean(TwitterCardMeta stub)
    {
        if(stub instanceof TwitterCardMetaStub) {
            super.setStub((PageBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setFetchRequest(stub.getFetchRequest());   
            setTargetUrl(stub.getTargetUrl());   
            setPageUrl(stub.getPageUrl());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setLastFetchResult(stub.getLastFetchResult());   
            setResponseCode(stub.getResponseCode());   
            setContentType(stub.getContentType());   
            setContentLength(stub.getContentLength());   
            setLanguage(stub.getLanguage());   
            setRedirect(stub.getRedirect());   
            setLocation(stub.getLocation());   
            setPageTitle(stub.getPageTitle());   
            setNote(stub.getNote());   
            setDeferred(stub.isDeferred());   
            setStatus(stub.getStatus());   
            setRefreshStatus(stub.getRefreshStatus());   
            setRefreshInterval(stub.getRefreshInterval());   
            setNextRefreshTime(stub.getNextRefreshTime());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setCardType(stub.getCardType());   
            TwitterSummaryCard summaryCard = stub.getSummaryCard();
            if(summaryCard instanceof TwitterSummaryCardBean) {
                setSummaryCard((TwitterSummaryCardBean) summaryCard);   
            } else {
                setSummaryCard(new TwitterSummaryCardBean(summaryCard));   
            }
            TwitterPhotoCard photoCard = stub.getPhotoCard();
            if(photoCard instanceof TwitterPhotoCardBean) {
                setPhotoCard((TwitterPhotoCardBean) photoCard);   
            } else {
                setPhotoCard(new TwitterPhotoCardBean(photoCard));   
            }
            TwitterGalleryCard galleryCard = stub.getGalleryCard();
            if(galleryCard instanceof TwitterGalleryCardBean) {
                setGalleryCard((TwitterGalleryCardBean) galleryCard);   
            } else {
                setGalleryCard(new TwitterGalleryCardBean(galleryCard));   
            }
            TwitterAppCard appCard = stub.getAppCard();
            if(appCard instanceof TwitterAppCardBean) {
                setAppCard((TwitterAppCardBean) appCard);   
            } else {
                setAppCard(new TwitterAppCardBean(appCard));   
            }
            TwitterPlayerCard playerCard = stub.getPlayerCard();
            if(playerCard instanceof TwitterPlayerCardBean) {
                setPlayerCard((TwitterPlayerCardBean) playerCard);   
            } else {
                setPlayerCard(new TwitterPlayerCardBean(playerCard));   
            }
            TwitterProductCard productCard = stub.getProductCard();
            if(productCard instanceof TwitterProductCardBean) {
                setProductCard((TwitterProductCardBean) productCard);   
            } else {
                setProductCard(new TwitterProductCardBean(productCard));   
            }
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    public String getCardType()
    {
        if(getStub() != null) {
            return getStub().getCardType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.cardType;
        }
    }
    public void setCardType(String cardType)
    {
        if(getStub() != null) {
            getStub().setCardType(cardType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.cardType = cardType;
        }
    }

    public TwitterSummaryCard getSummaryCard()
    {  
        if(getStub() != null) {
            // Note the object type.
            TwitterSummaryCard _stub_field = getStub().getSummaryCard();
            if(_stub_field == null) {
                return null;
            } else {
                return new TwitterSummaryCardBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.summaryCard;
        }
    }
    public void setSummaryCard(TwitterSummaryCard summaryCard)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setSummaryCard(summaryCard);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(summaryCard == null) {
                this.summaryCard = null;
            } else {
                if(summaryCard instanceof TwitterSummaryCardBean) {
                    this.summaryCard = (TwitterSummaryCardBean) summaryCard;
                } else {
                    this.summaryCard = new TwitterSummaryCardBean(summaryCard);
                }
            }
        }
    }

    public TwitterPhotoCard getPhotoCard()
    {  
        if(getStub() != null) {
            // Note the object type.
            TwitterPhotoCard _stub_field = getStub().getPhotoCard();
            if(_stub_field == null) {
                return null;
            } else {
                return new TwitterPhotoCardBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.photoCard;
        }
    }
    public void setPhotoCard(TwitterPhotoCard photoCard)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setPhotoCard(photoCard);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(photoCard == null) {
                this.photoCard = null;
            } else {
                if(photoCard instanceof TwitterPhotoCardBean) {
                    this.photoCard = (TwitterPhotoCardBean) photoCard;
                } else {
                    this.photoCard = new TwitterPhotoCardBean(photoCard);
                }
            }
        }
    }

    public TwitterGalleryCard getGalleryCard()
    {  
        if(getStub() != null) {
            // Note the object type.
            TwitterGalleryCard _stub_field = getStub().getGalleryCard();
            if(_stub_field == null) {
                return null;
            } else {
                return new TwitterGalleryCardBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.galleryCard;
        }
    }
    public void setGalleryCard(TwitterGalleryCard galleryCard)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setGalleryCard(galleryCard);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(galleryCard == null) {
                this.galleryCard = null;
            } else {
                if(galleryCard instanceof TwitterGalleryCardBean) {
                    this.galleryCard = (TwitterGalleryCardBean) galleryCard;
                } else {
                    this.galleryCard = new TwitterGalleryCardBean(galleryCard);
                }
            }
        }
    }

    public TwitterAppCard getAppCard()
    {  
        if(getStub() != null) {
            // Note the object type.
            TwitterAppCard _stub_field = getStub().getAppCard();
            if(_stub_field == null) {
                return null;
            } else {
                return new TwitterAppCardBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.appCard;
        }
    }
    public void setAppCard(TwitterAppCard appCard)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setAppCard(appCard);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(appCard == null) {
                this.appCard = null;
            } else {
                if(appCard instanceof TwitterAppCardBean) {
                    this.appCard = (TwitterAppCardBean) appCard;
                } else {
                    this.appCard = new TwitterAppCardBean(appCard);
                }
            }
        }
    }

    public TwitterPlayerCard getPlayerCard()
    {  
        if(getStub() != null) {
            // Note the object type.
            TwitterPlayerCard _stub_field = getStub().getPlayerCard();
            if(_stub_field == null) {
                return null;
            } else {
                return new TwitterPlayerCardBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.playerCard;
        }
    }
    public void setPlayerCard(TwitterPlayerCard playerCard)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setPlayerCard(playerCard);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(playerCard == null) {
                this.playerCard = null;
            } else {
                if(playerCard instanceof TwitterPlayerCardBean) {
                    this.playerCard = (TwitterPlayerCardBean) playerCard;
                } else {
                    this.playerCard = new TwitterPlayerCardBean(playerCard);
                }
            }
        }
    }

    public TwitterProductCard getProductCard()
    {  
        if(getStub() != null) {
            // Note the object type.
            TwitterProductCard _stub_field = getStub().getProductCard();
            if(_stub_field == null) {
                return null;
            } else {
                return new TwitterProductCardBean(_stub_field);
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.productCard;
        }
    }
    public void setProductCard(TwitterProductCard productCard)
    {
        if(getStub() != null) {
            // Note the object type.
            getStub().setProductCard(productCard);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            if(productCard == null) {
                this.productCard = null;
            } else {
                if(productCard instanceof TwitterProductCardBean) {
                    this.productCard = (TwitterProductCardBean) productCard;
                } else {
                    this.productCard = new TwitterProductCardBean(productCard);
                }
            }
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public TwitterCardMetaStub getStub()
    {
        return (TwitterCardMetaStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("cardType = " + this.cardType).append(";");
            sb.append("summaryCard = " + this.summaryCard).append(";");
            sb.append("photoCard = " + this.photoCard).append(";");
            sb.append("galleryCard = " + this.galleryCard).append(";");
            sb.append("appCard = " + this.appCard).append(";");
            sb.append("playerCard = " + this.playerCard).append(";");
            sb.append("productCard = " + this.productCard).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = cardType == null ? 0 : cardType.hashCode();
            _hash = 31 * _hash + delta;
            delta = summaryCard == null ? 0 : summaryCard.hashCode();
            _hash = 31 * _hash + delta;
            delta = photoCard == null ? 0 : photoCard.hashCode();
            _hash = 31 * _hash + delta;
            delta = galleryCard == null ? 0 : galleryCard.hashCode();
            _hash = 31 * _hash + delta;
            delta = appCard == null ? 0 : appCard.hashCode();
            _hash = 31 * _hash + delta;
            delta = playerCard == null ? 0 : playerCard.hashCode();
            _hash = 31 * _hash + delta;
            delta = productCard == null ? 0 : productCard.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
