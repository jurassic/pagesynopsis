package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.stub.AnchorStructStub;


// Wrapper class + bean combo.
public class AnchorStructBean implements AnchorStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(AnchorStructBean.class.getName());

    // [1] With an embedded object.
    private AnchorStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String id;
    private String name;
    private String href;
    private String hrefUrl;
    private String urlScheme;
    private String rel;
    private String target;
    private String innerHtml;
    private String anchorText;
    private String anchorTitle;
    private String anchorImage;
    private String anchorLegend;
    private String note;

    // Ctors.
    public AnchorStructBean()
    {
        //this((String) null);
    }
    public AnchorStructBean(String uuid, String id, String name, String href, String hrefUrl, String urlScheme, String rel, String target, String innerHtml, String anchorText, String anchorTitle, String anchorImage, String anchorLegend, String note)
    {
        this.uuid = uuid;
        this.id = id;
        this.name = name;
        this.href = href;
        this.hrefUrl = hrefUrl;
        this.urlScheme = urlScheme;
        this.rel = rel;
        this.target = target;
        this.innerHtml = innerHtml;
        this.anchorText = anchorText;
        this.anchorTitle = anchorTitle;
        this.anchorImage = anchorImage;
        this.anchorLegend = anchorLegend;
        this.note = note;
    }
    public AnchorStructBean(AnchorStruct stub)
    {
        if(stub instanceof AnchorStructStub) {
            this.stub = (AnchorStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setId(stub.getId());   
            setName(stub.getName());   
            setHref(stub.getHref());   
            setHrefUrl(stub.getHrefUrl());   
            setUrlScheme(stub.getUrlScheme());   
            setRel(stub.getRel());   
            setTarget(stub.getTarget());   
            setInnerHtml(stub.getInnerHtml());   
            setAnchorText(stub.getAnchorText());   
            setAnchorTitle(stub.getAnchorTitle());   
            setAnchorImage(stub.getAnchorImage());   
            setAnchorLegend(stub.getAnchorLegend());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getId()
    {
        if(getStub() != null) {
            return getStub().getId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.id;
        }
    }
    public void setId(String id)
    {
        if(getStub() != null) {
            getStub().setId(id);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.id = id;
        }
    }

    public String getName()
    {
        if(getStub() != null) {
            return getStub().getName();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.name;
        }
    }
    public void setName(String name)
    {
        if(getStub() != null) {
            getStub().setName(name);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.name = name;
        }
    }

    public String getHref()
    {
        if(getStub() != null) {
            return getStub().getHref();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.href;
        }
    }
    public void setHref(String href)
    {
        if(getStub() != null) {
            getStub().setHref(href);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.href = href;
        }
    }

    public String getHrefUrl()
    {
        if(getStub() != null) {
            return getStub().getHrefUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.hrefUrl;
        }
    }
    public void setHrefUrl(String hrefUrl)
    {
        if(getStub() != null) {
            getStub().setHrefUrl(hrefUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.hrefUrl = hrefUrl;
        }
    }

    public String getUrlScheme()
    {
        if(getStub() != null) {
            return getStub().getUrlScheme();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.urlScheme;
        }
    }
    public void setUrlScheme(String urlScheme)
    {
        if(getStub() != null) {
            getStub().setUrlScheme(urlScheme);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.urlScheme = urlScheme;
        }
    }

    public String getRel()
    {
        if(getStub() != null) {
            return getStub().getRel();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rel;
        }
    }
    public void setRel(String rel)
    {
        if(getStub() != null) {
            getStub().setRel(rel);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rel = rel;
        }
    }

    public String getTarget()
    {
        if(getStub() != null) {
            return getStub().getTarget();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.target;
        }
    }
    public void setTarget(String target)
    {
        if(getStub() != null) {
            getStub().setTarget(target);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.target = target;
        }
    }

    public String getInnerHtml()
    {
        if(getStub() != null) {
            return getStub().getInnerHtml();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.innerHtml;
        }
    }
    public void setInnerHtml(String innerHtml)
    {
        if(getStub() != null) {
            getStub().setInnerHtml(innerHtml);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.innerHtml = innerHtml;
        }
    }

    public String getAnchorText()
    {
        if(getStub() != null) {
            return getStub().getAnchorText();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.anchorText;
        }
    }
    public void setAnchorText(String anchorText)
    {
        if(getStub() != null) {
            getStub().setAnchorText(anchorText);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.anchorText = anchorText;
        }
    }

    public String getAnchorTitle()
    {
        if(getStub() != null) {
            return getStub().getAnchorTitle();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.anchorTitle;
        }
    }
    public void setAnchorTitle(String anchorTitle)
    {
        if(getStub() != null) {
            getStub().setAnchorTitle(anchorTitle);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.anchorTitle = anchorTitle;
        }
    }

    public String getAnchorImage()
    {
        if(getStub() != null) {
            return getStub().getAnchorImage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.anchorImage;
        }
    }
    public void setAnchorImage(String anchorImage)
    {
        if(getStub() != null) {
            getStub().setAnchorImage(anchorImage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.anchorImage = anchorImage;
        }
    }

    public String getAnchorLegend()
    {
        if(getStub() != null) {
            return getStub().getAnchorLegend();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.anchorLegend;
        }
    }
    public void setAnchorLegend(String anchorLegend)
    {
        if(getStub() != null) {
            getStub().setAnchorLegend(anchorLegend);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.anchorLegend = anchorLegend;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHref() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHrefUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getUrlScheme() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRel() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTarget() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getInnerHtml() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorText() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorTitle() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorImage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAnchorLegend() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public AnchorStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(AnchorStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("id = " + this.id).append(";");
            sb.append("name = " + this.name).append(";");
            sb.append("href = " + this.href).append(";");
            sb.append("hrefUrl = " + this.hrefUrl).append(";");
            sb.append("urlScheme = " + this.urlScheme).append(";");
            sb.append("rel = " + this.rel).append(";");
            sb.append("target = " + this.target).append(";");
            sb.append("innerHtml = " + this.innerHtml).append(";");
            sb.append("anchorText = " + this.anchorText).append(";");
            sb.append("anchorTitle = " + this.anchorTitle).append(";");
            sb.append("anchorImage = " + this.anchorImage).append(";");
            sb.append("anchorLegend = " + this.anchorLegend).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = id == null ? 0 : id.hashCode();
            _hash = 31 * _hash + delta;
            delta = name == null ? 0 : name.hashCode();
            _hash = 31 * _hash + delta;
            delta = href == null ? 0 : href.hashCode();
            _hash = 31 * _hash + delta;
            delta = hrefUrl == null ? 0 : hrefUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = urlScheme == null ? 0 : urlScheme.hashCode();
            _hash = 31 * _hash + delta;
            delta = rel == null ? 0 : rel.hashCode();
            _hash = 31 * _hash + delta;
            delta = target == null ? 0 : target.hashCode();
            _hash = 31 * _hash + delta;
            delta = innerHtml == null ? 0 : innerHtml.hashCode();
            _hash = 31 * _hash + delta;
            delta = anchorText == null ? 0 : anchorText.hashCode();
            _hash = 31 * _hash + delta;
            delta = anchorTitle == null ? 0 : anchorTitle.hashCode();
            _hash = 31 * _hash + delta;
            delta = anchorImage == null ? 0 : anchorImage.hashCode();
            _hash = 31 * _hash + delta;
            delta = anchorLegend == null ? 0 : anchorLegend.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
