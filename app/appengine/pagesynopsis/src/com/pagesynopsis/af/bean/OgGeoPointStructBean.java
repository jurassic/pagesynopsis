package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.ws.stub.OgGeoPointStructStub;


// Wrapper class + bean combo.
public class OgGeoPointStructBean implements OgGeoPointStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgGeoPointStructBean.class.getName());

    // [1] With an embedded object.
    private OgGeoPointStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private Float latitude;
    private Float longitude;
    private Float altitude;

    // Ctors.
    public OgGeoPointStructBean()
    {
        //this((String) null);
    }
    public OgGeoPointStructBean(String uuid, Float latitude, Float longitude, Float altitude)
    {
        this.uuid = uuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }
    public OgGeoPointStructBean(OgGeoPointStruct stub)
    {
        if(stub instanceof OgGeoPointStructStub) {
            this.stub = (OgGeoPointStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setLatitude(stub.getLatitude());   
            setLongitude(stub.getLongitude());   
            setAltitude(stub.getAltitude());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public Float getLatitude()
    {
        if(getStub() != null) {
            return getStub().getLatitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.latitude;
        }
    }
    public void setLatitude(Float latitude)
    {
        if(getStub() != null) {
            getStub().setLatitude(latitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.latitude = latitude;
        }
    }

    public Float getLongitude()
    {
        if(getStub() != null) {
            return getStub().getLongitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.longitude;
        }
    }
    public void setLongitude(Float longitude)
    {
        if(getStub() != null) {
            getStub().setLongitude(longitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.longitude = longitude;
        }
    }

    public Float getAltitude()
    {
        if(getStub() != null) {
            return getStub().getAltitude();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.altitude;
        }
    }
    public void setAltitude(Float altitude)
    {
        if(getStub() != null) {
            getStub().setAltitude(altitude);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.altitude = altitude;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getLatitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getLongitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAltitude() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public OgGeoPointStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(OgGeoPointStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("latitude = " + this.latitude).append(";");
            sb.append("longitude = " + this.longitude).append(";");
            sb.append("altitude = " + this.altitude).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = latitude == null ? 0 : latitude.hashCode();
            _hash = 31 * _hash + delta;
            delta = longitude == null ? 0 : longitude.hashCode();
            _hash = 31 * _hash + delta;
            delta = altitude == null ? 0 : altitude.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
