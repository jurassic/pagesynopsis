package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.stub.ImageStructStub;


// Wrapper class + bean combo.
public class ImageStructBean implements ImageStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ImageStructBean.class.getName());

    // [1] With an embedded object.
    private ImageStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String id;
    private String alt;
    private String src;
    private String srcUrl;
    private String mediaType;
    private String widthAttr;
    private Integer width;
    private String heightAttr;
    private Integer height;
    private String note;

    // Ctors.
    public ImageStructBean()
    {
        //this((String) null);
    }
    public ImageStructBean(String uuid, String id, String alt, String src, String srcUrl, String mediaType, String widthAttr, Integer width, String heightAttr, Integer height, String note)
    {
        this.uuid = uuid;
        this.id = id;
        this.alt = alt;
        this.src = src;
        this.srcUrl = srcUrl;
        this.mediaType = mediaType;
        this.widthAttr = widthAttr;
        this.width = width;
        this.heightAttr = heightAttr;
        this.height = height;
        this.note = note;
    }
    public ImageStructBean(ImageStruct stub)
    {
        if(stub instanceof ImageStructStub) {
            this.stub = (ImageStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setId(stub.getId());   
            setAlt(stub.getAlt());   
            setSrc(stub.getSrc());   
            setSrcUrl(stub.getSrcUrl());   
            setMediaType(stub.getMediaType());   
            setWidthAttr(stub.getWidthAttr());   
            setWidth(stub.getWidth());   
            setHeightAttr(stub.getHeightAttr());   
            setHeight(stub.getHeight());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getId()
    {
        if(getStub() != null) {
            return getStub().getId();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.id;
        }
    }
    public void setId(String id)
    {
        if(getStub() != null) {
            getStub().setId(id);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.id = id;
        }
    }

    public String getAlt()
    {
        if(getStub() != null) {
            return getStub().getAlt();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.alt;
        }
    }
    public void setAlt(String alt)
    {
        if(getStub() != null) {
            getStub().setAlt(alt);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.alt = alt;
        }
    }

    public String getSrc()
    {
        if(getStub() != null) {
            return getStub().getSrc();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.src;
        }
    }
    public void setSrc(String src)
    {
        if(getStub() != null) {
            getStub().setSrc(src);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.src = src;
        }
    }

    public String getSrcUrl()
    {
        if(getStub() != null) {
            return getStub().getSrcUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.srcUrl;
        }
    }
    public void setSrcUrl(String srcUrl)
    {
        if(getStub() != null) {
            getStub().setSrcUrl(srcUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.srcUrl = srcUrl;
        }
    }

    public String getMediaType()
    {
        if(getStub() != null) {
            return getStub().getMediaType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.mediaType;
        }
    }
    public void setMediaType(String mediaType)
    {
        if(getStub() != null) {
            getStub().setMediaType(mediaType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.mediaType = mediaType;
        }
    }

    public String getWidthAttr()
    {
        if(getStub() != null) {
            return getStub().getWidthAttr();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.widthAttr;
        }
    }
    public void setWidthAttr(String widthAttr)
    {
        if(getStub() != null) {
            getStub().setWidthAttr(widthAttr);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.widthAttr = widthAttr;
        }
    }

    public Integer getWidth()
    {
        if(getStub() != null) {
            return getStub().getWidth();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.width;
        }
    }
    public void setWidth(Integer width)
    {
        if(getStub() != null) {
            getStub().setWidth(width);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.width = width;
        }
    }

    public String getHeightAttr()
    {
        if(getStub() != null) {
            return getStub().getHeightAttr();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.heightAttr;
        }
    }
    public void setHeightAttr(String heightAttr)
    {
        if(getStub() != null) {
            getStub().setHeightAttr(heightAttr);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.heightAttr = heightAttr;
        }
    }

    public Integer getHeight()
    {
        if(getStub() != null) {
            return getStub().getHeight();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.height;
        }
    }
    public void setHeight(Integer height)
    {
        if(getStub() != null) {
            getStub().setHeight(height);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.height = height;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getId() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getAlt() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getMediaType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidthAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getWidth() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeightAttr() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHeight() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public ImageStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(ImageStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("id = " + this.id).append(";");
            sb.append("alt = " + this.alt).append(";");
            sb.append("src = " + this.src).append(";");
            sb.append("srcUrl = " + this.srcUrl).append(";");
            sb.append("mediaType = " + this.mediaType).append(";");
            sb.append("widthAttr = " + this.widthAttr).append(";");
            sb.append("width = " + this.width).append(";");
            sb.append("heightAttr = " + this.heightAttr).append(";");
            sb.append("height = " + this.height).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = id == null ? 0 : id.hashCode();
            _hash = 31 * _hash + delta;
            delta = alt == null ? 0 : alt.hashCode();
            _hash = 31 * _hash + delta;
            delta = src == null ? 0 : src.hashCode();
            _hash = 31 * _hash + delta;
            delta = srcUrl == null ? 0 : srcUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = mediaType == null ? 0 : mediaType.hashCode();
            _hash = 31 * _hash + delta;
            delta = widthAttr == null ? 0 : widthAttr.hashCode();
            _hash = 31 * _hash + delta;
            delta = width == null ? 0 : width.hashCode();
            _hash = 31 * _hash + delta;
            delta = heightAttr == null ? 0 : heightAttr.hashCode();
            _hash = 31 * _hash + delta;
            delta = height == null ? 0 : height.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
