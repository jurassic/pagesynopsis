package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.stub.PageBaseStub;
import com.pagesynopsis.ws.stub.PageFetchStub;


// Wrapper class + bean combo.
public class PageFetchBean extends PageBaseBean implements PageFetch, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(PageFetchBean.class.getName());


    // [2] Or, without an embedded object.
    private Integer inputMaxRedirects;
    private Integer resultRedirectCount;
    private List<UrlStruct> redirectPages;
    private String destinationUrl;
    private String pageAuthor;
    private String pageSummary;
    private String favicon;
    private String faviconUrl;

    // Ctors.
    public PageFetchBean()
    {
        //this((String) null);
    }
    public PageFetchBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
    public PageFetchBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl)
    {
        this(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl, null, null);
    }
    public PageFetchBean(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl, Long createdTime, Long modifiedTime)
    {
        super(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, createdTime, modifiedTime);

        this.inputMaxRedirects = inputMaxRedirects;
        this.resultRedirectCount = resultRedirectCount;
        this.redirectPages = redirectPages;
        this.destinationUrl = destinationUrl;
        this.pageAuthor = pageAuthor;
        this.pageSummary = pageSummary;
        this.favicon = favicon;
        this.faviconUrl = faviconUrl;
    }
    public PageFetchBean(PageFetch stub)
    {
        if(stub instanceof PageFetchStub) {
            super.setStub((PageBaseStub) stub);
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setUser(stub.getUser());   
            setFetchRequest(stub.getFetchRequest());   
            setTargetUrl(stub.getTargetUrl());   
            setPageUrl(stub.getPageUrl());   
            setQueryString(stub.getQueryString());   
            setQueryParams(stub.getQueryParams());   
            setLastFetchResult(stub.getLastFetchResult());   
            setResponseCode(stub.getResponseCode());   
            setContentType(stub.getContentType());   
            setContentLength(stub.getContentLength());   
            setLanguage(stub.getLanguage());   
            setRedirect(stub.getRedirect());   
            setLocation(stub.getLocation());   
            setPageTitle(stub.getPageTitle());   
            setNote(stub.getNote());   
            setDeferred(stub.isDeferred());   
            setStatus(stub.getStatus());   
            setRefreshStatus(stub.getRefreshStatus());   
            setRefreshInterval(stub.getRefreshInterval());   
            setNextRefreshTime(stub.getNextRefreshTime());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setLastUpdatedTime(stub.getLastUpdatedTime());   
            setInputMaxRedirects(stub.getInputMaxRedirects());   
            setResultRedirectCount(stub.getResultRedirectCount());   
            setRedirectPages(stub.getRedirectPages());   
            setDestinationUrl(stub.getDestinationUrl());   
            setPageAuthor(stub.getPageAuthor());   
            setPageSummary(stub.getPageSummary());   
            setFavicon(stub.getFavicon());   
            setFaviconUrl(stub.getFaviconUrl());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    public String getUser()
    {
        return super.getUser();
    }
    public void setUser(String user)
    {
        super.setUser(user);
    }

    public String getFetchRequest()
    {
        return super.getFetchRequest();
    }
    public void setFetchRequest(String fetchRequest)
    {
        super.setFetchRequest(fetchRequest);
    }

    public String getTargetUrl()
    {
        return super.getTargetUrl();
    }
    public void setTargetUrl(String targetUrl)
    {
        super.setTargetUrl(targetUrl);
    }

    public String getPageUrl()
    {
        return super.getPageUrl();
    }
    public void setPageUrl(String pageUrl)
    {
        super.setPageUrl(pageUrl);
    }

    public String getQueryString()
    {
        return super.getQueryString();
    }
    public void setQueryString(String queryString)
    {
        super.setQueryString(queryString);
    }

    public List<KeyValuePairStruct> getQueryParams()
    {
        return super.getQueryParams();
    }
    public void setQueryParams(List<KeyValuePairStruct> queryParams)
    {
        super.setQueryParams(queryParams);
    }

    public String getLastFetchResult()
    {
        return super.getLastFetchResult();
    }
    public void setLastFetchResult(String lastFetchResult)
    {
        super.setLastFetchResult(lastFetchResult);
    }

    public Integer getResponseCode()
    {
        return super.getResponseCode();
    }
    public void setResponseCode(Integer responseCode)
    {
        super.setResponseCode(responseCode);
    }

    public String getContentType()
    {
        return super.getContentType();
    }
    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
    }

    public Integer getContentLength()
    {
        return super.getContentLength();
    }
    public void setContentLength(Integer contentLength)
    {
        super.setContentLength(contentLength);
    }

    public String getLanguage()
    {
        return super.getLanguage();
    }
    public void setLanguage(String language)
    {
        super.setLanguage(language);
    }

    public String getRedirect()
    {
        return super.getRedirect();
    }
    public void setRedirect(String redirect)
    {
        super.setRedirect(redirect);
    }

    public String getLocation()
    {
        return super.getLocation();
    }
    public void setLocation(String location)
    {
        super.setLocation(location);
    }

    public String getPageTitle()
    {
        return super.getPageTitle();
    }
    public void setPageTitle(String pageTitle)
    {
        super.setPageTitle(pageTitle);
    }

    public String getNote()
    {
        return super.getNote();
    }
    public void setNote(String note)
    {
        super.setNote(note);
    }

    public Boolean isDeferred()
    {
        return super.isDeferred();
    }
    public void setDeferred(Boolean deferred)
    {
        super.setDeferred(deferred);
    }

    public String getStatus()
    {
        return super.getStatus();
    }
    public void setStatus(String status)
    {
        super.setStatus(status);
    }

    public Integer getRefreshStatus()
    {
        return super.getRefreshStatus();
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        super.setRefreshStatus(refreshStatus);
    }

    public Long getRefreshInterval()
    {
        return super.getRefreshInterval();
    }
    public void setRefreshInterval(Long refreshInterval)
    {
        super.setRefreshInterval(refreshInterval);
    }

    public Long getNextRefreshTime()
    {
        return super.getNextRefreshTime();
    }
    public void setNextRefreshTime(Long nextRefreshTime)
    {
        super.setNextRefreshTime(nextRefreshTime);
    }

    public Long getLastCheckedTime()
    {
        return super.getLastCheckedTime();
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        super.setLastCheckedTime(lastCheckedTime);
    }

    public Long getLastUpdatedTime()
    {
        return super.getLastUpdatedTime();
    }
    public void setLastUpdatedTime(Long lastUpdatedTime)
    {
        super.setLastUpdatedTime(lastUpdatedTime);
    }

    public Integer getInputMaxRedirects()
    {
        if(getStub() != null) {
            return getStub().getInputMaxRedirects();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.inputMaxRedirects;
        }
    }
    public void setInputMaxRedirects(Integer inputMaxRedirects)
    {
        if(getStub() != null) {
            getStub().setInputMaxRedirects(inputMaxRedirects);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.inputMaxRedirects = inputMaxRedirects;
        }
    }

    public Integer getResultRedirectCount()
    {
        if(getStub() != null) {
            return getStub().getResultRedirectCount();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.resultRedirectCount;
        }
    }
    public void setResultRedirectCount(Integer resultRedirectCount)
    {
        if(getStub() != null) {
            getStub().setResultRedirectCount(resultRedirectCount);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.resultRedirectCount = resultRedirectCount;
        }
    }

    public List<UrlStruct> getRedirectPages()
    {
        if(getStub() != null) {
            List<UrlStruct> list = getStub().getRedirectPages();
            if(list != null) {
                List<UrlStruct> bean = new ArrayList<UrlStruct>();
                for(UrlStruct urlStruct : list) {
                    UrlStructBean elem = null;
                    if(urlStruct instanceof UrlStructBean) {
                        elem = (UrlStructBean) urlStruct;
                    } else if(urlStruct instanceof UrlStructStub) {
                        elem = new UrlStructBean(urlStruct);
                    }
                    if(elem != null) {
                        bean.add(elem);
                    }
                }
                return bean;
            } else {
                return null;
            }
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.redirectPages;
        }
    }
    public void setRedirectPages(List<UrlStruct> redirectPages)
    {
        if(redirectPages != null) {
            if(getStub() != null) {
                List<UrlStruct> stub = new ArrayList<UrlStruct>();
                for(UrlStruct urlStruct : redirectPages) {
                    UrlStructStub elem = null;
                    if(urlStruct instanceof UrlStructStub) {
                        elem = (UrlStructStub) urlStruct;
                    } else if(urlStruct instanceof UrlStructBean) {
                        elem = ((UrlStructBean) urlStruct).getStub();
                    } else if(urlStruct instanceof UrlStruct) {
                        elem = new UrlStructStub(urlStruct);
                    }
                    if(elem != null) {
                        stub.add(elem);
                    }
                }
                getStub().setRedirectPages(stub);
            } else {
                // Can this happen? Log it.
                log.info("Embedded object is null!");
                //this.redirectPages = redirectPages;  // ???

                List<UrlStruct> beans = new ArrayList<UrlStruct>();
                for(UrlStruct urlStruct : redirectPages) {
                    UrlStructBean elem = null;
                    if(urlStruct != null) {
                        if(urlStruct instanceof UrlStructBean) {
                            elem = (UrlStructBean) urlStruct;
                        } else {
                            elem = new UrlStructBean(urlStruct);
                        }
                    }
                    if(elem != null) {
                        beans.add(elem);
                    }
                }
                this.redirectPages = beans;
            }
        } else {
            this.redirectPages = null;
        }
    }

    public String getDestinationUrl()
    {
        if(getStub() != null) {
            return getStub().getDestinationUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.destinationUrl;
        }
    }
    public void setDestinationUrl(String destinationUrl)
    {
        if(getStub() != null) {
            getStub().setDestinationUrl(destinationUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.destinationUrl = destinationUrl;
        }
    }

    public String getPageAuthor()
    {
        if(getStub() != null) {
            return getStub().getPageAuthor();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageAuthor;
        }
    }
    public void setPageAuthor(String pageAuthor)
    {
        if(getStub() != null) {
            getStub().setPageAuthor(pageAuthor);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageAuthor = pageAuthor;
        }
    }

    public String getPageSummary()
    {
        if(getStub() != null) {
            return getStub().getPageSummary();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageSummary;
        }
    }
    public void setPageSummary(String pageSummary)
    {
        if(getStub() != null) {
            getStub().setPageSummary(pageSummary);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageSummary = pageSummary;
        }
    }

    public String getFavicon()
    {
        if(getStub() != null) {
            return getStub().getFavicon();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.favicon;
        }
    }
    public void setFavicon(String favicon)
    {
        if(getStub() != null) {
            getStub().setFavicon(favicon);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.favicon = favicon;
        }
    }

    public String getFaviconUrl()
    {
        if(getStub() != null) {
            return getStub().getFaviconUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.faviconUrl;
        }
    }
    public void setFaviconUrl(String faviconUrl)
    {
        if(getStub() != null) {
            getStub().setFaviconUrl(faviconUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.faviconUrl = faviconUrl;
        }
    }

    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    // Returns the reference to the embedded object. (Could be null.)
    public PageFetchStub getStub()
    {
        return (PageFetchStub) super.getStub();
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder( super.toString() );
            sb.append("inputMaxRedirects = " + this.inputMaxRedirects).append(";");
            sb.append("resultRedirectCount = " + this.resultRedirectCount).append(";");
            sb.append("redirectPages = " + this.redirectPages).append(";");
            sb.append("destinationUrl = " + this.destinationUrl).append(";");
            sb.append("pageAuthor = " + this.pageAuthor).append(";");
            sb.append("pageSummary = " + this.pageSummary).append(";");
            sb.append("favicon = " + this.favicon).append(";");
            sb.append("faviconUrl = " + this.faviconUrl).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = inputMaxRedirects == null ? 0 : inputMaxRedirects.hashCode();
            _hash = 31 * _hash + delta;
            delta = resultRedirectCount == null ? 0 : resultRedirectCount.hashCode();
            _hash = 31 * _hash + delta;
            delta = redirectPages == null ? 0 : redirectPages.hashCode();
            _hash = 31 * _hash + delta;
            delta = destinationUrl == null ? 0 : destinationUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageAuthor == null ? 0 : pageAuthor.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageSummary == null ? 0 : pageSummary.hashCode();
            _hash = 31 * _hash + delta;
            delta = favicon == null ? 0 : favicon.hashCode();
            _hash = 31 * _hash + delta;
            delta = faviconUrl == null ? 0 : faviconUrl.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
