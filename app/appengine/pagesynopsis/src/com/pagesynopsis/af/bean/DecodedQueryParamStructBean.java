package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.ws.stub.DecodedQueryParamStructStub;


// Wrapper class + bean combo.
public class DecodedQueryParamStructBean implements DecodedQueryParamStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(DecodedQueryParamStructBean.class.getName());

    // [1] With an embedded object.
    private DecodedQueryParamStructStub stub = null;

    // [2] Or, without an embedded object.
    private String paramType;
    private String originalString;
    private String decodedString;
    private String note;

    // Ctors.
    public DecodedQueryParamStructBean()
    {
        //this((String) null);
    }
    public DecodedQueryParamStructBean(String paramType, String originalString, String decodedString, String note)
    {
        this.paramType = paramType;
        this.originalString = originalString;
        this.decodedString = decodedString;
        this.note = note;
    }
    public DecodedQueryParamStructBean(DecodedQueryParamStruct stub)
    {
        if(stub instanceof DecodedQueryParamStructStub) {
            this.stub = (DecodedQueryParamStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setParamType(stub.getParamType());   
            setOriginalString(stub.getOriginalString());   
            setDecodedString(stub.getDecodedString());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getParamType()
    {
        if(getStub() != null) {
            return getStub().getParamType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.paramType;
        }
    }
    public void setParamType(String paramType)
    {
        if(getStub() != null) {
            getStub().setParamType(paramType);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.paramType = paramType;
        }
    }

    public String getOriginalString()
    {
        if(getStub() != null) {
            return getStub().getOriginalString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.originalString;
        }
    }
    public void setOriginalString(String originalString)
    {
        if(getStub() != null) {
            getStub().setOriginalString(originalString);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.originalString = originalString;
        }
    }

    public String getDecodedString()
    {
        if(getStub() != null) {
            return getStub().getDecodedString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.decodedString;
        }
    }
    public void setDecodedString(String decodedString)
    {
        if(getStub() != null) {
            getStub().setDecodedString(decodedString);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.decodedString = decodedString;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getParamType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getOriginalString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getDecodedString() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public DecodedQueryParamStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(DecodedQueryParamStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("paramType = " + this.paramType).append(";");
            sb.append("originalString = " + this.originalString).append(";");
            sb.append("decodedString = " + this.decodedString).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = paramType == null ? 0 : paramType.hashCode();
            _hash = 31 * _hash + delta;
            delta = originalString == null ? 0 : originalString.hashCode();
            _hash = 31 * _hash + delta;
            delta = decodedString == null ? 0 : decodedString.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
