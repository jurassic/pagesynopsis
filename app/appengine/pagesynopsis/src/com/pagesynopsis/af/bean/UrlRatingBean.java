package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.stub.UrlRatingStub;


// Wrapper class + bean combo.
public class UrlRatingBean implements UrlRating, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UrlRatingBean.class.getName());

    // [1] With an embedded object.
    private UrlRatingStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String domain;
    private String pageUrl;
    private String preview;
    private String flag;
    private Double rating;
    private String note;
    private String status;
    private Long ratedTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public UrlRatingBean()
    {
        //this((String) null);
    }
    public UrlRatingBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null);
    }
    public UrlRatingBean(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime)
    {
        this(guid, domain, pageUrl, preview, flag, rating, note, status, ratedTime, null, null);
    }
    public UrlRatingBean(String guid, String domain, String pageUrl, String preview, String flag, Double rating, String note, String status, Long ratedTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.domain = domain;
        this.pageUrl = pageUrl;
        this.preview = preview;
        this.flag = flag;
        this.rating = rating;
        this.note = note;
        this.status = status;
        this.ratedTime = ratedTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public UrlRatingBean(UrlRating stub)
    {
        if(stub instanceof UrlRatingStub) {
            this.stub = (UrlRatingStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setDomain(stub.getDomain());   
            setPageUrl(stub.getPageUrl());   
            setPreview(stub.getPreview());   
            setFlag(stub.getFlag());   
            setRating(stub.getRating());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setRatedTime(stub.getRatedTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getDomain()
    {
        if(getStub() != null) {
            return getStub().getDomain();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.domain;
        }
    }
    public void setDomain(String domain)
    {
        if(getStub() != null) {
            getStub().setDomain(domain);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.domain = domain;
        }
    }

    public String getPageUrl()
    {
        if(getStub() != null) {
            return getStub().getPageUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.pageUrl;
        }
    }
    public void setPageUrl(String pageUrl)
    {
        if(getStub() != null) {
            getStub().setPageUrl(pageUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.pageUrl = pageUrl;
        }
    }

    public String getPreview()
    {
        if(getStub() != null) {
            return getStub().getPreview();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.preview;
        }
    }
    public void setPreview(String preview)
    {
        if(getStub() != null) {
            getStub().setPreview(preview);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.preview = preview;
        }
    }

    public String getFlag()
    {
        if(getStub() != null) {
            return getStub().getFlag();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.flag;
        }
    }
    public void setFlag(String flag)
    {
        if(getStub() != null) {
            getStub().setFlag(flag);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.flag = flag;
        }
    }

    public Double getRating()
    {
        if(getStub() != null) {
            return getStub().getRating();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.rating;
        }
    }
    public void setRating(Double rating)
    {
        if(getStub() != null) {
            getStub().setRating(rating);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.rating = rating;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Long getRatedTime()
    {
        if(getStub() != null) {
            return getStub().getRatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.ratedTime;
        }
    }
    public void setRatedTime(Long ratedTime)
    {
        if(getStub() != null) {
            getStub().setRatedTime(ratedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.ratedTime = ratedTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public UrlRatingStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UrlRatingStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("domain = " + this.domain).append(";");
            sb.append("pageUrl = " + this.pageUrl).append(";");
            sb.append("preview = " + this.preview).append(";");
            sb.append("flag = " + this.flag).append(";");
            sb.append("rating = " + this.rating).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("ratedTime = " + this.ratedTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = domain == null ? 0 : domain.hashCode();
            _hash = 31 * _hash + delta;
            delta = pageUrl == null ? 0 : pageUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = preview == null ? 0 : preview.hashCode();
            _hash = 31 * _hash + delta;
            delta = flag == null ? 0 : flag.hashCode();
            _hash = 31 * _hash + delta;
            delta = rating == null ? 0 : rating.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = ratedTime == null ? 0 : ratedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
