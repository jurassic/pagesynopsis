package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.stub.RobotsTextRefreshStub;


// Wrapper class + bean combo.
public class RobotsTextRefreshBean implements RobotsTextRefresh, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RobotsTextRefreshBean.class.getName());

    // [1] With an embedded object.
    private RobotsTextRefreshStub stub = null;

    // [2] Or, without an embedded object.
    private String guid;
    private String robotsTextFile;
    private Integer refreshInterval;
    private String note;
    private String status;
    private Integer refreshStatus;
    private String result;
    private Long lastCheckedTime;
    private Long nextCheckedTime;
    private Long expirationTime;
    private Long createdTime;
    private Long modifiedTime;

    // Ctors.
    public RobotsTextRefreshBean()
    {
        //this((String) null);
    }
    public RobotsTextRefreshBean(String guid)
    {
        this(guid, null, null, null, null, null, null, null, null, null, null, null);
    }
    public RobotsTextRefreshBean(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime)
    {
        this(guid, robotsTextFile, refreshInterval, note, status, refreshStatus, result, lastCheckedTime, nextCheckedTime, expirationTime, null, null);
    }
    public RobotsTextRefreshBean(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime, Long createdTime, Long modifiedTime)
    {
        this.guid = guid;
        this.robotsTextFile = robotsTextFile;
        this.refreshInterval = refreshInterval;
        this.note = note;
        this.status = status;
        this.refreshStatus = refreshStatus;
        this.result = result;
        this.lastCheckedTime = lastCheckedTime;
        this.nextCheckedTime = nextCheckedTime;
        this.expirationTime = expirationTime;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }
    public RobotsTextRefreshBean(RobotsTextRefresh stub)
    {
        if(stub instanceof RobotsTextRefreshStub) {
            this.stub = (RobotsTextRefreshStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setGuid(stub.getGuid());   
            setRobotsTextFile(stub.getRobotsTextFile());   
            setRefreshInterval(stub.getRefreshInterval());   
            setNote(stub.getNote());   
            setStatus(stub.getStatus());   
            setRefreshStatus(stub.getRefreshStatus());   
            setResult(stub.getResult());   
            setLastCheckedTime(stub.getLastCheckedTime());   
            setNextCheckedTime(stub.getNextCheckedTime());   
            setExpirationTime(stub.getExpirationTime());   
            setCreatedTime(stub.getCreatedTime());   
            setModifiedTime(stub.getModifiedTime());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getGuid()
    {
        if(getStub() != null) {
            return getStub().getGuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.guid;
        }
    }
    public void setGuid(String guid)
    {
        if(getStub() != null) {
            getStub().setGuid(guid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.guid = guid;
        }
    }

    public String getRobotsTextFile()
    {
        if(getStub() != null) {
            return getStub().getRobotsTextFile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.robotsTextFile;
        }
    }
    public void setRobotsTextFile(String robotsTextFile)
    {
        if(getStub() != null) {
            getStub().setRobotsTextFile(robotsTextFile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.robotsTextFile = robotsTextFile;
        }
    }

    public Integer getRefreshInterval()
    {
        if(getStub() != null) {
            return getStub().getRefreshInterval();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshInterval;
        }
    }
    public void setRefreshInterval(Integer refreshInterval)
    {
        if(getStub() != null) {
            getStub().setRefreshInterval(refreshInterval);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshInterval = refreshInterval;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }

    public String getStatus()
    {
        if(getStub() != null) {
            return getStub().getStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.status;
        }
    }
    public void setStatus(String status)
    {
        if(getStub() != null) {
            getStub().setStatus(status);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.status = status;
        }
    }

    public Integer getRefreshStatus()
    {
        if(getStub() != null) {
            return getStub().getRefreshStatus();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.refreshStatus;
        }
    }
    public void setRefreshStatus(Integer refreshStatus)
    {
        if(getStub() != null) {
            getStub().setRefreshStatus(refreshStatus);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.refreshStatus = refreshStatus;
        }
    }

    public String getResult()
    {
        if(getStub() != null) {
            return getStub().getResult();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.result;
        }
    }
    public void setResult(String result)
    {
        if(getStub() != null) {
            getStub().setResult(result);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.result = result;
        }
    }

    public Long getLastCheckedTime()
    {
        if(getStub() != null) {
            return getStub().getLastCheckedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.lastCheckedTime;
        }
    }
    public void setLastCheckedTime(Long lastCheckedTime)
    {
        if(getStub() != null) {
            getStub().setLastCheckedTime(lastCheckedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.lastCheckedTime = lastCheckedTime;
        }
    }

    public Long getNextCheckedTime()
    {
        if(getStub() != null) {
            return getStub().getNextCheckedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.nextCheckedTime;
        }
    }
    public void setNextCheckedTime(Long nextCheckedTime)
    {
        if(getStub() != null) {
            getStub().setNextCheckedTime(nextCheckedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.nextCheckedTime = nextCheckedTime;
        }
    }

    public Long getExpirationTime()
    {
        if(getStub() != null) {
            return getStub().getExpirationTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.expirationTime;
        }
    }
    public void setExpirationTime(Long expirationTime)
    {
        if(getStub() != null) {
            getStub().setExpirationTime(expirationTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.expirationTime = expirationTime;
        }
    }

    public Long getCreatedTime()
    {
        if(getStub() != null) {
            return getStub().getCreatedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.createdTime;
        }
    }
    public void setCreatedTime(Long createdTime)
    {
        if(getStub() != null) {
            getStub().setCreatedTime(createdTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.createdTime = createdTime;
        }
    }

    public Long getModifiedTime()
    {
        if(getStub() != null) {
            return getStub().getModifiedTime();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.modifiedTime;
        }
    }
    public void setModifiedTime(Long modifiedTime)
    {
        if(getStub() != null) {
            getStub().setModifiedTime(modifiedTime);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.modifiedTime = modifiedTime;
        }
    }


    // Returns the reference to the embedded object. (Could be null.)
    public RobotsTextRefreshStub getStub()
    {
        return this.stub;
    }
    protected void setStub(RobotsTextRefreshStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("guid = " + this.guid).append(";");
            sb.append("robotsTextFile = " + this.robotsTextFile).append(";");
            sb.append("refreshInterval = " + this.refreshInterval).append(";");
            sb.append("note = " + this.note).append(";");
            sb.append("status = " + this.status).append(";");
            sb.append("refreshStatus = " + this.refreshStatus).append(";");
            sb.append("result = " + this.result).append(";");
            sb.append("lastCheckedTime = " + this.lastCheckedTime).append(";");
            sb.append("nextCheckedTime = " + this.nextCheckedTime).append(";");
            sb.append("expirationTime = " + this.expirationTime).append(";");
            sb.append("createdTime = " + this.createdTime).append(";");
            sb.append("modifiedTime = " + this.modifiedTime).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = guid == null ? 0 : guid.hashCode();
            _hash = 31 * _hash + delta;
            delta = robotsTextFile == null ? 0 : robotsTextFile.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshInterval == null ? 0 : refreshInterval.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            delta = status == null ? 0 : status.hashCode();
            _hash = 31 * _hash + delta;
            delta = refreshStatus == null ? 0 : refreshStatus.hashCode();
            _hash = 31 * _hash + delta;
            delta = result == null ? 0 : result.hashCode();
            _hash = 31 * _hash + delta;
            delta = lastCheckedTime == null ? 0 : lastCheckedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = nextCheckedTime == null ? 0 : nextCheckedTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = expirationTime == null ? 0 : expirationTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = createdTime == null ? 0 : createdTime.hashCode();
            _hash = 31 * _hash + delta;
            delta = modifiedTime == null ? 0 : modifiedTime.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
