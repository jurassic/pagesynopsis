package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.UserWebsiteStruct;
import com.pagesynopsis.ws.stub.UserWebsiteStructStub;


// Wrapper class + bean combo.
public class UserWebsiteStructBean implements UserWebsiteStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserWebsiteStructBean.class.getName());

    // [1] With an embedded object.
    private UserWebsiteStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String primarySite;
    private String homePage;
    private String blogSite;
    private String portfolioSite;
    private String twitterProfile;
    private String facebookProfile;
    private String googlePlusProfile;
    private String note;

    // Ctors.
    public UserWebsiteStructBean()
    {
        //this((String) null);
    }
    public UserWebsiteStructBean(String uuid, String primarySite, String homePage, String blogSite, String portfolioSite, String twitterProfile, String facebookProfile, String googlePlusProfile, String note)
    {
        this.uuid = uuid;
        this.primarySite = primarySite;
        this.homePage = homePage;
        this.blogSite = blogSite;
        this.portfolioSite = portfolioSite;
        this.twitterProfile = twitterProfile;
        this.facebookProfile = facebookProfile;
        this.googlePlusProfile = googlePlusProfile;
        this.note = note;
    }
    public UserWebsiteStructBean(UserWebsiteStruct stub)
    {
        if(stub instanceof UserWebsiteStructStub) {
            this.stub = (UserWebsiteStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setPrimarySite(stub.getPrimarySite());   
            setHomePage(stub.getHomePage());   
            setBlogSite(stub.getBlogSite());   
            setPortfolioSite(stub.getPortfolioSite());   
            setTwitterProfile(stub.getTwitterProfile());   
            setFacebookProfile(stub.getFacebookProfile());   
            setGooglePlusProfile(stub.getGooglePlusProfile());   
            setNote(stub.getNote());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getPrimarySite()
    {
        if(getStub() != null) {
            return getStub().getPrimarySite();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.primarySite;
        }
    }
    public void setPrimarySite(String primarySite)
    {
        if(getStub() != null) {
            getStub().setPrimarySite(primarySite);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.primarySite = primarySite;
        }
    }

    public String getHomePage()
    {
        if(getStub() != null) {
            return getStub().getHomePage();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.homePage;
        }
    }
    public void setHomePage(String homePage)
    {
        if(getStub() != null) {
            getStub().setHomePage(homePage);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.homePage = homePage;
        }
    }

    public String getBlogSite()
    {
        if(getStub() != null) {
            return getStub().getBlogSite();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.blogSite;
        }
    }
    public void setBlogSite(String blogSite)
    {
        if(getStub() != null) {
            getStub().setBlogSite(blogSite);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.blogSite = blogSite;
        }
    }

    public String getPortfolioSite()
    {
        if(getStub() != null) {
            return getStub().getPortfolioSite();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.portfolioSite;
        }
    }
    public void setPortfolioSite(String portfolioSite)
    {
        if(getStub() != null) {
            getStub().setPortfolioSite(portfolioSite);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.portfolioSite = portfolioSite;
        }
    }

    public String getTwitterProfile()
    {
        if(getStub() != null) {
            return getStub().getTwitterProfile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.twitterProfile;
        }
    }
    public void setTwitterProfile(String twitterProfile)
    {
        if(getStub() != null) {
            getStub().setTwitterProfile(twitterProfile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.twitterProfile = twitterProfile;
        }
    }

    public String getFacebookProfile()
    {
        if(getStub() != null) {
            return getStub().getFacebookProfile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.facebookProfile;
        }
    }
    public void setFacebookProfile(String facebookProfile)
    {
        if(getStub() != null) {
            getStub().setFacebookProfile(facebookProfile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.facebookProfile = facebookProfile;
        }
    }

    public String getGooglePlusProfile()
    {
        if(getStub() != null) {
            return getStub().getGooglePlusProfile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.googlePlusProfile;
        }
    }
    public void setGooglePlusProfile(String googlePlusProfile)
    {
        if(getStub() != null) {
            getStub().setGooglePlusProfile(googlePlusProfile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.googlePlusProfile = googlePlusProfile;
        }
    }

    public String getNote()
    {
        if(getStub() != null) {
            return getStub().getNote();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.note;
        }
    }
    public void setNote(String note)
    {
        if(getStub() != null) {
            getStub().setNote(note);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.note = note;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getPrimarySite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getHomePage() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getBlogSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getPortfolioSite() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getTwitterProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getFacebookProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getGooglePlusProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public UserWebsiteStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(UserWebsiteStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("primarySite = " + this.primarySite).append(";");
            sb.append("homePage = " + this.homePage).append(";");
            sb.append("blogSite = " + this.blogSite).append(";");
            sb.append("portfolioSite = " + this.portfolioSite).append(";");
            sb.append("twitterProfile = " + this.twitterProfile).append(";");
            sb.append("facebookProfile = " + this.facebookProfile).append(";");
            sb.append("googlePlusProfile = " + this.googlePlusProfile).append(";");
            sb.append("note = " + this.note).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = primarySite == null ? 0 : primarySite.hashCode();
            _hash = 31 * _hash + delta;
            delta = homePage == null ? 0 : homePage.hashCode();
            _hash = 31 * _hash + delta;
            delta = blogSite == null ? 0 : blogSite.hashCode();
            _hash = 31 * _hash + delta;
            delta = portfolioSite == null ? 0 : portfolioSite.hashCode();
            _hash = 31 * _hash + delta;
            delta = twitterProfile == null ? 0 : twitterProfile.hashCode();
            _hash = 31 * _hash + delta;
            delta = facebookProfile == null ? 0 : facebookProfile.hashCode();
            _hash = 31 * _hash + delta;
            delta = googlePlusProfile == null ? 0 : googlePlusProfile.hashCode();
            _hash = 31 * _hash + delta;
            delta = note == null ? 0 : note.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
