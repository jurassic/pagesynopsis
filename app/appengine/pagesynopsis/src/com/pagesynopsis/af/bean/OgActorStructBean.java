package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.stub.OgActorStructStub;


// Wrapper class + bean combo.
public class OgActorStructBean implements OgActorStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OgActorStructBean.class.getName());

    // [1] With an embedded object.
    private OgActorStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String profile;
    private String role;

    // Ctors.
    public OgActorStructBean()
    {
        //this((String) null);
    }
    public OgActorStructBean(String uuid, String profile, String role)
    {
        this.uuid = uuid;
        this.profile = profile;
        this.role = role;
    }
    public OgActorStructBean(OgActorStruct stub)
    {
        if(stub instanceof OgActorStructStub) {
            this.stub = (OgActorStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setProfile(stub.getProfile());   
            setRole(stub.getRole());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getProfile()
    {
        if(getStub() != null) {
            return getStub().getProfile();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.profile;
        }
    }
    public void setProfile(String profile)
    {
        if(getStub() != null) {
            getStub().setProfile(profile);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.profile = profile;
        }
    }

    public String getRole()
    {
        if(getStub() != null) {
            return getStub().getRole();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.role;
        }
    }
    public void setRole(String role)
    {
        if(getStub() != null) {
            getStub().setRole(role);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.role = role;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getProfile() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRole() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public OgActorStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(OgActorStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("profile = " + this.profile).append(";");
            sb.append("role = " + this.role).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = profile == null ? 0 : profile.hashCode();
            _hash = 31 * _hash + delta;
            delta = role == null ? 0 : role.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
