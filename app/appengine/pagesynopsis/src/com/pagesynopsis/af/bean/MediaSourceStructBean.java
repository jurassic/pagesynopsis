package com.pagesynopsis.af.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.stub.MediaSourceStructStub;


// Wrapper class + bean combo.
public class MediaSourceStructBean implements MediaSourceStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MediaSourceStructBean.class.getName());

    // [1] With an embedded object.
    private MediaSourceStructStub stub = null;

    // [2] Or, without an embedded object.
    private String uuid;
    private String src;
    private String srcUrl;
    private String type;
    private String remark;

    // Ctors.
    public MediaSourceStructBean()
    {
        //this((String) null);
    }
    public MediaSourceStructBean(String uuid, String src, String srcUrl, String type, String remark)
    {
        this.uuid = uuid;
        this.src = src;
        this.srcUrl = srcUrl;
        this.type = type;
        this.remark = remark;
    }
    public MediaSourceStructBean(MediaSourceStruct stub)
    {
        if(stub instanceof MediaSourceStructStub) {
            this.stub = (MediaSourceStructStub) stub;
        } else if(stub != null) {
            log.log(Level.INFO, "The arg object is not a stub type.");
            setUuid(stub.getUuid());   
            setSrc(stub.getSrc());   
            setSrcUrl(stub.getSrcUrl());   
            setType(stub.getType());   
            setRemark(stub.getRemark());   
        } else {
            // ???? When does this happen????
            //log.log(Level.WARNING, "The arg stub object is null. Need to check!!!");
            log.log(Level.INFO, "The arg stub object is null. Need to check!!!");
        }
    }

    public String getUuid()
    {
        if(getStub() != null) {
            return getStub().getUuid();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.uuid;
        }
    }
    public void setUuid(String uuid)
    {
        if(getStub() != null) {
            getStub().setUuid(uuid);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.uuid = uuid;
        }
    }

    public String getSrc()
    {
        if(getStub() != null) {
            return getStub().getSrc();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.src;
        }
    }
    public void setSrc(String src)
    {
        if(getStub() != null) {
            getStub().setSrc(src);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.src = src;
        }
    }

    public String getSrcUrl()
    {
        if(getStub() != null) {
            return getStub().getSrcUrl();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.srcUrl;
        }
    }
    public void setSrcUrl(String srcUrl)
    {
        if(getStub() != null) {
            getStub().setSrcUrl(srcUrl);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.srcUrl = srcUrl;
        }
    }

    public String getType()
    {
        if(getStub() != null) {
            return getStub().getType();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.type;
        }
    }
    public void setType(String type)
    {
        if(getStub() != null) {
            getStub().setType(type);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.type = type;
        }
    }

    public String getRemark()
    {
        if(getStub() != null) {
            return getStub().getRemark();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            return this.remark;
        }
    }
    public void setRemark(String remark)
    {
        if(getStub() != null) {
            getStub().setRemark(remark);
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            this.remark = remark;
        }
    }


    // @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getSrc() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getSrcUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getRemark() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    // Returns the reference to the embedded object. (Could be null.)
    public MediaSourceStructStub getStub()
    {
        return this.stub;
    }
    protected void setStub(MediaSourceStructStub stub)
    {
        this.stub = stub;
    }

    // Returns true if this bean is a wrapper around another bean/stub.
    public boolean isWrapper()
    {
        if(getStub() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString()
    {
        if(getStub() != null) {
            return getStub().toString();
        } else {
            log.log(Level.FINER, "The embedded object is null.");
            StringBuilder sb = new StringBuilder();
            sb.append("uuid = " + this.uuid).append(";");
            sb.append("src = " + this.src).append(";");
            sb.append("srcUrl = " + this.srcUrl).append(";");
            sb.append("type = " + this.type).append(";");
            sb.append("remark = " + this.remark).append(";");
            return sb.toString();
        }
    }

    @Override
    public int hashCode()
    {
        if(getStub() != null) {
            return getStub().hashCode(); // ???
        } else {
            int _hash = super.hashCode() + 7;
            int delta = 0;
            delta = uuid == null ? 0 : uuid.hashCode();
            _hash = 31 * _hash + delta;
            delta = src == null ? 0 : src.hashCode();
            _hash = 31 * _hash + delta;
            delta = srcUrl == null ? 0 : srcUrl.hashCode();
            _hash = 31 * _hash + delta;
            delta = type == null ? 0 : type.hashCode();
            _hash = 31 * _hash + delta;
            delta = remark == null ? 0 : remark.hashCode();
            _hash = 31 * _hash + delta;
            return _hash;
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

}
