package com.pagesynopsis.af.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.KeyValueRelationStruct;
import com.pagesynopsis.ws.ExternalServiceApiKeyStruct;
import com.pagesynopsis.ws.GeoPointStruct;
import com.pagesynopsis.ws.GeoCoordinateStruct;
import com.pagesynopsis.ws.StreetAddressStruct;
import com.pagesynopsis.ws.FullNameStruct;
import com.pagesynopsis.ws.UserWebsiteStruct;
import com.pagesynopsis.ws.ContactInfoStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.GaeAppStruct;
import com.pagesynopsis.ws.GaeUserStruct;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.PagerStateStruct;
import com.pagesynopsis.ws.AppBrandStruct;
import com.pagesynopsis.ws.ApiConsumer;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.ws.EncodedQueryParamStruct;
import com.pagesynopsis.ws.DecodedQueryParamStruct;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.MediaSourceStruct;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.OgContactInfoStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgGeoPointStruct;
import com.pagesynopsis.ws.OgQuantityStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgObjectBase;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterCardBase;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.PageBase;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.HelpNotice;
import com.pagesynopsis.ws.ServiceInfo;
import com.pagesynopsis.ws.FiveTen;
import com.pagesynopsis.ws.stub.KeyValuePairStructStub;
import com.pagesynopsis.ws.stub.KeyValuePairStructListStub;
import com.pagesynopsis.ws.stub.KeyValueRelationStructStub;
import com.pagesynopsis.ws.stub.KeyValueRelationStructListStub;
import com.pagesynopsis.ws.stub.ExternalServiceApiKeyStructStub;
import com.pagesynopsis.ws.stub.ExternalServiceApiKeyStructListStub;
import com.pagesynopsis.ws.stub.GeoPointStructStub;
import com.pagesynopsis.ws.stub.GeoPointStructListStub;
import com.pagesynopsis.ws.stub.GeoCoordinateStructStub;
import com.pagesynopsis.ws.stub.GeoCoordinateStructListStub;
import com.pagesynopsis.ws.stub.StreetAddressStructStub;
import com.pagesynopsis.ws.stub.StreetAddressStructListStub;
import com.pagesynopsis.ws.stub.FullNameStructStub;
import com.pagesynopsis.ws.stub.FullNameStructListStub;
import com.pagesynopsis.ws.stub.UserWebsiteStructStub;
import com.pagesynopsis.ws.stub.UserWebsiteStructListStub;
import com.pagesynopsis.ws.stub.ContactInfoStructStub;
import com.pagesynopsis.ws.stub.ContactInfoStructListStub;
import com.pagesynopsis.ws.stub.ReferrerInfoStructStub;
import com.pagesynopsis.ws.stub.ReferrerInfoStructListStub;
import com.pagesynopsis.ws.stub.GaeAppStructStub;
import com.pagesynopsis.ws.stub.GaeAppStructListStub;
import com.pagesynopsis.ws.stub.GaeUserStructStub;
import com.pagesynopsis.ws.stub.GaeUserStructListStub;
import com.pagesynopsis.ws.stub.NotificationStructStub;
import com.pagesynopsis.ws.stub.NotificationStructListStub;
import com.pagesynopsis.ws.stub.PagerStateStructStub;
import com.pagesynopsis.ws.stub.PagerStateStructListStub;
import com.pagesynopsis.ws.stub.AppBrandStructStub;
import com.pagesynopsis.ws.stub.AppBrandStructListStub;
import com.pagesynopsis.ws.stub.ApiConsumerStub;
import com.pagesynopsis.ws.stub.ApiConsumerListStub;
import com.pagesynopsis.ws.stub.UserStub;
import com.pagesynopsis.ws.stub.UserListStub;
import com.pagesynopsis.ws.stub.EncodedQueryParamStructStub;
import com.pagesynopsis.ws.stub.EncodedQueryParamStructListStub;
import com.pagesynopsis.ws.stub.DecodedQueryParamStructStub;
import com.pagesynopsis.ws.stub.DecodedQueryParamStructListStub;
import com.pagesynopsis.ws.stub.UrlStructStub;
import com.pagesynopsis.ws.stub.UrlStructListStub;
import com.pagesynopsis.ws.stub.AnchorStructStub;
import com.pagesynopsis.ws.stub.AnchorStructListStub;
import com.pagesynopsis.ws.stub.ImageStructStub;
import com.pagesynopsis.ws.stub.ImageStructListStub;
import com.pagesynopsis.ws.stub.MediaSourceStructStub;
import com.pagesynopsis.ws.stub.MediaSourceStructListStub;
import com.pagesynopsis.ws.stub.AudioStructStub;
import com.pagesynopsis.ws.stub.AudioStructListStub;
import com.pagesynopsis.ws.stub.VideoStructStub;
import com.pagesynopsis.ws.stub.VideoStructListStub;
import com.pagesynopsis.ws.stub.RobotsTextGroupStub;
import com.pagesynopsis.ws.stub.RobotsTextGroupListStub;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;
import com.pagesynopsis.ws.stub.RobotsTextFileListStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshStub;
import com.pagesynopsis.ws.stub.RobotsTextRefreshListStub;
import com.pagesynopsis.ws.stub.OgContactInfoStructStub;
import com.pagesynopsis.ws.stub.OgContactInfoStructListStub;
import com.pagesynopsis.ws.stub.OgImageStructStub;
import com.pagesynopsis.ws.stub.OgImageStructListStub;
import com.pagesynopsis.ws.stub.OgAudioStructStub;
import com.pagesynopsis.ws.stub.OgAudioStructListStub;
import com.pagesynopsis.ws.stub.OgVideoStructStub;
import com.pagesynopsis.ws.stub.OgVideoStructListStub;
import com.pagesynopsis.ws.stub.OgGeoPointStructStub;
import com.pagesynopsis.ws.stub.OgGeoPointStructListStub;
import com.pagesynopsis.ws.stub.OgQuantityStructStub;
import com.pagesynopsis.ws.stub.OgQuantityStructListStub;
import com.pagesynopsis.ws.stub.OgActorStructStub;
import com.pagesynopsis.ws.stub.OgActorStructListStub;
import com.pagesynopsis.ws.stub.OgProfileStub;
import com.pagesynopsis.ws.stub.OgProfileListStub;
import com.pagesynopsis.ws.stub.OgWebsiteStub;
import com.pagesynopsis.ws.stub.OgWebsiteListStub;
import com.pagesynopsis.ws.stub.OgBlogStub;
import com.pagesynopsis.ws.stub.OgBlogListStub;
import com.pagesynopsis.ws.stub.OgArticleStub;
import com.pagesynopsis.ws.stub.OgArticleListStub;
import com.pagesynopsis.ws.stub.OgBookStub;
import com.pagesynopsis.ws.stub.OgBookListStub;
import com.pagesynopsis.ws.stub.OgVideoStub;
import com.pagesynopsis.ws.stub.OgVideoListStub;
import com.pagesynopsis.ws.stub.OgMovieStub;
import com.pagesynopsis.ws.stub.OgMovieListStub;
import com.pagesynopsis.ws.stub.OgTvShowStub;
import com.pagesynopsis.ws.stub.OgTvShowListStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeStub;
import com.pagesynopsis.ws.stub.OgTvEpisodeListStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoStub;
import com.pagesynopsis.ws.stub.TwitterCardAppInfoListStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataStub;
import com.pagesynopsis.ws.stub.TwitterCardProductDataListStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardStub;
import com.pagesynopsis.ws.stub.TwitterSummaryCardListStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardStub;
import com.pagesynopsis.ws.stub.TwitterPhotoCardListStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardStub;
import com.pagesynopsis.ws.stub.TwitterGalleryCardListStub;
import com.pagesynopsis.ws.stub.TwitterAppCardStub;
import com.pagesynopsis.ws.stub.TwitterAppCardListStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardStub;
import com.pagesynopsis.ws.stub.TwitterPlayerCardListStub;
import com.pagesynopsis.ws.stub.TwitterProductCardStub;
import com.pagesynopsis.ws.stub.TwitterProductCardListStub;
import com.pagesynopsis.ws.stub.FetchRequestStub;
import com.pagesynopsis.ws.stub.FetchRequestListStub;
import com.pagesynopsis.ws.stub.PageInfoStub;
import com.pagesynopsis.ws.stub.PageInfoListStub;
import com.pagesynopsis.ws.stub.PageFetchStub;
import com.pagesynopsis.ws.stub.PageFetchListStub;
import com.pagesynopsis.ws.stub.LinkListStub;
import com.pagesynopsis.ws.stub.LinkListListStub;
import com.pagesynopsis.ws.stub.ImageSetStub;
import com.pagesynopsis.ws.stub.ImageSetListStub;
import com.pagesynopsis.ws.stub.AudioSetStub;
import com.pagesynopsis.ws.stub.AudioSetListStub;
import com.pagesynopsis.ws.stub.VideoSetStub;
import com.pagesynopsis.ws.stub.VideoSetListStub;
import com.pagesynopsis.ws.stub.OpenGraphMetaStub;
import com.pagesynopsis.ws.stub.OpenGraphMetaListStub;
import com.pagesynopsis.ws.stub.TwitterCardMetaStub;
import com.pagesynopsis.ws.stub.TwitterCardMetaListStub;
import com.pagesynopsis.ws.stub.DomainInfoStub;
import com.pagesynopsis.ws.stub.DomainInfoListStub;
import com.pagesynopsis.ws.stub.UrlRatingStub;
import com.pagesynopsis.ws.stub.UrlRatingListStub;
import com.pagesynopsis.ws.stub.HelpNoticeStub;
import com.pagesynopsis.ws.stub.HelpNoticeListStub;
import com.pagesynopsis.ws.stub.ServiceInfoStub;
import com.pagesynopsis.ws.stub.ServiceInfoListStub;
import com.pagesynopsis.ws.stub.FiveTenStub;
import com.pagesynopsis.ws.stub.FiveTenListStub;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.ApiConsumerBean;
import com.pagesynopsis.af.bean.UserBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgObjectBaseBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterCardBaseBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.bean.PageBaseBean;
import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.bean.VideoSetBean;
import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.bean.UrlRatingBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.ServiceInfoBean;
import com.pagesynopsis.af.bean.FiveTenBean;


public final class MarshalHelper
{
    private static final Logger log = Logger.getLogger(MarshalHelper.class.getName());

    // Static methods only.
    private MarshalHelper() {}

    // temporary
    public static KeyValuePairStructBean convertKeyValuePairStructToBean(KeyValuePairStruct stub)
    {
        KeyValuePairStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValuePairStructBean();
        } else if(stub instanceof KeyValuePairStructBean) {
        	bean = (KeyValuePairStructBean) stub;
        } else {
        	bean = new KeyValuePairStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValuePairStructBean> convertKeyValuePairStructListToBeanList(List<KeyValuePairStruct> stubList)
    {
        List<KeyValuePairStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStructBean>();
            for(KeyValuePairStruct stub : stubList) {
                beanList.add(convertKeyValuePairStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub)
    {
        return convertKeyValuePairStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValuePairStruct> convertKeyValuePairStructListStubToBeanList(KeyValuePairStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValuePairStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValuePairStruct>();
        } else {
            beanList = new ArrayList<KeyValuePairStruct>();
            for(KeyValuePairStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValuePairStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValuePairStructStub convertKeyValuePairStructToStub(KeyValuePairStruct bean)
    {
        KeyValuePairStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValuePairStructStub();
        } else if(bean instanceof KeyValuePairStructStub) {
            stub = (KeyValuePairStructStub) bean;
        } else if(bean instanceof KeyValuePairStructBean && ((KeyValuePairStructBean) bean).isWrapper()) {
            stub = ((KeyValuePairStructBean) bean).getStub();
        } else {
            stub = KeyValuePairStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static KeyValueRelationStructBean convertKeyValueRelationStructToBean(KeyValueRelationStruct stub)
    {
        KeyValueRelationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new KeyValueRelationStructBean();
        } else if(stub instanceof KeyValueRelationStructBean) {
        	bean = (KeyValueRelationStructBean) stub;
        } else {
        	bean = new KeyValueRelationStructBean(stub);
        }
        return bean;
    }
    public static List<KeyValueRelationStructBean> convertKeyValueRelationStructListToBeanList(List<KeyValueRelationStruct> stubList)
    {
        List<KeyValueRelationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStructBean>();
            for(KeyValueRelationStruct stub : stubList) {
                beanList.add(convertKeyValueRelationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub)
    {
        return convertKeyValueRelationStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<KeyValueRelationStruct> convertKeyValueRelationStructListStubToBeanList(KeyValueRelationStructListStub listStub, StringCursor forwardCursor)
    {
        List<KeyValueRelationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<KeyValueRelationStruct>();
        } else {
            beanList = new ArrayList<KeyValueRelationStruct>();
            for(KeyValueRelationStructStub stub : listStub.getList()) {
            	beanList.add(convertKeyValueRelationStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static KeyValueRelationStructStub convertKeyValueRelationStructToStub(KeyValueRelationStruct bean)
    {
        KeyValueRelationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new KeyValueRelationStructStub();
        } else if(bean instanceof KeyValueRelationStructStub) {
            stub = (KeyValueRelationStructStub) bean;
        } else if(bean instanceof KeyValueRelationStructBean && ((KeyValueRelationStructBean) bean).isWrapper()) {
            stub = ((KeyValueRelationStructBean) bean).getStub();
        } else {
            stub = KeyValueRelationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ExternalServiceApiKeyStructBean convertExternalServiceApiKeyStructToBean(ExternalServiceApiKeyStruct stub)
    {
        ExternalServiceApiKeyStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ExternalServiceApiKeyStructBean();
        } else if(stub instanceof ExternalServiceApiKeyStructBean) {
        	bean = (ExternalServiceApiKeyStructBean) stub;
        } else {
        	bean = new ExternalServiceApiKeyStructBean(stub);
        }
        return bean;
    }
    public static List<ExternalServiceApiKeyStructBean> convertExternalServiceApiKeyStructListToBeanList(List<ExternalServiceApiKeyStruct> stubList)
    {
        List<ExternalServiceApiKeyStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStructBean>();
            for(ExternalServiceApiKeyStruct stub : stubList) {
                beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub)
    {
        return convertExternalServiceApiKeyStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ExternalServiceApiKeyStruct> convertExternalServiceApiKeyStructListStubToBeanList(ExternalServiceApiKeyStructListStub listStub, StringCursor forwardCursor)
    {
        List<ExternalServiceApiKeyStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ExternalServiceApiKeyStruct>();
        } else {
            beanList = new ArrayList<ExternalServiceApiKeyStruct>();
            for(ExternalServiceApiKeyStructStub stub : listStub.getList()) {
            	beanList.add(convertExternalServiceApiKeyStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ExternalServiceApiKeyStructStub convertExternalServiceApiKeyStructToStub(ExternalServiceApiKeyStruct bean)
    {
        ExternalServiceApiKeyStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ExternalServiceApiKeyStructStub();
        } else if(bean instanceof ExternalServiceApiKeyStructStub) {
            stub = (ExternalServiceApiKeyStructStub) bean;
        } else if(bean instanceof ExternalServiceApiKeyStructBean && ((ExternalServiceApiKeyStructBean) bean).isWrapper()) {
            stub = ((ExternalServiceApiKeyStructBean) bean).getStub();
        } else {
            stub = ExternalServiceApiKeyStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoPointStructBean convertGeoPointStructToBean(GeoPointStruct stub)
    {
        GeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoPointStructBean();
        } else if(stub instanceof GeoPointStructBean) {
        	bean = (GeoPointStructBean) stub;
        } else {
        	bean = new GeoPointStructBean(stub);
        }
        return bean;
    }
    public static List<GeoPointStructBean> convertGeoPointStructListToBeanList(List<GeoPointStruct> stubList)
    {
        List<GeoPointStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStructBean>();
            for(GeoPointStruct stub : stubList) {
                beanList.add(convertGeoPointStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub)
    {
        return convertGeoPointStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GeoPointStruct> convertGeoPointStructListStubToBeanList(GeoPointStructListStub listStub, StringCursor forwardCursor)
    {
        List<GeoPointStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoPointStruct>();
        } else {
            beanList = new ArrayList<GeoPointStruct>();
            for(GeoPointStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoPointStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GeoPointStructStub convertGeoPointStructToStub(GeoPointStruct bean)
    {
        GeoPointStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoPointStructStub();
        } else if(bean instanceof GeoPointStructStub) {
            stub = (GeoPointStructStub) bean;
        } else if(bean instanceof GeoPointStructBean && ((GeoPointStructBean) bean).isWrapper()) {
            stub = ((GeoPointStructBean) bean).getStub();
        } else {
            stub = GeoPointStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GeoCoordinateStructBean convertGeoCoordinateStructToBean(GeoCoordinateStruct stub)
    {
        GeoCoordinateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GeoCoordinateStructBean();
        } else if(stub instanceof GeoCoordinateStructBean) {
        	bean = (GeoCoordinateStructBean) stub;
        } else {
        	bean = new GeoCoordinateStructBean(stub);
        }
        return bean;
    }
    public static List<GeoCoordinateStructBean> convertGeoCoordinateStructListToBeanList(List<GeoCoordinateStruct> stubList)
    {
        List<GeoCoordinateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStructBean>();
            for(GeoCoordinateStruct stub : stubList) {
                beanList.add(convertGeoCoordinateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub)
    {
        return convertGeoCoordinateStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GeoCoordinateStruct> convertGeoCoordinateStructListStubToBeanList(GeoCoordinateStructListStub listStub, StringCursor forwardCursor)
    {
        List<GeoCoordinateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GeoCoordinateStruct>();
        } else {
            beanList = new ArrayList<GeoCoordinateStruct>();
            for(GeoCoordinateStructStub stub : listStub.getList()) {
            	beanList.add(convertGeoCoordinateStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GeoCoordinateStructStub convertGeoCoordinateStructToStub(GeoCoordinateStruct bean)
    {
        GeoCoordinateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GeoCoordinateStructStub();
        } else if(bean instanceof GeoCoordinateStructStub) {
            stub = (GeoCoordinateStructStub) bean;
        } else if(bean instanceof GeoCoordinateStructBean && ((GeoCoordinateStructBean) bean).isWrapper()) {
            stub = ((GeoCoordinateStructBean) bean).getStub();
        } else {
            stub = GeoCoordinateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static StreetAddressStructBean convertStreetAddressStructToBean(StreetAddressStruct stub)
    {
        StreetAddressStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new StreetAddressStructBean();
        } else if(stub instanceof StreetAddressStructBean) {
        	bean = (StreetAddressStructBean) stub;
        } else {
        	bean = new StreetAddressStructBean(stub);
        }
        return bean;
    }
    public static List<StreetAddressStructBean> convertStreetAddressStructListToBeanList(List<StreetAddressStruct> stubList)
    {
        List<StreetAddressStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStructBean>();
            for(StreetAddressStruct stub : stubList) {
                beanList.add(convertStreetAddressStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub)
    {
        return convertStreetAddressStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<StreetAddressStruct> convertStreetAddressStructListStubToBeanList(StreetAddressStructListStub listStub, StringCursor forwardCursor)
    {
        List<StreetAddressStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<StreetAddressStruct>();
        } else {
            beanList = new ArrayList<StreetAddressStruct>();
            for(StreetAddressStructStub stub : listStub.getList()) {
            	beanList.add(convertStreetAddressStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static StreetAddressStructStub convertStreetAddressStructToStub(StreetAddressStruct bean)
    {
        StreetAddressStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new StreetAddressStructStub();
        } else if(bean instanceof StreetAddressStructStub) {
            stub = (StreetAddressStructStub) bean;
        } else if(bean instanceof StreetAddressStructBean && ((StreetAddressStructBean) bean).isWrapper()) {
            stub = ((StreetAddressStructBean) bean).getStub();
        } else {
            stub = StreetAddressStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FullNameStructBean convertFullNameStructToBean(FullNameStruct stub)
    {
        FullNameStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FullNameStructBean();
        } else if(stub instanceof FullNameStructBean) {
        	bean = (FullNameStructBean) stub;
        } else {
        	bean = new FullNameStructBean(stub);
        }
        return bean;
    }
    public static List<FullNameStructBean> convertFullNameStructListToBeanList(List<FullNameStruct> stubList)
    {
        List<FullNameStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStructBean>();
            for(FullNameStruct stub : stubList) {
                beanList.add(convertFullNameStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub)
    {
        return convertFullNameStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FullNameStruct> convertFullNameStructListStubToBeanList(FullNameStructListStub listStub, StringCursor forwardCursor)
    {
        List<FullNameStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FullNameStruct>();
        } else {
            beanList = new ArrayList<FullNameStruct>();
            for(FullNameStructStub stub : listStub.getList()) {
            	beanList.add(convertFullNameStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FullNameStructStub convertFullNameStructToStub(FullNameStruct bean)
    {
        FullNameStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FullNameStructStub();
        } else if(bean instanceof FullNameStructStub) {
            stub = (FullNameStructStub) bean;
        } else if(bean instanceof FullNameStructBean && ((FullNameStructBean) bean).isWrapper()) {
            stub = ((FullNameStructBean) bean).getStub();
        } else {
            stub = FullNameStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserWebsiteStructBean convertUserWebsiteStructToBean(UserWebsiteStruct stub)
    {
        UserWebsiteStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserWebsiteStructBean();
        } else if(stub instanceof UserWebsiteStructBean) {
        	bean = (UserWebsiteStructBean) stub;
        } else {
        	bean = new UserWebsiteStructBean(stub);
        }
        return bean;
    }
    public static List<UserWebsiteStructBean> convertUserWebsiteStructListToBeanList(List<UserWebsiteStruct> stubList)
    {
        List<UserWebsiteStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UserWebsiteStruct>();
        } else {
            beanList = new ArrayList<UserWebsiteStructBean>();
            for(UserWebsiteStruct stub : stubList) {
                beanList.add(convertUserWebsiteStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UserWebsiteStruct> convertUserWebsiteStructListStubToBeanList(UserWebsiteStructListStub listStub)
    {
        return convertUserWebsiteStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UserWebsiteStruct> convertUserWebsiteStructListStubToBeanList(UserWebsiteStructListStub listStub, StringCursor forwardCursor)
    {
        List<UserWebsiteStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UserWebsiteStruct>();
        } else {
            beanList = new ArrayList<UserWebsiteStruct>();
            for(UserWebsiteStructStub stub : listStub.getList()) {
            	beanList.add(convertUserWebsiteStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserWebsiteStructStub convertUserWebsiteStructToStub(UserWebsiteStruct bean)
    {
        UserWebsiteStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserWebsiteStructStub();
        } else if(bean instanceof UserWebsiteStructStub) {
            stub = (UserWebsiteStructStub) bean;
        } else if(bean instanceof UserWebsiteStructBean && ((UserWebsiteStructBean) bean).isWrapper()) {
            stub = ((UserWebsiteStructBean) bean).getStub();
        } else {
            stub = UserWebsiteStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ContactInfoStructBean convertContactInfoStructToBean(ContactInfoStruct stub)
    {
        ContactInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ContactInfoStructBean();
        } else if(stub instanceof ContactInfoStructBean) {
        	bean = (ContactInfoStructBean) stub;
        } else {
        	bean = new ContactInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ContactInfoStructBean> convertContactInfoStructListToBeanList(List<ContactInfoStruct> stubList)
    {
        List<ContactInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ContactInfoStruct>();
        } else {
            beanList = new ArrayList<ContactInfoStructBean>();
            for(ContactInfoStruct stub : stubList) {
                beanList.add(convertContactInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ContactInfoStruct> convertContactInfoStructListStubToBeanList(ContactInfoStructListStub listStub)
    {
        return convertContactInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ContactInfoStruct> convertContactInfoStructListStubToBeanList(ContactInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<ContactInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ContactInfoStruct>();
        } else {
            beanList = new ArrayList<ContactInfoStruct>();
            for(ContactInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertContactInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ContactInfoStructStub convertContactInfoStructToStub(ContactInfoStruct bean)
    {
        ContactInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ContactInfoStructStub();
        } else if(bean instanceof ContactInfoStructStub) {
            stub = (ContactInfoStructStub) bean;
        } else if(bean instanceof ContactInfoStructBean && ((ContactInfoStructBean) bean).isWrapper()) {
            stub = ((ContactInfoStructBean) bean).getStub();
        } else {
            stub = ContactInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ReferrerInfoStructBean convertReferrerInfoStructToBean(ReferrerInfoStruct stub)
    {
        ReferrerInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ReferrerInfoStructBean();
        } else if(stub instanceof ReferrerInfoStructBean) {
        	bean = (ReferrerInfoStructBean) stub;
        } else {
        	bean = new ReferrerInfoStructBean(stub);
        }
        return bean;
    }
    public static List<ReferrerInfoStructBean> convertReferrerInfoStructListToBeanList(List<ReferrerInfoStruct> stubList)
    {
        List<ReferrerInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStructBean>();
            for(ReferrerInfoStruct stub : stubList) {
                beanList.add(convertReferrerInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub)
    {
        return convertReferrerInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ReferrerInfoStruct> convertReferrerInfoStructListStubToBeanList(ReferrerInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<ReferrerInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ReferrerInfoStruct>();
        } else {
            beanList = new ArrayList<ReferrerInfoStruct>();
            for(ReferrerInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertReferrerInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ReferrerInfoStructStub convertReferrerInfoStructToStub(ReferrerInfoStruct bean)
    {
        ReferrerInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ReferrerInfoStructStub();
        } else if(bean instanceof ReferrerInfoStructStub) {
            stub = (ReferrerInfoStructStub) bean;
        } else if(bean instanceof ReferrerInfoStructBean && ((ReferrerInfoStructBean) bean).isWrapper()) {
            stub = ((ReferrerInfoStructBean) bean).getStub();
        } else {
            stub = ReferrerInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeAppStructBean convertGaeAppStructToBean(GaeAppStruct stub)
    {
        GaeAppStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeAppStructBean();
        } else if(stub instanceof GaeAppStructBean) {
        	bean = (GaeAppStructBean) stub;
        } else {
        	bean = new GaeAppStructBean(stub);
        }
        return bean;
    }
    public static List<GaeAppStructBean> convertGaeAppStructListToBeanList(List<GaeAppStruct> stubList)
    {
        List<GaeAppStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStructBean>();
            for(GaeAppStruct stub : stubList) {
                beanList.add(convertGaeAppStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub)
    {
        return convertGaeAppStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeAppStruct> convertGaeAppStructListStubToBeanList(GaeAppStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeAppStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeAppStruct>();
        } else {
            beanList = new ArrayList<GaeAppStruct>();
            for(GaeAppStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeAppStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeAppStructStub convertGaeAppStructToStub(GaeAppStruct bean)
    {
        GaeAppStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeAppStructStub();
        } else if(bean instanceof GaeAppStructStub) {
            stub = (GaeAppStructStub) bean;
        } else if(bean instanceof GaeAppStructBean && ((GaeAppStructBean) bean).isWrapper()) {
            stub = ((GaeAppStructBean) bean).getStub();
        } else {
            stub = GaeAppStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static GaeUserStructBean convertGaeUserStructToBean(GaeUserStruct stub)
    {
        GaeUserStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new GaeUserStructBean();
        } else if(stub instanceof GaeUserStructBean) {
        	bean = (GaeUserStructBean) stub;
        } else {
        	bean = new GaeUserStructBean(stub);
        }
        return bean;
    }
    public static List<GaeUserStructBean> convertGaeUserStructListToBeanList(List<GaeUserStruct> stubList)
    {
        List<GaeUserStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStructBean>();
            for(GaeUserStruct stub : stubList) {
                beanList.add(convertGaeUserStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub)
    {
        return convertGaeUserStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<GaeUserStruct> convertGaeUserStructListStubToBeanList(GaeUserStructListStub listStub, StringCursor forwardCursor)
    {
        List<GaeUserStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<GaeUserStruct>();
        } else {
            beanList = new ArrayList<GaeUserStruct>();
            for(GaeUserStructStub stub : listStub.getList()) {
            	beanList.add(convertGaeUserStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static GaeUserStructStub convertGaeUserStructToStub(GaeUserStruct bean)
    {
        GaeUserStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new GaeUserStructStub();
        } else if(bean instanceof GaeUserStructStub) {
            stub = (GaeUserStructStub) bean;
        } else if(bean instanceof GaeUserStructBean && ((GaeUserStructBean) bean).isWrapper()) {
            stub = ((GaeUserStructBean) bean).getStub();
        } else {
            stub = GaeUserStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static NotificationStructBean convertNotificationStructToBean(NotificationStruct stub)
    {
        NotificationStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new NotificationStructBean();
        } else if(stub instanceof NotificationStructBean) {
        	bean = (NotificationStructBean) stub;
        } else {
        	bean = new NotificationStructBean(stub);
        }
        return bean;
    }
    public static List<NotificationStructBean> convertNotificationStructListToBeanList(List<NotificationStruct> stubList)
    {
        List<NotificationStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<NotificationStruct>();
        } else {
            beanList = new ArrayList<NotificationStructBean>();
            for(NotificationStruct stub : stubList) {
                beanList.add(convertNotificationStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<NotificationStruct> convertNotificationStructListStubToBeanList(NotificationStructListStub listStub)
    {
        return convertNotificationStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<NotificationStruct> convertNotificationStructListStubToBeanList(NotificationStructListStub listStub, StringCursor forwardCursor)
    {
        List<NotificationStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<NotificationStruct>();
        } else {
            beanList = new ArrayList<NotificationStruct>();
            for(NotificationStructStub stub : listStub.getList()) {
            	beanList.add(convertNotificationStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static NotificationStructStub convertNotificationStructToStub(NotificationStruct bean)
    {
        NotificationStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new NotificationStructStub();
        } else if(bean instanceof NotificationStructStub) {
            stub = (NotificationStructStub) bean;
        } else if(bean instanceof NotificationStructBean && ((NotificationStructBean) bean).isWrapper()) {
            stub = ((NotificationStructBean) bean).getStub();
        } else {
            stub = NotificationStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PagerStateStructBean convertPagerStateStructToBean(PagerStateStruct stub)
    {
        PagerStateStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PagerStateStructBean();
        } else if(stub instanceof PagerStateStructBean) {
        	bean = (PagerStateStructBean) stub;
        } else {
        	bean = new PagerStateStructBean(stub);
        }
        return bean;
    }
    public static List<PagerStateStructBean> convertPagerStateStructListToBeanList(List<PagerStateStruct> stubList)
    {
        List<PagerStateStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStructBean>();
            for(PagerStateStruct stub : stubList) {
                beanList.add(convertPagerStateStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub)
    {
        return convertPagerStateStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<PagerStateStruct> convertPagerStateStructListStubToBeanList(PagerStateStructListStub listStub, StringCursor forwardCursor)
    {
        List<PagerStateStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PagerStateStruct>();
        } else {
            beanList = new ArrayList<PagerStateStruct>();
            for(PagerStateStructStub stub : listStub.getList()) {
            	beanList.add(convertPagerStateStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static PagerStateStructStub convertPagerStateStructToStub(PagerStateStruct bean)
    {
        PagerStateStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PagerStateStructStub();
        } else if(bean instanceof PagerStateStructStub) {
            stub = (PagerStateStructStub) bean;
        } else if(bean instanceof PagerStateStructBean && ((PagerStateStructBean) bean).isWrapper()) {
            stub = ((PagerStateStructBean) bean).getStub();
        } else {
            stub = PagerStateStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AppBrandStructBean convertAppBrandStructToBean(AppBrandStruct stub)
    {
        AppBrandStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AppBrandStructBean();
        } else if(stub instanceof AppBrandStructBean) {
        	bean = (AppBrandStructBean) stub;
        } else {
        	bean = new AppBrandStructBean(stub);
        }
        return bean;
    }
    public static List<AppBrandStructBean> convertAppBrandStructListToBeanList(List<AppBrandStruct> stubList)
    {
        List<AppBrandStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AppBrandStruct>();
        } else {
            beanList = new ArrayList<AppBrandStructBean>();
            for(AppBrandStruct stub : stubList) {
                beanList.add(convertAppBrandStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AppBrandStruct> convertAppBrandStructListStubToBeanList(AppBrandStructListStub listStub)
    {
        return convertAppBrandStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AppBrandStruct> convertAppBrandStructListStubToBeanList(AppBrandStructListStub listStub, StringCursor forwardCursor)
    {
        List<AppBrandStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AppBrandStruct>();
        } else {
            beanList = new ArrayList<AppBrandStruct>();
            for(AppBrandStructStub stub : listStub.getList()) {
            	beanList.add(convertAppBrandStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AppBrandStructStub convertAppBrandStructToStub(AppBrandStruct bean)
    {
        AppBrandStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AppBrandStructStub();
        } else if(bean instanceof AppBrandStructStub) {
            stub = (AppBrandStructStub) bean;
        } else if(bean instanceof AppBrandStructBean && ((AppBrandStructBean) bean).isWrapper()) {
            stub = ((AppBrandStructBean) bean).getStub();
        } else {
            stub = AppBrandStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ApiConsumerBean convertApiConsumerToBean(ApiConsumer stub)
    {
        ApiConsumerBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ApiConsumerBean();
        } else if(stub instanceof ApiConsumerBean) {
        	bean = (ApiConsumerBean) stub;
        } else {
        	bean = new ApiConsumerBean(stub);
        }
        return bean;
    }
    public static List<ApiConsumerBean> convertApiConsumerListToBeanList(List<ApiConsumer> stubList)
    {
        List<ApiConsumerBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumerBean>();
            for(ApiConsumer stub : stubList) {
                beanList.add(convertApiConsumerToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub)
    {
        return convertApiConsumerListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ApiConsumer> convertApiConsumerListStubToBeanList(ApiConsumerListStub listStub, StringCursor forwardCursor)
    {
        List<ApiConsumer> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ApiConsumer>();
        } else {
            beanList = new ArrayList<ApiConsumer>();
            for(ApiConsumerStub stub : listStub.getList()) {
            	beanList.add(convertApiConsumerToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ApiConsumerStub convertApiConsumerToStub(ApiConsumer bean)
    {
        ApiConsumerStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ApiConsumerStub();
        } else if(bean instanceof ApiConsumerStub) {
            stub = (ApiConsumerStub) bean;
        } else if(bean instanceof ApiConsumerBean && ((ApiConsumerBean) bean).isWrapper()) {
            stub = ((ApiConsumerBean) bean).getStub();
        } else {
            stub = ApiConsumerStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UserBean convertUserToBean(User stub)
    {
        UserBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UserBean();
        } else if(stub instanceof UserBean) {
        	bean = (UserBean) stub;
        } else {
        	bean = new UserBean(stub);
        }
        return bean;
    }
    public static List<UserBean> convertUserListToBeanList(List<User> stubList)
    {
        List<UserBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<UserBean>();
            for(User stub : stubList) {
                beanList.add(convertUserToBean(stub));
            }
        }
        return beanList;
    }
    public static List<User> convertUserListStubToBeanList(UserListStub listStub)
    {
        return convertUserListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<User> convertUserListStubToBeanList(UserListStub listStub, StringCursor forwardCursor)
    {
        List<User> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<User>();
        } else {
            beanList = new ArrayList<User>();
            for(UserStub stub : listStub.getList()) {
            	beanList.add(convertUserToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UserStub convertUserToStub(User bean)
    {
        UserStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UserStub();
        } else if(bean instanceof UserStub) {
            stub = (UserStub) bean;
        } else if(bean instanceof UserBean && ((UserBean) bean).isWrapper()) {
            stub = ((UserBean) bean).getStub();
        } else {
            stub = UserStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static EncodedQueryParamStructBean convertEncodedQueryParamStructToBean(EncodedQueryParamStruct stub)
    {
        EncodedQueryParamStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new EncodedQueryParamStructBean();
        } else if(stub instanceof EncodedQueryParamStructBean) {
        	bean = (EncodedQueryParamStructBean) stub;
        } else {
        	bean = new EncodedQueryParamStructBean(stub);
        }
        return bean;
    }
    public static List<EncodedQueryParamStructBean> convertEncodedQueryParamStructListToBeanList(List<EncodedQueryParamStruct> stubList)
    {
        List<EncodedQueryParamStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<EncodedQueryParamStruct>();
        } else {
            beanList = new ArrayList<EncodedQueryParamStructBean>();
            for(EncodedQueryParamStruct stub : stubList) {
                beanList.add(convertEncodedQueryParamStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<EncodedQueryParamStruct> convertEncodedQueryParamStructListStubToBeanList(EncodedQueryParamStructListStub listStub)
    {
        return convertEncodedQueryParamStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<EncodedQueryParamStruct> convertEncodedQueryParamStructListStubToBeanList(EncodedQueryParamStructListStub listStub, StringCursor forwardCursor)
    {
        List<EncodedQueryParamStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<EncodedQueryParamStruct>();
        } else {
            beanList = new ArrayList<EncodedQueryParamStruct>();
            for(EncodedQueryParamStructStub stub : listStub.getList()) {
            	beanList.add(convertEncodedQueryParamStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static EncodedQueryParamStructStub convertEncodedQueryParamStructToStub(EncodedQueryParamStruct bean)
    {
        EncodedQueryParamStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new EncodedQueryParamStructStub();
        } else if(bean instanceof EncodedQueryParamStructStub) {
            stub = (EncodedQueryParamStructStub) bean;
        } else if(bean instanceof EncodedQueryParamStructBean && ((EncodedQueryParamStructBean) bean).isWrapper()) {
            stub = ((EncodedQueryParamStructBean) bean).getStub();
        } else {
            stub = EncodedQueryParamStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static DecodedQueryParamStructBean convertDecodedQueryParamStructToBean(DecodedQueryParamStruct stub)
    {
        DecodedQueryParamStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new DecodedQueryParamStructBean();
        } else if(stub instanceof DecodedQueryParamStructBean) {
        	bean = (DecodedQueryParamStructBean) stub;
        } else {
        	bean = new DecodedQueryParamStructBean(stub);
        }
        return bean;
    }
    public static List<DecodedQueryParamStructBean> convertDecodedQueryParamStructListToBeanList(List<DecodedQueryParamStruct> stubList)
    {
        List<DecodedQueryParamStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<DecodedQueryParamStruct>();
        } else {
            beanList = new ArrayList<DecodedQueryParamStructBean>();
            for(DecodedQueryParamStruct stub : stubList) {
                beanList.add(convertDecodedQueryParamStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<DecodedQueryParamStruct> convertDecodedQueryParamStructListStubToBeanList(DecodedQueryParamStructListStub listStub)
    {
        return convertDecodedQueryParamStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<DecodedQueryParamStruct> convertDecodedQueryParamStructListStubToBeanList(DecodedQueryParamStructListStub listStub, StringCursor forwardCursor)
    {
        List<DecodedQueryParamStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<DecodedQueryParamStruct>();
        } else {
            beanList = new ArrayList<DecodedQueryParamStruct>();
            for(DecodedQueryParamStructStub stub : listStub.getList()) {
            	beanList.add(convertDecodedQueryParamStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static DecodedQueryParamStructStub convertDecodedQueryParamStructToStub(DecodedQueryParamStruct bean)
    {
        DecodedQueryParamStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new DecodedQueryParamStructStub();
        } else if(bean instanceof DecodedQueryParamStructStub) {
            stub = (DecodedQueryParamStructStub) bean;
        } else if(bean instanceof DecodedQueryParamStructBean && ((DecodedQueryParamStructBean) bean).isWrapper()) {
            stub = ((DecodedQueryParamStructBean) bean).getStub();
        } else {
            stub = DecodedQueryParamStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UrlStructBean convertUrlStructToBean(UrlStruct stub)
    {
        UrlStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UrlStructBean();
        } else if(stub instanceof UrlStructBean) {
        	bean = (UrlStructBean) stub;
        } else {
        	bean = new UrlStructBean(stub);
        }
        return bean;
    }
    public static List<UrlStructBean> convertUrlStructListToBeanList(List<UrlStruct> stubList)
    {
        List<UrlStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UrlStruct>();
        } else {
            beanList = new ArrayList<UrlStructBean>();
            for(UrlStruct stub : stubList) {
                beanList.add(convertUrlStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UrlStruct> convertUrlStructListStubToBeanList(UrlStructListStub listStub)
    {
        return convertUrlStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UrlStruct> convertUrlStructListStubToBeanList(UrlStructListStub listStub, StringCursor forwardCursor)
    {
        List<UrlStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UrlStruct>();
        } else {
            beanList = new ArrayList<UrlStruct>();
            for(UrlStructStub stub : listStub.getList()) {
            	beanList.add(convertUrlStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UrlStructStub convertUrlStructToStub(UrlStruct bean)
    {
        UrlStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UrlStructStub();
        } else if(bean instanceof UrlStructStub) {
            stub = (UrlStructStub) bean;
        } else if(bean instanceof UrlStructBean && ((UrlStructBean) bean).isWrapper()) {
            stub = ((UrlStructBean) bean).getStub();
        } else {
            stub = UrlStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AnchorStructBean convertAnchorStructToBean(AnchorStruct stub)
    {
        AnchorStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AnchorStructBean();
        } else if(stub instanceof AnchorStructBean) {
        	bean = (AnchorStructBean) stub;
        } else {
        	bean = new AnchorStructBean(stub);
        }
        return bean;
    }
    public static List<AnchorStructBean> convertAnchorStructListToBeanList(List<AnchorStruct> stubList)
    {
        List<AnchorStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AnchorStruct>();
        } else {
            beanList = new ArrayList<AnchorStructBean>();
            for(AnchorStruct stub : stubList) {
                beanList.add(convertAnchorStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AnchorStruct> convertAnchorStructListStubToBeanList(AnchorStructListStub listStub)
    {
        return convertAnchorStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AnchorStruct> convertAnchorStructListStubToBeanList(AnchorStructListStub listStub, StringCursor forwardCursor)
    {
        List<AnchorStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AnchorStruct>();
        } else {
            beanList = new ArrayList<AnchorStruct>();
            for(AnchorStructStub stub : listStub.getList()) {
            	beanList.add(convertAnchorStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AnchorStructStub convertAnchorStructToStub(AnchorStruct bean)
    {
        AnchorStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AnchorStructStub();
        } else if(bean instanceof AnchorStructStub) {
            stub = (AnchorStructStub) bean;
        } else if(bean instanceof AnchorStructBean && ((AnchorStructBean) bean).isWrapper()) {
            stub = ((AnchorStructBean) bean).getStub();
        } else {
            stub = AnchorStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ImageStructBean convertImageStructToBean(ImageStruct stub)
    {
        ImageStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ImageStructBean();
        } else if(stub instanceof ImageStructBean) {
        	bean = (ImageStructBean) stub;
        } else {
        	bean = new ImageStructBean(stub);
        }
        return bean;
    }
    public static List<ImageStructBean> convertImageStructListToBeanList(List<ImageStruct> stubList)
    {
        List<ImageStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ImageStruct>();
        } else {
            beanList = new ArrayList<ImageStructBean>();
            for(ImageStruct stub : stubList) {
                beanList.add(convertImageStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ImageStruct> convertImageStructListStubToBeanList(ImageStructListStub listStub)
    {
        return convertImageStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ImageStruct> convertImageStructListStubToBeanList(ImageStructListStub listStub, StringCursor forwardCursor)
    {
        List<ImageStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ImageStruct>();
        } else {
            beanList = new ArrayList<ImageStruct>();
            for(ImageStructStub stub : listStub.getList()) {
            	beanList.add(convertImageStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ImageStructStub convertImageStructToStub(ImageStruct bean)
    {
        ImageStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ImageStructStub();
        } else if(bean instanceof ImageStructStub) {
            stub = (ImageStructStub) bean;
        } else if(bean instanceof ImageStructBean && ((ImageStructBean) bean).isWrapper()) {
            stub = ((ImageStructBean) bean).getStub();
        } else {
            stub = ImageStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static MediaSourceStructBean convertMediaSourceStructToBean(MediaSourceStruct stub)
    {
        MediaSourceStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new MediaSourceStructBean();
        } else if(stub instanceof MediaSourceStructBean) {
        	bean = (MediaSourceStructBean) stub;
        } else {
        	bean = new MediaSourceStructBean(stub);
        }
        return bean;
    }
    public static List<MediaSourceStructBean> convertMediaSourceStructListToBeanList(List<MediaSourceStruct> stubList)
    {
        List<MediaSourceStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<MediaSourceStruct>();
        } else {
            beanList = new ArrayList<MediaSourceStructBean>();
            for(MediaSourceStruct stub : stubList) {
                beanList.add(convertMediaSourceStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<MediaSourceStruct> convertMediaSourceStructListStubToBeanList(MediaSourceStructListStub listStub)
    {
        return convertMediaSourceStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<MediaSourceStruct> convertMediaSourceStructListStubToBeanList(MediaSourceStructListStub listStub, StringCursor forwardCursor)
    {
        List<MediaSourceStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<MediaSourceStruct>();
        } else {
            beanList = new ArrayList<MediaSourceStruct>();
            for(MediaSourceStructStub stub : listStub.getList()) {
            	beanList.add(convertMediaSourceStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static MediaSourceStructStub convertMediaSourceStructToStub(MediaSourceStruct bean)
    {
        MediaSourceStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new MediaSourceStructStub();
        } else if(bean instanceof MediaSourceStructStub) {
            stub = (MediaSourceStructStub) bean;
        } else if(bean instanceof MediaSourceStructBean && ((MediaSourceStructBean) bean).isWrapper()) {
            stub = ((MediaSourceStructBean) bean).getStub();
        } else {
            stub = MediaSourceStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AudioStructBean convertAudioStructToBean(AudioStruct stub)
    {
        AudioStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AudioStructBean();
        } else if(stub instanceof AudioStructBean) {
        	bean = (AudioStructBean) stub;
        } else {
        	bean = new AudioStructBean(stub);
        }
        return bean;
    }
    public static List<AudioStructBean> convertAudioStructListToBeanList(List<AudioStruct> stubList)
    {
        List<AudioStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AudioStruct>();
        } else {
            beanList = new ArrayList<AudioStructBean>();
            for(AudioStruct stub : stubList) {
                beanList.add(convertAudioStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AudioStruct> convertAudioStructListStubToBeanList(AudioStructListStub listStub)
    {
        return convertAudioStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AudioStruct> convertAudioStructListStubToBeanList(AudioStructListStub listStub, StringCursor forwardCursor)
    {
        List<AudioStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AudioStruct>();
        } else {
            beanList = new ArrayList<AudioStruct>();
            for(AudioStructStub stub : listStub.getList()) {
            	beanList.add(convertAudioStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AudioStructStub convertAudioStructToStub(AudioStruct bean)
    {
        AudioStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AudioStructStub();
        } else if(bean instanceof AudioStructStub) {
            stub = (AudioStructStub) bean;
        } else if(bean instanceof AudioStructBean && ((AudioStructBean) bean).isWrapper()) {
            stub = ((AudioStructBean) bean).getStub();
        } else {
            stub = AudioStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static VideoStructBean convertVideoStructToBean(VideoStruct stub)
    {
        VideoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new VideoStructBean();
        } else if(stub instanceof VideoStructBean) {
        	bean = (VideoStructBean) stub;
        } else {
        	bean = new VideoStructBean(stub);
        }
        return bean;
    }
    public static List<VideoStructBean> convertVideoStructListToBeanList(List<VideoStruct> stubList)
    {
        List<VideoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<VideoStruct>();
        } else {
            beanList = new ArrayList<VideoStructBean>();
            for(VideoStruct stub : stubList) {
                beanList.add(convertVideoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<VideoStruct> convertVideoStructListStubToBeanList(VideoStructListStub listStub)
    {
        return convertVideoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<VideoStruct> convertVideoStructListStubToBeanList(VideoStructListStub listStub, StringCursor forwardCursor)
    {
        List<VideoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<VideoStruct>();
        } else {
            beanList = new ArrayList<VideoStruct>();
            for(VideoStructStub stub : listStub.getList()) {
            	beanList.add(convertVideoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static VideoStructStub convertVideoStructToStub(VideoStruct bean)
    {
        VideoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new VideoStructStub();
        } else if(bean instanceof VideoStructStub) {
            stub = (VideoStructStub) bean;
        } else if(bean instanceof VideoStructBean && ((VideoStructBean) bean).isWrapper()) {
            stub = ((VideoStructBean) bean).getStub();
        } else {
            stub = VideoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RobotsTextGroupBean convertRobotsTextGroupToBean(RobotsTextGroup stub)
    {
        RobotsTextGroupBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RobotsTextGroupBean();
        } else if(stub instanceof RobotsTextGroupBean) {
        	bean = (RobotsTextGroupBean) stub;
        } else {
        	bean = new RobotsTextGroupBean(stub);
        }
        return bean;
    }
    public static List<RobotsTextGroupBean> convertRobotsTextGroupListToBeanList(List<RobotsTextGroup> stubList)
    {
        List<RobotsTextGroupBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RobotsTextGroup>();
        } else {
            beanList = new ArrayList<RobotsTextGroupBean>();
            for(RobotsTextGroup stub : stubList) {
                beanList.add(convertRobotsTextGroupToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RobotsTextGroup> convertRobotsTextGroupListStubToBeanList(RobotsTextGroupListStub listStub)
    {
        return convertRobotsTextGroupListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<RobotsTextGroup> convertRobotsTextGroupListStubToBeanList(RobotsTextGroupListStub listStub, StringCursor forwardCursor)
    {
        List<RobotsTextGroup> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RobotsTextGroup>();
        } else {
            beanList = new ArrayList<RobotsTextGroup>();
            for(RobotsTextGroupStub stub : listStub.getList()) {
            	beanList.add(convertRobotsTextGroupToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static RobotsTextGroupStub convertRobotsTextGroupToStub(RobotsTextGroup bean)
    {
        RobotsTextGroupStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RobotsTextGroupStub();
        } else if(bean instanceof RobotsTextGroupStub) {
            stub = (RobotsTextGroupStub) bean;
        } else if(bean instanceof RobotsTextGroupBean && ((RobotsTextGroupBean) bean).isWrapper()) {
            stub = ((RobotsTextGroupBean) bean).getStub();
        } else {
            stub = RobotsTextGroupStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RobotsTextFileBean convertRobotsTextFileToBean(RobotsTextFile stub)
    {
        RobotsTextFileBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RobotsTextFileBean();
        } else if(stub instanceof RobotsTextFileBean) {
        	bean = (RobotsTextFileBean) stub;
        } else {
        	bean = new RobotsTextFileBean(stub);
        }
        return bean;
    }
    public static List<RobotsTextFileBean> convertRobotsTextFileListToBeanList(List<RobotsTextFile> stubList)
    {
        List<RobotsTextFileBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RobotsTextFile>();
        } else {
            beanList = new ArrayList<RobotsTextFileBean>();
            for(RobotsTextFile stub : stubList) {
                beanList.add(convertRobotsTextFileToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RobotsTextFile> convertRobotsTextFileListStubToBeanList(RobotsTextFileListStub listStub)
    {
        return convertRobotsTextFileListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<RobotsTextFile> convertRobotsTextFileListStubToBeanList(RobotsTextFileListStub listStub, StringCursor forwardCursor)
    {
        List<RobotsTextFile> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RobotsTextFile>();
        } else {
            beanList = new ArrayList<RobotsTextFile>();
            for(RobotsTextFileStub stub : listStub.getList()) {
            	beanList.add(convertRobotsTextFileToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static RobotsTextFileStub convertRobotsTextFileToStub(RobotsTextFile bean)
    {
        RobotsTextFileStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RobotsTextFileStub();
        } else if(bean instanceof RobotsTextFileStub) {
            stub = (RobotsTextFileStub) bean;
        } else if(bean instanceof RobotsTextFileBean && ((RobotsTextFileBean) bean).isWrapper()) {
            stub = ((RobotsTextFileBean) bean).getStub();
        } else {
            stub = RobotsTextFileStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static RobotsTextRefreshBean convertRobotsTextRefreshToBean(RobotsTextRefresh stub)
    {
        RobotsTextRefreshBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new RobotsTextRefreshBean();
        } else if(stub instanceof RobotsTextRefreshBean) {
        	bean = (RobotsTextRefreshBean) stub;
        } else {
        	bean = new RobotsTextRefreshBean(stub);
        }
        return bean;
    }
    public static List<RobotsTextRefreshBean> convertRobotsTextRefreshListToBeanList(List<RobotsTextRefresh> stubList)
    {
        List<RobotsTextRefreshBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<RobotsTextRefresh>();
        } else {
            beanList = new ArrayList<RobotsTextRefreshBean>();
            for(RobotsTextRefresh stub : stubList) {
                beanList.add(convertRobotsTextRefreshToBean(stub));
            }
        }
        return beanList;
    }
    public static List<RobotsTextRefresh> convertRobotsTextRefreshListStubToBeanList(RobotsTextRefreshListStub listStub)
    {
        return convertRobotsTextRefreshListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<RobotsTextRefresh> convertRobotsTextRefreshListStubToBeanList(RobotsTextRefreshListStub listStub, StringCursor forwardCursor)
    {
        List<RobotsTextRefresh> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<RobotsTextRefresh>();
        } else {
            beanList = new ArrayList<RobotsTextRefresh>();
            for(RobotsTextRefreshStub stub : listStub.getList()) {
            	beanList.add(convertRobotsTextRefreshToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static RobotsTextRefreshStub convertRobotsTextRefreshToStub(RobotsTextRefresh bean)
    {
        RobotsTextRefreshStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new RobotsTextRefreshStub();
        } else if(bean instanceof RobotsTextRefreshStub) {
            stub = (RobotsTextRefreshStub) bean;
        } else if(bean instanceof RobotsTextRefreshBean && ((RobotsTextRefreshBean) bean).isWrapper()) {
            stub = ((RobotsTextRefreshBean) bean).getStub();
        } else {
            stub = RobotsTextRefreshStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgContactInfoStructBean convertOgContactInfoStructToBean(OgContactInfoStruct stub)
    {
        OgContactInfoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgContactInfoStructBean();
        } else if(stub instanceof OgContactInfoStructBean) {
        	bean = (OgContactInfoStructBean) stub;
        } else {
        	bean = new OgContactInfoStructBean(stub);
        }
        return bean;
    }
    public static List<OgContactInfoStructBean> convertOgContactInfoStructListToBeanList(List<OgContactInfoStruct> stubList)
    {
        List<OgContactInfoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgContactInfoStruct>();
        } else {
            beanList = new ArrayList<OgContactInfoStructBean>();
            for(OgContactInfoStruct stub : stubList) {
                beanList.add(convertOgContactInfoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgContactInfoStruct> convertOgContactInfoStructListStubToBeanList(OgContactInfoStructListStub listStub)
    {
        return convertOgContactInfoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgContactInfoStruct> convertOgContactInfoStructListStubToBeanList(OgContactInfoStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgContactInfoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgContactInfoStruct>();
        } else {
            beanList = new ArrayList<OgContactInfoStruct>();
            for(OgContactInfoStructStub stub : listStub.getList()) {
            	beanList.add(convertOgContactInfoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgContactInfoStructStub convertOgContactInfoStructToStub(OgContactInfoStruct bean)
    {
        OgContactInfoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgContactInfoStructStub();
        } else if(bean instanceof OgContactInfoStructStub) {
            stub = (OgContactInfoStructStub) bean;
        } else if(bean instanceof OgContactInfoStructBean && ((OgContactInfoStructBean) bean).isWrapper()) {
            stub = ((OgContactInfoStructBean) bean).getStub();
        } else {
            stub = OgContactInfoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgImageStructBean convertOgImageStructToBean(OgImageStruct stub)
    {
        OgImageStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgImageStructBean();
        } else if(stub instanceof OgImageStructBean) {
        	bean = (OgImageStructBean) stub;
        } else {
        	bean = new OgImageStructBean(stub);
        }
        return bean;
    }
    public static List<OgImageStructBean> convertOgImageStructListToBeanList(List<OgImageStruct> stubList)
    {
        List<OgImageStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgImageStruct>();
        } else {
            beanList = new ArrayList<OgImageStructBean>();
            for(OgImageStruct stub : stubList) {
                beanList.add(convertOgImageStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgImageStruct> convertOgImageStructListStubToBeanList(OgImageStructListStub listStub)
    {
        return convertOgImageStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgImageStruct> convertOgImageStructListStubToBeanList(OgImageStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgImageStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgImageStruct>();
        } else {
            beanList = new ArrayList<OgImageStruct>();
            for(OgImageStructStub stub : listStub.getList()) {
            	beanList.add(convertOgImageStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgImageStructStub convertOgImageStructToStub(OgImageStruct bean)
    {
        OgImageStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgImageStructStub();
        } else if(bean instanceof OgImageStructStub) {
            stub = (OgImageStructStub) bean;
        } else if(bean instanceof OgImageStructBean && ((OgImageStructBean) bean).isWrapper()) {
            stub = ((OgImageStructBean) bean).getStub();
        } else {
            stub = OgImageStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgAudioStructBean convertOgAudioStructToBean(OgAudioStruct stub)
    {
        OgAudioStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgAudioStructBean();
        } else if(stub instanceof OgAudioStructBean) {
        	bean = (OgAudioStructBean) stub;
        } else {
        	bean = new OgAudioStructBean(stub);
        }
        return bean;
    }
    public static List<OgAudioStructBean> convertOgAudioStructListToBeanList(List<OgAudioStruct> stubList)
    {
        List<OgAudioStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgAudioStruct>();
        } else {
            beanList = new ArrayList<OgAudioStructBean>();
            for(OgAudioStruct stub : stubList) {
                beanList.add(convertOgAudioStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgAudioStruct> convertOgAudioStructListStubToBeanList(OgAudioStructListStub listStub)
    {
        return convertOgAudioStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgAudioStruct> convertOgAudioStructListStubToBeanList(OgAudioStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgAudioStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgAudioStruct>();
        } else {
            beanList = new ArrayList<OgAudioStruct>();
            for(OgAudioStructStub stub : listStub.getList()) {
            	beanList.add(convertOgAudioStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgAudioStructStub convertOgAudioStructToStub(OgAudioStruct bean)
    {
        OgAudioStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgAudioStructStub();
        } else if(bean instanceof OgAudioStructStub) {
            stub = (OgAudioStructStub) bean;
        } else if(bean instanceof OgAudioStructBean && ((OgAudioStructBean) bean).isWrapper()) {
            stub = ((OgAudioStructBean) bean).getStub();
        } else {
            stub = OgAudioStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgVideoStructBean convertOgVideoStructToBean(OgVideoStruct stub)
    {
        OgVideoStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgVideoStructBean();
        } else if(stub instanceof OgVideoStructBean) {
        	bean = (OgVideoStructBean) stub;
        } else {
        	bean = new OgVideoStructBean(stub);
        }
        return bean;
    }
    public static List<OgVideoStructBean> convertOgVideoStructListToBeanList(List<OgVideoStruct> stubList)
    {
        List<OgVideoStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgVideoStruct>();
        } else {
            beanList = new ArrayList<OgVideoStructBean>();
            for(OgVideoStruct stub : stubList) {
                beanList.add(convertOgVideoStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgVideoStruct> convertOgVideoStructListStubToBeanList(OgVideoStructListStub listStub)
    {
        return convertOgVideoStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgVideoStruct> convertOgVideoStructListStubToBeanList(OgVideoStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgVideoStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgVideoStruct>();
        } else {
            beanList = new ArrayList<OgVideoStruct>();
            for(OgVideoStructStub stub : listStub.getList()) {
            	beanList.add(convertOgVideoStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgVideoStructStub convertOgVideoStructToStub(OgVideoStruct bean)
    {
        OgVideoStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgVideoStructStub();
        } else if(bean instanceof OgVideoStructStub) {
            stub = (OgVideoStructStub) bean;
        } else if(bean instanceof OgVideoStructBean && ((OgVideoStructBean) bean).isWrapper()) {
            stub = ((OgVideoStructBean) bean).getStub();
        } else {
            stub = OgVideoStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgGeoPointStructBean convertOgGeoPointStructToBean(OgGeoPointStruct stub)
    {
        OgGeoPointStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgGeoPointStructBean();
        } else if(stub instanceof OgGeoPointStructBean) {
        	bean = (OgGeoPointStructBean) stub;
        } else {
        	bean = new OgGeoPointStructBean(stub);
        }
        return bean;
    }
    public static List<OgGeoPointStructBean> convertOgGeoPointStructListToBeanList(List<OgGeoPointStruct> stubList)
    {
        List<OgGeoPointStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgGeoPointStruct>();
        } else {
            beanList = new ArrayList<OgGeoPointStructBean>();
            for(OgGeoPointStruct stub : stubList) {
                beanList.add(convertOgGeoPointStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgGeoPointStruct> convertOgGeoPointStructListStubToBeanList(OgGeoPointStructListStub listStub)
    {
        return convertOgGeoPointStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgGeoPointStruct> convertOgGeoPointStructListStubToBeanList(OgGeoPointStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgGeoPointStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgGeoPointStruct>();
        } else {
            beanList = new ArrayList<OgGeoPointStruct>();
            for(OgGeoPointStructStub stub : listStub.getList()) {
            	beanList.add(convertOgGeoPointStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgGeoPointStructStub convertOgGeoPointStructToStub(OgGeoPointStruct bean)
    {
        OgGeoPointStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgGeoPointStructStub();
        } else if(bean instanceof OgGeoPointStructStub) {
            stub = (OgGeoPointStructStub) bean;
        } else if(bean instanceof OgGeoPointStructBean && ((OgGeoPointStructBean) bean).isWrapper()) {
            stub = ((OgGeoPointStructBean) bean).getStub();
        } else {
            stub = OgGeoPointStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgQuantityStructBean convertOgQuantityStructToBean(OgQuantityStruct stub)
    {
        OgQuantityStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgQuantityStructBean();
        } else if(stub instanceof OgQuantityStructBean) {
        	bean = (OgQuantityStructBean) stub;
        } else {
        	bean = new OgQuantityStructBean(stub);
        }
        return bean;
    }
    public static List<OgQuantityStructBean> convertOgQuantityStructListToBeanList(List<OgQuantityStruct> stubList)
    {
        List<OgQuantityStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgQuantityStruct>();
        } else {
            beanList = new ArrayList<OgQuantityStructBean>();
            for(OgQuantityStruct stub : stubList) {
                beanList.add(convertOgQuantityStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgQuantityStruct> convertOgQuantityStructListStubToBeanList(OgQuantityStructListStub listStub)
    {
        return convertOgQuantityStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgQuantityStruct> convertOgQuantityStructListStubToBeanList(OgQuantityStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgQuantityStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgQuantityStruct>();
        } else {
            beanList = new ArrayList<OgQuantityStruct>();
            for(OgQuantityStructStub stub : listStub.getList()) {
            	beanList.add(convertOgQuantityStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgQuantityStructStub convertOgQuantityStructToStub(OgQuantityStruct bean)
    {
        OgQuantityStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgQuantityStructStub();
        } else if(bean instanceof OgQuantityStructStub) {
            stub = (OgQuantityStructStub) bean;
        } else if(bean instanceof OgQuantityStructBean && ((OgQuantityStructBean) bean).isWrapper()) {
            stub = ((OgQuantityStructBean) bean).getStub();
        } else {
            stub = OgQuantityStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgActorStructBean convertOgActorStructToBean(OgActorStruct stub)
    {
        OgActorStructBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgActorStructBean();
        } else if(stub instanceof OgActorStructBean) {
        	bean = (OgActorStructBean) stub;
        } else {
        	bean = new OgActorStructBean(stub);
        }
        return bean;
    }
    public static List<OgActorStructBean> convertOgActorStructListToBeanList(List<OgActorStruct> stubList)
    {
        List<OgActorStructBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgActorStruct>();
        } else {
            beanList = new ArrayList<OgActorStructBean>();
            for(OgActorStruct stub : stubList) {
                beanList.add(convertOgActorStructToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgActorStruct> convertOgActorStructListStubToBeanList(OgActorStructListStub listStub)
    {
        return convertOgActorStructListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgActorStruct> convertOgActorStructListStubToBeanList(OgActorStructListStub listStub, StringCursor forwardCursor)
    {
        List<OgActorStruct> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgActorStruct>();
        } else {
            beanList = new ArrayList<OgActorStruct>();
            for(OgActorStructStub stub : listStub.getList()) {
            	beanList.add(convertOgActorStructToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgActorStructStub convertOgActorStructToStub(OgActorStruct bean)
    {
        OgActorStructStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgActorStructStub();
        } else if(bean instanceof OgActorStructStub) {
            stub = (OgActorStructStub) bean;
        } else if(bean instanceof OgActorStructBean && ((OgActorStructBean) bean).isWrapper()) {
            stub = ((OgActorStructBean) bean).getStub();
        } else {
            stub = OgActorStructStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgProfileBean convertOgProfileToBean(OgProfile stub)
    {
        OgProfileBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgProfileBean();
        } else if(stub instanceof OgProfileBean) {
        	bean = (OgProfileBean) stub;
        } else {
        	bean = new OgProfileBean(stub);
        }
        return bean;
    }
    public static List<OgProfileBean> convertOgProfileListToBeanList(List<OgProfile> stubList)
    {
        List<OgProfileBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgProfile>();
        } else {
            beanList = new ArrayList<OgProfileBean>();
            for(OgProfile stub : stubList) {
                beanList.add(convertOgProfileToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgProfile> convertOgProfileListStubToBeanList(OgProfileListStub listStub)
    {
        return convertOgProfileListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgProfile> convertOgProfileListStubToBeanList(OgProfileListStub listStub, StringCursor forwardCursor)
    {
        List<OgProfile> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgProfile>();
        } else {
            beanList = new ArrayList<OgProfile>();
            for(OgProfileStub stub : listStub.getList()) {
            	beanList.add(convertOgProfileToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgProfileStub convertOgProfileToStub(OgProfile bean)
    {
        OgProfileStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgProfileStub();
        } else if(bean instanceof OgProfileStub) {
            stub = (OgProfileStub) bean;
        } else if(bean instanceof OgProfileBean && ((OgProfileBean) bean).isWrapper()) {
            stub = ((OgProfileBean) bean).getStub();
        } else {
            stub = OgProfileStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgWebsiteBean convertOgWebsiteToBean(OgWebsite stub)
    {
        OgWebsiteBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgWebsiteBean();
        } else if(stub instanceof OgWebsiteBean) {
        	bean = (OgWebsiteBean) stub;
        } else {
        	bean = new OgWebsiteBean(stub);
        }
        return bean;
    }
    public static List<OgWebsiteBean> convertOgWebsiteListToBeanList(List<OgWebsite> stubList)
    {
        List<OgWebsiteBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgWebsite>();
        } else {
            beanList = new ArrayList<OgWebsiteBean>();
            for(OgWebsite stub : stubList) {
                beanList.add(convertOgWebsiteToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgWebsite> convertOgWebsiteListStubToBeanList(OgWebsiteListStub listStub)
    {
        return convertOgWebsiteListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgWebsite> convertOgWebsiteListStubToBeanList(OgWebsiteListStub listStub, StringCursor forwardCursor)
    {
        List<OgWebsite> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgWebsite>();
        } else {
            beanList = new ArrayList<OgWebsite>();
            for(OgWebsiteStub stub : listStub.getList()) {
            	beanList.add(convertOgWebsiteToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgWebsiteStub convertOgWebsiteToStub(OgWebsite bean)
    {
        OgWebsiteStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgWebsiteStub();
        } else if(bean instanceof OgWebsiteStub) {
            stub = (OgWebsiteStub) bean;
        } else if(bean instanceof OgWebsiteBean && ((OgWebsiteBean) bean).isWrapper()) {
            stub = ((OgWebsiteBean) bean).getStub();
        } else {
            stub = OgWebsiteStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgBlogBean convertOgBlogToBean(OgBlog stub)
    {
        OgBlogBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgBlogBean();
        } else if(stub instanceof OgBlogBean) {
        	bean = (OgBlogBean) stub;
        } else {
        	bean = new OgBlogBean(stub);
        }
        return bean;
    }
    public static List<OgBlogBean> convertOgBlogListToBeanList(List<OgBlog> stubList)
    {
        List<OgBlogBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgBlog>();
        } else {
            beanList = new ArrayList<OgBlogBean>();
            for(OgBlog stub : stubList) {
                beanList.add(convertOgBlogToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgBlog> convertOgBlogListStubToBeanList(OgBlogListStub listStub)
    {
        return convertOgBlogListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgBlog> convertOgBlogListStubToBeanList(OgBlogListStub listStub, StringCursor forwardCursor)
    {
        List<OgBlog> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgBlog>();
        } else {
            beanList = new ArrayList<OgBlog>();
            for(OgBlogStub stub : listStub.getList()) {
            	beanList.add(convertOgBlogToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgBlogStub convertOgBlogToStub(OgBlog bean)
    {
        OgBlogStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgBlogStub();
        } else if(bean instanceof OgBlogStub) {
            stub = (OgBlogStub) bean;
        } else if(bean instanceof OgBlogBean && ((OgBlogBean) bean).isWrapper()) {
            stub = ((OgBlogBean) bean).getStub();
        } else {
            stub = OgBlogStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgArticleBean convertOgArticleToBean(OgArticle stub)
    {
        OgArticleBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgArticleBean();
        } else if(stub instanceof OgArticleBean) {
        	bean = (OgArticleBean) stub;
        } else {
        	bean = new OgArticleBean(stub);
        }
        return bean;
    }
    public static List<OgArticleBean> convertOgArticleListToBeanList(List<OgArticle> stubList)
    {
        List<OgArticleBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgArticle>();
        } else {
            beanList = new ArrayList<OgArticleBean>();
            for(OgArticle stub : stubList) {
                beanList.add(convertOgArticleToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgArticle> convertOgArticleListStubToBeanList(OgArticleListStub listStub)
    {
        return convertOgArticleListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgArticle> convertOgArticleListStubToBeanList(OgArticleListStub listStub, StringCursor forwardCursor)
    {
        List<OgArticle> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgArticle>();
        } else {
            beanList = new ArrayList<OgArticle>();
            for(OgArticleStub stub : listStub.getList()) {
            	beanList.add(convertOgArticleToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgArticleStub convertOgArticleToStub(OgArticle bean)
    {
        OgArticleStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgArticleStub();
        } else if(bean instanceof OgArticleStub) {
            stub = (OgArticleStub) bean;
        } else if(bean instanceof OgArticleBean && ((OgArticleBean) bean).isWrapper()) {
            stub = ((OgArticleBean) bean).getStub();
        } else {
            stub = OgArticleStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgBookBean convertOgBookToBean(OgBook stub)
    {
        OgBookBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgBookBean();
        } else if(stub instanceof OgBookBean) {
        	bean = (OgBookBean) stub;
        } else {
        	bean = new OgBookBean(stub);
        }
        return bean;
    }
    public static List<OgBookBean> convertOgBookListToBeanList(List<OgBook> stubList)
    {
        List<OgBookBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgBook>();
        } else {
            beanList = new ArrayList<OgBookBean>();
            for(OgBook stub : stubList) {
                beanList.add(convertOgBookToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgBook> convertOgBookListStubToBeanList(OgBookListStub listStub)
    {
        return convertOgBookListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgBook> convertOgBookListStubToBeanList(OgBookListStub listStub, StringCursor forwardCursor)
    {
        List<OgBook> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgBook>();
        } else {
            beanList = new ArrayList<OgBook>();
            for(OgBookStub stub : listStub.getList()) {
            	beanList.add(convertOgBookToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgBookStub convertOgBookToStub(OgBook bean)
    {
        OgBookStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgBookStub();
        } else if(bean instanceof OgBookStub) {
            stub = (OgBookStub) bean;
        } else if(bean instanceof OgBookBean && ((OgBookBean) bean).isWrapper()) {
            stub = ((OgBookBean) bean).getStub();
        } else {
            stub = OgBookStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgVideoBean convertOgVideoToBean(OgVideo stub)
    {
        OgVideoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgVideoBean();
        } else if(stub instanceof OgVideoBean) {
        	bean = (OgVideoBean) stub;
        } else {
        	bean = new OgVideoBean(stub);
        }
        return bean;
    }
    public static List<OgVideoBean> convertOgVideoListToBeanList(List<OgVideo> stubList)
    {
        List<OgVideoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgVideo>();
        } else {
            beanList = new ArrayList<OgVideoBean>();
            for(OgVideo stub : stubList) {
                beanList.add(convertOgVideoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgVideo> convertOgVideoListStubToBeanList(OgVideoListStub listStub)
    {
        return convertOgVideoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgVideo> convertOgVideoListStubToBeanList(OgVideoListStub listStub, StringCursor forwardCursor)
    {
        List<OgVideo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgVideo>();
        } else {
            beanList = new ArrayList<OgVideo>();
            for(OgVideoStub stub : listStub.getList()) {
            	beanList.add(convertOgVideoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgVideoStub convertOgVideoToStub(OgVideo bean)
    {
        OgVideoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgVideoStub();
        } else if(bean instanceof OgVideoStub) {
            stub = (OgVideoStub) bean;
        } else if(bean instanceof OgVideoBean && ((OgVideoBean) bean).isWrapper()) {
            stub = ((OgVideoBean) bean).getStub();
        } else {
            stub = OgVideoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgMovieBean convertOgMovieToBean(OgMovie stub)
    {
        OgMovieBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgMovieBean();
        } else if(stub instanceof OgMovieBean) {
        	bean = (OgMovieBean) stub;
        } else {
        	bean = new OgMovieBean(stub);
        }
        return bean;
    }
    public static List<OgMovieBean> convertOgMovieListToBeanList(List<OgMovie> stubList)
    {
        List<OgMovieBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgMovie>();
        } else {
            beanList = new ArrayList<OgMovieBean>();
            for(OgMovie stub : stubList) {
                beanList.add(convertOgMovieToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgMovie> convertOgMovieListStubToBeanList(OgMovieListStub listStub)
    {
        return convertOgMovieListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgMovie> convertOgMovieListStubToBeanList(OgMovieListStub listStub, StringCursor forwardCursor)
    {
        List<OgMovie> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgMovie>();
        } else {
            beanList = new ArrayList<OgMovie>();
            for(OgMovieStub stub : listStub.getList()) {
            	beanList.add(convertOgMovieToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgMovieStub convertOgMovieToStub(OgMovie bean)
    {
        OgMovieStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgMovieStub();
        } else if(bean instanceof OgMovieStub) {
            stub = (OgMovieStub) bean;
        } else if(bean instanceof OgMovieBean && ((OgMovieBean) bean).isWrapper()) {
            stub = ((OgMovieBean) bean).getStub();
        } else {
            stub = OgMovieStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgTvShowBean convertOgTvShowToBean(OgTvShow stub)
    {
        OgTvShowBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgTvShowBean();
        } else if(stub instanceof OgTvShowBean) {
        	bean = (OgTvShowBean) stub;
        } else {
        	bean = new OgTvShowBean(stub);
        }
        return bean;
    }
    public static List<OgTvShowBean> convertOgTvShowListToBeanList(List<OgTvShow> stubList)
    {
        List<OgTvShowBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgTvShow>();
        } else {
            beanList = new ArrayList<OgTvShowBean>();
            for(OgTvShow stub : stubList) {
                beanList.add(convertOgTvShowToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgTvShow> convertOgTvShowListStubToBeanList(OgTvShowListStub listStub)
    {
        return convertOgTvShowListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgTvShow> convertOgTvShowListStubToBeanList(OgTvShowListStub listStub, StringCursor forwardCursor)
    {
        List<OgTvShow> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgTvShow>();
        } else {
            beanList = new ArrayList<OgTvShow>();
            for(OgTvShowStub stub : listStub.getList()) {
            	beanList.add(convertOgTvShowToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgTvShowStub convertOgTvShowToStub(OgTvShow bean)
    {
        OgTvShowStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgTvShowStub();
        } else if(bean instanceof OgTvShowStub) {
            stub = (OgTvShowStub) bean;
        } else if(bean instanceof OgTvShowBean && ((OgTvShowBean) bean).isWrapper()) {
            stub = ((OgTvShowBean) bean).getStub();
        } else {
            stub = OgTvShowStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OgTvEpisodeBean convertOgTvEpisodeToBean(OgTvEpisode stub)
    {
        OgTvEpisodeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OgTvEpisodeBean();
        } else if(stub instanceof OgTvEpisodeBean) {
        	bean = (OgTvEpisodeBean) stub;
        } else {
        	bean = new OgTvEpisodeBean(stub);
        }
        return bean;
    }
    public static List<OgTvEpisodeBean> convertOgTvEpisodeListToBeanList(List<OgTvEpisode> stubList)
    {
        List<OgTvEpisodeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OgTvEpisode>();
        } else {
            beanList = new ArrayList<OgTvEpisodeBean>();
            for(OgTvEpisode stub : stubList) {
                beanList.add(convertOgTvEpisodeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OgTvEpisode> convertOgTvEpisodeListStubToBeanList(OgTvEpisodeListStub listStub)
    {
        return convertOgTvEpisodeListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OgTvEpisode> convertOgTvEpisodeListStubToBeanList(OgTvEpisodeListStub listStub, StringCursor forwardCursor)
    {
        List<OgTvEpisode> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OgTvEpisode>();
        } else {
            beanList = new ArrayList<OgTvEpisode>();
            for(OgTvEpisodeStub stub : listStub.getList()) {
            	beanList.add(convertOgTvEpisodeToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OgTvEpisodeStub convertOgTvEpisodeToStub(OgTvEpisode bean)
    {
        OgTvEpisodeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OgTvEpisodeStub();
        } else if(bean instanceof OgTvEpisodeStub) {
            stub = (OgTvEpisodeStub) bean;
        } else if(bean instanceof OgTvEpisodeBean && ((OgTvEpisodeBean) bean).isWrapper()) {
            stub = ((OgTvEpisodeBean) bean).getStub();
        } else {
            stub = OgTvEpisodeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterCardAppInfoBean convertTwitterCardAppInfoToBean(TwitterCardAppInfo stub)
    {
        TwitterCardAppInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterCardAppInfoBean();
        } else if(stub instanceof TwitterCardAppInfoBean) {
        	bean = (TwitterCardAppInfoBean) stub;
        } else {
        	bean = new TwitterCardAppInfoBean(stub);
        }
        return bean;
    }
    public static List<TwitterCardAppInfoBean> convertTwitterCardAppInfoListToBeanList(List<TwitterCardAppInfo> stubList)
    {
        List<TwitterCardAppInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterCardAppInfo>();
        } else {
            beanList = new ArrayList<TwitterCardAppInfoBean>();
            for(TwitterCardAppInfo stub : stubList) {
                beanList.add(convertTwitterCardAppInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterCardAppInfo> convertTwitterCardAppInfoListStubToBeanList(TwitterCardAppInfoListStub listStub)
    {
        return convertTwitterCardAppInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterCardAppInfo> convertTwitterCardAppInfoListStubToBeanList(TwitterCardAppInfoListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterCardAppInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterCardAppInfo>();
        } else {
            beanList = new ArrayList<TwitterCardAppInfo>();
            for(TwitterCardAppInfoStub stub : listStub.getList()) {
            	beanList.add(convertTwitterCardAppInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterCardAppInfoStub convertTwitterCardAppInfoToStub(TwitterCardAppInfo bean)
    {
        TwitterCardAppInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterCardAppInfoStub();
        } else if(bean instanceof TwitterCardAppInfoStub) {
            stub = (TwitterCardAppInfoStub) bean;
        } else if(bean instanceof TwitterCardAppInfoBean && ((TwitterCardAppInfoBean) bean).isWrapper()) {
            stub = ((TwitterCardAppInfoBean) bean).getStub();
        } else {
            stub = TwitterCardAppInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterCardProductDataBean convertTwitterCardProductDataToBean(TwitterCardProductData stub)
    {
        TwitterCardProductDataBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterCardProductDataBean();
        } else if(stub instanceof TwitterCardProductDataBean) {
        	bean = (TwitterCardProductDataBean) stub;
        } else {
        	bean = new TwitterCardProductDataBean(stub);
        }
        return bean;
    }
    public static List<TwitterCardProductDataBean> convertTwitterCardProductDataListToBeanList(List<TwitterCardProductData> stubList)
    {
        List<TwitterCardProductDataBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterCardProductData>();
        } else {
            beanList = new ArrayList<TwitterCardProductDataBean>();
            for(TwitterCardProductData stub : stubList) {
                beanList.add(convertTwitterCardProductDataToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterCardProductData> convertTwitterCardProductDataListStubToBeanList(TwitterCardProductDataListStub listStub)
    {
        return convertTwitterCardProductDataListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterCardProductData> convertTwitterCardProductDataListStubToBeanList(TwitterCardProductDataListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterCardProductData> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterCardProductData>();
        } else {
            beanList = new ArrayList<TwitterCardProductData>();
            for(TwitterCardProductDataStub stub : listStub.getList()) {
            	beanList.add(convertTwitterCardProductDataToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterCardProductDataStub convertTwitterCardProductDataToStub(TwitterCardProductData bean)
    {
        TwitterCardProductDataStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterCardProductDataStub();
        } else if(bean instanceof TwitterCardProductDataStub) {
            stub = (TwitterCardProductDataStub) bean;
        } else if(bean instanceof TwitterCardProductDataBean && ((TwitterCardProductDataBean) bean).isWrapper()) {
            stub = ((TwitterCardProductDataBean) bean).getStub();
        } else {
            stub = TwitterCardProductDataStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterSummaryCardBean convertTwitterSummaryCardToBean(TwitterSummaryCard stub)
    {
        TwitterSummaryCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterSummaryCardBean();
        } else if(stub instanceof TwitterSummaryCardBean) {
        	bean = (TwitterSummaryCardBean) stub;
        } else {
        	bean = new TwitterSummaryCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterSummaryCardBean> convertTwitterSummaryCardListToBeanList(List<TwitterSummaryCard> stubList)
    {
        List<TwitterSummaryCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterSummaryCard>();
        } else {
            beanList = new ArrayList<TwitterSummaryCardBean>();
            for(TwitterSummaryCard stub : stubList) {
                beanList.add(convertTwitterSummaryCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterSummaryCard> convertTwitterSummaryCardListStubToBeanList(TwitterSummaryCardListStub listStub)
    {
        return convertTwitterSummaryCardListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterSummaryCard> convertTwitterSummaryCardListStubToBeanList(TwitterSummaryCardListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterSummaryCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterSummaryCard>();
        } else {
            beanList = new ArrayList<TwitterSummaryCard>();
            for(TwitterSummaryCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterSummaryCardToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterSummaryCardStub convertTwitterSummaryCardToStub(TwitterSummaryCard bean)
    {
        TwitterSummaryCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterSummaryCardStub();
        } else if(bean instanceof TwitterSummaryCardStub) {
            stub = (TwitterSummaryCardStub) bean;
        } else if(bean instanceof TwitterSummaryCardBean && ((TwitterSummaryCardBean) bean).isWrapper()) {
            stub = ((TwitterSummaryCardBean) bean).getStub();
        } else {
            stub = TwitterSummaryCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterPhotoCardBean convertTwitterPhotoCardToBean(TwitterPhotoCard stub)
    {
        TwitterPhotoCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterPhotoCardBean();
        } else if(stub instanceof TwitterPhotoCardBean) {
        	bean = (TwitterPhotoCardBean) stub;
        } else {
        	bean = new TwitterPhotoCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterPhotoCardBean> convertTwitterPhotoCardListToBeanList(List<TwitterPhotoCard> stubList)
    {
        List<TwitterPhotoCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterPhotoCard>();
        } else {
            beanList = new ArrayList<TwitterPhotoCardBean>();
            for(TwitterPhotoCard stub : stubList) {
                beanList.add(convertTwitterPhotoCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterPhotoCard> convertTwitterPhotoCardListStubToBeanList(TwitterPhotoCardListStub listStub)
    {
        return convertTwitterPhotoCardListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterPhotoCard> convertTwitterPhotoCardListStubToBeanList(TwitterPhotoCardListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterPhotoCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterPhotoCard>();
        } else {
            beanList = new ArrayList<TwitterPhotoCard>();
            for(TwitterPhotoCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterPhotoCardToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterPhotoCardStub convertTwitterPhotoCardToStub(TwitterPhotoCard bean)
    {
        TwitterPhotoCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterPhotoCardStub();
        } else if(bean instanceof TwitterPhotoCardStub) {
            stub = (TwitterPhotoCardStub) bean;
        } else if(bean instanceof TwitterPhotoCardBean && ((TwitterPhotoCardBean) bean).isWrapper()) {
            stub = ((TwitterPhotoCardBean) bean).getStub();
        } else {
            stub = TwitterPhotoCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterGalleryCardBean convertTwitterGalleryCardToBean(TwitterGalleryCard stub)
    {
        TwitterGalleryCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterGalleryCardBean();
        } else if(stub instanceof TwitterGalleryCardBean) {
        	bean = (TwitterGalleryCardBean) stub;
        } else {
        	bean = new TwitterGalleryCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterGalleryCardBean> convertTwitterGalleryCardListToBeanList(List<TwitterGalleryCard> stubList)
    {
        List<TwitterGalleryCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterGalleryCard>();
        } else {
            beanList = new ArrayList<TwitterGalleryCardBean>();
            for(TwitterGalleryCard stub : stubList) {
                beanList.add(convertTwitterGalleryCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterGalleryCard> convertTwitterGalleryCardListStubToBeanList(TwitterGalleryCardListStub listStub)
    {
        return convertTwitterGalleryCardListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterGalleryCard> convertTwitterGalleryCardListStubToBeanList(TwitterGalleryCardListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterGalleryCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterGalleryCard>();
        } else {
            beanList = new ArrayList<TwitterGalleryCard>();
            for(TwitterGalleryCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterGalleryCardToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterGalleryCardStub convertTwitterGalleryCardToStub(TwitterGalleryCard bean)
    {
        TwitterGalleryCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterGalleryCardStub();
        } else if(bean instanceof TwitterGalleryCardStub) {
            stub = (TwitterGalleryCardStub) bean;
        } else if(bean instanceof TwitterGalleryCardBean && ((TwitterGalleryCardBean) bean).isWrapper()) {
            stub = ((TwitterGalleryCardBean) bean).getStub();
        } else {
            stub = TwitterGalleryCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterAppCardBean convertTwitterAppCardToBean(TwitterAppCard stub)
    {
        TwitterAppCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterAppCardBean();
        } else if(stub instanceof TwitterAppCardBean) {
        	bean = (TwitterAppCardBean) stub;
        } else {
        	bean = new TwitterAppCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterAppCardBean> convertTwitterAppCardListToBeanList(List<TwitterAppCard> stubList)
    {
        List<TwitterAppCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterAppCard>();
        } else {
            beanList = new ArrayList<TwitterAppCardBean>();
            for(TwitterAppCard stub : stubList) {
                beanList.add(convertTwitterAppCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterAppCard> convertTwitterAppCardListStubToBeanList(TwitterAppCardListStub listStub)
    {
        return convertTwitterAppCardListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterAppCard> convertTwitterAppCardListStubToBeanList(TwitterAppCardListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterAppCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterAppCard>();
        } else {
            beanList = new ArrayList<TwitterAppCard>();
            for(TwitterAppCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterAppCardToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterAppCardStub convertTwitterAppCardToStub(TwitterAppCard bean)
    {
        TwitterAppCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterAppCardStub();
        } else if(bean instanceof TwitterAppCardStub) {
            stub = (TwitterAppCardStub) bean;
        } else if(bean instanceof TwitterAppCardBean && ((TwitterAppCardBean) bean).isWrapper()) {
            stub = ((TwitterAppCardBean) bean).getStub();
        } else {
            stub = TwitterAppCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterPlayerCardBean convertTwitterPlayerCardToBean(TwitterPlayerCard stub)
    {
        TwitterPlayerCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterPlayerCardBean();
        } else if(stub instanceof TwitterPlayerCardBean) {
        	bean = (TwitterPlayerCardBean) stub;
        } else {
        	bean = new TwitterPlayerCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterPlayerCardBean> convertTwitterPlayerCardListToBeanList(List<TwitterPlayerCard> stubList)
    {
        List<TwitterPlayerCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterPlayerCard>();
        } else {
            beanList = new ArrayList<TwitterPlayerCardBean>();
            for(TwitterPlayerCard stub : stubList) {
                beanList.add(convertTwitterPlayerCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterPlayerCard> convertTwitterPlayerCardListStubToBeanList(TwitterPlayerCardListStub listStub)
    {
        return convertTwitterPlayerCardListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterPlayerCard> convertTwitterPlayerCardListStubToBeanList(TwitterPlayerCardListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterPlayerCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterPlayerCard>();
        } else {
            beanList = new ArrayList<TwitterPlayerCard>();
            for(TwitterPlayerCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterPlayerCardToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterPlayerCardStub convertTwitterPlayerCardToStub(TwitterPlayerCard bean)
    {
        TwitterPlayerCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterPlayerCardStub();
        } else if(bean instanceof TwitterPlayerCardStub) {
            stub = (TwitterPlayerCardStub) bean;
        } else if(bean instanceof TwitterPlayerCardBean && ((TwitterPlayerCardBean) bean).isWrapper()) {
            stub = ((TwitterPlayerCardBean) bean).getStub();
        } else {
            stub = TwitterPlayerCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterProductCardBean convertTwitterProductCardToBean(TwitterProductCard stub)
    {
        TwitterProductCardBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterProductCardBean();
        } else if(stub instanceof TwitterProductCardBean) {
        	bean = (TwitterProductCardBean) stub;
        } else {
        	bean = new TwitterProductCardBean(stub);
        }
        return bean;
    }
    public static List<TwitterProductCardBean> convertTwitterProductCardListToBeanList(List<TwitterProductCard> stubList)
    {
        List<TwitterProductCardBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterProductCard>();
        } else {
            beanList = new ArrayList<TwitterProductCardBean>();
            for(TwitterProductCard stub : stubList) {
                beanList.add(convertTwitterProductCardToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterProductCard> convertTwitterProductCardListStubToBeanList(TwitterProductCardListStub listStub)
    {
        return convertTwitterProductCardListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterProductCard> convertTwitterProductCardListStubToBeanList(TwitterProductCardListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterProductCard> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterProductCard>();
        } else {
            beanList = new ArrayList<TwitterProductCard>();
            for(TwitterProductCardStub stub : listStub.getList()) {
            	beanList.add(convertTwitterProductCardToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterProductCardStub convertTwitterProductCardToStub(TwitterProductCard bean)
    {
        TwitterProductCardStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterProductCardStub();
        } else if(bean instanceof TwitterProductCardStub) {
            stub = (TwitterProductCardStub) bean;
        } else if(bean instanceof TwitterProductCardBean && ((TwitterProductCardBean) bean).isWrapper()) {
            stub = ((TwitterProductCardBean) bean).getStub();
        } else {
            stub = TwitterProductCardStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FetchRequestBean convertFetchRequestToBean(FetchRequest stub)
    {
        FetchRequestBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FetchRequestBean();
        } else if(stub instanceof FetchRequestBean) {
        	bean = (FetchRequestBean) stub;
        } else {
        	bean = new FetchRequestBean(stub);
        }
        return bean;
    }
    public static List<FetchRequestBean> convertFetchRequestListToBeanList(List<FetchRequest> stubList)
    {
        List<FetchRequestBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FetchRequest>();
        } else {
            beanList = new ArrayList<FetchRequestBean>();
            for(FetchRequest stub : stubList) {
                beanList.add(convertFetchRequestToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FetchRequest> convertFetchRequestListStubToBeanList(FetchRequestListStub listStub)
    {
        return convertFetchRequestListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FetchRequest> convertFetchRequestListStubToBeanList(FetchRequestListStub listStub, StringCursor forwardCursor)
    {
        List<FetchRequest> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FetchRequest>();
        } else {
            beanList = new ArrayList<FetchRequest>();
            for(FetchRequestStub stub : listStub.getList()) {
            	beanList.add(convertFetchRequestToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FetchRequestStub convertFetchRequestToStub(FetchRequest bean)
    {
        FetchRequestStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FetchRequestStub();
        } else if(bean instanceof FetchRequestStub) {
            stub = (FetchRequestStub) bean;
        } else if(bean instanceof FetchRequestBean && ((FetchRequestBean) bean).isWrapper()) {
            stub = ((FetchRequestBean) bean).getStub();
        } else {
            stub = FetchRequestStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PageInfoBean convertPageInfoToBean(PageInfo stub)
    {
        PageInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PageInfoBean();
        } else if(stub instanceof PageInfoBean) {
        	bean = (PageInfoBean) stub;
        } else {
        	bean = new PageInfoBean(stub);
        }
        return bean;
    }
    public static List<PageInfoBean> convertPageInfoListToBeanList(List<PageInfo> stubList)
    {
        List<PageInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PageInfo>();
        } else {
            beanList = new ArrayList<PageInfoBean>();
            for(PageInfo stub : stubList) {
                beanList.add(convertPageInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PageInfo> convertPageInfoListStubToBeanList(PageInfoListStub listStub)
    {
        return convertPageInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<PageInfo> convertPageInfoListStubToBeanList(PageInfoListStub listStub, StringCursor forwardCursor)
    {
        List<PageInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PageInfo>();
        } else {
            beanList = new ArrayList<PageInfo>();
            for(PageInfoStub stub : listStub.getList()) {
            	beanList.add(convertPageInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static PageInfoStub convertPageInfoToStub(PageInfo bean)
    {
        PageInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PageInfoStub();
        } else if(bean instanceof PageInfoStub) {
            stub = (PageInfoStub) bean;
        } else if(bean instanceof PageInfoBean && ((PageInfoBean) bean).isWrapper()) {
            stub = ((PageInfoBean) bean).getStub();
        } else {
            stub = PageInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static PageFetchBean convertPageFetchToBean(PageFetch stub)
    {
        PageFetchBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new PageFetchBean();
        } else if(stub instanceof PageFetchBean) {
        	bean = (PageFetchBean) stub;
        } else {
        	bean = new PageFetchBean(stub);
        }
        return bean;
    }
    public static List<PageFetchBean> convertPageFetchListToBeanList(List<PageFetch> stubList)
    {
        List<PageFetchBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<PageFetch>();
        } else {
            beanList = new ArrayList<PageFetchBean>();
            for(PageFetch stub : stubList) {
                beanList.add(convertPageFetchToBean(stub));
            }
        }
        return beanList;
    }
    public static List<PageFetch> convertPageFetchListStubToBeanList(PageFetchListStub listStub)
    {
        return convertPageFetchListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<PageFetch> convertPageFetchListStubToBeanList(PageFetchListStub listStub, StringCursor forwardCursor)
    {
        List<PageFetch> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<PageFetch>();
        } else {
            beanList = new ArrayList<PageFetch>();
            for(PageFetchStub stub : listStub.getList()) {
            	beanList.add(convertPageFetchToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static PageFetchStub convertPageFetchToStub(PageFetch bean)
    {
        PageFetchStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new PageFetchStub();
        } else if(bean instanceof PageFetchStub) {
            stub = (PageFetchStub) bean;
        } else if(bean instanceof PageFetchBean && ((PageFetchBean) bean).isWrapper()) {
            stub = ((PageFetchBean) bean).getStub();
        } else {
            stub = PageFetchStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static LinkListBean convertLinkListToBean(LinkList stub)
    {
        LinkListBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new LinkListBean();
        } else if(stub instanceof LinkListBean) {
        	bean = (LinkListBean) stub;
        } else {
        	bean = new LinkListBean(stub);
        }
        return bean;
    }
    public static List<LinkListBean> convertLinkListListToBeanList(List<LinkList> stubList)
    {
        List<LinkListBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<LinkList>();
        } else {
            beanList = new ArrayList<LinkListBean>();
            for(LinkList stub : stubList) {
                beanList.add(convertLinkListToBean(stub));
            }
        }
        return beanList;
    }
    public static List<LinkList> convertLinkListListStubToBeanList(LinkListListStub listStub)
    {
        return convertLinkListListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<LinkList> convertLinkListListStubToBeanList(LinkListListStub listStub, StringCursor forwardCursor)
    {
        List<LinkList> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<LinkList>();
        } else {
            beanList = new ArrayList<LinkList>();
            for(LinkListStub stub : listStub.getList()) {
            	beanList.add(convertLinkListToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static LinkListStub convertLinkListToStub(LinkList bean)
    {
        LinkListStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new LinkListStub();
        } else if(bean instanceof LinkListStub) {
            stub = (LinkListStub) bean;
        } else if(bean instanceof LinkListBean && ((LinkListBean) bean).isWrapper()) {
            stub = ((LinkListBean) bean).getStub();
        } else {
            stub = LinkListStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ImageSetBean convertImageSetToBean(ImageSet stub)
    {
        ImageSetBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ImageSetBean();
        } else if(stub instanceof ImageSetBean) {
        	bean = (ImageSetBean) stub;
        } else {
        	bean = new ImageSetBean(stub);
        }
        return bean;
    }
    public static List<ImageSetBean> convertImageSetListToBeanList(List<ImageSet> stubList)
    {
        List<ImageSetBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ImageSet>();
        } else {
            beanList = new ArrayList<ImageSetBean>();
            for(ImageSet stub : stubList) {
                beanList.add(convertImageSetToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ImageSet> convertImageSetListStubToBeanList(ImageSetListStub listStub)
    {
        return convertImageSetListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ImageSet> convertImageSetListStubToBeanList(ImageSetListStub listStub, StringCursor forwardCursor)
    {
        List<ImageSet> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ImageSet>();
        } else {
            beanList = new ArrayList<ImageSet>();
            for(ImageSetStub stub : listStub.getList()) {
            	beanList.add(convertImageSetToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ImageSetStub convertImageSetToStub(ImageSet bean)
    {
        ImageSetStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ImageSetStub();
        } else if(bean instanceof ImageSetStub) {
            stub = (ImageSetStub) bean;
        } else if(bean instanceof ImageSetBean && ((ImageSetBean) bean).isWrapper()) {
            stub = ((ImageSetBean) bean).getStub();
        } else {
            stub = ImageSetStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static AudioSetBean convertAudioSetToBean(AudioSet stub)
    {
        AudioSetBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new AudioSetBean();
        } else if(stub instanceof AudioSetBean) {
        	bean = (AudioSetBean) stub;
        } else {
        	bean = new AudioSetBean(stub);
        }
        return bean;
    }
    public static List<AudioSetBean> convertAudioSetListToBeanList(List<AudioSet> stubList)
    {
        List<AudioSetBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<AudioSet>();
        } else {
            beanList = new ArrayList<AudioSetBean>();
            for(AudioSet stub : stubList) {
                beanList.add(convertAudioSetToBean(stub));
            }
        }
        return beanList;
    }
    public static List<AudioSet> convertAudioSetListStubToBeanList(AudioSetListStub listStub)
    {
        return convertAudioSetListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<AudioSet> convertAudioSetListStubToBeanList(AudioSetListStub listStub, StringCursor forwardCursor)
    {
        List<AudioSet> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<AudioSet>();
        } else {
            beanList = new ArrayList<AudioSet>();
            for(AudioSetStub stub : listStub.getList()) {
            	beanList.add(convertAudioSetToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static AudioSetStub convertAudioSetToStub(AudioSet bean)
    {
        AudioSetStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new AudioSetStub();
        } else if(bean instanceof AudioSetStub) {
            stub = (AudioSetStub) bean;
        } else if(bean instanceof AudioSetBean && ((AudioSetBean) bean).isWrapper()) {
            stub = ((AudioSetBean) bean).getStub();
        } else {
            stub = AudioSetStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static VideoSetBean convertVideoSetToBean(VideoSet stub)
    {
        VideoSetBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new VideoSetBean();
        } else if(stub instanceof VideoSetBean) {
        	bean = (VideoSetBean) stub;
        } else {
        	bean = new VideoSetBean(stub);
        }
        return bean;
    }
    public static List<VideoSetBean> convertVideoSetListToBeanList(List<VideoSet> stubList)
    {
        List<VideoSetBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<VideoSet>();
        } else {
            beanList = new ArrayList<VideoSetBean>();
            for(VideoSet stub : stubList) {
                beanList.add(convertVideoSetToBean(stub));
            }
        }
        return beanList;
    }
    public static List<VideoSet> convertVideoSetListStubToBeanList(VideoSetListStub listStub)
    {
        return convertVideoSetListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<VideoSet> convertVideoSetListStubToBeanList(VideoSetListStub listStub, StringCursor forwardCursor)
    {
        List<VideoSet> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<VideoSet>();
        } else {
            beanList = new ArrayList<VideoSet>();
            for(VideoSetStub stub : listStub.getList()) {
            	beanList.add(convertVideoSetToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static VideoSetStub convertVideoSetToStub(VideoSet bean)
    {
        VideoSetStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new VideoSetStub();
        } else if(bean instanceof VideoSetStub) {
            stub = (VideoSetStub) bean;
        } else if(bean instanceof VideoSetBean && ((VideoSetBean) bean).isWrapper()) {
            stub = ((VideoSetBean) bean).getStub();
        } else {
            stub = VideoSetStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static OpenGraphMetaBean convertOpenGraphMetaToBean(OpenGraphMeta stub)
    {
        OpenGraphMetaBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new OpenGraphMetaBean();
        } else if(stub instanceof OpenGraphMetaBean) {
        	bean = (OpenGraphMetaBean) stub;
        } else {
        	bean = new OpenGraphMetaBean(stub);
        }
        return bean;
    }
    public static List<OpenGraphMetaBean> convertOpenGraphMetaListToBeanList(List<OpenGraphMeta> stubList)
    {
        List<OpenGraphMetaBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<OpenGraphMeta>();
        } else {
            beanList = new ArrayList<OpenGraphMetaBean>();
            for(OpenGraphMeta stub : stubList) {
                beanList.add(convertOpenGraphMetaToBean(stub));
            }
        }
        return beanList;
    }
    public static List<OpenGraphMeta> convertOpenGraphMetaListStubToBeanList(OpenGraphMetaListStub listStub)
    {
        return convertOpenGraphMetaListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<OpenGraphMeta> convertOpenGraphMetaListStubToBeanList(OpenGraphMetaListStub listStub, StringCursor forwardCursor)
    {
        List<OpenGraphMeta> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<OpenGraphMeta>();
        } else {
            beanList = new ArrayList<OpenGraphMeta>();
            for(OpenGraphMetaStub stub : listStub.getList()) {
            	beanList.add(convertOpenGraphMetaToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static OpenGraphMetaStub convertOpenGraphMetaToStub(OpenGraphMeta bean)
    {
        OpenGraphMetaStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new OpenGraphMetaStub();
        } else if(bean instanceof OpenGraphMetaStub) {
            stub = (OpenGraphMetaStub) bean;
        } else if(bean instanceof OpenGraphMetaBean && ((OpenGraphMetaBean) bean).isWrapper()) {
            stub = ((OpenGraphMetaBean) bean).getStub();
        } else {
            stub = OpenGraphMetaStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static TwitterCardMetaBean convertTwitterCardMetaToBean(TwitterCardMeta stub)
    {
        TwitterCardMetaBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new TwitterCardMetaBean();
        } else if(stub instanceof TwitterCardMetaBean) {
        	bean = (TwitterCardMetaBean) stub;
        } else {
        	bean = new TwitterCardMetaBean(stub);
        }
        return bean;
    }
    public static List<TwitterCardMetaBean> convertTwitterCardMetaListToBeanList(List<TwitterCardMeta> stubList)
    {
        List<TwitterCardMetaBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<TwitterCardMeta>();
        } else {
            beanList = new ArrayList<TwitterCardMetaBean>();
            for(TwitterCardMeta stub : stubList) {
                beanList.add(convertTwitterCardMetaToBean(stub));
            }
        }
        return beanList;
    }
    public static List<TwitterCardMeta> convertTwitterCardMetaListStubToBeanList(TwitterCardMetaListStub listStub)
    {
        return convertTwitterCardMetaListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<TwitterCardMeta> convertTwitterCardMetaListStubToBeanList(TwitterCardMetaListStub listStub, StringCursor forwardCursor)
    {
        List<TwitterCardMeta> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<TwitterCardMeta>();
        } else {
            beanList = new ArrayList<TwitterCardMeta>();
            for(TwitterCardMetaStub stub : listStub.getList()) {
            	beanList.add(convertTwitterCardMetaToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static TwitterCardMetaStub convertTwitterCardMetaToStub(TwitterCardMeta bean)
    {
        TwitterCardMetaStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new TwitterCardMetaStub();
        } else if(bean instanceof TwitterCardMetaStub) {
            stub = (TwitterCardMetaStub) bean;
        } else if(bean instanceof TwitterCardMetaBean && ((TwitterCardMetaBean) bean).isWrapper()) {
            stub = ((TwitterCardMetaBean) bean).getStub();
        } else {
            stub = TwitterCardMetaStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static DomainInfoBean convertDomainInfoToBean(DomainInfo stub)
    {
        DomainInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new DomainInfoBean();
        } else if(stub instanceof DomainInfoBean) {
        	bean = (DomainInfoBean) stub;
        } else {
        	bean = new DomainInfoBean(stub);
        }
        return bean;
    }
    public static List<DomainInfoBean> convertDomainInfoListToBeanList(List<DomainInfo> stubList)
    {
        List<DomainInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<DomainInfo>();
        } else {
            beanList = new ArrayList<DomainInfoBean>();
            for(DomainInfo stub : stubList) {
                beanList.add(convertDomainInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<DomainInfo> convertDomainInfoListStubToBeanList(DomainInfoListStub listStub)
    {
        return convertDomainInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<DomainInfo> convertDomainInfoListStubToBeanList(DomainInfoListStub listStub, StringCursor forwardCursor)
    {
        List<DomainInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<DomainInfo>();
        } else {
            beanList = new ArrayList<DomainInfo>();
            for(DomainInfoStub stub : listStub.getList()) {
            	beanList.add(convertDomainInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static DomainInfoStub convertDomainInfoToStub(DomainInfo bean)
    {
        DomainInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new DomainInfoStub();
        } else if(bean instanceof DomainInfoStub) {
            stub = (DomainInfoStub) bean;
        } else if(bean instanceof DomainInfoBean && ((DomainInfoBean) bean).isWrapper()) {
            stub = ((DomainInfoBean) bean).getStub();
        } else {
            stub = DomainInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static UrlRatingBean convertUrlRatingToBean(UrlRating stub)
    {
        UrlRatingBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new UrlRatingBean();
        } else if(stub instanceof UrlRatingBean) {
        	bean = (UrlRatingBean) stub;
        } else {
        	bean = new UrlRatingBean(stub);
        }
        return bean;
    }
    public static List<UrlRatingBean> convertUrlRatingListToBeanList(List<UrlRating> stubList)
    {
        List<UrlRatingBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<UrlRating>();
        } else {
            beanList = new ArrayList<UrlRatingBean>();
            for(UrlRating stub : stubList) {
                beanList.add(convertUrlRatingToBean(stub));
            }
        }
        return beanList;
    }
    public static List<UrlRating> convertUrlRatingListStubToBeanList(UrlRatingListStub listStub)
    {
        return convertUrlRatingListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<UrlRating> convertUrlRatingListStubToBeanList(UrlRatingListStub listStub, StringCursor forwardCursor)
    {
        List<UrlRating> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<UrlRating>();
        } else {
            beanList = new ArrayList<UrlRating>();
            for(UrlRatingStub stub : listStub.getList()) {
            	beanList.add(convertUrlRatingToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static UrlRatingStub convertUrlRatingToStub(UrlRating bean)
    {
        UrlRatingStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new UrlRatingStub();
        } else if(bean instanceof UrlRatingStub) {
            stub = (UrlRatingStub) bean;
        } else if(bean instanceof UrlRatingBean && ((UrlRatingBean) bean).isWrapper()) {
            stub = ((UrlRatingBean) bean).getStub();
        } else {
            stub = UrlRatingStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static HelpNoticeBean convertHelpNoticeToBean(HelpNotice stub)
    {
        HelpNoticeBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new HelpNoticeBean();
        } else if(stub instanceof HelpNoticeBean) {
        	bean = (HelpNoticeBean) stub;
        } else {
        	bean = new HelpNoticeBean(stub);
        }
        return bean;
    }
    public static List<HelpNoticeBean> convertHelpNoticeListToBeanList(List<HelpNotice> stubList)
    {
        List<HelpNoticeBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<HelpNotice>();
        } else {
            beanList = new ArrayList<HelpNoticeBean>();
            for(HelpNotice stub : stubList) {
                beanList.add(convertHelpNoticeToBean(stub));
            }
        }
        return beanList;
    }
    public static List<HelpNotice> convertHelpNoticeListStubToBeanList(HelpNoticeListStub listStub)
    {
        return convertHelpNoticeListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<HelpNotice> convertHelpNoticeListStubToBeanList(HelpNoticeListStub listStub, StringCursor forwardCursor)
    {
        List<HelpNotice> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<HelpNotice>();
        } else {
            beanList = new ArrayList<HelpNotice>();
            for(HelpNoticeStub stub : listStub.getList()) {
            	beanList.add(convertHelpNoticeToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static HelpNoticeStub convertHelpNoticeToStub(HelpNotice bean)
    {
        HelpNoticeStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new HelpNoticeStub();
        } else if(bean instanceof HelpNoticeStub) {
            stub = (HelpNoticeStub) bean;
        } else if(bean instanceof HelpNoticeBean && ((HelpNoticeBean) bean).isWrapper()) {
            stub = ((HelpNoticeBean) bean).getStub();
        } else {
            stub = HelpNoticeStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static ServiceInfoBean convertServiceInfoToBean(ServiceInfo stub)
    {
        ServiceInfoBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new ServiceInfoBean();
        } else if(stub instanceof ServiceInfoBean) {
        	bean = (ServiceInfoBean) stub;
        } else {
        	bean = new ServiceInfoBean(stub);
        }
        return bean;
    }
    public static List<ServiceInfoBean> convertServiceInfoListToBeanList(List<ServiceInfo> stubList)
    {
        List<ServiceInfoBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfoBean>();
            for(ServiceInfo stub : stubList) {
                beanList.add(convertServiceInfoToBean(stub));
            }
        }
        return beanList;
    }
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub)
    {
        return convertServiceInfoListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<ServiceInfo> convertServiceInfoListStubToBeanList(ServiceInfoListStub listStub, StringCursor forwardCursor)
    {
        List<ServiceInfo> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<ServiceInfo>();
        } else {
            beanList = new ArrayList<ServiceInfo>();
            for(ServiceInfoStub stub : listStub.getList()) {
            	beanList.add(convertServiceInfoToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static ServiceInfoStub convertServiceInfoToStub(ServiceInfo bean)
    {
        ServiceInfoStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new ServiceInfoStub();
        } else if(bean instanceof ServiceInfoStub) {
            stub = (ServiceInfoStub) bean;
        } else if(bean instanceof ServiceInfoBean && ((ServiceInfoBean) bean).isWrapper()) {
            stub = ((ServiceInfoBean) bean).getStub();
        } else {
            stub = ServiceInfoStub.convertBeanToStub(bean);
        }
        return stub;
    }

    // temporary
    public static FiveTenBean convertFiveTenToBean(FiveTen stub)
    {
        FiveTenBean bean = null;
        if(stub == null) {
            log.log(Level.WARNING, "Stub arg is null.");
            //bean = new FiveTenBean();
        } else if(stub instanceof FiveTenBean) {
        	bean = (FiveTenBean) stub;
        } else {
        	bean = new FiveTenBean(stub);
        }
        return bean;
    }
    public static List<FiveTenBean> convertFiveTenListToBeanList(List<FiveTen> stubList)
    {
        List<FiveTenBean> beanList = null;
        if(stubList == null) {
            log.log(Level.WARNING, "stubList is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTenBean>();
            for(FiveTen stub : stubList) {
                beanList.add(convertFiveTenToBean(stub));
            }
        }
        return beanList;
    }
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub)
    {
        return convertFiveTenListStubToBeanList(listStub, null);
    }
    // forwardCursor is an "in-out" param.
    public static List<FiveTen> convertFiveTenListStubToBeanList(FiveTenListStub listStub, StringCursor forwardCursor)
    {
        List<FiveTen> beanList = null;
        if(listStub == null) {
            log.log(Level.WARNING, "listStub is null.");
            //beanList = new ArrayList<FiveTen>();
        } else {
            beanList = new ArrayList<FiveTen>();
            for(FiveTenStub stub : listStub.getList()) {
            	beanList.add(convertFiveTenToBean(stub));
            }
            if(forwardCursor != null) {
                String webSafeString = listStub.getForwardCursor();
                forwardCursor.setWebSafeString(webSafeString);
            }
        }
        return beanList;
    }

    // temporary
    public static FiveTenStub convertFiveTenToStub(FiveTen bean)
    {
        FiveTenStub stub = null;
        if(bean == null) {
            log.log(Level.WARNING, "Bean arg is null.");
            //stub = new FiveTenStub();
        } else if(bean instanceof FiveTenStub) {
            stub = (FiveTenStub) bean;
        } else if(bean instanceof FiveTenBean && ((FiveTenBean) bean).isWrapper()) {
            stub = ((FiveTenBean) bean).getStub();
        } else {
            stub = FiveTenStub.convertBeanToStub(bean);
        }
        return stub;
    }

    
}
