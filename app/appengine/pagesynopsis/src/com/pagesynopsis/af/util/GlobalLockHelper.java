package com.pagesynopsis.af.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;

import com.pagesynopsis.ws.ApiConsumer;
import com.pagesynopsis.ws.User;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.PageInfo;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.ws.UrlRating;
import com.pagesynopsis.ws.ServiceInfo;
import com.pagesynopsis.ws.FiveTen;


// Temporary
public final class GlobalLockHelper
{
    private static final Logger log = Logger.getLogger(GlobalLockHelper.class.getName());


    // Singleton
    private GlobalLockHelper() {}

    private static final class GlobalLockHelperHolder
    {
        private static final GlobalLockHelper INSTANCE = new GlobalLockHelper();
    }
    public static GlobalLockHelper getInstance()
    {
        return GlobalLockHelperHolder.INSTANCE;
    }


    // temporary
    // Poorman's "global locking" using a distributed cache...
    private static Cache getCache()
    {
        // 30 seconds
        return CacheHelper.getFlashInstance().getCache();
    }


    private static String getApiConsumerLockKey(ApiConsumer apiConsumer)
    {
        // Note: We assume that the arg apiConsumer is not null.
        return "ApiConsumer-Processing-Lock-" + apiConsumer.getGuid();
    }
    public String lockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            // The stored value is not important.
            String val = apiConsumer.getGuid();
            getCache().put(getApiConsumerLockKey(apiConsumer), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockApiConsumer(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            String val = apiConsumer.getGuid();
            getCache().remove(getApiConsumerLockKey(apiConsumer));
            return val;
        } else {
            return null;
        }
    }
    public boolean isApiConsumerLocked(ApiConsumer apiConsumer)
    {
        if(getCache() != null && apiConsumer != null) {
            return getCache().containsKey(getApiConsumerLockKey(apiConsumer)); 
        } else {
            return false;
        }
    }

    private static String getUserLockKey(User user)
    {
        // Note: We assume that the arg user is not null.
        return "User-Processing-Lock-" + user.getGuid();
    }
    public String lockUser(User user)
    {
        if(getCache() != null && user != null) {
            // The stored value is not important.
            String val = user.getGuid();
            getCache().put(getUserLockKey(user), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUser(User user)
    {
        if(getCache() != null && user != null) {
            String val = user.getGuid();
            getCache().remove(getUserLockKey(user));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUserLocked(User user)
    {
        if(getCache() != null && user != null) {
            return getCache().containsKey(getUserLockKey(user)); 
        } else {
            return false;
        }
    }

    private static String getRobotsTextFileLockKey(RobotsTextFile robotsTextFile)
    {
        // Note: We assume that the arg robotsTextFile is not null.
        return "RobotsTextFile-Processing-Lock-" + robotsTextFile.getGuid();
    }
    public String lockRobotsTextFile(RobotsTextFile robotsTextFile)
    {
        if(getCache() != null && robotsTextFile != null) {
            // The stored value is not important.
            String val = robotsTextFile.getGuid();
            getCache().put(getRobotsTextFileLockKey(robotsTextFile), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockRobotsTextFile(RobotsTextFile robotsTextFile)
    {
        if(getCache() != null && robotsTextFile != null) {
            String val = robotsTextFile.getGuid();
            getCache().remove(getRobotsTextFileLockKey(robotsTextFile));
            return val;
        } else {
            return null;
        }
    }
    public boolean isRobotsTextFileLocked(RobotsTextFile robotsTextFile)
    {
        if(getCache() != null && robotsTextFile != null) {
            return getCache().containsKey(getRobotsTextFileLockKey(robotsTextFile)); 
        } else {
            return false;
        }
    }

    private static String getRobotsTextRefreshLockKey(RobotsTextRefresh robotsTextRefresh)
    {
        // Note: We assume that the arg robotsTextRefresh is not null.
        return "RobotsTextRefresh-Processing-Lock-" + robotsTextRefresh.getGuid();
    }
    public String lockRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh)
    {
        if(getCache() != null && robotsTextRefresh != null) {
            // The stored value is not important.
            String val = robotsTextRefresh.getGuid();
            getCache().put(getRobotsTextRefreshLockKey(robotsTextRefresh), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh)
    {
        if(getCache() != null && robotsTextRefresh != null) {
            String val = robotsTextRefresh.getGuid();
            getCache().remove(getRobotsTextRefreshLockKey(robotsTextRefresh));
            return val;
        } else {
            return null;
        }
    }
    public boolean isRobotsTextRefreshLocked(RobotsTextRefresh robotsTextRefresh)
    {
        if(getCache() != null && robotsTextRefresh != null) {
            return getCache().containsKey(getRobotsTextRefreshLockKey(robotsTextRefresh)); 
        } else {
            return false;
        }
    }

    private static String getOgProfileLockKey(OgProfile ogProfile)
    {
        // Note: We assume that the arg ogProfile is not null.
        return "OgProfile-Processing-Lock-" + ogProfile.getGuid();
    }
    public String lockOgProfile(OgProfile ogProfile)
    {
        if(getCache() != null && ogProfile != null) {
            // The stored value is not important.
            String val = ogProfile.getGuid();
            getCache().put(getOgProfileLockKey(ogProfile), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgProfile(OgProfile ogProfile)
    {
        if(getCache() != null && ogProfile != null) {
            String val = ogProfile.getGuid();
            getCache().remove(getOgProfileLockKey(ogProfile));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgProfileLocked(OgProfile ogProfile)
    {
        if(getCache() != null && ogProfile != null) {
            return getCache().containsKey(getOgProfileLockKey(ogProfile)); 
        } else {
            return false;
        }
    }

    private static String getOgWebsiteLockKey(OgWebsite ogWebsite)
    {
        // Note: We assume that the arg ogWebsite is not null.
        return "OgWebsite-Processing-Lock-" + ogWebsite.getGuid();
    }
    public String lockOgWebsite(OgWebsite ogWebsite)
    {
        if(getCache() != null && ogWebsite != null) {
            // The stored value is not important.
            String val = ogWebsite.getGuid();
            getCache().put(getOgWebsiteLockKey(ogWebsite), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgWebsite(OgWebsite ogWebsite)
    {
        if(getCache() != null && ogWebsite != null) {
            String val = ogWebsite.getGuid();
            getCache().remove(getOgWebsiteLockKey(ogWebsite));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgWebsiteLocked(OgWebsite ogWebsite)
    {
        if(getCache() != null && ogWebsite != null) {
            return getCache().containsKey(getOgWebsiteLockKey(ogWebsite)); 
        } else {
            return false;
        }
    }

    private static String getOgBlogLockKey(OgBlog ogBlog)
    {
        // Note: We assume that the arg ogBlog is not null.
        return "OgBlog-Processing-Lock-" + ogBlog.getGuid();
    }
    public String lockOgBlog(OgBlog ogBlog)
    {
        if(getCache() != null && ogBlog != null) {
            // The stored value is not important.
            String val = ogBlog.getGuid();
            getCache().put(getOgBlogLockKey(ogBlog), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgBlog(OgBlog ogBlog)
    {
        if(getCache() != null && ogBlog != null) {
            String val = ogBlog.getGuid();
            getCache().remove(getOgBlogLockKey(ogBlog));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgBlogLocked(OgBlog ogBlog)
    {
        if(getCache() != null && ogBlog != null) {
            return getCache().containsKey(getOgBlogLockKey(ogBlog)); 
        } else {
            return false;
        }
    }

    private static String getOgArticleLockKey(OgArticle ogArticle)
    {
        // Note: We assume that the arg ogArticle is not null.
        return "OgArticle-Processing-Lock-" + ogArticle.getGuid();
    }
    public String lockOgArticle(OgArticle ogArticle)
    {
        if(getCache() != null && ogArticle != null) {
            // The stored value is not important.
            String val = ogArticle.getGuid();
            getCache().put(getOgArticleLockKey(ogArticle), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgArticle(OgArticle ogArticle)
    {
        if(getCache() != null && ogArticle != null) {
            String val = ogArticle.getGuid();
            getCache().remove(getOgArticleLockKey(ogArticle));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgArticleLocked(OgArticle ogArticle)
    {
        if(getCache() != null && ogArticle != null) {
            return getCache().containsKey(getOgArticleLockKey(ogArticle)); 
        } else {
            return false;
        }
    }

    private static String getOgBookLockKey(OgBook ogBook)
    {
        // Note: We assume that the arg ogBook is not null.
        return "OgBook-Processing-Lock-" + ogBook.getGuid();
    }
    public String lockOgBook(OgBook ogBook)
    {
        if(getCache() != null && ogBook != null) {
            // The stored value is not important.
            String val = ogBook.getGuid();
            getCache().put(getOgBookLockKey(ogBook), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgBook(OgBook ogBook)
    {
        if(getCache() != null && ogBook != null) {
            String val = ogBook.getGuid();
            getCache().remove(getOgBookLockKey(ogBook));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgBookLocked(OgBook ogBook)
    {
        if(getCache() != null && ogBook != null) {
            return getCache().containsKey(getOgBookLockKey(ogBook)); 
        } else {
            return false;
        }
    }

    private static String getOgVideoLockKey(OgVideo ogVideo)
    {
        // Note: We assume that the arg ogVideo is not null.
        return "OgVideo-Processing-Lock-" + ogVideo.getGuid();
    }
    public String lockOgVideo(OgVideo ogVideo)
    {
        if(getCache() != null && ogVideo != null) {
            // The stored value is not important.
            String val = ogVideo.getGuid();
            getCache().put(getOgVideoLockKey(ogVideo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgVideo(OgVideo ogVideo)
    {
        if(getCache() != null && ogVideo != null) {
            String val = ogVideo.getGuid();
            getCache().remove(getOgVideoLockKey(ogVideo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgVideoLocked(OgVideo ogVideo)
    {
        if(getCache() != null && ogVideo != null) {
            return getCache().containsKey(getOgVideoLockKey(ogVideo)); 
        } else {
            return false;
        }
    }

    private static String getOgMovieLockKey(OgMovie ogMovie)
    {
        // Note: We assume that the arg ogMovie is not null.
        return "OgMovie-Processing-Lock-" + ogMovie.getGuid();
    }
    public String lockOgMovie(OgMovie ogMovie)
    {
        if(getCache() != null && ogMovie != null) {
            // The stored value is not important.
            String val = ogMovie.getGuid();
            getCache().put(getOgMovieLockKey(ogMovie), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgMovie(OgMovie ogMovie)
    {
        if(getCache() != null && ogMovie != null) {
            String val = ogMovie.getGuid();
            getCache().remove(getOgMovieLockKey(ogMovie));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgMovieLocked(OgMovie ogMovie)
    {
        if(getCache() != null && ogMovie != null) {
            return getCache().containsKey(getOgMovieLockKey(ogMovie)); 
        } else {
            return false;
        }
    }

    private static String getOgTvShowLockKey(OgTvShow ogTvShow)
    {
        // Note: We assume that the arg ogTvShow is not null.
        return "OgTvShow-Processing-Lock-" + ogTvShow.getGuid();
    }
    public String lockOgTvShow(OgTvShow ogTvShow)
    {
        if(getCache() != null && ogTvShow != null) {
            // The stored value is not important.
            String val = ogTvShow.getGuid();
            getCache().put(getOgTvShowLockKey(ogTvShow), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgTvShow(OgTvShow ogTvShow)
    {
        if(getCache() != null && ogTvShow != null) {
            String val = ogTvShow.getGuid();
            getCache().remove(getOgTvShowLockKey(ogTvShow));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgTvShowLocked(OgTvShow ogTvShow)
    {
        if(getCache() != null && ogTvShow != null) {
            return getCache().containsKey(getOgTvShowLockKey(ogTvShow)); 
        } else {
            return false;
        }
    }

    private static String getOgTvEpisodeLockKey(OgTvEpisode ogTvEpisode)
    {
        // Note: We assume that the arg ogTvEpisode is not null.
        return "OgTvEpisode-Processing-Lock-" + ogTvEpisode.getGuid();
    }
    public String lockOgTvEpisode(OgTvEpisode ogTvEpisode)
    {
        if(getCache() != null && ogTvEpisode != null) {
            // The stored value is not important.
            String val = ogTvEpisode.getGuid();
            getCache().put(getOgTvEpisodeLockKey(ogTvEpisode), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOgTvEpisode(OgTvEpisode ogTvEpisode)
    {
        if(getCache() != null && ogTvEpisode != null) {
            String val = ogTvEpisode.getGuid();
            getCache().remove(getOgTvEpisodeLockKey(ogTvEpisode));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOgTvEpisodeLocked(OgTvEpisode ogTvEpisode)
    {
        if(getCache() != null && ogTvEpisode != null) {
            return getCache().containsKey(getOgTvEpisodeLockKey(ogTvEpisode)); 
        } else {
            return false;
        }
    }

    private static String getTwitterSummaryCardLockKey(TwitterSummaryCard twitterSummaryCard)
    {
        // Note: We assume that the arg twitterSummaryCard is not null.
        return "TwitterSummaryCard-Processing-Lock-" + twitterSummaryCard.getGuid();
    }
    public String lockTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard)
    {
        if(getCache() != null && twitterSummaryCard != null) {
            // The stored value is not important.
            String val = twitterSummaryCard.getGuid();
            getCache().put(getTwitterSummaryCardLockKey(twitterSummaryCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterSummaryCard(TwitterSummaryCard twitterSummaryCard)
    {
        if(getCache() != null && twitterSummaryCard != null) {
            String val = twitterSummaryCard.getGuid();
            getCache().remove(getTwitterSummaryCardLockKey(twitterSummaryCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterSummaryCardLocked(TwitterSummaryCard twitterSummaryCard)
    {
        if(getCache() != null && twitterSummaryCard != null) {
            return getCache().containsKey(getTwitterSummaryCardLockKey(twitterSummaryCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterPhotoCardLockKey(TwitterPhotoCard twitterPhotoCard)
    {
        // Note: We assume that the arg twitterPhotoCard is not null.
        return "TwitterPhotoCard-Processing-Lock-" + twitterPhotoCard.getGuid();
    }
    public String lockTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard)
    {
        if(getCache() != null && twitterPhotoCard != null) {
            // The stored value is not important.
            String val = twitterPhotoCard.getGuid();
            getCache().put(getTwitterPhotoCardLockKey(twitterPhotoCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterPhotoCard(TwitterPhotoCard twitterPhotoCard)
    {
        if(getCache() != null && twitterPhotoCard != null) {
            String val = twitterPhotoCard.getGuid();
            getCache().remove(getTwitterPhotoCardLockKey(twitterPhotoCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterPhotoCardLocked(TwitterPhotoCard twitterPhotoCard)
    {
        if(getCache() != null && twitterPhotoCard != null) {
            return getCache().containsKey(getTwitterPhotoCardLockKey(twitterPhotoCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterGalleryCardLockKey(TwitterGalleryCard twitterGalleryCard)
    {
        // Note: We assume that the arg twitterGalleryCard is not null.
        return "TwitterGalleryCard-Processing-Lock-" + twitterGalleryCard.getGuid();
    }
    public String lockTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard)
    {
        if(getCache() != null && twitterGalleryCard != null) {
            // The stored value is not important.
            String val = twitterGalleryCard.getGuid();
            getCache().put(getTwitterGalleryCardLockKey(twitterGalleryCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterGalleryCard(TwitterGalleryCard twitterGalleryCard)
    {
        if(getCache() != null && twitterGalleryCard != null) {
            String val = twitterGalleryCard.getGuid();
            getCache().remove(getTwitterGalleryCardLockKey(twitterGalleryCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterGalleryCardLocked(TwitterGalleryCard twitterGalleryCard)
    {
        if(getCache() != null && twitterGalleryCard != null) {
            return getCache().containsKey(getTwitterGalleryCardLockKey(twitterGalleryCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterAppCardLockKey(TwitterAppCard twitterAppCard)
    {
        // Note: We assume that the arg twitterAppCard is not null.
        return "TwitterAppCard-Processing-Lock-" + twitterAppCard.getGuid();
    }
    public String lockTwitterAppCard(TwitterAppCard twitterAppCard)
    {
        if(getCache() != null && twitterAppCard != null) {
            // The stored value is not important.
            String val = twitterAppCard.getGuid();
            getCache().put(getTwitterAppCardLockKey(twitterAppCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterAppCard(TwitterAppCard twitterAppCard)
    {
        if(getCache() != null && twitterAppCard != null) {
            String val = twitterAppCard.getGuid();
            getCache().remove(getTwitterAppCardLockKey(twitterAppCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterAppCardLocked(TwitterAppCard twitterAppCard)
    {
        if(getCache() != null && twitterAppCard != null) {
            return getCache().containsKey(getTwitterAppCardLockKey(twitterAppCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterPlayerCardLockKey(TwitterPlayerCard twitterPlayerCard)
    {
        // Note: We assume that the arg twitterPlayerCard is not null.
        return "TwitterPlayerCard-Processing-Lock-" + twitterPlayerCard.getGuid();
    }
    public String lockTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard)
    {
        if(getCache() != null && twitterPlayerCard != null) {
            // The stored value is not important.
            String val = twitterPlayerCard.getGuid();
            getCache().put(getTwitterPlayerCardLockKey(twitterPlayerCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard)
    {
        if(getCache() != null && twitterPlayerCard != null) {
            String val = twitterPlayerCard.getGuid();
            getCache().remove(getTwitterPlayerCardLockKey(twitterPlayerCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterPlayerCardLocked(TwitterPlayerCard twitterPlayerCard)
    {
        if(getCache() != null && twitterPlayerCard != null) {
            return getCache().containsKey(getTwitterPlayerCardLockKey(twitterPlayerCard)); 
        } else {
            return false;
        }
    }

    private static String getTwitterProductCardLockKey(TwitterProductCard twitterProductCard)
    {
        // Note: We assume that the arg twitterProductCard is not null.
        return "TwitterProductCard-Processing-Lock-" + twitterProductCard.getGuid();
    }
    public String lockTwitterProductCard(TwitterProductCard twitterProductCard)
    {
        if(getCache() != null && twitterProductCard != null) {
            // The stored value is not important.
            String val = twitterProductCard.getGuid();
            getCache().put(getTwitterProductCardLockKey(twitterProductCard), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterProductCard(TwitterProductCard twitterProductCard)
    {
        if(getCache() != null && twitterProductCard != null) {
            String val = twitterProductCard.getGuid();
            getCache().remove(getTwitterProductCardLockKey(twitterProductCard));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterProductCardLocked(TwitterProductCard twitterProductCard)
    {
        if(getCache() != null && twitterProductCard != null) {
            return getCache().containsKey(getTwitterProductCardLockKey(twitterProductCard)); 
        } else {
            return false;
        }
    }

    private static String getFetchRequestLockKey(FetchRequest fetchRequest)
    {
        // Note: We assume that the arg fetchRequest is not null.
        return "FetchRequest-Processing-Lock-" + fetchRequest.getGuid();
    }
    public String lockFetchRequest(FetchRequest fetchRequest)
    {
        if(getCache() != null && fetchRequest != null) {
            // The stored value is not important.
            String val = fetchRequest.getGuid();
            getCache().put(getFetchRequestLockKey(fetchRequest), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFetchRequest(FetchRequest fetchRequest)
    {
        if(getCache() != null && fetchRequest != null) {
            String val = fetchRequest.getGuid();
            getCache().remove(getFetchRequestLockKey(fetchRequest));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFetchRequestLocked(FetchRequest fetchRequest)
    {
        if(getCache() != null && fetchRequest != null) {
            return getCache().containsKey(getFetchRequestLockKey(fetchRequest)); 
        } else {
            return false;
        }
    }

    private static String getPageInfoLockKey(PageInfo pageInfo)
    {
        // Note: We assume that the arg pageInfo is not null.
        return "PageInfo-Processing-Lock-" + pageInfo.getGuid();
    }
    public String lockPageInfo(PageInfo pageInfo)
    {
        if(getCache() != null && pageInfo != null) {
            // The stored value is not important.
            String val = pageInfo.getGuid();
            getCache().put(getPageInfoLockKey(pageInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockPageInfo(PageInfo pageInfo)
    {
        if(getCache() != null && pageInfo != null) {
            String val = pageInfo.getGuid();
            getCache().remove(getPageInfoLockKey(pageInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isPageInfoLocked(PageInfo pageInfo)
    {
        if(getCache() != null && pageInfo != null) {
            return getCache().containsKey(getPageInfoLockKey(pageInfo)); 
        } else {
            return false;
        }
    }

    private static String getPageFetchLockKey(PageFetch pageFetch)
    {
        // Note: We assume that the arg pageFetch is not null.
        return "PageFetch-Processing-Lock-" + pageFetch.getGuid();
    }
    public String lockPageFetch(PageFetch pageFetch)
    {
        if(getCache() != null && pageFetch != null) {
            // The stored value is not important.
            String val = pageFetch.getGuid();
            getCache().put(getPageFetchLockKey(pageFetch), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockPageFetch(PageFetch pageFetch)
    {
        if(getCache() != null && pageFetch != null) {
            String val = pageFetch.getGuid();
            getCache().remove(getPageFetchLockKey(pageFetch));
            return val;
        } else {
            return null;
        }
    }
    public boolean isPageFetchLocked(PageFetch pageFetch)
    {
        if(getCache() != null && pageFetch != null) {
            return getCache().containsKey(getPageFetchLockKey(pageFetch)); 
        } else {
            return false;
        }
    }

    private static String getLinkListLockKey(LinkList linkList)
    {
        // Note: We assume that the arg linkList is not null.
        return "LinkList-Processing-Lock-" + linkList.getGuid();
    }
    public String lockLinkList(LinkList linkList)
    {
        if(getCache() != null && linkList != null) {
            // The stored value is not important.
            String val = linkList.getGuid();
            getCache().put(getLinkListLockKey(linkList), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockLinkList(LinkList linkList)
    {
        if(getCache() != null && linkList != null) {
            String val = linkList.getGuid();
            getCache().remove(getLinkListLockKey(linkList));
            return val;
        } else {
            return null;
        }
    }
    public boolean isLinkListLocked(LinkList linkList)
    {
        if(getCache() != null && linkList != null) {
            return getCache().containsKey(getLinkListLockKey(linkList)); 
        } else {
            return false;
        }
    }

    private static String getImageSetLockKey(ImageSet imageSet)
    {
        // Note: We assume that the arg imageSet is not null.
        return "ImageSet-Processing-Lock-" + imageSet.getGuid();
    }
    public String lockImageSet(ImageSet imageSet)
    {
        if(getCache() != null && imageSet != null) {
            // The stored value is not important.
            String val = imageSet.getGuid();
            getCache().put(getImageSetLockKey(imageSet), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockImageSet(ImageSet imageSet)
    {
        if(getCache() != null && imageSet != null) {
            String val = imageSet.getGuid();
            getCache().remove(getImageSetLockKey(imageSet));
            return val;
        } else {
            return null;
        }
    }
    public boolean isImageSetLocked(ImageSet imageSet)
    {
        if(getCache() != null && imageSet != null) {
            return getCache().containsKey(getImageSetLockKey(imageSet)); 
        } else {
            return false;
        }
    }

    private static String getAudioSetLockKey(AudioSet audioSet)
    {
        // Note: We assume that the arg audioSet is not null.
        return "AudioSet-Processing-Lock-" + audioSet.getGuid();
    }
    public String lockAudioSet(AudioSet audioSet)
    {
        if(getCache() != null && audioSet != null) {
            // The stored value is not important.
            String val = audioSet.getGuid();
            getCache().put(getAudioSetLockKey(audioSet), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockAudioSet(AudioSet audioSet)
    {
        if(getCache() != null && audioSet != null) {
            String val = audioSet.getGuid();
            getCache().remove(getAudioSetLockKey(audioSet));
            return val;
        } else {
            return null;
        }
    }
    public boolean isAudioSetLocked(AudioSet audioSet)
    {
        if(getCache() != null && audioSet != null) {
            return getCache().containsKey(getAudioSetLockKey(audioSet)); 
        } else {
            return false;
        }
    }

    private static String getVideoSetLockKey(VideoSet videoSet)
    {
        // Note: We assume that the arg videoSet is not null.
        return "VideoSet-Processing-Lock-" + videoSet.getGuid();
    }
    public String lockVideoSet(VideoSet videoSet)
    {
        if(getCache() != null && videoSet != null) {
            // The stored value is not important.
            String val = videoSet.getGuid();
            getCache().put(getVideoSetLockKey(videoSet), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockVideoSet(VideoSet videoSet)
    {
        if(getCache() != null && videoSet != null) {
            String val = videoSet.getGuid();
            getCache().remove(getVideoSetLockKey(videoSet));
            return val;
        } else {
            return null;
        }
    }
    public boolean isVideoSetLocked(VideoSet videoSet)
    {
        if(getCache() != null && videoSet != null) {
            return getCache().containsKey(getVideoSetLockKey(videoSet)); 
        } else {
            return false;
        }
    }

    private static String getOpenGraphMetaLockKey(OpenGraphMeta openGraphMeta)
    {
        // Note: We assume that the arg openGraphMeta is not null.
        return "OpenGraphMeta-Processing-Lock-" + openGraphMeta.getGuid();
    }
    public String lockOpenGraphMeta(OpenGraphMeta openGraphMeta)
    {
        if(getCache() != null && openGraphMeta != null) {
            // The stored value is not important.
            String val = openGraphMeta.getGuid();
            getCache().put(getOpenGraphMetaLockKey(openGraphMeta), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockOpenGraphMeta(OpenGraphMeta openGraphMeta)
    {
        if(getCache() != null && openGraphMeta != null) {
            String val = openGraphMeta.getGuid();
            getCache().remove(getOpenGraphMetaLockKey(openGraphMeta));
            return val;
        } else {
            return null;
        }
    }
    public boolean isOpenGraphMetaLocked(OpenGraphMeta openGraphMeta)
    {
        if(getCache() != null && openGraphMeta != null) {
            return getCache().containsKey(getOpenGraphMetaLockKey(openGraphMeta)); 
        } else {
            return false;
        }
    }

    private static String getTwitterCardMetaLockKey(TwitterCardMeta twitterCardMeta)
    {
        // Note: We assume that the arg twitterCardMeta is not null.
        return "TwitterCardMeta-Processing-Lock-" + twitterCardMeta.getGuid();
    }
    public String lockTwitterCardMeta(TwitterCardMeta twitterCardMeta)
    {
        if(getCache() != null && twitterCardMeta != null) {
            // The stored value is not important.
            String val = twitterCardMeta.getGuid();
            getCache().put(getTwitterCardMetaLockKey(twitterCardMeta), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockTwitterCardMeta(TwitterCardMeta twitterCardMeta)
    {
        if(getCache() != null && twitterCardMeta != null) {
            String val = twitterCardMeta.getGuid();
            getCache().remove(getTwitterCardMetaLockKey(twitterCardMeta));
            return val;
        } else {
            return null;
        }
    }
    public boolean isTwitterCardMetaLocked(TwitterCardMeta twitterCardMeta)
    {
        if(getCache() != null && twitterCardMeta != null) {
            return getCache().containsKey(getTwitterCardMetaLockKey(twitterCardMeta)); 
        } else {
            return false;
        }
    }

    private static String getDomainInfoLockKey(DomainInfo domainInfo)
    {
        // Note: We assume that the arg domainInfo is not null.
        return "DomainInfo-Processing-Lock-" + domainInfo.getGuid();
    }
    public String lockDomainInfo(DomainInfo domainInfo)
    {
        if(getCache() != null && domainInfo != null) {
            // The stored value is not important.
            String val = domainInfo.getGuid();
            getCache().put(getDomainInfoLockKey(domainInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockDomainInfo(DomainInfo domainInfo)
    {
        if(getCache() != null && domainInfo != null) {
            String val = domainInfo.getGuid();
            getCache().remove(getDomainInfoLockKey(domainInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isDomainInfoLocked(DomainInfo domainInfo)
    {
        if(getCache() != null && domainInfo != null) {
            return getCache().containsKey(getDomainInfoLockKey(domainInfo)); 
        } else {
            return false;
        }
    }

    private static String getUrlRatingLockKey(UrlRating urlRating)
    {
        // Note: We assume that the arg urlRating is not null.
        return "UrlRating-Processing-Lock-" + urlRating.getGuid();
    }
    public String lockUrlRating(UrlRating urlRating)
    {
        if(getCache() != null && urlRating != null) {
            // The stored value is not important.
            String val = urlRating.getGuid();
            getCache().put(getUrlRatingLockKey(urlRating), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockUrlRating(UrlRating urlRating)
    {
        if(getCache() != null && urlRating != null) {
            String val = urlRating.getGuid();
            getCache().remove(getUrlRatingLockKey(urlRating));
            return val;
        } else {
            return null;
        }
    }
    public boolean isUrlRatingLocked(UrlRating urlRating)
    {
        if(getCache() != null && urlRating != null) {
            return getCache().containsKey(getUrlRatingLockKey(urlRating)); 
        } else {
            return false;
        }
    }

    private static String getServiceInfoLockKey(ServiceInfo serviceInfo)
    {
        // Note: We assume that the arg serviceInfo is not null.
        return "ServiceInfo-Processing-Lock-" + serviceInfo.getGuid();
    }
    public String lockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            // The stored value is not important.
            String val = serviceInfo.getGuid();
            getCache().put(getServiceInfoLockKey(serviceInfo), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockServiceInfo(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            String val = serviceInfo.getGuid();
            getCache().remove(getServiceInfoLockKey(serviceInfo));
            return val;
        } else {
            return null;
        }
    }
    public boolean isServiceInfoLocked(ServiceInfo serviceInfo)
    {
        if(getCache() != null && serviceInfo != null) {
            return getCache().containsKey(getServiceInfoLockKey(serviceInfo)); 
        } else {
            return false;
        }
    }

    private static String getFiveTenLockKey(FiveTen fiveTen)
    {
        // Note: We assume that the arg fiveTen is not null.
        return "FiveTen-Processing-Lock-" + fiveTen.getGuid();
    }
    public String lockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            // The stored value is not important.
            String val = fiveTen.getGuid();
            getCache().put(getFiveTenLockKey(fiveTen), val);
            return val;
        } else {
            return null;
        }
    }
    public String unlockFiveTen(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            String val = fiveTen.getGuid();
            getCache().remove(getFiveTenLockKey(fiveTen));
            return val;
        } else {
            return null;
        }
    }
    public boolean isFiveTenLocked(FiveTen fiveTen)
    {
        if(getCache() != null && fiveTen != null) {
            return getCache().containsKey(getFiveTenLockKey(fiveTen)); 
        } else {
            return false;
        }
    }

 
}
