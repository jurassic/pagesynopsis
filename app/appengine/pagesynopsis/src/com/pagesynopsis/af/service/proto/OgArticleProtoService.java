package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.impl.OgArticleServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgArticleProtoService extends OgArticleServiceImpl implements OgArticleService
{
    private static final Logger log = Logger.getLogger(OgArticleProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgArticleProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgArticle related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        return super.getOgArticle(guid);
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        return super.getOgArticle(guid, field);
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        return super.getOgArticles(guids);
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return super.getAllOgArticles();
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgArticles(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgArticleKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        return super.createOgArticle(ogArticle);
    }

    @Override
    public OgArticle constructOgArticle(OgArticle ogArticle) throws BaseException
    {
        return super.constructOgArticle(ogArticle);
    }


    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        return super.updateOgArticle(ogArticle);
    }
        
    @Override
    public OgArticle refreshOgArticle(OgArticle ogArticle) throws BaseException
    {
        return super.refreshOgArticle(ogArticle);
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        return super.deleteOgArticle(guid);
    }

    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        return super.deleteOgArticle(ogArticle);
    }

    @Override
    public Integer createOgArticles(List<OgArticle> ogArticles) throws BaseException
    {
        return super.createOgArticles(ogArticles);
    }

    // TBD
    //@Override
    //public Boolean updateOgArticles(List<OgArticle> ogArticles) throws BaseException
    //{
    //}

}
