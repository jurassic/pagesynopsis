package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.DomainInfo;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.DomainInfoBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.DomainInfoService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class DomainInfoServiceImpl implements DomainInfoService
{
    private static final Logger log = Logger.getLogger(DomainInfoServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "DomainInfo-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("DomainInfo:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public DomainInfoServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // DomainInfo related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public DomainInfo getDomainInfo(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getDomainInfo(): guid = " + guid);

        DomainInfoBean bean = null;
        if(getCache() != null) {
            bean = (DomainInfoBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DomainInfoBean) getProxyFactory().getDomainInfoServiceProxy().getDomainInfo(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DomainInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DomainInfoBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getDomainInfo(String guid, String field) throws BaseException
    {
        DomainInfoBean bean = null;
        if(getCache() != null) {
            bean = (DomainInfoBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (DomainInfoBean) getProxyFactory().getDomainInfoServiceProxy().getDomainInfo(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "DomainInfoBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve DomainInfoBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("excluded")) {
            return bean.isExcluded();
        } else if(field.equals("category")) {
            return bean.getCategory();
        } else if(field.equals("reputation")) {
            return bean.getReputation();
        } else if(field.equals("authority")) {
            return bean.getAuthority();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<DomainInfo> getDomainInfos(List<String> guids) throws BaseException
    {
        log.fine("getDomainInfos()");

        // TBD: Is there a better way????
        List<DomainInfo> domainInfos = getProxyFactory().getDomainInfoServiceProxy().getDomainInfos(guids);
        if(domainInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoBean list.");
        }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<DomainInfo> getAllDomainInfos() throws BaseException
    {
        return getAllDomainInfos(null, null, null);
    }


    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDomainInfos(ordering, offset, count, null);
    }

    @Override
    public List<DomainInfo> getAllDomainInfos(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDomainInfos(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DomainInfo> domainInfos = getProxyFactory().getDomainInfoServiceProxy().getAllDomainInfos(ordering, offset, count, forwardCursor);
        if(domainInfos == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoBean list.");
        }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllDomainInfoKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllDomainInfoKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllDomainInfoKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDomainInfoServiceProxy().getAllDomainInfoKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve DomainInfoBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DomainInfoBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<DomainInfo> findDomainInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoServiceImpl.findDomainInfos(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<DomainInfo> domainInfos = getProxyFactory().getDomainInfoServiceProxy().findDomainInfos(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(domainInfos == null) {
            log.log(Level.WARNING, "Failed to find domainInfos for the given criterion.");
        }

        log.finer("END");
        return domainInfos;
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findDomainInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoServiceImpl.findDomainInfoKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getDomainInfoServiceProxy().findDomainInfoKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find DomainInfo keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty DomainInfo key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("DomainInfoServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getDomainInfoServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createDomainInfo(String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        DomainInfoBean bean = new DomainInfoBean(null, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
        return createDomainInfo(bean);
    }

    @Override
    public String createDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DomainInfo bean = constructDomainInfo(domainInfo);
        //return bean.getGuid();

        // Param domainInfo cannot be null.....
        if(domainInfo == null) {
            log.log(Level.INFO, "Param domainInfo is null!");
            throw new BadRequestException("Param domainInfo object is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else if(domainInfo instanceof DomainInfo) {
            // bean = new DomainInfoBean(null, domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createDomainInfo(): Arg domainInfo is of an unknown type.");
            //bean = new DomainInfoBean();
            bean = new DomainInfoBean(domainInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDomainInfoServiceProxy().createDomainInfo(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public DomainInfo constructDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null) {
            log.log(Level.INFO, "Param domainInfo is null!");
            throw new BadRequestException("Param domainInfo object is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else if(domainInfo instanceof DomainInfo) {
            // bean = new DomainInfoBean(null, domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
        } else {
            log.log(Level.WARNING, "createDomainInfo(): Arg domainInfo is of an unknown type.");
            //bean = new DomainInfoBean();
            bean = new DomainInfoBean(domainInfo.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getDomainInfoServiceProxy().createDomainInfo(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateDomainInfo(String guid, String domain, Boolean excluded, String category, String reputation, String authority, String note, String status, Long lastCheckedTime, Long lastUpdatedTime) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        DomainInfoBean bean = new DomainInfoBean(guid, domain, excluded, category, reputation, authority, note, status, lastCheckedTime, lastUpdatedTime);
        return updateDomainInfo(bean);
    }
        
    @Override
    public Boolean updateDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //DomainInfo bean = refreshDomainInfo(domainInfo);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else {  // if(domainInfo instanceof DomainInfo)
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getDomainInfoServiceProxy().updateDomainInfo(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public DomainInfo refreshDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else {  // if(domainInfo instanceof DomainInfo)
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getDomainInfoServiceProxy().updateDomainInfo(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteDomainInfo(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfo(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            DomainInfo domainInfo = null;
            try {
                domainInfo = getProxyFactory().getDomainInfoServiceProxy().getDomainInfo(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch domainInfo with a key, " + guid);
                return false;
            }
            if(domainInfo != null) {
                String beanGuid = domainInfo.getGuid();
                Boolean suc1 = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfo(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("domainInfo with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteDomainInfo(DomainInfo domainInfo) throws BaseException
    {
        log.finer("BEGIN");

        // Param domainInfo cannot be null.....
        if(domainInfo == null || domainInfo.getGuid() == null) {
            log.log(Level.INFO, "Param domainInfo or its guid is null!");
            throw new BadRequestException("Param domainInfo object or its guid is null!");
        }
        DomainInfoBean bean = null;
        if(domainInfo instanceof DomainInfoBean) {
            bean = (DomainInfoBean) domainInfo;
        } else {  // if(domainInfo instanceof DomainInfo)
            // ????
            log.warning("domainInfo is not an instance of DomainInfoBean.");
            bean = new DomainInfoBean(domainInfo.getGuid(), domainInfo.getDomain(), domainInfo.isExcluded(), domainInfo.getCategory(), domainInfo.getReputation(), domainInfo.getAuthority(), domainInfo.getNote(), domainInfo.getStatus(), domainInfo.getLastCheckedTime(), domainInfo.getLastUpdatedTime());
        }
        Boolean suc = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfo(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteDomainInfos(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getDomainInfoServiceProxy().deleteDomainInfos(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    {
        log.finer("BEGIN");

        if(domainInfos == null) {
            log.log(Level.WARNING, "createDomainInfos() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = domainInfos.size();
        if(size == 0) {
            log.log(Level.WARNING, "createDomainInfos() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(DomainInfo domainInfo : domainInfos) {
            String guid = createDomainInfo(domainInfo);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createDomainInfos() failed for at least one domainInfo. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateDomainInfos(List<DomainInfo> domainInfos) throws BaseException
    //{
    //}

}
