package com.pagesynopsis.af.service;

import java.util.List;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface RobotsTextRefreshService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException;
    Object getRobotsTextRefresh(String guid, String field) throws BaseException;
    List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException;
    List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException;
    /* @Deprecated */ List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException;
    List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createRobotsTextRefresh(String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException;
    //String createRobotsTextRefresh(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return RobotsTextRefresh?)
    String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException;
    RobotsTextRefresh constructRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException;
    Boolean updateRobotsTextRefresh(String guid, String robotsTextFile, Integer refreshInterval, String note, String status, Integer refreshStatus, String result, Long lastCheckedTime, Long nextCheckedTime, Long expirationTime) throws BaseException;
    //Boolean updateRobotsTextRefresh(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException;
    RobotsTextRefresh refreshRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException;
    Boolean deleteRobotsTextRefresh(String guid) throws BaseException;
    Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException;
    Long deleteRobotsTextRefreshes(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException;
//    Boolean updateRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException;

}
