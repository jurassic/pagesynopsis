package com.pagesynopsis.af.service;

public abstract class AbstractServiceFactory
{
    public abstract ApiConsumerService getApiConsumerService();
    public abstract UserService getUserService();
    public abstract RobotsTextFileService getRobotsTextFileService();
    public abstract RobotsTextRefreshService getRobotsTextRefreshService();
    public abstract OgProfileService getOgProfileService();
    public abstract OgWebsiteService getOgWebsiteService();
    public abstract OgBlogService getOgBlogService();
    public abstract OgArticleService getOgArticleService();
    public abstract OgBookService getOgBookService();
    public abstract OgVideoService getOgVideoService();
    public abstract OgMovieService getOgMovieService();
    public abstract OgTvShowService getOgTvShowService();
    public abstract OgTvEpisodeService getOgTvEpisodeService();
    public abstract TwitterSummaryCardService getTwitterSummaryCardService();
    public abstract TwitterPhotoCardService getTwitterPhotoCardService();
    public abstract TwitterGalleryCardService getTwitterGalleryCardService();
    public abstract TwitterAppCardService getTwitterAppCardService();
    public abstract TwitterPlayerCardService getTwitterPlayerCardService();
    public abstract TwitterProductCardService getTwitterProductCardService();
    public abstract FetchRequestService getFetchRequestService();
    public abstract PageInfoService getPageInfoService();
    public abstract PageFetchService getPageFetchService();
    public abstract LinkListService getLinkListService();
    public abstract ImageSetService getImageSetService();
    public abstract AudioSetService getAudioSetService();
    public abstract VideoSetService getVideoSetService();
    public abstract OpenGraphMetaService getOpenGraphMetaService();
    public abstract TwitterCardMetaService getTwitterCardMetaService();
    public abstract DomainInfoService getDomainInfoService();
    public abstract UrlRatingService getUrlRatingService();
    public abstract ServiceInfoService getServiceInfoService();
    public abstract FiveTenService getFiveTenService();

}
