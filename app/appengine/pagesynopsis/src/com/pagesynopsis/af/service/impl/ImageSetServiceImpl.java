package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.ImageSetBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.ImageSetService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ImageSetServiceImpl implements ImageSetService
{
    private static final Logger log = Logger.getLogger(ImageSetServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "ImageSet-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("ImageSet:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public ImageSetServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // ImageSet related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public ImageSet getImageSet(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getImageSet(): guid = " + guid);

        ImageSetBean bean = null;
        if(getCache() != null) {
            bean = (ImageSetBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ImageSetBean) getProxyFactory().getImageSetServiceProxy().getImageSet(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ImageSetBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ImageSetBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getImageSet(String guid, String field) throws BaseException
    {
        ImageSetBean bean = null;
        if(getCache() != null) {
            bean = (ImageSetBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (ImageSetBean) getProxyFactory().getImageSetServiceProxy().getImageSet(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "ImageSetBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve ImageSetBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return bean.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return bean.getPageUrl();
        } else if(field.equals("queryString")) {
            return bean.getQueryString();
        } else if(field.equals("queryParams")) {
            return bean.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return bean.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("contentType")) {
            return bean.getContentType();
        } else if(field.equals("contentLength")) {
            return bean.getContentLength();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("redirect")) {
            return bean.getRedirect();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("pageTitle")) {
            return bean.getPageTitle();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("deferred")) {
            return bean.isDeferred();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("refreshStatus")) {
            return bean.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return bean.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return bean.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("mediaTypeFilter")) {
            return bean.getMediaTypeFilter();
        } else if(field.equals("pageImages")) {
            return bean.getPageImages();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ImageSet> getImageSets(List<String> guids) throws BaseException
    {
        log.fine("getImageSets()");

        // TBD: Is there a better way????
        List<ImageSet> imageSets = getProxyFactory().getImageSetServiceProxy().getImageSets(guids);
        if(imageSets == null) {
            log.log(Level.WARNING, "Failed to retrieve ImageSetBean list.");
        }

        log.finer("END");
        return imageSets;
    }

    @Override
    public List<ImageSet> getAllImageSets() throws BaseException
    {
        return getAllImageSets(null, null, null);
    }


    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSets(ordering, offset, count, null);
    }

    @Override
    public List<ImageSet> getAllImageSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllImageSets(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ImageSet> imageSets = getProxyFactory().getImageSetServiceProxy().getAllImageSets(ordering, offset, count, forwardCursor);
        if(imageSets == null) {
            log.log(Level.WARNING, "Failed to retrieve ImageSetBean list.");
        }

        log.finer("END");
        return imageSets;
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllImageSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllImageSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllImageSetKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getImageSetServiceProxy().getAllImageSetKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve ImageSetBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ImageSetBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<ImageSet> findImageSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ImageSetServiceImpl.findImageSets(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<ImageSet> imageSets = getProxyFactory().getImageSetServiceProxy().findImageSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(imageSets == null) {
            log.log(Level.WARNING, "Failed to find imageSets for the given criterion.");
        }

        log.finer("END");
        return imageSets;
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findImageSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ImageSetServiceImpl.findImageSetKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getImageSetServiceProxy().findImageSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find ImageSet keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty ImageSet key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("ImageSetServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getImageSetServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createImageSet(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ImageSetBean bean = new ImageSetBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
        return createImageSet(bean);
    }

    @Override
    public String createImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ImageSet bean = constructImageSet(imageSet);
        //return bean.getGuid();

        // Param imageSet cannot be null.....
        if(imageSet == null) {
            log.log(Level.INFO, "Param imageSet is null!");
            throw new BadRequestException("Param imageSet object is null!");
        }
        ImageSetBean bean = null;
        if(imageSet instanceof ImageSetBean) {
            bean = (ImageSetBean) imageSet;
        } else if(imageSet instanceof ImageSet) {
            // bean = new ImageSetBean(null, imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ImageSetBean(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        } else {
            log.log(Level.WARNING, "createImageSet(): Arg imageSet is of an unknown type.");
            //bean = new ImageSetBean();
            bean = new ImageSetBean(imageSet.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getImageSetServiceProxy().createImageSet(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public ImageSet constructImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param imageSet cannot be null.....
        if(imageSet == null) {
            log.log(Level.INFO, "Param imageSet is null!");
            throw new BadRequestException("Param imageSet object is null!");
        }
        ImageSetBean bean = null;
        if(imageSet instanceof ImageSetBean) {
            bean = (ImageSetBean) imageSet;
        } else if(imageSet instanceof ImageSet) {
            // bean = new ImageSetBean(null, imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new ImageSetBean(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        } else {
            log.log(Level.WARNING, "createImageSet(): Arg imageSet is of an unknown type.");
            //bean = new ImageSetBean();
            bean = new ImageSetBean(imageSet.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getImageSetServiceProxy().createImageSet(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateImageSet(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> mediaTypeFilter, Set<ImageStruct> pageImages) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ImageSetBean bean = new ImageSetBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, mediaTypeFilter, pageImages);
        return updateImageSet(bean);
    }
        
    @Override
    public Boolean updateImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //ImageSet bean = refreshImageSet(imageSet);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param imageSet cannot be null.....
        if(imageSet == null || imageSet.getGuid() == null) {
            log.log(Level.INFO, "Param imageSet or its guid is null!");
            throw new BadRequestException("Param imageSet object or its guid is null!");
        }
        ImageSetBean bean = null;
        if(imageSet instanceof ImageSetBean) {
            bean = (ImageSetBean) imageSet;
        } else {  // if(imageSet instanceof ImageSet)
            bean = new ImageSetBean(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        }
        Boolean suc = getProxyFactory().getImageSetServiceProxy().updateImageSet(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public ImageSet refreshImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param imageSet cannot be null.....
        if(imageSet == null || imageSet.getGuid() == null) {
            log.log(Level.INFO, "Param imageSet or its guid is null!");
            throw new BadRequestException("Param imageSet object or its guid is null!");
        }
        ImageSetBean bean = null;
        if(imageSet instanceof ImageSetBean) {
            bean = (ImageSetBean) imageSet;
        } else {  // if(imageSet instanceof ImageSet)
            bean = new ImageSetBean(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        }
        Boolean suc = getProxyFactory().getImageSetServiceProxy().updateImageSet(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteImageSet(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getImageSetServiceProxy().deleteImageSet(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            ImageSet imageSet = null;
            try {
                imageSet = getProxyFactory().getImageSetServiceProxy().getImageSet(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch imageSet with a key, " + guid);
                return false;
            }
            if(imageSet != null) {
                String beanGuid = imageSet.getGuid();
                Boolean suc1 = getProxyFactory().getImageSetServiceProxy().deleteImageSet(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("imageSet with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteImageSet(ImageSet imageSet) throws BaseException
    {
        log.finer("BEGIN");

        // Param imageSet cannot be null.....
        if(imageSet == null || imageSet.getGuid() == null) {
            log.log(Level.INFO, "Param imageSet or its guid is null!");
            throw new BadRequestException("Param imageSet object or its guid is null!");
        }
        ImageSetBean bean = null;
        if(imageSet instanceof ImageSetBean) {
            bean = (ImageSetBean) imageSet;
        } else {  // if(imageSet instanceof ImageSet)
            // ????
            log.warning("imageSet is not an instance of ImageSetBean.");
            bean = new ImageSetBean(imageSet.getGuid(), imageSet.getUser(), imageSet.getFetchRequest(), imageSet.getTargetUrl(), imageSet.getPageUrl(), imageSet.getQueryString(), imageSet.getQueryParams(), imageSet.getLastFetchResult(), imageSet.getResponseCode(), imageSet.getContentType(), imageSet.getContentLength(), imageSet.getLanguage(), imageSet.getRedirect(), imageSet.getLocation(), imageSet.getPageTitle(), imageSet.getNote(), imageSet.isDeferred(), imageSet.getStatus(), imageSet.getRefreshStatus(), imageSet.getRefreshInterval(), imageSet.getNextRefreshTime(), imageSet.getLastCheckedTime(), imageSet.getLastUpdatedTime(), imageSet.getMediaTypeFilter(), imageSet.getPageImages());
        }
        Boolean suc = getProxyFactory().getImageSetServiceProxy().deleteImageSet(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteImageSets(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getImageSetServiceProxy().deleteImageSets(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createImageSets(List<ImageSet> imageSets) throws BaseException
    {
        log.finer("BEGIN");

        if(imageSets == null) {
            log.log(Level.WARNING, "createImageSets() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = imageSets.size();
        if(size == 0) {
            log.log(Level.WARNING, "createImageSets() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ImageSet imageSet : imageSets) {
            String guid = createImageSet(imageSet);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createImageSets() failed for at least one imageSet. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateImageSets(List<ImageSet> imageSets) throws BaseException
    //{
    //}

}
