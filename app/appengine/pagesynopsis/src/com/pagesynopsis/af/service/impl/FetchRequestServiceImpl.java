package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.FetchRequestService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class FetchRequestServiceImpl implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "FetchRequest-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("FetchRequest:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public FetchRequestServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getFetchRequest(): guid = " + guid);

        FetchRequestBean bean = null;
        if(getCache() != null) {
            bean = (FetchRequestBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FetchRequestBean) getProxyFactory().getFetchRequestServiceProxy().getFetchRequest(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FetchRequestBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FetchRequestBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        FetchRequestBean bean = null;
        if(getCache() != null) {
            bean = (FetchRequestBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (FetchRequestBean) getProxyFactory().getFetchRequestServiceProxy().getFetchRequest(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "FetchRequestBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve FetchRequestBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("targetUrl")) {
            return bean.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return bean.getPageUrl();
        } else if(field.equals("queryString")) {
            return bean.getQueryString();
        } else if(field.equals("queryParams")) {
            return bean.getQueryParams();
        } else if(field.equals("version")) {
            return bean.getVersion();
        } else if(field.equals("fetchObject")) {
            return bean.getFetchObject();
        } else if(field.equals("fetchStatus")) {
            return bean.getFetchStatus();
        } else if(field.equals("result")) {
            return bean.getResult();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("nextPageUrl")) {
            return bean.getNextPageUrl();
        } else if(field.equals("followPages")) {
            return bean.getFollowPages();
        } else if(field.equals("followDepth")) {
            return bean.getFollowDepth();
        } else if(field.equals("createVersion")) {
            return bean.isCreateVersion();
        } else if(field.equals("deferred")) {
            return bean.isDeferred();
        } else if(field.equals("alert")) {
            return bean.isAlert();
        } else if(field.equals("notificationPref")) {
            return bean.getNotificationPref();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        log.fine("getFetchRequests()");

        // TBD: Is there a better way????
        List<FetchRequest> fetchRequests = getProxyFactory().getFetchRequestServiceProxy().getFetchRequests(guids);
        if(fetchRequests == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestBean list.");
        }

        log.finer("END");
        return fetchRequests;
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return getAllFetchRequests(null, null, null);
    }


    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequests(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FetchRequest> fetchRequests = getProxyFactory().getFetchRequestServiceProxy().getAllFetchRequests(ordering, offset, count, forwardCursor);
        if(fetchRequests == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestBean list.");
        }

        log.finer("END");
        return fetchRequests;
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllFetchRequestKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFetchRequestServiceProxy().getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve FetchRequestBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FetchRequestBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestServiceImpl.findFetchRequests(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<FetchRequest> fetchRequests = getProxyFactory().getFetchRequestServiceProxy().findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(fetchRequests == null) {
            log.log(Level.WARNING, "Failed to find fetchRequests for the given criterion.");
        }

        log.finer("END");
        return fetchRequests;
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestServiceImpl.findFetchRequestKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getFetchRequestServiceProxy().findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find FetchRequest keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty FetchRequest key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("FetchRequestServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getFetchRequestServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createFetchRequest(String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        NotificationStructBean notificationPrefBean = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefBean = (NotificationStructBean) notificationPref;
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefBean = new NotificationStructBean(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            notificationPrefBean = null;   // ????
        }
        FetchRequestBean bean = new FetchRequestBean(null, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfoBean, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPrefBean);
        return createFetchRequest(bean);
    }

    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FetchRequest bean = constructFetchRequest(fetchRequest);
        //return bean.getGuid();

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null) {
            log.log(Level.INFO, "Param fetchRequest is null!");
            throw new BadRequestException("Param fetchRequest object is null!");
        }
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        } else if(fetchRequest instanceof FetchRequest) {
            // bean = new FetchRequestBean(null, fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FetchRequestBean(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
        } else {
            log.log(Level.WARNING, "createFetchRequest(): Arg fetchRequest is of an unknown type.");
            //bean = new FetchRequestBean();
            bean = new FetchRequestBean(fetchRequest.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFetchRequestServiceProxy().createFetchRequest(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public FetchRequest constructFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null) {
            log.log(Level.INFO, "Param fetchRequest is null!");
            throw new BadRequestException("Param fetchRequest object is null!");
        }
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        } else if(fetchRequest instanceof FetchRequest) {
            // bean = new FetchRequestBean(null, fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new FetchRequestBean(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
        } else {
            log.log(Level.WARNING, "createFetchRequest(): Arg fetchRequest is of an unknown type.");
            //bean = new FetchRequestBean();
            bean = new FetchRequestBean(fetchRequest.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getFetchRequestServiceProxy().createFetchRequest(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateFetchRequest(String guid, String user, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String version, String fetchObject, Integer fetchStatus, String result, String note, ReferrerInfoStruct referrerInfo, String status, String nextPageUrl, Integer followPages, Integer followDepth, Boolean createVersion, Boolean deferred, Boolean alert, NotificationStruct notificationPref) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        ReferrerInfoStructBean referrerInfoBean = null;
        if(referrerInfo instanceof ReferrerInfoStructBean) {
            referrerInfoBean = (ReferrerInfoStructBean) referrerInfo;
        } else if(referrerInfo instanceof ReferrerInfoStruct) {
            referrerInfoBean = new ReferrerInfoStructBean(referrerInfo.getReferer(), referrerInfo.getUserAgent(), referrerInfo.getLanguage(), referrerInfo.getHostname(), referrerInfo.getIpAddress(), referrerInfo.getNote());
        } else {
            referrerInfoBean = null;   // ????
        }
        NotificationStructBean notificationPrefBean = null;
        if(notificationPref instanceof NotificationStructBean) {
            notificationPrefBean = (NotificationStructBean) notificationPref;
        } else if(notificationPref instanceof NotificationStruct) {
            notificationPrefBean = new NotificationStructBean(notificationPref.getPreferredMode(), notificationPref.getMobileNumber(), notificationPref.getEmailAddress(), notificationPref.getTwitterUsername(), notificationPref.getFacebookId(), notificationPref.getLinkedinId(), notificationPref.getNote());
        } else {
            notificationPrefBean = null;   // ????
        }
        FetchRequestBean bean = new FetchRequestBean(guid, user, targetUrl, pageUrl, queryString, queryParams, version, fetchObject, fetchStatus, result, note, referrerInfoBean, status, nextPageUrl, followPages, followDepth, createVersion, deferred, alert, notificationPrefBean);
        return updateFetchRequest(bean);
    }
        
    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //FetchRequest bean = refreshFetchRequest(fetchRequest);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        } else {  // if(fetchRequest instanceof FetchRequest)
            bean = new FetchRequestBean(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
        }
        Boolean suc = getProxyFactory().getFetchRequestServiceProxy().updateFetchRequest(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public FetchRequest refreshFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        } else {  // if(fetchRequest instanceof FetchRequest)
            bean = new FetchRequestBean(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
        }
        Boolean suc = getProxyFactory().getFetchRequestServiceProxy().updateFetchRequest(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getFetchRequestServiceProxy().deleteFetchRequest(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            FetchRequest fetchRequest = null;
            try {
                fetchRequest = getProxyFactory().getFetchRequestServiceProxy().getFetchRequest(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch fetchRequest with a key, " + guid);
                return false;
            }
            if(fetchRequest != null) {
                String beanGuid = fetchRequest.getGuid();
                Boolean suc1 = getProxyFactory().getFetchRequestServiceProxy().deleteFetchRequest(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("fetchRequest with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("BEGIN");

        // Param fetchRequest cannot be null.....
        if(fetchRequest == null || fetchRequest.getGuid() == null) {
            log.log(Level.INFO, "Param fetchRequest or its guid is null!");
            throw new BadRequestException("Param fetchRequest object or its guid is null!");
        }
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        } else {  // if(fetchRequest instanceof FetchRequest)
            // ????
            log.warning("fetchRequest is not an instance of FetchRequestBean.");
            bean = new FetchRequestBean(fetchRequest.getGuid(), fetchRequest.getUser(), fetchRequest.getTargetUrl(), fetchRequest.getPageUrl(), fetchRequest.getQueryString(), fetchRequest.getQueryParams(), fetchRequest.getVersion(), fetchRequest.getFetchObject(), fetchRequest.getFetchStatus(), fetchRequest.getResult(), fetchRequest.getNote(), (ReferrerInfoStructBean) fetchRequest.getReferrerInfo(), fetchRequest.getStatus(), fetchRequest.getNextPageUrl(), fetchRequest.getFollowPages(), fetchRequest.getFollowDepth(), fetchRequest.isCreateVersion(), fetchRequest.isDeferred(), fetchRequest.isAlert(), (NotificationStructBean) fetchRequest.getNotificationPref());
        }
        Boolean suc = getProxyFactory().getFetchRequestServiceProxy().deleteFetchRequest(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteFetchRequests(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getFetchRequestServiceProxy().deleteFetchRequests(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    {
        log.finer("BEGIN");

        if(fetchRequests == null) {
            log.log(Level.WARNING, "createFetchRequests() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = fetchRequests.size();
        if(size == 0) {
            log.log(Level.WARNING, "createFetchRequests() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(FetchRequest fetchRequest : fetchRequests) {
            String guid = createFetchRequest(fetchRequest);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createFetchRequests() failed for at least one fetchRequest. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    //{
    //}

}
