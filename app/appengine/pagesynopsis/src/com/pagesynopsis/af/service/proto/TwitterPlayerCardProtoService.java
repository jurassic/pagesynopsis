package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.impl.TwitterPlayerCardServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TwitterPlayerCardProtoService extends TwitterPlayerCardServiceImpl implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterPlayerCardProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterPlayerCard related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterPlayerCard getTwitterPlayerCard(String guid) throws BaseException
    {
        return super.getTwitterPlayerCard(guid);
    }

    @Override
    public Object getTwitterPlayerCard(String guid, String field) throws BaseException
    {
        return super.getTwitterPlayerCard(guid, field);
    }

    @Override
    public List<TwitterPlayerCard> getTwitterPlayerCards(List<String> guids) throws BaseException
    {
        return super.getTwitterPlayerCards(guids);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards() throws BaseException
    {
        return super.getAllTwitterPlayerCards();
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> getAllTwitterPlayerCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllTwitterPlayerCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterPlayerCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllTwitterPlayerCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterPlayerCard> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return super.createTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public TwitterPlayerCard constructTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return super.constructTwitterPlayerCard(twitterPlayerCard);
    }


    @Override
    public Boolean updateTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return super.updateTwitterPlayerCard(twitterPlayerCard);
    }
        
    @Override
    public TwitterPlayerCard refreshTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return super.refreshTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(String guid) throws BaseException
    {
        return super.deleteTwitterPlayerCard(guid);
    }

    @Override
    public Boolean deleteTwitterPlayerCard(TwitterPlayerCard twitterPlayerCard) throws BaseException
    {
        return super.deleteTwitterPlayerCard(twitterPlayerCard);
    }

    @Override
    public Integer createTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    {
        return super.createTwitterPlayerCards(twitterPlayerCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterPlayerCards(List<TwitterPlayerCard> twitterPlayerCards) throws BaseException
    //{
    //}

}
