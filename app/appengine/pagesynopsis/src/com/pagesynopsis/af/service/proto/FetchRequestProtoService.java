package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.NotificationStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.ReferrerInfoStruct;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.impl.FetchRequestServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class FetchRequestProtoService extends FetchRequestServiceImpl implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public FetchRequestProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        return super.getFetchRequest(guid);
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return super.getFetchRequest(guid, field);
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        return super.getFetchRequests(guids);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return super.getAllFetchRequests();
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequests(ordering, offset, count, null);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllFetchRequests(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllFetchRequestKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllFetchRequestKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.createFetchRequest(fetchRequest);
    }

    @Override
    public FetchRequest constructFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.constructFetchRequest(fetchRequest);
    }


    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.updateFetchRequest(fetchRequest);
    }
        
    @Override
    public FetchRequest refreshFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.refreshFetchRequest(fetchRequest);
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return super.deleteFetchRequest(guid);
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.deleteFetchRequest(fetchRequest);
    }

    @Override
    public Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    {
        return super.createFetchRequests(fetchRequests);
    }

    // TBD
    //@Override
    //public Boolean updateFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    //{
    //}

}
