package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.LinkListService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkListServiceImpl implements LinkListService
{
    private static final Logger log = Logger.getLogger(LinkListServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "LinkList-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("LinkList:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public LinkListServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkList related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getLinkList(): guid = " + guid);

        LinkListBean bean = null;
        if(getCache() != null) {
            bean = (LinkListBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkListBean) getProxyFactory().getLinkListServiceProxy().getLinkList(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkListBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkListBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        LinkListBean bean = null;
        if(getCache() != null) {
            bean = (LinkListBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (LinkListBean) getProxyFactory().getLinkListServiceProxy().getLinkList(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "LinkListBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve LinkListBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return bean.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return bean.getPageUrl();
        } else if(field.equals("queryString")) {
            return bean.getQueryString();
        } else if(field.equals("queryParams")) {
            return bean.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return bean.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("contentType")) {
            return bean.getContentType();
        } else if(field.equals("contentLength")) {
            return bean.getContentLength();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("redirect")) {
            return bean.getRedirect();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("pageTitle")) {
            return bean.getPageTitle();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("deferred")) {
            return bean.isDeferred();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("refreshStatus")) {
            return bean.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return bean.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return bean.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("urlSchemeFilter")) {
            return bean.getUrlSchemeFilter();
        } else if(field.equals("pageAnchors")) {
            return bean.getPageAnchors();
        } else if(field.equals("excludeRelativeUrls")) {
            return bean.isExcludeRelativeUrls();
        } else if(field.equals("excludedBaseUrls")) {
            return bean.getExcludedBaseUrls();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        log.fine("getLinkLists()");

        // TBD: Is there a better way????
        List<LinkList> linkLists = getProxyFactory().getLinkListServiceProxy().getLinkLists(guids);
        if(linkLists == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkListBean list.");
        }

        log.finer("END");
        return linkLists;
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return getAllLinkLists(null, null, null);
    }


    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkLists(ordering, offset, count, null);
    }

    @Override
    public List<LinkList> getAllLinkLists(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkLists(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<LinkList> linkLists = getProxyFactory().getLinkListServiceProxy().getAllLinkLists(ordering, offset, count, forwardCursor);
        if(linkLists == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkListBean list.");
        }

        log.finer("END");
        return linkLists;
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllLinkListKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllLinkListKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkListServiceProxy().getAllLinkListKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve LinkListBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkListBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListServiceImpl.findLinkLists(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<LinkList> linkLists = getProxyFactory().getLinkListServiceProxy().findLinkLists(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(linkLists == null) {
            log.log(Level.WARNING, "Failed to find linkLists for the given criterion.");
        }

        log.finer("END");
        return linkLists;
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListServiceImpl.findLinkListKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getLinkListServiceProxy().findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find LinkList keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty LinkList key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("LinkListServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getLinkListServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createLinkList(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        LinkListBean bean = new LinkListBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
        return createLinkList(bean);
    }

    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkList bean = constructLinkList(linkList);
        //return bean.getGuid();

        // Param linkList cannot be null.....
        if(linkList == null) {
            log.log(Level.INFO, "Param linkList is null!");
            throw new BadRequestException("Param linkList object is null!");
        }
        LinkListBean bean = null;
        if(linkList instanceof LinkListBean) {
            bean = (LinkListBean) linkList;
        } else if(linkList instanceof LinkList) {
            // bean = new LinkListBean(null, linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkListBean(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        } else {
            log.log(Level.WARNING, "createLinkList(): Arg linkList is of an unknown type.");
            //bean = new LinkListBean();
            bean = new LinkListBean(linkList.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkListServiceProxy().createLinkList(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public LinkList constructLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkList cannot be null.....
        if(linkList == null) {
            log.log(Level.INFO, "Param linkList is null!");
            throw new BadRequestException("Param linkList object is null!");
        }
        LinkListBean bean = null;
        if(linkList instanceof LinkListBean) {
            bean = (LinkListBean) linkList;
        } else if(linkList instanceof LinkList) {
            // bean = new LinkListBean(null, linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new LinkListBean(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        } else {
            log.log(Level.WARNING, "createLinkList(): Arg linkList is of an unknown type.");
            //bean = new LinkListBean();
            bean = new LinkListBean(linkList.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getLinkListServiceProxy().createLinkList(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateLinkList(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, List<String> urlSchemeFilter, List<AnchorStruct> pageAnchors, Boolean excludeRelativeUrls, List<String> excludedBaseUrls) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        LinkListBean bean = new LinkListBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, urlSchemeFilter, pageAnchors, excludeRelativeUrls, excludedBaseUrls);
        return updateLinkList(bean);
    }
        
    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //LinkList bean = refreshLinkList(linkList);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param linkList cannot be null.....
        if(linkList == null || linkList.getGuid() == null) {
            log.log(Level.INFO, "Param linkList or its guid is null!");
            throw new BadRequestException("Param linkList object or its guid is null!");
        }
        LinkListBean bean = null;
        if(linkList instanceof LinkListBean) {
            bean = (LinkListBean) linkList;
        } else {  // if(linkList instanceof LinkList)
            bean = new LinkListBean(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        }
        Boolean suc = getProxyFactory().getLinkListServiceProxy().updateLinkList(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public LinkList refreshLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkList cannot be null.....
        if(linkList == null || linkList.getGuid() == null) {
            log.log(Level.INFO, "Param linkList or its guid is null!");
            throw new BadRequestException("Param linkList object or its guid is null!");
        }
        LinkListBean bean = null;
        if(linkList instanceof LinkListBean) {
            bean = (LinkListBean) linkList;
        } else {  // if(linkList instanceof LinkList)
            bean = new LinkListBean(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        }
        Boolean suc = getProxyFactory().getLinkListServiceProxy().updateLinkList(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getLinkListServiceProxy().deleteLinkList(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            LinkList linkList = null;
            try {
                linkList = getProxyFactory().getLinkListServiceProxy().getLinkList(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch linkList with a key, " + guid);
                return false;
            }
            if(linkList != null) {
                String beanGuid = linkList.getGuid();
                Boolean suc1 = getProxyFactory().getLinkListServiceProxy().deleteLinkList(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("linkList with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        log.finer("BEGIN");

        // Param linkList cannot be null.....
        if(linkList == null || linkList.getGuid() == null) {
            log.log(Level.INFO, "Param linkList or its guid is null!");
            throw new BadRequestException("Param linkList object or its guid is null!");
        }
        LinkListBean bean = null;
        if(linkList instanceof LinkListBean) {
            bean = (LinkListBean) linkList;
        } else {  // if(linkList instanceof LinkList)
            // ????
            log.warning("linkList is not an instance of LinkListBean.");
            bean = new LinkListBean(linkList.getGuid(), linkList.getUser(), linkList.getFetchRequest(), linkList.getTargetUrl(), linkList.getPageUrl(), linkList.getQueryString(), linkList.getQueryParams(), linkList.getLastFetchResult(), linkList.getResponseCode(), linkList.getContentType(), linkList.getContentLength(), linkList.getLanguage(), linkList.getRedirect(), linkList.getLocation(), linkList.getPageTitle(), linkList.getNote(), linkList.isDeferred(), linkList.getStatus(), linkList.getRefreshStatus(), linkList.getRefreshInterval(), linkList.getNextRefreshTime(), linkList.getLastCheckedTime(), linkList.getLastUpdatedTime(), linkList.getUrlSchemeFilter(), linkList.getPageAnchors(), linkList.isExcludeRelativeUrls(), linkList.getExcludedBaseUrls());
        }
        Boolean suc = getProxyFactory().getLinkListServiceProxy().deleteLinkList(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteLinkLists(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getLinkListServiceProxy().deleteLinkLists(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createLinkLists(List<LinkList> linkLists) throws BaseException
    {
        log.finer("BEGIN");

        if(linkLists == null) {
            log.log(Level.WARNING, "createLinkLists() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = linkLists.size();
        if(size == 0) {
            log.log(Level.WARNING, "createLinkLists() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(LinkList linkList : linkLists) {
            String guid = createLinkList(linkList);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createLinkLists() failed for at least one linkList. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateLinkLists(List<LinkList> linkLists) throws BaseException
    //{
    //}

}
