package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.impl.PageFetchServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class PageFetchProtoService extends PageFetchServiceImpl implements PageFetchService
{
    private static final Logger log = Logger.getLogger(PageFetchProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public PageFetchProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // PageFetch related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        return super.getPageFetch(guid);
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        return super.getPageFetch(guid, field);
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        return super.getPageFetches(guids);
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return super.getAllPageFetches();
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllPageFetches(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllPageFetchKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        return super.createPageFetch(pageFetch);
    }

    @Override
    public PageFetch constructPageFetch(PageFetch pageFetch) throws BaseException
    {
        return super.constructPageFetch(pageFetch);
    }


    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        return super.updatePageFetch(pageFetch);
    }
        
    @Override
    public PageFetch refreshPageFetch(PageFetch pageFetch) throws BaseException
    {
        return super.refreshPageFetch(pageFetch);
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        return super.deletePageFetch(guid);
    }

    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        return super.deletePageFetch(pageFetch);
    }

    @Override
    public Integer createPageFetches(List<PageFetch> pageFetches) throws BaseException
    {
        return super.createPageFetches(pageFetches);
    }

    // TBD
    //@Override
    //public Boolean updatePageFetches(List<PageFetch> pageFetches) throws BaseException
    //{
    //}

}
