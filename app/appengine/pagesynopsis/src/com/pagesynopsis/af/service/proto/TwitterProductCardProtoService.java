package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.TwitterCardAppInfo;
import com.pagesynopsis.ws.TwitterCardProductData;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.impl.TwitterProductCardServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class TwitterProductCardProtoService extends TwitterProductCardServiceImpl implements TwitterProductCardService
{
    private static final Logger log = Logger.getLogger(TwitterProductCardProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterProductCardProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterProductCard related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterProductCard getTwitterProductCard(String guid) throws BaseException
    {
        return super.getTwitterProductCard(guid);
    }

    @Override
    public Object getTwitterProductCard(String guid, String field) throws BaseException
    {
        return super.getTwitterProductCard(guid, field);
    }

    @Override
    public List<TwitterProductCard> getTwitterProductCards(List<String> guids) throws BaseException
    {
        return super.getTwitterProductCards(guids);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards() throws BaseException
    {
        return super.getAllTwitterProductCards();
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCards(ordering, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> getAllTwitterProductCards(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllTwitterProductCards(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterProductCardKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllTwitterProductCardKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterProductCard> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return super.createTwitterProductCard(twitterProductCard);
    }

    @Override
    public TwitterProductCard constructTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return super.constructTwitterProductCard(twitterProductCard);
    }


    @Override
    public Boolean updateTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return super.updateTwitterProductCard(twitterProductCard);
    }
        
    @Override
    public TwitterProductCard refreshTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return super.refreshTwitterProductCard(twitterProductCard);
    }

    @Override
    public Boolean deleteTwitterProductCard(String guid) throws BaseException
    {
        return super.deleteTwitterProductCard(guid);
    }

    @Override
    public Boolean deleteTwitterProductCard(TwitterProductCard twitterProductCard) throws BaseException
    {
        return super.deleteTwitterProductCard(twitterProductCard);
    }

    @Override
    public Integer createTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException
    {
        return super.createTwitterProductCards(twitterProductCards);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterProductCards(List<TwitterProductCard> twitterProductCards) throws BaseException
    //{
    //}

}
