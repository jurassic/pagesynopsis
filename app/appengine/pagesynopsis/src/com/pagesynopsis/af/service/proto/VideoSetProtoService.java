package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.VideoSet;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.VideoSetBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.impl.VideoSetServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class VideoSetProtoService extends VideoSetServiceImpl implements VideoSetService
{
    private static final Logger log = Logger.getLogger(VideoSetProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public VideoSetProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // VideoSet related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public VideoSet getVideoSet(String guid) throws BaseException
    {
        return super.getVideoSet(guid);
    }

    @Override
    public Object getVideoSet(String guid, String field) throws BaseException
    {
        return super.getVideoSet(guid, field);
    }

    @Override
    public List<VideoSet> getVideoSets(List<String> guids) throws BaseException
    {
        return super.getVideoSets(guids);
    }

    @Override
    public List<VideoSet> getAllVideoSets() throws BaseException
    {
        return super.getAllVideoSets();
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSets(ordering, offset, count, null);
    }

    @Override
    public List<VideoSet> getAllVideoSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllVideoSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllVideoSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllVideoSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllVideoSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<VideoSet> findVideoSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findVideoSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findVideoSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findVideoSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createVideoSet(VideoSet videoSet) throws BaseException
    {
        return super.createVideoSet(videoSet);
    }

    @Override
    public VideoSet constructVideoSet(VideoSet videoSet) throws BaseException
    {
        return super.constructVideoSet(videoSet);
    }


    @Override
    public Boolean updateVideoSet(VideoSet videoSet) throws BaseException
    {
        return super.updateVideoSet(videoSet);
    }
        
    @Override
    public VideoSet refreshVideoSet(VideoSet videoSet) throws BaseException
    {
        return super.refreshVideoSet(videoSet);
    }

    @Override
    public Boolean deleteVideoSet(String guid) throws BaseException
    {
        return super.deleteVideoSet(guid);
    }

    @Override
    public Boolean deleteVideoSet(VideoSet videoSet) throws BaseException
    {
        return super.deleteVideoSet(videoSet);
    }

    @Override
    public Integer createVideoSets(List<VideoSet> videoSets) throws BaseException
    {
        return super.createVideoSets(videoSets);
    }

    // TBD
    //@Override
    //public Boolean updateVideoSets(List<VideoSet> videoSets) throws BaseException
    //{
    //}

}
