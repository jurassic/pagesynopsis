package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgProfileService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgProfileServiceImpl implements OgProfileService
{
    private static final Logger log = Logger.getLogger(OgProfileServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "OgProfile-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("OgProfile:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public OgProfileServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgProfile related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgProfile(): guid = " + guid);

        OgProfileBean bean = null;
        if(getCache() != null) {
            bean = (OgProfileBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgProfileBean) getProxyFactory().getOgProfileServiceProxy().getOgProfile(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgProfileBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgProfileBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        OgProfileBean bean = null;
        if(getCache() != null) {
            bean = (OgProfileBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgProfileBean) getProxyFactory().getOgProfileServiceProxy().getOgProfile(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgProfileBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgProfileBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("profileId")) {
            return bean.getProfileId();
        } else if(field.equals("firstName")) {
            return bean.getFirstName();
        } else if(field.equals("lastName")) {
            return bean.getLastName();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("gender")) {
            return bean.getGender();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        log.fine("getOgProfiles()");

        // TBD: Is there a better way????
        List<OgProfile> ogProfiles = getProxyFactory().getOgProfileServiceProxy().getOgProfiles(guids);
        if(ogProfiles == null) {
            log.log(Level.WARNING, "Failed to retrieve OgProfileBean list.");
        }

        log.finer("END");
        return ogProfiles;
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return getAllOgProfiles(null, null, null);
    }


    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfiles(ordering, offset, count, null);
    }

    @Override
    public List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfiles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgProfile> ogProfiles = getProxyFactory().getOgProfileServiceProxy().getAllOgProfiles(ordering, offset, count, forwardCursor);
        if(ogProfiles == null) {
            log.log(Level.WARNING, "Failed to retrieve OgProfileBean list.");
        }

        log.finer("END");
        return ogProfiles;
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgProfileKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgProfileKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgProfileServiceProxy().getAllOgProfileKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgProfileBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgProfileBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileServiceImpl.findOgProfiles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgProfile> ogProfiles = getProxyFactory().getOgProfileServiceProxy().findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(ogProfiles == null) {
            log.log(Level.WARNING, "Failed to find ogProfiles for the given criterion.");
        }

        log.finer("END");
        return ogProfiles;
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileServiceImpl.findOgProfileKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgProfileServiceProxy().findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgProfile keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgProfile key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgProfileServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getOgProfileServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        OgProfileBean bean = new OgProfileBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
        return createOgProfile(bean);
    }

    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgProfile bean = constructOgProfile(ogProfile);
        //return bean.getGuid();

        // Param ogProfile cannot be null.....
        if(ogProfile == null) {
            log.log(Level.INFO, "Param ogProfile is null!");
            throw new BadRequestException("Param ogProfile object is null!");
        }
        OgProfileBean bean = null;
        if(ogProfile instanceof OgProfileBean) {
            bean = (OgProfileBean) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            // bean = new OgProfileBean(null, ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        } else {
            log.log(Level.WARNING, "createOgProfile(): Arg ogProfile is of an unknown type.");
            //bean = new OgProfileBean();
            bean = new OgProfileBean(ogProfile.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgProfileServiceProxy().createOgProfile(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OgProfile constructOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogProfile cannot be null.....
        if(ogProfile == null) {
            log.log(Level.INFO, "Param ogProfile is null!");
            throw new BadRequestException("Param ogProfile object is null!");
        }
        OgProfileBean bean = null;
        if(ogProfile instanceof OgProfileBean) {
            bean = (OgProfileBean) ogProfile;
        } else if(ogProfile instanceof OgProfile) {
            // bean = new OgProfileBean(null, ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        } else {
            log.log(Level.WARNING, "createOgProfile(): Arg ogProfile is of an unknown type.");
            //bean = new OgProfileBean();
            bean = new OgProfileBean(ogProfile.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgProfileServiceProxy().createOgProfile(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgProfileBean bean = new OgProfileBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, profileId, firstName, lastName, username, gender);
        return updateOgProfile(bean);
    }
        
    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgProfile bean = refreshOgProfile(ogProfile);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param ogProfile cannot be null.....
        if(ogProfile == null || ogProfile.getGuid() == null) {
            log.log(Level.INFO, "Param ogProfile or its guid is null!");
            throw new BadRequestException("Param ogProfile object or its guid is null!");
        }
        OgProfileBean bean = null;
        if(ogProfile instanceof OgProfileBean) {
            bean = (OgProfileBean) ogProfile;
        } else {  // if(ogProfile instanceof OgProfile)
            bean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        }
        Boolean suc = getProxyFactory().getOgProfileServiceProxy().updateOgProfile(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OgProfile refreshOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogProfile cannot be null.....
        if(ogProfile == null || ogProfile.getGuid() == null) {
            log.log(Level.INFO, "Param ogProfile or its guid is null!");
            throw new BadRequestException("Param ogProfile object or its guid is null!");
        }
        OgProfileBean bean = null;
        if(ogProfile instanceof OgProfileBean) {
            bean = (OgProfileBean) ogProfile;
        } else {  // if(ogProfile instanceof OgProfile)
            bean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        }
        Boolean suc = getProxyFactory().getOgProfileServiceProxy().updateOgProfile(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getOgProfileServiceProxy().deleteOgProfile(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            OgProfile ogProfile = null;
            try {
                ogProfile = getProxyFactory().getOgProfileServiceProxy().getOgProfile(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch ogProfile with a key, " + guid);
                return false;
            }
            if(ogProfile != null) {
                String beanGuid = ogProfile.getGuid();
                Boolean suc1 = getProxyFactory().getOgProfileServiceProxy().deleteOgProfile(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("ogProfile with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogProfile cannot be null.....
        if(ogProfile == null || ogProfile.getGuid() == null) {
            log.log(Level.INFO, "Param ogProfile or its guid is null!");
            throw new BadRequestException("Param ogProfile object or its guid is null!");
        }
        OgProfileBean bean = null;
        if(ogProfile instanceof OgProfileBean) {
            bean = (OgProfileBean) ogProfile;
        } else {  // if(ogProfile instanceof OgProfile)
            // ????
            log.warning("ogProfile is not an instance of OgProfileBean.");
            bean = new OgProfileBean(ogProfile.getGuid(), ogProfile.getUrl(), ogProfile.getType(), ogProfile.getSiteName(), ogProfile.getTitle(), ogProfile.getDescription(), ogProfile.getFbAdmins(), ogProfile.getFbAppId(), ogProfile.getImage(), ogProfile.getAudio(), ogProfile.getVideo(), ogProfile.getLocale(), ogProfile.getLocaleAlternate(), ogProfile.getProfileId(), ogProfile.getFirstName(), ogProfile.getLastName(), ogProfile.getUsername(), ogProfile.getGender());
        }
        Boolean suc = getProxyFactory().getOgProfileServiceProxy().deleteOgProfile(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getOgProfileServiceProxy().deleteOgProfiles(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgProfiles(List<OgProfile> ogProfiles) throws BaseException
    {
        log.finer("BEGIN");

        if(ogProfiles == null) {
            log.log(Level.WARNING, "createOgProfiles() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = ogProfiles.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOgProfiles() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OgProfile ogProfile : ogProfiles) {
            String guid = createOgProfile(ogProfile);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOgProfiles() failed for at least one ogProfile. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgProfiles(List<OgProfile> ogProfiles) throws BaseException
    //{
    //}

}
