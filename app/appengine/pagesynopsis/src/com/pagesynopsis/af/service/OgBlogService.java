package com.pagesynopsis.af.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgBlogService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgBlog getOgBlog(String guid) throws BaseException;
    Object getOgBlog(String guid, String field) throws BaseException;
    List<OgBlog> getOgBlogs(List<String> guids) throws BaseException;
    List<OgBlog> getAllOgBlogs() throws BaseException;
    /* @Deprecated */ List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException;
    List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgBlog(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException;
    //String createOgBlog(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgBlog?)
    String createOgBlog(OgBlog ogBlog) throws BaseException;
    OgBlog constructOgBlog(OgBlog ogBlog) throws BaseException;
    Boolean updateOgBlog(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String note) throws BaseException;
    //Boolean updateOgBlog(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgBlog(OgBlog ogBlog) throws BaseException;
    OgBlog refreshOgBlog(OgBlog ogBlog) throws BaseException;
    Boolean deleteOgBlog(String guid) throws BaseException;
    Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException;
    Long deleteOgBlogs(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createOgBlogs(List<OgBlog> ogBlogs) throws BaseException;
//    Boolean updateOgBlogs(List<OgBlog> ogBlogs) throws BaseException;

}
