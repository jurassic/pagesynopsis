package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.impl.OgBookServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgBookProtoService extends OgBookServiceImpl implements OgBookService
{
    private static final Logger log = Logger.getLogger(OgBookProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgBookProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgBook related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgBook getOgBook(String guid) throws BaseException
    {
        return super.getOgBook(guid);
    }

    @Override
    public Object getOgBook(String guid, String field) throws BaseException
    {
        return super.getOgBook(guid, field);
    }

    @Override
    public List<OgBook> getOgBooks(List<String> guids) throws BaseException
    {
        return super.getOgBooks(guids);
    }

    @Override
    public List<OgBook> getAllOgBooks() throws BaseException
    {
        return super.getAllOgBooks();
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBooks(ordering, offset, count, null);
    }

    @Override
    public List<OgBook> getAllOgBooks(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgBooks(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBookKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBookKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgBookKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBook> findOgBooks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgBooks(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBookKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgBookKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgBook(OgBook ogBook) throws BaseException
    {
        return super.createOgBook(ogBook);
    }

    @Override
    public OgBook constructOgBook(OgBook ogBook) throws BaseException
    {
        return super.constructOgBook(ogBook);
    }


    @Override
    public Boolean updateOgBook(OgBook ogBook) throws BaseException
    {
        return super.updateOgBook(ogBook);
    }
        
    @Override
    public OgBook refreshOgBook(OgBook ogBook) throws BaseException
    {
        return super.refreshOgBook(ogBook);
    }

    @Override
    public Boolean deleteOgBook(String guid) throws BaseException
    {
        return super.deleteOgBook(guid);
    }

    @Override
    public Boolean deleteOgBook(OgBook ogBook) throws BaseException
    {
        return super.deleteOgBook(ogBook);
    }

    @Override
    public Integer createOgBooks(List<OgBook> ogBooks) throws BaseException
    {
        return super.createOgBooks(ogBooks);
    }

    // TBD
    //@Override
    //public Boolean updateOgBooks(List<OgBook> ogBooks) throws BaseException
    //{
    //}

}
