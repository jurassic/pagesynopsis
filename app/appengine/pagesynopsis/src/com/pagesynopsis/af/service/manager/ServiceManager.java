package com.pagesynopsis.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.AbstractServiceFactory;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;

// TBD:
// Factory? DI? (Does the current "dual" approach make sense?)
// Make it a singleton? Implement pooling, etc. ????
public final class ServiceManager
{
    private static final Logger log = Logger.getLogger(ServiceManager.class.getName());

    // Reference to the Abstract factory.
    private static AbstractServiceFactory serviceFactory = ServiceFactoryManager.getServiceFactory();

    // All service getters/setters are delegated to ServiceController.
    // TBD: Use a DI framework such as Spring or Guice....
    private static ServiceController serviceController = new ServiceController(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

    // Static methods only.
    private ServiceManager() {}

    // Returns a ApiConsumerService instance.
	public static ApiConsumerService getApiConsumerService() 
    {
        if(serviceController.getApiConsumerService() == null) {
            serviceController.setApiConsumerService(serviceFactory.getApiConsumerService());
        }
        return serviceController.getApiConsumerService();
    }
    // Injects a ApiConsumerService instance.
	public static void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        serviceController.setApiConsumerService(apiConsumerService);
    }

    // Returns a UserService instance.
	public static UserService getUserService() 
    {
        if(serviceController.getUserService() == null) {
            serviceController.setUserService(serviceFactory.getUserService());
        }
        return serviceController.getUserService();
    }
    // Injects a UserService instance.
	public static void setUserService(UserService userService) 
    {
        serviceController.setUserService(userService);
    }

    // Returns a RobotsTextFileService instance.
	public static RobotsTextFileService getRobotsTextFileService() 
    {
        if(serviceController.getRobotsTextFileService() == null) {
            serviceController.setRobotsTextFileService(serviceFactory.getRobotsTextFileService());
        }
        return serviceController.getRobotsTextFileService();
    }
    // Injects a RobotsTextFileService instance.
	public static void setRobotsTextFileService(RobotsTextFileService robotsTextFileService) 
    {
        serviceController.setRobotsTextFileService(robotsTextFileService);
    }

    // Returns a RobotsTextRefreshService instance.
	public static RobotsTextRefreshService getRobotsTextRefreshService() 
    {
        if(serviceController.getRobotsTextRefreshService() == null) {
            serviceController.setRobotsTextRefreshService(serviceFactory.getRobotsTextRefreshService());
        }
        return serviceController.getRobotsTextRefreshService();
    }
    // Injects a RobotsTextRefreshService instance.
	public static void setRobotsTextRefreshService(RobotsTextRefreshService robotsTextRefreshService) 
    {
        serviceController.setRobotsTextRefreshService(robotsTextRefreshService);
    }

    // Returns a OgProfileService instance.
	public static OgProfileService getOgProfileService() 
    {
        if(serviceController.getOgProfileService() == null) {
            serviceController.setOgProfileService(serviceFactory.getOgProfileService());
        }
        return serviceController.getOgProfileService();
    }
    // Injects a OgProfileService instance.
	public static void setOgProfileService(OgProfileService ogProfileService) 
    {
        serviceController.setOgProfileService(ogProfileService);
    }

    // Returns a OgWebsiteService instance.
	public static OgWebsiteService getOgWebsiteService() 
    {
        if(serviceController.getOgWebsiteService() == null) {
            serviceController.setOgWebsiteService(serviceFactory.getOgWebsiteService());
        }
        return serviceController.getOgWebsiteService();
    }
    // Injects a OgWebsiteService instance.
	public static void setOgWebsiteService(OgWebsiteService ogWebsiteService) 
    {
        serviceController.setOgWebsiteService(ogWebsiteService);
    }

    // Returns a OgBlogService instance.
	public static OgBlogService getOgBlogService() 
    {
        if(serviceController.getOgBlogService() == null) {
            serviceController.setOgBlogService(serviceFactory.getOgBlogService());
        }
        return serviceController.getOgBlogService();
    }
    // Injects a OgBlogService instance.
	public static void setOgBlogService(OgBlogService ogBlogService) 
    {
        serviceController.setOgBlogService(ogBlogService);
    }

    // Returns a OgArticleService instance.
	public static OgArticleService getOgArticleService() 
    {
        if(serviceController.getOgArticleService() == null) {
            serviceController.setOgArticleService(serviceFactory.getOgArticleService());
        }
        return serviceController.getOgArticleService();
    }
    // Injects a OgArticleService instance.
	public static void setOgArticleService(OgArticleService ogArticleService) 
    {
        serviceController.setOgArticleService(ogArticleService);
    }

    // Returns a OgBookService instance.
	public static OgBookService getOgBookService() 
    {
        if(serviceController.getOgBookService() == null) {
            serviceController.setOgBookService(serviceFactory.getOgBookService());
        }
        return serviceController.getOgBookService();
    }
    // Injects a OgBookService instance.
	public static void setOgBookService(OgBookService ogBookService) 
    {
        serviceController.setOgBookService(ogBookService);
    }

    // Returns a OgVideoService instance.
	public static OgVideoService getOgVideoService() 
    {
        if(serviceController.getOgVideoService() == null) {
            serviceController.setOgVideoService(serviceFactory.getOgVideoService());
        }
        return serviceController.getOgVideoService();
    }
    // Injects a OgVideoService instance.
	public static void setOgVideoService(OgVideoService ogVideoService) 
    {
        serviceController.setOgVideoService(ogVideoService);
    }

    // Returns a OgMovieService instance.
	public static OgMovieService getOgMovieService() 
    {
        if(serviceController.getOgMovieService() == null) {
            serviceController.setOgMovieService(serviceFactory.getOgMovieService());
        }
        return serviceController.getOgMovieService();
    }
    // Injects a OgMovieService instance.
	public static void setOgMovieService(OgMovieService ogMovieService) 
    {
        serviceController.setOgMovieService(ogMovieService);
    }

    // Returns a OgTvShowService instance.
	public static OgTvShowService getOgTvShowService() 
    {
        if(serviceController.getOgTvShowService() == null) {
            serviceController.setOgTvShowService(serviceFactory.getOgTvShowService());
        }
        return serviceController.getOgTvShowService();
    }
    // Injects a OgTvShowService instance.
	public static void setOgTvShowService(OgTvShowService ogTvShowService) 
    {
        serviceController.setOgTvShowService(ogTvShowService);
    }

    // Returns a OgTvEpisodeService instance.
	public static OgTvEpisodeService getOgTvEpisodeService() 
    {
        if(serviceController.getOgTvEpisodeService() == null) {
            serviceController.setOgTvEpisodeService(serviceFactory.getOgTvEpisodeService());
        }
        return serviceController.getOgTvEpisodeService();
    }
    // Injects a OgTvEpisodeService instance.
	public static void setOgTvEpisodeService(OgTvEpisodeService ogTvEpisodeService) 
    {
        serviceController.setOgTvEpisodeService(ogTvEpisodeService);
    }

    // Returns a TwitterSummaryCardService instance.
	public static TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        if(serviceController.getTwitterSummaryCardService() == null) {
            serviceController.setTwitterSummaryCardService(serviceFactory.getTwitterSummaryCardService());
        }
        return serviceController.getTwitterSummaryCardService();
    }
    // Injects a TwitterSummaryCardService instance.
	public static void setTwitterSummaryCardService(TwitterSummaryCardService twitterSummaryCardService) 
    {
        serviceController.setTwitterSummaryCardService(twitterSummaryCardService);
    }

    // Returns a TwitterPhotoCardService instance.
	public static TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        if(serviceController.getTwitterPhotoCardService() == null) {
            serviceController.setTwitterPhotoCardService(serviceFactory.getTwitterPhotoCardService());
        }
        return serviceController.getTwitterPhotoCardService();
    }
    // Injects a TwitterPhotoCardService instance.
	public static void setTwitterPhotoCardService(TwitterPhotoCardService twitterPhotoCardService) 
    {
        serviceController.setTwitterPhotoCardService(twitterPhotoCardService);
    }

    // Returns a TwitterGalleryCardService instance.
	public static TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        if(serviceController.getTwitterGalleryCardService() == null) {
            serviceController.setTwitterGalleryCardService(serviceFactory.getTwitterGalleryCardService());
        }
        return serviceController.getTwitterGalleryCardService();
    }
    // Injects a TwitterGalleryCardService instance.
	public static void setTwitterGalleryCardService(TwitterGalleryCardService twitterGalleryCardService) 
    {
        serviceController.setTwitterGalleryCardService(twitterGalleryCardService);
    }

    // Returns a TwitterAppCardService instance.
	public static TwitterAppCardService getTwitterAppCardService() 
    {
        if(serviceController.getTwitterAppCardService() == null) {
            serviceController.setTwitterAppCardService(serviceFactory.getTwitterAppCardService());
        }
        return serviceController.getTwitterAppCardService();
    }
    // Injects a TwitterAppCardService instance.
	public static void setTwitterAppCardService(TwitterAppCardService twitterAppCardService) 
    {
        serviceController.setTwitterAppCardService(twitterAppCardService);
    }

    // Returns a TwitterPlayerCardService instance.
	public static TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        if(serviceController.getTwitterPlayerCardService() == null) {
            serviceController.setTwitterPlayerCardService(serviceFactory.getTwitterPlayerCardService());
        }
        return serviceController.getTwitterPlayerCardService();
    }
    // Injects a TwitterPlayerCardService instance.
	public static void setTwitterPlayerCardService(TwitterPlayerCardService twitterPlayerCardService) 
    {
        serviceController.setTwitterPlayerCardService(twitterPlayerCardService);
    }

    // Returns a TwitterProductCardService instance.
	public static TwitterProductCardService getTwitterProductCardService() 
    {
        if(serviceController.getTwitterProductCardService() == null) {
            serviceController.setTwitterProductCardService(serviceFactory.getTwitterProductCardService());
        }
        return serviceController.getTwitterProductCardService();
    }
    // Injects a TwitterProductCardService instance.
	public static void setTwitterProductCardService(TwitterProductCardService twitterProductCardService) 
    {
        serviceController.setTwitterProductCardService(twitterProductCardService);
    }

    // Returns a FetchRequestService instance.
	public static FetchRequestService getFetchRequestService() 
    {
        if(serviceController.getFetchRequestService() == null) {
            serviceController.setFetchRequestService(serviceFactory.getFetchRequestService());
        }
        return serviceController.getFetchRequestService();
    }
    // Injects a FetchRequestService instance.
	public static void setFetchRequestService(FetchRequestService fetchRequestService) 
    {
        serviceController.setFetchRequestService(fetchRequestService);
    }

    // Returns a PageInfoService instance.
	public static PageInfoService getPageInfoService() 
    {
        if(serviceController.getPageInfoService() == null) {
            serviceController.setPageInfoService(serviceFactory.getPageInfoService());
        }
        return serviceController.getPageInfoService();
    }
    // Injects a PageInfoService instance.
	public static void setPageInfoService(PageInfoService pageInfoService) 
    {
        serviceController.setPageInfoService(pageInfoService);
    }

    // Returns a PageFetchService instance.
	public static PageFetchService getPageFetchService() 
    {
        if(serviceController.getPageFetchService() == null) {
            serviceController.setPageFetchService(serviceFactory.getPageFetchService());
        }
        return serviceController.getPageFetchService();
    }
    // Injects a PageFetchService instance.
	public static void setPageFetchService(PageFetchService pageFetchService) 
    {
        serviceController.setPageFetchService(pageFetchService);
    }

    // Returns a LinkListService instance.
	public static LinkListService getLinkListService() 
    {
        if(serviceController.getLinkListService() == null) {
            serviceController.setLinkListService(serviceFactory.getLinkListService());
        }
        return serviceController.getLinkListService();
    }
    // Injects a LinkListService instance.
	public static void setLinkListService(LinkListService linkListService) 
    {
        serviceController.setLinkListService(linkListService);
    }

    // Returns a ImageSetService instance.
	public static ImageSetService getImageSetService() 
    {
        if(serviceController.getImageSetService() == null) {
            serviceController.setImageSetService(serviceFactory.getImageSetService());
        }
        return serviceController.getImageSetService();
    }
    // Injects a ImageSetService instance.
	public static void setImageSetService(ImageSetService imageSetService) 
    {
        serviceController.setImageSetService(imageSetService);
    }

    // Returns a AudioSetService instance.
	public static AudioSetService getAudioSetService() 
    {
        if(serviceController.getAudioSetService() == null) {
            serviceController.setAudioSetService(serviceFactory.getAudioSetService());
        }
        return serviceController.getAudioSetService();
    }
    // Injects a AudioSetService instance.
	public static void setAudioSetService(AudioSetService audioSetService) 
    {
        serviceController.setAudioSetService(audioSetService);
    }

    // Returns a VideoSetService instance.
	public static VideoSetService getVideoSetService() 
    {
        if(serviceController.getVideoSetService() == null) {
            serviceController.setVideoSetService(serviceFactory.getVideoSetService());
        }
        return serviceController.getVideoSetService();
    }
    // Injects a VideoSetService instance.
	public static void setVideoSetService(VideoSetService videoSetService) 
    {
        serviceController.setVideoSetService(videoSetService);
    }

    // Returns a OpenGraphMetaService instance.
	public static OpenGraphMetaService getOpenGraphMetaService() 
    {
        if(serviceController.getOpenGraphMetaService() == null) {
            serviceController.setOpenGraphMetaService(serviceFactory.getOpenGraphMetaService());
        }
        return serviceController.getOpenGraphMetaService();
    }
    // Injects a OpenGraphMetaService instance.
	public static void setOpenGraphMetaService(OpenGraphMetaService openGraphMetaService) 
    {
        serviceController.setOpenGraphMetaService(openGraphMetaService);
    }

    // Returns a TwitterCardMetaService instance.
	public static TwitterCardMetaService getTwitterCardMetaService() 
    {
        if(serviceController.getTwitterCardMetaService() == null) {
            serviceController.setTwitterCardMetaService(serviceFactory.getTwitterCardMetaService());
        }
        return serviceController.getTwitterCardMetaService();
    }
    // Injects a TwitterCardMetaService instance.
	public static void setTwitterCardMetaService(TwitterCardMetaService twitterCardMetaService) 
    {
        serviceController.setTwitterCardMetaService(twitterCardMetaService);
    }

    // Returns a DomainInfoService instance.
	public static DomainInfoService getDomainInfoService() 
    {
        if(serviceController.getDomainInfoService() == null) {
            serviceController.setDomainInfoService(serviceFactory.getDomainInfoService());
        }
        return serviceController.getDomainInfoService();
    }
    // Injects a DomainInfoService instance.
	public static void setDomainInfoService(DomainInfoService domainInfoService) 
    {
        serviceController.setDomainInfoService(domainInfoService);
    }

    // Returns a UrlRatingService instance.
	public static UrlRatingService getUrlRatingService() 
    {
        if(serviceController.getUrlRatingService() == null) {
            serviceController.setUrlRatingService(serviceFactory.getUrlRatingService());
        }
        return serviceController.getUrlRatingService();
    }
    // Injects a UrlRatingService instance.
	public static void setUrlRatingService(UrlRatingService urlRatingService) 
    {
        serviceController.setUrlRatingService(urlRatingService);
    }

    // Returns a ServiceInfoService instance.
	public static ServiceInfoService getServiceInfoService() 
    {
        if(serviceController.getServiceInfoService() == null) {
            serviceController.setServiceInfoService(serviceFactory.getServiceInfoService());
        }
        return serviceController.getServiceInfoService();
    }
    // Injects a ServiceInfoService instance.
	public static void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        serviceController.setServiceInfoService(serviceInfoService);
    }

    // Returns a FiveTenService instance.
	public static FiveTenService getFiveTenService() 
    {
        if(serviceController.getFiveTenService() == null) {
            serviceController.setFiveTenService(serviceFactory.getFiveTenService());
        }
        return serviceController.getFiveTenService();
    }
    // Injects a FiveTenService instance.
	public static void setFiveTenService(FiveTenService fiveTenService) 
    {
        serviceController.setFiveTenService(fiveTenService);
    }

}
