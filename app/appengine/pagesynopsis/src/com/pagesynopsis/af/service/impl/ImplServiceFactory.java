package com.pagesynopsis.af.service.impl;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.AbstractServiceFactory;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;

public class ImplServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ImplServiceFactory.class.getName());

    private ImplServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ImplServiceFactoryHolder
    {
        private static final ImplServiceFactory INSTANCE = new ImplServiceFactory();
    }

    // Singleton method
    public static ImplServiceFactory getInstance()
    {
        return ImplServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerServiceImpl();
    }

    @Override
    public UserService getUserService()
    {
        return new UserServiceImpl();
    }

    @Override
    public RobotsTextFileService getRobotsTextFileService()
    {
        return new RobotsTextFileServiceImpl();
    }

    @Override
    public RobotsTextRefreshService getRobotsTextRefreshService()
    {
        return new RobotsTextRefreshServiceImpl();
    }

    @Override
    public OgProfileService getOgProfileService()
    {
        return new OgProfileServiceImpl();
    }

    @Override
    public OgWebsiteService getOgWebsiteService()
    {
        return new OgWebsiteServiceImpl();
    }

    @Override
    public OgBlogService getOgBlogService()
    {
        return new OgBlogServiceImpl();
    }

    @Override
    public OgArticleService getOgArticleService()
    {
        return new OgArticleServiceImpl();
    }

    @Override
    public OgBookService getOgBookService()
    {
        return new OgBookServiceImpl();
    }

    @Override
    public OgVideoService getOgVideoService()
    {
        return new OgVideoServiceImpl();
    }

    @Override
    public OgMovieService getOgMovieService()
    {
        return new OgMovieServiceImpl();
    }

    @Override
    public OgTvShowService getOgTvShowService()
    {
        return new OgTvShowServiceImpl();
    }

    @Override
    public OgTvEpisodeService getOgTvEpisodeService()
    {
        return new OgTvEpisodeServiceImpl();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardServiceImpl();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardServiceImpl();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardServiceImpl();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardServiceImpl();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardServiceImpl();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardServiceImpl();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestServiceImpl();
    }

    @Override
    public PageInfoService getPageInfoService()
    {
        return new PageInfoServiceImpl();
    }

    @Override
    public PageFetchService getPageFetchService()
    {
        return new PageFetchServiceImpl();
    }

    @Override
    public LinkListService getLinkListService()
    {
        return new LinkListServiceImpl();
    }

    @Override
    public ImageSetService getImageSetService()
    {
        return new ImageSetServiceImpl();
    }

    @Override
    public AudioSetService getAudioSetService()
    {
        return new AudioSetServiceImpl();
    }

    @Override
    public VideoSetService getVideoSetService()
    {
        return new VideoSetServiceImpl();
    }

    @Override
    public OpenGraphMetaService getOpenGraphMetaService()
    {
        return new OpenGraphMetaServiceImpl();
    }

    @Override
    public TwitterCardMetaService getTwitterCardMetaService()
    {
        return new TwitterCardMetaServiceImpl();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoServiceImpl();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingServiceImpl();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoServiceImpl();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenServiceImpl();
    }


}
