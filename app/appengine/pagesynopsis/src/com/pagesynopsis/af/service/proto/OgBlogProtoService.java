package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.impl.OgBlogServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgBlogProtoService extends OgBlogServiceImpl implements OgBlogService
{
    private static final Logger log = Logger.getLogger(OgBlogProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgBlogProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgBlog related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgBlog getOgBlog(String guid) throws BaseException
    {
        return super.getOgBlog(guid);
    }

    @Override
    public Object getOgBlog(String guid, String field) throws BaseException
    {
        return super.getOgBlog(guid, field);
    }

    @Override
    public List<OgBlog> getOgBlogs(List<String> guids) throws BaseException
    {
        return super.getOgBlogs(guids);
    }

    @Override
    public List<OgBlog> getAllOgBlogs() throws BaseException
    {
        return super.getAllOgBlogs();
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogs(ordering, offset, count, null);
    }

    @Override
    public List<OgBlog> getAllOgBlogs(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgBlogs(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgBlogKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgBlogKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllOgBlogKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgBlog> findOgBlogs(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgBlogs(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgBlogKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findOgBlogKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgBlog(OgBlog ogBlog) throws BaseException
    {
        return super.createOgBlog(ogBlog);
    }

    @Override
    public OgBlog constructOgBlog(OgBlog ogBlog) throws BaseException
    {
        return super.constructOgBlog(ogBlog);
    }


    @Override
    public Boolean updateOgBlog(OgBlog ogBlog) throws BaseException
    {
        return super.updateOgBlog(ogBlog);
    }
        
    @Override
    public OgBlog refreshOgBlog(OgBlog ogBlog) throws BaseException
    {
        return super.refreshOgBlog(ogBlog);
    }

    @Override
    public Boolean deleteOgBlog(String guid) throws BaseException
    {
        return super.deleteOgBlog(guid);
    }

    @Override
    public Boolean deleteOgBlog(OgBlog ogBlog) throws BaseException
    {
        return super.deleteOgBlog(ogBlog);
    }

    @Override
    public Integer createOgBlogs(List<OgBlog> ogBlogs) throws BaseException
    {
        return super.createOgBlogs(ogBlogs);
    }

    // TBD
    //@Override
    //public Boolean updateOgBlogs(List<OgBlog> ogBlogs) throws BaseException
    //{
    //}

}
