package com.pagesynopsis.af.service.proto;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.AbstractServiceFactory;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;

public class ProtoServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(ProtoServiceFactory.class.getName());

    private ProtoServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ProtoServiceFactoryHolder
    {
        private static final ProtoServiceFactory INSTANCE = new ProtoServiceFactory();
    }

    // Singleton method
    public static ProtoServiceFactory getInstance()
    {
        return ProtoServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerProtoService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserProtoService();
    }

    @Override
    public RobotsTextFileService getRobotsTextFileService()
    {
        return new RobotsTextFileProtoService();
    }

    @Override
    public RobotsTextRefreshService getRobotsTextRefreshService()
    {
        return new RobotsTextRefreshProtoService();
    }

    @Override
    public OgProfileService getOgProfileService()
    {
        return new OgProfileProtoService();
    }

    @Override
    public OgWebsiteService getOgWebsiteService()
    {
        return new OgWebsiteProtoService();
    }

    @Override
    public OgBlogService getOgBlogService()
    {
        return new OgBlogProtoService();
    }

    @Override
    public OgArticleService getOgArticleService()
    {
        return new OgArticleProtoService();
    }

    @Override
    public OgBookService getOgBookService()
    {
        return new OgBookProtoService();
    }

    @Override
    public OgVideoService getOgVideoService()
    {
        return new OgVideoProtoService();
    }

    @Override
    public OgMovieService getOgMovieService()
    {
        return new OgMovieProtoService();
    }

    @Override
    public OgTvShowService getOgTvShowService()
    {
        return new OgTvShowProtoService();
    }

    @Override
    public OgTvEpisodeService getOgTvEpisodeService()
    {
        return new OgTvEpisodeProtoService();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardProtoService();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardProtoService();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardProtoService();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardProtoService();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardProtoService();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardProtoService();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestProtoService();
    }

    @Override
    public PageInfoService getPageInfoService()
    {
        return new PageInfoProtoService();
    }

    @Override
    public PageFetchService getPageFetchService()
    {
        return new PageFetchProtoService();
    }

    @Override
    public LinkListService getLinkListService()
    {
        return new LinkListProtoService();
    }

    @Override
    public ImageSetService getImageSetService()
    {
        return new ImageSetProtoService();
    }

    @Override
    public AudioSetService getAudioSetService()
    {
        return new AudioSetProtoService();
    }

    @Override
    public VideoSetService getVideoSetService()
    {
        return new VideoSetProtoService();
    }

    @Override
    public OpenGraphMetaService getOpenGraphMetaService()
    {
        return new OpenGraphMetaProtoService();
    }

    @Override
    public TwitterCardMetaService getTwitterCardMetaService()
    {
        return new TwitterCardMetaProtoService();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoProtoService();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingProtoService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoProtoService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenProtoService();
    }


}
