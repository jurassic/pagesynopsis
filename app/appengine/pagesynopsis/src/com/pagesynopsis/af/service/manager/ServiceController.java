package com.pagesynopsis.af.service.manager;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;

// TBD: DI
// (For now, this class is used to make ServiceManager "DI-capable".)
public class ServiceController
{
    private static final Logger log = Logger.getLogger(ServiceController.class.getName());

    // "Injectable" services.
    private ApiConsumerService apiConsumerService = null;
    private UserService userService = null;
    private RobotsTextFileService robotsTextFileService = null;
    private RobotsTextRefreshService robotsTextRefreshService = null;
    private OgProfileService ogProfileService = null;
    private OgWebsiteService ogWebsiteService = null;
    private OgBlogService ogBlogService = null;
    private OgArticleService ogArticleService = null;
    private OgBookService ogBookService = null;
    private OgVideoService ogVideoService = null;
    private OgMovieService ogMovieService = null;
    private OgTvShowService ogTvShowService = null;
    private OgTvEpisodeService ogTvEpisodeService = null;
    private TwitterSummaryCardService twitterSummaryCardService = null;
    private TwitterPhotoCardService twitterPhotoCardService = null;
    private TwitterGalleryCardService twitterGalleryCardService = null;
    private TwitterAppCardService twitterAppCardService = null;
    private TwitterPlayerCardService twitterPlayerCardService = null;
    private TwitterProductCardService twitterProductCardService = null;
    private FetchRequestService fetchRequestService = null;
    private PageInfoService pageInfoService = null;
    private PageFetchService pageFetchService = null;
    private LinkListService linkListService = null;
    private ImageSetService imageSetService = null;
    private AudioSetService audioSetService = null;
    private VideoSetService videoSetService = null;
    private OpenGraphMetaService openGraphMetaService = null;
    private TwitterCardMetaService twitterCardMetaService = null;
    private DomainInfoService domainInfoService = null;
    private UrlRatingService urlRatingService = null;
    private ServiceInfoService serviceInfoService = null;
    private FiveTenService fiveTenService = null;

    // Ctor injection.
    public ServiceController(ApiConsumerService apiConsumerService, UserService userService, RobotsTextFileService robotsTextFileService, RobotsTextRefreshService robotsTextRefreshService, OgProfileService ogProfileService, OgWebsiteService ogWebsiteService, OgBlogService ogBlogService, OgArticleService ogArticleService, OgBookService ogBookService, OgVideoService ogVideoService, OgMovieService ogMovieService, OgTvShowService ogTvShowService, OgTvEpisodeService ogTvEpisodeService, TwitterSummaryCardService twitterSummaryCardService, TwitterPhotoCardService twitterPhotoCardService, TwitterGalleryCardService twitterGalleryCardService, TwitterAppCardService twitterAppCardService, TwitterPlayerCardService twitterPlayerCardService, TwitterProductCardService twitterProductCardService, FetchRequestService fetchRequestService, PageInfoService pageInfoService, PageFetchService pageFetchService, LinkListService linkListService, ImageSetService imageSetService, AudioSetService audioSetService, VideoSetService videoSetService, OpenGraphMetaService openGraphMetaService, TwitterCardMetaService twitterCardMetaService, DomainInfoService domainInfoService, UrlRatingService urlRatingService, ServiceInfoService serviceInfoService, FiveTenService fiveTenService)
    {
        this.apiConsumerService = apiConsumerService;
        this.userService = userService;
        this.robotsTextFileService = robotsTextFileService;
        this.robotsTextRefreshService = robotsTextRefreshService;
        this.ogProfileService = ogProfileService;
        this.ogWebsiteService = ogWebsiteService;
        this.ogBlogService = ogBlogService;
        this.ogArticleService = ogArticleService;
        this.ogBookService = ogBookService;
        this.ogVideoService = ogVideoService;
        this.ogMovieService = ogMovieService;
        this.ogTvShowService = ogTvShowService;
        this.ogTvEpisodeService = ogTvEpisodeService;
        this.twitterSummaryCardService = twitterSummaryCardService;
        this.twitterPhotoCardService = twitterPhotoCardService;
        this.twitterGalleryCardService = twitterGalleryCardService;
        this.twitterAppCardService = twitterAppCardService;
        this.twitterPlayerCardService = twitterPlayerCardService;
        this.twitterProductCardService = twitterProductCardService;
        this.fetchRequestService = fetchRequestService;
        this.pageInfoService = pageInfoService;
        this.pageFetchService = pageFetchService;
        this.linkListService = linkListService;
        this.imageSetService = imageSetService;
        this.audioSetService = audioSetService;
        this.videoSetService = videoSetService;
        this.openGraphMetaService = openGraphMetaService;
        this.twitterCardMetaService = twitterCardMetaService;
        this.domainInfoService = domainInfoService;
        this.urlRatingService = urlRatingService;
        this.serviceInfoService = serviceInfoService;
        this.fiveTenService = fiveTenService;
    }

    // Returns a ApiConsumerService instance.
	public ApiConsumerService getApiConsumerService() 
    {
        return this.apiConsumerService;
    }
    // Setter injection for ApiConsumerService.
	public void setApiConsumerService(ApiConsumerService apiConsumerService) 
    {
        this.apiConsumerService = apiConsumerService;
    }

    // Returns a UserService instance.
	public UserService getUserService() 
    {
        return this.userService;
    }
    // Setter injection for UserService.
	public void setUserService(UserService userService) 
    {
        this.userService = userService;
    }

    // Returns a RobotsTextFileService instance.
	public RobotsTextFileService getRobotsTextFileService() 
    {
        return this.robotsTextFileService;
    }
    // Setter injection for RobotsTextFileService.
	public void setRobotsTextFileService(RobotsTextFileService robotsTextFileService) 
    {
        this.robotsTextFileService = robotsTextFileService;
    }

    // Returns a RobotsTextRefreshService instance.
	public RobotsTextRefreshService getRobotsTextRefreshService() 
    {
        return this.robotsTextRefreshService;
    }
    // Setter injection for RobotsTextRefreshService.
	public void setRobotsTextRefreshService(RobotsTextRefreshService robotsTextRefreshService) 
    {
        this.robotsTextRefreshService = robotsTextRefreshService;
    }

    // Returns a OgProfileService instance.
	public OgProfileService getOgProfileService() 
    {
        return this.ogProfileService;
    }
    // Setter injection for OgProfileService.
	public void setOgProfileService(OgProfileService ogProfileService) 
    {
        this.ogProfileService = ogProfileService;
    }

    // Returns a OgWebsiteService instance.
	public OgWebsiteService getOgWebsiteService() 
    {
        return this.ogWebsiteService;
    }
    // Setter injection for OgWebsiteService.
	public void setOgWebsiteService(OgWebsiteService ogWebsiteService) 
    {
        this.ogWebsiteService = ogWebsiteService;
    }

    // Returns a OgBlogService instance.
	public OgBlogService getOgBlogService() 
    {
        return this.ogBlogService;
    }
    // Setter injection for OgBlogService.
	public void setOgBlogService(OgBlogService ogBlogService) 
    {
        this.ogBlogService = ogBlogService;
    }

    // Returns a OgArticleService instance.
	public OgArticleService getOgArticleService() 
    {
        return this.ogArticleService;
    }
    // Setter injection for OgArticleService.
	public void setOgArticleService(OgArticleService ogArticleService) 
    {
        this.ogArticleService = ogArticleService;
    }

    // Returns a OgBookService instance.
	public OgBookService getOgBookService() 
    {
        return this.ogBookService;
    }
    // Setter injection for OgBookService.
	public void setOgBookService(OgBookService ogBookService) 
    {
        this.ogBookService = ogBookService;
    }

    // Returns a OgVideoService instance.
	public OgVideoService getOgVideoService() 
    {
        return this.ogVideoService;
    }
    // Setter injection for OgVideoService.
	public void setOgVideoService(OgVideoService ogVideoService) 
    {
        this.ogVideoService = ogVideoService;
    }

    // Returns a OgMovieService instance.
	public OgMovieService getOgMovieService() 
    {
        return this.ogMovieService;
    }
    // Setter injection for OgMovieService.
	public void setOgMovieService(OgMovieService ogMovieService) 
    {
        this.ogMovieService = ogMovieService;
    }

    // Returns a OgTvShowService instance.
	public OgTvShowService getOgTvShowService() 
    {
        return this.ogTvShowService;
    }
    // Setter injection for OgTvShowService.
	public void setOgTvShowService(OgTvShowService ogTvShowService) 
    {
        this.ogTvShowService = ogTvShowService;
    }

    // Returns a OgTvEpisodeService instance.
	public OgTvEpisodeService getOgTvEpisodeService() 
    {
        return this.ogTvEpisodeService;
    }
    // Setter injection for OgTvEpisodeService.
	public void setOgTvEpisodeService(OgTvEpisodeService ogTvEpisodeService) 
    {
        this.ogTvEpisodeService = ogTvEpisodeService;
    }

    // Returns a TwitterSummaryCardService instance.
	public TwitterSummaryCardService getTwitterSummaryCardService() 
    {
        return this.twitterSummaryCardService;
    }
    // Setter injection for TwitterSummaryCardService.
	public void setTwitterSummaryCardService(TwitterSummaryCardService twitterSummaryCardService) 
    {
        this.twitterSummaryCardService = twitterSummaryCardService;
    }

    // Returns a TwitterPhotoCardService instance.
	public TwitterPhotoCardService getTwitterPhotoCardService() 
    {
        return this.twitterPhotoCardService;
    }
    // Setter injection for TwitterPhotoCardService.
	public void setTwitterPhotoCardService(TwitterPhotoCardService twitterPhotoCardService) 
    {
        this.twitterPhotoCardService = twitterPhotoCardService;
    }

    // Returns a TwitterGalleryCardService instance.
	public TwitterGalleryCardService getTwitterGalleryCardService() 
    {
        return this.twitterGalleryCardService;
    }
    // Setter injection for TwitterGalleryCardService.
	public void setTwitterGalleryCardService(TwitterGalleryCardService twitterGalleryCardService) 
    {
        this.twitterGalleryCardService = twitterGalleryCardService;
    }

    // Returns a TwitterAppCardService instance.
	public TwitterAppCardService getTwitterAppCardService() 
    {
        return this.twitterAppCardService;
    }
    // Setter injection for TwitterAppCardService.
	public void setTwitterAppCardService(TwitterAppCardService twitterAppCardService) 
    {
        this.twitterAppCardService = twitterAppCardService;
    }

    // Returns a TwitterPlayerCardService instance.
	public TwitterPlayerCardService getTwitterPlayerCardService() 
    {
        return this.twitterPlayerCardService;
    }
    // Setter injection for TwitterPlayerCardService.
	public void setTwitterPlayerCardService(TwitterPlayerCardService twitterPlayerCardService) 
    {
        this.twitterPlayerCardService = twitterPlayerCardService;
    }

    // Returns a TwitterProductCardService instance.
	public TwitterProductCardService getTwitterProductCardService() 
    {
        return this.twitterProductCardService;
    }
    // Setter injection for TwitterProductCardService.
	public void setTwitterProductCardService(TwitterProductCardService twitterProductCardService) 
    {
        this.twitterProductCardService = twitterProductCardService;
    }

    // Returns a FetchRequestService instance.
	public FetchRequestService getFetchRequestService() 
    {
        return this.fetchRequestService;
    }
    // Setter injection for FetchRequestService.
	public void setFetchRequestService(FetchRequestService fetchRequestService) 
    {
        this.fetchRequestService = fetchRequestService;
    }

    // Returns a PageInfoService instance.
	public PageInfoService getPageInfoService() 
    {
        return this.pageInfoService;
    }
    // Setter injection for PageInfoService.
	public void setPageInfoService(PageInfoService pageInfoService) 
    {
        this.pageInfoService = pageInfoService;
    }

    // Returns a PageFetchService instance.
	public PageFetchService getPageFetchService() 
    {
        return this.pageFetchService;
    }
    // Setter injection for PageFetchService.
	public void setPageFetchService(PageFetchService pageFetchService) 
    {
        this.pageFetchService = pageFetchService;
    }

    // Returns a LinkListService instance.
	public LinkListService getLinkListService() 
    {
        return this.linkListService;
    }
    // Setter injection for LinkListService.
	public void setLinkListService(LinkListService linkListService) 
    {
        this.linkListService = linkListService;
    }

    // Returns a ImageSetService instance.
	public ImageSetService getImageSetService() 
    {
        return this.imageSetService;
    }
    // Setter injection for ImageSetService.
	public void setImageSetService(ImageSetService imageSetService) 
    {
        this.imageSetService = imageSetService;
    }

    // Returns a AudioSetService instance.
	public AudioSetService getAudioSetService() 
    {
        return this.audioSetService;
    }
    // Setter injection for AudioSetService.
	public void setAudioSetService(AudioSetService audioSetService) 
    {
        this.audioSetService = audioSetService;
    }

    // Returns a VideoSetService instance.
	public VideoSetService getVideoSetService() 
    {
        return this.videoSetService;
    }
    // Setter injection for VideoSetService.
	public void setVideoSetService(VideoSetService videoSetService) 
    {
        this.videoSetService = videoSetService;
    }

    // Returns a OpenGraphMetaService instance.
	public OpenGraphMetaService getOpenGraphMetaService() 
    {
        return this.openGraphMetaService;
    }
    // Setter injection for OpenGraphMetaService.
	public void setOpenGraphMetaService(OpenGraphMetaService openGraphMetaService) 
    {
        this.openGraphMetaService = openGraphMetaService;
    }

    // Returns a TwitterCardMetaService instance.
	public TwitterCardMetaService getTwitterCardMetaService() 
    {
        return this.twitterCardMetaService;
    }
    // Setter injection for TwitterCardMetaService.
	public void setTwitterCardMetaService(TwitterCardMetaService twitterCardMetaService) 
    {
        this.twitterCardMetaService = twitterCardMetaService;
    }

    // Returns a DomainInfoService instance.
	public DomainInfoService getDomainInfoService() 
    {
        return this.domainInfoService;
    }
    // Setter injection for DomainInfoService.
	public void setDomainInfoService(DomainInfoService domainInfoService) 
    {
        this.domainInfoService = domainInfoService;
    }

    // Returns a UrlRatingService instance.
	public UrlRatingService getUrlRatingService() 
    {
        return this.urlRatingService;
    }
    // Setter injection for UrlRatingService.
	public void setUrlRatingService(UrlRatingService urlRatingService) 
    {
        this.urlRatingService = urlRatingService;
    }

    // Returns a ServiceInfoService instance.
	public ServiceInfoService getServiceInfoService() 
    {
        return this.serviceInfoService;
    }
    // Setter injection for ServiceInfoService.
	public void setServiceInfoService(ServiceInfoService serviceInfoService) 
    {
        this.serviceInfoService = serviceInfoService;
    }

    // Returns a FiveTenService instance.
	public FiveTenService getFiveTenService() 
    {
        return this.fiveTenService;
    }
    // Setter injection for FiveTenService.
	public void setFiveTenService(FiveTenService fiveTenService) 
    {
        this.fiveTenService = fiveTenService;
    }

}
