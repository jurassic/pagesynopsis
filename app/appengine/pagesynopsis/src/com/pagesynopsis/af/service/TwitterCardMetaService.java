package com.pagesynopsis.af.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface TwitterCardMetaService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException;
    Object getTwitterCardMeta(String guid, String field) throws BaseException;
    List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException;
    List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException;
    /* @Deprecated */ List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException;
    List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException;
    //String createTwitterCardMeta(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return TwitterCardMeta?)
    String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException;
    TwitterCardMeta constructTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException;
    Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException;
    //Boolean updateTwitterCardMeta(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException;
    TwitterCardMeta refreshTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException;
    Boolean deleteTwitterCardMeta(String guid) throws BaseException;
    Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException;
    Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException;
//    Boolean updateTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException;

}
