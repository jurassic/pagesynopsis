package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.PageFetch;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.PageFetchBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.PageFetchService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class PageFetchServiceImpl implements PageFetchService
{
    private static final Logger log = Logger.getLogger(PageFetchServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "PageFetch-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("PageFetch:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public PageFetchServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // PageFetch related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public PageFetch getPageFetch(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getPageFetch(): guid = " + guid);

        PageFetchBean bean = null;
        if(getCache() != null) {
            bean = (PageFetchBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (PageFetchBean) getProxyFactory().getPageFetchServiceProxy().getPageFetch(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "PageFetchBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve PageFetchBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getPageFetch(String guid, String field) throws BaseException
    {
        PageFetchBean bean = null;
        if(getCache() != null) {
            bean = (PageFetchBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (PageFetchBean) getProxyFactory().getPageFetchServiceProxy().getPageFetch(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "PageFetchBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve PageFetchBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return bean.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return bean.getPageUrl();
        } else if(field.equals("queryString")) {
            return bean.getQueryString();
        } else if(field.equals("queryParams")) {
            return bean.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return bean.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("contentType")) {
            return bean.getContentType();
        } else if(field.equals("contentLength")) {
            return bean.getContentLength();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("redirect")) {
            return bean.getRedirect();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("pageTitle")) {
            return bean.getPageTitle();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("deferred")) {
            return bean.isDeferred();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("refreshStatus")) {
            return bean.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return bean.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return bean.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("inputMaxRedirects")) {
            return bean.getInputMaxRedirects();
        } else if(field.equals("resultRedirectCount")) {
            return bean.getResultRedirectCount();
        } else if(field.equals("redirectPages")) {
            return bean.getRedirectPages();
        } else if(field.equals("destinationUrl")) {
            return bean.getDestinationUrl();
        } else if(field.equals("pageAuthor")) {
            return bean.getPageAuthor();
        } else if(field.equals("pageSummary")) {
            return bean.getPageSummary();
        } else if(field.equals("favicon")) {
            return bean.getFavicon();
        } else if(field.equals("faviconUrl")) {
            return bean.getFaviconUrl();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<PageFetch> getPageFetches(List<String> guids) throws BaseException
    {
        log.fine("getPageFetches()");

        // TBD: Is there a better way????
        List<PageFetch> pageFetches = getProxyFactory().getPageFetchServiceProxy().getPageFetches(guids);
        if(pageFetches == null) {
            log.log(Level.WARNING, "Failed to retrieve PageFetchBean list.");
        }

        log.finer("END");
        return pageFetches;
    }

    @Override
    public List<PageFetch> getAllPageFetches() throws BaseException
    {
        return getAllPageFetches(null, null, null);
    }


    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetches(ordering, offset, count, null);
    }

    @Override
    public List<PageFetch> getAllPageFetches(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageFetches(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<PageFetch> pageFetches = getProxyFactory().getPageFetchServiceProxy().getAllPageFetches(ordering, offset, count, forwardCursor);
        if(pageFetches == null) {
            log.log(Level.WARNING, "Failed to retrieve PageFetchBean list.");
        }

        log.finer("END");
        return pageFetches;
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllPageFetchKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllPageFetchKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllPageFetchKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getPageFetchServiceProxy().getAllPageFetchKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve PageFetchBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty PageFetchBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<PageFetch> findPageFetches(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageFetchServiceImpl.findPageFetches(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<PageFetch> pageFetches = getProxyFactory().getPageFetchServiceProxy().findPageFetches(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(pageFetches == null) {
            log.log(Level.WARNING, "Failed to find pageFetches for the given criterion.");
        }

        log.finer("END");
        return pageFetches;
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findPageFetchKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageFetchServiceImpl.findPageFetchKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getPageFetchServiceProxy().findPageFetchKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find PageFetch keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty PageFetch key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("PageFetchServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getPageFetchServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createPageFetch(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        PageFetchBean bean = new PageFetchBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
        return createPageFetch(bean);
    }

    @Override
    public String createPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //PageFetch bean = constructPageFetch(pageFetch);
        //return bean.getGuid();

        // Param pageFetch cannot be null.....
        if(pageFetch == null) {
            log.log(Level.INFO, "Param pageFetch is null!");
            throw new BadRequestException("Param pageFetch object is null!");
        }
        PageFetchBean bean = null;
        if(pageFetch instanceof PageFetchBean) {
            bean = (PageFetchBean) pageFetch;
        } else if(pageFetch instanceof PageFetch) {
            // bean = new PageFetchBean(null, pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new PageFetchBean(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        } else {
            log.log(Level.WARNING, "createPageFetch(): Arg pageFetch is of an unknown type.");
            //bean = new PageFetchBean();
            bean = new PageFetchBean(pageFetch.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getPageFetchServiceProxy().createPageFetch(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public PageFetch constructPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param pageFetch cannot be null.....
        if(pageFetch == null) {
            log.log(Level.INFO, "Param pageFetch is null!");
            throw new BadRequestException("Param pageFetch object is null!");
        }
        PageFetchBean bean = null;
        if(pageFetch instanceof PageFetchBean) {
            bean = (PageFetchBean) pageFetch;
        } else if(pageFetch instanceof PageFetch) {
            // bean = new PageFetchBean(null, pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new PageFetchBean(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        } else {
            log.log(Level.WARNING, "createPageFetch(): Arg pageFetch is of an unknown type.");
            //bean = new PageFetchBean();
            bean = new PageFetchBean(pageFetch.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getPageFetchServiceProxy().createPageFetch(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updatePageFetch(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, Integer inputMaxRedirects, Integer resultRedirectCount, List<UrlStruct> redirectPages, String destinationUrl, String pageAuthor, String pageSummary, String favicon, String faviconUrl) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        PageFetchBean bean = new PageFetchBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, inputMaxRedirects, resultRedirectCount, redirectPages, destinationUrl, pageAuthor, pageSummary, favicon, faviconUrl);
        return updatePageFetch(bean);
    }
        
    @Override
    public Boolean updatePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //PageFetch bean = refreshPageFetch(pageFetch);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param pageFetch cannot be null.....
        if(pageFetch == null || pageFetch.getGuid() == null) {
            log.log(Level.INFO, "Param pageFetch or its guid is null!");
            throw new BadRequestException("Param pageFetch object or its guid is null!");
        }
        PageFetchBean bean = null;
        if(pageFetch instanceof PageFetchBean) {
            bean = (PageFetchBean) pageFetch;
        } else {  // if(pageFetch instanceof PageFetch)
            bean = new PageFetchBean(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        }
        Boolean suc = getProxyFactory().getPageFetchServiceProxy().updatePageFetch(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public PageFetch refreshPageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param pageFetch cannot be null.....
        if(pageFetch == null || pageFetch.getGuid() == null) {
            log.log(Level.INFO, "Param pageFetch or its guid is null!");
            throw new BadRequestException("Param pageFetch object or its guid is null!");
        }
        PageFetchBean bean = null;
        if(pageFetch instanceof PageFetchBean) {
            bean = (PageFetchBean) pageFetch;
        } else {  // if(pageFetch instanceof PageFetch)
            bean = new PageFetchBean(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        }
        Boolean suc = getProxyFactory().getPageFetchServiceProxy().updatePageFetch(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deletePageFetch(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getPageFetchServiceProxy().deletePageFetch(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            PageFetch pageFetch = null;
            try {
                pageFetch = getProxyFactory().getPageFetchServiceProxy().getPageFetch(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch pageFetch with a key, " + guid);
                return false;
            }
            if(pageFetch != null) {
                String beanGuid = pageFetch.getGuid();
                Boolean suc1 = getProxyFactory().getPageFetchServiceProxy().deletePageFetch(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("pageFetch with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deletePageFetch(PageFetch pageFetch) throws BaseException
    {
        log.finer("BEGIN");

        // Param pageFetch cannot be null.....
        if(pageFetch == null || pageFetch.getGuid() == null) {
            log.log(Level.INFO, "Param pageFetch or its guid is null!");
            throw new BadRequestException("Param pageFetch object or its guid is null!");
        }
        PageFetchBean bean = null;
        if(pageFetch instanceof PageFetchBean) {
            bean = (PageFetchBean) pageFetch;
        } else {  // if(pageFetch instanceof PageFetch)
            // ????
            log.warning("pageFetch is not an instance of PageFetchBean.");
            bean = new PageFetchBean(pageFetch.getGuid(), pageFetch.getUser(), pageFetch.getFetchRequest(), pageFetch.getTargetUrl(), pageFetch.getPageUrl(), pageFetch.getQueryString(), pageFetch.getQueryParams(), pageFetch.getLastFetchResult(), pageFetch.getResponseCode(), pageFetch.getContentType(), pageFetch.getContentLength(), pageFetch.getLanguage(), pageFetch.getRedirect(), pageFetch.getLocation(), pageFetch.getPageTitle(), pageFetch.getNote(), pageFetch.isDeferred(), pageFetch.getStatus(), pageFetch.getRefreshStatus(), pageFetch.getRefreshInterval(), pageFetch.getNextRefreshTime(), pageFetch.getLastCheckedTime(), pageFetch.getLastUpdatedTime(), pageFetch.getInputMaxRedirects(), pageFetch.getResultRedirectCount(), pageFetch.getRedirectPages(), pageFetch.getDestinationUrl(), pageFetch.getPageAuthor(), pageFetch.getPageSummary(), pageFetch.getFavicon(), pageFetch.getFaviconUrl());
        }
        Boolean suc = getProxyFactory().getPageFetchServiceProxy().deletePageFetch(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deletePageFetches(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getPageFetchServiceProxy().deletePageFetches(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createPageFetches(List<PageFetch> pageFetches) throws BaseException
    {
        log.finer("BEGIN");

        if(pageFetches == null) {
            log.log(Level.WARNING, "createPageFetches() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = pageFetches.size();
        if(size == 0) {
            log.log(Level.WARNING, "createPageFetches() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(PageFetch pageFetch : pageFetches) {
            String guid = createPageFetch(pageFetch);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createPageFetches() failed for at least one pageFetch. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updatePageFetches(List<PageFetch> pageFetches) throws BaseException
    //{
    //}

}
