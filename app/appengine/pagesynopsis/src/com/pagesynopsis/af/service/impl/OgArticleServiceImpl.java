package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgArticleService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class OgArticleServiceImpl implements OgArticleService
{
    private static final Logger log = Logger.getLogger(OgArticleServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "OgArticle-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("OgArticle:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public OgArticleServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgArticle related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public OgArticle getOgArticle(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getOgArticle(): guid = " + guid);

        OgArticleBean bean = null;
        if(getCache() != null) {
            bean = (OgArticleBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgArticleBean) getProxyFactory().getOgArticleServiceProxy().getOgArticle(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgArticleBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgArticleBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getOgArticle(String guid, String field) throws BaseException
    {
        OgArticleBean bean = null;
        if(getCache() != null) {
            bean = (OgArticleBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (OgArticleBean) getProxyFactory().getOgArticleServiceProxy().getOgArticle(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "OgArticleBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve OgArticleBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("url")) {
            return bean.getUrl();
        } else if(field.equals("type")) {
            return bean.getType();
        } else if(field.equals("siteName")) {
            return bean.getSiteName();
        } else if(field.equals("title")) {
            return bean.getTitle();
        } else if(field.equals("description")) {
            return bean.getDescription();
        } else if(field.equals("fbAdmins")) {
            return bean.getFbAdmins();
        } else if(field.equals("fbAppId")) {
            return bean.getFbAppId();
        } else if(field.equals("image")) {
            return bean.getImage();
        } else if(field.equals("audio")) {
            return bean.getAudio();
        } else if(field.equals("video")) {
            return bean.getVideo();
        } else if(field.equals("locale")) {
            return bean.getLocale();
        } else if(field.equals("localeAlternate")) {
            return bean.getLocaleAlternate();
        } else if(field.equals("author")) {
            return bean.getAuthor();
        } else if(field.equals("section")) {
            return bean.getSection();
        } else if(field.equals("tag")) {
            return bean.getTag();
        } else if(field.equals("publishedDate")) {
            return bean.getPublishedDate();
        } else if(field.equals("modifiedDate")) {
            return bean.getModifiedDate();
        } else if(field.equals("expirationDate")) {
            return bean.getExpirationDate();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<OgArticle> getOgArticles(List<String> guids) throws BaseException
    {
        log.fine("getOgArticles()");

        // TBD: Is there a better way????
        List<OgArticle> ogArticles = getProxyFactory().getOgArticleServiceProxy().getOgArticles(guids);
        if(ogArticles == null) {
            log.log(Level.WARNING, "Failed to retrieve OgArticleBean list.");
        }

        log.finer("END");
        return ogArticles;
    }

    @Override
    public List<OgArticle> getAllOgArticles() throws BaseException
    {
        return getAllOgArticles(null, null, null);
    }


    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticles(ordering, offset, count, null);
    }

    @Override
    public List<OgArticle> getAllOgArticles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgArticles(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgArticle> ogArticles = getProxyFactory().getOgArticleServiceProxy().getAllOgArticles(ordering, offset, count, forwardCursor);
        if(ogArticles == null) {
            log.log(Level.WARNING, "Failed to retrieve OgArticleBean list.");
        }

        log.finer("END");
        return ogArticles;
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllOgArticleKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllOgArticleKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllOgArticleKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgArticleServiceProxy().getAllOgArticleKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve OgArticleBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgArticleBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<OgArticle> findOgArticles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgArticleServiceImpl.findOgArticles(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<OgArticle> ogArticles = getProxyFactory().getOgArticleServiceProxy().findOgArticles(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(ogArticles == null) {
            log.log(Level.WARNING, "Failed to find ogArticles for the given criterion.");
        }

        log.finer("END");
        return ogArticles;
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findOgArticleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgArticleServiceImpl.findOgArticleKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getOgArticleServiceProxy().findOgArticleKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find OgArticle keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty OgArticle key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("OgArticleServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getOgArticleServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createOgArticle(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        OgArticleBean bean = new OgArticleBean(null, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        return createOgArticle(bean);
    }

    @Override
    public String createOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgArticle bean = constructOgArticle(ogArticle);
        //return bean.getGuid();

        // Param ogArticle cannot be null.....
        if(ogArticle == null) {
            log.log(Level.INFO, "Param ogArticle is null!");
            throw new BadRequestException("Param ogArticle object is null!");
        }
        OgArticleBean bean = null;
        if(ogArticle instanceof OgArticleBean) {
            bean = (OgArticleBean) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            // bean = new OgArticleBean(null, ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        } else {
            log.log(Level.WARNING, "createOgArticle(): Arg ogArticle is of an unknown type.");
            //bean = new OgArticleBean();
            bean = new OgArticleBean(ogArticle.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgArticleServiceProxy().createOgArticle(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public OgArticle constructOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogArticle cannot be null.....
        if(ogArticle == null) {
            log.log(Level.INFO, "Param ogArticle is null!");
            throw new BadRequestException("Param ogArticle object is null!");
        }
        OgArticleBean bean = null;
        if(ogArticle instanceof OgArticleBean) {
            bean = (OgArticleBean) ogArticle;
        } else if(ogArticle instanceof OgArticle) {
            // bean = new OgArticleBean(null, ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        } else {
            log.log(Level.WARNING, "createOgArticle(): Arg ogArticle is of an unknown type.");
            //bean = new OgArticleBean();
            bean = new OgArticleBean(ogArticle.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getOgArticleServiceProxy().createOgArticle(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateOgArticle(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, List<String> author, String section, List<String> tag, String publishedDate, String modifiedDate, String expirationDate) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        OgArticleBean bean = new OgArticleBean(guid, url, type, siteName, title, description, fbAdmins, fbAppId, image, audio, video, locale, localeAlternate, author, section, tag, publishedDate, modifiedDate, expirationDate);
        return updateOgArticle(bean);
    }
        
    @Override
    public Boolean updateOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //OgArticle bean = refreshOgArticle(ogArticle);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param ogArticle cannot be null.....
        if(ogArticle == null || ogArticle.getGuid() == null) {
            log.log(Level.INFO, "Param ogArticle or its guid is null!");
            throw new BadRequestException("Param ogArticle object or its guid is null!");
        }
        OgArticleBean bean = null;
        if(ogArticle instanceof OgArticleBean) {
            bean = (OgArticleBean) ogArticle;
        } else {  // if(ogArticle instanceof OgArticle)
            bean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        }
        Boolean suc = getProxyFactory().getOgArticleServiceProxy().updateOgArticle(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public OgArticle refreshOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogArticle cannot be null.....
        if(ogArticle == null || ogArticle.getGuid() == null) {
            log.log(Level.INFO, "Param ogArticle or its guid is null!");
            throw new BadRequestException("Param ogArticle object or its guid is null!");
        }
        OgArticleBean bean = null;
        if(ogArticle instanceof OgArticleBean) {
            bean = (OgArticleBean) ogArticle;
        } else {  // if(ogArticle instanceof OgArticle)
            bean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        }
        Boolean suc = getProxyFactory().getOgArticleServiceProxy().updateOgArticle(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteOgArticle(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getOgArticleServiceProxy().deleteOgArticle(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            OgArticle ogArticle = null;
            try {
                ogArticle = getProxyFactory().getOgArticleServiceProxy().getOgArticle(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch ogArticle with a key, " + guid);
                return false;
            }
            if(ogArticle != null) {
                String beanGuid = ogArticle.getGuid();
                Boolean suc1 = getProxyFactory().getOgArticleServiceProxy().deleteOgArticle(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("ogArticle with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteOgArticle(OgArticle ogArticle) throws BaseException
    {
        log.finer("BEGIN");

        // Param ogArticle cannot be null.....
        if(ogArticle == null || ogArticle.getGuid() == null) {
            log.log(Level.INFO, "Param ogArticle or its guid is null!");
            throw new BadRequestException("Param ogArticle object or its guid is null!");
        }
        OgArticleBean bean = null;
        if(ogArticle instanceof OgArticleBean) {
            bean = (OgArticleBean) ogArticle;
        } else {  // if(ogArticle instanceof OgArticle)
            // ????
            log.warning("ogArticle is not an instance of OgArticleBean.");
            bean = new OgArticleBean(ogArticle.getGuid(), ogArticle.getUrl(), ogArticle.getType(), ogArticle.getSiteName(), ogArticle.getTitle(), ogArticle.getDescription(), ogArticle.getFbAdmins(), ogArticle.getFbAppId(), ogArticle.getImage(), ogArticle.getAudio(), ogArticle.getVideo(), ogArticle.getLocale(), ogArticle.getLocaleAlternate(), ogArticle.getAuthor(), ogArticle.getSection(), ogArticle.getTag(), ogArticle.getPublishedDate(), ogArticle.getModifiedDate(), ogArticle.getExpirationDate());
        }
        Boolean suc = getProxyFactory().getOgArticleServiceProxy().deleteOgArticle(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteOgArticles(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getOgArticleServiceProxy().deleteOgArticles(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createOgArticles(List<OgArticle> ogArticles) throws BaseException
    {
        log.finer("BEGIN");

        if(ogArticles == null) {
            log.log(Level.WARNING, "createOgArticles() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = ogArticles.size();
        if(size == 0) {
            log.log(Level.WARNING, "createOgArticles() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(OgArticle ogArticle : ogArticles) {
            String guid = createOgArticle(ogArticle);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createOgArticles() failed for at least one ogArticle. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateOgArticles(List<OgArticle> ogArticles) throws BaseException
    //{
    //}

}
