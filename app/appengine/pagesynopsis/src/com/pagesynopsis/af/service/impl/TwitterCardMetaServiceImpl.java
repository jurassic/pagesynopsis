package com.pagesynopsis.af.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.af.config.Config;

import com.pagesynopsis.af.bean.OgGeoPointStructBean;
import com.pagesynopsis.af.bean.GeoPointStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.StreetAddressStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.MediaSourceStructBean;
import com.pagesynopsis.af.bean.OgContactInfoStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.FullNameStructBean;
import com.pagesynopsis.af.bean.GeoCoordinateStructBean;
import com.pagesynopsis.af.bean.DecodedQueryParamStructBean;
import com.pagesynopsis.af.bean.GaeUserStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.GaeAppStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.KeyValueRelationStructBean;
import com.pagesynopsis.af.bean.ReferrerInfoStructBean;
import com.pagesynopsis.af.bean.PagerStateStructBean;
import com.pagesynopsis.af.bean.NotificationStructBean;
import com.pagesynopsis.af.bean.UserWebsiteStructBean;
import com.pagesynopsis.af.bean.HelpNoticeBean;
import com.pagesynopsis.af.bean.TwitterCardAppInfoBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.OgQuantityStructBean;
import com.pagesynopsis.af.bean.ContactInfoStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.EncodedQueryParamStructBean;
import com.pagesynopsis.af.bean.AppBrandStructBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.bean.ExternalServiceApiKeyStructBean;
import com.pagesynopsis.af.bean.TwitterCardProductDataBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;

import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.TwitterCardMetaService;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterCardMetaServiceImpl implements TwitterCardMetaService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaServiceImpl.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    // Public?
    public Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }
    private String getObjectCacheKey(String guid)
    {
        String key = "TwitterCardMeta-" + guid;
        return key;
    }


    // To be used only in subclasses (e.g., AppService) for finder methods...
    // Note, however, that findXXX() results should not really be cached...
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count)
    {
        return getFinderCacheKey(filter, ordering, params, values, grouping, unique, offset, count, null);
    }
    protected String getFinderCacheKey(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor)
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("TwitterCardMeta:");
    	if(filter != null) {
    		sb.append(filter);
    	}
    	sb.append(":");
    	if(ordering != null) {
    		sb.append(ordering);
    	}
    	sb.append(":");
    	if(params != null) {
    		sb.append(params);
    	}
    	sb.append(":");
    	if(values != null) {
    		for(String v : values) {
        		sb.append(v).append("-");    			
    		}
    	}
    	sb.append(":");
    	if(grouping != null) {
    		sb.append(grouping);
    	}
    	sb.append(":");
    	if(unique != null) {
    		sb.append(unique);
    	}
    	sb.append(":");
    	if(offset != null) {
    		sb.append(offset);
    	}
    	sb.append(":");
    	if(count != null) {
    		sb.append(count);
    	}
    	sb.append(":");
    	if(forwardCursor != null && forwardCursor.getWebSafeString() != null) {
    		sb.append(forwardCursor.getWebSafeString());
    	}
    	sb.append(":");

    	String cacheKey = sb.toString();
    	return cacheKey;
    }

    public TwitterCardMetaServiceImpl()
    {
        initCache();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterCardMeta related methods
    //////////////////////////////////////////////////////////////////////////

    @Override
    public TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getTwitterCardMeta(): guid = " + guid);

        TwitterCardMetaBean bean = null;
        if(getCache() != null) {
            bean = (TwitterCardMetaBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterCardMetaBean) getProxyFactory().getTwitterCardMetaServiceProxy().getTwitterCardMeta(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterCardMetaBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaBean for guid = " + guid);
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Object getTwitterCardMeta(String guid, String field) throws BaseException
    {
        TwitterCardMetaBean bean = null;
        if(getCache() != null) {
            bean = (TwitterCardMetaBean) getCache().get(getObjectCacheKey(guid));
        }
        if(bean == null) {
            bean = (TwitterCardMetaBean) getProxyFactory().getTwitterCardMetaServiceProxy().getTwitterCardMeta(guid);
            if(getCache() != null && bean != null) {
                // Note that the input arg guid might have been actually a key. 
                // We need to cache the object based on both guid/key and bean.guid... 
                String beanGuid = bean.getGuid();
                if(GUID.isValid(guid) && guid.equals(beanGuid)) {   // ??? what happens if guid != beanGuid while guid is valid??? Cant this happen???
                    getCache().put(getObjectCacheKey(guid), bean);
                } else {
                    // TBD: Dual caching makes things more complicated....
                    // Without this, cache will never be used when we fetch objects based on keys (not guids)
                    //getCache().put(getObjectCacheKey(guid), bean);
                    // TBD: Unfortunately, when an object is updated (without key information),
                    //      the cached object based on its key will go out of sync.....
                    // Until we solve that issue, we cannot use caching based on keys.....
                    getCache().put(getObjectCacheKey(beanGuid), bean);
                }
            }
        } else {
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "TwitterCardMetaBean object retrieved from Cache for guid = " + guid);
        }
        if(bean == null) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaBean for guid = " + guid);
            return null;
        }
        
        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("user")) {
            return bean.getUser();
        } else if(field.equals("fetchRequest")) {
            return bean.getFetchRequest();
        } else if(field.equals("targetUrl")) {
            return bean.getTargetUrl();
        } else if(field.equals("pageUrl")) {
            return bean.getPageUrl();
        } else if(field.equals("queryString")) {
            return bean.getQueryString();
        } else if(field.equals("queryParams")) {
            return bean.getQueryParams();
        } else if(field.equals("lastFetchResult")) {
            return bean.getLastFetchResult();
        } else if(field.equals("responseCode")) {
            return bean.getResponseCode();
        } else if(field.equals("contentType")) {
            return bean.getContentType();
        } else if(field.equals("contentLength")) {
            return bean.getContentLength();
        } else if(field.equals("language")) {
            return bean.getLanguage();
        } else if(field.equals("redirect")) {
            return bean.getRedirect();
        } else if(field.equals("location")) {
            return bean.getLocation();
        } else if(field.equals("pageTitle")) {
            return bean.getPageTitle();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("deferred")) {
            return bean.isDeferred();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("refreshStatus")) {
            return bean.getRefreshStatus();
        } else if(field.equals("refreshInterval")) {
            return bean.getRefreshInterval();
        } else if(field.equals("nextRefreshTime")) {
            return bean.getNextRefreshTime();
        } else if(field.equals("lastCheckedTime")) {
            return bean.getLastCheckedTime();
        } else if(field.equals("lastUpdatedTime")) {
            return bean.getLastUpdatedTime();
        } else if(field.equals("cardType")) {
            return bean.getCardType();
        } else if(field.equals("summaryCard")) {
            return bean.getSummaryCard();
        } else if(field.equals("photoCard")) {
            return bean.getPhotoCard();
        } else if(field.equals("galleryCard")) {
            return bean.getGalleryCard();
        } else if(field.equals("appCard")) {
            return bean.getAppCard();
        } else if(field.equals("playerCard")) {
            return bean.getPlayerCard();
        } else if(field.equals("productCard")) {
            return bean.getProductCard();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        log.fine("getTwitterCardMetas()");

        // TBD: Is there a better way????
        List<TwitterCardMeta> twitterCardMetas = getProxyFactory().getTwitterCardMetaServiceProxy().getTwitterCardMetas(guids);
        if(twitterCardMetas == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaBean list.");
        }

        log.finer("END");
        return twitterCardMetas;
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException
    {
        return getAllTwitterCardMetas(null, null, null);
    }


    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetas(ordering, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetas(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TwitterCardMeta> twitterCardMetas = getProxyFactory().getTwitterCardMetaServiceProxy().getAllTwitterCardMetas(ordering, offset, count, forwardCursor);
        if(twitterCardMetas == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaBean list.");
        }

        log.finer("END");
        return twitterCardMetas;
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllTwitterCardMetaKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("getAllTwitterCardMetaKeys(): ordering = " + ordering + "; offset = " + offset + "; count = " + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterCardMetaServiceProxy().getAllTwitterCardMetaKeys(ordering, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to retrieve TwitterCardMetaBean keys.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterCardMetaBean key list.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaServiceImpl.findTwitterCardMetas(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<TwitterCardMeta> twitterCardMetas = getProxyFactory().getTwitterCardMetaServiceProxy().findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(twitterCardMetas == null) {
            log.log(Level.WARNING, "Failed to find twitterCardMetas for the given criterion.");
        }

        log.finer("END");
        return twitterCardMetas;
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaServiceImpl.findTwitterCardMetaKeys(): filter=" + filter + "; ordering=" + ordering + "; params=" + params + "; grouping=" + grouping + "; unique=" + unique + "; offset=" + offset + "; count=" + count + "; forwardCursor = " + forwardCursor);

        // TBD: Is there a better way????
        List<String> keys = getProxyFactory().getTwitterCardMetaServiceProxy().findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
        if(keys == null) {
            log.log(Level.WARNING, "Failed to find TwitterCardMeta keys for the given criterion.");
        } else if(keys.isEmpty()) {
            log.log(Level.FINE, "Retrieved an empty TwitterCardMeta key list for the given criterion.");
        }

        log.finer("END");
        return keys;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("TwitterCardMetaServiceImpl.getCount(): filter=" + filter + "; params=" + params + "; aggregate=" + aggregate);

        Long count = getProxyFactory().getTwitterCardMetaServiceProxy().getCount(filter, params, values, aggregate);

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    @Override
    public String createTwitterCardMeta(String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        // Param pk/name cannot be null. What about an empty string ""????
        // if(name == null) {
        //     log.log(Level.INFO, "Param name is null!");
        //     return null;  // ???
        // }

        TwitterSummaryCardBean summaryCardBean = null;
        if(summaryCard instanceof TwitterSummaryCardBean) {
            summaryCardBean = (TwitterSummaryCardBean) summaryCard;
        } else if(summaryCard instanceof TwitterSummaryCard) {
            summaryCardBean = new TwitterSummaryCardBean(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            summaryCardBean = null;   // ????
        }
        TwitterPhotoCardBean photoCardBean = null;
        if(photoCard instanceof TwitterPhotoCardBean) {
            photoCardBean = (TwitterPhotoCardBean) photoCard;
        } else if(photoCard instanceof TwitterPhotoCard) {
            photoCardBean = new TwitterPhotoCardBean(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            photoCardBean = null;   // ????
        }
        TwitterGalleryCardBean galleryCardBean = null;
        if(galleryCard instanceof TwitterGalleryCardBean) {
            galleryCardBean = (TwitterGalleryCardBean) galleryCard;
        } else if(galleryCard instanceof TwitterGalleryCard) {
            galleryCardBean = new TwitterGalleryCardBean(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            galleryCardBean = null;   // ????
        }
        TwitterAppCardBean appCardBean = null;
        if(appCard instanceof TwitterAppCardBean) {
            appCardBean = (TwitterAppCardBean) appCard;
        } else if(appCard instanceof TwitterAppCard) {
            appCardBean = new TwitterAppCardBean(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), (TwitterCardAppInfoBean) appCard.getIphoneApp(), (TwitterCardAppInfoBean) appCard.getIpadApp(), (TwitterCardAppInfoBean) appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            appCardBean = null;   // ????
        }
        TwitterPlayerCardBean playerCardBean = null;
        if(playerCard instanceof TwitterPlayerCardBean) {
            playerCardBean = (TwitterPlayerCardBean) playerCard;
        } else if(playerCard instanceof TwitterPlayerCard) {
            playerCardBean = new TwitterPlayerCardBean(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            playerCardBean = null;   // ????
        }
        TwitterProductCardBean productCardBean = null;
        if(productCard instanceof TwitterProductCardBean) {
            productCardBean = (TwitterProductCardBean) productCard;
        } else if(productCard instanceof TwitterProductCard) {
            productCardBean = new TwitterProductCardBean(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), (TwitterCardProductDataBean) productCard.getData1(), (TwitterCardProductDataBean) productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            productCardBean = null;   // ????
        }
        TwitterCardMetaBean bean = new TwitterCardMetaBean(null, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCardBean, photoCardBean, galleryCardBean, appCardBean, playerCardBean, productCardBean);
        return createTwitterCardMeta(bean);
    }

    @Override
    public String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterCardMeta bean = constructTwitterCardMeta(twitterCardMeta);
        //return bean.getGuid();

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null) {
            log.log(Level.INFO, "Param twitterCardMeta is null!");
            throw new BadRequestException("Param twitterCardMeta object is null!");
        }
        TwitterCardMetaBean bean = null;
        if(twitterCardMeta instanceof TwitterCardMetaBean) {
            bean = (TwitterCardMetaBean) twitterCardMeta;
        } else if(twitterCardMeta instanceof TwitterCardMeta) {
            // bean = new TwitterCardMetaBean(null, twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
        } else {
            log.log(Level.WARNING, "createTwitterCardMeta(): Arg twitterCardMeta is of an unknown type.");
            //bean = new TwitterCardMetaBean();
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterCardMetaServiceProxy().createTwitterCardMeta(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: guid = " + guid);
        return guid;
    }

    @Override
    public TwitterCardMeta constructTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null) {
            log.log(Level.INFO, "Param twitterCardMeta is null!");
            throw new BadRequestException("Param twitterCardMeta object is null!");
        }
        TwitterCardMetaBean bean = null;
        if(twitterCardMeta instanceof TwitterCardMetaBean) {
            bean = (TwitterCardMetaBean) twitterCardMeta;
        } else if(twitterCardMeta instanceof TwitterCardMeta) {
            // bean = new TwitterCardMetaBean(null, twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
            // If this is a new object, its guid should have been null.
            // Or, the guid might have been explicitly set for a reason. In that case, use it.
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
        } else {
            log.log(Level.WARNING, "createTwitterCardMeta(): Arg twitterCardMeta is of an unknown type.");
            //bean = new TwitterCardMetaBean();
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid());   // ???
        }
        String guid = bean.getGuid();
        if(guid == null) { // It seems more convenient to set the guid on the client side rather than on the server side.
            bean.setGuid(GUID.generate());
        }
        guid = getProxyFactory().getTwitterCardMetaServiceProxy().createTwitterCardMeta(bean);
        // TBD: We should NOT really cache during creation....
        // The object we have may be "incomplete" (before saving)...
        // For us, however, it is generally safe as long as we set guid/createdTime before saving...
        // ...
        if(getCache() != null) {
            if(guid != null && !guid.isEmpty()) {
                getCache().put(getObjectCacheKey(guid), bean);   // Note: bean.getCreatedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean updateTwitterCardMeta(String guid, String user, String fetchRequest, String targetUrl, String pageUrl, String queryString, List<KeyValuePairStruct> queryParams, String lastFetchResult, Integer responseCode, String contentType, Integer contentLength, String language, String redirect, String location, String pageTitle, String note, Boolean deferred, String status, Integer refreshStatus, Long refreshInterval, Long nextRefreshTime, Long lastCheckedTime, Long lastUpdatedTime, String cardType, TwitterSummaryCard summaryCard, TwitterPhotoCard photoCard, TwitterGalleryCard galleryCard, TwitterAppCard appCard, TwitterPlayerCard playerCard, TwitterProductCard productCard) throws BaseException
    {
        // Note: We can retrieve the object with the given guid, update it, and save it.
        // Instead, we simply create a fake object it and save it.
        // (This should be the same. But, need to check. TBD.)
        TwitterSummaryCardBean summaryCardBean = null;
        if(summaryCard instanceof TwitterSummaryCardBean) {
            summaryCardBean = (TwitterSummaryCardBean) summaryCard;
        } else if(summaryCard instanceof TwitterSummaryCard) {
            summaryCardBean = new TwitterSummaryCardBean(summaryCard.getGuid(), summaryCard.getCard(), summaryCard.getUrl(), summaryCard.getTitle(), summaryCard.getDescription(), summaryCard.getSite(), summaryCard.getSiteId(), summaryCard.getCreator(), summaryCard.getCreatorId(), summaryCard.getImage(), summaryCard.getImageWidth(), summaryCard.getImageHeight(), summaryCard.getCreatedTime(), summaryCard.getModifiedTime());
        } else {
            summaryCardBean = null;   // ????
        }
        TwitterPhotoCardBean photoCardBean = null;
        if(photoCard instanceof TwitterPhotoCardBean) {
            photoCardBean = (TwitterPhotoCardBean) photoCard;
        } else if(photoCard instanceof TwitterPhotoCard) {
            photoCardBean = new TwitterPhotoCardBean(photoCard.getGuid(), photoCard.getCard(), photoCard.getUrl(), photoCard.getTitle(), photoCard.getDescription(), photoCard.getSite(), photoCard.getSiteId(), photoCard.getCreator(), photoCard.getCreatorId(), photoCard.getImage(), photoCard.getImageWidth(), photoCard.getImageHeight(), photoCard.getCreatedTime(), photoCard.getModifiedTime());
        } else {
            photoCardBean = null;   // ????
        }
        TwitterGalleryCardBean galleryCardBean = null;
        if(galleryCard instanceof TwitterGalleryCardBean) {
            galleryCardBean = (TwitterGalleryCardBean) galleryCard;
        } else if(galleryCard instanceof TwitterGalleryCard) {
            galleryCardBean = new TwitterGalleryCardBean(galleryCard.getGuid(), galleryCard.getCard(), galleryCard.getUrl(), galleryCard.getTitle(), galleryCard.getDescription(), galleryCard.getSite(), galleryCard.getSiteId(), galleryCard.getCreator(), galleryCard.getCreatorId(), galleryCard.getImage0(), galleryCard.getImage1(), galleryCard.getImage2(), galleryCard.getImage3(), galleryCard.getCreatedTime(), galleryCard.getModifiedTime());
        } else {
            galleryCardBean = null;   // ????
        }
        TwitterAppCardBean appCardBean = null;
        if(appCard instanceof TwitterAppCardBean) {
            appCardBean = (TwitterAppCardBean) appCard;
        } else if(appCard instanceof TwitterAppCard) {
            appCardBean = new TwitterAppCardBean(appCard.getGuid(), appCard.getCard(), appCard.getUrl(), appCard.getTitle(), appCard.getDescription(), appCard.getSite(), appCard.getSiteId(), appCard.getCreator(), appCard.getCreatorId(), appCard.getImage(), appCard.getImageWidth(), appCard.getImageHeight(), (TwitterCardAppInfoBean) appCard.getIphoneApp(), (TwitterCardAppInfoBean) appCard.getIpadApp(), (TwitterCardAppInfoBean) appCard.getGooglePlayApp(), appCard.getCreatedTime(), appCard.getModifiedTime());
        } else {
            appCardBean = null;   // ????
        }
        TwitterPlayerCardBean playerCardBean = null;
        if(playerCard instanceof TwitterPlayerCardBean) {
            playerCardBean = (TwitterPlayerCardBean) playerCard;
        } else if(playerCard instanceof TwitterPlayerCard) {
            playerCardBean = new TwitterPlayerCardBean(playerCard.getGuid(), playerCard.getCard(), playerCard.getUrl(), playerCard.getTitle(), playerCard.getDescription(), playerCard.getSite(), playerCard.getSiteId(), playerCard.getCreator(), playerCard.getCreatorId(), playerCard.getImage(), playerCard.getImageWidth(), playerCard.getImageHeight(), playerCard.getPlayer(), playerCard.getPlayerWidth(), playerCard.getPlayerHeight(), playerCard.getPlayerStream(), playerCard.getPlayerStreamContentType(), playerCard.getCreatedTime(), playerCard.getModifiedTime());
        } else {
            playerCardBean = null;   // ????
        }
        TwitterProductCardBean productCardBean = null;
        if(productCard instanceof TwitterProductCardBean) {
            productCardBean = (TwitterProductCardBean) productCard;
        } else if(productCard instanceof TwitterProductCard) {
            productCardBean = new TwitterProductCardBean(productCard.getGuid(), productCard.getCard(), productCard.getUrl(), productCard.getTitle(), productCard.getDescription(), productCard.getSite(), productCard.getSiteId(), productCard.getCreator(), productCard.getCreatorId(), productCard.getImage(), productCard.getImageWidth(), productCard.getImageHeight(), (TwitterCardProductDataBean) productCard.getData1(), (TwitterCardProductDataBean) productCard.getData2(), productCard.getCreatedTime(), productCard.getModifiedTime());
        } else {
            productCardBean = null;   // ????
        }
        TwitterCardMetaBean bean = new TwitterCardMetaBean(guid, user, fetchRequest, targetUrl, pageUrl, queryString, queryParams, lastFetchResult, responseCode, contentType, contentLength, language, redirect, location, pageTitle, note, deferred, status, refreshStatus, refreshInterval, nextRefreshTime, lastCheckedTime, lastUpdatedTime, cardType, summaryCardBean, photoCardBean, galleryCardBean, appCardBean, playerCardBean, productCardBean);
        return updateTwitterCardMeta(bean);
    }
        
    @Override
    public Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // TBD
        //TwitterCardMeta bean = refreshTwitterCardMeta(twitterCardMeta);
        //if(bean != null) {
        //    return true;
        //} else {
        //    return false;
        //}

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null || twitterCardMeta.getGuid() == null) {
            log.log(Level.INFO, "Param twitterCardMeta or its guid is null!");
            throw new BadRequestException("Param twitterCardMeta object or its guid is null!");
        }
        TwitterCardMetaBean bean = null;
        if(twitterCardMeta instanceof TwitterCardMetaBean) {
            bean = (TwitterCardMetaBean) twitterCardMeta;
        } else {  // if(twitterCardMeta instanceof TwitterCardMeta)
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
        }
        Boolean suc = getProxyFactory().getTwitterCardMetaServiceProxy().updateTwitterCardMeta(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    @Override
    public TwitterCardMeta refreshTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null || twitterCardMeta.getGuid() == null) {
            log.log(Level.INFO, "Param twitterCardMeta or its guid is null!");
            throw new BadRequestException("Param twitterCardMeta object or its guid is null!");
        }
        TwitterCardMetaBean bean = null;
        if(twitterCardMeta instanceof TwitterCardMetaBean) {
            bean = (TwitterCardMetaBean) twitterCardMeta;
        } else {  // if(twitterCardMeta instanceof TwitterCardMeta)
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
        }
        Boolean suc = getProxyFactory().getTwitterCardMetaServiceProxy().updateTwitterCardMeta(bean);
        // TBD: We should really erase rather than put/re-put during update....
        // The object, once saved, may have been modified (e.g., by the server)...
        // For us, however, it is generally safe as long as we set modifiedTime before saving...
        // ...
        if(getCache() != null) {
            if(Boolean.TRUE.equals(suc)) {
                //getCache().remove(getObjectCacheKey(bean.getGuid()));
                getCache().put(getObjectCacheKey(bean.getGuid()), bean);   // Note: bean.getModifiedTime() has changed.
            }
        }

        log.finer("END");
        return bean;
    }

    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        log.finer("BEGIN");

        // Param guid cannot be null.....
        if(guid == null) {
            log.log(Level.INFO, "Param guid is null!");
            throw new BadRequestException("Param guid is null!");
        }
        if(GUID.isValid(guid)) {
            Boolean suc = getProxyFactory().getTwitterCardMetaServiceProxy().deleteTwitterCardMeta(guid);
            if(getCache() != null) {
                //if(Boolean.TRUE.equals(suc)) {
                    getCache().remove(getObjectCacheKey(guid));
                //}
            }
            if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
            return suc;
        } else {
            // Note that arg guid might be actually a key.
            // We need to delete the cached object based on bean.guid as well... 
            TwitterCardMeta twitterCardMeta = null;
            try {
                twitterCardMeta = getProxyFactory().getTwitterCardMetaServiceProxy().getTwitterCardMeta(guid);
            } catch(BaseException e) {
                if(log.isLoggable(Level.WARNING)) log.warning("Exception while trying to fetch twitterCardMeta with a key, " + guid);
                return false;
            }
            if(twitterCardMeta != null) {
                String beanGuid = twitterCardMeta.getGuid();
                Boolean suc1 = getProxyFactory().getTwitterCardMetaServiceProxy().deleteTwitterCardMeta(guid);
                if(getCache() != null) {
                    //if(Boolean.TRUE.equals(suc)) {
                        getCache().remove(getObjectCacheKey(guid));
                        getCache().remove(getObjectCacheKey(beanGuid));
                    //}
                }
                if(log.isLoggable(Level.FINER)) log.finer("END: suc1 = " + suc1);
                return suc1;
            } else {
                // Nothing to delete ???? Possibly, an error ????
                if(log.isLoggable(Level.WARNING)) log.warning("twitterCardMeta with a key, " + guid + ", not found. Nothing to delete?");
                return true;
            }
        }
    }

    // ???
    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("BEGIN");

        // Param twitterCardMeta cannot be null.....
        if(twitterCardMeta == null || twitterCardMeta.getGuid() == null) {
            log.log(Level.INFO, "Param twitterCardMeta or its guid is null!");
            throw new BadRequestException("Param twitterCardMeta object or its guid is null!");
        }
        TwitterCardMetaBean bean = null;
        if(twitterCardMeta instanceof TwitterCardMetaBean) {
            bean = (TwitterCardMetaBean) twitterCardMeta;
        } else {  // if(twitterCardMeta instanceof TwitterCardMeta)
            // ????
            log.warning("twitterCardMeta is not an instance of TwitterCardMetaBean.");
            bean = new TwitterCardMetaBean(twitterCardMeta.getGuid(), twitterCardMeta.getUser(), twitterCardMeta.getFetchRequest(), twitterCardMeta.getTargetUrl(), twitterCardMeta.getPageUrl(), twitterCardMeta.getQueryString(), twitterCardMeta.getQueryParams(), twitterCardMeta.getLastFetchResult(), twitterCardMeta.getResponseCode(), twitterCardMeta.getContentType(), twitterCardMeta.getContentLength(), twitterCardMeta.getLanguage(), twitterCardMeta.getRedirect(), twitterCardMeta.getLocation(), twitterCardMeta.getPageTitle(), twitterCardMeta.getNote(), twitterCardMeta.isDeferred(), twitterCardMeta.getStatus(), twitterCardMeta.getRefreshStatus(), twitterCardMeta.getRefreshInterval(), twitterCardMeta.getNextRefreshTime(), twitterCardMeta.getLastCheckedTime(), twitterCardMeta.getLastUpdatedTime(), twitterCardMeta.getCardType(), (TwitterSummaryCardBean) twitterCardMeta.getSummaryCard(), (TwitterPhotoCardBean) twitterCardMeta.getPhotoCard(), (TwitterGalleryCardBean) twitterCardMeta.getGalleryCard(), (TwitterAppCardBean) twitterCardMeta.getAppCard(), (TwitterPlayerCardBean) twitterCardMeta.getPlayerCard(), (TwitterProductCardBean) twitterCardMeta.getProductCard());
        }
        Boolean suc = getProxyFactory().getTwitterCardMetaServiceProxy().deleteTwitterCardMeta(bean);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
                getCache().remove(getObjectCacheKey(bean.getGuid()));
            //}
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: suc = " + suc);
        return suc;
    }

    // TBD
    @Override
    public Long deleteTwitterCardMetas(String filter, String params, List<String> values) throws BaseException
    {
        Long count = getProxyFactory().getTwitterCardMetaServiceProxy().deleteTwitterCardMetas(filter, params, values);
        if(getCache() != null) {
            //if(Boolean.TRUE.equals(suc)) {
            //    getCache().remove(...);  // ???
            //}
        }

        return count;
    }

    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    {
        log.finer("BEGIN");

        if(twitterCardMetas == null) {
            log.log(Level.WARNING, "createTwitterCardMetas() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = twitterCardMetas.size();
        if(size == 0) {
            log.log(Level.WARNING, "createTwitterCardMetas() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(TwitterCardMeta twitterCardMeta : twitterCardMetas) {
            String guid = createTwitterCardMeta(twitterCardMeta);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createTwitterCardMetas() failed for at least one twitterCardMeta. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    //{
    //}

}
