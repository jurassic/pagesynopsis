package com.pagesynopsis.af.service;

import java.util.List;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;


// Business object layer.
// Each of the methods returns a bean object.
// which can be used in the web service layer (wrapped in a stub)
// or can be used in UI.
// (TBD: Use AppEngine MemCache to cache either xxxDataObjects or xxxBeans.)
public interface OgProfileService extends Service
{
    // TBD: Return an interface or a bean wrapper.

    OgProfile getOgProfile(String guid) throws BaseException;
    Object getOgProfile(String guid, String field) throws BaseException;
    List<OgProfile> getOgProfiles(List<String> guids) throws BaseException;
    List<OgProfile> getAllOgProfiles() throws BaseException;
    /* @Deprecated */ List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count) throws BaseException;
    List<OgProfile> getAllOgProfiles(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException;
    List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values) throws BaseException;
    /* @Deprecated */ List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    /* @Deprecated */ List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException;
    List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException;
    Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException;
    String createOgProfile(String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException;
    //String createOgProfile(Map<String, Object> args) throws BaseException;   // Returns Guid.  (Return OgProfile?)
    String createOgProfile(OgProfile ogProfile) throws BaseException;
    OgProfile constructOgProfile(OgProfile ogProfile) throws BaseException;
    Boolean updateOgProfile(String guid, String url, String type, String siteName, String title, String description, List<String> fbAdmins, List<String> fbAppId, List<OgImageStruct> image, List<OgAudioStruct> audio, List<OgVideoStruct> video, String locale, List<String> localeAlternate, String profileId, String firstName, String lastName, String username, String gender) throws BaseException;
    //Boolean updateOgProfile(String guid, Map<String, Object> args) throws BaseException;
    Boolean updateOgProfile(OgProfile ogProfile) throws BaseException;
    OgProfile refreshOgProfile(OgProfile ogProfile) throws BaseException;
    Boolean deleteOgProfile(String guid) throws BaseException;
    Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException;
    Long deleteOgProfiles(String filter, String params, List<String> values) throws BaseException;

    // TBD
    Integer createOgProfiles(List<OgProfile> ogProfiles) throws BaseException;
//    Boolean updateOgProfiles(List<OgProfile> ogProfiles) throws BaseException;

}
