package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.UrlStruct;
import com.pagesynopsis.ws.ImageStruct;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.AnchorStruct;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.AudioStruct;
import com.pagesynopsis.ws.VideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.UrlStructBean;
import com.pagesynopsis.af.bean.ImageStructBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.AnchorStructBean;
import com.pagesynopsis.af.bean.KeyValuePairStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.AudioStructBean;
import com.pagesynopsis.af.bean.VideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.impl.AudioSetServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class AudioSetProtoService extends AudioSetServiceImpl implements AudioSetService
{
    private static final Logger log = Logger.getLogger(AudioSetProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AudioSetProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AudioSet related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AudioSet getAudioSet(String guid) throws BaseException
    {
        return super.getAudioSet(guid);
    }

    @Override
    public Object getAudioSet(String guid, String field) throws BaseException
    {
        return super.getAudioSet(guid, field);
    }

    @Override
    public List<AudioSet> getAudioSets(List<String> guids) throws BaseException
    {
        return super.getAudioSets(guids);
    }

    @Override
    public List<AudioSet> getAllAudioSets() throws BaseException
    {
        return super.getAllAudioSets();
    }

    @Override
    public List<AudioSet> getAllAudioSets(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAudioSets(ordering, offset, count, null);
    }

    @Override
    public List<AudioSet> getAllAudioSets(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllAudioSets(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllAudioSetKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllAudioSetKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findAudioSets(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAudioSet(AudioSet audioSet) throws BaseException
    {
        return super.createAudioSet(audioSet);
    }

    @Override
    public AudioSet constructAudioSet(AudioSet audioSet) throws BaseException
    {
        return super.constructAudioSet(audioSet);
    }


    @Override
    public Boolean updateAudioSet(AudioSet audioSet) throws BaseException
    {
        return super.updateAudioSet(audioSet);
    }
        
    @Override
    public AudioSet refreshAudioSet(AudioSet audioSet) throws BaseException
    {
        return super.refreshAudioSet(audioSet);
    }

    @Override
    public Boolean deleteAudioSet(String guid) throws BaseException
    {
        return super.deleteAudioSet(guid);
    }

    @Override
    public Boolean deleteAudioSet(AudioSet audioSet) throws BaseException
    {
        return super.deleteAudioSet(audioSet);
    }

    @Override
    public Integer createAudioSets(List<AudioSet> audioSets) throws BaseException
    {
        return super.createAudioSets(audioSets);
    }

    // TBD
    //@Override
    //public Boolean updateAudioSets(List<AudioSet> audioSets) throws BaseException
    //{
    //}

}
