package com.pagesynopsis.af.service.proto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.core.StringCursor;
import com.pagesynopsis.ws.RobotsTextRefresh;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.impl.RobotsTextRefreshServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class RobotsTextRefreshProtoService extends RobotsTextRefreshServiceImpl implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshProtoService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public RobotsTextRefreshProtoService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // RobotsTextRefresh related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        return super.getRobotsTextRefresh(guid);
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        return super.getRobotsTextRefresh(guid, field);
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        return super.getRobotsTextRefreshes(guids);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return super.getAllRobotsTextRefreshes();
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshes(ordering, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllRobotsTextRefreshes(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return getAllRobotsTextRefreshKeys(ordering, offset, count, null);
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.getAllRobotsTextRefreshKeys(ordering, offset, count, forwardCursor);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, null);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count, StringCursor forwardCursor) throws BaseException
    {
        return super.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count, forwardCursor);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return super.createRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public RobotsTextRefresh constructRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return super.constructRobotsTextRefresh(robotsTextRefresh);
    }


    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return super.updateRobotsTextRefresh(robotsTextRefresh);
    }
        
    @Override
    public RobotsTextRefresh refreshRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return super.refreshRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return super.deleteRobotsTextRefresh(guid);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return super.deleteRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Integer createRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    {
        return super.createRobotsTextRefreshes(robotsTextRefreshes);
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    //{
    //}

}
