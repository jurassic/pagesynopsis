package com.pagesynopsis.common;


public class OpenGraphType
{
    private OpenGraphType() {}

    public final static String OG_TYPE_PROFILE = "profile";
    public final static String OG_TYPE_WEBSITE = "website";
    public final static String OG_TYPE_BLOG = "blog";
    public final static String OG_TYPE_ARTICLE = "article";
    public final static String OG_TYPE_BOOK = "book";
    public final static String OG_TYPE_VIDEO = "video";
    public final static String OG_TYPE_MOVIE = "movie";
    public final static String OG_TYPE_TVSHOW = "tvshow";
    public final static String OG_TYPE_TVEPISODE = "tvepisode";
    // etc...
    

    // temporary
//    public static boolean isBuiltIn(String type) 
//    {
//        return false;
//    }

    
    
    
}
