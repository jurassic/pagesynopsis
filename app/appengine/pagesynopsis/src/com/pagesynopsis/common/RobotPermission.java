package com.pagesynopsis.common;


public class RobotPermission
{
    private RobotPermission() {}

    // Use int ???
    public static final String PERMISSION_UNKNOWN = "unknown";
    public static final String PERMISSION_ALLOWED = "allowed";
    public static final String PERMISSION_DISALLOWED = "disallowed";
    // ...

}
