package com.pagesynopsis.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

// TBD: ....
public class FetchStatus
{
    private static final Logger log = Logger.getLogger(FetchStatus.class.getName());

    // Static methods only.
    private FetchStatus() {}

    // TBD
    //public static final int STATUS_UNKNOWN = -1;  1000 ???
    public static final int STATUS_REQUESTED = 1;         // Currently, not being used.
    public static final int STATUS_CREATED = 2;           // FetchRequest obj has been created. (not necessarily persisted)
    public static final int STATUS_SCHEDULED = 4;         // ???
    public static final int STATUS_PROCESSING = 8;        // Before the scraping attempt.   (For error handling....)  Currently, not being used. 
    public static final int STATUS_FAILED = 16;           // after the scraping attempt. failed???
    public static final int STATUS_PROCESSED = 32;        // after the scraping attempt. success???
    // etc.
    //...
    
}
