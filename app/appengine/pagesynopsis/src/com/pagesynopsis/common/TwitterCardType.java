package com.pagesynopsis.common;


public class TwitterCardType
{
    private TwitterCardType() {}

    public final static String TC_TYPE_SUMMARY = "summary";
    public final static String TC_TYPE_PHOTO = "photo";
    public final static String TC_TYPE_GALLERY = "gallery";
    public final static String TC_TYPE_APP = "app";
    public final static String TC_TYPE_PLAYER = "player";
    public final static String TC_TYPE_PRODUCT = "product";
    // etc...

}
