package com.pagesynopsis.app.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pagesynopsis.af.service.AbstractServiceFactory;
import com.pagesynopsis.af.service.ApiConsumerService;
import com.pagesynopsis.af.service.UserService;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.OgWebsiteService;
import com.pagesynopsis.af.service.OgBlogService;
import com.pagesynopsis.af.service.OgArticleService;
import com.pagesynopsis.af.service.OgBookService;
import com.pagesynopsis.af.service.OgVideoService;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.OgTvShowService;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.TwitterSummaryCardService;
import com.pagesynopsis.af.service.TwitterPhotoCardService;
import com.pagesynopsis.af.service.TwitterGalleryCardService;
import com.pagesynopsis.af.service.TwitterAppCardService;
import com.pagesynopsis.af.service.TwitterPlayerCardService;
import com.pagesynopsis.af.service.TwitterProductCardService;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.PageFetchService;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.ImageSetService;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.VideoSetService;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.DomainInfoService;
import com.pagesynopsis.af.service.UrlRatingService;
import com.pagesynopsis.af.service.ServiceInfoService;
import com.pagesynopsis.af.service.FiveTenService;

public class AppServiceFactory extends AbstractServiceFactory
{
    private static final Logger log = Logger.getLogger(AppServiceFactory.class.getName());

    private AppServiceFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class AppServiceFactoryHolder
    {
        private static final AppServiceFactory INSTANCE = new AppServiceFactory();
    }

    // Singleton method
    public static AppServiceFactory getInstance()
    {
        return AppServiceFactoryHolder.INSTANCE;
    }

    @Override
    public ApiConsumerService getApiConsumerService()
    {
        return new ApiConsumerAppService();
    }

    @Override
    public UserService getUserService()
    {
        return new UserAppService();
    }

    @Override
    public RobotsTextFileService getRobotsTextFileService()
    {
        return new RobotsTextFileAppService();
    }

    @Override
    public RobotsTextRefreshService getRobotsTextRefreshService()
    {
        return new RobotsTextRefreshAppService();
    }

    @Override
    public OgProfileService getOgProfileService()
    {
        return new OgProfileAppService();
    }

    @Override
    public OgWebsiteService getOgWebsiteService()
    {
        return new OgWebsiteAppService();
    }

    @Override
    public OgBlogService getOgBlogService()
    {
        return new OgBlogAppService();
    }

    @Override
    public OgArticleService getOgArticleService()
    {
        return new OgArticleAppService();
    }

    @Override
    public OgBookService getOgBookService()
    {
        return new OgBookAppService();
    }

    @Override
    public OgVideoService getOgVideoService()
    {
        return new OgVideoAppService();
    }

    @Override
    public OgMovieService getOgMovieService()
    {
        return new OgMovieAppService();
    }

    @Override
    public OgTvShowService getOgTvShowService()
    {
        return new OgTvShowAppService();
    }

    @Override
    public OgTvEpisodeService getOgTvEpisodeService()
    {
        return new OgTvEpisodeAppService();
    }

    @Override
    public TwitterSummaryCardService getTwitterSummaryCardService()
    {
        return new TwitterSummaryCardAppService();
    }

    @Override
    public TwitterPhotoCardService getTwitterPhotoCardService()
    {
        return new TwitterPhotoCardAppService();
    }

    @Override
    public TwitterGalleryCardService getTwitterGalleryCardService()
    {
        return new TwitterGalleryCardAppService();
    }

    @Override
    public TwitterAppCardService getTwitterAppCardService()
    {
        return new TwitterAppCardAppService();
    }

    @Override
    public TwitterPlayerCardService getTwitterPlayerCardService()
    {
        return new TwitterPlayerCardAppService();
    }

    @Override
    public TwitterProductCardService getTwitterProductCardService()
    {
        return new TwitterProductCardAppService();
    }

    @Override
    public FetchRequestService getFetchRequestService()
    {
        return new FetchRequestAppService();
    }

    @Override
    public PageInfoService getPageInfoService()
    {
        return new PageInfoAppService();
    }

    @Override
    public PageFetchService getPageFetchService()
    {
        return new PageFetchAppService();
    }

    @Override
    public LinkListService getLinkListService()
    {
        return new LinkListAppService();
    }

    @Override
    public ImageSetService getImageSetService()
    {
        return new ImageSetAppService();
    }

    @Override
    public AudioSetService getAudioSetService()
    {
        return new AudioSetAppService();
    }

    @Override
    public VideoSetService getVideoSetService()
    {
        return new VideoSetAppService();
    }

    @Override
    public OpenGraphMetaService getOpenGraphMetaService()
    {
        return new OpenGraphMetaAppService();
    }

    @Override
    public TwitterCardMetaService getTwitterCardMetaService()
    {
        return new TwitterCardMetaAppService();
    }

    @Override
    public DomainInfoService getDomainInfoService()
    {
        return new DomainInfoAppService();
    }

    @Override
    public UrlRatingService getUrlRatingService()
    {
        return new UrlRatingAppService();
    }

    @Override
    public ServiceInfoService getServiceInfoService()
    {
        return new ServiceInfoAppService();
    }

    @Override
    public FiveTenService getFiveTenService()
    {
        return new FiveTenAppService();
    }


}
