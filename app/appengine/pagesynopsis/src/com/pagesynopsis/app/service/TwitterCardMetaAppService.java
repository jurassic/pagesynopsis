package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.TwitterCardMetaService;
import com.pagesynopsis.af.service.impl.TwitterCardMetaServiceImpl;
import com.pagesynopsis.app.fetch.MetadataProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.TwitterCardMeta;


// Updated.
public class TwitterCardMetaAppService extends TwitterCardMetaServiceImpl implements TwitterCardMetaService
{
    private static final Logger log = Logger.getLogger(TwitterCardMetaAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public TwitterCardMetaAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // TwitterCardMeta related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public TwitterCardMeta getTwitterCardMeta(String guid) throws BaseException
    {
        return super.getTwitterCardMeta(guid);
    }

    @Override
    public Object getTwitterCardMeta(String guid, String field) throws BaseException
    {
        return super.getTwitterCardMeta(guid, field);
    }

    @Override
    public List<TwitterCardMeta> getTwitterCardMetas(List<String> guids) throws BaseException
    {
        return super.getTwitterCardMetas(guids);
    }

    @Override
    public List<TwitterCardMeta> getAllTwitterCardMetas() throws BaseException
    {
        return super.getAllTwitterCardMetas();
    }

    @Override
    public List<String> getAllTwitterCardMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllTwitterCardMetaKeys(ordering, offset, count);
    }

    @Override
    public List<TwitterCardMeta> findTwitterCardMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterCardMetas(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findTwitterCardMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findTwitterCardMetaKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("TOP: createTwitterCardMeta()");

        // TBD:
        twitterCardMeta = validateTwitterCardMeta(twitterCardMeta);
        twitterCardMeta = enhanceTwitterCardMeta(twitterCardMeta);
        // ...
        
        // ????
        Integer fefreshStatus = ((TwitterCardMetaBean) twitterCardMeta).getRefreshStatus();
        if(fefreshStatus == null) {
            ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processTwitterCardMeta().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = twitterCardMeta.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                twitterCardMeta = processTwitterCardMeta(twitterCardMeta);
                if(twitterCardMeta != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: twitterCardMeta = " + twitterCardMeta);
                    // Need to to do this to avoid double processing.
                    ((TwitterCardMetaBean) twitterCardMeta).setDeferred(true);
                    // ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...
        

        String guid = super.createTwitterCardMeta(twitterCardMeta);

        log.finer("BOTTOM: createTwitterCardMeta(): guid = " + guid);
        return guid;
    }

    @Override
    public TwitterCardMeta constructTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("TOP: constructTwitterCardMeta()");

        // TBD:
        twitterCardMeta = validateTwitterCardMeta(twitterCardMeta);
        twitterCardMeta = enhanceTwitterCardMeta(twitterCardMeta);
        // ...
        
        // ????
        Integer fefreshStatus = ((TwitterCardMetaBean) twitterCardMeta).getRefreshStatus();
        if(fefreshStatus == null) {
            ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processTwitterCardMeta().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = twitterCardMeta.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                twitterCardMeta = processTwitterCardMeta(twitterCardMeta);
                if(twitterCardMeta != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: twitterCardMeta = " + twitterCardMeta);
                    // Need to to do this to avoid double processing.
                    ((TwitterCardMetaBean) twitterCardMeta).setDeferred(true);
                    // ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...

        
        twitterCardMeta = super.constructTwitterCardMeta(twitterCardMeta);
        
        log.finer("BOTTOM: constructTwitterCardMeta(): twitterCardMeta = " + twitterCardMeta);
        return twitterCardMeta;
    }


    @Override
    public Boolean updateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("TOP: updateTwitterCardMeta()");

        // TBD:
        twitterCardMeta = validateTwitterCardMeta(twitterCardMeta);
        Boolean suc = super.updateTwitterCardMeta(twitterCardMeta);

        log.finer("BOTTOM: createTwitterCardMeta(): suc = " + suc);
        return suc;
    }
        
    @Override
    public TwitterCardMeta refreshTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("TOP: refreshTwitterCardMeta()");

        // TBD:
        twitterCardMeta = validateTwitterCardMeta(twitterCardMeta);
        twitterCardMeta = super.refreshTwitterCardMeta(twitterCardMeta);
        
        log.finer("BOTTOM: refreshTwitterCardMeta(): twitterCardMeta = " + twitterCardMeta);
        return twitterCardMeta;
    }

    
    @Override
    public Boolean deleteTwitterCardMeta(String guid) throws BaseException
    {
        return super.deleteTwitterCardMeta(guid);
    }

    @Override
    public Boolean deleteTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        return super.deleteTwitterCardMeta(twitterCardMeta);
    }

    @Override
    public Integer createTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    {
        return super.createTwitterCardMetas(twitterCardMetas);
    }

    // TBD
    //@Override
    //public Boolean updateTwitterCardMetas(List<TwitterCardMeta> twitterCardMetas) throws BaseException
    //{
    //}

    
    
    // TBD...
    private TwitterCardMetaBean validateTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        TwitterCardMetaBean bean = null;
        if(twitterCardMeta instanceof TwitterCardMetaBean) {
            bean = (TwitterCardMetaBean) twitterCardMeta;
        }
        // else ???
        
        // ...
        String targetUrl =  twitterCardMeta.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = twitterCardMeta.getPageUrl();
        String query = twitterCardMeta.getQueryString();
        List<KeyValuePairStruct> queryParams = twitterCardMeta.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = twitterCardMeta.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private TwitterCardMeta enhanceTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return twitterCardMeta;
    }        

    private TwitterCardMeta processTwitterCardMeta(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        log.finer("TOP: processTwitterCardMeta()");
        // ...
        twitterCardMeta = MetadataProcessor.getInstance().processTwitterCardMetaFetch(twitterCardMeta);
        // ...   

        log.finer("BOTTOM: processTwitterCardMeta()");
        return twitterCardMeta;
    }


    
    
}
