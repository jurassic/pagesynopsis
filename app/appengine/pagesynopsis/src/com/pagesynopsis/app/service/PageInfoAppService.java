package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.PageInfoBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.PageInfoService;
import com.pagesynopsis.af.service.impl.PageInfoServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.PageInfo;


// Updated.
public class PageInfoAppService extends PageInfoServiceImpl implements PageInfoService
{
    private static final Logger log = Logger.getLogger(PageInfoAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public PageInfoAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // PageInfo related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public PageInfo getPageInfo(String guid) throws BaseException
    {
        return super.getPageInfo(guid);
    }

    @Override
    public Object getPageInfo(String guid, String field) throws BaseException
    {
        return super.getPageInfo(guid, field);
    }

    @Override
    public List<PageInfo> getPageInfos(List<String> guids) throws BaseException
    {
        return super.getPageInfos(guids);
    }

    @Override
    public List<PageInfo> getAllPageInfos() throws BaseException
    {
        return super.getAllPageInfos();
    }

    @Override
    public List<String> getAllPageInfoKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllPageInfoKeys(ordering, offset, count);
    }

    @Override
    public List<PageInfo> findPageInfos(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findPageInfos(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findPageInfoKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findPageInfoKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createPageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("TOP: createPageInfo()");

        // TBD:
        pageInfo = validatePageInfo(pageInfo);
        pageInfo = enhancePageInfo(pageInfo);
        // ...
        
        // ????
        Integer fefreshStatus = ((PageInfoBean) pageInfo).getRefreshStatus();
        if(fefreshStatus == null) {
            ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processPageInfo().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = pageInfo.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                pageInfo = processPageInfo(pageInfo);
                if(pageInfo != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: pageInfo = " + pageInfo);
                    // Need to to do this to avoid double processing.
                    ((PageInfoBean) pageInfo).setDeferred(true);
                    // ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...

        
        String guid = super.createPageInfo(pageInfo);

        log.finer("BOTTOM: createPageInfo(): guid = " + guid);
        return guid;
    }

    @Override
    public PageInfo constructPageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("TOP: constructPageInfo()");

        // TBD:
        pageInfo = validatePageInfo(pageInfo);
        pageInfo = enhancePageInfo(pageInfo);
        // ...
        
        // ????
        Integer fefreshStatus = ((PageInfoBean) pageInfo).getRefreshStatus();
        if(fefreshStatus == null) {
            ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processPageInfo().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = pageInfo.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                pageInfo = processPageInfo(pageInfo);
                if(pageInfo != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: pageInfo = " + pageInfo);
                    // Need to to do this to avoid double processing.
                    ((PageInfoBean) pageInfo).setDeferred(true);
                    // ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((PageInfoBean) pageInfo).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        pageInfo = super.constructPageInfo(pageInfo);
        
        log.finer("BOTTOM: constructPageInfo(): pageInfo = " + pageInfo);
        return pageInfo;
    }


    @Override
    public Boolean updatePageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("TOP: updatePageInfo()");

        // TBD:
        pageInfo = validatePageInfo(pageInfo);
        Boolean suc = super.updatePageInfo(pageInfo);

        log.finer("BOTTOM: createPageInfo(): suc = " + suc);
        return suc;
    }
        
    @Override
    public PageInfo refreshPageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("TOP: refreshPageInfo()");

        // TBD:
        pageInfo = validatePageInfo(pageInfo);
        pageInfo = super.refreshPageInfo(pageInfo);
        
        log.finer("BOTTOM: refreshPageInfo(): pageInfo = " + pageInfo);
        return pageInfo;
    }

    
    @Override
    public Boolean deletePageInfo(String guid) throws BaseException
    {
        return super.deletePageInfo(guid);
    }

    @Override
    public Boolean deletePageInfo(PageInfo pageInfo) throws BaseException
    {
        return super.deletePageInfo(pageInfo);
    }

    @Override
    public Integer createPageInfos(List<PageInfo> pageInfos) throws BaseException
    {
        return super.createPageInfos(pageInfos);
    }

    // TBD
    //@Override
    //public Boolean updatePageInfos(List<PageInfo> pageInfos) throws BaseException
    //{
    //}

    
    
    // TBD...
    private PageInfoBean validatePageInfo(PageInfo pageInfo) throws BaseException
    {
        PageInfoBean bean = null;
        if(pageInfo instanceof PageInfoBean) {
            bean = (PageInfoBean) pageInfo;
        }
        // else ???
        
        // ...
        String targetUrl =  pageInfo.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = pageInfo.getPageUrl();
        String query = pageInfo.getQueryString();
        List<KeyValuePairStruct> queryParams = pageInfo.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = pageInfo.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private PageInfo enhancePageInfo(PageInfo pageInfo) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return pageInfo;
    }        

    private PageInfo processPageInfo(PageInfo pageInfo) throws BaseException
    {
        log.finer("TOP: processPageInfo()");
        // ...
        pageInfo = FetchProcessor.getInstance().processPageInfoFetch(pageInfo);
        // ...   

        log.finer("BOTTOM: processPageInfo()");
        return pageInfo;
    }


    
    
}
