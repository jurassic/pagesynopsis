package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.FetchRequestBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.FetchRequestService;
import com.pagesynopsis.af.service.impl.FetchRequestServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.KeyValuePairStruct;


// Updated.
public class FetchRequestAppService extends FetchRequestServiceImpl implements FetchRequestService
{
    private static final Logger log = Logger.getLogger(FetchRequestAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public FetchRequestAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // FetchRequest related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public FetchRequest getFetchRequest(String guid) throws BaseException
    {
        return super.getFetchRequest(guid);
    }

    @Override
    public Object getFetchRequest(String guid, String field) throws BaseException
    {
        return super.getFetchRequest(guid, field);
    }

    @Override
    public List<FetchRequest> getFetchRequests(List<String> guids) throws BaseException
    {
        return super.getFetchRequests(guids);
    }

    @Override
    public List<FetchRequest> getAllFetchRequests() throws BaseException
    {
        return super.getAllFetchRequests();
    }

    @Override
    public List<String> getAllFetchRequestKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllFetchRequestKeys(ordering, offset, count);
    }

    @Override
    public List<FetchRequest> findFetchRequests(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFetchRequests(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findFetchRequestKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findFetchRequestKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    
    // TBD:
    // Place holder for now...
    
    
    @Override
    public String createFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: createFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        fetchRequest = enhanceFetchRequest(fetchRequest);
        // ...
        
        // ????
        Integer fetchStatus = ((FetchRequestBean) fetchRequest).getFetchStatus();
        if(fetchStatus == null) {
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_CREATED);
        }
        // ...

        Boolean isDeferred = fetchRequest.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                fetchRequest = processFetchRequest(fetchRequest);
                if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: fetchRequest = " + fetchRequest);
                // ...
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_SCHEDULED);
        }
        // ...
        
        String guid = super.createFetchRequest(fetchRequest);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createFetchRequest(): guid = " + guid);
        return guid;
    }

    @Override
    public FetchRequest constructFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: constructFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        fetchRequest = enhanceFetchRequest(fetchRequest);
        // ...
        
        // ????
        Integer fetchStatus = ((FetchRequestBean) fetchRequest).getFetchStatus();
        if(fetchStatus == null) {
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_CREATED);
        }
        // ...

        Boolean isDeferred = fetchRequest.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                fetchRequest = processFetchRequest(fetchRequest);
                if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: fetchRequest = " + fetchRequest);
                // ...
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
                // change it to deferred and try again later?
                // or, throw exception here???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((FetchRequestBean) fetchRequest).setFetchStatus(FetchStatus.STATUS_SCHEDULED);
        }
        // ...
                
        fetchRequest = super.constructFetchRequest(fetchRequest);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: constructFetchRequest(): fetchRequest = " + fetchRequest);
        return fetchRequest;
    }


    @Override
    public Boolean updateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: updateFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        // ...

 
        Boolean suc = super.updateFetchRequest(fetchRequest);

        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: createFetchRequest(): suc = " + suc);
        return suc;
    }
        
    @Override
    public FetchRequest refreshFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: refreshFetchRequest()");

        // TBD:
        fetchRequest = validateFetchRequest(fetchRequest);
        // ...

        
        fetchRequest = super.refreshFetchRequest(fetchRequest);
        
        if(log.isLoggable(Level.FINER)) log.finer("BOTTOM: refreshFetchRequest(): fetchRequest = " + fetchRequest);
        return fetchRequest;
    }

    @Override
    public Boolean deleteFetchRequest(String guid) throws BaseException
    {
        return super.deleteFetchRequest(guid);
    }

    @Override
    public Boolean deleteFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        return super.deleteFetchRequest(fetchRequest);
    }

    @Override
    public Integer createFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    {
        return super.createFetchRequests(fetchRequests);
    }

    // TBD
    //@Override
    //public Boolean updateFetchRequests(List<FetchRequest> fetchRequests) throws BaseException
    //{
    //}

    
    
    // TBD:
    // Place holder for now...
    

    // TBD...
    private FetchRequestBean validateFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        FetchRequestBean bean = null;
        if(fetchRequest instanceof FetchRequestBean) {
            bean = (FetchRequestBean) fetchRequest;
        }
        // else ???
        
        // ...
        String targetUrl =  bean.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        }
        
        // TBD: 
        String pageUrl = bean.getPageUrl();
        String queryString = bean.getQueryString();
        // TBD
        List<KeyValuePairStruct> queryParameters = bean.getQueryParams();
        //
        
        
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = fetchRequest.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private FetchRequest enhanceFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        
        // TBD:
        
        // Create shortlink, etc...
        // ....
        
        
        return fetchRequest;
    }        

    private FetchRequest processFetchRequest(FetchRequest fetchRequest) throws BaseException
    {
        log.finer("TOP: processFetchRequest()");
        // ...
        // Note that FetchRequest needs to update/create both fetchRequest and pageInfo....  
        // ...
        fetchRequest = FetchProcessor.getInstance().processPageFetch(fetchRequest);
        // ...   

        log.finer("BOTTOM: processFetchRequest()");
        return fetchRequest;
    }


}
