package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.DeferredTask;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.RetryOptions;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.impl.RobotsTextFileServiceImpl;
import com.pagesynopsis.af.service.impl.RobotsTextRefreshServiceImpl;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.util.RobotsTextConstants;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.core.GUID;


// Updated.
public class RobotsTextFileAppService extends RobotsTextFileServiceImpl implements RobotsTextFileService
{
    private static final Logger log = Logger.getLogger(RobotsTextFileAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }

    public RobotsTextFileAppService()
    {
         super();
    }

    //  
    private RobotsTextRefreshService robotsTextRefreshService = null;
    private RobotsTextRefreshService getRobotsTextRefreshService()
    {
        if(robotsTextRefreshService == null) {
            robotsTextRefreshService = new RobotsTextRefreshServiceImpl();
        }
        return robotsTextRefreshService;
    }
    


    //////////////////////////////////////////////////////////////////////////
    // RobotsTextFile related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public RobotsTextFile getRobotsTextFile(String guid) throws BaseException
    {
        return super.getRobotsTextFile(guid);
    }

    @Override
    public Object getRobotsTextFile(String guid, String field) throws BaseException
    {
        return super.getRobotsTextFile(guid, field);
    }

    @Override
    public List<RobotsTextFile> getRobotsTextFiles(List<String> guids) throws BaseException
    {
        return super.getRobotsTextFiles(guids);
    }

    @Override
    public List<RobotsTextFile> getAllRobotsTextFiles() throws BaseException
    {
        return super.getAllRobotsTextFiles();
    }

    @Override
    public List<String> getAllRobotsTextFileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllRobotsTextFileKeys(ordering, offset, count);
    }

    @Override
    public List<RobotsTextFile> findRobotsTextFiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findRobotsTextFiles(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findRobotsTextFileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findRobotsTextFileKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("TOP: createRobotsTextFile()");

        // TBD:
        robotsTextFile = validateRobotsTextFile(robotsTextFile);
        robotsTextFile = enhanceRobotsTextFile(robotsTextFile);
        robotsTextFile = processRobotsTextFile(robotsTextFile, true);
        // ...
                
        String guid = super.createRobotsTextFile(robotsTextFile);

        log.finer("BOTTOM: createRobotsTextFile(): guid = " + guid);
        return guid;
    }

    @Override
    public RobotsTextFile constructRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("TOP: constructRobotsTextFile()");

        // TBD:
        robotsTextFile = validateRobotsTextFile(robotsTextFile);
        robotsTextFile = enhanceRobotsTextFile(robotsTextFile);
        robotsTextFile = processRobotsTextFile(robotsTextFile, true);
        // ...
                        
        robotsTextFile = super.constructRobotsTextFile(robotsTextFile);
        
        log.finer("BOTTOM: constructRobotsTextFile(): robotsTextFile = " + robotsTextFile);
        return robotsTextFile;
    }


    @Override
    public Boolean updateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("TOP: updateRobotsTextFile()");

        // TBD:
        robotsTextFile = validateRobotsTextFile(robotsTextFile);
        robotsTextFile = processRobotsTextFile(robotsTextFile, false);
        Boolean suc = super.updateRobotsTextFile(robotsTextFile);

        log.finer("BOTTOM: createRobotsTextFile(): suc = " + suc);
        return suc;
    }
        
    @Override
    public RobotsTextFile refreshRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        log.finer("TOP: refreshRobotsTextFile()");

        // TBD:
        robotsTextFile = validateRobotsTextFile(robotsTextFile);
        robotsTextFile = processRobotsTextFile(robotsTextFile, false);
        robotsTextFile = super.refreshRobotsTextFile(robotsTextFile);
        
        log.finer("BOTTOM: refreshRobotsTextFile(): robotsTextFile = " + robotsTextFile);
        return robotsTextFile;
    }

    
    @Override
    public Boolean deleteRobotsTextFile(String guid) throws BaseException
    {
        return super.deleteRobotsTextFile(guid);
    }

    @Override
    public Boolean deleteRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        return super.deleteRobotsTextFile(robotsTextFile);
    }

    @Override
    public Integer createRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    {
        return super.createRobotsTextFiles(robotsTextFiles);
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextFiles(List<RobotsTextFile> robotsTextFiles) throws BaseException
    //{
    //}

    
    
    // TBD...
    private RobotsTextFileBean validateRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        }
        // else ???
        
        String guid = bean.getGuid();
        if(guid == null || guid.isEmpty()) {
            guid = GUID.generate();
            bean.setGuid(guid);
        }
        
        // ...
        String siteUrl =  bean.getSiteUrl();
        if(siteUrl == null || siteUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Site Url is null or empty. Cannot proceed.");
        } else if(siteUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Site Url is too long. Cannot proceed. siteUrl = " + siteUrl);            
        }
                
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private RobotsTextFile enhanceRobotsTextFile(RobotsTextFile robotsTextFile) throws BaseException
    {        
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        }
        // else ???
        
        Long createdTime = bean.getCreatedTime();
        if(createdTime == null || createdTime == 0L) {
            createdTime = System.currentTimeMillis();
            bean.setCreatedTime(createdTime);
        }
        

        // TBD:


        return bean;
    }        

    private RobotsTextFile processRobotsTextFile(RobotsTextFile robotsTextFile, boolean creating)
    {
        log.finer("TOP: processRobotsTextFile(). creating = " + creating);
        RobotsTextFileBean bean = null;
        if(robotsTextFile instanceof RobotsTextFileBean) {
            bean = (RobotsTextFileBean) robotsTextFile;
        }
        // else ???
        
        // Publish RSS feed here???
        try {
            DeferredTask task = new DelayedRefreshTask(robotsTextFile, creating);
            RetryOptions retryOptions = RetryOptions.Builder.withTaskRetryLimit(5);   // Max 5 retries...
            TaskOptions taskOptions = TaskOptions.Builder.withPayload(task).retryOptions(retryOptions).countdownMillis(3333L);  // 3 second delay...
            Queue queue = QueueFactory.getDefaultQueue();
            queue.add(taskOptions);
        } catch(Exception e) {
            log.log(Level.WARNING, "DelayedRefreshTask failed.", e);
        }

        log.finer("BOTTOM: processRobotsTextFile()");
        return robotsTextFile;
    }

    
    // Using TaskQueue...
    private static final class DelayedRefreshTask implements DeferredTask 
    {
        private static final long serialVersionUID = 1L;

        private RobotsTextFile mRobotsTextFile;
        private boolean mCreating;
//        public DelayedRefreshTask(RobotsTextFile robotsTextFile) 
//        {
//            this(robotsTextFile, false);
//        }
        public DelayedRefreshTask(RobotsTextFile robotsTextFile, boolean creating) 
        {
            mRobotsTextFile = robotsTextFile;
            mCreating = creating;
        }
        //  
        private RobotsTextRefreshService robotsTextRefreshService = null;
        private RobotsTextRefreshService getRobotsTextRefreshService()
        {
            if(robotsTextRefreshService == null) {
                robotsTextRefreshService = new RobotsTextRefreshServiceImpl();
            }
            return robotsTextRefreshService;
        }

        @Override
        public void run()
        {
            log.fine("DelayedRefreshTask.run().");

            if(mRobotsTextFile == null) {
                log.warning("DelayedRefreshTask failed because mRobotsTextFile is null.");
                return;
            }
            
            // This cannot be null...
            String fileGuid = mRobotsTextFile.getGuid();
            
            // Now
            long now = System.currentTimeMillis();

            // Note:
            // We do not actually "process" the mRobotsTextFile....
            // This has been done BEFORE it's saved/updated...
            // Here, we create/update RobotsTextRefresh, which is in turn responsible for actual periodic processing...
            // ....
            
            try {
                RobotsTextRefreshBean robotsTextRefresh = null;
                if(mCreating) {
                    robotsTextRefresh = new RobotsTextRefreshBean();
                    robotsTextRefresh.setGuid(GUID.generate());
                    robotsTextRefresh.setRobotsTextFile(fileGuid);
                    robotsTextRefresh.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    robotsTextRefresh.setRefreshInterval(RobotsTextConstants.REFRESH_INTERVAL);
                    robotsTextRefresh.setExpirationTime(now + RobotsTextConstants.EXPIRATION_TIME_MILLIS);
                    // ...
                    
                    try {
                        robotsTextRefresh = (RobotsTextRefreshBean) getRobotsTextRefreshService().constructRobotsTextRefresh(robotsTextRefresh);
                    } catch(BaseException e) {
                        // ignore...
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to save robotsTextRefresh for robotsTextFile = " + fileGuid, e);
                    }
                } else {
                    try {
                        // Find the existing robotsTextRefresh.
                        // If this fails, just ignore and move on....
                        String filter = "robotsTextFile=='" + fileGuid + "'";
                        // String ordering = "createdTime desc";
                        String ordering = null;
                        long offset = 0L;
                        int count = 1;
                        List<String> keys = getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, null, null, null, null, offset, count);
                        if(keys != null && !keys.isEmpty()) {
                            robotsTextRefresh = (RobotsTextRefreshBean) getRobotsTextRefreshService().getRobotsTextRefresh(keys.get(0));
                        }
                    } catch(BaseException e) {
                        // ignore...
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch robotsTextRefresh for robotsTextFile = " + fileGuid, e);
                    }

                    if(robotsTextRefresh != null) {
                        // Extend the expiration time...
                        robotsTextRefresh.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                        robotsTextRefresh.setExpirationTime(now + RobotsTextConstants.EXPIRATION_TIME_MILLIS);
                        // ....
        
                        try {
                            robotsTextRefresh = (RobotsTextRefreshBean) getRobotsTextRefreshService().refreshRobotsTextRefresh(robotsTextRefresh);
                        } catch(BaseException e) {
                            // ignore...
                            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to update robotsTextRefresh for robotsTextFile = " + fileGuid, e);
                        }
                    } else {
                        // Ignore..
                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "No robotsTextRefresh found for robotsTextFile = " + fileGuid);
                    }
                }
                
                if(log.isLoggable(Level.INFO)) {
                    log.info("DelayedRefreshTask(): robotsTextRefresh = " + robotsTextRefresh);
                }
            } catch(Exception e) {
                // Ignore...
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to create/update robotsTextRefresh due to unknown reason: robotsTextFile = " + fileGuid, e);
            }
        }
    }

    
    
}
