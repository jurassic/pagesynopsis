package com.pagesynopsis.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgProfileService;
import com.pagesynopsis.af.service.impl.OgProfileServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgProfileAppService extends OgProfileServiceImpl implements OgProfileService
{
    private static final Logger log = Logger.getLogger(OgProfileAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgProfileAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgProfile related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgProfile getOgProfile(String guid) throws BaseException
    {
        return super.getOgProfile(guid);
    }

    @Override
    public Object getOgProfile(String guid, String field) throws BaseException
    {
        return super.getOgProfile(guid, field);
    }

    @Override
    public List<OgProfile> getOgProfiles(List<String> guids) throws BaseException
    {
        return super.getOgProfiles(guids);
    }

    @Override
    public List<OgProfile> getAllOgProfiles() throws BaseException
    {
        return super.getAllOgProfiles();
    }

    @Override
    public List<String> getAllOgProfileKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllOgProfileKeys(ordering, offset, count);
    }

    @Override
    public List<OgProfile> findOgProfiles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgProfiles(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findOgProfileKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgProfileKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgProfile(OgProfile ogProfile) throws BaseException
    {
        return super.createOgProfile(ogProfile);
    }

    @Override
    public OgProfile constructOgProfile(OgProfile ogProfile) throws BaseException
    {
        return super.constructOgProfile(ogProfile);
    }


    @Override
    public Boolean updateOgProfile(OgProfile ogProfile) throws BaseException
    {
        return super.updateOgProfile(ogProfile);
    }
        
    @Override
    public OgProfile refreshOgProfile(OgProfile ogProfile) throws BaseException
    {
        return super.refreshOgProfile(ogProfile);
    }

    @Override
    public Boolean deleteOgProfile(String guid) throws BaseException
    {
        return super.deleteOgProfile(guid);
    }

    @Override
    public Boolean deleteOgProfile(OgProfile ogProfile) throws BaseException
    {
        return super.deleteOgProfile(ogProfile);
    }

    @Override
    public Integer createOgProfiles(List<OgProfile> ogProfiles) throws BaseException
    {
        return super.createOgProfiles(ogProfiles);
    }

    // TBD
    //@Override
    //public Boolean updateOgProfiles(List<OgProfile> ogProfiles) throws BaseException
    //{
    //}

}
