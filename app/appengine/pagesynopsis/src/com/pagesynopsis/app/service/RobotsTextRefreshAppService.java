package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.RobotsTextRefreshBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.RobotsTextFileService;
import com.pagesynopsis.af.service.RobotsTextRefreshService;
import com.pagesynopsis.af.service.impl.RobotsTextFileServiceImpl;
import com.pagesynopsis.af.service.impl.RobotsTextRefreshServiceImpl;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextRefresh;


// Updated.
public class RobotsTextRefreshAppService extends RobotsTextRefreshServiceImpl implements RobotsTextRefreshService
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }

    public RobotsTextRefreshAppService()
    {
         super();
    }

    //  
    private RobotsTextFileService robotsTextFileService = null;
    private RobotsTextFileService getRobotsTextFileService()
    {
        if(robotsTextFileService == null) {
            robotsTextFileService = new RobotsTextFileServiceImpl();
        }
        return robotsTextFileService;
    }
    
    

    //////////////////////////////////////////////////////////////////////////
    // RobotsTextRefresh related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public RobotsTextRefresh getRobotsTextRefresh(String guid) throws BaseException
    {
        return super.getRobotsTextRefresh(guid);
    }

    @Override
    public Object getRobotsTextRefresh(String guid, String field) throws BaseException
    {
        return super.getRobotsTextRefresh(guid, field);
    }

    @Override
    public List<RobotsTextRefresh> getRobotsTextRefreshes(List<String> guids) throws BaseException
    {
        return super.getRobotsTextRefreshes(guids);
    }

    @Override
    public List<RobotsTextRefresh> getAllRobotsTextRefreshes() throws BaseException
    {
        return super.getAllRobotsTextRefreshes();
    }

    @Override
    public List<String> getAllRobotsTextRefreshKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllRobotsTextRefreshKeys(ordering, offset, count);
    }

    @Override
    public List<RobotsTextRefresh> findRobotsTextRefreshes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findRobotsTextRefreshes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findRobotsTextRefreshKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findRobotsTextRefreshKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("TOP: createRobotsTextRefresh()");

        // TBD:
        robotsTextRefresh = validateRobotsTextRefresh(robotsTextRefresh);
        robotsTextRefresh = enhanceRobotsTextRefresh(robotsTextRefresh);
        // ...
        
//        // ????
//        Integer fefreshStatus = ((RobotsTextRefreshBean) robotsTextRefresh).getRefreshStatus();
//        if(fefreshStatus == null) {
//            ((RobotsTextRefreshBean) robotsTextRefresh).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
//        }
//        // ...

        // Note:
        // POST/PUT should not call processRobotsTextRefresh().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // ...


//        Boolean isDeferred = robotsTextRefresh.isDeferred();
//        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
//            log.fine("Fetch to be procssed in a non-deferred mode.");
//            try {
//                robotsTextRefresh = processRobotsTextRefresh(robotsTextRefresh);
//                log.info("Fetch processed in a non-deferred mode: robotsTextRefresh = " + robotsTextRefresh);
//                // ...
//                // ...
//            } catch(Exception e) {
//                log.log(Level.WARNING, "Failed to process fetch", e);
//                // ???
//                // ...
//            }
//        } else {
//            // "Deferred" cron will process the fetch
//            ((RobotsTextRefreshBean) robotsTextRefresh).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
//        }
//        // ...
        
        String guid = super.createRobotsTextRefresh(robotsTextRefresh);

        log.finer("BOTTOM: createRobotsTextRefresh(): guid = " + guid);
        return guid;
    }

    @Override
    public RobotsTextRefresh constructRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("TOP: constructRobotsTextRefresh()");

        // TBD:
        robotsTextRefresh = validateRobotsTextRefresh(robotsTextRefresh);
        robotsTextRefresh = enhanceRobotsTextRefresh(robotsTextRefresh);
        // ...
        
//        // ????
//        Integer fefreshStatus = ((RobotsTextRefreshBean) robotsTextRefresh).getRefreshStatus();
//        if(fefreshStatus == null) {
//            ((RobotsTextRefreshBean) robotsTextRefresh).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
//        }
//        // ...
        
        
        // Note:
        // POST/PUT should not call processRobotsTextRefresh().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // ...

        
//        Boolean isDeferred = robotsTextRefresh.isDeferred();
//        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
//            log.fine("Fetch to be procssed in a non-deferred mode.");
//            try {
//                robotsTextRefresh = processRobotsTextRefresh(robotsTextRefresh);
//                log.info("Fetch processed in a non-deferred mode: robotsTextRefresh = " + robotsTextRefresh);
//                // ...
//                // ...
//            } catch(Exception e) {
//                log.log(Level.WARNING, "Failed to process fetch", e);
//                // ???
//                // ...
//                // change it to deferred and try again later?
//                // or, throw exception here???
//                // ...
//            }
//        } else {
//            // "Deferred" cron will process the fetch
//            ((RobotsTextRefreshBean) robotsTextRefresh).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
//        }
//        // ...
                
        robotsTextRefresh = super.constructRobotsTextRefresh(robotsTextRefresh);
        
        log.finer("BOTTOM: constructRobotsTextRefresh(): robotsTextRefresh = " + robotsTextRefresh);
        return robotsTextRefresh;
    }


    @Override
    public Boolean updateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("TOP: updateRobotsTextRefresh()");

        // TBD:
        robotsTextRefresh = validateRobotsTextRefresh(robotsTextRefresh);
        Boolean suc = super.updateRobotsTextRefresh(robotsTextRefresh);

        log.finer("BOTTOM: createRobotsTextRefresh(): suc = " + suc);
        return suc;
    }
        
    @Override
    public RobotsTextRefresh refreshRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("TOP: refreshRobotsTextRefresh()");

        // TBD:
        robotsTextRefresh = validateRobotsTextRefresh(robotsTextRefresh);
        robotsTextRefresh = super.refreshRobotsTextRefresh(robotsTextRefresh);
        
        log.finer("BOTTOM: refreshRobotsTextRefresh(): robotsTextRefresh = " + robotsTextRefresh);
        return robotsTextRefresh;
    }

    
    @Override
    public Boolean deleteRobotsTextRefresh(String guid) throws BaseException
    {
        return super.deleteRobotsTextRefresh(guid);
    }

    @Override
    public Boolean deleteRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        return super.deleteRobotsTextRefresh(robotsTextRefresh);
    }

    @Override
    public Integer createRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    {
        return super.createRobotsTextRefreshes(robotsTextRefreshes);
    }

    // TBD
    //@Override
    //public Boolean updateRobotsTextRefreshes(List<RobotsTextRefresh> robotsTextRefreshes) throws BaseException
    //{
    //}

    
    
    // TBD...
    private RobotsTextRefreshBean validateRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        RobotsTextRefreshBean bean = null;
        if(robotsTextRefresh instanceof RobotsTextRefreshBean) {
            bean = (RobotsTextRefreshBean) robotsTextRefresh;
        }
        // else ???
        
//        // ...
//        String targetUrl =  robotsTextRefresh.getTargetUrl();
//        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
//        } else if(targetUrl.length() > 500) {
//            // ??? Truncate or error?
//            // TBD ....
//            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
//        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private RobotsTextRefresh enhanceRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return robotsTextRefresh;
    }        

    private RobotsTextRefresh processRobotsTextRefresh(RobotsTextRefresh robotsTextRefresh) throws BaseException
    {
        log.finer("TOP: processRobotsTextRefresh()");
        // ...
        //robotsTextRefresh = RobotsTextProcessor.getInstance().processRobotsTextFetch(robotsTextRefresh);
        // ...   

        log.finer("BOTTOM: processRobotsTextRefresh()");
        return robotsTextRefresh;
    }


    
    
}
