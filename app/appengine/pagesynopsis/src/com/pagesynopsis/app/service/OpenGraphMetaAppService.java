package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.OpenGraphMetaService;
import com.pagesynopsis.af.service.impl.OpenGraphMetaServiceImpl;
import com.pagesynopsis.app.fetch.MetadataProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.OpenGraphMeta;


// Updated.
public class OpenGraphMetaAppService extends OpenGraphMetaServiceImpl implements OpenGraphMetaService
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OpenGraphMetaAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OpenGraphMeta related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OpenGraphMeta getOpenGraphMeta(String guid) throws BaseException
    {
        return super.getOpenGraphMeta(guid);
    }

    @Override
    public Object getOpenGraphMeta(String guid, String field) throws BaseException
    {
        return super.getOpenGraphMeta(guid, field);
    }

    @Override
    public List<OpenGraphMeta> getOpenGraphMetas(List<String> guids) throws BaseException
    {
        return super.getOpenGraphMetas(guids);
    }

    @Override
    public List<OpenGraphMeta> getAllOpenGraphMetas() throws BaseException
    {
        return super.getAllOpenGraphMetas();
    }

    @Override
    public List<String> getAllOpenGraphMetaKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllOpenGraphMetaKeys(ordering, offset, count);
    }

    @Override
    public List<OpenGraphMeta> findOpenGraphMetas(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOpenGraphMetas(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findOpenGraphMetaKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOpenGraphMetaKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("TOP: createOpenGraphMeta()");

        // TBD:
        openGraphMeta = validateOpenGraphMeta(openGraphMeta);
        openGraphMeta = enhanceOpenGraphMeta(openGraphMeta);
        // ...
        
        // ????
        Integer fefreshStatus = ((OpenGraphMetaBean) openGraphMeta).getRefreshStatus();
        if(fefreshStatus == null) {
            ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processOpenGraphMeta().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = openGraphMeta.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                openGraphMeta = processOpenGraphMeta(openGraphMeta);
                if(openGraphMeta != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: openGraphMeta = " + openGraphMeta);
                    // Need to to do this to avoid double processing.
                    ((OpenGraphMetaBean) openGraphMeta).setDeferred(true);
                    // ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...
        

        String guid = super.createOpenGraphMeta(openGraphMeta);

        log.finer("BOTTOM: createOpenGraphMeta(): guid = " + guid);
        return guid;
    }

    @Override
    public OpenGraphMeta constructOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("TOP: constructOpenGraphMeta()");

        // TBD:
        openGraphMeta = validateOpenGraphMeta(openGraphMeta);
        openGraphMeta = enhanceOpenGraphMeta(openGraphMeta);
        // ...
        
        // ????
        Integer fefreshStatus = ((OpenGraphMetaBean) openGraphMeta).getRefreshStatus();
        if(fefreshStatus == null) {
            ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processOpenGraphMeta().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = openGraphMeta.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                openGraphMeta = processOpenGraphMeta(openGraphMeta);
                if(openGraphMeta != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: openGraphMeta = " + openGraphMeta);
                    // Need to to do this to avoid double processing.
                    ((OpenGraphMetaBean) openGraphMeta).setDeferred(true);
                    // ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...
                
   
        openGraphMeta = super.constructOpenGraphMeta(openGraphMeta);
        
        log.finer("BOTTOM: constructOpenGraphMeta(): openGraphMeta = " + openGraphMeta);
        return openGraphMeta;
    }


    @Override
    public Boolean updateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("TOP: updateOpenGraphMeta()");

        // TBD:
        openGraphMeta = validateOpenGraphMeta(openGraphMeta);
        Boolean suc = super.updateOpenGraphMeta(openGraphMeta);

        log.finer("BOTTOM: createOpenGraphMeta(): suc = " + suc);
        return suc;
    }
        
    @Override
    public OpenGraphMeta refreshOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("TOP: refreshOpenGraphMeta()");

        // TBD:
        openGraphMeta = validateOpenGraphMeta(openGraphMeta);
        openGraphMeta = super.refreshOpenGraphMeta(openGraphMeta);
        
        log.finer("BOTTOM: refreshOpenGraphMeta(): openGraphMeta = " + openGraphMeta);
        return openGraphMeta;
    }

    
    @Override
    public Boolean deleteOpenGraphMeta(String guid) throws BaseException
    {
        return super.deleteOpenGraphMeta(guid);
    }

    @Override
    public Boolean deleteOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        return super.deleteOpenGraphMeta(openGraphMeta);
    }

    @Override
    public Integer createOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    {
        return super.createOpenGraphMetas(openGraphMetas);
    }

    // TBD
    //@Override
    //public Boolean updateOpenGraphMetas(List<OpenGraphMeta> openGraphMetas) throws BaseException
    //{
    //}

    
    
    // TBD...
    private OpenGraphMetaBean validateOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        OpenGraphMetaBean bean = null;
        if(openGraphMeta instanceof OpenGraphMetaBean) {
            bean = (OpenGraphMetaBean) openGraphMeta;
        }
        // else ???
        
        // ...
        String targetUrl =  openGraphMeta.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = openGraphMeta.getPageUrl();
        String query = openGraphMeta.getQueryString();
        List<KeyValuePairStruct> queryParams = openGraphMeta.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = openGraphMeta.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private OpenGraphMeta enhanceOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return openGraphMeta;
    }        

    private OpenGraphMeta processOpenGraphMeta(OpenGraphMeta openGraphMeta) throws BaseException
    {
        log.finer("TOP: processOpenGraphMeta()");
        // ...
        openGraphMeta = MetadataProcessor.getInstance().processOpenGraphMetaFetch(openGraphMeta);
        // ...   

        log.finer("BOTTOM: processOpenGraphMeta()");
        return openGraphMeta;
    }


    
    
}
