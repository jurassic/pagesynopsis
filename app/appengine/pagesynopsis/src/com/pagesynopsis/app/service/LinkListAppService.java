package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.LinkListBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.LinkListService;
import com.pagesynopsis.af.service.impl.LinkListServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.LinkList;


// Updated.
public class LinkListAppService extends LinkListServiceImpl implements LinkListService
{
    private static final Logger log = Logger.getLogger(LinkListAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public LinkListAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // LinkList related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public LinkList getLinkList(String guid) throws BaseException
    {
        return super.getLinkList(guid);
    }

    @Override
    public Object getLinkList(String guid, String field) throws BaseException
    {
        return super.getLinkList(guid, field);
    }

    @Override
    public List<LinkList> getLinkLists(List<String> guids) throws BaseException
    {
        return super.getLinkLists(guids);
    }

    @Override
    public List<LinkList> getAllLinkLists() throws BaseException
    {
        return super.getAllLinkLists();
    }

    @Override
    public List<String> getAllLinkListKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllLinkListKeys(ordering, offset, count);
    }

    @Override
    public List<LinkList> findLinkLists(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkLists(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findLinkListKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findLinkListKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createLinkList(LinkList linkList) throws BaseException
    {
        log.finer("TOP: createLinkList()");

        // TBD:
        linkList = validateLinkList(linkList);
        linkList = enhanceLinkList(linkList);
        // ...
        
        // ????
        Integer fefreshStatus = ((LinkListBean) linkList).getRefreshStatus();
        if(fefreshStatus == null) {
            ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processLinkList().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = linkList.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                linkList = processLinkList(linkList);
                if(linkList != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: linkList = " + linkList);
                    // Need to to do this to avoid double processing.
                    ((LinkListBean) linkList).setDeferred(true);
                    // ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Save it.
        String guid = super.createLinkList(linkList);

        log.finer("BOTTOM: createLinkList(): guid = " + guid);
        return guid;
    }

    @Override
    public LinkList constructLinkList(LinkList linkList) throws BaseException
    {
        log.finer("TOP: constructLinkList()");

        // TBD:
        linkList = validateLinkList(linkList);
        linkList = enhanceLinkList(linkList);
        // ...
        
        // ????
        Integer fefreshStatus = ((LinkListBean) linkList).getRefreshStatus();
        if(fefreshStatus == null) {
            ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processLinkList().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = linkList.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                linkList = processLinkList(linkList);
                if(linkList != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: linkList = " + linkList);
                    // Need to to do this to avoid double processing.
                    ((LinkListBean) linkList).setDeferred(true);
                    // ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((LinkListBean) linkList).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Update it.
        linkList = super.constructLinkList(linkList);
        
        log.finer("BOTTOM: constructLinkList(): linkList = " + linkList);
        return linkList;
    }


    @Override
    public Boolean updateLinkList(LinkList linkList) throws BaseException
    {
        log.finer("TOP: updateLinkList()");

        // TBD:
        linkList = validateLinkList(linkList);
        Boolean suc = super.updateLinkList(linkList);

        log.finer("BOTTOM: createLinkList(): suc = " + suc);
        return suc;
    }
        
    @Override
    public LinkList refreshLinkList(LinkList linkList) throws BaseException
    {
        log.finer("TOP: refreshLinkList()");

        // TBD:
        linkList = validateLinkList(linkList);
        linkList = super.refreshLinkList(linkList);
        
        log.finer("BOTTOM: refreshLinkList(): linkList = " + linkList);
        return linkList;
    }

    
    @Override
    public Boolean deleteLinkList(String guid) throws BaseException
    {
        return super.deleteLinkList(guid);
    }

    @Override
    public Boolean deleteLinkList(LinkList linkList) throws BaseException
    {
        return super.deleteLinkList(linkList);
    }

    @Override
    public Integer createLinkLists(List<LinkList> linkLists) throws BaseException
    {
        return super.createLinkLists(linkLists);
    }

    // TBD
    //@Override
    //public Boolean updateLinkLists(List<LinkList> linkLists) throws BaseException
    //{
    //}

    
    
    // TBD...
    private LinkListBean validateLinkList(LinkList linkList) throws BaseException
    {
        LinkListBean bean = null;
        if(linkList instanceof LinkListBean) {
            bean = (LinkListBean) linkList;
        }
        // else ???
        
        // ...
        String targetUrl =  linkList.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = linkList.getPageUrl();
        String query = linkList.getQueryString();
        List<KeyValuePairStruct> queryParams = linkList.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = linkList.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private LinkList enhanceLinkList(LinkList linkList) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return linkList;
    }        

    private LinkList processLinkList(LinkList linkList) throws BaseException
    {
        log.finer("TOP: processLinkList()");
        // ...
        linkList = FetchProcessor.getInstance().processLinkFetch(linkList);
        // ...   

        log.finer("BOTTOM: processLinkList()");
        return linkList;
    }


    
    
}
