package com.pagesynopsis.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgMovieService;
import com.pagesynopsis.af.service.impl.OgMovieServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgMovieAppService extends OgMovieServiceImpl implements OgMovieService
{
    private static final Logger log = Logger.getLogger(OgMovieAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgMovieAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgMovie related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgMovie getOgMovie(String guid) throws BaseException
    {
        return super.getOgMovie(guid);
    }

    @Override
    public Object getOgMovie(String guid, String field) throws BaseException
    {
        return super.getOgMovie(guid, field);
    }

    @Override
    public List<OgMovie> getOgMovies(List<String> guids) throws BaseException
    {
        return super.getOgMovies(guids);
    }

    @Override
    public List<OgMovie> getAllOgMovies() throws BaseException
    {
        return super.getAllOgMovies();
    }

    @Override
    public List<String> getAllOgMovieKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllOgMovieKeys(ordering, offset, count);
    }

    @Override
    public List<OgMovie> findOgMovies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgMovies(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findOgMovieKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgMovieKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgMovie(OgMovie ogMovie) throws BaseException
    {
        return super.createOgMovie(ogMovie);
    }

    @Override
    public OgMovie constructOgMovie(OgMovie ogMovie) throws BaseException
    {
        return super.constructOgMovie(ogMovie);
    }


    @Override
    public Boolean updateOgMovie(OgMovie ogMovie) throws BaseException
    {
        return super.updateOgMovie(ogMovie);
    }
        
    @Override
    public OgMovie refreshOgMovie(OgMovie ogMovie) throws BaseException
    {
        return super.refreshOgMovie(ogMovie);
    }

    @Override
    public Boolean deleteOgMovie(String guid) throws BaseException
    {
        return super.deleteOgMovie(guid);
    }

    @Override
    public Boolean deleteOgMovie(OgMovie ogMovie) throws BaseException
    {
        return super.deleteOgMovie(ogMovie);
    }

    @Override
    public Integer createOgMovies(List<OgMovie> ogMovies) throws BaseException
    {
        return super.createOgMovies(ogMovies);
    }

    // TBD
    //@Override
    //public Boolean updateOgMovies(List<OgMovie> ogMovies) throws BaseException
    //{
    //}

}
