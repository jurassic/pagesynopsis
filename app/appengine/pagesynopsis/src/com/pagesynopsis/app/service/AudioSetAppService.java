package com.pagesynopsis.app.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.AudioSetService;
import com.pagesynopsis.af.service.impl.AudioSetServiceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.AudioSet;


// Updated.
public class AudioSetAppService extends AudioSetServiceImpl implements AudioSetService
{
    private static final Logger log = Logger.getLogger(AudioSetAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public AudioSetAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // AudioSet related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public AudioSet getAudioSet(String guid) throws BaseException
    {
        return super.getAudioSet(guid);
    }

    @Override
    public Object getAudioSet(String guid, String field) throws BaseException
    {
        return super.getAudioSet(guid, field);
    }

    @Override
    public List<AudioSet> getAudioSets(List<String> guids) throws BaseException
    {
        return super.getAudioSets(guids);
    }

    @Override
    public List<AudioSet> getAllAudioSets() throws BaseException
    {
        return super.getAllAudioSets();
    }

    @Override
    public List<String> getAllAudioSetKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllAudioSetKeys(ordering, offset, count);
    }

    @Override
    public List<AudioSet> findAudioSets(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAudioSets(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findAudioSetKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findAudioSetKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createAudioSet(AudioSet audioSet) throws BaseException
    {
        log.finer("TOP: createAudioSet()");

        // TBD:
        audioSet = validateAudioSet(audioSet);
        audioSet = enhanceAudioSet(audioSet);
        // ...
        
        // ????
        Integer fefreshStatus = ((AudioSetBean) audioSet).getRefreshStatus();
        if(fefreshStatus == null) {
            ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);  // ???
        }
        // ...

        // Note:
        // POST/PUT should not call processAudioSet().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = audioSet.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                audioSet = processAudioSet(audioSet);
                if(audioSet != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: audioSet = " + audioSet);
                    // Need to to do this to avoid double processing.
                    ((AudioSetBean) audioSet).setDeferred(true);
                    // ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Save it.
        String guid = super.createAudioSet(audioSet);

        log.finer("BOTTOM: createAudioSet(): guid = " + guid);
        return guid;
    }

    @Override
    public AudioSet constructAudioSet(AudioSet audioSet) throws BaseException
    {
        log.finer("TOP: constructAudioSet()");

        // TBD:
        audioSet = validateAudioSet(audioSet);
        audioSet = enhanceAudioSet(audioSet);
        // ...
        
        // ????
        Integer fefreshStatus = ((AudioSetBean) audioSet).getRefreshStatus();
        if(fefreshStatus == null) {
            ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);   // ????
        }
        // ...
        
        
        // Note:
        // POST/PUT should not call processAudioSet().
        // Web page info should really be fetched during GET, if it hasn't already been fetched...  
        // --> This has changed as of 4/08/13
        // If deferred==false, we process it before saving it....

        Boolean isDeferred = audioSet.isDeferred();
        if(Boolean.FALSE.equals(isDeferred)) {   // by default, all messages are processed in a "deferred" (cron) mode...
            log.fine("Fetch to be procssed in a non-deferred mode.");
            try {
                audioSet = processAudioSet(audioSet);
                if(audioSet != null) {
                    if(log.isLoggable(Level.INFO)) log.info("Fetch processed in a non-deferred mode: audioSet = " + audioSet);
                    // Need to to do this to avoid double processing.
                    ((AudioSetBean) audioSet).setDeferred(true);
                    // ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ?????
                    // ....
                } else {
                    log.warning("Fetch processing in a non-deferred mode falied.");
                }
                // ...
            } catch(Exception e) {
                log.log(Level.WARNING, "Failed to process fetch", e);
                // ???
                // ...
            }
        } else {
            // "Deferred" cron will process the fetch
            ((AudioSetBean) audioSet).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
        }
        // ...


        // Update it.
        audioSet = super.constructAudioSet(audioSet);
        
        log.finer("BOTTOM: constructAudioSet(): audioSet = " + audioSet);
        return audioSet;
    }


    @Override
    public Boolean updateAudioSet(AudioSet audioSet) throws BaseException
    {
        log.finer("TOP: updateAudioSet()");

        // TBD:
        audioSet = validateAudioSet(audioSet);
        Boolean suc = super.updateAudioSet(audioSet);

        log.finer("BOTTOM: createAudioSet(): suc = " + suc);
        return suc;
    }
        
    @Override
    public AudioSet refreshAudioSet(AudioSet audioSet) throws BaseException
    {
        log.finer("TOP: refreshAudioSet()");

        // TBD:
        audioSet = validateAudioSet(audioSet);
        audioSet = super.refreshAudioSet(audioSet);
        
        log.finer("BOTTOM: refreshAudioSet(): audioSet = " + audioSet);
        return audioSet;
    }

    
    @Override
    public Boolean deleteAudioSet(String guid) throws BaseException
    {
        return super.deleteAudioSet(guid);
    }

    @Override
    public Boolean deleteAudioSet(AudioSet audioSet) throws BaseException
    {
        return super.deleteAudioSet(audioSet);
    }

    @Override
    public Integer createAudioSets(List<AudioSet> audioSets) throws BaseException
    {
        return super.createAudioSets(audioSets);
    }

    // TBD
    //@Override
    //public Boolean updateAudioSets(List<AudioSet> audioSets) throws BaseException
    //{
    //}

    
    
    // TBD...
    private AudioSetBean validateAudioSet(AudioSet audioSet) throws BaseException
    {
        AudioSetBean bean = null;
        if(audioSet instanceof AudioSetBean) {
            bean = (AudioSetBean) audioSet;
        }
        // else ???
        
        // ...
        String targetUrl =  audioSet.getTargetUrl();
        if(targetUrl == null || targetUrl.isEmpty()) {   // Validation as well.......
            // ???  --> Note that the request will not even be saved ....
            throw new BaseException("Target Url is null or empty. Cannot proceed with fetch.");
        } else if(targetUrl.length() > 500) {
            // ??? Truncate or error?
            // TBD ....
            throw new BaseException("Target Url is too long. Cannot proceed with fetch. targetUrl = " + targetUrl);            
        }
        
        // TBD:
        // If we truncate targetUrl...
        // We need to check pageUrl as well....
        // ...         
        
        
        
        // TBD: 
        String pageUrl = audioSet.getPageUrl();
        String query = audioSet.getQueryString();
        List<KeyValuePairStruct> queryParams = audioSet.getQueryParams();
        // if either of these is null/empty
        // populate them based on targetUrl...
        // --> This should have done earlier (closer to front end) ???
        // ...
        
        
//        // Next, check input...
//        List<String> input = audioSet.getInput();
//        if(input == null || input.isEmpty()) {
//            // ???  --> Note that the request will not even be saved ....
//            throw new BaseException("Input is null or empty. Cannot proceed with fetch.");
//        }

        
        
        // TBD:
        // Minimum value for refresh interval???
        // etc....
        
        // ...

        
        // TBD:
        // ...
        
        
        return bean;
    }
    
    private AudioSet enhanceAudioSet(AudioSet audioSet) throws BaseException
    {        
        // TBD:
        // Create shortlink, etc...
        // ....
        return audioSet;
    }        

    private AudioSet processAudioSet(AudioSet audioSet) throws BaseException
    {
        log.finer("TOP: processAudioSet()");
        // ...
        audioSet = FetchProcessor.getInstance().processAudioFetch(audioSet);
        // ...   

        log.finer("BOTTOM: processAudioSet()");
        return audioSet;
    }


    
    
}
