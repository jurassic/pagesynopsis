package com.pagesynopsis.app.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
//import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import java.util.List;
import java.util.ArrayList;

import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.proxy.AbstractProxyFactory;
import com.pagesynopsis.af.proxy.manager.ProxyFactoryManager;
import com.pagesynopsis.af.service.ServiceConstants;
import com.pagesynopsis.af.service.OgTvEpisodeService;
import com.pagesynopsis.af.service.impl.OgTvEpisodeServiceImpl;


// TBD:
// This class is only a place-holder.
// Copy this class into your package, and add any business logic beyond what is included in impl class.
public class OgTvEpisodeAppService extends OgTvEpisodeServiceImpl implements OgTvEpisodeService
{
    private static final Logger log = Logger.getLogger(OgTvEpisodeAppService.class.getName());
    //private static final ProxyFactory proxyFactory = ProxyFactoryManager.getProxyFactory();

    private static AbstractProxyFactory getProxyFactory()
    {
        return ProxyFactoryManager.getProxyFactory();
    }


    public OgTvEpisodeAppService()
    {
         super();
    }


    //////////////////////////////////////////////////////////////////////////
    // OgTvEpisode related methods
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public OgTvEpisode getOgTvEpisode(String guid) throws BaseException
    {
        return super.getOgTvEpisode(guid);
    }

    @Override
    public Object getOgTvEpisode(String guid, String field) throws BaseException
    {
        return super.getOgTvEpisode(guid, field);
    }

    @Override
    public List<OgTvEpisode> getOgTvEpisodes(List<String> guids) throws BaseException
    {
        return super.getOgTvEpisodes(guids);
    }

    @Override
    public List<OgTvEpisode> getAllOgTvEpisodes() throws BaseException
    {
        return super.getAllOgTvEpisodes();
    }

    @Override
    public List<String> getAllOgTvEpisodeKeys(String ordering, Long offset, Integer count) throws BaseException
    {
        return super.getAllOgTvEpisodeKeys(ordering, offset, count);
    }

    @Override
    public List<OgTvEpisode> findOgTvEpisodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgTvEpisodes(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public List<String> findOgTvEpisodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
        return super.findOgTvEpisodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        return super.getCount(filter, params, values, aggregate);
    }


    @Override
    public String createOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return super.createOgTvEpisode(ogTvEpisode);
    }

    @Override
    public OgTvEpisode constructOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return super.constructOgTvEpisode(ogTvEpisode);
    }


    @Override
    public Boolean updateOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return super.updateOgTvEpisode(ogTvEpisode);
    }
        
    @Override
    public OgTvEpisode refreshOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return super.refreshOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Boolean deleteOgTvEpisode(String guid) throws BaseException
    {
        return super.deleteOgTvEpisode(guid);
    }

    @Override
    public Boolean deleteOgTvEpisode(OgTvEpisode ogTvEpisode) throws BaseException
    {
        return super.deleteOgTvEpisode(ogTvEpisode);
    }

    @Override
    public Integer createOgTvEpisodes(List<OgTvEpisode> ogTvEpisodes) throws BaseException
    {
        return super.createOgTvEpisodes(ogTvEpisodes);
    }

    // TBD
    //@Override
    //public Boolean updateOgTvEpisodes(List<OgTvEpisode> ogTvEpisodes) throws BaseException
    //{
    //}

}
