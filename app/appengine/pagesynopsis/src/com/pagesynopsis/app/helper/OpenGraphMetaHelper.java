package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.OpenGraphMetaAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OpenGraphMeta;


public class OpenGraphMetaHelper
{
    private static final Logger log = Logger.getLogger(OpenGraphMetaHelper.class.getName());

    private OpenGraphMetaHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private OpenGraphMetaAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private OpenGraphMetaAppService getOpenGraphMetaService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new OpenGraphMetaAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class OpenGraphMetaHelperHolder
    {
        private static final OpenGraphMetaHelper INSTANCE = new OpenGraphMetaHelper();
    }

    // Singleton method
    public static OpenGraphMetaHelper getInstance()
    {
        return OpenGraphMetaHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public OpenGraphMeta getOpenGraphMeta(String guid) 
    {
        OpenGraphMeta message = null;
        try {
            message = getOpenGraphMetaService().getOpenGraphMeta(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getOpenGraphMetaKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getOpenGraphMetaService().findOpenGraphMetaKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<OpenGraphMeta> getOpenGraphMetasForRefreshProcessing(Integer maxCount)
    {
        List<OpenGraphMeta> openGraphMetas = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            openGraphMetas = getOpenGraphMetaService().findOpenGraphMetas(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return openGraphMetas;
    }


}
