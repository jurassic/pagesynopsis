package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.RobotsTextFileAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextFile;


public class RobotsTextFileHelper
{
    private static final Logger log = Logger.getLogger(RobotsTextFileHelper.class.getName());

    private RobotsTextFileHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private RobotsTextFileAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private RobotsTextFileAppService getRobotsTextFileService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new RobotsTextFileAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class RobotsTextFileHelperHolder
    {
        private static final RobotsTextFileHelper INSTANCE = new RobotsTextFileHelper();
    }

    // Singleton method
    public static RobotsTextFileHelper getInstance()
    {
        return RobotsTextFileHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public RobotsTextFile getRobotsTextFile(String guid) 
    {
        RobotsTextFile message = null;
        try {
            message = getRobotsTextFileService().getRobotsTextFile(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    

    public RobotsTextFile getRobotsTextFileBySiteUrl(String siteUrl, Long timeCutoff) 
    {
        RobotsTextFile bean = null;
        try {
            String filter = "siteUrl=='" + siteUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
//                filter += " && createdTime >= " + timeCutoff;
                filter += " && lastCheckedTime >= " + timeCutoff;
            }
//            String ordering = "createdTime desc";
            String ordering = "lastCheckedTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<RobotsTextFile> beans = getRobotsTextFileService().findRobotsTextFiles(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getRobotsTextFileService().getRobotsTextFile(guid);
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for siteUrl = " + siteUrl, e);
        }
        return bean;
    }

    
    
    // Note:
    // Refreshing is done through RobotsTextRefresh...
    
//    public List<String> getRobotsTextFileKeysForRefreshProcessing(Integer maxCount)
//    {
//        List<String> keys = null;
//        long now = System.currentTimeMillis();
//        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
//        String ordering = "nextRefreshTime asc";
//        try {
//            keys = getRobotsTextFileService().findRobotsTextFileKeys(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
//        }
//
//        return keys;
//    }
//    
//    public List<RobotsTextFile> getRobotsTextFilesForRefreshProcessing(Integer maxCount)
//    {
//        List<RobotsTextFile> robotsTextFiles = null;
//        long now = System.currentTimeMillis();
//        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
//        String ordering = "nextRefreshTime asc";
//        try {
//            robotsTextFiles = getRobotsTextFileService().findRobotsTextFiles(filter, ordering, null, null, null, null, 0L, maxCount);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
//        }
//
//        return robotsTextFiles;
//    }


}
