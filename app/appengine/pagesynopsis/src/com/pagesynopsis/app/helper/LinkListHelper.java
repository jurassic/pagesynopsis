package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.LinkListAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.LinkList;


public class LinkListHelper
{
    private static final Logger log = Logger.getLogger(LinkListHelper.class.getName());

    private LinkListHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private LinkListAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private LinkListAppService getLinkListService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new LinkListAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class LinkListHelperHolder
    {
        private static final LinkListHelper INSTANCE = new LinkListHelper();
    }

    // Singleton method
    public static LinkListHelper getInstance()
    {
        return LinkListHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public LinkList getLinkList(String guid) 
    {
        LinkList message = null;
        try {
            message = getLinkListService().getLinkList(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getLinkListKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getLinkListService().findLinkListKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<LinkList> getLinkListsForRefreshProcessing(Integer maxCount)
    {
        List<LinkList> linkLists = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            linkLists = getLinkListService().findLinkLists(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return linkLists;
    }


}
