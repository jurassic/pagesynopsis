package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.ImageSetAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.ImageSet;


public class ImageSetHelper
{
    private static final Logger log = Logger.getLogger(ImageSetHelper.class.getName());

    private ImageSetHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private ImageSetAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private ImageSetAppService getImageSetService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new ImageSetAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class ImageSetHelperHolder
    {
        private static final ImageSetHelper INSTANCE = new ImageSetHelper();
    }

    // Singleton method
    public static ImageSetHelper getInstance()
    {
        return ImageSetHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public ImageSet getImageSet(String guid) 
    {
        ImageSet imageSet = null;
        try {
            imageSet = getImageSetService().getImageSet(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the imageSet for guid = " + guid, e);
        }
        return imageSet;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getImageSetKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getImageSetService().findImageSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: ...
    public List<ImageSet> getImageSetsForRefreshProcessing(Integer maxCount)
    {
        List<ImageSet> imageSets = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            imageSets = getImageSetService().findImageSets(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return imageSets;
    }


}
