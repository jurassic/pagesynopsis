package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.PageInfoAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.PageInfo;


public class PageInfoHelper
{
    private static final Logger log = Logger.getLogger(PageInfoHelper.class.getName());

    private PageInfoHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private PageInfoAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private PageInfoAppService getPageInfoService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new PageInfoAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class PageInfoHelperHolder
    {
        private static final PageInfoHelper INSTANCE = new PageInfoHelper();
    }

    // Singleton method
    public static PageInfoHelper getInstance()
    {
        return PageInfoHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public PageInfo getPageInfo(String guid) 
    {
        PageInfo message = null;
        try {
            message = getPageInfoService().getPageInfo(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getPageInfoKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getPageInfoService().findPageInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<PageInfo> getPageInfosForRefreshProcessing(Integer maxCount)
    {
        List<PageInfo> pageInfos = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            pageInfos = getPageInfoService().findPageInfos(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return pageInfos;
    }


}
