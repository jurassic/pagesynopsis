package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.FetchRequestAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.FetchRequest;


public class FetchRequestHelper
{
    private static final Logger log = Logger.getLogger(FetchRequestHelper.class.getName());

    private FetchRequestHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private FetchRequestAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private FetchRequestAppService getFetchRequestService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new FetchRequestAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class FetchRequestHelperHolder
    {
        private static final FetchRequestHelper INSTANCE = new FetchRequestHelper();
    }

    // Singleton method
    public static FetchRequestHelper getInstance()
    {
        return FetchRequestHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public FetchRequest getFetchRequest(String guid) 
    {
        FetchRequest message = null;
        try {
            message = getFetchRequestService().getFetchRequest(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
//    // ????
//    public FetchRequest getFetchRequestByKey(String key) 
//    {
//        FetchRequest message = null;
//        try {
//            // ??????
//            String guid = key;
//            message = getFetchRequestService().getFetchRequest(key);
//        } catch (BaseException e) {
//            log.log(Level.WARNING, "Failed to retrieve the message for key = " + key, e);
//        }
//        return message;
//    }


    
    ///////////////////////////////////////////////////////////////////////////
    // Note: Google app engine puts so many restrictions as to what kind of query can be performed....
    //       http://code.google.com/appengine/docs/java/datastore/queries.html
    ///////////////////////////////////////////////////////////////////////////
    

    // TBD
    
    public List<String> getFetchRequestKeysForProcessing(Integer maxCount)
    {
        return getFetchRequestKeysForProcessing(maxCount, null);
    }
    // target == {"pageinfo", "linklist"}
    public List<String> getFetchRequestKeysForProcessing(Integer maxCount, String target)
    {
        List<String> keys = null;
        String filter = "fetchStatus == " + FetchStatus.STATUS_SCHEDULED;
        if(target != null && !target.isEmpty()) {
            filter += " && fetchObject == '" + target + "'";
        }
        String ordering = null;
        try {
            keys = getFetchRequestService().findFetchRequestKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<FetchRequest> getFetchRequestsForProcessing(Integer maxCount)
    {
        return getFetchRequestsForProcessing(maxCount, null);
    }
    // target == {"pageinfo", "linklist"}
    public List<FetchRequest> getFetchRequestsForProcessing(Integer maxCount, String target)
    {
        List<FetchRequest> fetchRequests = null;
        String filter = "fetchStatus == " + FetchStatus.STATUS_SCHEDULED;
        if(target != null && !target.isEmpty()) {
            filter += " && fetchObject == '" + target + "'";
        }
        String ordering = null;
        try {
            fetchRequests = getFetchRequestService().findFetchRequests(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return fetchRequests;
    }
 

}
