package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.PageFetchAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.PageFetch;


public class PageFetchHelper
{
    private static final Logger log = Logger.getLogger(PageFetchHelper.class.getName());

    private PageFetchHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private PageFetchAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private PageFetchAppService getPageFetchService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new PageFetchAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class PageFetchHelperHolder
    {
        private static final PageFetchHelper INSTANCE = new PageFetchHelper();
    }

    // Singleton method
    public static PageFetchHelper getInstance()
    {
        return PageFetchHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public PageFetch getPageFetch(String guid) 
    {
        PageFetch message = null;
        try {
            message = getPageFetchService().getPageFetch(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getPageFetchKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getPageFetchService().findPageFetchKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<PageFetch> getPageFetchesForRefreshProcessing(Integer maxCount)
    {
        List<PageFetch> pageFetches = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            pageFetches = getPageFetchService().findPageFetches(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return pageFetches;
    }


}
