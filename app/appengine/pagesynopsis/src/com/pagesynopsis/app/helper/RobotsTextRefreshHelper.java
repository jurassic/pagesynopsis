package com.pagesynopsis.app.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.service.RobotsTextRefreshAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.FetchStatus;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.RobotsTextRefresh;


public class RobotsTextRefreshHelper
{
    private static final Logger log = Logger.getLogger(RobotsTextRefreshHelper.class.getName());

    private RobotsTextRefreshHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private RobotsTextRefreshAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private RobotsTextRefreshAppService getRobotsTextRefreshService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new RobotsTextRefreshAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class RobotsTextRefreshHelperHolder
    {
        private static final RobotsTextRefreshHelper INSTANCE = new RobotsTextRefreshHelper();
    }

    // Singleton method
    public static RobotsTextRefreshHelper getInstance()
    {
        return RobotsTextRefreshHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public RobotsTextRefresh getRobotsTextRefresh(String guid) 
    {
        RobotsTextRefresh message = null;
        try {
            message = getRobotsTextRefreshService().getRobotsTextRefresh(guid);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve the message for guid = " + guid, e);
        }
        return message;
    }
    
    
    // TBD:
    // Check the pageTitle field as well....  ????
    // ...
    

    public List<String> getRobotsTextRefreshKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextCheckedTime <= " + now;
        String ordering = "nextCheckedTime asc";
        try {
            keys = getRobotsTextRefreshService().findRobotsTextRefreshKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    public List<RobotsTextRefresh> getRobotsTextRefreshsForRefreshProcessing(Integer maxCount)
    {
        List<RobotsTextRefresh> robotsTextRefreshs = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextCheckedTime <= " + now;
        String ordering = "nextCheckedTime asc";
        try {
            robotsTextRefreshs = getRobotsTextRefreshService().findRobotsTextRefreshes(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (BaseException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return robotsTextRefreshs;
    }


}
