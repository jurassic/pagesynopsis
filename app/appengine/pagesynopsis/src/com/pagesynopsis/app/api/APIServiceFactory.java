package com.pagesynopsis.app.api;

// TBD:
// ...
public abstract class APIServiceFactory
{
    public abstract UrlShortenerAPIService getUrlShortenerAPIService();
    public abstract TwitterAPIService getTwitterAPIService();
    // ...

}
