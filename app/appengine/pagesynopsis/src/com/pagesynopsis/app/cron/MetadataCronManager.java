package com.pagesynopsis.app.cron;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.fetch.MetadataProcessor;
import com.pagesynopsis.app.helper.AudioSetHelper;
import com.pagesynopsis.app.helper.FetchRequestHelper;
import com.pagesynopsis.app.helper.ImageSetHelper;
import com.pagesynopsis.app.helper.LinkListHelper;
import com.pagesynopsis.app.helper.TwitterCardMetaHelper;
import com.pagesynopsis.app.helper.OpenGraphMetaHelper;
import com.pagesynopsis.app.helper.VideoSetHelper;
import com.pagesynopsis.app.service.AudioSetAppService;
import com.pagesynopsis.app.service.FetchRequestAppService;
import com.pagesynopsis.app.service.ImageSetAppService;
import com.pagesynopsis.app.service.LinkListAppService;
import com.pagesynopsis.app.service.TwitterCardMetaAppService;
import com.pagesynopsis.app.service.OpenGraphMetaAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.app.service.VideoSetAppService;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.FetchRequest;
import com.pagesynopsis.ws.ImageSet;
import com.pagesynopsis.ws.LinkList;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.VideoSet;


public class MetadataCronManager
{
    private static final Logger log = Logger.getLogger(MetadataCronManager.class.getName());   

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private OpenGraphMetaAppService openGraphMetaAppService = null;
    private TwitterCardMetaAppService twitterCardMetaAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private OpenGraphMetaAppService getOpenGraphMetaService()
    {
        if(openGraphMetaAppService == null) {
            openGraphMetaAppService = new OpenGraphMetaAppService();
        }
        return openGraphMetaAppService;
    }
    private TwitterCardMetaAppService getTwitterCardMetaService()
    {
        if(twitterCardMetaAppService == null) {
            twitterCardMetaAppService = new TwitterCardMetaAppService();
        }
        return twitterCardMetaAppService;
    }
    // etc. ...

    
    private MetadataCronManager()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class MetadataCronManagerHolder
    {
        private static final MetadataCronManager INSTANCE = new MetadataCronManager();
    }

    // Singleton method
    public static MetadataCronManager getInstance()
    {
        return MetadataCronManagerHolder.INSTANCE;
    }

    private void init()
    {
        // TBD: ...
    }
    
    
    
    public int processOpenGraphMetaRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = OpenGraphMetaHelper.getInstance().getOpenGraphMetaKeysForRefreshProcessing(maxCount);
        return processOpenGraphMetas(keys);
    }

    private int processOpenGraphMetas(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    OpenGraphMeta openGraphMeta = OpenGraphMetaHelper.getInstance().getOpenGraphMeta(key);
                    log.fine("OpenGraphMeta retrieved for processing: openGraphMeta = " + openGraphMeta);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    openGraphMeta = MetadataProcessor.getInstance().processOpenGraphMetaFetch(openGraphMeta);
                    log.fine("OpenGraphMeta processed: openGraphMeta = " + openGraphMeta);
                   
                    // ????
                    // ...
                    Boolean suc = getOpenGraphMetaService().updateOpenGraphMeta(openGraphMeta);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    

    
    public int processTwitterCardMetaRefresh()
    {
        // Get a list of requests to be processed...
        Integer maxCount = 10;   // ???
        List<String> keys = TwitterCardMetaHelper.getInstance().getTwitterCardMetaKeysForRefreshProcessing(maxCount);
        return processTwitterCardMetas(keys);
    }

    private int processTwitterCardMetas(List<String> keys)
    {
        log.finer("Begin processing: ");

        // TBD::
        int counter = 0;
        if(keys != null && !keys.isEmpty()) {
            int size = keys.size();
            log.info("Start processing " + size + " requests.");

            // TBD: iterate over primary keys...
            for(String key: keys) {
                try {
                    // ??? key or guid ???????
                    TwitterCardMeta twitterCardMeta = TwitterCardMetaHelper.getInstance().getTwitterCardMeta(key);
                    log.fine("TwitterCardMeta retrieved for processing: twitterCardMeta = " + twitterCardMeta);

                    // TBD:
                    // "lock" the message before processing (to avoid multiple send....)
                    
                    twitterCardMeta = MetadataProcessor.getInstance().processTwitterCardMetaFetch(twitterCardMeta);
                    log.fine("TwitterCardMeta processed: twitterCardMeta = " + twitterCardMeta);
                   
                    // ????
                    // ...
                    Boolean suc = getTwitterCardMetaService().updateTwitterCardMeta(twitterCardMeta);
                    log.info("Refresh request processed and updated: key = " + key + "; suc = " + suc);
                    // ....
                    
                    // if suc == true ???
                    counter++;
                } catch (BaseException e) {
                    log.log(Level.WARNING, "Exception occurred while saving the updated fetch record for key = " + key, e);
                    // Continue ???
                } finally {
                    // TBD:
                    // "Unlock" the message, here or while updating....
                }
            }

        }

        log.finer("End processing: " + counter + " requests.");
        return counter;
    }
    


}
