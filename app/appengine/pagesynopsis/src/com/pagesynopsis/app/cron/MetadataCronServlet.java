package com.pagesynopsis.app.cron;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pagesynopsis.ws.core.StatusCode;


// ...
public class MetadataCronServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MetadataCronServlet.class.getName());
    

    @Override
    public void init() throws ServletException
    {
        super.init();
        
        // Hack: "Preload" the Jersey client.... to reduce the initial loading time... 
        //JerseyClient.getInstance().initClient();  // The call does not do anything other than initializing the JerseyClient singletong instance...
        // ...
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("MetadataCronServlet::doGet() called.");
        
        // TBD:
        // Check the header X-AppEngine-Cron: true
        // ...
        boolean isGaeCron = false;
        String headerXGAECron = req.getHeader("X-AppEngine-Cron");
        if(headerXGAECron != null) {
            isGaeCron = Boolean.parseBoolean(headerXGAECron);
        }
        log.fine("isGaeCron = " + isGaeCron);

        
        // TBD:
        // Based on pathInfo + query params
        // Process different requests...  ???
        // ...
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        log.fine("contextPath = " + contextPath);
        log.fine("servletPath = " + servletPath);
        log.fine("pathInfo = " + pathInfo);

        
        // TBD:
        // ...
        
        String target = "opengraph";
        if(servletPath != null && !servletPath.isEmpty()) {
            if(servletPath.endsWith("/")) {
                servletPath = servletPath.substring(0, servletPath.length() - 1);
            }
            int lidx = servletPath.lastIndexOf("/");
            if(lidx < 0) {
                target = servletPath;
            } else {
                target = servletPath.substring(lidx + 1);
            }
        }


        // TBD:
        // ...
        // refresh only, at this point...
        // ....
        String mode = "fetch";    // or, "refresh"
        if(pathInfo != null && !pathInfo.isEmpty()) {
            if(pathInfo.startsWith("/")) {
                pathInfo = pathInfo.substring(1);
            }
            mode = pathInfo;
        }

        // TBD: validate mode???
        
        int cnt = 0;
        if("opengraph".equals(target)) {
            if("fetch".equals(mode)) {
//                cnt = MetadataCronManager.getInstance().processOpenGraphMetaFetch();
            } else if("refresh".equals(mode)) {
                cnt = MetadataCronManager.getInstance().processOpenGraphMetaRefresh();
            } else {
                // ???
            }
        } else if("twittercard".equals(target)) {
            if("fetch".equals(mode)) {
//                cnt = MetadataCronManager.getInstance().processTwitterCardMetaFetch();
            } else if("refresh".equals(mode)) {
                cnt = MetadataCronManager.getInstance().processTwitterCardMetaRefresh();
            } else {
                // ???
            }
        } else {
            // ????
            // Error....
        }
            
        log.info("Processed " + cnt + " fetch requests at " + System.currentTimeMillis());

        
        // Always return 200 ????
        // return 200 only if cnt > 0 ????
        resp.setStatus(StatusCode.OK);
    }

    
    // TBD:
    // depending on mail.type...
    // ...
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    // TBD: Need to use refreshXXX() rather than updateXXX()
    //      (Some UI fields need to be updated based on the server data....)
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
    
    // TBD:
    // validators???
    // e.g., max length of subject, etc.
    // ...
    
    
    
}
