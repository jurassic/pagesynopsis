package com.pagesynopsis.app.robotcheck;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextGroup;


// TBD: ..
public class RobotMap
{
    private static final Logger log = Logger.getLogger(RobotMap.class.getName());

    // User-Agent -> RobotsTextGroup (allow/disallow URL list)
    private Map<String, RobotsTextGroup> userAgentGroups = new HashMap<String, RobotsTextGroup>();
    // ...
    
    
    public RobotMap(RobotsTextFile robotsTextFile)
    {
        init(robotsTextFile);
    }

    
    // ???
    private void init(RobotsTextFile robotsTextFile)
    {
        if(robotsTextFile == null) {
            return;
        }
        
        List<RobotsTextGroup> groups = robotsTextFile.getGroups();
        if(groups == null || groups.isEmpty()) {
            return;
        }
        
        for(RobotsTextGroup g : groups) {
            
            String userAgent = g.getUserAgent();
            // check if userAgent is not null???

            userAgentGroups.put(userAgent, g);
        }
                
    }
    
    
    public RobotsTextGroup getRobotsTextGroup(String userAgent)
    {
        if(userAgentGroups.containsKey(userAgent)) {
            return userAgentGroups.get(userAgent);
        }
        if(userAgentGroups.containsKey("*")) {
            return userAgentGroups.get("*");
        }
        // ???
        return null;
    }
    
}
