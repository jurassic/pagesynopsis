package com.pagesynopsis.app.robotcheck;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.helper.URLHelper;


public class RobotCheckUtil
{
    private static final Logger log = Logger.getLogger(RobotCheckUtil.class.getName());

    private RobotCheckUtil() {}

    
    // TBD:
    public static String getSiteUrlFromTargetUrl(String targetUrl)
    {        
        String siteUrl =  URLHelper.getInstance().getTopLevelURL(targetUrl);
        return siteUrl;
    }


    // temporary
    public static String getPathFromUrl(String url)
    {
        if(url == null || url.isEmpty()) {
            return url;
        }
        
        if(! (url.startsWith("http://") || url.startsWith("https://"))) {
            // ???
            return url;
        }

        int idx1 = url.indexOf("/", 8);
        if(idx1 <= 0) {
            return "/";
        } else {
            return url.substring(idx1);
        }
    }
    

}
