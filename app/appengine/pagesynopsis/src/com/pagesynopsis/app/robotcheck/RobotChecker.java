package com.pagesynopsis.app.robotcheck;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.app.fetch.RobotsTextProcessor;
import com.pagesynopsis.app.helper.RobotsTextFileHelper;
import com.pagesynopsis.app.service.RobotsTextFileAppService;
import com.pagesynopsis.app.service.UserAppService;
import com.pagesynopsis.common.RobotPermission;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextGroup;


public class RobotChecker
{
    private static final Logger log = Logger.getLogger(RobotChecker.class.getName());

    private RobotChecker() {}

    // TBD: Is this safe for concurrent calls??
    private UserAppService userAppService = null;
    private RobotsTextFileAppService fetchRequestAppService = null;
    // etc...


    private UserAppService getUserService()
    {
        if(userAppService == null) {
            userAppService = new UserAppService();
        }
        return userAppService;
    }   
    private RobotsTextFileAppService getRobotsTextFileService()
    {
        if(fetchRequestAppService == null) {
            fetchRequestAppService = new RobotsTextFileAppService();
        }
        return fetchRequestAppService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class RobotCheckerHolder
    {
        private static final RobotChecker INSTANCE = new RobotChecker();
    }

    // Singleton method
    public static RobotChecker getInstance()
    {
        return RobotCheckerHolder.INSTANCE;
    }

    
    
    // TBD:
    // this should be really in a different class....
    public List<String> getSitemapList(String siteUrl)
    {
        RobotsTextFile robotsTextFile = getRobotsTextFile(siteUrl);
        if(robotsTextFile == null) {
            return null;
        }
        List<String> sitemaps = robotsTextFile.getSitemaps();
        return sitemaps;
    }
    
    
    
    // TBD:
    // Need to think about how to do this...
    // the following is just a crude prototype
    
    
    // Returns three values: null, true, false. Or use RobotPermission ???
    // TBD: Should we allow null robot name ????
    // public Boolean isRobotAllowed(String targetUrl, String robot)
    public String isRobotAllowed(String targetUrl, String robot)
    {
        RobotsTextFile robotsTextFile = getRobotsTextFile(targetUrl);
        if(robotsTextFile == null) {
            // Cannot proceed...
            // return null;
            return RobotPermission.PERMISSION_UNKNOWN;
        }
        
        List<RobotsTextGroup> groups = robotsTextFile.getGroups();
        if(groups == null || groups.isEmpty()) {
            // everybody is allowed
            // return true;
            return RobotPermission.PERMISSION_ALLOWED;
        }
        
        // TBD:
        
        if(robot == null || robot.isEmpty()) {
            robot = "*";    // ???
        }
        
        RobotMap robotMap = new RobotMap(robotsTextFile);
        RobotsTextGroup group = robotMap.getRobotsTextGroup(robot);
        if(group == null) {
            // ???
            // return true;
            return RobotPermission.PERMISSION_ALLOWED;
        }
        
        List<String> allowedUrls = group.getAllows();
        List<String> disallowedUrls = group.getDisallows();
        if(disallowedUrls == null || disallowedUrls.isEmpty()) {
            // return true;
            return RobotPermission.PERMISSION_ALLOWED;
        }
        
        // ...
        String targetPath = RobotCheckUtil.getPathFromUrl(targetUrl);
        if(targetPath == null || targetPath.isEmpty()) {
            targetPath = "/";   // ???
        }
        
        
        // TBD:
        // Regex matching???
        // Handle wildcards *, and ?, etc...
        // ....
        
        
        // ????
        for(String url : disallowedUrls) {
            if(targetPath.startsWith(url)) {                
                for(String a : allowedUrls) {
                    if(a.startsWith(url) && targetPath.startsWith(a)) {
                        // return true;
                        return RobotPermission.PERMISSION_ALLOWED;
                    }
                }
                // return false;
                return RobotPermission.PERMISSION_DISALLOWED;
            }
        }
        // ????
        
        // return true;
        return RobotPermission.PERMISSION_ALLOWED;
    }
    
    
    // 
    public RobotsTextFile getRobotsTextFile(String targetUrl) 
    {
        String siteUrl = RobotCheckUtil.getSiteUrlFromTargetUrl(targetUrl);
        if(siteUrl == null) {
            if(log.isLoggable(Level.WARNING)) log.warning("Failed to find the website top-level URL from targetUrl = " + targetUrl);
            return null;
        }
        // TBD: Validate siteUrl ???
        
        // Arbitrary. Two days...
        Long timeCutoff = System.currentTimeMillis() - 2 * 24 * 3600 * 1000L;  
        RobotsTextFile robotsTextFile = RobotsTextFileHelper.getInstance().getRobotsTextFileBySiteUrl(siteUrl, timeCutoff);
        
        if(robotsTextFile != null) {
            if(log.isLoggable(Level.INFO)) log.info("robotsTextFile found for siteUrl = " + siteUrl);
        } else {
            try {
                robotsTextFile = RobotsTextProcessor.getInstance().processRobotsTextFetch(siteUrl);
                if(robotsTextFile != null) {
                    robotsTextFile = getRobotsTextFileService().constructRobotsTextFile(robotsTextFile);
                }
            // } catch (BaseException e) {
            } catch (Exception e) {
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to fetch/save robotsTextFile for siteUrl = " + siteUrl, e);
            }
        }
        
        return robotsTextFile;
    }

    

}
