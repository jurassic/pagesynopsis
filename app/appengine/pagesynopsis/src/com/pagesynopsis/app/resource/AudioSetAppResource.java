package com.pagesynopsis.app.resource;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.pagesynopsis.af.auth.TwoLeggedOAuthProvider;
import com.pagesynopsis.af.bean.AudioSetBean;
import com.pagesynopsis.af.resource.impl.BaseResourceImpl;
import com.pagesynopsis.af.resource.impl.AudioSetResourceImpl;
import com.pagesynopsis.app.fetch.FetchProcessor;
import com.pagesynopsis.app.service.AudioSetAppService;
import com.pagesynopsis.app.util.QueryStringUtil;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.AudioSet;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;
import com.pagesynopsis.ws.resource.exception.UnauthorizedRsException;
import com.pagesynopsis.ws.stub.AudioSetStub;
import com.sun.jersey.api.core.HttpContext;


@Path("/audioset/")
public class AudioSetAppResource extends BaseResourceImpl
{
    private static final Logger log = Logger.getLogger(AudioSetAppResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    private AudioSetAppService audioSetService = null;
    
    public AudioSetAppResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }

        this.httpContext = httpContext;
    }

    private AudioSetAppService getAudioSetAppService()
    {
        if(audioSetService == null) {
            audioSetService = new AudioSetAppService();
        }
        return audioSetService;
    }


    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAudioSetByTargetUrl(@QueryParam("targetUrl") String targetUrl, @QueryParam("fetch") Boolean fetch, @QueryParam("refresh") Integer refresh)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(targetUrl == null || targetUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, targetUrl, is missing.", resourceUri);
            }

            // refresh==0: refreshFirst, but do not update refreshInterval
            // refresh==-1: refreshFirst, but do not save (create/update)...
            boolean refreshFirst = false;
            Long refreshInterval = null;
            if(refresh != null) {
                refreshFirst = true;
                if(refresh > 0) {
                    refreshInterval = refresh * 1000L;
                }
            }
            log.fine("refreshFirst = " + refreshFirst + "; refreshInterval = " + refreshInterval);
            
            // Existing record for targetUrl in DB 
            AudioSetStub stub = null;
            if(refresh == null || refresh > -1) {
                String filter = "targetUrl=='" + targetUrl + "'";
                String ordering = "createdTime desc";
                Long offset = 0L;
                Integer count = 1;
                List<String> beanKeys = getAudioSetAppService().findAudioSetKeys(filter, ordering, null, null, null, null, offset, count);
                if(beanKeys != null && !beanKeys.isEmpty()) {
                    String beanKey = beanKeys.get(0);
                    AudioSet fullBean = getAudioSetAppService().getAudioSet(beanKey); 
                    stub = AudioSetStub.convertBeanToStub(fullBean);
                }
            }

            // Whether to create/update records in the DB...
            boolean update = false;   // Do not update by default...
            boolean create = true;    // Create, if update==false, by default... If update==true, this flag is not used...

            if(stub != null) {
                create = false;
                if(refreshFirst == true && refreshInterval != null) {
                    update = true;
                } else {
                    // update = false;  // default...
                }
            } else {
                if(refreshFirst == true && refresh == -1) {
                    create = false;
                    // update = false;  // default...
                } else {
                    // create = true;   // default....
                }
            }

            // Fetch/refresh if necessary...
            if(refreshFirst==true || (stub == null && (fetch == null || fetch == true))) {
                if(stub == null) {
                    stub = new AudioSetStub();   // ????
                    String guid = GUID.generate();
                    stub.setGuid(guid);
                    stub.setTargetUrl(targetUrl);
                    try {
                        URL url = new URL(targetUrl);  // ???
                        String query = url.getQuery();
                        String pageUrl = targetUrl;
                        if(query != null) {
                            int qidx = targetUrl.indexOf("?");
                            if(qidx > 0) {
                                pageUrl = targetUrl.substring(0, qidx);
                            }
                        }
                        stub.setPageUrl(pageUrl);
                        if(query != null && !query.isEmpty()) {
                            stub.setQueryString(query); 
                            List<KeyValuePairStruct> parameters = QueryStringUtil.parseQueryParameters(query);
                            stub.setQueryParams(parameters);
                        }
                    } catch(MalformedURLException e) {
                        log.log(Level.WARNING, "targetUrl is malformed. targetUrl = " + targetUrl, e);
                        // ignore and continue???? or, bail out???
                        throw new BadRequestException("targetUrl is malformed. targetUrl = " + targetUrl, e);
                    }
                }
                stub.setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);  // ???
                if(refreshInterval != null) {
                    stub.setRefreshInterval(refreshInterval);
                }

                // ...
                AudioSet bean = AudioSetResourceImpl.convertAudioSetStubToBean(stub);
                bean = FetchProcessor.getInstance().processAudioFetch(bean);
                // ...
                if(bean != null) {
                    // Need to to do this to avoid double processing.
                    ((AudioSetBean) bean).setDeferred(true);
                    // ....
                    if(update) {
                        bean = getAudioSetAppService().refreshAudioSet(bean);
                    } else {
                        if(create) {
                            bean = getAudioSetAppService().constructAudioSet(bean);
                        }
                    }
                }
                if(bean == null) {
                    throw new InternalServerErrorRsException("Failed to create/update a audioSet due to unknown error: targetUrl = " + targetUrl, resourceUri);                        
                }
                stub = AudioSetStub.convertBeanToStub(bean);
            }

            if(stub != null) {
                if(log.isLoggable(Level.FINE)) log.fine("Returning stub = " + stub);
                return Response.ok(stub).build();
            } else {
                // TBD: Throw 404 ??? Or, just return en empty/null object???
                throw new ResourceNotFoundRsException("AudioSet not found for the given targetUrl, " + targetUrl, resourceUri);
            }
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    

/*
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAudioSetByPageUrl(@QueryParam("pageUrl") String pageUrl, @QueryParam("queryString") String queryString, @QueryParam("fetch") Boolean fetch)
    {
        try {
            if(pageUrl == null || pageUrl.isEmpty()) {
                throw new BadRequestRsException("Arg, pageUrl, is missing.", resourceUri);
            }

            String filter = "pageUrl=='" + pageUrl + "'";
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<AudioSet> beans = getAudioSetAppService().findAudioSets(filter, ordering, null, null, null, null, offset, count);
            
            AudioSetStub stub = null;
            if(beans != null || !beans.isEmpty()) {
                stub = AudioSetStub.convertBeanToStub(beans.get(0));
                return Response.ok(stub).build();
            } else {
                // default: fetch == true
                if(fetch == null && fetch != false) {
                    stub = new AudioSetStub();   // ????
                    String guid = GUID.generate();
                    stub.setGuid(guid);
                    stub.setPageUrl(pageUrl);
                    // ...
                    
                    // ...
                    AudioSetBean bean = AudioSetResourceImpl.convertAudioSetStubToBean(stub);
                    bean = (AudioSetBean) getAudioSetAppService().constructAudioSet(bean);
                    if(bean == null) {
                        throw new InternalServerErrorRsException("Failed to create a audioSet due to unknown error: pageUrl = " + pageUrl, resourceUri);                        
                    }
                    stub = AudioSetStub.convertBeanToStub(bean);
                    return Response.ok(stub).build();
                } else {
                    // TBD: Throw 404 ??? Or, just return en empty/null object???
                    throw new ResourceNotFoundRsException("AudioSet not found for the given pageUrl, " + pageUrl, resourceUri);
                }
            }
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }
*/

}
