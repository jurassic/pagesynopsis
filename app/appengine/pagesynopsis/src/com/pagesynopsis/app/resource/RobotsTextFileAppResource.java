package com.pagesynopsis.app.resource;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.sun.jersey.api.core.HttpContext;

import com.pagesynopsis.af.auth.TwoLeggedOAuthProvider;
import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.resource.impl.BaseResourceImpl;
import com.pagesynopsis.af.resource.impl.RobotsTextFileResourceImpl;
import com.pagesynopsis.app.fetch.RobotsTextProcessor;
import com.pagesynopsis.app.service.RobotsTextFileAppService;
import com.pagesynopsis.app.util.QueryStringUtil;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.util.RobotsTextConstants;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.KeyValuePairStruct;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;
import com.pagesynopsis.ws.exception.RequestConflictException;
import com.pagesynopsis.ws.exception.RequestForbiddenException;
import com.pagesynopsis.ws.exception.resource.BaseResourceException;
import com.pagesynopsis.ws.resource.exception.BadRequestRsException;
import com.pagesynopsis.ws.resource.exception.InternalServerErrorRsException;
import com.pagesynopsis.ws.resource.exception.RequestForbiddenRsException;
import com.pagesynopsis.ws.resource.exception.ResourceNotFoundRsException;
import com.pagesynopsis.ws.resource.exception.ServiceUnavailableRsException;
import com.pagesynopsis.ws.resource.exception.UnauthorizedRsException;
import com.pagesynopsis.ws.stub.RobotsTextFileStub;


@Path("/robotstextfile/")
public class RobotsTextFileAppResource extends BaseResourceImpl
{
    private static final Logger log = Logger.getLogger(RobotsTextFileAppResource.class.getName());

    private UriInfo uriInfo;
    private HttpHeaders httpHeaders;
    private Request request;
    private String resourceUri;
    private boolean isRunningOnDevel;
    private HttpContext httpContext;

    private RobotsTextFileAppService robotsTextFileService = null;
    
    public RobotsTextFileAppResource(@Context javax.ws.rs.core.UriInfo uriInfo, @Context javax.ws.rs.core.HttpHeaders httpHeaders, @Context javax.ws.rs.core.Request request, @Context ServletContext servletContext, @Context HttpContext httpContext)
    {
        this.uriInfo = uriInfo;
        this.httpHeaders = httpHeaders;
        this.request = request;
        this.resourceUri = uriInfo.getAbsolutePath().toString();

        String serverInfo = servletContext.getServerInfo();
        log.info("ServerInfo = " + serverInfo);
        if(serverInfo != null && serverInfo.contains("Development")) {
            isRunningOnDevel = true;
        } else {
            isRunningOnDevel = false;
        }

        this.httpContext = httpContext;
    }

    private RobotsTextFileAppService getRobotsTextFileAppService()
    {
        if(robotsTextFileService == null) {
            robotsTextFileService = new RobotsTextFileAppService();
        }
        return robotsTextFileService;
    }


    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findRobotsTextFileBySiteUrl(@QueryParam("siteUrl") String siteUrl)
    {
        if(isAnonReadAllowed()) {
            // Bypassing the auth check...
            log.info("Anonymous read. No auth check done for this request.");
        } else {
            if(isRunningOnDevel && isIgnoreAuth()) {
                log.warning("OAuth is being bypassed! If it's a production environment, it is a serious error.");
            } else {
                // Verify the signature first.
                try {
                    if(!TwoLeggedOAuthProvider.getInstance().verify(httpContext.getRequest())) {
                        log.warning("Two-legged OAuth verification failed.");
                        throw new UnauthorizedRsException("Two-legged OAuth verification failed.", resourceUri);
                    }
                } catch (BadRequestException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to bad request.", e);
                    throw new BadRequestRsException("OAuth verification failed due to bad request.", e, resourceUri);  // ???
                } catch (BaseException e) {
                    log.log(Level.WARNING, "OAuth verification failed due to unknown error.", e);
                    throw new BaseResourceException("OAuth verification failed due to unknown error.", e, resourceUri);  // ???
                } catch (Exception e) {
                    log.log(Level.WARNING, "OAuth verification failed due to internal error.", e);
                    throw new InternalServerErrorRsException("OAuth verification failed due to internal error.", e, resourceUri);  // ???
                }
            }
        }

        try {
            if(siteUrl == null || siteUrl.isEmpty()) {    // TBD: Validate siteUrl ????
                throw new BadRequestRsException("Arg, siteUrl, is missing.", resourceUri);
            }
            // TBD: siteUrl validation???
            if( !(siteUrl.startsWith("http://") || siteUrl.startsWith("https://")) ) {
                if(log.isLoggable(Level.WARNING)) log.warning("Invalid siteUrl = " + siteUrl);
                throw new BadRequestRsException("Invalid siteUrl = " + siteUrl);
            }
            // TBD: Pick domain part only?
            //      Or, throw error if includes path???
            // ????
            // We store siteUrl without robots.txt part....
            if(siteUrl.endsWith("robots.txt")) {
                siteUrl = siteUrl.substring(0, siteUrl.lastIndexOf("robots.txt"));
                if(log.isLoggable(Level.INFO)) log.info("siteUrl modified: " + siteUrl);
            }
            // ...
            if(siteUrl.length() > 500) {
                if(log.isLoggable(Level.WARNING)) log.warning("siteUrl is too long: " + siteUrl);
                throw new BadRequestRsException("siteUrl is too long: " + siteUrl);
            }
            
            // Existing record for siteUrl in DB 
            RobotsTextFileStub stub = null;
            try {
                String filter = "siteUrl=='" + siteUrl + "'";
                String ordering = "createdTime desc";
                Long offset = 0L;
                Integer count = 1;
                List<String> beanKeys = getRobotsTextFileAppService().findRobotsTextFileKeys(filter, ordering, null, null, null, null, offset, count);
                if(beanKeys != null && !beanKeys.isEmpty()) {
                    String beanKey = beanKeys.get(0);
                    RobotsTextFile fullBean = getRobotsTextFileAppService().getRobotsTextFile(beanKey); 
                    stub = RobotsTextFileStub.convertBeanToStub(fullBean);
                }
            } catch(BaseException ex) {
                // What to do???? Just continue???
                throw new BaseResourceException(ex, resourceUri);
            } catch(Exception ex) {
                // What to do???? Just continue???
                throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
            }

            Long lastCheckedTime = null;
            if(stub != null) {
                lastCheckedTime = stub.getLastCheckedTime();
                // String contentHash = stub.getContentHash();
            } else {
                // create = true;   // default....
            }
            
            long now = System.currentTimeMillis();
            if(lastCheckedTime != null && lastCheckedTime > now - RobotsTextConstants.MAX_AGE_MILLIS) {
                // just return the exiting stub
            } else {
                RobotsTextFile bean = null;
                if(stub != null) {
                    bean = RobotsTextFileResourceImpl.convertRobotsTextFileStubToBean(stub);
                    bean = RobotsTextProcessor.getInstance().processRobotsTextFile(bean);
                    // Update, because it's pretty old...
                    if(bean != null) {
//                        // Need to to do this to avoid double processing.
//                        ((RobotsTextFileBean) bean).setDeferred(true);
//                        // ....
                        bean = getRobotsTextFileAppService().refreshRobotsTextFile(bean);
                    }
                } else {
                    bean = RobotsTextProcessor.getInstance().processRobotsTextFetch(siteUrl);
                    if(bean != null) {
//                        // Need to to do this to avoid double processing.
//                        ((RobotsTextFileBean) bean).setDeferred(true);
//                        // ....
                        bean = getRobotsTextFileAppService().constructRobotsTextFile(bean);
                    }
                }
                // ...
                if(bean == null) {
                    throw new InternalServerErrorRsException("Failed to create/process robotsTextFile due to unknown error: siteUrl = " + siteUrl, resourceUri);                        
                }
                stub = RobotsTextFileStub.convertBeanToStub(bean);
            }
            if(stub != null) {
                if(log.isLoggable(Level.FINE)) log.fine("Returning stub = " + stub);
                return Response.ok(stub).build();
            } else {
                // TBD: Throw 404 ??? Or, just return an empty/null object???
                throw new ResourceNotFoundRsException("RobotsTextFile not found for the given siteUrl, " + siteUrl, resourceUri);
            }
        } catch(BadRequestException ex) {
            throw new BadRequestRsException(ex, resourceUri);
        } catch(RequestForbiddenException ex) {
            throw new RequestForbiddenRsException(ex, resourceUri);
        } catch(RequestConflictException ex) {
            throw new ServiceUnavailableRsException(ex, resourceUri);
        } catch(InternalServerErrorException ex) {
            throw new InternalServerErrorRsException(ex, resourceUri);
        } catch(BaseException ex) {
            throw new BaseResourceException(ex, resourceUri);
        } catch(Exception ex) {
            throw new InternalServerErrorRsException("Unknown internal error!", ex, resourceUri);
        }
    }

    

}
