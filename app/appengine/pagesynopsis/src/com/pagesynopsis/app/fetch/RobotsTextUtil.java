package com.pagesynopsis.app.fetch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.af.bean.RobotsTextFileBean;
import com.pagesynopsis.af.bean.RobotsTextGroupBean;
import com.pagesynopsis.af.util.URLUtil;
import com.pagesynopsis.helper.URLHelper;
import com.pagesynopsis.ws.RobotsTextFile;
import com.pagesynopsis.ws.RobotsTextGroup;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.exception.BadRequestException;


// Reference:
// https://developers.google.com/webmasters/control-crawl-index/docs/robots_txt
public class RobotsTextUtil
{
    private static final Logger log = Logger.getLogger(RobotsTextUtil.class.getName());

    // Static methods only.
    private RobotsTextUtil() {}

    
    // TBD:
    // Use StringTokenizer, java.util.Scanner, and/or Apache Commons Digester, ?????
    // .....
    
    
    // Temporary
    private static final String PREFIX_USERAGENT = "User-agent";
    private static final String PREFIX_ALLOW = "Allow";
    private static final String PREFIX_DISALLOW = "Disallow";
    private static final String PREFIX_SITEMAP = "Sitemap";
    // ...
    
    // State var..
    // state == 0 --> before starting
    // state == 1 --> in "preamble"  (user-agent)
    // state == 2 --> in "body"      (allow/disallow)
    // Q: encountering sitemap changes state ??? 
    //    No, except when state==1. it changes state from 1 to 2... ????
    // ..... Probably not. sitemap does not change the state....
    // .....

    
    public static RobotsTextFileBean parseRobotsText(String content)
    {
        return parseRobotsText(content, null);
    }
    public static RobotsTextFileBean parseRobotsText(String content, String siteUrl)
    {
        if(content == null || content.isEmpty()) {
            log.warning("content is not set.");
            return null;   // ????
        }
        
        RobotsTextFileBean robotsTextFile = null;
        try {
            BufferedReader reader = new BufferedReader(new StringReader(content));
            String line = null;
            int state = 0;
            List<RobotsTextGroup> groups = null;
            List<String> userAgentList = null;
            List<String> allowList = null;
            List<String> disallowList = null;
            List<String> sitemaps = null;
            while((line = reader.readLine()) != null) {
                line = line.trim();
                if(line.isEmpty()) {
                    // Skip
                    continue;
                }
                if(line.startsWith("#")) {
                    // Comment. Skip
                    continue;
                }
                String[] parts = line.split(":\\s*", 2);
                if(parts == null || parts.length != 2) {
                    // error
                    if(log.isLoggable(Level.WARNING)) log.warning("Invalid line = " + line);
                    continue;
                }
                
                String prefix = parts[0];
                String value = parts[1];
                if(prefix.equalsIgnoreCase(PREFIX_SITEMAP)) {
                    // TBD: Validate url???
                    // value should be an aboluste url...
                    if(log.isLoggable(Level.INFO)) log.info("Sitemap found: " + value);
                    if(sitemaps == null) {
                        sitemaps = new ArrayList<String>();
                    }
                    sitemaps.add(value);
                } else if(prefix.equalsIgnoreCase(PREFIX_USERAGENT)) {
                    
                    if(state==0 || state==1) {
 
                        if(userAgentList == null) {
                            userAgentList = new ArrayList<String>();
                        }
                        userAgentList.add(value);
                        
                        state = 1;
                        
                    } else {   // state == 2
                        
                        
                        // End of current group...
                        
                        if(userAgentList == null || userAgentList.isEmpty()) {
                            // ???
                            // this cannot happen....
                            log.warning("Invalid state: userAgentList is null/empty.");
                        } else {
                            if(groups == null) {
                                groups = new ArrayList<RobotsTextGroup>();
                            }
                            for(String ua : userAgentList) {
                                RobotsTextGroupBean group = new RobotsTextGroupBean();
                                String uuid = GUID.generate();
                                group.setUuid(uuid);
                                group.setUserAgent(ua);
                                
                                // Deep copy? 
                                // Sharing reference is OK?
                                if(allowList != null && !allowList.isEmpty()) {
                                    group.setAllows(allowList);
                                }
                                if(disallowList != null && !disallowList.isEmpty()) {
                                    group.setDisallows(disallowList);
                                }
                                
                                groups.add(group);
                                
                            }
                        }
                        
                        allowList = null;
                        disallowList = null;
                        userAgentList = new ArrayList<String>();
                        userAgentList.add(value);
                        // note: sitemaps is for the whole loop... Do not reset it...
                        state = 1;
                    }

                } else if(prefix.equalsIgnoreCase(PREFIX_ALLOW) || prefix.equalsIgnoreCase(PREFIX_DISALLOW)) {
                    
                    if(state==0) {
                        // Error
                        // ...
                    } else {  // state==1 || state==2
                        
                        if(prefix.equalsIgnoreCase(PREFIX_ALLOW)) {
                            if(allowList == null) {
                                allowList = new ArrayList<String>();
                            }
                            allowList.add(value);
                        } else if(prefix.equalsIgnoreCase(PREFIX_DISALLOW)) {
                            if(disallowList == null) {
                                disallowList = new ArrayList<String>();
                            }
                            disallowList.add(value);
                        }
                                                
                        state = 2;
                    }
 
                    
                } else {
                    // Error...
                    if(log.isLoggable(Level.WARNING)) log.warning("Invalid prefix = " + prefix + "; value = " + value);
                    continue;
                }
                
            }
            
            // Check if the "last" group is non-empty...
            if(userAgentList != null && !userAgentList.isEmpty()) {
                if(groups == null) {
                    groups = new ArrayList<RobotsTextGroup>();
                }
                for(String ua : userAgentList) {
                    RobotsTextGroupBean group = new RobotsTextGroupBean();
                    String uuid = GUID.generate();
                    group.setUuid(uuid);
                    group.setUserAgent(ua);
                    
                    // Deep copy? 
                    // Sharing reference is OK?
                    if(allowList != null && !allowList.isEmpty()) {
                        group.setAllows(allowList);
                    }
                    if(disallowList != null && !disallowList.isEmpty()) {
                        group.setDisallows(disallowList);
                    }
                    
                    groups.add(group);
                    
                }
            }
            
            robotsTextFile = new RobotsTextFileBean();
            String guid = GUID.generate();
            robotsTextFile.setGuid(guid);
            robotsTextFile.setContent(content);
            if(siteUrl != null) {
                // TBD: Validate siteUrl... ???
                robotsTextFile.setSiteUrl(siteUrl);
            }
            long now = System.currentTimeMillis();
            robotsTextFile.setLastCheckedTime(now);
            robotsTextFile.setLastUpdatedTime(now);
            if(sitemaps != null && !sitemaps.isEmpty()) {
                robotsTextFile.setSitemaps(sitemaps);
            }
            if(groups != null && !groups.isEmpty()) {
                robotsTextFile.setGroups(groups);
            }
            // ...
            
        } catch(IOException e) {
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Could not parse the robots.txt file content.", e);
        }

        return robotsTextFile;
    }
    
    
    
}
