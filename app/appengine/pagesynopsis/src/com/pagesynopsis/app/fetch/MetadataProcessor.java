package com.pagesynopsis.app.fetch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.pagesynopsis.af.bean.OgActorStructBean;
import com.pagesynopsis.af.bean.OgArticleBean;
import com.pagesynopsis.af.bean.OgAudioStructBean;
import com.pagesynopsis.af.bean.OgBlogBean;
import com.pagesynopsis.af.bean.OgBookBean;
import com.pagesynopsis.af.bean.OgImageStructBean;
import com.pagesynopsis.af.bean.OgMovieBean;
import com.pagesynopsis.af.bean.OgObjectBaseBean;
import com.pagesynopsis.af.bean.OgProfileBean;
import com.pagesynopsis.af.bean.OgTvEpisodeBean;
import com.pagesynopsis.af.bean.OgTvShowBean;
import com.pagesynopsis.af.bean.OgVideoBean;
import com.pagesynopsis.af.bean.OgVideoStructBean;
import com.pagesynopsis.af.bean.OgWebsiteBean;
import com.pagesynopsis.af.bean.OpenGraphMetaBean;
import com.pagesynopsis.af.bean.TwitterAppCardBean;
import com.pagesynopsis.af.bean.TwitterCardBaseBean;
import com.pagesynopsis.af.bean.TwitterCardMetaBean;
import com.pagesynopsis.af.bean.TwitterGalleryCardBean;
import com.pagesynopsis.af.bean.TwitterPhotoCardBean;
import com.pagesynopsis.af.bean.TwitterPlayerCardBean;
import com.pagesynopsis.af.bean.TwitterProductCardBean;
import com.pagesynopsis.af.bean.TwitterSummaryCardBean;
import com.pagesynopsis.app.common.FetchResult;
import com.pagesynopsis.common.OpenGraphType;
import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.common.TwitterCardType;
import com.pagesynopsis.ws.BaseException;
import com.pagesynopsis.ws.OgActorStruct;
import com.pagesynopsis.ws.OgArticle;
import com.pagesynopsis.ws.OgAudioStruct;
import com.pagesynopsis.ws.OgBlog;
import com.pagesynopsis.ws.OgBook;
import com.pagesynopsis.ws.OgImageStruct;
import com.pagesynopsis.ws.OgMovie;
import com.pagesynopsis.ws.OgObjectBase;
import com.pagesynopsis.ws.OgProfile;
import com.pagesynopsis.ws.OgTvEpisode;
import com.pagesynopsis.ws.OgTvShow;
import com.pagesynopsis.ws.OgVideo;
import com.pagesynopsis.ws.OgVideoStruct;
import com.pagesynopsis.ws.OgWebsite;
import com.pagesynopsis.ws.OpenGraphMeta;
import com.pagesynopsis.ws.TwitterAppCard;
import com.pagesynopsis.ws.TwitterCardBase;
import com.pagesynopsis.ws.TwitterCardMeta;
import com.pagesynopsis.ws.TwitterGalleryCard;
import com.pagesynopsis.ws.TwitterPhotoCard;
import com.pagesynopsis.ws.TwitterPlayerCard;
import com.pagesynopsis.ws.TwitterProductCard;
import com.pagesynopsis.ws.TwitterSummaryCard;
import com.pagesynopsis.ws.core.GUID;
import com.pagesynopsis.ws.exception.BadRequestException;
import com.pagesynopsis.ws.exception.InternalServerErrorException;


// Note: HTMLUnit API.
// http://htmlunit.sourceforge.net/apidocs/index.html
public class MetadataProcessor
{
    private static final Logger log = Logger.getLogger(MetadataProcessor.class.getName());   
    
    // temporary
    private static final long REFRESH_INTERVAL = 3600 * 1000L * 24 * 7;   // a week
    private static final long MIN_REFRESH_INTERVAL = 1800 * 1000L;        // half an hour...
    // temporary
    // If createdTime is older than < now - MAX_REFRESHING_AGE_MILLIS
    // Do not refresh it any more...
    // This should really depend on the current refresh counter (if we keep track of it)
    //                           and/or refresh internal, etc....
    private static final long MAX_REFRESHING_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    
    private MetadataProcessor()
    {
        init();
    }

    // Initialization-on-demand holder.
    private static class MetadataProcessorHolder
    {
        private static final MetadataProcessor INSTANCE = new MetadataProcessor();
    }

    // Singleton method
    public static MetadataProcessor getInstance()
    {
        return MetadataProcessorHolder.INSTANCE;
    }

    private void init()
    {
        // TBD:
        // ...
    }

    

    public OpenGraphMeta processOpenGraphMetaFetch(OpenGraphMeta openGraphMeta) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processOpenGraphMetaFetch() called with openGraphMeta = " + openGraphMeta);
        if(openGraphMeta == null) {
            // ????
            log.warning("openGraphMeta is null.");
            throw new BadRequestException("openGraphMeta is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  openGraphMeta.getTargetUrl();
        // ...
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((OpenGraphMetaBean) openGraphMeta).setLastCheckedTime(now);
            ((OpenGraphMetaBean) openGraphMeta).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            
            log.finer("Before calling connect()");
            
            Connection connection = Jsoup.connect(targetUrl);
            if(connection != null) {
                Document doc = connection.get();
                Response resp = connection.response();
                if(resp != null) {
                    statusCode = resp.statusCode();
                    ((OpenGraphMetaBean) openGraphMeta).setResponseCode(statusCode);
                    if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);
                    String contentType = resp.contentType();
                    ((OpenGraphMetaBean) openGraphMeta).setContentType(contentType);
                    if(log.isLoggable(Level.INFO)) log.info("contentType = " + contentType + " for targetUrl, " + targetUrl);
                    String contentLanguage = resp.header("Content-Language");
                    ((OpenGraphMetaBean) openGraphMeta).setLanguage(contentLanguage);
                    if(log.isLoggable(Level.INFO)) log.info("contentLanguage = " + contentLanguage + " for targetUrl, " + targetUrl);
                }

                if(doc != null) {
                    
                    Elements metaTitles = doc.select("title");
                    if(metaTitles != null && metaTitles.size() > 0) {
                        String pageTitle = metaTitles.first().text();
                        if(log.isLoggable(Level.FINE)) log.fine("pageTitle = " + pageTitle);
                        if(pageTitle != null) {
                            if(pageTitle.length() > 500) {
                                String truncatedTitle = pageTitle.substring(0, 500);
                                if(log.isLoggable(Level.INFO)) log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                                if(log.isLoggable(Level.INFO)) log.info("Truncated pageTitle = " + truncatedTitle);
                                pageTitle = truncatedTitle;
                            }
                            ((OpenGraphMetaBean) openGraphMeta).setPageTitle(pageTitle);
                        }                        
                    }
                    
//                    Elements metaDescriptions = doc.select("meta[name=description]");
//                    if(metaDescriptions != null && metaDescriptions.size() > 0) {
//                        String pageDescription = metaDescriptions.first().attr("content");
//                        if(log.isLoggable(Level.INFO)) log.info("pageDescription = " + pageDescription);
//                        if(pageDescription != null) {
//                            ((OpenGraphMetaBean) openGraphMeta).setPageDescription(pageDescription);
//                        }                        
//                    }

                    String ogType = null;
                    Elements metaOgTypes = doc.select("meta[property=og:type]");
                    if(metaOgTypes != null && metaOgTypes.size() > 0) {
                        ogType = metaOgTypes.first().attr("content");
                        if(log.isLoggable(Level.FINE)) log.fine("ogType = " + ogType);
                        if(ogType != null) {
                            ((OpenGraphMetaBean) openGraphMeta).setOgType(ogType);
                        }                        
                    }
                    
                    OgObjectBase ogObject = null;
                    if(OpenGraphType.OG_TYPE_PROFILE.equals(ogType)) {
                        ogObject = new OgProfileBean();                        
                    } else if(OpenGraphType.OG_TYPE_WEBSITE.equals(ogType)) {
                        ogObject = new OgWebsiteBean();
                    } else if(OpenGraphType.OG_TYPE_BLOG.equals(ogType)) {
                        ogObject = new OgBlogBean();
                    } else if(OpenGraphType.OG_TYPE_ARTICLE.equals(ogType)) {
                        ogObject = new OgArticleBean();
                    } else if(OpenGraphType.OG_TYPE_BOOK.equals(ogType)) {
                        ogObject = new OgBookBean();
                    } else if(OpenGraphType.OG_TYPE_VIDEO.equals(ogType)) {
                        ogObject = new OgVideoBean();
                    } else if(OpenGraphType.OG_TYPE_MOVIE.equals(ogType)) {
                        ogObject = new OgMovieBean();
                    } else if(OpenGraphType.OG_TYPE_TVSHOW.equals(ogType)) {
                        ogObject = new OgTvShowBean();
                    } else if(OpenGraphType.OG_TYPE_TVEPISODE.equals(ogType)) {
                        ogObject = new OgTvEpisodeBean();
                    } else {
                        if(log.isLoggable(Level.WARNING)) log.warning("Unrecognized ogType = " + ogType);
                    }

                    if(ogObject != null) {
                        ((OgObjectBaseBean) ogObject).setType(ogType);
                        ((OgObjectBaseBean) ogObject).setGuid(GUID.generate());  // ???
                        ((OgObjectBaseBean) ogObject).setCreatedTime(now);       // ???

                        Elements metaOgTitles = doc.select("meta[property=og:title]");
                        if(metaOgTitles != null && metaOgTitles.size() > 0) {
                            String ogTitle = metaOgTitles.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogTitle = " + ogTitle);
                            if(ogTitle != null) {
                                ((OgObjectBaseBean) ogObject).setTitle(ogTitle);
                            }                        
                        }

                        Elements metaOgUrls = doc.select("meta[property=og:url]");
                        if(metaOgUrls != null && metaOgUrls.size() > 0) {
                            String ogUrl = metaOgUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogUrl = " + ogUrl);
                            if(ogUrl != null) {
                                ((OgObjectBaseBean) ogObject).setUrl(ogUrl);
                            }                        
                        }

                        Elements metaOgDescriptions = doc.select("meta[property=og:description]");
                        if(metaOgDescriptions != null && metaOgDescriptions.size() > 0) {
                            String ogDescription = metaOgDescriptions.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogDescription = " + ogDescription);
                            if(ogDescription != null) {
                                ((OgObjectBaseBean) ogObject).setDescription(ogDescription);
                            }                        
                        }

                        Elements metaOgSiteNames = doc.select("meta[property=og:site_name]");
                        if(metaOgSiteNames != null && metaOgSiteNames.size() > 0) {
                            String ogSiteName = metaOgSiteNames.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogSiteName = " + ogSiteName);
                            if(ogSiteName != null) {
                                ((OgObjectBaseBean) ogObject).setSiteName(ogSiteName);
                            }                        
                        }

//                        Elements metaOgDeterminers = doc.select("meta[property=og:determiner]");
//                        if(metaOgDeterminers != null && metaOgDeterminers.size() > 0) {
//                            String ogDeterminer = metaOgDeterminers.first().attr("content");
//                            if(log.isLoggable(Level.FINE)) log.fine("ogDeterminer = " + ogDeterminer);
//                            if(ogDeterminer != null) {
//                                ((OgObjectBaseBean) ogObject).setDeterminer(ogDeterminer);
//                            }                        
//                        }


                        Elements metaOgLocales = doc.select("meta[property=og:locale]");
                        if(metaOgLocales != null && metaOgLocales.size() > 0) {
                            String ogLocale = metaOgLocales.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogLocale = " + ogLocale);
                            if(ogLocale != null) {
                                ((OgObjectBaseBean) ogObject).setLocale(ogLocale);
                            }                        
                        }
                        Elements metaOgLocaleAlternates = doc.select("meta[property=og:locale:alternate]");
                        if(metaOgLocaleAlternates != null && metaOgLocaleAlternates.size() > 0) {
                            List<String> ogLocaleAlternates = new ArrayList<String>();
                            for(int i=0; i<metaOgLocaleAlternates.size(); i++) {
                                String localeAlternate = metaOgLocaleAlternates.get(i).attr("content");
                                ogLocaleAlternates.add(localeAlternate);
                            }
                            if(log.isLoggable(Level.FINE)) log.fine("ogLocaleAlternates = " + ogLocaleAlternates);
                            if(ogLocaleAlternates != null) {
                                ((OgObjectBaseBean) ogObject).setLocaleAlternate(ogLocaleAlternates);
                            }                        
                        }

                        // FB tags..
                        Elements metaFbAdmins = doc.select("meta[property=fb:admins]");
                        if(metaFbAdmins != null && metaFbAdmins.size() > 0) {
                            List<String> fbAdminList = new ArrayList<String>();
                            for(int i=0; i<metaFbAdmins.size(); i++) {
                                String fbAdmin = metaFbAdmins.get(i).attr("content");
                                fbAdminList.add(fbAdmin);
                            }
                            if(log.isLoggable(Level.FINE)) log.fine("fbAdminList = " + fbAdminList);
                            if(fbAdminList != null) {
                                ((OgObjectBaseBean) ogObject).setFbAdmins(fbAdminList);
                            }                        
                        }
                        Elements metaFbAppIds = doc.select("meta[property=fb:appIds]");
                        if(metaFbAppIds != null && metaFbAppIds.size() > 0) {
                            List<String> fbAppIdList = new ArrayList<String>();
                            for(int i=0; i<metaFbAppIds.size(); i++) {
                                String fbAppId = metaFbAppIds.get(i).attr("content");
                                fbAppIdList.add(fbAppId);
                            }
                            if(log.isLoggable(Level.FINE)) log.fine("fbAppIdList = " + fbAppIdList);
                            if(fbAppIdList != null) {
                                ((OgObjectBaseBean) ogObject).setFbAppId(fbAppIdList);
                            }                        
                        }

                        

                        // Note:
                        // We do NOT currently support "arrays"...
                        // Only a single image/audio/video per page...
                        // Clearly, this can be a problem
                        // e.g.,  meta tags ordered this way
                        //        imageA, imageB, imageB:w, imageB:h
                        //        (we'll end up retrieving one image: {imageA, imageB:w, imageB:h},
                        //        which is wrong...
                        // ...
                        
                        
                        OgImageStructBean ogImageStruct = null;

                        Elements metaOgImageUrls = doc.select("meta[property=og:image]");
                        if(metaOgImageUrls == null || metaOgImageUrls.size() == 0) {
                            metaOgImageUrls = doc.select("meta[property=og:image:url]");
                        }
                        if(metaOgImageUrls != null && metaOgImageUrls.size() > 0) {
                            String ogImageUrl = metaOgImageUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogImageUrl = " + ogImageUrl);
                            if(ogImageUrl != null) {
                                if(ogImageStruct == null) {
                                    ogImageStruct = new OgImageStructBean();
                                }
                                ogImageStruct.setUrl(ogImageUrl);
                            }                        
                        }
                        Elements metaOgImageSecureUrls = doc.select("meta[property=og:image:secure_url]");
                        if(metaOgImageSecureUrls != null && metaOgImageSecureUrls.size() > 0) {
                            String ogImageSecureUrl = metaOgImageSecureUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogImageSecureUrl = " + ogImageSecureUrl);
                            if(ogImageSecureUrl != null) {
                                if(ogImageStruct == null) {
                                    ogImageStruct = new OgImageStructBean();
                                }
                                ogImageStruct.setSecureUrl(ogImageSecureUrl);
                            }                        
                        }
                        Elements metaOgImageTypes = doc.select("meta[property=og:image:type]");
                        if(metaOgImageTypes != null && metaOgImageTypes.size() > 0) {
                            String ogImageType = metaOgImageTypes.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogImageType = " + ogImageType);
                            if(ogImageType != null) {
                                if(ogImageStruct == null) {
                                    ogImageStruct = new OgImageStructBean();
                                }
                                ogImageStruct.setType(ogImageType);
                            }                        
                        }
                        Elements metaOgImageWidths = doc.select("meta[property=og:image:width]");
                        if(metaOgImageWidths != null && metaOgImageWidths.size() > 0) {
                            String ogImageWidth = metaOgImageWidths.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogImageWidth = " + ogImageWidth);
                            if(ogImageWidth != null) {
                                try {
                                    int width = Integer.parseInt(ogImageWidth);
                                    if(ogImageStruct == null) {
                                        ogImageStruct = new OgImageStructBean();
                                    }
                                    ogImageStruct.setWidth(width);
                                } catch(NumberFormatException e) {
                                    // Ignore...
                                }
                            }                        
                        }
                        Elements metaOgImageHeights = doc.select("meta[property=og:image:height]");
                        if(metaOgImageHeights != null && metaOgImageHeights.size() > 0) {
                            String ogImageHeight = metaOgImageHeights.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogImageHeight = " + ogImageHeight);
                            if(ogImageHeight != null) {
                                try {
                                    int height = Integer.parseInt(ogImageHeight);
                                    if(ogImageStruct == null) {
                                        ogImageStruct = new OgImageStructBean();
                                    }
                                    ogImageStruct.setHeight(height);
                                } catch(NumberFormatException e) {
                                    // Ignore...
                                }
                            }                        
                        }

                        if(ogImageStruct != null) {
                            String uuid = ogImageStruct.getUuid();
                            if(uuid == null || uuid.isEmpty()) {
                                uuid = GUID.generate();
                                ogImageStruct.setUuid(uuid);
                            }
                            List<OgImageStruct> imageList = new ArrayList<OgImageStruct>();
                            imageList.add(ogImageStruct);
                            ((OgObjectBaseBean) ogObject).setImage(imageList);
                        }
                        
                       
                        
                        OgAudioStructBean ogAudioStruct = null;

                        Elements metaOgAudioUrls = doc.select("meta[property=og:audio]");
                        if(metaOgAudioUrls == null || metaOgAudioUrls.size() == 0) {
                            metaOgAudioUrls = doc.select("meta[property=og:audio:url]");
                        }
                        if(metaOgAudioUrls != null && metaOgAudioUrls.size() > 0) {
                            String ogAudioUrl = metaOgAudioUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogAudioUrl = " + ogAudioUrl);
                            if(ogAudioUrl != null) {
                                if(ogAudioStruct == null) {
                                    ogAudioStruct = new OgAudioStructBean();
                                }
                                ogAudioStruct.setUrl(ogAudioUrl);
                            }                        
                        }
                        Elements metaOgAudioSecureUrls = doc.select("meta[property=og:audio:secure_url]");
                        if(metaOgAudioSecureUrls != null && metaOgAudioSecureUrls.size() > 0) {
                            String ogAudioSecureUrl = metaOgAudioSecureUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogAudioSecureUrl = " + ogAudioSecureUrl);
                            if(ogAudioSecureUrl != null) {
                                if(ogAudioStruct == null) {
                                    ogAudioStruct = new OgAudioStructBean();
                                }
                                ogAudioStruct.setSecureUrl(ogAudioSecureUrl);
                            }                        
                        }
                        Elements metaOgAudioTypes = doc.select("meta[property=og:audio:type]");
                        if(metaOgAudioTypes != null && metaOgAudioTypes.size() > 0) {
                            String ogAudioType = metaOgAudioTypes.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogAudioType = " + ogAudioType);
                            if(ogAudioType != null) {
                                if(ogAudioStruct == null) {
                                    ogAudioStruct = new OgAudioStructBean();
                                }
                                ogAudioStruct.setType(ogAudioType);
                            }                        
                        }

                        if(ogAudioStruct != null) {
                            String uuid = ogAudioStruct.getUuid();
                            if(uuid == null || uuid.isEmpty()) {
                                uuid = GUID.generate();
                                ogAudioStruct.setUuid(uuid);
                            }
                            List<OgAudioStruct> audioList = new ArrayList<OgAudioStruct>();
                            audioList.add(ogAudioStruct);
                            ((OgObjectBaseBean) ogObject).setAudio(audioList);
                        }
                        
                        
                        
                        OgVideoStructBean ogVideoStruct = null;

                        Elements metaOgVideoUrls = doc.select("meta[property=og:video]");
                        if(metaOgVideoUrls == null || metaOgVideoUrls.size() == 0) {
                            metaOgVideoUrls = doc.select("meta[property=og:video:url]");
                        }
                        if(metaOgVideoUrls != null && metaOgVideoUrls.size() > 0) {
                            String ogVideoUrl = metaOgVideoUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogVideoUrl = " + ogVideoUrl);
                            if(ogVideoUrl != null) {
                                if(ogVideoStruct == null) {
                                    ogVideoStruct = new OgVideoStructBean();
                                }
                                ogVideoStruct.setUrl(ogVideoUrl);
                            }                        
                        }
                        Elements metaOgVideoSecureUrls = doc.select("meta[property=og:video:secure_url]");
                        if(metaOgVideoSecureUrls != null && metaOgVideoSecureUrls.size() > 0) {
                            String ogVideoSecureUrl = metaOgVideoSecureUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogVideoSecureUrl = " + ogVideoSecureUrl);
                            if(ogVideoSecureUrl != null) {
                                if(ogVideoStruct == null) {
                                    ogVideoStruct = new OgVideoStructBean();
                                }
                                ogVideoStruct.setSecureUrl(ogVideoSecureUrl);
                            }                        
                        }
                        Elements metaOgVideoTypes = doc.select("meta[property=og:video:type]");
                        if(metaOgVideoTypes != null && metaOgVideoTypes.size() > 0) {
                            String ogVideoType = metaOgVideoTypes.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogVideoType = " + ogVideoType);
                            if(ogVideoType != null) {
                                if(ogVideoStruct == null) {
                                    ogVideoStruct = new OgVideoStructBean();
                                }
                                ogVideoStruct.setType(ogVideoType);
                            }                        
                        }
                        Elements metaOgVideoWidths = doc.select("meta[property=og:video:width]");
                        if(metaOgVideoWidths != null && metaOgVideoWidths.size() > 0) {
                            String ogVideoWidth = metaOgVideoWidths.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogVideoWidth = " + ogVideoWidth);
                            if(ogVideoWidth != null) {
                                try {
                                    int width = Integer.parseInt(ogVideoWidth);
                                    if(ogVideoStruct == null) {
                                        ogVideoStruct = new OgVideoStructBean();
                                    }
                                    ogVideoStruct.setWidth(width);
                                } catch(NumberFormatException e) {
                                    // Ignore...
                                }
                            }                        
                        }
                        Elements metaOgVideoHeights = doc.select("meta[property=og:video:height]");
                        if(metaOgVideoHeights != null && metaOgVideoHeights.size() > 0) {
                            String ogVideoHeight = metaOgVideoHeights.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogVideoHeight = " + ogVideoHeight);
                            if(ogVideoHeight != null) {
                                try {
                                    int height = Integer.parseInt(ogVideoHeight);
                                    if(ogVideoStruct == null) {
                                        ogVideoStruct = new OgVideoStructBean();
                                    }
                                    ogVideoStruct.setHeight(height);
                                } catch(NumberFormatException e) {
                                    // Ignore...
                                }
                            }                        
                        }

                        if(ogVideoStruct != null) {
                            String uuid = ogVideoStruct.getUuid();
                            if(uuid == null || uuid.isEmpty()) {
                                uuid = GUID.generate();
                                ogVideoStruct.setUuid(uuid);
                            }
                            List<OgVideoStruct> videoList = new ArrayList<OgVideoStruct>();
                            videoList.add(ogVideoStruct);
                            ((OgObjectBaseBean) ogObject).setVideo(videoList);
                        }
                        


                        // TBD:
                        // Namespace ???
                        // ...
                        
                        if(OpenGraphType.OG_TYPE_PROFILE.equals(ogType)) {

                            Elements metaOgFirstNames = doc.select("meta[property=profile:first_name]");
                            if(metaOgFirstNames != null && metaOgFirstNames.size() > 0) {
                                String ogFirstName = metaOgFirstNames.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogFirstName = " + ogFirstName);
                                if(ogFirstName != null) {
                                    ((OgProfileBean) ogObject).setFirstName(ogFirstName);
                                }                        
                            }
                            Elements metaOgLastNames = doc.select("meta[property=profile:last_name]");
                            if(metaOgLastNames != null && metaOgLastNames.size() > 0) {
                                String ogLastName = metaOgLastNames.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogLastName = " + ogLastName);
                                if(ogLastName != null) {
                                    ((OgProfileBean) ogObject).setLastName(ogLastName);
                                }                        
                            }
                            Elements metaOgUsernames = doc.select("meta[property=profile:username]");
                            if(metaOgUsernames != null && metaOgUsernames.size() > 0) {
                                String ogUsername = metaOgUsernames.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogUsername = " + ogUsername);
                                if(ogUsername != null) {
                                    ((OgProfileBean) ogObject).setUsername(ogUsername);
                                }                        
                            }
                            Elements metaOgGenders = doc.select("meta[property=profile:gender]");
                            if(metaOgGenders != null && metaOgGenders.size() > 0) {
                                String ogGender = metaOgGenders.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogGender = " + ogGender);
                                if(ogGender != null) {
                                    ((OgProfileBean) ogObject).setGender(ogGender);
                                }                        
                            }
                            
                            Elements metaFbProfileIds = doc.select("meta[property=fb:profile_id]");
                            if(metaFbProfileIds != null && metaFbProfileIds.size() > 0) {
                                String fbProfileId = metaFbProfileIds.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("fbProfileId = " + fbProfileId);
                                if(fbProfileId != null) {
                                    ((OgProfileBean) ogObject).setProfileId(fbProfileId);
                                }                        
                            }
                            

                            ((OpenGraphMetaBean) openGraphMeta).setOgProfile((OgProfile) ogObject);

                        } else if(OpenGraphType.OG_TYPE_WEBSITE.equals(ogType)) {

                            // No additional element..
                            // ...
                            ((OpenGraphMetaBean) openGraphMeta).setOgWebsite((OgWebsite) ogObject);

                        } else if(OpenGraphType.OG_TYPE_BLOG.equals(ogType)) {
                            
                            // No additional element..
                            // ...
                            ((OpenGraphMetaBean) openGraphMeta).setOgBlog((OgBlog) ogObject);

                        } else if(OpenGraphType.OG_TYPE_ARTICLE.equals(ogType)) {

                            Elements metaOgSections = doc.select("meta[property=article:section]");
                            if(metaOgSections != null && metaOgSections.size() > 0) {
                                String ogSection = metaOgSections.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogSection = " + ogSection);
                                if(ogSection != null) {
                                    ((OgArticleBean) ogObject).setSection(ogSection);
                                }                        
                            }
                            Elements metaOgTags = doc.select("meta[property=article:tag]");
                            if(metaOgTags != null && metaOgTags.size() > 0) {
                                List<String> ogTags = new ArrayList<String>();
                                for(int i=0; i<metaOgTags.size(); i++) {
                                    String localeAlternate = metaOgTags.get(i).attr("content");
                                    ogTags.add(localeAlternate);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogTags = " + ogTags);
                                if(ogTags != null) {
                                    ((OgArticleBean) ogObject).setTag(ogTags);
                                }                        
                            }
                            
                            Elements metaOgAuthors = doc.select("meta[property=article:author]");
                            if(metaOgAuthors != null && metaOgAuthors.size() > 0) {
                                List<String> ogAuthors = new ArrayList<String>();
                                for(int i=0; i<metaOgAuthors.size(); i++) {
                                    String localeAlternate = metaOgAuthors.get(i).attr("content");
                                    ogAuthors.add(localeAlternate);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogAuthors = " + ogAuthors);
                                if(ogAuthors != null) {
                                    ((OgArticleBean) ogObject).setAuthor(ogAuthors);
                                }                        
                            }
                            
                            Elements metaOgPublishedTimes = doc.select("meta[property=article:published_time]");
                            if(metaOgPublishedTimes != null && metaOgPublishedTimes.size() > 0) {
                                String ogPublishedTime = metaOgPublishedTimes.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogPublishedTime = " + ogPublishedTime);
                                if(ogPublishedTime != null) {
                                    ((OgArticleBean) ogObject).setPublishedDate(ogPublishedTime);
                                }                        
                            }
                            Elements metaOgModifiedTimes = doc.select("meta[property=article:modified_time]");
                            if(metaOgModifiedTimes != null && metaOgModifiedTimes.size() > 0) {
                                String ogModifiedTime = metaOgModifiedTimes.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogModifiedTime = " + ogModifiedTime);
                                if(ogModifiedTime != null) {
                                    ((OgArticleBean) ogObject).setModifiedDate(ogModifiedTime);
                                }                        
                            }
                            Elements metaOgExpirationTimes = doc.select("meta[property=article:expiration_time]");
                            if(metaOgExpirationTimes != null && metaOgExpirationTimes.size() > 0) {
                                String ogExpirationTime = metaOgExpirationTimes.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogExpirationTime = " + ogExpirationTime);
                                if(ogExpirationTime != null) {
                                    ((OgArticleBean) ogObject).setExpirationDate(ogExpirationTime);
                                }                        
                            }

                            ((OpenGraphMetaBean) openGraphMeta).setOgArticle((OgArticle) ogObject);
                            
                            
                        } else if(OpenGraphType.OG_TYPE_BOOK.equals(ogType)) {
                            // TBD:
                      
                            
                            Elements metaOgIsbns = doc.select("meta[property=book:isbn]");
                            if(metaOgIsbns != null && metaOgIsbns.size() > 0) {
                                String ogIsbn = metaOgIsbns.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogIsbn = " + ogIsbn);
                                if(ogIsbn != null) {
                                    ((OgBookBean) ogObject).setIsbn(ogIsbn);
                                }                        
                            }

                            Elements metaOgTags = doc.select("meta[property=book:tag]");
                            if(metaOgTags != null && metaOgTags.size() > 0) {
                                List<String> ogTags = new ArrayList<String>();
                                for(int i=0; i<metaOgTags.size(); i++) {
                                    String localeAlternate = metaOgTags.get(i).attr("content");
                                    ogTags.add(localeAlternate);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogTags = " + ogTags);
                                if(ogTags != null) {
                                    ((OgBookBean) ogObject).setTag(ogTags);
                                }                        
                            }
                            
                            Elements metaOgAuthors = doc.select("meta[property=book:author]");
                            if(metaOgAuthors != null && metaOgAuthors.size() > 0) {
                                List<String> ogAuthors = new ArrayList<String>();
                                for(int i=0; i<metaOgAuthors.size(); i++) {
                                    String author = metaOgAuthors.get(i).attr("content");
                                    ogAuthors.add(author);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogAuthors = " + ogAuthors);
                                if(ogAuthors != null) {
                                    ((OgBookBean) ogObject).setAuthor(ogAuthors);
                                }                        
                            }

                            Elements metaOgReleaseDates = doc.select("meta[property=book:release_date]");
                            if(metaOgReleaseDates != null && metaOgReleaseDates.size() > 0) {
                                String ogReleaseDate = metaOgReleaseDates.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogReleaseDate = " + ogReleaseDate);
                                if(ogReleaseDate != null) {
                                    ((OgBookBean) ogObject).setReleaseDate(ogReleaseDate);
                                }                        
                            }

                            ((OpenGraphMetaBean) openGraphMeta).setOgBook((OgBook) ogObject);

                        
                        } else if(OpenGraphType.OG_TYPE_VIDEO.equals(ogType)) {
                            // TBD:
                             
                            Elements metaOgDurations = doc.select("meta[property=video:duration]");
                            if(metaOgDurations != null && metaOgDurations.size() > 0) {
                                String ogDuration = metaOgDurations.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogDuration = " + ogDuration);
                                if(ogDuration != null) {
                                    try {
                                        int duration = Integer.parseInt(ogDuration);
                                        ((OgVideoBean) ogObject).setDuration(duration);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }

                            Elements metaOgTags = doc.select("meta[property=video:tag]");
                            if(metaOgTags != null && metaOgTags.size() > 0) {
                                List<String> ogTags = new ArrayList<String>();
                                for(int i=0; i<metaOgTags.size(); i++) {
                                    String tag = metaOgTags.get(i).attr("content");
                                    ogTags.add(tag);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogTags = " + ogTags);
                                if(ogTags != null) {
                                    ((OgVideoBean) ogObject).setTag(ogTags);
                                }                        
                            }

                            Elements metaOgReleaseDates = doc.select("meta[property=video:release_date]");
                            if(metaOgReleaseDates != null && metaOgReleaseDates.size() > 0) {
                                String ogReleaseDate = metaOgReleaseDates.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogReleaseDate = " + ogReleaseDate);
                                if(ogReleaseDate != null) {
                                    ((OgVideoBean) ogObject).setReleaseDate(ogReleaseDate);
                                }                        
                            }

                            Elements metaOgDirectors = doc.select("meta[property=video:director]");
                            if(metaOgDirectors != null && metaOgDirectors.size() > 0) {
                                List<String> ogDirectors = new ArrayList<String>();
                                for(int i=0; i<metaOgDirectors.size(); i++) {
                                    String director = metaOgDirectors.get(i).attr("content");
                                    ogDirectors.add(director);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogDirectors = " + ogDirectors);
                                if(ogDirectors != null) {
                                    ((OgVideoBean) ogObject).setDirector(ogDirectors);
                                }                        
                            }

                            Elements metaOgWriters = doc.select("meta[property=video:writer]");
                            if(metaOgWriters != null && metaOgWriters.size() > 0) {
                                List<String> ogWriters = new ArrayList<String>();
                                for(int i=0; i<metaOgWriters.size(); i++) {
                                   String writer = metaOgWriters.get(i).attr("content");
                                    ogWriters.add(writer);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogWriters = " + ogWriters);
                                if(ogWriters != null) {
                                    ((OgVideoBean) ogObject).setWriter(ogWriters);
                                }                        
                            }

                            // TBD: How to get a series of actor and actor:role 
                            Elements metaOgActors = doc.select("meta[property=video:actor]");
                            if(metaOgActors != null && metaOgActors.size() > 0) {                                
                                List<OgActorStruct> ogActors = new ArrayList<OgActorStruct>();
                                for(int i=0; i<metaOgActors.size(); i++) {
                                    String actorProfile = metaOgActors.get(i).attr("content");
                                    // TBD: Role???
                                    OgActorStructBean actorStruct = new OgActorStructBean();
                                    actorStruct.setProfile(actorProfile);
                                    ogActors.add(actorStruct);
                                }
                                if(log.isLoggable(Level.FINE)) log.fine("ogActors = " + ogActors);
                                if(ogActors != null) {
                                    ((OgVideoBean) ogObject).setActor(ogActors);
                                }                        
                            }
                            
                            
                            ((OpenGraphMetaBean) openGraphMeta).setOgVideo((OgVideo) ogObject);
                        } else if(OpenGraphType.OG_TYPE_MOVIE.equals(ogType)) {
                            // TBD:
                            
                           Elements metaOgDurations = doc.select("meta[property=video:duration]");
                           if(metaOgDurations != null && metaOgDurations.size() > 0) {
                               String ogDuration = metaOgDurations.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogDuration = " + ogDuration);
                               if(ogDuration != null) {
                                   try {
                                       int duration = Integer.parseInt(ogDuration);
                                       ((OgMovieBean) ogObject).setDuration(duration);
                                   } catch(NumberFormatException e) {
                                       // Ignore...
                                   }
                               }                        
                           }

                           Elements metaOgTags = doc.select("meta[property=video:tag]");
                           if(metaOgTags != null && metaOgTags.size() > 0) {
                               List<String> ogTags = new ArrayList<String>();
                               for(int i=0; i<metaOgTags.size(); i++) {
                                   String tag = metaOgTags.get(i).attr("content");
                                   ogTags.add(tag);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogTags = " + ogTags);
                               if(ogTags != null) {
                                   ((OgMovieBean) ogObject).setTag(ogTags);
                               }                        
                           }

                           Elements metaOgReleaseDates = doc.select("meta[property=video:release_date]");
                           if(metaOgReleaseDates != null && metaOgReleaseDates.size() > 0) {
                               String ogReleaseDate = metaOgReleaseDates.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogReleaseDate = " + ogReleaseDate);
                               if(ogReleaseDate != null) {
                                   ((OgMovieBean) ogObject).setReleaseDate(ogReleaseDate);
                               }                        
                           }

                           Elements metaOgDirectors = doc.select("meta[property=video:director]");
                           if(metaOgDirectors != null && metaOgDirectors.size() > 0) {
                               List<String> ogDirectors = new ArrayList<String>();
                               for(int i=0; i<metaOgDirectors.size(); i++) {
                                   String director = metaOgDirectors.get(i).attr("content");
                                   ogDirectors.add(director);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogDirectors = " + ogDirectors);
                               if(ogDirectors != null) {
                                   ((OgMovieBean) ogObject).setDirector(ogDirectors);
                               }                        
                           }

                           Elements metaOgWriters = doc.select("meta[property=video:writer]");
                           if(metaOgWriters != null && metaOgWriters.size() > 0) {
                               List<String> ogWriters = new ArrayList<String>();
                               for(int i=0; i<metaOgWriters.size(); i++) {
                                  String writer = metaOgWriters.get(i).attr("content");
                                   ogWriters.add(writer);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogWriters = " + ogWriters);
                               if(ogWriters != null) {
                                   ((OgMovieBean) ogObject).setWriter(ogWriters);
                               }                        
                           }

                           // TBD: How to get a series of actor and actor:role 
                           Elements metaOgActors = doc.select("meta[property=video:actor]");
                           if(metaOgActors != null && metaOgActors.size() > 0) {                                
                               List<OgActorStruct> ogActors = new ArrayList<OgActorStruct>();
                               for(int i=0; i<metaOgActors.size(); i++) {
                                   String actorProfile = metaOgActors.get(i).attr("content");
                                   // TBD: Role???
                                   OgActorStructBean actorStruct = new OgActorStructBean();
                                   actorStruct.setProfile(actorProfile);
                                   ogActors.add(actorStruct);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogActors = " + ogActors);
                               if(ogActors != null) {
                                   ((OgMovieBean) ogObject).setActor(ogActors);
                               }                        
                           }
                           
                            
                            ((OpenGraphMetaBean) openGraphMeta).setOgMovie((OgMovie) ogObject);
                        } else if(OpenGraphType.OG_TYPE_TVSHOW.equals(ogType)) {
                            // TBD:
                            
                            
                           Elements metaOgDurations = doc.select("meta[property=video:duration]");
                           if(metaOgDurations != null && metaOgDurations.size() > 0) {
                               String ogDuration = metaOgDurations.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogDuration = " + ogDuration);
                               if(ogDuration != null) {
                                   try {
                                       int duration = Integer.parseInt(ogDuration);
                                       ((OgTvShowBean) ogObject).setDuration(duration);
                                   } catch(NumberFormatException e) {
                                       // Ignore...
                                   }
                               }                        
                           }

                           Elements metaOgTags = doc.select("meta[property=video:tag]");
                           if(metaOgTags != null && metaOgTags.size() > 0) {
                               List<String> ogTags = new ArrayList<String>();
                               for(int i=0; i<metaOgTags.size(); i++) {
                                   String tag = metaOgTags.get(i).attr("content");
                                   ogTags.add(tag);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogTags = " + ogTags);
                               if(ogTags != null) {
                                   ((OgTvShowBean) ogObject).setTag(ogTags);
                               }                        
                           }

                           Elements metaOgReleaseDates = doc.select("meta[property=video:release_date]");
                           if(metaOgReleaseDates != null && metaOgReleaseDates.size() > 0) {
                               String ogReleaseDate = metaOgReleaseDates.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogReleaseDate = " + ogReleaseDate);
                               if(ogReleaseDate != null) {
                                   ((OgTvShowBean) ogObject).setReleaseDate(ogReleaseDate);
                               }                        
                           }

                           Elements metaOgDirectors = doc.select("meta[property=video:director]");
                           if(metaOgDirectors != null && metaOgDirectors.size() > 0) {
                               List<String> ogDirectors = new ArrayList<String>();
                               for(int i=0; i<metaOgDirectors.size(); i++) {
                                   String director = metaOgDirectors.get(i).attr("content");
                                   ogDirectors.add(director);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogDirectors = " + ogDirectors);
                               if(ogDirectors != null) {
                                   ((OgTvShowBean) ogObject).setDirector(ogDirectors);
                               }                        
                           }

                           Elements metaOgWriters = doc.select("meta[property=video:writer]");
                           if(metaOgWriters != null && metaOgWriters.size() > 0) {
                               List<String> ogWriters = new ArrayList<String>();
                               for(int i=0; i<metaOgWriters.size(); i++) {
                                  String writer = metaOgWriters.get(i).attr("content");
                                   ogWriters.add(writer);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogWriters = " + ogWriters);
                               if(ogWriters != null) {
                                   ((OgTvShowBean) ogObject).setWriter(ogWriters);
                               }                        
                           }

                           // TBD: How to get a series of actor and actor:role 
                           Elements metaOgActors = doc.select("meta[property=video:actor]");
                           if(metaOgActors != null && metaOgActors.size() > 0) {                                
                               List<OgActorStruct> ogActors = new ArrayList<OgActorStruct>();
                               for(int i=0; i<metaOgActors.size(); i++) {
                                   String actorProfile = metaOgActors.get(i).attr("content");
                                   // TBD: Role???
                                   OgActorStructBean actorStruct = new OgActorStructBean();
                                   actorStruct.setProfile(actorProfile);
                                   ogActors.add(actorStruct);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogActors = " + ogActors);
                               if(ogActors != null) {
                                   ((OgTvShowBean) ogObject).setActor(ogActors);
                               }                        
                           }
                            
                           ((OpenGraphMetaBean) openGraphMeta).setOgTvShow((OgTvShow) ogObject);
                        } else if(OpenGraphType.OG_TYPE_TVEPISODE.equals(ogType)) {
                            // TBD:
                            
                           Elements metaOgDurations = doc.select("meta[property=video:duration]");
                           if(metaOgDurations != null && metaOgDurations.size() > 0) {
                               String ogDuration = metaOgDurations.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogDuration = " + ogDuration);
                               if(ogDuration != null) {
                                   try {
                                       int duration = Integer.parseInt(ogDuration);
                                       ((OgTvEpisodeBean) ogObject).setDuration(duration);
                                   } catch(NumberFormatException e) {
                                       // Ignore...
                                   }
                               }                        
                           }

                           Elements metaOgTags = doc.select("meta[property=video:tag]");
                           if(metaOgTags != null && metaOgTags.size() > 0) {
                               List<String> ogTags = new ArrayList<String>();
                               for(int i=0; i<metaOgTags.size(); i++) {
                                   String tag = metaOgTags.get(i).attr("content");
                                   ogTags.add(tag);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogTags = " + ogTags);
                               if(ogTags != null) {
                                   ((OgTvEpisodeBean) ogObject).setTag(ogTags);
                               }                        
                           }

                           Elements metaOgSeriess = doc.select("meta[property=video:series]");
                           if(metaOgSeriess != null && metaOgSeriess.size() > 0) {
                               String ogSeries = metaOgSeriess.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogSeries = " + ogSeries);
                               if(ogSeries != null) {
                                   ((OgTvEpisodeBean) ogObject).setSeries(ogSeries);
                               }                        
                           }

                           Elements metaOgReleaseDates = doc.select("meta[property=video:release_date]");
                           if(metaOgReleaseDates != null && metaOgReleaseDates.size() > 0) {
                               String ogReleaseDate = metaOgReleaseDates.first().attr("content");
                               if(log.isLoggable(Level.FINE)) log.fine("ogReleaseDate = " + ogReleaseDate);
                               if(ogReleaseDate != null) {
                                   ((OgTvEpisodeBean) ogObject).setReleaseDate(ogReleaseDate);
                               }                        
                           }

                           Elements metaOgDirectors = doc.select("meta[property=video:director]");
                           if(metaOgDirectors != null && metaOgDirectors.size() > 0) {
                               List<String> ogDirectors = new ArrayList<String>();
                               for(int i=0; i<metaOgDirectors.size(); i++) {
                                   String director = metaOgDirectors.get(i).attr("content");
                                   ogDirectors.add(director);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogDirectors = " + ogDirectors);
                               if(ogDirectors != null) {
                                   ((OgTvEpisodeBean) ogObject).setDirector(ogDirectors);
                               }                        
                           }

                           Elements metaOgWriters = doc.select("meta[property=video:writer]");
                           if(metaOgWriters != null && metaOgWriters.size() > 0) {
                               List<String> ogWriters = new ArrayList<String>();
                               for(int i=0; i<metaOgWriters.size(); i++) {
                                  String writer = metaOgWriters.get(i).attr("content");
                                   ogWriters.add(writer);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogWriters = " + ogWriters);
                               if(ogWriters != null) {
                                   ((OgTvEpisodeBean) ogObject).setWriter(ogWriters);
                               }                        
                           }

                           // TBD: How to get a series of actor and actor:role 
                           Elements metaOgActors = doc.select("meta[property=video:actor]");
                           if(metaOgActors != null && metaOgActors.size() > 0) {                                
                               List<OgActorStruct> ogActors = new ArrayList<OgActorStruct>();
                               for(int i=0; i<metaOgActors.size(); i++) {
                                   String actorProfile = metaOgActors.get(i).attr("content");
                                   // TBD: Role???
                                   OgActorStructBean actorStruct = new OgActorStructBean();
                                   actorStruct.setProfile(actorProfile);
                                   ogActors.add(actorStruct);
                               }
                               if(log.isLoggable(Level.FINE)) log.fine("ogActors = " + ogActors);
                               if(ogActors != null) {
                                   ((OgTvEpisodeBean) ogObject).setActor(ogActors);
                               }                        
                           }
                           
                            
                            ((OpenGraphMetaBean) openGraphMeta).setOgTvEpisode((OgTvEpisode) ogObject);
                        } else {
                            // ???
                            if(log.isLoggable(Level.WARNING)) log.warning("No OpenGraph metadata set for ogType = " + ogType);
                        }
                    }
                    
                    ((OpenGraphMetaBean) openGraphMeta).setLastFetchResult(FetchResult.RES_SUCCESS);
                    ((OpenGraphMetaBean) openGraphMeta).setLastUpdatedTime(now);  // ???
                    // etc...

                } else {
                    // Can this happen ???
                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to fetch document for targetUrl = " + targetUrl);
                }
            } else {
                // TBD ....
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to connect to targetUrl = " + targetUrl);
                ((OpenGraphMetaBean) openGraphMeta).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }
            

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = openGraphMeta.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = openGraphMeta.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((OpenGraphMetaBean) openGraphMeta).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((OpenGraphMetaBean) openGraphMeta).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((OpenGraphMetaBean) openGraphMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = openGraphMeta.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((OpenGraphMetaBean) openGraphMeta).setRefreshInterval(refreshInterval);
                    }
                    ((OpenGraphMetaBean) openGraphMeta).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for openGraphMeta = " + openGraphMeta.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for openGraphMeta = " + openGraphMeta.getGuid());
            }
            // ...

        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (Exception e) {
            throw new BaseException("Unknown rrror processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
        }
        
        log.finer("END: processOpenGraphMetaFetch()");
        return openGraphMeta;
    }
    
    
    

    public TwitterCardMeta processTwitterCardMetaFetch(TwitterCardMeta twitterCardMeta) throws BaseException
    {
        if(log.isLoggable(Level.FINER)) log.finer("BEGIN: processTwitterCardMetaFetch() called with twitterCardMeta = " + twitterCardMeta);
        if(twitterCardMeta == null) {
            // ????
            log.warning("twitterCardMeta is null.");
            throw new BadRequestException("twitterCardMeta is null.");
        }

        // TargetUrl/input cannot be null
        String targetUrl =  twitterCardMeta.getTargetUrl();
        // ...
        
        int statusCode = -1;  // ???
        try {
            // Starting processing...
            long now = System.currentTimeMillis();
            ((TwitterCardMetaBean) twitterCardMeta).setLastCheckedTime(now);
            ((TwitterCardMetaBean) twitterCardMeta).setLastFetchResult(FetchResult.RES_UNKNOWN);  // ???
            //((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_PROCESSING);  // ???
            
            log.finer("Before calling connect()");
            
            Connection connection = Jsoup.connect(targetUrl);
            if(connection != null) {
                Document doc = connection.get();
                Response resp = connection.response();
                if(resp != null) {
                    statusCode = resp.statusCode();
                    ((TwitterCardMetaBean) twitterCardMeta).setResponseCode(statusCode);
                    if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for targetUrl, " + targetUrl);
                    String contentType = resp.contentType();
                    ((TwitterCardMetaBean) twitterCardMeta).setContentType(contentType);
                    if(log.isLoggable(Level.INFO)) log.info("contentType = " + contentType + " for targetUrl, " + targetUrl);
                    String contentLanguage = resp.header("Content-Language");
                    ((TwitterCardMetaBean) twitterCardMeta).setLanguage(contentLanguage);
                    if(log.isLoggable(Level.INFO)) log.info("contentLanguage = " + contentLanguage + " for targetUrl, " + targetUrl);
                }

                if(doc != null) {
                    
                    Elements metaTitles = doc.select("title");
                    if(metaTitles != null && metaTitles.size() > 0) {
                        String pageTitle = metaTitles.first().text();
                        if(log.isLoggable(Level.FINE)) log.fine("pageTitle = " + pageTitle);
                        if(pageTitle != null) {
                            if(pageTitle.length() > 500) {
                                String truncatedTitle = pageTitle.substring(0, 500);
                                if(log.isLoggable(Level.INFO)) log.info("Title has been truncated: Original pageTitle = " + pageTitle);
                                if(log.isLoggable(Level.INFO)) log.info("Truncated pageTitle = " + truncatedTitle);
                                pageTitle = truncatedTitle;
                            }
                            ((TwitterCardMetaBean) twitterCardMeta).setPageTitle(pageTitle);
                        }                        
                    }
                    
//                    Elements metaDescriptions = doc.select("meta[name=description]");
//                    if(metaDescriptions != null && metaDescriptions.size() > 0) {
//                        String pageDescription = metaDescriptions.first().attr("content");
//                        if(log.isLoggable(Level.FINE)) log.fine("pageDescription = " + pageDescription);
//                        if(pageDescription != null) {
//                            ((TwitterCardMetaBean) twitterCardMeta).setPageDescription(pageDescription);
//                        }                        
//                    }

                    String cardType = null;
                    Elements metaTcTypes = doc.select("meta[name=twitter:card]");
                    if(metaTcTypes != null && metaTcTypes.size() > 0) {
                        cardType = metaTcTypes.first().attr("content");
                        if(log.isLoggable(Level.FINE)) log.fine("cardType = " + cardType);
                        if(cardType != null) {
                            ((TwitterCardMetaBean) twitterCardMeta).setCardType(cardType);
                        }                        
                    }
                    
                    TwitterCardBase tcMetadata = null;
                    if(TwitterCardType.TC_TYPE_SUMMARY.equals(cardType)) {
                        tcMetadata = new TwitterSummaryCardBean();
                    } else if(TwitterCardType.TC_TYPE_PHOTO.equals(cardType)) {
                        tcMetadata = new TwitterPhotoCardBean();
                    } else if(TwitterCardType.TC_TYPE_GALLERY.equals(cardType)) {
                        tcMetadata = new TwitterGalleryCardBean();
                    } else if(TwitterCardType.TC_TYPE_APP.equals(cardType)) {
                        tcMetadata = new TwitterAppCardBean();
                    } else if(TwitterCardType.TC_TYPE_PLAYER.equals(cardType)) {
                        tcMetadata = new TwitterPlayerCardBean();
                    } else if(TwitterCardType.TC_TYPE_PRODUCT.equals(cardType)) {
                        tcMetadata = new TwitterProductCardBean();
                    } else {
                        if(log.isLoggable(Level.WARNING)) log.warning("Unrecognized cardType = " + cardType);
                    }

                    if(tcMetadata != null) {
                        ((TwitterCardBaseBean) tcMetadata).setCard(cardType);
                        ((TwitterCardBaseBean) tcMetadata).setGuid(GUID.generate());  // ???
                        ((TwitterCardBaseBean) tcMetadata).setCreatedTime(now);       // ???

                        Elements metaTcTitles = doc.select("meta[name=twitter:title]");
                        if(metaTcTitles == null || metaTcTitles.size() == 0) {
                            metaTcTitles = doc.select("meta[property=og:title]");
                        }
                        if(metaTcTitles != null && metaTcTitles.size() > 0) {
                            String ogTitle = metaTcTitles.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogTitle = " + ogTitle);
                            if(ogTitle != null) {
                                ((TwitterCardBaseBean) tcMetadata).setTitle(ogTitle);
                            }                        
                        }

                        Elements metaTcUrls = doc.select("meta[name=twitter:url]");
                        if(metaTcUrls == null || metaTcUrls.size() == 0) {
                            metaTcUrls = doc.select("meta[property=og:url]");
                        }
                        if(metaTcUrls != null && metaTcUrls.size() > 0) {
                            String ogUrl = metaTcUrls.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogUrl = " + ogUrl);
                            if(ogUrl != null) {
                                ((TwitterCardBaseBean) tcMetadata).setUrl(ogUrl);
                            }                        
                        }

                        Elements metaTcDescriptions = doc.select("meta[name=twitter:description]");
                        if(metaTcDescriptions == null || metaTcDescriptions.size() == 0) {
                            metaTcDescriptions = doc.select("meta[property=og:description]");
                        }
                        if(metaTcDescriptions != null && metaTcDescriptions.size() > 0) {
                            String ogDescription = metaTcDescriptions.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogDescription = " + ogDescription);
                            if(ogDescription != null) {
                                ((TwitterCardBaseBean) tcMetadata).setDescription(ogDescription);
                            }                        
                        }

                        Elements metaTcSites = doc.select("meta[name=twitter:site]");
                        if(metaTcSites != null && metaTcSites.size() > 0) {
                            String ogSite = metaTcSites.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogSite = " + ogSite);
                            if(ogSite != null) {
                                ((TwitterCardBaseBean) tcMetadata).setSite(ogSite);
                            }                        
                        }
                        Elements metaTcSiteIds = doc.select("meta[name=twitter:site:id]");
                        if(metaTcSiteIds != null && metaTcSiteIds.size() > 0) {
                            String ogSiteId = metaTcSiteIds.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogSiteId = " + ogSiteId);
                            if(ogSiteId != null) {
                                ((TwitterCardBaseBean) tcMetadata).setSiteId(ogSiteId);
                            }                        
                        }

                        Elements metaTcCreators = doc.select("meta[name=twitter:creator]");
                        if(metaTcCreators != null && metaTcCreators.size() > 0) {
                            String ogCreator = metaTcCreators.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogCreator = " + ogCreator);
                            if(ogCreator != null) {
                                ((TwitterCardBaseBean) tcMetadata).setCreator(ogCreator);
                            }                        
                        }
                        Elements metaTcCreatorIds = doc.select("meta[name=twitter:creator:id]");
                        if(metaTcCreatorIds != null && metaTcCreatorIds.size() > 0) {
                            String ogCreatorId = metaTcCreatorIds.first().attr("content");
                            if(log.isLoggable(Level.FINE)) log.fine("ogCreatorId = " + ogCreatorId);
                            if(ogCreatorId != null) {
                                ((TwitterCardBaseBean) tcMetadata).setCreatorId(ogCreatorId);
                            }                        
                        }


                        // TBD:
                        // Namespace ???
                        // ...
                        
                        if(TwitterCardType.TC_TYPE_SUMMARY.equals(cardType)) {

                            Elements metaTcImages = doc.select("meta[name=twitter:image]");
                            if(metaTcImages == null || metaTcImages.size() == 0) {
                                metaTcImages = doc.select("meta[property=og:image]");
                            }
                            if(metaTcImages != null && metaTcImages.size() > 0) {
                                String ogImage = metaTcImages.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("ogImage = " + ogImage);
                                if(ogImage != null) {
                                    ((TwitterSummaryCardBean) tcMetadata).setImage(ogImage);
                                }                        
                            }

                            Elements metaTcImageWidths = doc.select("meta[name=twitter:image:width]");
                            if(metaTcImageWidths == null || metaTcImageWidths.size() == 0) {
                                metaTcImageWidths = doc.select("meta[property=og:image:width]");
                            }
                            if(metaTcImageWidths != null && metaTcImageWidths.size() > 0) {
                                String tcImageWidth = metaTcImageWidths.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageWidth = " + tcImageWidth);
                                if(tcImageWidth != null) {
                                    try {
                                        int width = Integer.parseInt(tcImageWidth);
                                        ((TwitterPhotoCardBean) tcMetadata).setImageWidth(width);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }
                            Elements metaTcImageHeights = doc.select("meta[name=twitter:image:height]");
                            if(metaTcImageHeights == null || metaTcImageHeights.size() == 0) {
                                metaTcImageHeights = doc.select("meta[property=og:image:height]");
                            }
                            if(metaTcImageHeights != null && metaTcImageHeights.size() > 0) {
                                String tcImageHeight = metaTcImageHeights.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageHeight = " + tcImageHeight);
                                if(tcImageHeight != null) {
                                    try {
                                        int height = Integer.parseInt(tcImageHeight);
                                        ((TwitterPhotoCardBean) tcMetadata).setImageHeight(height);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }


                            ((TwitterCardMetaBean) twitterCardMeta).setSummaryCard((TwitterSummaryCard) tcMetadata);

                        } else if(TwitterCardType.TC_TYPE_PHOTO.equals(cardType)) {


                            Elements metaTcImages = doc.select("meta[name=twitter:image]");
                            if(metaTcImages == null || metaTcImages.size() == 0) {
                                metaTcImages = doc.select("meta[property=og:image]");
                            }
                            if(metaTcImages != null && metaTcImages.size() > 0) {
                                String tcImage = metaTcImages.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage = " + tcImage);
                                if(tcImage != null) {
                                    ((TwitterPhotoCardBean) tcMetadata).setImage(tcImage);
                                }                        
                            }

                            Elements metaTcImageWidths = doc.select("meta[name=twitter:image:width]");
                            if(metaTcImageWidths == null || metaTcImageWidths.size() == 0) {
                                metaTcImageWidths = doc.select("meta[property=og:image:width]");
                            }
                            if(metaTcImageWidths != null && metaTcImageWidths.size() > 0) {
                                String tcImageWidth = metaTcImageWidths.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageWidth = " + tcImageWidth);
                                if(tcImageWidth != null) {
                                    try {
                                        int width = Integer.parseInt(tcImageWidth);
                                        ((TwitterPhotoCardBean) tcMetadata).setImageWidth(width);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }
                            Elements metaTcImageHeights = doc.select("meta[name=twitter:image:height]");
                            if(metaTcImageHeights == null || metaTcImageHeights.size() == 0) {
                                metaTcImageHeights = doc.select("meta[property=og:image:height]");
                            }
                            if(metaTcImageHeights != null && metaTcImageHeights.size() > 0) {
                                String tcImageHeight = metaTcImageHeights.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageHeight = " + tcImageHeight);
                                if(tcImageHeight != null) {
                                    try {
                                        int height = Integer.parseInt(tcImageHeight);
                                        ((TwitterPhotoCardBean) tcMetadata).setImageHeight(height);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }


                            
                            ((TwitterCardMetaBean) twitterCardMeta).setPhotoCard((TwitterPhotoCard) tcMetadata);

                        } else if(TwitterCardType.TC_TYPE_GALLERY.equals(cardType)) {


                            Elements metaTcImages0 = doc.select("meta[name=twitter:image0]");
                            if(metaTcImages0 != null && metaTcImages0.size() > 0) {
                                String tcImage0 = metaTcImages0.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage0 = " + tcImage0);
                                if(tcImage0 != null) {
                                    ((TwitterGalleryCardBean) tcMetadata).setImage0(tcImage0);
                                }
                            }
                            Elements metaTcImages1 = doc.select("meta[name=twitter:image1]");
                            if(metaTcImages1 != null && metaTcImages1.size() > 0) {
                                String tcImage1 = metaTcImages1.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage1 = " + tcImage1);
                                if(tcImage1 != null) {
                                    ((TwitterGalleryCardBean) tcMetadata).setImage1(tcImage1);
                                }
                            }
                            Elements metaTcImages2 = doc.select("meta[name=twitter:image2]");
                            if(metaTcImages2 != null && metaTcImages2.size() > 0) {
                                String tcImage2 = metaTcImages2.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage2 = " + tcImage2);
                                if(tcImage2 != null) {
                                    ((TwitterGalleryCardBean) tcMetadata).setImage2(tcImage2);
                                }
                            }
                            Elements metaTcImages3 = doc.select("meta[name=twitter:image3]");
                            if(metaTcImages3 != null && metaTcImages3.size() > 0) {
                                String tcImage3 = metaTcImages3.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage3 = " + tcImage3);
                                if(tcImage3 != null) {
                                    ((TwitterGalleryCardBean) tcMetadata).setImage3(tcImage3);
                                }
                            }

                            
                            ((TwitterCardMetaBean) twitterCardMeta).setGalleryCard((TwitterGalleryCard) tcMetadata);


                        } else if(TwitterCardType.TC_TYPE_APP.equals(cardType)) {

                            Elements metaTcImages = doc.select("meta[name=twitter:image]");
                            if(metaTcImages == null || metaTcImages.size() == 0) {
                                metaTcImages = doc.select("meta[property=og:image]");
                            }
                            if(metaTcImages != null && metaTcImages.size() > 0) {
                                String tcImage = metaTcImages.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage = " + tcImage);
                                if(tcImage != null) {
                                    ((TwitterAppCardBean) tcMetadata).setImage(tcImage);
                                }                        
                            }

                            Elements metaTcImageWidths = doc.select("meta[name=twitter:image:width]");
                            if(metaTcImageWidths == null || metaTcImageWidths.size() == 0) {
                                metaTcImageWidths = doc.select("meta[property=og:image:width]");
                            }
                            if(metaTcImageWidths != null && metaTcImageWidths.size() > 0) {
                                String tcImageWidth = metaTcImageWidths.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageWidth = " + tcImageWidth);
                                if(tcImageWidth != null) {
                                    try {
                                        int width = Integer.parseInt(tcImageWidth);
                                        ((TwitterAppCardBean) tcMetadata).setImageWidth(width);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }
                            Elements metaTcImageHeights = doc.select("meta[name=twitter:image:height]");
                            if(metaTcImageHeights == null || metaTcImageHeights.size() == 0) {
                                metaTcImageHeights = doc.select("meta[property=og:image:height]");
                            }
                            if(metaTcImageHeights != null && metaTcImageHeights.size() > 0) {
                                String tcImageHeight = metaTcImageHeights.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageHeight = " + tcImageHeight);
                                if(tcImageHeight != null) {
                                    try {
                                        int height = Integer.parseInt(tcImageHeight);
                                        ((TwitterAppCardBean) tcMetadata).setImageHeight(height);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }


                            // TBD:
                            // iphone
                            // ipad
                            // google play app info
                            // ...

                            
                            ((TwitterCardMetaBean) twitterCardMeta).setAppCard((TwitterAppCard) tcMetadata);
     
                        } else if(TwitterCardType.TC_TYPE_PLAYER.equals(cardType)) {

                            Elements metaTcImages = doc.select("meta[name=twitter:image]");
                            if(metaTcImages == null || metaTcImages.size() == 0) {
                                metaTcImages = doc.select("meta[property=og:image]");
                            }
                            if(metaTcImages != null && metaTcImages.size() > 0) {
                                String tcImage = metaTcImages.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage = " + tcImage);
                                if(tcImage != null) {
                                    ((TwitterPlayerCardBean) tcMetadata).setImage(tcImage);
                                }                        
                            }

                            Elements metaTcPlayers = doc.select("meta[name=twitter:player]");
                            if(metaTcPlayers != null && metaTcPlayers.size() > 0) {
                                String tcPlayer = metaTcPlayers.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcPlayer = " + tcPlayer);
                                if(tcPlayer != null) {
                                    ((TwitterPlayerCardBean) tcMetadata).setPlayer(tcPlayer);
                                }                        
                            }
                            Elements metaTcPlayerWidths = doc.select("meta[name=twitter:player:width]");
                            if(metaTcPlayerWidths != null && metaTcPlayerWidths.size() > 0) {
                                String tcPlayerWidth = metaTcPlayerWidths.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcPlayerWidth = " + tcPlayerWidth);
                                if(tcPlayerWidth != null) {
                                    try {
                                        int width = Integer.parseInt(tcPlayerWidth);
                                        ((TwitterPlayerCardBean) tcMetadata).setPlayerWidth(width);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }
                            Elements metaTcPlayerHeights = doc.select("meta[name=twitter:player:height]");
                            if(metaTcPlayerHeights != null && metaTcPlayerHeights.size() > 0) {
                                String tcPlayerHeight = metaTcPlayerHeights.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcPlayerHeight = " + tcPlayerHeight);
                                if(tcPlayerHeight != null) {
                                    try {
                                        int height = Integer.parseInt(tcPlayerHeight);
                                        ((TwitterPlayerCardBean) tcMetadata).setPlayerHeight(height);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }

                            Elements metaTcPlayerStreams = doc.select("meta[name=twitter:player:stream]");
                            if(metaTcPlayerStreams != null && metaTcPlayerStreams.size() > 0) {
                                String tcPlayerStream = metaTcPlayerStreams.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcPlayerStream = " + tcPlayerStream);
                                if(tcPlayerStream != null) {
                                    ((TwitterPlayerCardBean) tcMetadata).setPlayerStream(tcPlayerStream);
                                }                        
                            }
                            Elements metaTcPlayerStreamContentTypes = doc.select("meta[name=twitter:player:stream:content_type]");
                            if(metaTcPlayerStreamContentTypes != null && metaTcPlayerStreamContentTypes.size() > 0) {
                                String tcPlayerStreamContentType = metaTcPlayerStreamContentTypes.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcPlayerStreamContentType = " + tcPlayerStreamContentType);
                                if(tcPlayerStreamContentType != null) {
                                    ((TwitterPlayerCardBean) tcMetadata).setPlayerStreamContentType(tcPlayerStreamContentType);
                                }                        
                            }
                            
                            ((TwitterCardMetaBean) twitterCardMeta).setPlayerCard((TwitterPlayerCard) tcMetadata);

                        } else if(TwitterCardType.TC_TYPE_PRODUCT.equals(cardType)) {

                            Elements metaTcImages = doc.select("meta[name=twitter:image]");
                            if(metaTcImages == null || metaTcImages.size() == 0) {
                                metaTcImages = doc.select("meta[property=og:image]");
                            }
                            if(metaTcImages != null && metaTcImages.size() > 0) {
                                String tcImage = metaTcImages.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImage = " + tcImage);
                                if(tcImage != null) {
                                    ((TwitterProductCardBean) tcMetadata).setImage(tcImage);
                                }                        
                            }

                            Elements metaTcImageWidths = doc.select("meta[name=twitter:image:width]");
                            if(metaTcImageWidths == null || metaTcImageWidths.size() == 0) {
                                metaTcImageWidths = doc.select("meta[property=og:image:width]");
                            }
                            if(metaTcImageWidths != null && metaTcImageWidths.size() > 0) {
                                String tcImageWidth = metaTcImageWidths.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageWidth = " + tcImageWidth);
                                if(tcImageWidth != null) {
                                    try {
                                        int width = Integer.parseInt(tcImageWidth);
                                        ((TwitterProductCardBean) tcMetadata).setImageWidth(width);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }
                            Elements metaTcImageHeights = doc.select("meta[name=twitter:image:height]");
                            if(metaTcImageHeights == null || metaTcImageHeights.size() == 0) {
                                metaTcImageHeights = doc.select("meta[property=og:image:height]");
                            }
                            if(metaTcImageHeights != null && metaTcImageHeights.size() > 0) {
                                String tcImageHeight = metaTcImageHeights.first().attr("content");
                                if(log.isLoggable(Level.FINE)) log.fine("tcImageHeight = " + tcImageHeight);
                                if(tcImageHeight != null) {
                                    try {
                                        int height = Integer.parseInt(tcImageHeight);
                                        ((TwitterProductCardBean) tcMetadata).setImageHeight(height);
                                    } catch(NumberFormatException e) {
                                        // Ignore...
                                    }
                                }                        
                            }


                            // TBD:
                            // data1, data2
                            // ...
                            // ...

                            
                            ((TwitterCardMetaBean) twitterCardMeta).setProductCard((TwitterProductCard) tcMetadata);

                        } else {
                            // ???
                            if(log.isLoggable(Level.WARNING)) log.warning("No TwitterCard set for cardType = " + cardType);
                        }
                    }                   
                    
                    ((TwitterCardMetaBean) twitterCardMeta).setLastFetchResult(FetchResult.RES_SUCCESS);
                    ((TwitterCardMetaBean) twitterCardMeta).setLastUpdatedTime(now);  // ???
                    // etc...

                } else {
                    // Can this happen ???
                    if(log.isLoggable(Level.WARNING)) log.warning("Failed to fetch document for targetUrl = " + targetUrl);
                }
            } else {
                // TBD ....
                if(log.isLoggable(Level.WARNING)) log.warning("Failed to connect to targetUrl = " + targetUrl);
                ((TwitterCardMetaBean) twitterCardMeta).setLastFetchResult(FetchResult.RES_FAILURE);
                // etc...
                // What to do???
                // ????
            }
            

            // TBD:
            // Update these only if RefreshStatus != STATUS_NOREFRESH???
            Integer refreshStatus = twitterCardMeta.getRefreshStatus();
            if(refreshStatus ==  null || refreshStatus != RefreshStatus.STATUS_NOREFRESH) {
                Long createdTime = twitterCardMeta.getCreatedTime();
                if(createdTime != null && createdTime < now - MAX_REFRESHING_AGE_MILLIS) {
                    // The record is too old, no more refreshing...
                    ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_NOREFRESH);
                    // ((TwitterCardMetaBean) twitterCardMeta).setNextRefreshTime(Long.MAX_VALUE);  // ????
                    ((TwitterCardMetaBean) twitterCardMeta).setNextRefreshTime(0L);   // ???
                    // ...
                } else {
                    ((TwitterCardMetaBean) twitterCardMeta).setRefreshStatus(RefreshStatus.STATUS_SCHEDULED);
                    Long refreshInterval = twitterCardMeta.getRefreshInterval();
                    if(refreshInterval == null || refreshInterval < MIN_REFRESH_INTERVAL) {
                        if(refreshInterval == null) {
                            if(log.isLoggable(Level.INFO)) log.info("refreshInterval is set to the default value: " + REFRESH_INTERVAL);
                            refreshInterval = REFRESH_INTERVAL;  // Default value
                        } else if(refreshInterval < MIN_REFRESH_INTERVAL) {
                            if(log.isLoggable(Level.WARNING)) log.warning("Input refreshInterval is too smmal, " + refreshInterval + ". Resetting it to the minimum value: " + MIN_REFRESH_INTERVAL);
                            refreshInterval = MIN_REFRESH_INTERVAL;
                        }
                        ((TwitterCardMetaBean) twitterCardMeta).setRefreshInterval(refreshInterval);
                    }
                    ((TwitterCardMetaBean) twitterCardMeta).setNextRefreshTime(now + refreshInterval);
                }
            } else {
                // This should not happen.... (because we filter out records based on refreshStatus....)
                log.warning("Invalid refreshStatus for twitterCardMeta = " + twitterCardMeta.getGuid());
                throw new InternalServerErrorException("Invalid refreshStatus for twitterCardMeta = " + twitterCardMeta.getGuid());
            }
            // ...

        } catch (IOException e) {
            throw new BaseException("Error processing targetUrl = " + targetUrl, e);
        } catch (Exception e) {
            throw new BaseException("Unknown error processing targetUrl = " + targetUrl, e);
        } finally{
            // ???
        }
        
        log.finer("END: processTwitterCardMetaFetch()");
        return twitterCardMeta;
    }


    

}
