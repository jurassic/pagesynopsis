package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.LinkListJsBean;
import com.pagesynopsis.wa.service.LinkListWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class LinkListHelper
{
    private static final Logger log = Logger.getLogger(LinkListHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private LinkListHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private LinkListWebService fetchRequestWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private LinkListWebService getLinkListService()
    {
        if(fetchRequestWebService == null) {
            fetchRequestWebService = new LinkListWebService();
        }
        return fetchRequestWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class LinkListHelperHolder
    {
        private static final LinkListHelper INSTANCE = new LinkListHelper();
    }

    // Singleton method
    public static LinkListHelper getInstance()
    {
        return LinkListHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public LinkListJsBean getLinkList(String guid) 
    {
        LinkListJsBean bean = null;
        try {
            bean = getLinkListService().getLinkList(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public LinkListJsBean getLinkListByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getLinkListByTargetUrl(targetUrl, cutoff);
    }
    public LinkListJsBean getLinkListByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getLinkListByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private LinkListJsBean getLinkListByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        LinkListJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<LinkListJsBean> beans = getLinkListService().findLinkLists(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getLinkListService().getLinkList(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }
    

    public List<String> getLinkListKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getLinkListService().findLinkListKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<LinkListJsBean> getLinkListsForRefreshProcessing(Integer maxCount)
    {
        List<LinkListJsBean> linkLists = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            linkLists = getLinkListService().findLinkLists(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return linkLists;
    }


}
