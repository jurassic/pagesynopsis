package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.VideoSetJsBean;
import com.pagesynopsis.wa.service.VideoSetWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class VideoSetHelper
{
    private static final Logger log = Logger.getLogger(VideoSetHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private VideoSetHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private VideoSetWebService fetchRequestWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private VideoSetWebService getVideoSetService()
    {
        if(fetchRequestWebService == null) {
            fetchRequestWebService = new VideoSetWebService();
        }
        return fetchRequestWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class VideoSetHelperHolder
    {
        private static final VideoSetHelper INSTANCE = new VideoSetHelper();
    }

    // Singleton method
    public static VideoSetHelper getInstance()
    {
        return VideoSetHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public VideoSetJsBean getVideoSet(String guid) 
    {
        VideoSetJsBean bean = null;
        try {
            bean = getVideoSetService().getVideoSet(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public VideoSetJsBean getVideoSetByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getVideoSetByTargetUrl(targetUrl, cutoff);
    }
    public VideoSetJsBean getVideoSetByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getVideoSetByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private VideoSetJsBean getVideoSetByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        VideoSetJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<VideoSetJsBean> beans = getVideoSetService().findVideoSets(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getVideoSetService().getVideoSet(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }
    

    public List<String> getVideoSetKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getVideoSetService().findVideoSetKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD:
    public List<VideoSetJsBean> getVideoSetsForRefreshProcessing(Integer maxCount)
    {
        List<VideoSetJsBean> videoSets = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            videoSets = getVideoSetService().findVideoSets(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return videoSets;
    }


}
