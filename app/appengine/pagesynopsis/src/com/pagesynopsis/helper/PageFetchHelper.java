package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.PageFetchJsBean;
import com.pagesynopsis.wa.service.PageFetchWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class PageFetchHelper
{
    private static final Logger log = Logger.getLogger(PageFetchHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private PageFetchHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private PageFetchWebService pageFetchWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private PageFetchWebService getPageFetchService()
    {
        if(pageFetchWebService == null) {
            pageFetchWebService = new PageFetchWebService();
        }
        return pageFetchWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class PageFetchHelperHolder
    {
        private static final PageFetchHelper INSTANCE = new PageFetchHelper();
    }

    // Singleton method
    public static PageFetchHelper getInstance()
    {
        return PageFetchHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public PageFetchJsBean getPageFetch(String guid) 
    {
        PageFetchJsBean bean = null;
        try {
            bean = getPageFetchService().getPageFetch(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }
    
    public PageFetchJsBean getPageFetchByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getPageFetchByTargetUrl(targetUrl, cutoff);
    }
    public PageFetchJsBean getPageFetchByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getPageFetchByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private PageFetchJsBean getPageFetchByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        PageFetchJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<PageFetchJsBean> beans = getPageFetchService().findPageFetches(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getPageFetchService().getPageFetch(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }
    

    public List<String> getPageFetchKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getPageFetchService().findPageFetchKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<PageFetchJsBean> getPageFetchesForRefreshProcessing(Integer maxCount)
    {
        List<PageFetchJsBean> pageFetches = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            pageFetches = getPageFetchService().findPageFetches(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return pageFetches;
    }


}
