package com.pagesynopsis.helper;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pagesynopsis.common.RefreshStatus;
import com.pagesynopsis.fe.WebException;
import com.pagesynopsis.fe.bean.PageInfoJsBean;
import com.pagesynopsis.wa.service.PageInfoWebService;
import com.pagesynopsis.wa.service.UserWebService;


public class PageInfoHelper
{
    private static final Logger log = Logger.getLogger(PageInfoHelper.class.getName());

    // Arbitrary...
    private static final long MAX_AGE_MILLIS = 3600 * 1000L * 24 * 30;   // ~1 month

    private PageInfoHelper() {}

    // TBD: Is this safe for concurrent calls??
    private UserWebService userWebService = null;
    private PageInfoWebService pageInfoWebService = null;
    // etc...


    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }   
    private PageInfoWebService getPageInfoService()
    {
        if(pageInfoWebService == null) {
            pageInfoWebService = new PageInfoWebService();
        }
        return pageInfoWebService;
    }
    // etc. ...


    // Initialization-on-demand holder.
    private static final class PageInfoHelperHolder
    {
        private static final PageInfoHelper INSTANCE = new PageInfoHelper();
    }

    // Singleton method
    public static PageInfoHelper getInstance()
    {
        return PageInfoHelperHolder.INSTANCE;
    }

    
    // Key or guid...
    public PageInfoJsBean getPageInfo(String guid) 
    {
        PageInfoJsBean bean = null;
        try {
            bean = getPageInfoService().getPageInfo(guid);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
        }
        return bean;
    }

    public PageInfoJsBean getPageInfoByTargetUrl(String targetUrl) 
    {
        // Long cutoff = System.currentTimeMillis() - MAX_AGE_MILLIS;
        Long cutoff = null;
        return getPageInfoByTargetUrl(targetUrl, cutoff);
    }
    public PageInfoJsBean getPageInfoByTargetUrl(String targetUrl, Long timeCutoff) 
    {
        return getPageInfoByTargetUrl(targetUrl, timeCutoff, false);  // This should really be true.. ????? 
    }
    private PageInfoJsBean getPageInfoByTargetUrl(String targetUrl, Long timeCutoff, boolean refreshingRecordOnly) 
    {
        PageInfoJsBean bean = null;
        try {
            String filter = "targetUrl=='" + targetUrl + "'";
            if(timeCutoff != null && timeCutoff > 0L) {
                filter += " && createdTime >= " + timeCutoff;
            }
            // TBD: This requires new datastore index....
            if(refreshingRecordOnly == true) {
                // We should not return stale record...
                filter += " && refreshStatus != " + RefreshStatus.STATUS_NOREFRESH;
            }
            String ordering = "createdTime desc";
            Long offset = 0L;
            Integer count = 1;
            List<PageInfoJsBean> beans = getPageInfoService().findPageInfos(filter, ordering, null, null, null, null, offset, count);
            if(beans != null && !beans.isEmpty()) {
                bean = beans.get(0);
                String guid = bean.getGuid();
                try {
                    bean = getPageInfoService().getPageInfo(guid);
                } catch (WebException e) {
                    log.log(Level.WARNING, "Failed to retrieve the bean for guid = " + guid, e);
                }
            }
            // else  ???
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve the bean for targetUrl = " + targetUrl, e);
        }
        return bean;
    }

    

    public List<String> getPageInfoKeysForRefreshProcessing(Integer maxCount)
    {
        List<String> keys = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            keys = getPageInfoService().findPageInfoKeys(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request key list.", e);
        }

        return keys;
    }
    
    // TBD: 
    public List<PageInfoJsBean> getPageInfosForRefreshProcessing(Integer maxCount)
    {
        List<PageInfoJsBean> pageInfos = null;
        long now = System.currentTimeMillis();
        String filter = "refreshStatus == " + RefreshStatus.STATUS_SCHEDULED + " && nextRefreshTime <= " + now;
        String ordering = "nextRefreshTime asc";
        try {
            pageInfos = getPageInfoService().findPageInfos(filter, ordering, null, null, null, null, 0L, maxCount);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve fetch request list.", e);
        }

        return pageInfos;
    }


}
