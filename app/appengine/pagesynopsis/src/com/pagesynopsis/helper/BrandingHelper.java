package com.pagesynopsis.helper;

import java.util.logging.Logger;

import com.pagesynopsis.app.util.ConfigUtil;
import com.pagesynopsis.common.AppBrand;


// temporary
public class BrandingHelper
{
    private static final Logger log = Logger.getLogger(BrandingHelper.class.getName());

    private String mBrand = null;
    private BrandingHelper() 
    {
        mBrand = initBrand();
    }

    // Initialization-on-demand holder.
    private static final class BrandingHelperHolder
    {
        private static final BrandingHelper INSTANCE = new BrandingHelper();
    }

    // Singleton method
    public static BrandingHelper getInstance()
    {
        return BrandingHelperHolder.INSTANCE;
    }

    private String initBrand()
    {
        String brand = ConfigUtil.getApplicationBrand();
        if(!AppBrand.isValidBrand(brand)) {
            brand = AppBrand.getDefaultBrand();
        }
        return brand;
    }

    
    public String getAppBrand()
    { 
        return mBrand;
    }
    
    public String getBrandDisplayName()
    { 
        return AppBrand.getDisplayName(mBrand);
    }

    public boolean isBrandPageSynopsis()
    {
        return AppBrand.BRAND_PAGESYNOPSIS.equals(mBrand);
    }

    public boolean isBrandPageSum()
    {
        return AppBrand.BRAND_PAGESUM.equals(mBrand);
    }

    public boolean isBrandPageRobots()
    {
        return AppBrand.BRAND_PAGEROBOTS.equals(mBrand);
    }

    public boolean isBrandPageMetas()
    {
        return AppBrand.BRAND_PAGEMETAS.equals(mBrand);
    }



}
